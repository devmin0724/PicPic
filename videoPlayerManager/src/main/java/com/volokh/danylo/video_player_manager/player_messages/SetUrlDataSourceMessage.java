package com.volokh.danylo.video_player_manager.player_messages;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import com.volokh.danylo.video_player_manager.cache.VideoLoader;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManagerCallback;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.io.IOException;

/**
 * This PlayerMessage calls {@link MediaPlayer#setDataSource(Context, Uri)} on the instance that is used inside {@link VideoPlayerView}
 */
public class SetUrlDataSourceMessage extends SetDataSourceMessage {

    private final String mVideoUrl;
    public static String TEMP_PATH = "";

    public SetUrlDataSourceMessage(VideoPlayerView videoPlayerView, String videoUrl, VideoPlayerManagerCallback callback) {
        super(videoPlayerView, callback);
        mVideoUrl = videoUrl;
    }

    @Override
    protected void performAction(VideoPlayerView currentPlayer) {
        currentPlayer.setDataSource(mVideoUrl);

//        TEMP_PATH = currentPlayer.getContext().getExternalCacheDir().getAbsolutePath();
//        VideoLoader loader = new VideoLoader(currentPlayer.getContext());
//        loader.DisplayVideo(mVideoUrl, currentPlayer, false);
    }
}
