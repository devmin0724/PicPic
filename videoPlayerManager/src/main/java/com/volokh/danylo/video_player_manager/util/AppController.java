package com.volokh.danylo.video_player_manager.util;

import android.app.Application;

/**
 * Created by Jong-min on 2015-07-21.
 */
public class AppController extends Application {

    public static String TEMP_SRC = "";

    public String getTempStr(){
        return getExternalCacheDir() + "/";
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TEMP_SRC = getExternalCacheDir() + "/";
    }

}
