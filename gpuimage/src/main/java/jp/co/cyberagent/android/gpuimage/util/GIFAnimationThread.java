package jp.co.cyberagent.android.gpuimage.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImageView;

/**
 * Created by devmin-sikkle on 2015-11-27.
 */
public class GIFAnimationThread extends Thread {

    GPUImageView myView;
    private boolean running = false;
    int delay = 30;
    ArrayList<Bitmap> arrAni = new ArrayList<Bitmap>();
    int navi = 0;

    public GIFAnimationThread(GPUImageView myView) {
        this.myView = myView;
    }

    public void setAni(ArrayList<Bitmap> arr, int delay) {
        this.arrAni = arr;
        this.delay = delay;
        this.running = true;
    }

    public void setRunning(boolean run) {
        this.running = run;
    }
    public boolean getRunning(){
        return running;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public void run() {
        try{
            while (running) {
                if (arrAni != null) {
                    synchronized (arrAni) {
                        myView.setImage(arrAni.get(navi));
                    }
                }
                try {
                    sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(navi < arrAni.size()-1){
                    navi = navi + 1;
                }else{
                    navi = 0;
                }
            }
        }catch (IndexOutOfBoundsException e){

        }
    }

}
