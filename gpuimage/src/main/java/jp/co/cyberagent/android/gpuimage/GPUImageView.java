package jp.co.cyberagent.android.gpuimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.SurfaceHolder;

import java.io.File;
import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.util.GIFAnimationThread;

public class GPUImageView extends GLSurfaceView {

    private GPUImage mGPUImage;
    private GPUImageFilter mFilter;
    private float mRatio = 0.0f;

    GIFAnimationThread thread;

    public GPUImageView(Context context) {
        super(context);
        init();
    }

    public GPUImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mGPUImage = new GPUImage(getContext());
        mGPUImage.setGLSurfaceView(this);
        thread = new GIFAnimationThread(this);
    }

    public void setImageArray(ArrayList<Bitmap> arr, int delay){
        if(!thread.getRunning()){
            thread.interrupt();
            thread.setRunning(false);
            thread = new GIFAnimationThread(this);
        }
        thread.setAni(arr, delay);
        if(!thread.isAlive()){
            try{
                thread.start();
            }catch (IllegalThreadStateException e){

            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mRatio == 0.0f) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = MeasureSpec.getSize(heightMeasureSpec);

            int newHeight;
            int newWidth;
            if (width / mRatio < height) {
                newWidth = width;
                newHeight = Math.round(width / mRatio);
            } else {
                newHeight = height;
                newWidth = Math.round(height * mRatio);
            }

            int newWidthSpec = MeasureSpec.makeMeasureSpec(newWidth, MeasureSpec.EXACTLY);
            int newHeightSpec = MeasureSpec.makeMeasureSpec(newHeight, MeasureSpec.EXACTLY);
            super.onMeasure(newWidthSpec, newHeightSpec);
        }
    }

    // TODO Should be an xml attribute. But then GPUImage can not be distributed as .jar anymore.
    public void setRatio(float ratio) {
        mRatio = ratio;
        requestLayout();
        mGPUImage.deleteImage();
    }

    /**
     * Set the filter to be applied on the image.
     *
     * @param filter Filter that should be applied on the image.
     */
    public void setFilter(GPUImageFilter filter) {
        mFilter = filter;
        mGPUImage.setFilter(filter);
        requestRender();
    }

    /**
     * Get the current applied filter.
     *
     * @return the current filter
     */
    public GPUImageFilter getFilter() {
        return mFilter;
    }

    /**
     * Sets the image on which the filter should be applied.
     *
     * @param bitmap the new image
     */
    public void setImage(final Bitmap bitmap) {
        mGPUImage.setImage(bitmap);
    }

    /**
     * Sets the image on which the filter should be applied from a Uri.
     *
     * @param uri the uri of the new image
     */
    public void setImage(final Uri uri) {
        mGPUImage.setImage(uri);
    }

    /**
     * Sets the image on which the filter should be applied from a File.
     *
     * @param file the file of the new image
     */
    public void setImage(final File file) {
        mGPUImage.setImage(file);
    }

    /**
     * Save current image with applied filter to Pictures. It will be stored on
     * the default Picture folder on the phone below the given folerName and
     * fileName. <br />
     * This method is async and will notify when the image was saved through the
     * listener.
     *
     * @param folderName the folder name
     * @param fileName the file name
     * @param listener the listener
     */
    public void saveToPictures(final String folderName, final String fileName,
                               final GPUImage.OnPictureSavedListener listener) {
        mGPUImage.saveToPictures(folderName, fileName, listener);
    }

    public Bitmap getCurrentBitmap(){
        return mGPUImage.getCuttentBitmap();
    }
}
