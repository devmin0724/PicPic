/*
 * Copyright (C) 2012 CyberAgent
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.cyberagent.android.gpuimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import java.io.File;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CustomImageView extends GLSurfaceView {

    private GPUImageRenderer renderer;
    GPUImageFilter mFilter;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void init(GPUImageFilter filter) {

        mFilter = filter;

        renderer = new GPUImageRenderer(mFilter);

        setZOrderOnTop(true);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.TRANSLUCENT);

//        renderer = new GPUImageRenderer(mFilter);
//        mGPUImageView = (GPUImageView) findViewById(R.id.test_gpu);
//        setEGLConfigChooser(8,8,8,8,16,0);
        setRenderer(renderer);
//        renderer.changeRenderer();

//        getHolder().setFormat(PixelFormat.TRANSLUCENT);

//        renderer.changeRenderer();


//        mGPUImage = new GPUImage(getContext());
//        mGPUImage.setGLSurfaceView(this);
//
//        getRenderer().changeRenderer();
    }

}
