LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := gpuimage-library
LOCAL_LDFLAGS := -Wl,--build-id
LOCAL_SRC_FILES := \
	D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\main\jni\Android.mk \
	D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\main\jni\Application.mk \
	D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\main\jni\empty.c \
	D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\main\jni\yuv-decoder.c \

LOCAL_C_INCLUDES += D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\main\jni
LOCAL_C_INCLUDES += D:\LJM\source\picpic\2016.03.13.16.51\PicPic\gpuimage\src\debug\jni

include $(BUILD_SHARED_LIBRARY)
