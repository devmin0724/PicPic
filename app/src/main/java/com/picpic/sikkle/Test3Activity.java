package com.picpic.sikkle;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.picpic.sikkle.beans.Video;
import com.picpic.sikkle.utils.newvideo.IVideoDownloadListener;
import com.picpic.sikkle.utils.newvideo.VideosAdapter;
import com.picpic.sikkle.utils.newvideo.VideosDownloader;

import java.util.ArrayList;


public class Test3Activity extends Activity implements IVideoDownloadListener {

    private static String TAG = "MainActivity";

    private Context context;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    private VideosAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Video> urls;
    VideosDownloader videosDownloader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test3);

        context = Test3Activity.this;
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        urls = new ArrayList<Video>();
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new VideosAdapter(Test3Activity.this, urls);
        mRecyclerView.setAdapter(mAdapter);

        videosDownloader = new VideosDownloader(context);
        videosDownloader.setOnVideoDownloadListener(this);

        getVideoUrls();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    LinearLayoutManager layoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                    int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                    int findFirstCompletelyVisibleItemPosition = layoutManager.findFirstCompletelyVisibleItemPosition();

                    Video video;
                    if (urls != null && urls.size() > 0) {
                        if (findFirstCompletelyVisibleItemPosition >= 0) {
                            video = urls.get(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(findFirstCompletelyVisibleItemPosition);
                            mAdapter.videoPlayerController.handlePlayBack(video);
                        } else {
                            video = urls.get(firstVisiblePosition);
                            mAdapter.videoPlayerController.setcurrentPositionOfItemToPlay(firstVisiblePosition);
                            mAdapter.videoPlayerController.handlePlayBack(video);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onVideoDownloaded(Video video) {
        mAdapter.videoPlayerController.handlePlayBack(video);
    }

    private void getVideoUrls() {
        Video video1 = new Video("0", "1", "http://gif.picpic.world/wnalswn7727@daum.net1447683324085_2.mp4");
        urls.add(video1);
        Video video2 = new Video("1", "2", "http://gif.picpic.world/1914408545320001449063961427_2.mp4");
        urls.add(video2);
        Video video3 = new Video("2", "3", "http://gif.picpic.world/twice95_2.mp4");
        urls.add(video3);
        Video video4 = new Video("3", "4", "http://gif.picpic.world/twice19_2.mp4");
        urls.add(video4);
        Video video5 = new Video("4", "5", "http://gif.picpic.world/NAS_Pb5ADo1_2.mp4");
        urls.add(video5);
        Video video6 = new Video("5", "6", "http://gif.picpic.world/twice97_2.mp4");
        urls.add(video6);
        Video video7 = new Video("6", "7", "http://gif.picpic.world/twice96_2.mp4");
        urls.add(video7);

        mAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        videosDownloader.startVideosDownloading(urls);
    }

}
