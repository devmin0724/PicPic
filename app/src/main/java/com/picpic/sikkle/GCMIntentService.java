package com.picpic.sikkle;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.picpic.sikkle.beans.NotificationItem;
import com.picpic.sikkle.ui.AndTagFeedActivity;
import com.picpic.sikkle.ui.InitActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.utils.AppController;

import java.util.Iterator;

public class GCMIntentService extends GCMBaseIntentService {
    private static final String tag = "GCMIntentService";
    private static final String PROJECT_ID = "108654133036";

    AlertDialog alertFull, alertPop;

    // 구글 api 페이지 주소 [https://code.google.com/apis/console/#project:긴 번호]
    // #project: 이후의 숫자가 위의 PROJECT_ID 값에 해당한다

    // public 기본 생성자를 무조건 만들어야 한다.
    public GCMIntentService() {
        this(PROJECT_ID);
    }

    public GCMIntentService(String project_id) {
        super(project_id);
    }

    public static int checkType(String type) {
//372030
        try {
            if (type.equals("PL")) {
                return NotificationItem.NOTIFIACIONT_POST_LIKE;
            } else if (type.equals("CL")) {
                return NotificationItem.NOTIFIACIONT_COMMENT_LIKE;
            } else if (type.equals("PC")) {
                return NotificationItem.NOTIFIACIONT_POST_COMMENT;
            } else if (type.equals("FM")) {
                return NotificationItem.NOTIFIACIONT_FOLLOW_ME;
            } else if (type.equals("RM")) {
                return NotificationItem.NOTIFIACIONT_REPIC_ME;
            } else if (type.equals("CMP")) {
                return NotificationItem.NOTIFIACIONT_REPORT_POST;
            } else if (type.equals("CMC")) {
                return NotificationItem.NOTIFIACIONT_REPORT_COMMENT;
            } else if (type.equals("FP")) {
                return NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE;
            } else if (type.equals("FI")) {
                return NotificationItem.NOTIFIACIONT_FOLLOW_ID;
            } else if (type.equals("FJA")) {
                return NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN;
            } else if (type.equals("FCA")) {
                return NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE;
            } else if (type.equals("NO")) {
                Log.e("GCM return Int", NotificationItem.NOTIFIACIONT_NOTICE + "");
                return NotificationItem.NOTIFIACIONT_NOTICE;
            } else if (type.equals("TMP")) {
                Log.e("GCM return Int", NotificationItem.NOTIFIACIONT_TAG_ME_POST + "");
                return NotificationItem.NOTIFIACIONT_TAG_ME_POST;
            } else if (type.equals("TMC")) {
                Log.e("GCM return Int", NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT + "");
                return NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT;
            } else if (type.equals("RP")) {
                Log.e("GCM return Int", NotificationItem.NOTIFIACIONT_REPLY_POST + "");
                return NotificationItem.NOTIFIACIONT_REPLY_POST;
            } else {
//            return NotificationItem.NOTIFIACIONT_POST_LIKE;
                return 99;
            }
        } catch (NullPointerException e) {
            return 99;
        }
    }

    /**
     * 푸시로 받은 메시지
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Bundle b = intent.getExtras();

        Iterator<String> iterator = b.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String value = b.get(key).toString();
            Log.e(tag, "onMessage. " + key + " : " + value);
        }

        NotificationItem item = new NotificationItem();
        try {
            if (intent.getStringExtra("type").equals("NO")) {
                if (intent != null) {
                    item.setTargetId1(intent.getStringExtra("title"));
                    item.setTargetName(intent.getStringExtra("body"));
                    item.setType(checkType(intent.getStringExtra("type")));
                    notiGoGo(item);
                }
            } else {
                try {
                    if (intent != null) {

                        if (intent.getStringExtra("follow_yn").equals("Y")) {
                            item.setIsFollow(true);
                        } else {
                            item.setIsFollow(false);
                        }

                        item.setUrl(intent.getStringExtra("url"));
                        item.setId(intent.getStringExtra("who_id"));
                        item.setTargetId1(intent.getStringExtra("target_id1"));
                        item.setTargetId2(intent.getStringExtra("target_id2"));
                        item.setEmail(intent.getStringExtra("who_email"));

                        if (intent.getStringExtra("confirm_yn").equals("Y")) {
                            item.setIsConfirm(true);
                        } else {
                            item.setIsConfirm(false);
                        }

                        item.setTargetName(intent.getStringExtra("target_name"));
                        item.setPro(intent.getStringExtra("profile_picture"));
                        item.setTime(intent.getStringExtra("from"));
                        try {
                            item.setType(checkType(intent.getStringExtra("type")));
                        } catch (NullPointerException e2) {
                            return;
                        }

                        notiGoGo(item);
                    }
                } catch (NullPointerException e3) {
                    item.setPro(intent.getStringExtra("profile_picture"));
                    item.setTime(intent.getStringExtra("from"));
                    item.setUrl(intent.getStringExtra("url"));
                    item.setType(checkType(intent.getStringExtra("type")));
                    item.setId(intent.getStringExtra("who_id"));
                    item.setEmail(intent.getStringExtra("who_email"));
                    item.setTargetId1(intent.getStringExtra("target_id1"));
                    item.setTargetId2(intent.getStringExtra("target_id2"));

                    notiGoGo(item);
                }
            }
        } catch (NullPointerException e) {

        }
    }

    private void notiGoGo(NotificationItem item) {

        try {
            final int navi = item.getType();

            String ment = "", title = "PicPic";

            Intent i = null;

            switch (navi) {
                case NotificationItem.NOTIFIACIONT_POST_LIKE:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmPL", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(com.picpic.sikkle.R.string.notification_post_like_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 0);
                    break;
                case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmCL", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_comment_like_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 1);
                    break;
                case NotificationItem.NOTIFIACIONT_POST_COMMENT:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmPC", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_post_comment_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 1);
                    break;
                case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmFM", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_follow_me_ment);
                    i = new Intent(this, UserFeedActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("email", item.getEmail());
                    break;
                case NotificationItem.NOTIFIACIONT_REPIC_ME:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmRM", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_post_repic_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 0);
                    break;
                case NotificationItem.NOTIFIACIONT_REPORT_POST:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmCMP", true)) {
                        return;
                    }
                    ment = getResources().getString(R.string.notification_report_post_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 0);
                    break;
                case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmCMC", true)) {
                        return;
                    }
                    ment = getResources().getString(R.string.notification_report_comment_ment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 1);
                    break;
                case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmFP", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_profile_change_ment);
                    i = new Intent(this, UserFeedActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("email", item.getEmail());
                    break;
                case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmFI", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_id_change_ment);
                    i = new Intent(this, UserFeedActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("email", item.getEmail());
                    break;
                case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmFJA", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_and_join_1_ment) + " &" + item.getTargetName() + getResources().getString(R.string.notification_and_join_2_ment);
                    i = new Intent(this, AndTagFeedActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("tag", item.getTargetId2());
                    break;
                case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    if (!AppController.getSp().getBoolean("alarmFCA", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_and_create_1_ment) + " &" + item.getTargetName() + getResources().getString(R.string.notification_and_create_2_ment);
                    i = new Intent(this, AndTagFeedActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("tag", item.getTargetId2());
                    break;
                case NotificationItem.NOTIFIACIONT_NOTICE:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    title = item.getTargetId1();
                    ment = item.getTargetName();
                    i = new Intent(this, InitActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    break;
                case NotificationItem.NOTIFIACIONT_TAG_ME_POST:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_tag_me_post);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 0);
                    break;
                case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_tag_me_comment);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 1);
                    break;
                case NotificationItem.NOTIFIACIONT_REPLY_POST:
                    if (!AppController.getSp().getBoolean("alarmAll", true)) {
                        return;
                    }
                    ment = item.getId() + getResources().getString(R.string.notification_post_comment_post);
                    i = new Intent(this, SinglePostV1Activity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    i.putExtra("postId", item.getTargetId1());
                    i.putExtra("navi", 1);
                    break;
            }

            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = new Notification(R.mipmap.ic_launcher, "PicPic", System.currentTimeMillis());
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            int tempA = Notification.DEFAULT_LIGHTS;
            int tempB = Notification.DEFAULT_LIGHTS;
            int tempC = Notification.DEFAULT_LIGHTS;
            if (AppController.getSp().getBoolean("isAlarmS", false)) {
                tempA = Notification.DEFAULT_SOUND;
            } else {
                tempA = Notification.DEFAULT_LIGHTS;
            }

            if (AppController.getSp().getBoolean("isAlarmF", false)) {
                tempB = Notification.DEFAULT_VIBRATE;
            } else {
                tempB = Notification.DEFAULT_LIGHTS;
            }

            if (AppController.getSp().getBoolean("isAlarmN", false)) {
                tempC = Notification.DEFAULT_LIGHTS;
            } else {
                tempC = Notification.DEFAULT_LIGHTS;
            }
            notification.defaults = Notification.DEFAULT_VIBRATE;
            notification.number = 1;

//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                i, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    i, PendingIntent.FLAG_CANCEL_CURRENT);
//        notification.setLatestEventInfo(this, title, ment, pendingIntent);
//        notification.se
//        notification.
//        nm.notify(1234, notification);
            //FLAG_ACTIVITY_BROUGHT_TO_FRONT

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Notification.Builder mBuilder = new Notification.Builder(this);
                Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.loading_5);
                mBuilder.setLargeIcon(b);
                mBuilder.setSmallIcon(R.drawable.picpic_logo_noti, 50);
                mBuilder.setTicker("PicPicT");
                mBuilder.setWhen(System.currentTimeMillis());
                mBuilder.setNumber(1);
                mBuilder.setContentTitle(title);
                mBuilder.setContentText(ment);
                mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setAutoCancel(true);

//            mBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

                Notification.BigTextStyle style = new Notification.BigTextStyle(mBuilder);
                style.setSummaryText("PicPic Notice");
                style.setBigContentTitle(title);
                style.bigText(ment);

                mBuilder.setStyle(style);

                nm.notify(111, mBuilder.build());
            } else {
                NotificationCompat.Builder mCompatBuilder = new NotificationCompat.Builder(this);
//        mCompatBuilder.setSmallIcon(R.drawable.ic_launcher);
                Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.loading_5);
                mCompatBuilder.setLargeIcon(b);
                mCompatBuilder.setSmallIcon(R.drawable.picpic_logo_noti, 50);
//                mCompatBuilder.setSmallIcon()
//        mCompatBuilder.setSmallIcon()
                mCompatBuilder.setTicker("PicPicTicker");
                mCompatBuilder.setWhen(System.currentTimeMillis());
                mCompatBuilder.setNumber(1);
                mCompatBuilder.setContentTitle(title);
                mCompatBuilder.setContentText(ment);
                mCompatBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
                mCompatBuilder.setContentIntent(pendingIntent);
                mCompatBuilder.setAutoCancel(true);

//        Notification.BigTextStyle style = new Notification.BigTextStyle(mCompatBuilder);

                nm.notify(222, mCompatBuilder.build());
            }
        } catch (NullPointerException e) {

        }

//        Notification.BigTextStyle style = new Notification.BigTextStyle();
    }

    //        Intent i = new Intent(this, SingleContentIntentActivity.class);
//        i.putExtra("pId", "");
//        i.putExtra("type", "");

    /**
     * 에러 발생시
     */
    @Override
    protected void onError(Context context, String errorId) {
        Log.d(tag, "onError. errorId : " + errorId);
    }

    /**
     * 단말에서 GCM 서비스 등록 했을 때 등록 id를 받는다
     */
    @Override
    protected void onRegistered(Context context, String regId) {
        Log.d(tag, "onRegistered. regId : " + regId);
    }

    /**
     * 단말에서 GCM 서비스 등록 해지를 하면 해지된 등록 id를 받는다
     */
    @Override
    protected void onUnregistered(Context context, String regId) {
        Log.d(tag, "onUnregistered. regId : " + regId);
    }
}
