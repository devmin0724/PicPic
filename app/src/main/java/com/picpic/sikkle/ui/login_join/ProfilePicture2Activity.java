package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.FileUploadActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ProfilePicture2Activity extends Activity implements View.OnClickListener {
    private static final int PICK_FROM_CAMERA = 1000;
    private static final int PICK_FROM_ALBUM = 1001;
    private static final int CROP_FROM_CAMERA = 1002;
    private static final int CROP_FROM_ALBUM = 1003;
    private static final int FILE_UPLOAD_RETURN = 2001;
    CircleImageView cimv;
    ImageView imvDumyPro, imvTakeCamera, imvTakeAlbum;
    Button btnComplete;
    File f = null;
    String fileName, imagePath;
    private Uri mImageCaptureUri;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_picture2);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("프로필 사진 변경 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        AppController.URL = getResources().getString(R.string.s_url);

        initViews();

        f = new File(AppController.BASE_SRC + AppController.getSp().getString("email", "") + "_" + System.currentTimeMillis() + ".jpg");

        fileName = AppController.getSp().getString("email", "") + "_" + System.currentTimeMillis() + ".jpg";
    }

    private void initViews() {
        cimv = (CircleImageView) findViewById(R.id.cimv_profile_picture2);

        imvDumyPro = (ImageView) findViewById(R.id.imv_profile_picture2);
        imvTakeAlbum = (ImageView) findViewById(R.id.imv_profile_picture2_album);
        imvTakeCamera = (ImageView) findViewById(R.id.imv_profile_picture2_take);

        btnComplete = (Button) findViewById(R.id.btn_profile_picture2_complete);

        imvTakeAlbum.setOnClickListener(this);
        imvTakeCamera.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
    }

    private void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String url = "tmp_" + String.valueOf(System.currentTimeMillis())
                + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), url));

        Log.e("filepath", mImageCaptureUri.getPath() + "");

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                mImageCaptureUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CROP_FROM_CAMERA:

            case CROP_FROM_ALBUM:

                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                opt.inSampleSize = 1;
                opt.inPreferredConfig = QrBitmapFactory.Options.Config.ARGB_8888;
                opt.inSampleSize = 1;

                Bitmap photo = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);

                //TODO jpg 파일 저장후 경로 저장
                imagePath = f.getAbsolutePath();

                cimv.setImageBitmap(photo);

                cimv.setVisibility(View.VISIBLE);
                imvDumyPro.setVisibility(View.GONE);

                break;


            case PICK_FROM_ALBUM:
                mImageCaptureUri = data.getData();
                Log.e("filepath", mImageCaptureUri.getPath());

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("output", Uri.fromFile(f));

                startActivityForResult(intent, CROP_FROM_ALBUM);

                break;
            case PICK_FROM_CAMERA:
                Intent intent2 = new Intent("com.android.camera.action.CROP");
                intent2.setDataAndType(mImageCaptureUri, "image/*");

                intent2.putExtra("aspectX", 1);
                intent2.putExtra("aspectY", 1);
                intent2.putExtra("output", Uri.fromFile(f));
                startActivityForResult(intent2, CROP_FROM_CAMERA);

                break;
            case FILE_UPLOAD_RETURN:
                if (resultCode == RESULT_OK) {
                    StringTransMethod stmFile = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getInt("result") == 0) {
                                    AppController.getSe().putString("profile_picture", AppController.URL + fileName);
                                    AppController.getSe().commit();
                                    setResult(RESULT_OK);
                                    finish();
                                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    Map<String, String> params = new HashMap<>();

                    params.put("myId", AppController.getSp().getString("email", ""));
                    params.put("url", fileName);

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 207, stmFile);
                    AppController.apiDataTaskNew.execute();

                }
                break;
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_profile_picture2_album:
                doTakeAlbumAction();
                break;
            case R.id.imv_profile_picture2_take:
                doTakePhotoAction();
                break;
            case R.id.btn_profile_picture2_complete:

                Intent i = new Intent(ProfilePicture2Activity.this, FileUploadActivity.class);
                i.putExtra("url", imagePath);
                i.putExtra("file", fileName);
                startActivityForResult(i, FILE_UPLOAD_RETURN);
                //TODO 사진 파일 전송 후 넘어가기

                break;
        }
    }

}
