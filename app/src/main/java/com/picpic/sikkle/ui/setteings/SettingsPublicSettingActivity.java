package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SettingsPublicSettingActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack;
    ImageView imvSearchYn, imvProfile, imvId, imvAndtag, imvAndtagJoin, imvRepic, imvPostLike, imvCommentLike, imvPostComment;
    FrameLayout imvComplete;

    ArrayList<Boolean> checkList = new ArrayList<>();

    SharedPreferences sp;
    SharedPreferences.Editor se;
    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_public_setting);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 공개설정 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_public_setting_back);

        imvComplete = (FrameLayout) findViewById(R.id.imv_settings_public_setting_next);

        imvSearchYn = (ImageView) findViewById(R.id.imv_settings_public_setting_search_yn);
        imvProfile = (ImageView) findViewById(R.id.imv_settings_public_setting_change_profile);
        imvId = (ImageView) findViewById(R.id.imv_settings_public_setting_change_id);
        imvPostLike = (ImageView) findViewById(R.id.imv_settings_public_setting_like_post);
        imvCommentLike = (ImageView) findViewById(R.id.imv_settings_public_setting_like_comment);
        imvPostComment = (ImageView) findViewById(R.id.imv_settings_public_setting_post_comment);
        imvAndtag = (ImageView) findViewById(R.id.imv_settings_public_setting_create_andtag);
        imvAndtagJoin = (ImageView) findViewById(R.id.imv_settings_public_setting_join_andtag);
        imvRepic = (ImageView) findViewById(R.id.imv_settings_public_setting_repic);

        layBack.setOnClickListener(this);

        imvComplete.setOnClickListener(this);

        imvSearchYn.setOnClickListener(this);
        imvProfile.setOnClickListener(this);
        imvId.setOnClickListener(this);
        imvPostLike.setOnClickListener(this);
        imvCommentLike.setOnClickListener(this);
        imvPostComment.setOnClickListener(this);
        imvAndtag.setOnClickListener(this);
        imvAndtagJoin.setOnClickListener(this);
        imvRepic.setOnClickListener(this);

        sp = AppController.getSp();
        se = AppController.getSe();

        imvSearchYn.setSelected(sp.getBoolean("isSearchIdOpen", true));
        imvProfile.setSelected(sp.getBoolean("isProfileChangeOpen", true));
        imvId.setSelected(sp.getBoolean("isIdChangeOpen", true));
        imvPostLike.setSelected(sp.getBoolean("isPostLike", true));
        imvCommentLike.setSelected(sp.getBoolean("isCommentLike", true));
        imvPostComment.setSelected(sp.getBoolean("isPostComment", true));
        imvAndtag.setSelected(sp.getBoolean("isCreateAndTagOpen", true));
        imvAndtagJoin.setSelected(sp.getBoolean("isJoinAndTagOpen", true));
        imvRepic.setSelected(sp.getBoolean("isRepicOpen", true));

//        sp.getBoolean("isSearchIdOpen", true);
//        sp.getBoolean("isProfileChangeOpen", true);
//        sp.getBoolean("isIdChangeOpen", true);
//        sp.getBoolean("isPostLike", true);
//        sp.getBoolean("isCommentLike", true);
//        sp.getBoolean("isPostComment", true);
//        sp.getBoolean("isCreateAndTagOpen", true);
//        sp.getBoolean("isJoinAndTagOpen", true);
//        sp.getBoolean("isRepicOpen", true);

        for (int i = 0; i < 9; i++) {
            checkList.add(true);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_public_setting_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_settings_public_setting_next:

                StringTransMethod stmPublicSetting = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);
                            if (jd.getInt("result") == 0) {
                                finish();
                            }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };

                Map<String, String> params = new HashMap<>();

                params.put("myId", AppController.getSp().getString("email", ""));
                params.put("fri_yn", checkList.get(0) ? "Y" : "N");
                params.put("fp_yn", checkList.get(1) ? "Y" : "N");
                params.put("fi_yn", checkList.get(2) ? "Y" : "N");
                params.put("pl_yn", checkList.get(3) ? "Y" : "N");
                params.put("cl_yn", checkList.get(4) ? "Y" : "N");
                params.put("pc_yn", checkList.get(5) ? "Y" : "N");
                params.put("fca_yn", checkList.get(6) ? "Y" : "N");
                params.put("fja_yn", checkList.get(7) ? "Y" : "N");
                params.put("rm_yn", checkList.get(8) ? "Y" : "N");

                AppController.getSe().putBoolean("isSearchIdOpen", checkList.get(0));
                AppController.getSe().putBoolean("isProfileChangeOpen", checkList.get(1));
                AppController.getSe().putBoolean("isIdChangeOpen", checkList.get(2));
                AppController.getSe().putBoolean("isPostLike", checkList.get(3));
                AppController.getSe().putBoolean("isCommentLike", checkList.get(4));
                AppController.getSe().putBoolean("isPostComment", checkList.get(5));
                AppController.getSe().putBoolean("isCreateAndTagOpen", checkList.get(6));
                AppController.getSe().putBoolean("isJoinAndTagOpen", checkList.get(7));
                AppController.getSe().putBoolean("isRepicOpen", checkList.get(8));
                AppController.getSe().commit();

                AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 210, stmPublicSetting);
                AppController.apiDataTaskNew.execute();

                break;
            case R.id.imv_settings_public_setting_search_yn:
                imvSearchYn.setSelected(!imvSearchYn.isSelected());
                checkList.set(0, !imvSearchYn.isSelected());
                break;
            case R.id.imv_settings_public_setting_change_profile:
                imvProfile.setSelected(!imvProfile.isSelected());
                checkList.set(1, !imvProfile.isSelected());
                break;
            case R.id.imv_settings_public_setting_change_id:
                imvId.setSelected(!imvId.isSelected());
                checkList.set(2, !imvId.isSelected());
                break;
            case R.id.imv_settings_public_setting_like_post:
                imvPostLike.setSelected(!imvPostLike.isSelected());
                checkList.set(3, !imvPostLike.isSelected());
                break;
            case R.id.imv_settings_public_setting_like_comment:
                imvCommentLike.setSelected(!imvCommentLike.isSelected());
                checkList.set(4, !imvCommentLike.isSelected());
                break;
            case R.id.imv_settings_public_setting_post_comment:
                imvPostComment.setSelected(!imvPostComment.isSelected());
                checkList.set(5, !imvPostComment.isSelected());
                break;
            case R.id.imv_settings_public_setting_create_andtag:
                imvAndtag.setSelected(!imvAndtag.isSelected());
                checkList.set(6, !imvAndtag.isSelected());
                break;
            case R.id.imv_settings_public_setting_join_andtag:
                imvAndtagJoin.setSelected(!imvAndtagJoin.isSelected());
                checkList.set(7, !imvAndtagJoin.isSelected());
                break;
            case R.id.imv_settings_public_setting_repic:
                imvRepic.setSelected(!imvRepic.isSelected());
                checkList.set(8, !imvRepic.isSelected());
                break;
        }
    }
}
