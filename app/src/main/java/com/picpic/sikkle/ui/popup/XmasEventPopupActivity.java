package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.picpic.sikkle.R;
//import com.picpic.sikkle.gifcamera.faceslice.FaceSliceCameraActivity;

public class XmasEventPopupActivity extends Activity implements View.OnClickListener{

    private static int FACE_SLICE_RETURN = 1111;

    ImageView imvGIF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_xmas_event_popup);

        imvGIF = (ImageView)findViewById(R.id.imv_xmas_gif);

        findViewById(R.id.imv_xmas_pop_btn).setOnClickListener(this);
        findViewById(R.id.imv_xmas_x).setOnClickListener(this);

//        try {
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("xmas.gif"));
//            imvGIF.setImageDrawable(gd);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imv_xmas_x :
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.imv_xmas_pop_btn :
//                startActivityForResult(new Intent(XmasEventPopupActivity.this, FaceSliceCameraActivity.class), FACE_SLICE_RETURN);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FACE_SLICE_RETURN){
            if(resultCode == RESULT_OK){
                setResult(RESULT_OK);
                finish();
            }else{
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }
}
