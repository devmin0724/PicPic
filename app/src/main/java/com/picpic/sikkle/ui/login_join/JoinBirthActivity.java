package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class JoinBirthActivity extends Activity implements View.OnClickListener {

    public static Activity ac;
    public final int RETURN_BIRTH_CHOICE = 11111;
    String tempEmail = "", tempPass = "";
    LinearLayout lay;
    TextView tv;
    Button btn;
    boolean isBirth = false;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join_birth);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 생일선택 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        tempEmail = getIntent().getExtras().getString("email");
        tempPass = getIntent().getExtras().getString("password");

        initViews();
    }

    private void initViews() {
        lay = (LinearLayout) findViewById(R.id.lay_join_birth_birth);
        lay.setOnClickListener(this);

        tv = (TextView) findViewById(R.id.tv_join_birth_birth);

        btn = (Button) findViewById(R.id.btn_join_birth_next);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_join_birth_next:
                if (!isBirth) {
                    lay.startAnimation(AppController.shake);
                    tv.setText(R.string.join_hint_birth_empty);
                    tv.setTextColor(Color.RED);
                } else {
//                    changeViews();
                    Intent i = new Intent(JoinBirthActivity.this, JoinSexActivity.class);
                    i.putExtra("email", tempEmail);
                    i.putExtra("password", tempPass);
                    i.putExtra("birth", tv.getText().toString());
                    startActivity(i);
                }
                break;
            case R.id.lay_join_birth_birth:
                startActivityForResult(new Intent(JoinBirthActivity.this, BirthDialogActivity.class), RETURN_BIRTH_CHOICE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RETURN_BIRTH_CHOICE) {
            if (resultCode == RESULT_OK) {
                isBirth = true;
                String tempYear = data.getExtras().getInt("year") + "/";
                String tempMonth = data.getExtras().getInt("month") + "/";
                String tempDay = data.getExtras().getInt("day") + "";

                tv.setText(tempYear + tempMonth + tempDay);
                tv.setTextColor(Color.BLACK);
            }
        }
    }
}
