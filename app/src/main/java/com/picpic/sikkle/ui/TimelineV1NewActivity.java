/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan. 
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna. 
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus. 
 * Vestibulum commodo. Ut rhoncus gravida arcu. 
 */

package com.picpic.sikkle.ui;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.etiennelawlor.quickreturn.library.enums.QuickReturnViewType;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;
import com.etiennelawlor.quickreturn.library.utils.QuickReturnUtils;
import com.etiennelawlor.quickreturn.library.utils.ScrollAPI;
import com.etiennelawlor.quickreturn.library.utils.TransAPI;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.VideoListViewAdapter;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.fragment.AllFragment;
import com.picpic.sikkle.fragment.FollowFragment;
import com.picpic.sikkle.fragment.FragmentTimeline;
import com.picpic.sikkle.fragment.HotFragment;
import com.picpic.sikkle.fragment.TimelineAllFragment;
import com.picpic.sikkle.fragment.TimelineFollowFragment;
import com.picpic.sikkle.fragment.TimelineHotFragment;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.BaseVideoItem;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.ItemFactory;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.ListViewItemPositionGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimelineV1NewActivity extends FragmentActivity implements View.OnClickListener {

    private final int MY_PERMISSION_REQUEST_STORAGE = 100;
    public static final int HOT_SINGLE_RETURN = 1111;
    public static final int ALL_SINGLE_RETURN = 1112;
    public static final int FOLLOW_SINGLE_RETURN = 1113;
    LinearLayout lay1, lay2, lay3;

    public static View header;
    public static Activity ac;

    LinearLayout laySearch;
    ImageView imvLogo;
    private static int listNavi = 0;
    boolean is = false, isStop = false, isFirst = true;
    private NativeAd nativeAd;
    ImageView imvNoti;
    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage;

    ViewPager vp;

    HotFragment hotF = HotFragment.create(0);
    AllFragment allF = AllFragment.create(1);
    FollowFragment followF = FollowFragment.create(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_timeline_v1_new);

        ac = this;
        AppController.tagNavi = 0;

        imvNoti = (ImageView) findViewById(R.id.imv_timeline_noti);
        layTimeline = (LinearLayout) findViewById(R.id.lay_timeline_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_timeline_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_timeline_camera);
        layAndTag = (LinearLayout) findViewById(R.id.lay_timeline_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_timeline_mypage);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 인기기피드 페지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        header = findViewById(R.id.lay_timeline_header);

        vp = (ViewPager) findViewById(R.id.vp_timeline);

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                lay1.setSelected(false);
                lay2.setSelected(false);
                lay3.setSelected(false);

                switch (position) {
                    case 0:
                        lay1.setSelected(true);
                        break;
                    case 1:
                        lay2.setSelected(true);
                        break;
                    case 2:
                        lay3.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        lay1 = (LinearLayout) findViewById(R.id.lay_timeline_header_1);
        lay2 = (LinearLayout) findViewById(R.id.lay_timeline_header_2);
        lay3 = (LinearLayout) findViewById(R.id.lay_timeline_header_3);

        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay3.setOnClickListener(this);

        imvLogo = (ImageView) findViewById(R.id.imv_main_timeline_home);

        laySearch = (LinearLayout) findViewById(R.id.imv_timeline_search);

        imvLogo.setOnClickListener(this);
        laySearch.setOnClickListener(this);

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layAndTag.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        layTimeline.setSelected(true);

        try {
            listNavi = getIntent().getExtras().getInt("listNavi");
            switch (listNavi) {
                case 0:
                    lay1.setSelected(true);
                    break;
                case 1:
                    lay2.setSelected(true);
                    break;
                case 2:
                    lay3.setSelected(true);
                    break;
            }
        } catch (NullPointerException e) {
            listNavi = 0;
            lay1.setSelected(true);
        }

        Log.e("listNavi 1", listNavi + "");

//        nativeAd = new NativeAd(TimelineV1NewActivity.this, getResources().getString(R.string.facenook_ad_id));
//        nativeAd.setAdListener(new AdListener() {
//
//            @Override
//            public void onError(Ad ad, AdError error) {
//                Log.e("error", error.getErrorMessage() + " / " + error.getErrorCode());
//            }
//
//            @Override
//            public void onAdLoaded(Ad ad) {
//                if (ad != nativeAd) {
//                    return;
//                }
////                checkPermission();
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    checkPermission();
//                } else {
//                    start();
//                }
//            }
//
//            @Override
//            public void onAdClicked(Ad ad) {
//
//            }
//        });
//
//        nativeAd.loadAd();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        } else {
            start();
        }
    }

    private void start() {
        isFirst = false;

        FragmentManager ff = getSupportFragmentManager();

        vp.setAdapter(new PagerAdapter(ff));

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                lay1.setSelected(false);
                lay2.setSelected(false);
                lay3.setSelected(false);

                switch (position) {
                    case 0:
                        lay1.setSelected(true);
                        break;
                    case 1:
                        lay2.setSelected(true);
                        break;
                    case 2:
                        lay3.setSelected(true);
                        break;
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        vp.setOffscreenPageLimit(6);

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
//        Log.i("per", "CheckPermission : " + checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE));

        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(TimelineV1NewActivity.this, "Read/Write external storage & Camera permission", Toast.LENGTH_SHORT).show();
            }

            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_STORAGE);
        } else {

            start();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                    start();
                } else {
                    Toast.makeText(getApplicationContext(), "permission 승인 필요", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        notiCheck();
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

    @Override
    protected void onDestroy() {

        android.util.Log.d("TAG", "TOTAL MEMORY : " + (Runtime.getRuntime().totalMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "MAX MEMORY : " + (Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "FREE MEMORY : " + (Runtime.getRuntime().freeMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "ALLOCATION MEMORY : " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + "MB");

        isStop = true;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_timeline_header_1:
//                getData(0);
                initTap(0);
                break;
            case R.id.lay_timeline_header_2:
//                getData(1);
                initTap(1);
                break;
            case R.id.lay_timeline_header_3:
//                getData(2);
                initTap(2);
                break;
            case R.id.imv_main_timeline_home:
//                if (lay1.isSelected()) {
//                    hotFragment = new TimelineHotFragment();
//                    pa.setFragment(hotFragment, 0);
//                    pa.notifyDataSetChanged();
//                    hotFragment.getData();
//                }
//                if (lay2.isSelected()) {
//                    allFragment = new TimelineAllFragment();
//                    pa.setFragment(allFragment, 1);
//                    pa.notifyDataSetChanged();
//                    allFragment.getData();
//                }
//                if (lay3.isSelected()) {
//                    followFragment = new TimelineFollowFragment();
//                    pa.setFragment(followFragment, 2);
//                    pa.notifyDataSetChanged();
//                    followFragment.getData();
//                }
                break;
            case R.id.imv_timeline_search:
                Intent i = new Intent(TimelineV1NewActivity.this, InterestSearchActivity.class);
                i.putExtra("navi", 0);
                startActivity(i);
                break;
            case R.id.lay_timeline_timeline:
                break;
            case R.id.lay_timeline_interest:
                i = new Intent(TimelineV1NewActivity.this, InterestFeedV1Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
//                startActivityForResult(i, POST_WRITE_RETURN);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_camera:
                startActivity(new Intent(TimelineV1NewActivity.this, CameraPopUp.class));
                break;
            case R.id.lay_timeline_noti:
                i = new Intent(TimelineV1NewActivity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_mypage:
//                i = new Intent(TimelineV1NewActivity.this, MyPageActivity.class);
//                i = new Intent(TimelineV1NewActivity.this, MyPageV1Activity.class);
                i = new Intent(TimelineV1NewActivity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }

    private void initTap(int a) {
        lay1.setSelected(false);
        lay2.setSelected(false);
        lay3.setSelected(false);
        switch (a) {
            case 0:
                lay1.setSelected(true);
                break;
            case 1:
                lay2.setSelected(true);
                break;
            case 2:
                lay3.setSelected(true);
                break;
        }
        vp.setCurrentItem(a);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HOT_SINGLE_RETURN) {
//            hotF.setLike(data.getExtras().getString("id"), data.getExtras().getBoolean("is"), data.getExtras().getInt("ct"));
        } else if (requestCode == ALL_SINGLE_RETURN) {
            allF.setLike(data.getExtras().getString("id"), data.getExtras().getBoolean("is"), data.getExtras().getInt("ct"));
        } else if (requestCode == FOLLOW_SINGLE_RETURN) {
            followF.setLike(data.getExtras().getString("id"), data.getExtras().getBoolean("is"), data.getExtras().getInt("ct"));
        }
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return hotF;
                case 1:
                    return allF;
                case 2:
                    return followF;
            }
            return FragmentTimeline.create(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

    }
}
