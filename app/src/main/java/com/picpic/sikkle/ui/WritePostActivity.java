package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.ui.popup.CategorySelectActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;
import com.picpic.sikkle.widget.ReviewImageView;
import com.picpic.sikkle.widget.tags.Link;
import com.picpic.sikkle.widget.tags.TagEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.GifDrawable;

public class WritePostActivity extends Activity implements View.OnClickListener {

    public static final int ANDTAG_RETURN = 40001;
    public static final int FRIEND_TAG_RETURN = 40002;
    private static final int FILE_UPLOAD_RETURN_1 = 1111;
    private static final int FILE_UPLOAD_RETURN_2 = 1112;
    private static final int FILE_UPLOAD_RETURN_3 = 1114;
    private static final int FILE_UPLOAD_RETURN_4 = 1115;
    private static final int POST_UPLOAD_RETURN = 1113;
    private static final int CATEGORY_SELECT_RETURN = 2222;
    LinearLayout layBack, layAndTag, layFace, layPinter, layTwitter, layTumblr, tvComplete;
    TextView tvAndTitle, tvAndBody, tvCount;
    ImageView imvAndX, imvAdd, imvSharp, imvAnd;
    ReviewImageView imvGIF;
    TagEditText edtInput;
    boolean isAndtag = false, isTagging = false, isAndTagMake = false;
    //    String url1, url2, url3, fileName1, fileName2, fileName3, thumb, thumbName;
    String url1, url2, fileName1, fileName2, fileName3, thumb, thumbName;
    ArrayList<int[]> hashtagSpans;
    ArrayList<int[]> calloutSpans;
    ArrayList<int[]> andSpans;
    String hashCollection = "";
    String userCollection = "";
    String andCollection = "";
    ListPopupWindow pop;

    LinearLayout layGuide;
    int intentNavi = 0;
    int categoryNavi = 0;

    ScrollView sv;

    int[] titles = {
            R.string.category_0, R.string.category_1, R.string.category_2, R.string.category_3, R.string.category_4, R.string.category_5, R.string.category_6, R.string.category_7, R.string.category_8, R.string.category_9, R.string.category_10, R.string.category_11, R.string.category_12, R.string.category_13, R.string.category_14, R.string.category_15, R.string.category_16, R.string.category_17, R.string.category_18, R.string.category_19, R.string.category_20, R.string.category_21, R.string.category_22, R.string.category_23, R.string.category_24, R.string.category_25
    };

    MultiAutoCompleteTextView.Tokenizer customT = new MultiAutoCompleteTextView.Tokenizer() {
        @Override
        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;

            while (i > 0 && text.charAt(i - 1) != ' ') {
                i--;
            }
            while (i < cursor && text.charAt(i) == ' ') {
                i++;
            }

            return i;
        }

        @Override
        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();

            while (i < len) {
                if (text.charAt(i) == ' ') {
                    return i;
                } else {
                    i++;
                }
            }

            return len;
        }

        @Override
        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();

            while (i > 0 && text.charAt(i - 1) == ' ') {
                i--;
            }

            if (i > 0 && text.charAt(i - 1) == ' ') {
                return text;
            } else {
                return text + " ";
            }
        }
    };
    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.length() > 1) {
                if (s.charAt(s.length() - 1) == ' ') {
                    isTagging = false;
                    edtInput.dismissDropDown();
                    if (pop != null) {
                        if (pop.isShowing()) {
                            pop.dismiss();
                        }
                    }
                } else {
                    int tagPos = s.toString().lastIndexOf("#");
                    int userPos = s.toString().lastIndexOf("@");
                    int andPos = s.toString().lastIndexOf("&");

                    int max = 0;
                    String maxStr = "";

                    if (tagPos > userPos) {
                        if (tagPos > andPos) {
                            max = tagPos;
                            maxStr = "#";
                        } else {
                            max = andPos;
                            maxStr = "&";
                        }
                    } else if (tagPos < userPos) {
                        if (userPos > andPos) {
                            max = userPos;
                            maxStr = "@";
                        } else {
                            max = andPos;
                            maxStr = "&";
                        }
                    } else {
                        if (tagPos > andPos) {
                            max = tagPos;
                            maxStr = "#";
                        } else {
                            max = andPos;
                            maxStr = "&";
                        }
                    }

                    if (max < s.length()) {
                        String tempS = s.toString().substring(max + 1, s.length());
                        Log.e("tempS", tempS);
                        isTagging = true;
                        if (maxStr.equals("#")) {
//                            getTagSearch(tempS);
                        } else if (maxStr.equals("@")) {
                            getUserTagSearch(tempS);
                        } else if (maxStr.equals("&")) {
//                            int fi = s.toString().indexOf("&");
//                            int li = s.toString().lastIndexOf("&");
//                            Log.e("f/l", fi + "/" + li);
//                            if (s.toString().lastIndexOf("&") == s.toString().indexOf("&")) {
//                                getAndTagSearch(tempS);
//                            } else {
//                                edtInput.setText(edtInput.getText().toString().substring(0, s.length() - 1));
//                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.write_andtag_exists), Toast.LENGTH_LONG).show();
//                                edtInput.setSelection(edtInput.length());
//                            }
                        }
                    }

                }
            } else {
                isTagging = false;
                edtInput.dismissDropDown();
                if (pop != null) {
                    if (pop.isShowing()) {
                        pop.dismiss();
                    }
                }
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    public void startExtMediaScan(Context mContext) {
//        mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(new File(url2)); //out is your output file
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://"
                            + Environment.getExternalStorageDirectory())));
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void hideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

//        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        inputManager.hideSoftInputFromWindow(edtInput.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_write_post);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("글쓰기 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();

        startExtMediaScan(getApplicationContext());
    }

    private void initViews() {
        layBack = (LinearLayout) findViewById(R.id.lay_write_back);
        layAndTag = (LinearLayout) findViewById(R.id.lay_write_andtag);
        layFace = (LinearLayout) findViewById(R.id.lay_write_sns_facebook);
        layPinter = (LinearLayout) findViewById(R.id.lay_write_sns_pinterest);
        layTwitter = (LinearLayout) findViewById(R.id.lay_write_sns_twitter);
        layTumblr = (LinearLayout) findViewById(R.id.lay_write_sns_tumblr);

        tvComplete = (LinearLayout) findViewById(R.id.tv_write_complete);
        tvAndTitle = (TextView) findViewById(R.id.tv_write_andtag_title);
        tvAndBody = (TextView) findViewById(R.id.tv_write_andtag_body);
        tvCount = (TextView) findViewById(R.id.tv_write_count);

        imvAndX = (ImageView) findViewById(R.id.imv_write_andtag_x);
        imvGIF = (ReviewImageView) findViewById(R.id.imv_write_gif);
        imvAdd = (ImageView) findViewById(R.id.imv_write_add);
        imvSharp = (ImageView) findViewById(R.id.imv_write_sharp);
        imvAnd = (ImageView) findViewById(R.id.imv_write_and);

        edtInput = (TagEditText) findViewById(R.id.edt_write_input);

        layGuide = (LinearLayout) findViewById(R.id.lay_write_post_guide);
        layGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layGuide.getVisibility() == View.VISIBLE) {
                    layGuide.setVisibility(View.GONE);
                }
            }
        });

        sv = (ScrollView) findViewById(R.id.sv_write);

        layBack.setOnClickListener(this);
        layFace.setOnClickListener(this);
        layPinter.setOnClickListener(this);
        layTwitter.setOnClickListener(this);
        layTumblr.setOnClickListener(this);

        imvAndX.setOnClickListener(this);
        imvAdd.setOnClickListener(this);
        imvSharp.setOnClickListener(this);
        imvAnd.setOnClickListener(this);

        tvComplete.setOnClickListener(this);

        edtInput.setTokenizer(customT);

        edtInput.addTextChangedListener(tw);
//        url1 = getIntent().getExtras().getString("url1");
        url2 = getIntent().getExtras().getString("url2");
//        url3 = getIntent().getExtras().getString("url3");
        try {
            thumb = getIntent().getExtras().getString("thumb");
        } catch (NullPointerException e) {
            thumb = "";
        }

        Log.e("url2", url2);

        GifDrawable gd = null;
        try {
//            gd = new GifDrawable(AppController.cameraPath2);
            gd = new GifDrawable(url2);
            imvGIF.setImageDrawable(gd);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        String tempCurrnet = System.currentTimeMillis() + "";
//        fileName2 = AppController.getSp().getString("email", "") + tempCurrnet + "_2.gif";

        fileName2 = url2.split("/")[url2.split("/").length-1];
        Log.e("fff", fileName2);

//        if (url1.equals(url2)) {
////            fileName1 = AppController.getSp().getString("email", "") + tempCurrnet + "_1.gif";
//            fileName2 = AppController.getSp().getString("email", "") + tempCurrnet + "_2.gif";
////            fileName3 = AppController.getSp().getString("email", "") + tempCurrnet + "_3.gif";
////            thumbName = AppController.getSp().getString("email", "") + tempCurrnet + ".jpg";
//        } else {
////            String tempSplit1[] = url1.split(".gif");
////            String tempUrl1 = tempSplit1[0];
////            int spot1 = tempUrl1.lastIndexOf("/") + 1;
////            tempUrl1 = tempUrl1.substring(spot1, tempUrl1.length());
////            fileName1 = tempUrl1 + ".gif";
//
//            String tempSplit2[] = url2.split(".gif");
//            String tempUrl2 = tempSplit2[0];
//            int spot2 = tempUrl2.lastIndexOf("/") + 1;
//            tempUrl2 = tempUrl2.substring(spot2, tempUrl2.length());
//            fileName2 = tempUrl2 + ".gif";
//
////            String tempSplit3[] = url3.split(".gif");
////            String tempUrl3 = tempSplit3[0];
////            int spot3 = tempUrl3.lastIndexOf("/") + 1;
////            tempUrl3 = tempUrl3.substring(spot3, tempUrl3.length());
////            fileName3 = tempUrl3 + ".gif";
//
////            String tempSplit4[] = url1.split(".gif");
////            String tempUrl4 = tempSplit4[0];
////            int spot4 = tempUrl4.lastIndexOf("/") + 1;
////            tempUrl4 = tempUrl4.substring(spot4, tempUrl4.length() - 2);
////            thumbName = tempUrl4 + ".jpg";
//        }

//        try {
//            if (thumb.equals("")) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = gd.seekToFrameAndGet(0);
//                String tempFName = Environment.getExternalStorageDirectory() + "/picpic/" + thumbName;
//                thumb = tempFName;
//                QrBitmapFactory.compressToFile(b, QrBitmapFactory.QrJPEG, tempFName, 100, opt);
//            }
//        } catch (NullPointerException e) {
//            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//            Bitmap b = gd.seekToFrameAndGet(0);
//            String tempFName = Environment.getExternalStorageDirectory() + "/picpic/" + thumbName;
//            thumb = tempFName;
//            QrBitmapFactory.compressToFile(b, QrBitmapFactory.QrJPEG, tempFName, 100, opt);
//        }

        linkSetting();

        intentNavi = getIntent().getExtras().getInt("navi");

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (intentNavi == 1) {
                    layAndTag.setVisibility(View.VISIBLE);

                    edtInput.setText(getIntent().getExtras().getString("tag_title") + " ");

                    isAndtag = true;

                    tvAndTitle.setText(getIntent().getExtras().getString("tag_title"));
                    tvAndTitle.setTag(getIntent().getExtras().getString("tag_id"));
                    tvAndBody.setText(getIntent().getExtras().getString("tag_body"));
                    layGuide.setVisibility(View.GONE);
                } else if (intentNavi == 2) {
                    layGuide.setVisibility(View.VISIBLE);
                } else {
                    layGuide.setVisibility(View.GONE);
                    layAndTag.setVisibility(View.GONE);
                }
            }
        };

        Log.e("intentNavi", intentNavi + "");

        handler.sendEmptyMessageDelayed(0, 500);

//        edtInput.requestFocus();

        hideKeyboard();

//        sv.smoothScrollTo(sv.getMaxScrollAmount(), 0);


        sv.post(new Runnable() {
            @Override
            public void run() {
//                sv.fullScroll(View.FO);
                sv.smoothScrollTo(sv.getHeight(), 0);
            }
        });
//        sv.fullScroll(View.FOCUS_DOWN);
    }

    private void andTagSwitch(boolean is) {
        if (is) {
            layAndTag.setVisibility(View.VISIBLE);

            isAndtag = true;

        } else {
            layAndTag.setVisibility(View.GONE);

            isAndtag = false;
            isAndTagMake = false;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void getTagSearch(String str) {

        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
//                MinUtils.d("return_tag_search", tempReturn);

                        if (!j.isNull("data")) {
                            JSONArray ja = new JSONArray(j.getString("data"));
                            final ArrayList<TagListItem> arr = new ArrayList<>();
                            ArrayList<String> arr2 = new ArrayList<>();
                            JSONObject jd;
                            TagListItem tli;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_name(jd.getString("tag_str"));
                                tli.setTag_id(jd.getString("tag_id"));

                                arr2.add(jd.getString("tag_str"));

                                arr.add(tli);
                            }

                            if (pop != null) {
                                if (pop.isShowing()) {
                                    pop.dismiss();
                                }
                            }

                            NewTagAdapter adapter = new NewTagAdapter(WritePostActivity.this, arr, 0);

                            pop = new ListPopupWindow(WritePostActivity.this);

                            pop.setAnchorView(edtInput);

                            pop.setAdapter(adapter);

                            pop.setModal(false);

                            pop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    String tagName = arr.get(position).getTag_name();
                                    String tempEdt = edtInput.getText().toString();
                                    int aa = tempEdt.lastIndexOf("#");
                                    if (aa >= 0) {
                                        String tempSSS = tempEdt.substring(0, aa + 1);
                                        String replace = tempSSS + tagName;
                                        edtInput.setText(replace.toString() + " ");
                                        edtInput.setSelection(edtInput.length());
                                        pop.dismiss();
                                    }
                                }
                            });
                            pop.show();
                        }

                    }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("tag_name", str);

        AppController.apiTaskNew = new APITaskNew(this, params, 501, stm);
        AppController.apiTaskNew.execute();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void getUserTagSearch(String str) {

        StringTransMethod stmUserTagSearch = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
//                MinUtils.d("return_tag_search", tempReturn);
                        if (!j.isNull("friend")) {
                            JSONArray ja = new JSONArray(j.getString("friend"));
                            final ArrayList<TagListItem> arr = new ArrayList<>();
                            JSONObject jd;
                            TagListItem tli;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_url(jd.getString("profile_picture"));
                                tli.setTag_name(jd.getString("id"));
                                tli.setTag_id(jd.getString("email"));

                                arr.add(tli);
                            }

                            NewTagAdapter adapter = new NewTagAdapter(WritePostActivity.this, arr, 1);

                            if (pop != null) {
                                if (pop.isShowing()) {
                                    pop.dismiss();
                                }
                            }

                            pop = new ListPopupWindow(WritePostActivity.this);

                            pop.setAnchorView(edtInput);
                            pop.setAdapter(adapter);

                            pop.setModal(false);

                            pop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    String tagName = arr.get(position).getTag_name();
                                    String tempEdt = edtInput.getText().toString();
                                    int aa = tempEdt.lastIndexOf("@");
                                    if (aa >= 0) {
                                        String tempSSS = tempEdt.substring(0, aa + 1);
                                        String replace = tempSSS + tagName;
                                        edtInput.setText(replace.toString() + " ");
                                        edtInput.setSelection(edtInput.length());
                                        pop.dismiss();
                                    }
                                }
                            });
                            pop.show();
                        }
                    }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("str", str);

        AppController.apiTaskNew = new APITaskNew(this, params, 512, stmUserTagSearch);
        AppController.apiTaskNew.execute();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void getAndTagSearch(String str) {
        StringTransMethod stmAndTagSearch = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
//                MinUtils.d("return_andtag_search", tempReturn);

                        if (!j.isNull("data")) {
                            JSONArray ja = new JSONArray(j.getString("data"));
                            final ArrayList<TagListItem> arr = new ArrayList<>();
                            JSONObject jd;
                            TagListItem tli;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_url(jd.getString("url"));
                                tli.setTag_name(jd.getString("tag_name"));
                                tli.setTag_id(jd.getString("tag_id"));
                                tli.setTag_body(jd.getString("explanation"));
                                tli.setJoin_cnt(jd.getInt("join_cnt"));

                                arr.add(tli);
                            }

                            NewTagAdapter adapter = new NewTagAdapter(WritePostActivity.this, arr, 2);

                            if (pop != null) {
                                if (pop.isShowing()) {
                                    pop.dismiss();
                                }
                            }

                            pop = new ListPopupWindow(WritePostActivity.this);

                            pop.setAnchorView(edtInput);
                            pop.setAdapter(adapter);

                            pop.setModal(false);

                            pop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    String tagName = arr.get(position).getTag_name();
                                    String tempEdt = edtInput.getText().toString();
                                    int aa = tempEdt.lastIndexOf("&");
                                    if (aa >= 0) {
                                        String tempSSS = tempEdt.substring(0, aa + 1);
                                        String replace = tempSSS + tagName;
                                        edtInput.setText(replace.toString() + " ");
                                        edtInput.setSelection(edtInput.length());
                                        pop.dismiss();

                                        andTagSwitch(true);
                                        tvAndTitle.setText(arr.get(position).getTag_name());
                                        tvAndTitle.setTag(arr.get(position).getTag_id());
                                        tvAndBody.setText(arr.get(position).getTag_body());
                                    }
                                }
                            });
                            pop.show();
                        }
                    } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("str", str);
        params.put("page", "1");

        AppController.apiTaskNew = new APITaskNew(this, params, 502, stmAndTagSearch);
        AppController.apiTaskNew.execute();
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onBackPressed() {
        if (intentNavi == 2) {
            startActivity(new Intent(WritePostActivity.this, NewGridTimelineActivity.class));
            finish();
        } else {
            if (pop != null) {
                if (pop.isShowing()) {
                    pop.dismiss();
                } else {
                    finish();
                    overridePendingTransition(0, 0);
                }
            } else {
                finish();
                overridePendingTransition(0, 0);
            }
        }
    }

    private void linkSetting() {
        Link linkHashtag = new Link(Pattern.compile("(#\\w+)"))
                .setTextColor(0xff5567d2);
//                .set;

        Link linkUsername = new Link(Pattern.compile("(@\\w+)"))
                .setTextColor(0xff484848);

//        Link linkAnd = new Link(Pattern.compile("(&\\w+)"))
//                .setTextColor(0xff9d9d9d);

        List<Link> links = new ArrayList<>();
        links.add(linkHashtag);
        links.add(linkUsername);
//        links.add(linkAnd);

        edtInput.addLinks(links);
    }

    private void getTags() {
        String commentsText = edtInput.getText().toString();

        hashtagSpans = getSpans(commentsText, '#');
        calloutSpans = getSpans(commentsText, '@');
        andSpans = MinUtils.getSpans(commentsText, '&');
        for (int i = 0; i < hashtagSpans.size(); i++) {
            hashCollection = hashCollection + commentsText.substring(hashtagSpans.get(i)[0] + 1, hashtagSpans.get(i)[1]) + ",";
        }
        for (int i = 0; i < calloutSpans.size(); i++) {
            userCollection = userCollection + commentsText.substring(calloutSpans.get(i)[0] + 1, calloutSpans.get(i)[1]) + ",";
        }
        for (int i = 0; i < andSpans.size(); i++) {
            andCollection = andCollection + commentsText.substring(andSpans.get(i)[0] + 1, andSpans.get(i)[1]) + ",";
        }

        if (hashCollection.length() > 1) {
            hashCollection = hashCollection.substring(0, hashCollection.length() - 1);
        }
        if (userCollection.length() > 1) {
            userCollection = userCollection.substring(0, userCollection.length() - 1);
        }
        if (andCollection.length() > 1) {
            andCollection = andCollection.substring(0, andCollection.length() - 1);
        }

        if(categoryNavi != 0){
            hashCollection += "," + getResources().getString(titles[categoryNavi]);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_write_back:
                if (intentNavi == 2) {
                    startActivity(new Intent(WritePostActivity.this, NewGridTimelineActivity.class));
                }
                finish();
                break;
            case R.id.lay_write_sns_facebook:
                if (layFace.isSelected()) {
                    layFace.setSelected(false);
                } else {
                    layFace.setSelected(true);
                }
                break;
            case R.id.lay_write_sns_pinterest:
                if (layPinter.isSelected()) {
                    layPinter.setSelected(false);
                } else {
                    layPinter.setSelected(true);
                }
                break;
            case R.id.lay_write_sns_twitter:
                if (layTwitter.isSelected()) {
                    layTwitter.setSelected(false);
                } else {
                    layTwitter.setSelected(true);
                }
                break;
            case R.id.lay_write_sns_tumblr:
                if (layTumblr.isSelected()) {
                    layTumblr.setSelected(false);
                } else {
                    layTumblr.setSelected(true);
                }
                break;
            case R.id.imv_write_andtag_x:
                andTagSwitch(false);
                String tempReplace = "&" + tvAndTitle.getText().toString();
                edtInput.setText(edtInput.getText().toString().replace(tempReplace, ""));
                break;
            case R.id.imv_write_add:
                //TODO 친구태그 추가
                startActivityForResult(new Intent(WritePostActivity.this, FriendTagReturnActivity.class), FRIEND_TAG_RETURN);
                break;
            case R.id.imv_write_sharp:
                //TODO 태그 추가
                String tempS = edtInput.getText().toString();
                edtInput.setText(tempS + " #");
                edtInput.setSelection(edtInput.length());
                break;
            case R.id.imv_write_and:
                //TODO &태그 추가
                if (!isAndtag) {
                    startActivityForResult(new Intent(WritePostActivity.this, MakeAndTagActivity.class), ANDTAG_RETURN);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.write_andtag_exists), Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tv_write_complete:
                tvComplete.setClickable(false);
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Upload Click").build());
                //TODO 태그들 집합

                startActivityForResult(new Intent(WritePostActivity.this, CategorySelectActivity.class), CATEGORY_SELECT_RETURN);

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ANDTAG_RETURN) {
            if (resultCode == RESULT_OK) {
                isAndTagMake = true;
                andTagSwitch(true);
                tvAndBody.setText(data.getExtras().getString("body"));
                tvAndTitle.setText(data.getExtras().getString("title"));

                edtInput.setText(edtInput.getText().toString() + " &" + data.getExtras().getString("title"));

            }
        } else if (requestCode == FRIEND_TAG_RETURN) {
            if (resultCode == RESULT_OK) {
                ArrayList<String> arr = (ArrayList<String>) data.getExtras().getSerializable("data");

                String tempUserTag = "";

                if (arr.size() > 0) {
                    for (int i = 0; i < arr.size(); i++) {
                        if (i == arr.size() - 1) {
                            tempUserTag = tempUserTag + " @" + arr.get(i) + " ";
                        } else {
                            tempUserTag = tempUserTag + " @" + arr.get(i);
                        }
                    }
                    edtInput.setText(edtInput.getText().toString() + tempUserTag);
                }
            }
        } else if (requestCode == FILE_UPLOAD_RETURN_1) {
            if (resultCode == RESULT_OK) {

                Intent i = new Intent(WritePostActivity.this, FileUploadActivity.class);
                i.putExtra("url", url2);
                i.putExtra("file", fileName2);
//                i.putExtra("file", url2.split("/")[url2.split("/").length-1]);
                startActivityForResult(i, FILE_UPLOAD_RETURN_2);
            }
        } else if (requestCode == FILE_UPLOAD_RETURN_2) {
//            Intent i = new Intent(WritePostActivity.this, FileUploadActivity.class);
//            i.putExtra("url", url3);
//            i.putExtra("file", fileName3);
//            startActivityForResult(i, FILE_UPLOAD_RETURN_3);
//        } else if (requestCode == FILE_UPLOAD_RETURN_3) {

            if (resultCode == RESULT_OK) {
                StringTransMethod stmFileSetting = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        StringTransMethod stmWrite = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);

                                    if (jd.getInt("result") == 0) {
//                                    File f1 = new File(url1);
//                        File f3 = new File(url3);
//                File f4 = new File(thumb);

//                                    f1.delete();
//                        f3.delete();
//                f4.delete();

//                        Log.e("return_data", return_msg);
//                        MinUtils.d("return_data", return_msg);
                                        setResult(RESULT_OK);
                                        finish();
                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };
                        Map<String, String> data2 = new HashMap<>();

                        data2.put("my_id", AppController.getSp().getString("email", ""));
                        data2.put("body", edtInput.getText().toString());
                        data2.put("url", fileName2);
                        data2.put("type", "W");
                        data2.put("tags", hashCollection);
                        data2.put("and_tag", andCollection);
                        data2.put("user_tags", userCollection);

                        AppController.apiDataTaskNew = new APIDataTaskNew(WritePostActivity.this, data2, 232, stmWrite);
                        AppController.apiDataTaskNew.execute();
                    }
                };

                Map<String, String> params = new HashMap<>();
                params.put("url", fileName2);

                AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 233, stmFileSetting);
                AppController.apiDataTaskNew.execute();

//            Intent i = new Intent(WritePostActivity.this, FileUploadActivity.class);
//            i.putExtra("url", thumb);
//            i.putExtra("file", thumbName);
//            startActivityForResult(i, FILE_UPLOAD_RETURN_4);
            }

        } else if (requestCode == FILE_UPLOAD_RETURN_4) {
            if (resultCode == RESULT_OK) {
                //TODO post쓰기
                StringTransMethod stmWrite = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);

                            if (jd.getInt("result") == 0) {
                                File f1 = new File(url1);
//                        File f3 = new File(url3);
//                File f4 = new File(thumb);

                                f1.delete();
//                        f3.delete();
//                f4.delete();

//                        Log.e("return_data", return_msg);
//                        MinUtils.d("return_data", return_msg);
                                setResult(RESULT_OK);
                                finish();
                            }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                Map<String, String> data2 = new HashMap<>();

                data2.put("my_id", AppController.getSp().getString("email", ""));
                data2.put("body", edtInput.getText().toString());
                data2.put("url", fileName2);
                data2.put("type", "W");
                data2.put("tags", hashCollection);
                data2.put("and_tag", andCollection);
                data2.put("user_tags", userCollection);

                AppController.apiDataTaskNew = new APIDataTaskNew(this, data2, 221, stmWrite);
                AppController.apiDataTaskNew.execute();

            }
        }else if(requestCode == CATEGORY_SELECT_RETURN){
            if(resultCode == RESULT_OK){

                categoryNavi = data.getExtras().getInt("result");

                if (isAndTagMake) {
                    StringTransMethod stmMakeAnd = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);

                                if (jd.getInt("result") == 0) {
                                    getTags();

                                    Intent i = new Intent(WritePostActivity.this, FileUploadActivity.class);
                                    i.putExtra("url", url2);
                                    i.putExtra("file", fileName2);
                                    startActivityForResult(i, FILE_UPLOAD_RETURN_2);
                                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    Map<String, String> params = new HashMap<>();

                    params.put("my_id", AppController.getSp().getString("email", ""));
                    params.put("body", tvAndBody.getText().toString());
                    params.put("tag_name", tvAndTitle.getText().toString());

                    AppController.apiTaskNew = new APITaskNew(this, params, 230, stmMakeAnd);
                    AppController.apiTaskNew.execute();

                } else {
                    getTags();

                    Intent i = new Intent(WritePostActivity.this, FileUploadActivity.class);
                    i.putExtra("url", url2);
                    i.putExtra("file", fileName2);
                    startActivityForResult(i, FILE_UPLOAD_RETURN_2);
                }

            }
        }
        tvComplete.setClickable(true);
    }

    class NewTagAdapter implements ListAdapter, Filterable {

        public int navi = 0;
        private DataSetObservable mDataSetObservable = new DataSetObservable();
        private LayoutInflater mInflater;
        private List<TagListItem> mFilteredContactList;
        private List<TagListItem> mContactList;
        private Filter mFilter;

        public NewTagAdapter(Context context, List<TagListItem> contactList, int inNavi) {
            navi = inNavi;
            init(context, contactList);
        }

        private void init(Context context, List<TagListItem> contactList) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mFilteredContactList = mContactList = contactList;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            mDataSetObservable.registerObserver(observer);
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            mDataSetObservable.unregisterObserver(observer);
        }

        @Override
        public int getCount() {
            return mFilteredContactList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFilteredContactList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.row_tag_list, null);

                holder = new Holder();
                holder.imv = (CircleImageView) convertView.findViewById(R.id.cimv_tag_list_row);
                holder.tv = (TextView) convertView.findViewById(R.id.tv_tag_list_row);
                holder.fimv = (FixedImageView) convertView.findViewById(R.id.imv_tag_list_row);
                holder.lay = (FrameLayout) convertView.findViewById(R.id.lay_tag_list_row);

                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            TagListItem model = mContactList.get(position);

            switch (navi) {
                case 0:
                    holder.lay.setVisibility(View.GONE);
                    holder.fimv.setVisibility(View.GONE);
                    holder.imv.setVisibility(View.GONE);
                    break;
                case 1:
                    holder.lay.setVisibility(View.VISIBLE);
                    holder.fimv.setVisibility(View.GONE);
                    holder.imv.setVisibility(View.VISIBLE);
                    holder.imv.setImageURLString(AppController.URL + model.getTag_url());
                    break;
                case 2:
                    holder.lay.setVisibility(View.VISIBLE);
                    holder.fimv.setVisibility(View.VISIBLE);
                    String tempUrl = model.getTag_url();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    holder.fimv.setImageURLString(AppController.URL + model.getTag_url(), AppController.URL + tempName);
                    holder.imv.setVisibility(View.GONE);
                    holder.fimv.setTag(model.getTag_body());
                    break;
            }

            holder.tv.setText(model.getTag_name());
            holder.tv.setTag(model.getTag_id());

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return getCount() == 0;
        }

        @Override
        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new TagFilter();
            }
            return mFilter;
        }

        public void notifyDataSetChanged() {
            mDataSetObservable.notifyChanged();
        }

        public void notifyDataSetInvalidated() {
            mDataSetObservable.notifyInvalidated();
        }

        private class Holder {
            public CircleImageView imv;
            public TextView tv;
            public FixedImageView fimv;
            public FrameLayout lay;
        }

        private class TagFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    filterResults.values = mContactList;
                    filterResults.count = mContactList.size();
                } else {
                    final String lastToken = constraint.toString().toLowerCase();
                    final int count = mContactList.size();
                    final List<TagListItem> list = new ArrayList<>();
                    TagListItem tagListItem;

                    for (int i = 0; i < count; i++) {
                        tagListItem = mContactList.get(i);
                        if (tagListItem.getTag_name().toLowerCase().contains(lastToken)
                                || tagListItem.getTag_id().toLowerCase().startsWith(lastToken)) {
                            list.add(tagListItem);
                        }
                    }

                    filterResults.values = list;
                    filterResults.count = list.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredContactList = (List<TagListItem>) results.values;
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }

    }
}
