package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.messenger.MessengerUtils;
import com.facebook.messenger.ShareToMessengerParams;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.TumblrLoginActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.pinterest.android.pdk.PDKCallback;
//import com.pinterest.android.pdk.PDKClient;
//import com.pinterest.android.pdk.PDKException;
//import com.pinterest.android.pdk.PDKResponse;
//import com.pinterest.android.pdk.PDKUser;
//import com.pinterest.android.pdk.Utils;

public class SharePopupForTimelineActivity extends Activity implements View.OnClickListener {

    public static final String TAG = "TumblrActivity";
    private static final int TAG_LOGIN = 400;
    private static final int TAG_LOGOUT = 401;
    private static final int REPIC_SAVE_RETURN = 10001;
    private static final int REQUEST_CODE_TUMBLR_LOGIN = 5593;
    private static final int REQUEST_CODE_SHARE_TO_MESSENGER = 10001;
    public static ProgressDialog mProgressDialog;
    private static boolean DEBUG = true;
    private final String USER_FIELDS = "id,image,counts,created_at,first_name,last_name,bio";
//    LinearLayout laySave, layFace, layTwitter, layTumblr, layPinterest, layKakao, layLink, layDownLoad, layMore, layFM;
    LinearLayout laySave, layFace, layTwitter, layTumblr, layPinterest, layKakao, layDownLoad, layMore, layFM;
    ShareDialog shareDialog;
    String strPostId, strBody, strUrl;
    CallbackManager callbackManager;
    int w = 0, h = 0;
    String access_token = "";
    String access_token_secret = "";
    boolean isTrans = false;

    private long latestId = -1;

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Uri urlToDownload;

    BroadcastReceiver onComlete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, getResources().getString(R.string.downlaod_complete), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_for_timline);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 공유하기 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        strPostId = getIntent().getExtras().getString("postId");
        strBody = getIntent().getExtras().getString("body");
        strUrl = getIntent().getExtras().getString("url");

//        w = getIntent().getExtras().getInt("w");
//        h = getIntent().getExtras().getInt("h");

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComlete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("SNS에 공유중입니다.");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        initViews();
    }

    private void shareItFacebookMessenger(File file){

        String tempFileName = AppController.BASE_SRC + file.getName() + ".gif";

        copyFile(file, tempFileName);

        if(new File(tempFileName) == null){
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_message), Toast.LENGTH_SHORT);
            return;
        }

        Uri uri = Uri.fromFile(new File(tempFileName));

        ShareToMessengerParams shareToMessengerParams = ShareToMessengerParams.newBuilder(
                uri, "image/gif").build();

        MessengerUtils.shareToMessenger(this, REQUEST_CODE_SHARE_TO_MESSENGER, shareToMessengerParams);

        mProgressDialog.dismiss();
    }

    private boolean copyFile(File file, String save_file) {
        boolean result;
        if (file != null && file.exists()) {
            FileInputStream fis = null;
            FileOutputStream newfos = null;
            try {
                fis = new FileInputStream(file);
                newfos = new FileOutputStream(save_file);
                int readcount = 0;
                byte[] buffer = new byte[1024];
                while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
                    newfos.write(buffer, 0, readcount);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    newfos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    private void shareIt(File file) {
        Intent sendFile = new Intent(Intent.ACTION_SEND);

        String tempFileName = AppController.BASE_SRC + file.getName() + ".gif";

        copyFile(file, tempFileName);

        Uri uri = Uri.fromFile(new File(tempFileName));

        sendFile.putExtra(Intent.EXTRA_STREAM, uri);
        sendFile.setType("image/gif");
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(sendFile, 0);
        for (int i = 0; i > activityList.size(); i++) {
            Log.e("list" + i, activityList.get(i).activityInfo.name);
        }
        for (final ResolveInfo app : activityList) {
//            if ((app.activityInfo.name).contains("kakao.story")) {
            if ((app.activityInfo.name).contains("kakao.talk")) {
                Log.e("name", app.activityInfo.name);
                ActivityInfo activity = app.activityInfo;
                ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

                sendFile.setComponent(name);
                sendFile.putExtra(Intent.EXTRA_STREAM, uri);
                sendFile.setType("image/gif");

                startActivity(sendFile);

                mProgressDialog.dismiss();

                break;
            }
        }

        mProgressDialog.dismiss();
    }

    private void shareItAll(File file) {
        Intent sendFile = new Intent(Intent.ACTION_SEND);

        String tempFileName = AppController.BASE_SRC + file.getName() + ".gif";

        copyFile(file, tempFileName);

        Uri uri = Uri.fromFile(new File(tempFileName));

        sendFile.putExtra(Intent.EXTRA_STREAM, uri);
        sendFile.setType("image/gif");

        startActivity(sendFile);

        mProgressDialog.dismiss();

    }

    private void initViews() {
        laySave = (LinearLayout) findViewById(R.id.lay_popup3_1);
//        layLink = (LinearLayout) findViewById(R.id.lay_popup3_1_2);
        layFace = (LinearLayout) findViewById(R.id.lay_popup3_2);
//        layTwitter = (LinearLayout) findViewById(R.id.lay_popup3_3);
//        layTumblr = (LinearLayout) findViewById(R.id.lay_popup3_4);
//        layPinterest = (LinearLayout) findViewById(R.id.lay_popup3_5);
        layFM = (LinearLayout)findViewById(R.id.lay_popup3_fm);

        layKakao = (LinearLayout) findViewById(R.id.lay_popup3_2_1);
        layDownLoad = (LinearLayout) findViewById(R.id.lay_popup3_3);
        layMore = (LinearLayout) findViewById(R.id.lay_popup3_4);

        layFM.setOnClickListener(this);

//        layLink.setOnClickListener(this);
        laySave.setOnClickListener(this);
        layKakao.setOnClickListener(this);
        layFace.setOnClickListener(this);
        layDownLoad.setOnClickListener(this);
        layMore.setOnClickListener(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_TUMBLR_LOGIN) {
            if (resultCode == TumblrLoginActivity.TUMBLR_LOGIN_RESULT_CODE_SUCCESS) {
                String token = data.getStringExtra(TumblrLoginActivity.TUMBLR_EXTRA_TOKEN);
                String tokenSecret = data.getStringExtra(TumblrLoginActivity.TUMBLR_EXTRA_TOKEN_SECRET);

                Log.d(TAG, "token       : " + token);
                Log.d(TAG, "tokenSecret : " + tokenSecret);

                String username = TumblrLoginActivity.getUsername(this);

//                TumblrLoginActivity.ExportDatabaseCSVTask t = new TumblrLoginActivity.ExportDatabaseCSVTask(AppController.SHARE_LINK + strPostId, strBody, strUrl);
//                t.execute("");
            } else if (resultCode == TumblrLoginActivity.TUMBLR_LOGIN_RESULT_CODE_FAILURE) {
                if (mProgressDialog != null) {
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void tumblrLogin() {
        mProgressDialog.show();

        Intent tumblrLoginIntent = new Intent(this, TumblrLoginActivity.class);
        tumblrLoginIntent.putExtra(TumblrLoginActivity.TUMBLR_CONSUMER_KEY, getResources().getString(R.string.TUMBLR_CONSUMER_KEY));
        tumblrLoginIntent.putExtra(TumblrLoginActivity.TUMBLR_CONSUMER_SECRET, getResources().getString(R.string.TUMBLR_CONSUMER_SECRET));
        startActivityForResult(tumblrLoginIntent, REQUEST_CODE_TUMBLR_LOGIN);
    }

    private void repicSave() {
        isTrans = true;
        StringTransMethod stmRepicSave = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    //repic aleady added
                    if (jd.getInt("result") == 0) {
                        if (jd.getString("duplicate_yn").equals("N")) {
                            finish();
                            overridePendingTransition(0, 0);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.repic_add_complete), Toast.LENGTH_LONG).show();
                        } else {
                            finish();
                            overridePendingTransition(0, 0);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.repic_add_aleady), Toast.LENGTH_LONG).show();
                        }
                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    } else {
                        finish();
                        overridePendingTransition(0, 0);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.repic_add_aleady), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (mProgressDialog != null) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    }
                    isTrans = false;
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("myId", AppController.getSp().getString("email", ""));
        params.put("post_id", strPostId);

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicSave);
        AppController.apiDataTaskNew.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_popup3_1:
                if (!isTrans) {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Repic Save Click" + "_" + strPostId).build());
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Repic Save Click").build());
                    repicSave();
                }
                break;
            case R.id.lay_popup3_2_1:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click").build());
//                shareIt();
                downAndSend(0);
                AppController.ActionLogInsert(strPostId, "K", this);
                break;
            case R.id.lay_popup3_2:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Share Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Share Click").build());
                if (strUrl.equals("")) {
                    strUrl = AppController.LOGO_LINK;
                }
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle("PicPic")
                        .setContentDescription(
                                strBody)
                        .setContentUrl(Uri.parse(AppController.SHARE_LINK + strPostId))
                        .setImageUrl(Uri.parse(strUrl))
                        .build();

                shareDialog.show(linkContent);

                AppController.ActionLogInsert(strPostId, "F", this);

                //TODO Facebook:F Kakao:K Download:D LinkCopy:I
                break;
//            case R.id.lay_popup3_1_2:
//                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
//                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Link Copy Click" + "_" + strPostId).build());
//                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
//                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Link Copy Click").build());
//                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//                clipboardManager.setText(strUrl);
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.copy_complete), Toast.LENGTH_SHORT).show();
//                AppController.ActionLogInsert(strPostId, "I");
//                break;
            case R.id.lay_popup3_3:
                urlToDownload = Uri.parse(strUrl);
                List<String> pathSegments = urlToDownload.getPathSegments();
                request = new DownloadManager.Request(urlToDownload);
                request.setTitle("PicPic");
                request.setDescription(strBody);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pathSegments.get(pathSegments.size() - 1));
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
                latestId = downloadManager.enqueue(request);

//                Uri tempUri = downloadManager.getUriForDownloadedFile(latestId);
//
//                Cursor cursor = getContentResolver().query(tempUri, null, null, null, null );
//                cursor.moveToNext();
//                String path = cursor.getString( cursor.getColumnIndex( "_data" ) );
//                cursor.close();
//
//                copyFile(new File(path), AppController.BASE_SRC + "PicPic_" + new File(path).getName() + ".gif");
//
//                deleteFile(path);

                finish();
                overridePendingTransition(0, 0);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.download_start), Toast.LENGTH_LONG).show();

                AppController.ActionLogInsert(strPostId, "D", this);
                break;
            case R.id.lay_popup3_4 :
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click").build());
                downAndSend(2);
                AppController.ActionLogInsert(strPostId, "M", this);
                break;
            case R.id.lay_popup3_fm :
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Messenger Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Messenger Click").build());

                downAndSend(1);

                AppController.ActionLogInsert(strPostId, "FM", this);
                break;
//            case R.id.lay_popup3_3:
//                //TODO 트위터
//                break;
        }
    }

    private void downAndSend(int a){
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("SNS에 공유하기 위해 파일 다운로드 중입니다.");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mProgressDialog.show();

        switch (a){
            case 0 :
                new DownLoadGIFForKaKao().execute();
                break;
            case 1 :
                //TODO 페메
                new DownLoadGIFForFacebookMessenger().execute();
                break;
            case 2 :
                new DownLoadGIFForShare().execute();
                break;
        }

    }

    class DownLoadGIFForKaKao extends AsyncTask<Void, String, String>{

        @Override
        protected String doInBackground(Void... voids) {
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                File f = AppController.fileCache.getFile(strUrl);
                URL imageUrl = null;
                imageUrl = new URL(strUrl);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                String contentType = conn.getContentType();
                try{
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();
                }catch (FileNotFoundException e){
//                ac.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imv.setImageResource(R.drawable.non_interest);
//                    }
//                });
                    return null;
                }

                shareIt(f);

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
            return null;
        }
    }
    class DownLoadGIFForFacebookMessenger extends AsyncTask<Void, String, String>{

        @Override
        protected String doInBackground(Void... voids) {
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                File f = AppController.fileCache.getFile(strUrl);
                URL imageUrl = null;
                imageUrl = new URL(strUrl);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                String contentType = conn.getContentType();
                try{
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();
                }catch (FileNotFoundException e){
//                ac.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imv.setImageResource(R.drawable.non_interest);
//                    }
//                });
                    return null;
                }

                shareItFacebookMessenger(f);

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
            return null;
        }
    }
    class DownLoadGIFForShare extends AsyncTask<Void, String, String>{

        @Override
        protected String doInBackground(Void... voids) {
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                File f = AppController.fileCache.getFile(strUrl);
                URL imageUrl = null;
                imageUrl = new URL(strUrl);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                String contentType = conn.getContentType();
                try{
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();
                }catch (FileNotFoundException e){
//                ac.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imv.setImageResource(R.drawable.non_interest);
//                    }
//                });
                    return null;
                }

                shareItAll(f);

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
            return null;
        }
    }

}
