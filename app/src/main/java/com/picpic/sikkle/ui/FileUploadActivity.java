package com.picpic.sikkle.ui;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FileUploadActivity extends Activity {
    Animation scale, rotate;
    ImageView imv;

    int serverResponseCode;

    InputStream is   = null;
    ByteArrayOutputStream baos = null;
    HttpURLConnection conn = null;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    /**
     * Called when the activity has detected the user's press of the back
     * key.  The default implementation simply finishes the current activity,
     * but you can override this to do whatever you want.
     */
    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        setContentView(R.layout.activity_data_loading);

        scale = AnimationUtils.loadAnimation(this, R.anim.scale);
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        imv = (ImageView) findViewById(R.id.imv_loading_1);
        try {
            imv.setBackgroundResource(R.drawable.loading_animation);
            AnimationDrawable frameAnimation = (AnimationDrawable) imv.getBackground();

            // Start the animation (looped playback by default).
            frameAnimation.start();
        }catch (OutOfMemoryError e){

        }

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("파일 업로드 페이지");
//        AppController.t.setAppInstallerId();
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        sendFile(getIntent().getExtras().getString("url"), getIntent().getExtras().getString("file"));
    }

    public void sendFile(final String imagePath, final String fileName) {
        new Thread(new Runnable() {
            public void run() {

                try{
                    uploadFile(imagePath, fileName);
                }catch (NullPointerException e){

                }

            }
        }).start();
    }

    public int uploadFile(final String sourceFileUri, String tempF) throws NullPointerException{

//        String fileName = sourceFileUri;
        String fileName = tempF;

        Log.e("aaa", sourceFileUri + "/" + tempF);

        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        FileInputStream fileInputStream = null;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 50 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            finish();

//            Log.e("uploadFile", "Source File not exist :"
//                    + uploadFilePath + "" + uploadFileName);

            runOnUiThread(new Runnable() {
                public void run() {
//                    messageText.setText("Source File not exist :"
//                            + uploadFilePath + "" + uploadFileName);
                }
            });

            return 0;

        } else {
            try {

                // open a URL connection to the Servlet
                fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(AppController.upload_server_url);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; ;name=\"uploaded_file\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                final String tempS = conn.getContent().toString();
                final String serverResponseMessage = conn.getResponseMessage();
//                Log.e("up", conn.getContent().toString());

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {

                            String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
                                    + sourceFileUri;

//                            Log.e("msg", serverResponseMessage + "/" + tempS);

                            try {
                                is = conn.getInputStream();
                                baos = new ByteArrayOutputStream();
                                byte[] byteBuffer = new byte[1024];
                                byte[] byteData = null;
                                int nLength = 0;
                                while((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                                    baos.write(byteBuffer, 0, nLength);
                                }
                                byteData = baos.toByteArray();

                                String response = new String(byteData);

                                Log.i("response", response);

                                if(response.equals("success")){
                                    setResult(RESULT_OK);
                                    finish();
                                }else{
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                setResult(RESULT_CANCELED);
                                finish();
                            }

//                            messageText.setText(msg);
//                            Toast.makeText(WriteActivity.this, "File Upload Complete.",
//                                    Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getApplicationContext(), "게시완료",
//                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    setResult(RESULT_CANCELED);
                    finish();
                }

                //close the streams //

            } catch (MalformedURLException ex) {

                finish();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
//                        messageText.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(getApplicationContext(), "MalformedURLException",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                finish();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
//                        messageText.setText("Got Exception : see logcat ");
                        Toast.makeText(getApplicationContext(), "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Exception", "Exception : "
                        + e.getMessage(), e);
            }finally {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    dos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return serverResponseCode;

        } // End else block
    }

}
