package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.InitActivity;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV1Activity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.setteings.SettingsActivity;
import com.picpic.sikkle.utils.AppController;

public class LogoutPopUpActivity extends Activity {

    TextView tvLeft, tvRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logout_pop_up);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("로그아웃 확인 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        tvLeft = (TextView) findViewById(R.id.tv_logout_left);
        tvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });
        tvRight = (TextView) findViewById(R.id.tv_logout_right);
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Logout Click").build());
                if (AppController.getSp().getString("register_form", "10001").equals("10002")) {
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    LoginManager.getInstance().logOut();
                }
//                AppController.getSe().putBoolean("isAutoLogin", false);
//                AppController.getSe().remove("protectAccount");
//                AppController.getSe().remove("alarmAll");
                AppController.getSe().clear();
                AppController.getSe().commit();
                finish();
//                MyPageActivity.ac.finish();
                MyPageV2Activity.ac.finish();
                SettingsActivity.ac.finish();
                overridePendingTransition(0, 0);
                startActivity(new Intent(LogoutPopUpActivity.this, InitActivity.class));
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
