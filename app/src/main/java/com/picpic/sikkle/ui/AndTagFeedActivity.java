package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.TimeLineAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter2;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedStopImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndTagFeedActivity extends Activity implements View.OnClickListener {

    private static final int AND_TAG_INFO_RETURN = 1111;
    private static final int AND_TAG_INFO_LIST_RETURN = 1112;
    String tempTag = "", tempTagId = "", tempFounderId = "", tempTagTitle = "", tempTagBody = "";
    ListView list;
    ImageView imvList, imvGrid;
    TextView tvId, tvTitle, tvBody, tvCount, tvJoin;
    FixedStopImageView fimvBg;
    CircleImageView cimv, cimv1, cimv2, cimv3;
    LinearLayout layNon, imvBack, imvShare, imvMore;
    StaggeredGridView gv;
    boolean isGrid = true, isFollow = false;
    TimelineResult m_ResultList = null;
    TimeLineAdapter m_ListAdapter = null;
    TimelineResult m_ResultList2 = null;
    TimeLineGridAdapter m_ListAdapter2 = null;
    GridResult m_ResultList3 = null;
    TimeLineGridAdapter2 m_ListAdapter3 = null;
    int gifWidth = 200, gifHeight = 200;
    private boolean lastItemVisibleFlag = false;
    int lastListCount = 0;
    boolean isLoading = false;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_and_tag_feed);

        tempTag = getIntent().getExtras().getString("tag");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("앤드태그 피드 페이지" + "_" + tempTag);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("앤드태그 피드 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();
    }


    private void initViews() {
        list = (ListView) findViewById(R.id.list_and_tag_feed);

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = inflater2.inflate(R.layout.header_and_tag_feed, null, false);

        imvBack = (LinearLayout) header.findViewById(R.id.imv_and_tag_feed_back);
        imvBack.setOnClickListener(this);
        imvShare = (LinearLayout) header.findViewById(R.id.imv_and_tag_feed_share);
        imvMore = (LinearLayout) header.findViewById(R.id.imv_and_tag_feed_more);
        imvList = (ImageView) header.findViewById(R.id.imv_and_tag_feed_sort_1);
        imvList.setOnClickListener(this);
        imvGrid = (ImageView) header.findViewById(R.id.imv_and_tag_feed_sort_2);
        imvGrid.setOnClickListener(this);

        fimvBg = (FixedStopImageView) header.findViewById(R.id.imv_and_tag_feed_bg);

        layNon = (LinearLayout) findViewById(R.id.lay_and_tag_feed_non);

        tvId = (TextView) header.findViewById(R.id.tv_and_tag_feed_founder_id);
        tvId.setOnClickListener(this);
        tvTitle = (TextView) header.findViewById(R.id.tv_and_tag_feed_title);
        tvBody = (TextView) header.findViewById(R.id.tv_and_tag_feed_body);
        tvJoin = (TextView) header.findViewById(R.id.tv_and_tag_feed_join);
        tvJoin.setOnClickListener(this);
        tvCount = (TextView) header.findViewById(R.id.tv_and_tag_feed_rank_count);
        tvCount.setOnClickListener(this);

        cimv = (CircleImageView) header.findViewById(R.id.cimv_and_tag_feed_founder_pro);
        cimv.setOnClickListener(this);
        cimv1 = (CircleImageView) header.findViewById(R.id.cimv_and_tag_feed_rank_1);
        cimv1.setOnClickListener(this);
        cimv2 = (CircleImageView) header.findViewById(R.id.cimv_and_tag_feed_rank_2);
        cimv2.setOnClickListener(this);
        cimv3 = (CircleImageView) header.findViewById(R.id.cimv_and_tag_feed_rank_3);
        cimv3.setOnClickListener(this);

        gv = (StaggeredGridView) findViewById(R.id.gv_and_tag_feed);

        gv.addHeaderView(header);

        list.addHeaderView(header);

        imvGrid.setSelected(true);
        isGrid = true;

        getData();
    }

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_id", tempTag);

        AppController.apiTaskNew = new APITaskNew(this, params, 503, stmDataResult);
        AppController.apiTaskNew.execute();

    }

    private void getGridData(boolean is, int page) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

        gv.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();

//        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("str", "");
//
//        AppController.apiTaskNew = new APITaskNew(params, 511, stmInitListRestult);
//        AppController.apiTaskNew.execute();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
//        params.put("str", AppController.getSp().getString("email", ""));
        params.put("str", "");
        params.put("type", "U");

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmInitListRestult2 = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                MinUtils.d("rr", result);
                JSONObject jd = new JSONObject(result);

                if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    Log.e("length", jarr.length() + "");

                    gv.setVisibility(View.VISIBLE);

                    if (jarr.length() < 1) {
                        Log.e("length", jarr.length() + "");
                    } else {
                        GridResult gr = new GridResult();
                        GridItem gi;
                        JSONObject j;
                        for (int i = 0; i < jarr.length(); i++) {
                            gi = new GridItem();

                            j = jarr.getJSONObject(i);

                            gi.setUrl(j.getString("url"));
                            gi.setPost_id(j.getString("post_id"));

                            if (!j.isNull("height1")) {
                                gi.setHeight1(j.getInt("height1"));

                            }
                            if (!j.isNull("height2")) {

                                gi.setHeight2(j.getInt("height2"));
                            }
                            if (!j.isNull("heightTh")) {
                                gi.setHeightTH(j.getInt("heightTh"));

                            }
                            if (!j.isNull("width1")) {

                                gi.setWidth1(j.getInt("width1"));
                            }
                            if (!j.isNull("width2")) {

                                gi.setWidth2(j.getInt("width2"));
                            }
                            if (!j.isNull("widthTh")) {
                                gi.setWidthTH(j.getInt("widthTh"));

                            }
                            if (!j.isNull("size1")) {

                                gi.setSize1(j.getInt("size1"));
                            }
                            if (!j.isNull("size2")) {
                                gi.setSize2(j.getInt("size2"));

                            }
                            if (!j.isNull("sizeTh")) {
                                gi.setSizeTH(j.getInt("sizeTh"));

                            }

                            gr.add(gi);

                        }
//                        initList(1);
                        updateResultList3(gr);
                    }
                }

            } catch (JSONException e) {

            }
        }
    };

    protected boolean updateResultList3(GridResult resultList) {

        Log.e("o1", "o1");

        if (resultList == null || resultList.size() == 0) {
            Log.e("o2", "o2");
            return false;
        }

        if (m_ResultList3 != null && gv != null && m_ListAdapter3 != null) {
            Log.e("o3", "o3");
            if (m_ResultList3 != resultList) {
                Log.e("o4", "o4");
                m_ResultList3.addAll(resultList);
                m_ListAdapter3.setResultList(m_ResultList3);
            }
            return true;
        }
        Log.e("o5", "o5");
        m_ResultList3 = resultList;

        m_ListAdapter3 = new TimeLineGridAdapter2(this, 0, m_ResultList3);

        gv.setVisibility(View.VISIBLE);

        gv.setAdapter(m_ListAdapter3);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(AndTagFeedActivity.this, SinglePostV1Activity.class);
                        i.putExtra("postId", m_ResultList3.get(position - 1).getPost_id());
                        i.putExtra("navi", 0);
                        startActivity(i);
                        //TODO 터치했을때 해동

                    }
                });

            }
        });

        gv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 8;

                    if (lastListCount != count) {
                        Log.e("list", count + "/" + page);

                        if ((view.getCount() - 1) % 8 == 0) {
                            if (!isLoading) {
                                isLoading = true;

                                getGridData(false, page + 1);

                                isLoading = false;
                                lastListCount = count;
                            }
                        }
                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        return true;
    }

    StringTransMethod stmDataResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                final JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {

                    if (jd.getString("explanation").equals("")) {
                        finish();
                        overridePendingTransition(0, 0);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_not_exists_tag), Toast.LENGTH_LONG).show();
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    tempTagBody = jd.getString("explanation");


                                    tvBody.setText(jd.getString("explanation"));

                                    tvCount.setText("+" + jd.getString("join_cnt"));

                                    tempTagTitle = "&" + jd.getString("tag_name");

                                    tvTitle.setText("&" + jd.getString("tag_name"));
                                    tvTitle.setTag(jd.getString("tag_id"));

                                    cimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));

                                    tvId.setText(jd.getString("id"));

                                    fimvBg.setImageURLString(AppController.URL + jd.getString("url"));

                                    JSONArray jarr = new JSONArray(jd.getString("friend"));

                                    switch (jarr.length()) {
                                        case 0:
                                            cimv1.setVisibility(View.GONE);
                                            cimv2.setVisibility(View.GONE);
                                            cimv3.setVisibility(View.GONE);
                                            break;
                                        case 1:
                                            cimv1.setImageURLString(AppController.URL + jarr.getJSONObject(0).getString("profile_picture"));
                                            cimv1.setTag(jarr.getJSONObject(0).getString("id"));
                                            cimv2.setVisibility(View.GONE);
                                            cimv3.setVisibility(View.GONE);
                                            break;
                                        case 2:
                                            cimv1.setImageURLString(AppController.URL + jarr.getJSONObject(0).getString("profile_picture"));
                                            cimv1.setTag(jarr.getJSONObject(0).getString("id"));
                                            cimv2.setImageURLString(AppController.URL + jarr.getJSONObject(1).getString("profile_picture"));
                                            cimv2.setTag(jarr.getJSONObject(1).getString("id"));
                                            cimv3.setVisibility(View.GONE);
                                            break;
                                        case 3:
                                            cimv1.setImageURLString(AppController.URL + jarr.getJSONObject(0).getString("profile_picture"));
                                            cimv1.setTag(jarr.getJSONObject(0).getString("id"));
                                            cimv2.setImageURLString(AppController.URL + jarr.getJSONObject(1).getString("profile_picture"));
                                            cimv2.setTag(jarr.getJSONObject(1).getString("id"));
                                            cimv3.setImageURLString(AppController.URL + jarr.getJSONObject(2).getString("profile_picture"));
                                            cimv3.setTag(jarr.getJSONObject(2).getString("id"));
                                            break;
                                        default:
                                            cimv1.setImageURLString(AppController.URL + jarr.getJSONObject(0).getString("profile_picture"));
                                            cimv1.setTag(jarr.getJSONObject(0).getString("id"));
                                            cimv2.setImageURLString(AppController.URL + jarr.getJSONObject(1).getString("profile_picture"));
                                            cimv2.setTag(jarr.getJSONObject(1).getString("id"));
                                            cimv3.setImageURLString(AppController.URL + jarr.getJSONObject(2).getString("profile_picture"));
                                            cimv3.setTag(jarr.getJSONObject(2).getString("id"));
                                            break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                getGridData(true, 1);
            }

        }
    };

    private void getInitListData() {
        m_ListAdapter = null;
        m_ResultList = null;
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_id", tempTag);
        params.put("page", "1");
        params.put("range", "N");

        AppController.apiTaskNew = new APITaskNew(this, params, 506, stmListResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmListResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                JSONObject jd = new JSONObject(result);
                TimelineResult tr = new TimelineResult();

                if (jd.getInt("result") == 0) {
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    for (int i = 0; i < jarr.length(); i++) {
                        JSONObject j = jarr.getJSONObject(i);

                        TimelineItem ti = new TimelineItem();

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        TimelineLastCommentItem tlci = new TimelineLastCommentItem();

                        JSONObject jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

                        tr.add(ti);


                    }
                    if (isGrid) {
                        initList(1);
                        updateResultList2(tr);
                    } else {
                        initList(0);
                        updateResultList(tr);
                    }
                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void initList(final int a) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (a) {
                    case 0:
                        list.setVisibility(View.VISIBLE);
                        gv.setVisibility(View.GONE);
                        layNon.setVisibility(View.GONE);
                        break;
                    case 1:
                        list.setVisibility(View.GONE);
                        gv.setVisibility(View.VISIBLE);
                        layNon.setVisibility(View.GONE);
                        break;
                    case 2:
                        list.setVisibility(View.GONE);
                        gv.setVisibility(View.GONE);
                        layNon.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    protected boolean updateResultList(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
                m_ListAdapter.setResultList(m_ResultList);
            }
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    m_ListAdapter.notifyDataSetChanged();
//                }
//            });

            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new TimeLineAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO 터치했을때 행동
                    }
                });

            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 15;
                    Log.e("count1", view.getCount() + "");
                    Log.e("count2", view.getChildCount() + "");
                    if ((view.getCount() - 1) % 15 == 0) {
                        StringTransMethod stmListResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    TimelineResult tr = new TimelineResult();

                                    if (jd.getInt("result") == 0) {
                                        JSONArray jarr = new JSONArray(jd.getString("data"));

                                        for (int i = 0; i < jarr.length(); i++) {
                                            JSONObject j = jarr.getJSONObject(i);

                                            TimelineItem ti = new TimelineItem();

                                            ti.setBody(j.getString("body"));
                                            ti.setId(j.getString("id"));
                                            ti.setComCount(j.getInt("com_cnt"));
                                            if (j.getString("like_yn").equals("Y")) {
                                                ti.setIsLike(true);
                                            } else if (j.getString("like_yn").equals("N")) {
                                                ti.setIsLike(false);
                                            } else {
                                                ti.setIsLike(false);
                                            }
                                            ti.setLikeCount(j.getInt("like_cnt"));
                                            if (!j.getString("and_tag_id").equals("")) {
                                                ti.setIsAndTag(true);
                                                ti.setAndTagId(j.getString("and_tag_id"));
                                                ti.setAndTagName(j.getString("and_tag"));
                                                ti.setAndTagBody(j.getString("and_tag_body"));
                                            } else {
                                                ti.setIsAndTag(false);
                                                ti.setAndTagId("");
                                                ti.setAndTagName("");
                                                ti.setAndTagBody("");
                                            }
                                            ti.setOwnerId(j.getString("email"));
                                            ti.setPlayCnt(j.getInt("play_cnt"));
                                            ti.setPro_url(j.getString("profile_picture"));
                                            ti.setPsot_id(j.getString("post_id"));
                                            ti.setUrl(j.getString("url"));
                                            ti.setTime(j.getString("date"));

                                            TimelineLastCommentItem tlci = new TimelineLastCommentItem();

                                            JSONObject jj = new JSONObject(j.getString("last_com"));

                                            if (jj.getString("id").equals("null")) {
                                                tlci.setIsExist(false);
                                            } else {
                                                tlci.setIsExist(true);
                                                tlci.setId(jj.getString("id"));
                                                tlci.setPro_url(jj.getString("profile_picture"));
                                                tlci.setBody(jj.getString("body"));
                                                tlci.setCom_id(jj.getString("com_id"));
                                                tlci.setEmail(jj.getString("email"));
                                            }

                                            ti.setTlci(tlci);

                                            tr.add(ti);


                                        }
                                        if (isGrid) {
                                            initList(1);
                                            updateResultList2(tr);
                                        } else {
                                            initList(0);
                                            updateResultList(tr);
                                        }
                                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("tag_id", tempTag);
                        params.put("page", (page + 1) + "");
                        params.put("range", "N");

                        AppController.apiTaskNew = new APITaskNew(AndTagFeedActivity.this, params, 506, stmListResult);
                        AppController.apiTaskNew.execute();

                    } else {

                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
            }
        });

        return true;
    }

    protected boolean updateResultList2(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList2 != null && gv != null && m_ListAdapter2 != null) {
            if (m_ResultList2 != resultList) {
                m_ResultList2.addAll(resultList);
                m_ListAdapter2.setResultList(m_ResultList2);
            }
            return true;
        }
        m_ResultList2 = resultList;

        m_ListAdapter2 = new TimeLineGridAdapter(this, 0, m_ResultList2);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gv.setAdapter(m_ListAdapter2);
            }
        });

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (position >= 0) {
                            Intent i = new Intent(AndTagFeedActivity.this, SinglePostV1Activity.class);
                            i.putExtra("postId", m_ResultList2.get(position - 1).getPsot_id());
                            i.putExtra("navi", 0);
                            startActivity(i);
                        }
                        //TODO 터치했을때 해동
                    }
                });

            }
        });

        gv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 15;
                    Log.e("count1", view.getCount() + "");
                    Log.e("count2", view.getChildCount() + "");
                    if ((view.getCount() - 1) % 15 == 0) {

                        StringTransMethod stmGridResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    TimelineResult tr = new TimelineResult();

                                    if (jd.getInt("result") == 0) {
                                        JSONArray jarr = new JSONArray(jd.getString("data"));

                                        for (int i = 0; i < jarr.length(); i++) {
                                            JSONObject j = jarr.getJSONObject(i);

                                            TimelineItem ti = new TimelineItem();

                                            ti.setBody(j.getString("body"));
                                            ti.setId(j.getString("id"));
                                            ti.setComCount(j.getInt("com_cnt"));
                                            if (j.getString("like_yn").equals("Y")) {
                                                ti.setIsLike(true);
                                            } else if (j.getString("like_yn").equals("N")) {
                                                ti.setIsLike(false);
                                            } else {
                                                ti.setIsLike(false);
                                            }
                                            ti.setLikeCount(j.getInt("like_cnt"));
                                            if (!j.getString("and_tag_id").equals("")) {
                                                ti.setIsAndTag(true);
                                                ti.setAndTagId(j.getString("and_tag_id"));
                                                ti.setAndTagName(j.getString("and_tag"));
                                                ti.setAndTagBody(j.getString("and_tag_body"));
                                            } else {
                                                ti.setIsAndTag(false);
                                                ti.setAndTagId("");
                                                ti.setAndTagName("");
                                                ti.setAndTagBody("");
                                            }
                                            ti.setOwnerId(j.getString("email"));
                                            ti.setPlayCnt(j.getInt("play_cnt"));
                                            ti.setPro_url(j.getString("profile_picture"));
                                            ti.setPsot_id(j.getString("post_id"));
                                            ti.setUrl(j.getString("url"));
                                            ti.setTime(j.getString("date"));

                                            TimelineLastCommentItem tlci = new TimelineLastCommentItem();

                                            JSONObject jj = new JSONObject(j.getString("last_com"));

                                            if (jj.getString("id").equals("null")) {
                                                tlci.setIsExist(false);
                                            } else {
                                                tlci.setIsExist(true);
                                                tlci.setId(jj.getString("id"));
                                                tlci.setPro_url(jj.getString("profile_picture"));
                                                tlci.setBody(jj.getString("body"));
                                                tlci.setCom_id(jj.getString("com_id"));
                                                tlci.setEmail(jj.getString("email"));
                                            }

                                            ti.setTlci(tlci);

                                            tr.add(ti);


                                        }
                                        if (isGrid) {
                                            initList(1);
                                            updateResultList2(tr);
                                        } else {
                                            initList(0);
                                            updateResultList(tr);
                                        }
                                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("tag_id", tempTag);
                        params.put("page", (page + 1) + "");
                        params.put("range", "N");

                        AppController.apiTaskNew = new APITaskNew(AndTagFeedActivity.this, params, 506, stmGridResult);
                        AppController.apiTaskNew.execute();

                    } else {

                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        return true;
    }

    @Override
    public void onClick(View v) {
        String myEmail = AppController.getSp().getString("email", "");
        String myId = AppController.getSp().getString("id", "");
        String tempEmail = tvId.getText().toString();
        String tempId = "";
        switch (v.getId()) {
            case R.id.imv_and_tag_feed_back:
                finish();
                break;
            case R.id.imv_and_tag_feed_sort_1:
                imvList.setSelected(true);
                imvGrid.setSelected(false);
                isGrid = false;
                getData();
                break;
            case R.id.imv_and_tag_feed_sort_2:
                imvList.setSelected(false);
                imvGrid.setSelected(true);
                isGrid = true;
                getData();
                break;
            case R.id.tv_and_tag_feed_join:
                Intent i0 = new Intent(AndTagFeedActivity.this, GIFCameraActivity.class);
                i0.putExtra("navi", 1);
                i0.putExtra("tag_id", tempTag);
                i0.putExtra("tag_title", tempTagTitle);
                i0.putExtra("tag_body", tempTagBody);
                startActivity(i0);
                finish();
                break;
            case R.id.cimv_and_tag_feed_founder_pro:
            case R.id.tv_and_tag_feed_founder_id:
                if (myEmail.equals(tempEmail)) {
                    startActivity(new Intent(AndTagFeedActivity.this, MyPageActivity.class));
                    finish();
                } else {
                    Intent i = new Intent(AndTagFeedActivity.this, UserFeedForIdActivity.class);
                    i.putExtra("id", tempEmail);
                    startActivity(i);
                }
                break;
            case R.id.tv_and_tag_feed_rank_count:
                //TODO 앤드태그 참여자 리스트
                Intent i2 = new Intent(AndTagFeedActivity.this, UserListActivity.class);
                i2.putExtra("pageNavi", UserListActivity.USER_LIST_AND_TAG_FOLLOWER);
                i2.putExtra("id", tempTag);
                startActivity(i2);
                break;
            case R.id.cimv_and_tag_feed_rank_1:
                tempId = cimv1.getTag().toString();
                if (myId.equals(tempId)) {
                    startActivity(new Intent(AndTagFeedActivity.this, MyPageActivity.class));
                    finish();
                } else {
                    Intent i3 = new Intent(AndTagFeedActivity.this, UserFeedForIdActivity.class);
                    i3.putExtra("id", tempId);
                    startActivity(i3);
                }
                break;
            case R.id.cimv_and_tag_feed_rank_2:
                tempId = cimv2.getTag().toString();
                if (myId.equals(tempId)) {
                    startActivity(new Intent(AndTagFeedActivity.this, MyPageActivity.class));
                    finish();
                } else {
                    Intent i4 = new Intent(AndTagFeedActivity.this, UserFeedForIdActivity.class);
                    i4.putExtra("id", tempId);
                    startActivity(i4);
                }
                break;
            case R.id.cimv_and_tag_feed_rank_3:
                tempId = cimv3.getTag().toString();
                if (myId.equals(tempId)) {
                    startActivity(new Intent(AndTagFeedActivity.this, MyPageActivity.class));
                    finish();
                } else {
                    Intent i5 = new Intent(AndTagFeedActivity.this, UserFeedForIdActivity.class);
                    i5.putExtra("id", tempId);
                    startActivity(i5);
                }
                break;
        }
    }
}
