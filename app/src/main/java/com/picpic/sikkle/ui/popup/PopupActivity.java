package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class PopupActivity extends Activity implements View.OnClickListener {

    //로그아웃
    public static final int POPUP_TYPE_1 = 10001;
    //비밀번호찾기
    public static final int POPUP_TYPE_2 = 10002;
    //탈퇴
    public static final int POPUP_TYPE_3 = 10003;

    TextView tvLogoutMsg, tvErr;
    LinearLayout layPw, laySignOut;
    EditText edtInputEmail;
    Button btnOk;

    int pageNavi = 0;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_popup);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("PopupActivity");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        pageNavi = getIntent().getExtras().getInt("pageNavi");

        initViews();
    }
    private void initViews() {
        tvLogoutMsg = (TextView) findViewById(R.id.tv_popup_logout_str);
        tvErr = (TextView) findViewById(R.id.tv_popup_err);

        layPw = (LinearLayout) findViewById(R.id.lay_popup_pw);
        laySignOut = (LinearLayout) findViewById(R.id.lay_popup_sign_out);

        edtInputEmail = (EditText) findViewById(R.id.edt_popup_input_email);

        btnOk = (Button) findViewById(R.id.btn_popup_down);

        btnOk.setOnClickListener(this);

        switch (pageNavi) {
            case POPUP_TYPE_1:
                tvLogoutMsg.setVisibility(View.VISIBLE);
                layPw.setVisibility(View.GONE);
                laySignOut.setVisibility(View.GONE);
                break;
            case POPUP_TYPE_2:
                tvLogoutMsg.setVisibility(View.GONE);
                layPw.setVisibility(View.VISIBLE);
                laySignOut.setVisibility(View.GONE);
                break;
            case POPUP_TYPE_3:
                tvLogoutMsg.setVisibility(View.GONE);
                layPw.setVisibility(View.GONE);
                laySignOut.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_popup_down:
                if (pageNavi == POPUP_TYPE_2) {
                    if (!edtInputEmail.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "이메일을 입력하세요.", Toast.LENGTH_SHORT).show();
                    } else {
                    }
                } else {
                    finish();
                }
                break;
        }
    }
}
