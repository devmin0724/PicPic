package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.CommentItem;
import com.picpic.sikkle.beans.CommentResult;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.swipemenuList.SwipeMenu;
import com.picpic.sikkle.widget.swipemenuList.SwipeMenuCreator;
import com.picpic.sikkle.widget.swipemenuList.SwipeMenuItem;
import com.picpic.sikkle.widget.swipemenuList.SwipeMenuListView;
import com.picpic.sikkle.widget.tags.Link;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.GifDrawable;

public class CommentActivity extends Activity implements View.OnClickListener {

    public static final int COMMENT_LIST_RETURN = 1111;
    public static final int COMMENT_WRITE_RETURN = 1112;
    public static final int COMMENT_EDIT_RETURN = 1113;
    public static final int COMMENT_DELETE_RETURN = 1114;
    public static final int PICK_FROM_ALBUM = 1115;

    ImageView imvLike, imvCommentGoGo;
    ProgressBar pbGoGo;
    LinearLayout layLike;
    FrameLayout layGogo;
    //    TagEditText edtGogo;
    AutoCompleteTextView edtGogo;
    TextView tvWholeLikeCount, tvwholeCommentCount;
    //    ListView list;
    SwipeMenuListView list;
    LinearLayout layImage, layImageX, layImageContent;
    ImageView imvImageContent;

    float xAtDown = 0, xAtUp = 0;
    boolean isTagging = false, isImage = false;
//    ListPopupWindow pop;

    String userCollection = "";
    ArrayList<int[]> calloutSpans;

    String[] tags = {
            "#aaaaa1", "#bbbbb1", "#ccccc1",
            "#aaaaa2", "#bbbbb2", "#ccccc2",
            "@aaaaa1", "@bbbbb1", "@ccccc1",
            "@aaaaa2", "@bbbbb2", "@ccccc2",
            "&aaaaa1", "&bbbbb1", "&ccccc1",
            "&aaaaa2", "&bbbbb2", "&ccccc2"
    };
    String[] tags1 = {
            "#aaaaa1", "#bbbbb1", "#ccccc1",
            "#aaaaa2", "#bbbbb2", "#ccccc2"
    };
    String[] tags2 = {
            "@aaaaa1", "@bbbbb1", "@ccccc1",
            "@aaaaa2", "@bbbbb2", "@ccccc2"
    };
    String[] tags3 = {
            "&aaaaa1", "&bbbbb1", "&ccccc1",
            "&aaaaa2", "&bbbbb2", "&ccccc2"
    };

    ArrayAdapter<String> adapter;
    String tempPostId = "", tempComId = "";
    //    boolean isLike = false, isEditing = false, isListEnd = false, isListLoading = false;
    boolean isEditing = false, isListEnd = false, isListLoading = false;
    DisplayMetrics metrics;
    int wholeCount = 0, pageNavi = 1;

    SwipeMenuItem optherItem1, optherItem2, optherItem3, optherItem4;

    MultiAutoCompleteTextView.Tokenizer token = new MultiAutoCompleteTextView.Tokenizer() {
        @Override
        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;
            if (edtGogo.getText().toString().length() > 1) {


                while (i > 0 && text.charAt(i - 1) != ' ') {
                    i--;
                }
                while (i < cursor && text.charAt(i) == ' ') {
                    i++;
                }
            }

            return i;
        }

        @Override
        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();
            if (edtGogo.getText().toString().length() > 1) {
                while (i < len) {
                    if (text.charAt(i) == ' ') {
                        return i;
                    } else {
                        i++;
                    }
                }
            }
            return len;
        }

        @Override
        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();
            if (edtGogo.getText().toString().length() > 1) {
                while (i > 0 && text.charAt(i - 1) == ' ') {
                    i--;
                }

                if (i > 0 && text.charAt(i - 1) == ' ') {
                    return text;
                } else {
                    if (text instanceof Spanned) {
                        SpannableString sp = new SpannableString(text + " ");
                        TextUtils.copySpansFrom((Spanned) text, 0, text.length(),
                                Object.class, sp, 0);
                        return sp;
                    } else {
                        return text + " ";
                    }
                }
            }
            return text;
        }
    };
    TextWatcher tw = new TextWatcher() {
        String tempS;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            tempS = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (tempS.length() > 1) {
                int startInt = tempS.lastIndexOf("@");
                int endInt = tempS.lastIndexOf(" ");

                if (startInt == -1) {

                } else {
                    if (endInt == -1) {
                        //TODO 검색
                        getUserTagSearch(tempS.substring(startInt, tempS.length()).replace("@", ""));
                    } else {
                        if (startInt > endInt) {
                            //TODO 검색
                            getUserTagSearch(tempS.substring(startInt, tempS.length()).replace("@", ""));
                        }
                    }
                }

            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    CommentResult m_ResultList = null;
    ArrayAdapter<CommentItem> m_ListAdapter = null;
    int lastCount = 0;

    @Override
    protected void onDestroy() {
        Intent i = new Intent();
        i.putExtra("comCount", lastCount);
        i.putExtra("postId", tempPostId);
        setResult(RESULT_OK, i);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_comment);

        tempPostId = getIntent().getExtras().getString("postId");
        try {
            lastCount = getIntent().getExtras().getInt("last");
        } catch (NullPointerException e) {
            lastCount = 0;
        }
//        isLike = getIntent().getExtras().getBoolean("isLike");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("댓글 페이지" + "_" + tempPostId);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("댓글 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        initViews();
    }

    private void getData() {

        isListLoading = true;

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);
        params.put("page", pageNavi + "");

        AppController.apiTaskNew = new APITaskNew(this, params, 321, stmCommentListResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmCommentListResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            Log.e("comment", result);
            try {
                final JSONObject j = new JSONObject(result);

//                    tvWholeLikeCount.setText();
                if (j.getInt("result") == 0) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                tvWholeLikeCount.setText(j.getInt("like") + "");
                                tvwholeCommentCount.setText(j.getInt("com") + "");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (j.getString("like_yn").equals("Y") || j.getString("like_yn").equals("y")) {
                                    imvLike.setSelected(true);
                                } else {
                                    imvLike.setSelected(false);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    pageNavi = pageNavi + 1;
                    if (!j.isNull("data")) {
                        JSONArray jarr = new JSONArray(j.getString("data"));
                        Log.e("data length", jarr.length() + "");
                        isListEnd = jarr.length() < 10;
                        CommentResult cr = new CommentResult();

                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject jd = jarr.getJSONObject(i);
                            CommentItem ci = new CommentItem();
                            ci.setId(jd.getString("id"));
                            if (jd.getString("like_yn").equals("Y") || jd.getString("like_yn").equals("y")) {
                                ci.setIsLike(true);
                            } else {
                                ci.setIsLike(false);
                            }
                            ci.setCom_id(jd.getString("reply_id"));
                            ci.setBody(jd.getString("body"));
                            ci.setLikeCnt(jd.getInt("like"));
                            ci.setEmail(jd.getString("email"));
                            ci.setTime(jd.getString("time"));
                            ci.setUrl(jd.getString("profile_picture"));
                            cr.add(0, ci);
                        }

                        updateResultList(cr);
                    } else {

                        if (pageNavi != 0) {
                            JSONArray jarr = new JSONArray(j.getString("data"));
                            Log.e("data length", jarr.length() + "");
                            isListEnd = jarr.length() < 10;
                            CommentResult cr = new CommentResult();

                            for (int i = 0; i < jarr.length(); i++) {
                                JSONObject jd = jarr.getJSONObject(i);
                                CommentItem ci = new CommentItem();
                                ci.setId(jd.getString("id"));
                                if (jd.getString("like_yn").equals("Y") || jd.getString("like_yn").equals("y")) {
                                    ci.setIsLike(true);
                                } else {
                                    ci.setIsLike(false);
                                }
                                ci.setCom_id(jd.getString("reply_id"));
                                ci.setBody(jd.getString("body"));
                                ci.setLikeCnt(jd.getInt("like"));
                                ci.setEmail(jd.getString("email"));
                                ci.setTime(jd.getString("time"));
                                ci.setUrl(jd.getString("profile_picture"));
                                cr.add(0, ci);
                            }

                            updateResultList(cr);
                        } else {
                            m_ResultList = null;
                            m_ListAdapter = null;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    list.setAdapter(m_ListAdapter);
                                }
                            });
                        }

                    }
                } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {

            } finally {
                pbGoGo.setVisibility(View.GONE);
                imvCommentGoGo.setVisibility(View.VISIBLE);
            }
        }
    };

    private void getInitData() {

        isListLoading = true;
//        m_ListAdapter = null;
        m_ResultList = null;
        pageNavi = 1;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                edtGogo.setText("");
            }
        });

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);
        params.put("page", pageNavi + "");

        AppController.apiTaskNew = new APITaskNew(this, params, 321, stmCommentListResult);
        AppController.apiTaskNew.execute();
    }

    private void commentWrite(String body) {

        getTags();

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);
        params.put("com_id", "");
        params.put("user_tags", userCollection);
        params.put("com_form", "W");
        params.put("body", body);
        params.put("url", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    StringTransMethod stmCommentStatusResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                Log.e("commentResult", result);
                JSONObject j = new JSONObject(result);
                if (j.getInt("result") == 0) {
                    getInitData();
                } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void commentDelete(String comId, int a) {

        m_ResultList.remove(a);

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);
        params.put("com_id", comId);
        params.put("com_form", "D");
        params.put("body", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    private void commentEdit(String body, String comId) {
        getTags();

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);
        params.put("com_id", comId);
        params.put("user_tags", userCollection);
        params.put("com_form", "E");
        params.put("body", body);
        params.put("url", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    private void initViews() {
        imvLike = (ImageView) findViewById(R.id.imv_comment_like);
        layGogo = (FrameLayout) findViewById(R.id.lay_comment_gogo);
        imvCommentGoGo = (ImageView) findViewById(R.id.imv_comment_gogo);
        pbGoGo = (ProgressBar) findViewById(R.id.pb_comment_gogo);

        layLike = (LinearLayout) findViewById(R.id.lay_comment_like);

        tvWholeLikeCount = (TextView) findViewById(R.id.tv_comment_top_like_cnt);
        tvwholeCommentCount = (TextView) findViewById(R.id.tv_comment_top_com_cnt);

        edtGogo = (AutoCompleteTextView) findViewById(R.id.edt_comment_gogo);

        edtGogo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (isEditing) {
                        if (edtGogo.getText().toString().equals("")) {

                        } else {
                            commentEdit(edtGogo.getText().toString(), tempComId);
                        }
                    } else {
                        if (edtGogo.getText().toString().equals("")) {

                        } else {
                            commentWrite(edtGogo.getText().toString());
                        }
                    }
                    return true;
                }
                return false;
            }
        });


//        edtGogo.setTokenizer(token);

//        linkSetting();

        edtGogo.addTextChangedListener(tw);

        list = (SwipeMenuListView) findViewById(R.id.list_comment);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                switch (menu.getViewType()) {
                    case 0:
                        menu.addMenuItem(optherItem1);

                        menu.addMenuItem(optherItem2);
                        break;
                    case 1:
                        menu.addMenuItem(optherItem3);

                        menu.addMenuItem(optherItem4);
                        break;
                }
            }
        };

        list.setMenuCreator(creator);
//        list.setCloseInterpolator(new BounceInterpolator());
//        list.setOpenInterpolator(new BounceInterpolator());

        layGogo.setOnClickListener(this);
        layLike.setOnClickListener(this);

        layImage = (LinearLayout) findViewById(R.id.lay_comment_image);
        layImage.setOnClickListener(this);

        layImageContent = (LinearLayout) findViewById(R.id.lay_comment_preview_image);

        layImageX = (LinearLayout) findViewById(R.id.lay_comment_image_x);
        layImageX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layImageContent.setVisibility(View.GONE);
                isImage = false;
            }
        });

        imvImageContent = (ImageView) findViewById(R.id.imv_comment_image_content);

        menuSetting();

        getData();

    }

    private void menuSetting() {
        optherItem1 = new SwipeMenuItem(getApplicationContext());

        optherItem1.setBackground(R.drawable.btn_onclick_comment_bg_1);
        optherItem1.setWidth(MinUtils.dp2px(60));
        optherItem1.setIcon(R.drawable.imv_comment_tag);

        optherItem2 = new SwipeMenuItem(getApplicationContext());

        optherItem2.setBackground(R.drawable.btn_onclick_comment_bg_2);
        optherItem2.setWidth(MinUtils.dp2px(60));
        optherItem2.setIcon(R.drawable.imv_comment_warning);

        optherItem3 = new SwipeMenuItem(getApplicationContext());

        optherItem3.setBackground(R.drawable.btn_onclick_comment_bg_1);
        optherItem3.setWidth(MinUtils.dp2px(60));
        optherItem3.setIcon(R.drawable.imv_comment_modify);

        optherItem4 = new SwipeMenuItem(getApplicationContext());

        optherItem4.setBackground(R.drawable.btn_onclick_comment_bg_2);
        optherItem4.setWidth(MinUtils.dp2px(60));
        optherItem4.setIcon(R.drawable.imv_comment_delete);
    }

    private void linkSetting() {
        Link linkUsername = new Link(Pattern.compile("(@\\w+)"))
                .setTextColor(0xff484848);
        List<Link> links = new ArrayList<>();
        links.add(linkUsername);

//        edtGogo.addLinks(links);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void getUserTagSearch(String str) {

        StringTransMethod stmUserTagListResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
//                            MinUtils.d("return_tag_search", result);
                        if (!j.isNull("friend")) {
                            JSONArray ja = new JSONArray(j.getString("friend"));
                            final ArrayList<TagListItem> arr = new ArrayList<>();
                            ArrayList<String> arr2 = new ArrayList<>();
                            JSONObject jd = null;
                            TagListItem tli = null;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_url(jd.getString("profile_picture"));
                                tli.setTag_name(jd.getString("id"));
                                tli.setTag_id(jd.getString("email"));

                                arr2.add(jd.getString("id"));

                                arr.add(tli);
                            }
                            final ArrayList<String> arr22 = arr2;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    edtGogo.setAdapter(new TagAdapter(CommentActivity.this, 0, arr22, 0, arr));
                                    edtGogo.showDropDown();
                                }
                            });

//                            Handler handler = new Handler() {
//                                @Override
//                                public void handleMessage(Message msg) {
//                                    edtGogo.showDropDown();
//                                }
//                            };
//
//                            handler.sendEmptyMessageDelayed(0, 500);

                            edtGogo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Log.e("dd", "dd");
                                    final String tagName = "@" + arr.get(position).getTag_name();
                                    String split = "";
                                    String edt = edtGogo.getText().toString();
                                    int last = 0;
                                    String oldEdt = "";
                                    last = edt.lastIndexOf("@");
                                    if (last != -1) {
                                        oldEdt = edt.substring(0, last - 1);
                                    }
                                    final int finalLast = last;
                                    final String finalOldEdt = oldEdt;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            edtGogo.setAdapter(null);
                                            if (finalLast != -1) {
                                                edtGogo.setText(finalOldEdt + " " + tagName + " ");
                                            } else {
                                                edtGogo.setText(tagName + " ");
                                            }
                                            edtGogo.setSelection(edtGogo.getText().toString().length());
                                            edtGogo.dismissDropDown();
                                        }
                                    });
                                }
                            });

                        }
                    } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> parmas = new HashMap<>();

        parmas.put("my_id", AppController.getSp().getString("email", ""));
        parmas.put("str", str);
        parmas.put("page", "1");

        AppController.apiTaskNew = new APITaskNew(this, parmas, 512, stmUserTagListResult);
        AppController.apiTaskNew.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_comment_gogo:
                if (pbGoGo.getVisibility() == View.GONE) {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Comment GoGo Click").build());
                    if (isEditing) {
                        if (edtGogo.getText().toString().equals("")) {

                        } else {
                            pbGoGo.setVisibility(View.VISIBLE);
                            imvCommentGoGo.setVisibility(View.GONE);
                            commentEdit(edtGogo.getText().toString(), tempComId);
                        }
                    } else {
                        if (edtGogo.getText().toString().equals("")) {

                        } else {
                            pbGoGo.setVisibility(View.VISIBLE);
                            imvCommentGoGo.setVisibility(View.GONE);
                            commentWrite(edtGogo.getText().toString());
                        }
                    }
                }
                break;
            case R.id.lay_comment_like:
                if (imvLike.isSelected()) {

                    StringTransMethod stmLikeResult = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getInt("result") == 0) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imvLike.setSelected(false);
                                            tvWholeLikeCount.setText((Integer.parseInt(tvWholeLikeCount.getText().toString()) - 1) + "");
                                        }
                                    });
                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };

                    Map<String, String> params1 = new HashMap<>();

                    params1.put("post_reply_id", tempPostId);
                    params1.put("click_id", AppController.getSp().getString("email", ""));
                    params1.put("like_form", "P");

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, params1, 303, stmLikeResult);
                    AppController.apiDataTaskNew.execute();

                } else {
                    StringTransMethod stmUnLikeResult = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd2 = new JSONObject(result);
                                if (jd2.getInt("result") == 0) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imvLike.setSelected(true);
                                            tvWholeLikeCount.setText((Integer.parseInt(tvWholeLikeCount.getText().toString()) + 1) + "");
                                        }
                                    });
                                } else if (jd2.getInt("result") == 100 || jd2.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    Map<String, String> params2 = new HashMap<>();

                    params2.put("post_reply_id", tempPostId);
                    params2.put("click_id", AppController.getSp().getString("email", ""));
                    params2.put("like_form", "P");

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, params2, 302, stmUnLikeResult);
                    AppController.apiDataTaskNew.execute();

                }
                break;
            case R.id.lay_comment_image:
                //TODO 이미지 로드
//                Intent albumIntent = new Intent(Intent.ACTION_PICK);
//                albumIntent.setType("image/png");

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
                String type = "image/gif";
                intent.setDataAndType(data, type);

                startActivityForResult(intent, PICK_FROM_ALBUM);
                break;
        }
    }

    protected boolean updateResultList(CommentResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(0, resultList);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new CommentAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                list.setAdapter(m_ListAdapter);
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //TODO 터치했을때 해동

                    }
                });

            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0 && view.getChildAt(0) != null && view.getChildAt(0).getTop() == 0) {
                    // TODO 다음 로딩
                    if (!isListEnd && !isListLoading) {
                        getData();
                    }
                }
            }
        });


        list.getFirstVisiblePosition();

        list.setSelection(list.getCount());

        isListLoading = false;
        return true;
    }

    private void getTags() {
        userCollection = "";
        String commentsText = edtGogo.getText().toString();

        calloutSpans = MinUtils.getSpans(commentsText, '@');

        for (int i = 0; i < calloutSpans.size(); i++) {
            int[] span = calloutSpans.get(i);
            int calloutStart = span[0];
            int calloutEnd = span[1];

            userCollection = userCollection + commentsText.substring(calloutStart + 1, calloutEnd) + ",";

        }

//        if (userCollection.length() > 1) {
//            userCollection = userCollection.substring(0, userCollection.length() - 1);
//        }

        for(int i=0;i<calloutSpans.size(); i++){
            Log.e("1", calloutSpans.get(i)[0]+"");
            Log.e("1", calloutSpans.get(i)[1]+"");
        }

//        Log.e("user Tags", userCollection);
    }

    //    class NewTagAdapter implements ListAdapter, Filterable {
//
//
//        public int navi = 0;
//        private DataSetObservable mDataSetObservable = new DataSetObservable();
//        private LayoutInflater mInflater;
//        private List<TagListItem> mFilteredContactList;
//        private List<TagListItem> mContactList;
//        private Filter mFilter;
//
//        public NewTagAdapter(Context context, List<TagListItem> contactList, int inNavi) {
//            navi = inNavi;
//            init(context, contactList);
//        }
//
//        private void init(Context context, List<TagListItem> contactList) {
//            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            mFilteredContactList = mContactList = contactList;
//        }
//
//        @Override
//        public boolean areAllItemsEnabled() {
//            return true;
//        }
//
//        @Override
//        public boolean isEnabled(int position) {
//            return true;
//        }
//
//        @Override
//        public void registerDataSetObserver(DataSetObserver observer) {
//            mDataSetObservable.registerObserver(observer);
//        }
//
//        @Override
//        public void unregisterDataSetObserver(DataSetObserver observer) {
//            mDataSetObservable.unregisterObserver(observer);
//        }
//
//        @Override
//        public int getCount() {
//            return mFilteredContactList.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return mFilteredContactList.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public boolean hasStableIds() {
//            return false;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            Holder holder;
//            if (convertView == null) {
//                convertView = mInflater.inflate(R.layout.row_tag_list, null);
//
//                holder = new Holder();
//                holder.imv = (CircleImageView) convertView.findViewById(R.id.cimv_tag_list_row);
//                holder.tv = (TextView) convertView.findViewById(R.id.tv_tag_list_row);
//                holder.fimv = (FixedImageView) convertView.findViewById(R.id.imv_tag_list_row);
//                holder.lay = (FrameLayout) convertView.findViewById(R.id.lay_tag_list_row);
//
//                convertView.setTag(holder);
//            } else {
//                holder = (Holder) convertView.getTag();
//            }
//
//            TagListItem model = mContactList.get(position);
//
//            switch (navi) {
//                case 0:
//                    holder.lay.setVisibility(View.GONE);
//                    holder.fimv.setVisibility(View.GONE);
//                    holder.imv.setVisibility(View.GONE);
//                    break;
//                case 1:
//                    holder.lay.setVisibility(View.VISIBLE);
//                    holder.fimv.setVisibility(View.GONE);
//                    holder.imv.setVisibility(View.VISIBLE);
//                    holder.imv.setImageURLString(AppController.URL + model.getTag_url());
//                    break;
//                case 2:
//                    holder.lay.setVisibility(View.VISIBLE);
//                    holder.fimv.setVisibility(View.VISIBLE);
//                    String tempUrl = model.getTag_url();
//
//                    int lastIdx = tempUrl.lastIndexOf("_");
//
//                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//                    holder.fimv.setImageURLString(AppController.URL + model.getTag_url(), AppController.URL + tempName);
//                    holder.imv.setVisibility(View.GONE);
//                    holder.fimv.setTag(model.getTag_body());
//                    break;
//            }
//
//            holder.tv.setText(model.getTag_name());
//            holder.tv.setTag(model.getTag_id());
//
//            return convertView;
//        }
//
//        @Override
//        public int getItemViewType(int position) {
//            return 0;
//        }
//
//        @Override
//        public int getViewTypeCount() {
//            return 1;
//        }
//
//        @Override
//        public boolean isEmpty() {
//            return getCount() == 0;
//        }
//
//        @Override
//        public Filter getFilter() {
//            if (mFilter == null) {
//                mFilter = new TagFilter();
//            }
//            return mFilter;
//        }
//
//        public void notifyDataSetChanged() {
//            mDataSetObservable.notifyChanged();
//        }
//
//        public void notifyDataSetInvalidated() {
//            mDataSetObservable.notifyInvalidated();
//        }
//
//        private class Holder {
//            public CircleImageView imv;
//            public TextView tv;
//            public FixedImageView fimv;
//            public FrameLayout lay;
//        }
//
//        private class TagFilter extends Filter {
//
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                FilterResults filterResults = new FilterResults();
//                if (constraint == null || constraint.length() == 0) {
//                    filterResults.values = mContactList;
//                    filterResults.count = mContactList.size();
//                } else {
//                    final String lastToken = constraint.toString().toLowerCase();
//                    final int count = mContactList.size();
//                    final List<TagListItem> list = new ArrayList<>();
//                    TagListItem tagListItem;
//
//                    for (int i = 0; i < count; i++) {
//                        tagListItem = mContactList.get(i);
//                        if (tagListItem.getTag_name().toLowerCase().contains(lastToken)
//                                || tagListItem.getTag_id().toLowerCase().startsWith(lastToken)) {
//                            list.add(tagListItem);
//                        }
//                    }
//
//                    filterResults.values = list;
//                    filterResults.count = list.size();
//                }
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//                mFilteredContactList = (List<TagListItem>) results.values;
//                if (results.count > 0) {
//                    notifyDataSetChanged();
//                } else {
//                    notifyDataSetInvalidated();
//                }
//            }
//        }
//
//    }
    class TagAdapter extends ArrayAdapter<String> {

        int typeNavi = 0;
        ArrayList<TagListItem> arr;
        LayoutInflater m_LayoutInflater = null;

        public TagAdapter(Context context, int resource, List<String> objects, int navi, ArrayList<TagListItem> arrayList) {
            super(context, resource, objects);
            typeNavi = navi;
            arr = arrayList;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position) + " ";
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            View v = convertView;

            final TagListHolder holder;

            if (v == null) {
                if (m_LayoutInflater == null)
                    m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                v = m_LayoutInflater.inflate(R.layout.row_tag_list, null);
                holder = new TagListHolder();

                holder.cimv = (CircleImageView) v.findViewById(R.id.cimv_tag_list_row);

                holder.tvTag = (TextView) v.findViewById(R.id.tv_tag_list_row);

                v.setTag(holder);

            } else {
                holder = (TagListHolder) v.getTag();
            }

            final String model = getItem(pos);
            //TODO 타임라인 데이터 insert

            holder.cimv.setVisibility(View.VISIBLE);
            holder.cimv.setImageURLString(AppController.URL + arr.get(pos).getTag_url());
            holder.tvTag.setText("@" + model + " ");


            return v;
        }

        class TagListHolder {
            TextView tvTag;
            CircleImageView cimv;
        }


    }

    class CommentAdapter extends ArrayAdapter<CommentItem> {
        LayoutInflater m_LayoutInflater = null;
        CommentResult result;

        public CommentAdapter(Context ctx, int txtViewId, List<CommentItem> modles) {
            super(ctx, txtViewId, modles);
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.result = (CommentResult) modles;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            if (getItem(position).getEmail().equals(AppController.getSp().getString("email", ""))) {
                return 1;
            } else {
                return 0;
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

//            if (viewArray != null && viewArray.get(position) != null) {
//                convertView = viewArray.get(position).get();
//                if (convertView != null)
//                    return convertView;
//            }

            convertView = m_LayoutInflater.inflate(R.layout.row_comment, null);

            final CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_comment_row);

            TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_comment_row_id);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tv_comment_row_time);
            final TextView tvLike = (TextView) convertView.findViewById(R.id.tv_comment_row_like);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tv_comment_row_body);
            final TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_comment_row_like_cnt);
            LinearLayout layMain = (LinearLayout) convertView.findViewById(R.id.lay_comment_row_main);

            final LinearLayout layLikeCnt = (LinearLayout) convertView.findViewById(R.id.lay_comment_row_like_cnt);

//                    v.setTag(holder);
//
//                } else {
//                    holder = (CommentHolder) v.getTag();
//                }

            final CommentItem model = getItem(pos);
            //TODO 타임라인 데이터 insert

            layMain.setLayoutParams(new LinearLayout.LayoutParams(metrics.widthPixels, LinearLayout.LayoutParams.WRAP_CONTENT));

            String tempTime = "";

            String tempDate = model.getTime();
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String tempC = sdf.format(now);

            ArrayList<String> nows = new ArrayList<>();
            ArrayList<String> posts = new ArrayList<>();
//                Log.e("aa", tempDate.substring(0,2));
//                Log.e("aa", tempDate.substring(2,4));

            for (int i = 0; i < tempDate.length(); i = i + 2) {
                nows.add(tempC.substring(i, i + 2));
                posts.add(tempDate.substring(i, i + 2));
            }

//                for (int i = 0; i < nows.size(); i++) {
//                    Log.e("now" + i, nows.get(i));
//                    Log.e("post" + i, posts.get(i));
//                }

            int tempNavi = 0;

            for (int i = 0; i < nows.size(); i++) {
                if (!nows.get(i).equals(posts.get(i))) {
                    tempNavi = i;
                    break;
                }
            }

            int tempNow = Integer.parseInt(nows.get(tempNavi));
            int tempPosts = Integer.parseInt(posts.get(tempNavi));

            switch (tempNavi) {
                case 0:

                    break;
                case 1:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_year);
                    } else {
                        tempTime = tempPosts - tempNow + getResources().getString(R.string.timeline_before_year);
                    }
                    break;
                case 2:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_month);
                    } else {
                        tempTime = (12 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_month);
                    }
                    break;
                case 3:
                    Calendar c = Calendar.getInstance();
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_day);
                    } else {
                        c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
                        int lastDay = c.getActualMaximum(Calendar.DATE);
                        tempTime = (lastDay - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_day);
                    }
                    break;
                case 4:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_hour);
                    } else {
                        tempTime = (24 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_hour);
                    }
                    break;
                case 5:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_minute);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_minute);
                    }
                    break;
                case 6:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_second);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_second);
                    }
                    break;
            }

            cimvPro.setImageURLString(AppController.URL + model.getUrl());
            cimvPro.setTag(model.getEmail());

            tvUserId.setText(model.getId());
            tvBody.setText(model.getBody());
            tvTime.setText(tempTime);
            int likeCount = model.getLikeCnt();

            if (model.isLike()) {
                m_ResultList.get(pos).setIsLike(true);
                tvLikeCnt.setVisibility(View.VISIBLE);
                tvLike.setText(getResources().getString(R.string.like_cancel));
                layLikeCnt.setVisibility(View.VISIBLE);
            } else {
                tvLike.setText(getResources().getString(R.string.like));
                m_ResultList.get(pos).setIsLike(false);
                if (likeCount == 0) {
                    tvLikeCnt.setVisibility(View.GONE);
                    layLikeCnt.setVisibility(View.GONE);
                } else {
                    tvLikeCnt.setVisibility(View.VISIBLE);
                    layLikeCnt.setVisibility(View.VISIBLE);
                }
            }

            tvLikeCnt.setText(likeCount + "");

            String commentsText = tvBody.getText().toString();

            ArrayList<int[]> calloutSpans = MinUtils.getSpans(commentsText, '@');

            SpannableString commentsContent = new SpannableString(commentsText);

            for (int i = 0; i < calloutSpans.size(); i++) {
                int[] span = calloutSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];

                commentsContent.setSpan(new CallTag(getContext()),
                        calloutStart,
                        calloutEnd, 0);

            }

            tvBody.setMovementMethod(LinkMovementMethod.getInstance());
            tvBody.setText(commentsContent);
            tvBody.setHighlightColor(getResources().getColor(android.R.color.transparent));

            final TextView tempLike = tvLike;
            final LinearLayout tempLikeCnt = layLikeCnt;

//                layBtn1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //TODO 태그
//                        edtGogo.setAdapter(null);
//                        if (isEditing) {
//                            edtGogo.setText("");
//                        }
//                        isEditing = false;
//                        if (edtGogo.getText().toString().equals("")) {
//                            edtGogo.setText("@" + model.getId());
//                        } else {
//                            edtGogo.setText(edtGogo.getText().toString() + " @" + model.getId());
//                        }
//
//                        edtGogo.setAdapter(adapter);
//                    }
//                });
//                layBtn2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //TODO 신고하기
//                        isEditing = false;
//                        if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
//
//                        } else {
//                            Intent i = new Intent(CommentActivity.this, ReportActivity.class);
//                            i.putExtra("postId", model.getCom_id());
//                            i.putExtra("pc", "C");
//                            startActivity(i);
//                        }
//                    }
//                });
//                layBtn3.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //TODO 수정하기
//                        isEditing = true;
//
//                        edtGogo.setAdapter(null);
//
//                        edtGogo.setText("");
//
//                        tempComId = model.getCom_id();
//                        edtGogo.setText(model.getBody());
//
//
//                        edtGogo.setAdapter(adapter);
//                    }
//                });
//                layBtn4.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        //TODO 삭제하기
//                        isEditing = false;
//                        commentDelete(model.getCom_id(), pos);
//                    }
//                });


            list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (menu.getViewType()) {
                        case 0:
                            if (index == 0) {
                                //TODO 태그
                                edtGogo.setAdapter(null);
                                if (isEditing) {
                                    edtGogo.setText("");
                                }
                                isEditing = false;
                                if (edtGogo.getText().toString().equals("")) {
                                    edtGogo.setText("@" + result.get(position).getId());
                                    edtGogo.setSelection(edtGogo.getText().toString().length());
                                } else {
                                    edtGogo.setText(edtGogo.getText().toString() + " @" + result.get(position).getId());
                                    edtGogo.setSelection(edtGogo.getText().toString().length());
                                }

                                edtGogo.setAdapter(adapter);
                            } else {
                                //TODO 신고
                                isEditing = false;
                                if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {

                                } else {
                                    Intent i = new Intent(CommentActivity.this, ReportActivity.class);
                                    i.putExtra("postId", result.get(position).getCom_id());
                                    i.putExtra("pc", "C");
                                    startActivity(i);
                                }
                            }
                            break;
                        case 1:
                            if (index == 0) {
                                //TODO 수정하기
                                isEditing = true;

                                edtGogo.setAdapter(null);

                                edtGogo.setText("");

                                tempComId = result.get(position).getCom_id();
                                edtGogo.setText(result.get(position).getBody());
                                edtGogo.setSelection(edtGogo.getText().toString().length());

                                edtGogo.setAdapter(adapter);
                            } else {
                                //TODO 삭제하기
                                isEditing = false;
                                commentDelete(result.get(position).getCom_id(), pos);
                            }
                            break;
                    }
                    return false;
                }
            });

            tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO 좋아요 누름
                    if (!model.isLike()) {
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                m_ResultList.get(pos).setIsLike(true);
                                                tvLike.setText(getResources().getString(R.string.like_cancel));
                                                tvLikeCnt.setText((model.getLikeCnt() + 1) + "");
                                                m_ResultList.get(pos).setLikeCnt(model.getLikeCnt() + 1);
                                                layLikeCnt.setVisibility(View.VISIBLE);
                                                tvLikeCnt.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };

                        Map<String, String> params2 = new HashMap<>();

                        params2.put("post_reply_id", model.getCom_id());
                        params2.put("click_id", AppController.getSp().getString("email", ""));
                        params2.put("like_form", "R");

                        AppController.apiDataTaskNew = new APIDataTaskNew(CommentActivity.this, params2, 302, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                m_ResultList.get(pos).setIsLike(false);
                                                tvLike.setText(getResources().getString(R.string.like));
                                                tvLikeCnt.setText((model.getLikeCnt() - 1) + "");
                                                m_ResultList.get(pos).setLikeCnt(model.getLikeCnt() - 1);
                                                if (model.getLikeCnt() == 0) {
                                                    layLikeCnt.setVisibility(View.GONE);
                                                    tvLikeCnt.setVisibility(View.GONE);
                                                }
                                            }
                                        });

                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };

                        Map<String, String> params1 = new HashMap<>();

                        params1.put("post_reply_id", model.getCom_id());
                        params1.put("click_id", AppController.getSp().getString("email", ""));
                        params1.put("like_form", "R");

                        AppController.apiDataTaskNew = new APIDataTaskNew(CommentActivity.this, params1, 303, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                }
            });
            tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO 좋아요 누른 사람들 목록
//                        startActivity();
                    Intent i = new Intent(CommentActivity.this, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_COMMENT_LIKE);
                    i.putExtra("postId", model.getCom_id());
                    startActivity(i);
                }
            });

//                sv.setOnScrollListener(new ObservableHorizontalScrollView.OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldX, int oldY) {
//                        Log.e("sv onscroll", "" + x + "/" + y + "/" + oldX + "/" + oldY);
//                        if (oldX > x) {
////                            int endX = oldX - x;
////                            int endX = x - oldX;
////                            int endY = oldY - y;
//                            if (oldX > x) {
//                                Log.e("scroll count", pos + "");
//                                Log.e("list count", list.getCount() + "/" + list.getChildCount());
//                                for (int i = 0; i < list.getCount(); i++) {
//                                    if (i != pos) {
////                                        ObservableHorizontalScrollView sv = (ObservableHorizontalScrollView) view.findViewById(R.id.hsv_comment_row);
////                                        ObservableHorizontalScrollView sv = (ObservableHorizontalScrollView) list.getChildAt(i).findViewById(R.id.hsv_comment_row);
////                                        sv.fullScroll(ScrollView.FOCUS_LEFT);
////                                        sv.scrollTo(0, 0);
////                                        sv.scroll
//                                    } else {
//
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onEndScroll(ObservableHorizontalScrollView scrollView) {
//
//                    }
//                });

            cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tempEmail = cimvPro.getTag().toString();
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(CommentActivity.this, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        startActivity(i);
                    }
                }
            });

            tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tempEmail = cimvPro.getTag().toString();
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(CommentActivity.this, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        startActivity(i);
                    }
                }
            });

            return convertView;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FROM_ALBUM) {
            if (resultCode == RESULT_OK) {
                Uri mImageCaptureUri = data.getData();

                Log.e("data", mImageCaptureUri.toString());

//                layImage.setVisibility(View.VISIBLE);

                try {
                    GifDrawable gd = new GifDrawable(getContentResolver(), mImageCaptureUri);

                    imvImageContent.setImageDrawable(gd);
                    layImageContent.setVisibility(View.VISIBLE);
                    isImage = true;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}