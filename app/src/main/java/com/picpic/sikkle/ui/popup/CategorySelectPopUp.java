/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.picpic.sikkle.R;

import java.util.ArrayList;
import java.util.List;

public class CategorySelectPopUp extends Activity implements View.OnClickListener {

    LinearLayout layX;
    ListView list;
    ArrayList<String> cates = new ArrayList<>();
    ArrayList<CategoryItem> arr = new ArrayList<>();
    String str;
    CategoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_category_select_pop_up);

        try {
            str = getIntent().getExtras().getString("txt");
        }catch (NullPointerException e){

        }

        layX = (LinearLayout) findViewById(R.id.lay_category_select_v1_x);
        layX.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list_category_select);

//        cates.add(getResources().getString(R.string.category_0));
//        cates.add(getResources().getString(R.string.category_2));
//        cates.add(getResources().getString(R.string.category_3));
//        cates.add(getResources().getString(R.string.category_4));
//        cates.add(getResources().getString(R.string.category_5));
//        cates.add(getResources().getString(R.string.category_6));
//        cates.add(getResources().getString(R.string.category_7));
//        cates.add(getResources().getString(R.string.category_8));
//        cates.add(getResources().getString(R.string.category_9));
//        cates.add(getResources().getString(R.string.category_10));
//        cates.add(getResources().getString(R.string.category_11));
//        cates.add(getResources().getString(R.string.category_12));
//        cates.add(getResources().getString(R.string.category_13));
//        cates.add(getResources().getString(R.string.category_14));
//        cates.add(getResources().getString(R.string.category_15));
//        cates.add(getResources().getString(R.string.category_16));
//        cates.add(getResources().getString(R.string.category_17));
//        cates.add(getResources().getString(R.string.category_18));
//        cates.add(getResources().getString(R.string.category_19));
//        cates.add(getResources().getString(R.string.category_20));
//        cates.add(getResources().getString(R.string.category_21));
//        cates.add(getResources().getString(R.string.category_22));
//        cates.add(getResources().getString(R.string.category_23));
//        cates.add(getResources().getString(R.string.category_24));
//        cates.add(getResources().getString(R.string.category_25));

        cates.add(getResources().getString(R.string.cate_0));
        cates.add(getResources().getString(R.string.cate_1));
        cates.add(getResources().getString(R.string.cate_2));
        cates.add(getResources().getString(R.string.cate_3));
        cates.add(getResources().getString(R.string.cate_4));
        cates.add(getResources().getString(R.string.cate_5));
        cates.add(getResources().getString(R.string.cate_6));
        cates.add(getResources().getString(R.string.cate_7));
        cates.add(getResources().getString(R.string.cate_8));
        cates.add(getResources().getString(R.string.cate_9));
        cates.add(getResources().getString(R.string.cate_10));
        cates.add(getResources().getString(R.string.cate_11));
        cates.add(getResources().getString(R.string.cate_12));
        cates.add(getResources().getString(R.string.cate_13));
        cates.add(getResources().getString(R.string.cate_14));
        cates.add(getResources().getString(R.string.cate_15));
        cates.add(getResources().getString(R.string.cate_16));
        cates.add(getResources().getString(R.string.cate_17));
        cates.add(getResources().getString(R.string.cate_18));
        cates.add(getResources().getString(R.string.cate_19));

//        arr.add
        for (int i = 0; i < cates.size(); i++) {
            CategoryItem ci = new CategoryItem();
            ci.setStr(cates.get(i));
            arr.add(ci);
        }

        adapter = new CategoryAdapter(this, 0, arr);

        list.setAdapter(adapter);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                for (int i = 0; i < cates.size(); i++) {
                    if (cates.get(i).equals(str)) {
                        arr.get(i).setIs(true);
                        adapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
        };

        handler.sendEmptyMessageDelayed(0, 500);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                Intent i = new Intent();
                i.putExtra("category", cates.get(position));
                setResult(RESULT_OK, i);
                finish();
                overridePendingTransition(0, 0);
            }
        });

    }

    public static class ViewHolder {
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {

                viewHolder = new SparseArray<View>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }

            return (T) childView;
        }
    }

    class CategoryAdapter extends ArrayAdapter<CategoryItem> {

        public CategoryAdapter(Context context, int resource, List<CategoryItem> objects) {
            super(context, resource, objects);
        }

        LayoutInflater m_LayoutInflater;

        @Override
        public int getCount() {
            return arr.size();
        }

        @Override
        public CategoryItem getItem(int position) {
            return arr.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (m_LayoutInflater == null) {
                m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            if (convertView == null) {
                convertView = m_LayoutInflater.inflate(R.layout.row_category_select, null);
            }

            TextView tv = ViewHolder.get(convertView, R.id.tv_category_row);

            CategoryItem model = getItem(position);

            tv.setText(model.getStr());

            if (model.is) {
//                convertView.setSelected(true);
                tv.setTextColor(0xff697fff);
            } else {
//                convertView.setSelected(false);
                tv.setTextColor(Color.BLACK);
            }

            //tv_category_row

            return convertView;
        }
    }

    class CategoryItem {
        String str = "";
        boolean is = false;

        public String getStr() {
            return str;
        }

        public void setStr(String str) {
            this.str = str;
        }

        public boolean is() {
            return is;
        }

        public void setIs(boolean is) {
            this.is = is;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setResult(RESULT_CANCELED);
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_category_select_v1_x:
                setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }
}
