package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPopUpActivity extends Activity implements View.OnClickListener {

    TextView tvMsg, tvLeft, tvRight;
    EditText edt;
    EditTextCheckIcon etci;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot_pop_up);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("잊어버렸어 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        tvMsg = (TextView) findViewById(R.id.tv_forgot_msg);
        tvLeft = (TextView) findViewById(R.id.tv_forgot_left);
        tvRight = (TextView) findViewById(R.id.tv_forgot_right);

        tvLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);

        etci = (EditTextCheckIcon) findViewById(R.id.etci_forgot);
        etci.setOnClickListener(this);

        edt = (EditText) findViewById(R.id.edt_forgot);
        edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etci.setState(EditTextCheckIcon.CHOICE_X);
                } else {
                    etci.setState(EditTextCheckIcon.CHOICE_NON);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etci_forgot:
                edt.setText("");
                break;
            case R.id.tv_forgot_left:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.tv_forgot_right:
                if (edt.getText().toString().equals("")) {
                    tvMsg.setVisibility(View.VISIBLE);
                    tvMsg.setText(getResources().getString(R.string.popup_empty_email));
                } else {

                    StringTransMethod stmForgot = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getInt("result") == 0) {
                                    if (jd.getString("data").equals("1")) {

                                        StringTransMethod stmForot = new StringTransMethod() {
                                            @Override
                                            public void endTrans(String result) {
                                                try {
                                                    JSONObject jd2 = new JSONObject(result);
                                                    if (jd2.getInt("result") == 0) {
                                                        startActivity(new Intent(ForgotPopUpActivity.this, ForgotConfirmActivity.class));
                                                        finish();
                                                        overridePendingTransition(0, 0);
                                                    }else if (jd2.getInt("result") == 100 || jd2.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {

                                                }
                                            }
                                        };

                                        Map<String, String> parmas2 = new HashMap<>();

                                        parmas2.put("my_id", edt.getText().toString());

                                        AppController.apiDataTaskNew = new APIDataTaskNew(ForgotPopUpActivity.this, parmas2, 220, stmForot);
                                        AppController.apiDataTaskNew.execute();

                                    } else {
                                        tvMsg.setVisibility(View.VISIBLE);
                                        tvMsg.setText(getResources().getString(R.string.popup_not_join));
                                    }
                                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                } else {
                                    tvMsg.setVisibility(View.VISIBLE);
                                    tvMsg.setText(getResources().getString(R.string.popup_not_join));
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };

                    Map<String, String> parmas = new HashMap<>();

                    parmas.put("email", edt.getText().toString());

                    AppController.apiTaskNew = new APITaskNew(this, parmas, 213, stmForgot);
                    AppController.apiTaskNew.execute();

                }
                break;
        }
    }
}
