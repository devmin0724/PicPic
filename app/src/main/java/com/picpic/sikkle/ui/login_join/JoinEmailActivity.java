package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.EditTextCheckIcon;
import com.picpic.sikkle.widget.Term1Span;
import com.picpic.sikkle.widget.Term2Span;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JoinEmailActivity extends Activity implements View.OnClickListener, View.OnFocusChangeListener {

    public static Activity ac;
    EditText edt;
    EditTextCheckIcon etci;
    Button btn;
    TextView tv;
    LinearLayout lay;
    TextWatcher emailTW = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (s.length() > 0) {
                if (s.charAt(s.length() - 1) == ' ') {
                    edt.setText(s.toString().replace(" ", "_"));
                    edt.setSelection(edt.length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join_email);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 이메일 입력 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        initViews();
    }

    private void initViews() {
        edt = (EditText) findViewById(R.id.edt_join_email);
        edt.addTextChangedListener(emailTW);
        edt.setOnFocusChangeListener(this);

        etci = (EditTextCheckIcon) findViewById(R.id.etci_join_email);
        etci.setOnClickListener(this);

        btn = (Button) findViewById(R.id.btn_join_email_next);
        btn.setOnClickListener(this);

        lay = (LinearLayout) findViewById(R.id.lay_join_email_bottom);
        lay.setOnClickListener(this);

        tv = (TextView) findViewById(R.id.tv_join_email_bottom);

        String commentsText = tv.getText().toString();

        int[] term1 = new int[2];
        int[] term2 = new int[2];

        String tempTerm1 = getResources().getString(R.string.join_term1);
        String tempTerm2 = getResources().getString(R.string.join_term2);
        term1[0] = commentsText.lastIndexOf(tempTerm1);
        term1[1] = term1[0] + tempTerm1.length();
        term2[0] = commentsText.lastIndexOf(tempTerm2);
        term2[1] = term2[0] + tempTerm2.length();

        SpannableString commentsContent = new SpannableString(commentsText);

        commentsContent.setSpan(new Term1Span(JoinEmailActivity.this), term1[0], term1[1], 0);
        commentsContent.setSpan(new Term2Span(JoinEmailActivity.this), term2[0], term2[1], 0);

        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(commentsContent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_join_email_next:
                String temp1 = edt.getText().toString();
                if (!temp1.equals("")) {
                    //TODO
                    if (!checkEmail(temp1)) {
                        edt.startAnimation(AppController.shake);
                        etci.setState(EditTextCheckIcon.CHOICE_WRONG);
                        edt.setText("");
                        edt.setHint(R.string.join_hint_email_pattern);
                    } else {
                        StringTransMethod stmEmailCheck = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getString("data").equals("0") || jd.getString("data").equals("2")) {
                                        Intent i = new Intent(JoinEmailActivity.this, JoinPassWordActivity.class);

                                        i.putExtra("email", edt.getText().toString());

                                        startActivity(i);
                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    } else {
                                        edt.startAnimation(AppController.shake);
                                        etci.setState(EditTextCheckIcon.CHOICE_WRONG);
                                        edt.setText("");
                                        edt.setHint(R.string.join_hint_email_already);
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("email", temp1);

                        AppController.apiTaskNew = new APITaskNew(this, params, 213, stmEmailCheck);
                        AppController.apiTaskNew.execute();

                    }
                } else {
                    edt.startAnimation(AppController.shake);
                    etci.setState(EditTextCheckIcon.CHOICE_WRONG);
                    edt.setText("");
                    edt.setHint(R.string.join_hint_email_empty);
                }
                break;
            case R.id.etci_join_email:
                edt.setText("");
                break;
            case R.id.lay_join_email_bottom:

                break;
        }
    }

    public boolean checkEmail(String email) {

        if (email.length() > 45) {
            return false;
        }

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
//        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();

        return isNormal;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            etci.setState(EditTextCheckIcon.CHOICE_X);
        } else {
            etci.setState(EditTextCheckIcon.CHOICE_NON);
        }
    }
}
