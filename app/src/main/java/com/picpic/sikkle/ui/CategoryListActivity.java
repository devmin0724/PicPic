package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.NativeAd;
import com.google.android.gms.analytics.HitBuilders;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.video_list_demo.adapter.VideoRecyclerViewAdapter;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.BaseVideoItem;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.ItemFactory;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.TimelineVideoItem;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.TimelineVideoItem2;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class CategoryListActivity extends Activity implements View.OnClickListener {

    public static final int CATEGORY_RETURN = 1111;

    LinearLayout layHome, layProgress;
//    ListView list;

    TextView tv;
    int titles[] = {
            R.string.cate_1, R.string.cate_2, R.string.cate_3, R.string.cate_4,
            R.string.cate_5, R.string.cate_6, R.string.cate_7, R.string.cate_8,
            R.string.cate_9, R.string.cate_10, R.string.cate_11, R.string.cate_12,
            R.string.cate_13, R.string.cate_14, R.string.cate_15, R.string.cate_16,
            R.string.cate_17, R.string.cate_18, R.string.cate_19
    };

    int pageNavi = 0;

    public Activity ac;
    private RecyclerView rv;
    private LinearLayoutManager lm;

    private int listNavi = 0;
    boolean is = false, isStop = false, isFirst = true;
    private NativeAd nativeAd;
    private LinearLayout layNextLoading;

    private int nowPage = 1;
    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();

    Timer scrollTimer;
    TimerTask scrollTimerTask;

    /**
     * Only the one (most visible) view should be active (and playing).
     * To calculate visibility of views we use {@link SingleListViewItemActiveCalculator}
     */
    private final ListItemsVisibilityCalculator mListItemVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    private VideoRecyclerViewAdapter videoRecyclerViewAdapter;
//    private VideoListViewAdapter videoRecyclerViewAdapter;

    /**
     * ItemsPositionGetter is used by {@link ListItemsVisibilityCalculator} for getting information about
     * items position in the RecyclerView and LayoutManager
     */
    private ItemsPositionGetter mItemsPositionGetter;

    /**
     * Here we use {@link SingleVideoPlayerManager}, which means that only one video playback is possible.
     */
    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {
        }
    });

    private int mScrollState = RecyclerView.SCROLL_STATE_IDLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_category_list);

        tv = (TextView) findViewById(R.id.tv_category_list_title);
        layHome = (LinearLayout) findViewById(R.id.imv_category_list_home);
        layHome.setOnClickListener(this);
//        laySearch = (LinearLayout) findViewById(R.id.imv_category_list_search);
//        laySearch.setOnClickListener(this);

        layProgress = (LinearLayout) findViewById(R.id.lay_category_list_progress);
        layNextLoading = (LinearLayout) findViewById(R.id.lay_category_next_loading);

//        list = (StaggeredGridView) findViewById(R.id.grid_view_category_list);
//        list = (ListView) findViewById(R.id.grid_view_category_list);
        rv = (RecyclerView) findViewById(R.id.grid_view_category_list);

        pageNavi = getIntent().getExtras().getInt("pageNavi");

        tv.setText(getResources().getString(titles[pageNavi]));

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 카테고리 리스트 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

//        try {
//            mList.add(ItemFactory.createItemFromAsset("blank.mp4", R.drawable.non_p, this, mVideoPlayerManager));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
//        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);

        ac = this;
        AppController.tagNavi = 0;

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 인기기피드 페지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layProgress.setVisibility(View.VISIBLE);

//        try {
//            listNavi = getIntent().getExtras().getInt("listNavi");
//            switch (listNavi) {
//                case 0:
//                    lay1.setSelected(true);
//                    break;
//                case 1:
//                    lay2.setSelected(true);
//                    break;
//                case 2:
//                    lay3.setSelected(true);
//                    break;
//            }
//        } catch (NullPointerException e) {
//            listNavi = 0;
//            lay1.setSelected(true);
//        }

        Log.e("listNavi 1", listNavi + "");

        videoRecyclerViewAdapter = new VideoRecyclerViewAdapter(mVideoPlayerManager, this, mList);

        lm = new LinearLayoutManager(this);
        rv.setLayoutManager(lm);
//        rv.setAdapter(footerAdapter.wrap((IAdapter) videoRecyclerViewAdapter));
        rv.setAdapter(videoRecyclerViewAdapter);
        mItemsPositionGetter = new RecyclerViewItemPositionGetter(lm, rv);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
                mScrollState = newState;

                if (newState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {
                    try {
                        mListItemVisibilityCalculator.onScrollStateIdle(
                                mItemsPositionGetter,
                                lm.findFirstVisibleItemPosition(),
                                lm.findLastVisibleItemPosition());
                    } catch (IndexOutOfBoundsException e) {

                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!mList.isEmpty()) {
                    // on each scroll event we need to call onScroll for mListItemVisibilityCalculator
                    // in order to recalculate the items visibility
//                videoRecyclerViewAdapter.notifyDataSetChanged();
//                    Handler handler = new Handler() {
//                        @Override
//                        public void handleMessage(Message msg) {
//                            try {
//                                if (!mList.isEmpty() && mScrollState == RecyclerView.SCROLL_STATE_IDLE) {
//                                    try {
//                                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(lm, rv);
//                                        mListItemVisibilityCalculator.onScroll(
//                                                mItemsPositionGetter,
//                                                lm.findFirstVisibleItemPosition(),
//                                                lm.findLastVisibleItemPosition() - lm.findFirstVisibleItemPosition() + 1,
//                                                mScrollState);
//
//                                    } catch (IndexOutOfBoundsException e) {
//
//                                    }
//                                }
//                            } catch (Exception e) {
//                                Log.e("exception", e.toString());
//                            }
//                        }
//                    };
//
//                    handler.sendEmptyMessageDelayed(0, 500);
                    if (scrollTimer != null) {
                        scrollTimer.cancel();
                    }
                    scrollTimer = new Timer();
                    scrollTimerTask = new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                if (!mList.isEmpty() && mScrollState == RecyclerView.SCROLL_STATE_IDLE) {
                                    try {
                                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(lm, rv);
                                        mListItemVisibilityCalculator.onScroll(
                                                mItemsPositionGetter,
                                                lm.findFirstVisibleItemPosition(),
                                                lm.findLastVisibleItemPosition() - lm.findFirstVisibleItemPosition() + 1,
                                                mScrollState);

                                    } catch (IndexOutOfBoundsException e) {

                                    }
                                }
                            } catch (Exception e) {
                                Log.e("exception", e.toString());
                            }
                        }
                    };
                    scrollTimer.schedule(scrollTimerTask, 100);

                }
            }
        });

        rv.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                if (nowPage < currentPage) {
                    Log.e("pagepagepage", nowPage + " / " + currentPage);
                    layNextLoading.setVisibility(View.VISIBLE);

                    if (nowPage != currentPage) {
                        final Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("page", currentPage + "");
                        params.put("category_num", "" + (pageNavi + 1));

                        AppController.apiTaskNew = new APITaskNew(ac, params, 522, stm);
                        AppController.apiTaskNew.execute();

                        nowPage = currentPage;
                    } else {
                        layNextLoading.setVisibility(View.GONE);
                    }

                }
            }
        });

        start();
    }

    private void start() {
        isFirst = false;
        layProgress.setVisibility(View.GONE);

        getData();

    }

    public void getData() {

        layProgress.setVisibility(View.VISIBLE);

        final int preCount = mList.size();
        Log.e("size", mList.size() + "");

        StringTransMethod stmDataResult = new StringTransMethod() {
            @Override
            public void endTrans(final String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    MinUtils.d("dataFollow", result);
                    if (jd.getInt("result") == 0) {

                        JSONArray jarr = new JSONArray(jd.getString("data"));
                        TimelineItem ti;
                        JSONObject j;


                        for (int i = 0; i < jarr.length(); i++) {

                            ti = new TimelineItem();

                            j = jarr.getJSONObject(i);

                            ti.setBody(j.getString("body"));
                            ti.setId(j.getString("id"));
                            ti.setComCount(j.getInt("com_cnt"));
                            if (j.getString("like_yn").equals("Y")) {
                                ti.setIsLike(true);
                            } else if (j.getString("like_yn").equals("N")) {
                                ti.setIsLike(false);
                            } else {
                                ti.setIsLike(false);
                            }
                            ti.setLikeCount(j.getInt("like_cnt"));
                            if (!j.getString("and_tag_id").equals("")) {
                                ti.setIsAndTag(true);
                                ti.setAndTagId(j.getString("and_tag_id"));
                                ti.setAndTagName(j.getString("and_tag"));
                                ti.setAndTagBody(j.getString("and_tag_body"));
                            } else {
                                ti.setIsAndTag(false);
                                ti.setAndTagId("");
                                ti.setAndTagName("");
                                ti.setAndTagBody("");
                            }
                            if (j.getString("follow_yn").equals("Y")) {
                                ti.setIsFollow(true);
                            } else {
                                ti.setIsFollow(false);
                            }
                            ti.setOwnerId(j.getString("email"));
                            ti.setPlayCnt(j.getInt("play_cnt"));
                            ti.setPro_url(j.getString("profile_picture"));
                            ti.setPsot_id(j.getString("post_id"));
                            ti.setUrl(j.getString("url"));
                            ti.setTime(j.getString("date"));

                            ti.setWidth(j.getInt("width1"));
                            ti.setHeight(j.getInt("height1"));

                            ti.setWidth2(j.getInt("width2"));
                            ti.setHeight2(j.getInt("height2"));

                            ti.setTags(j.getString("tag_list"));
                            ti.setTopNavi(0);

                            ti.setAd(false);
                            ti.setTopNavi(3);

                            mList.add(ItemFactory.createItemFromModel2(CategoryListActivity.this, ti, mVideoPlayerManager, 1));

                        }
//                        TimelineItem adItem = new TimelineItem();
//                        adItem.setAd(true);
//                        adItem.setAdVisible(true);
//                        adItem.setUrl("blank.mp4");
//
//                        mList.add(ItemFactory.createItemFromModel2(TimelineV1NewActivity.this, adItem, mVideoPlayerManager, 0));

//                        for(int i=0;i<preCount;i++){
//                            mList.remove(i);
//                        }

//                        mList.remove(0);

//                        for (int i = 0; i < mList.size() - 10; i++) {
//                            mList.remove(i);
//                        }

                        videoRecyclerViewAdapter.notifyDataSetChanged();

                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(lm, rv);
//                        mItemsPositionGetter = new ListViewItemPositionGetter(lm, rv);
                        layProgress.setVisibility(View.GONE);

                        layNextLoading.setVisibility(View.GONE);

//                        mRecyclerView.scrollToPosition(1);
//                        mRecyclerView.smoothScrollToPosition(2);

                        rv.smoothScrollToPosition(2);

                        rv.smoothScrollToPosition(0);

//                        QuickReturnListViewOnScrollListener listener = new QuickReturnListViewOnScrollListener.Builder(QuickReturnViewType.HEADER)
//                                .header(TimelineV1NewActivity.header)
//                                .minHeaderTranslation(-headerHeight)
//                                .isSnappable(true)
//                                .build();
//
//                        list.setOnScrollListener(listener);

//                        updateResultList();


                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    } else {

                    }

                    layProgress.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("page", "1");
        params.put("category_num", "" + (pageNavi + 1));

        Log.e("listNavi 2", listNavi + "");

        AppController.apiTaskNew = new APITaskNew(this, params, 522, stmDataResult);
        AppController.apiTaskNew.execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mVideoPlayerManager.resetMediaPlayer();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_category_list_home:
                overridePendingTransition(0, 0);
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mList.isEmpty()) {

            rv.post(new Runnable() {
                @Override
                public void run() {

                    mListItemVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            lm.findFirstVisibleItemPosition(),
                            lm.findLastVisibleItemPosition());

                }
            });

            videoRecyclerViewAdapter.notifyDataSetChanged();

        }
    }

    StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {

            MinUtils.d("timelineR", result);
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;

                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        ti.setWidth(j.getInt("width1"));
                        ti.setHeight(j.getInt("height1"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);
                        ti.setTopNavi(3);

                        mList.add(ItemFactory.createItemFromModel2(CategoryListActivity.this, ti, mVideoPlayerManager, 1));
                    }

//                    TimelineItem adItem = new TimelineItem();
//                    adItem.setAd(true);
//                    if (nowPage % 2 == 0) {
//                        adItem.setAdVisible(false);
//                    } else {
//                        adItem.setAdVisible(true);
//                    }
//
//                    mList.add(ItemFactory.createItemFromModel2(TimelineV1NewActivity.ac, adItem, mVideoPlayerManager, 0));

                    updateResultList();

                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                layNextLoading.setVisibility(View.GONE);
            }

        }
    };

    public boolean updateResultList() {

//        mVideoPlayerManager.resetMediaPlayer();

//        mListItemVisibilityCalculator =
//                new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);

//        mItemsPositionGetter = new ListViewItemPositionGetter(list);

//        videoRecyclerViewAdapter.notifyDataSetChanged();

//        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);

        rv.post(new Runnable() {
            @Override
            public void run() {

                mListItemVisibilityCalculator.onScrollStateIdle(
                        mItemsPositionGetter,
                        lm.findFirstVisibleItemPosition(),
                        lm.findLastVisibleItemPosition());
//                        list.getFirstVisiblePosition(),
//                        list.getLastVisiblePosition());

            }
        });

        videoRecyclerViewAdapter.notifyDataSetChanged();

        layProgress.setVisibility(View.GONE);

        layNextLoading.setVisibility(View.GONE);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CATEGORY_RETURN) {
            String id = data.getExtras().getString("id");
            boolean is = data.getExtras().getBoolean("is");
            int ct = data.getExtras().getInt("ct");
            for (int i = 0; i < mList.size(); i++) {
                TimelineVideoItem2 tvi = (TimelineVideoItem2) mList.get(i);

                if (tvi.getItem().getPsot_id().equals(id)) {
                    tvi.setLike(id, is, ct);
                    TimelineItem ti = tvi.getItem();
                    ti.setIsLike(is);
                    ti.setLikeCount(ct);
                    mList.set(i, ItemFactory.createItemFromModel2(CategoryListActivity.this, ti, mVideoPlayerManager, 1));
                }
            }
            videoRecyclerViewAdapter.notifyDataSetChanged();
        }
    }
}
