package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.UserListItem;
import com.picpic.sikkle.beans.UserListResult;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FriendTagReturnActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack, layResult, imvComplete;
    TextView tvSelected, tvNo;
    EditText edtSearch;
    ListView list;
    ArrayList<String> arr;
    UserListResult m_ResultList = null;
    ArrayAdapter<UserListItem> m_ListAdapter = null;
    boolean lastItemVisibleFlag = false;

    int listNavi = 0;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0) {
                getFollowingMeList(true, 1);
            } else {
                getUserTagSearch(true, s.toString(), 1);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_friend_tag_return);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("글쓰기 친구 태그 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_friend_tag_return_back);
        layBack.setOnClickListener(this);

        layResult = (LinearLayout) findViewById(R.id.lay_friend_tag_return_result);

        tvSelected = (TextView) findViewById(R.id.tv_friend_tag_return_selected);
        tvNo = (TextView) findViewById(R.id.tv_friend_tag_return_no_result);

        edtSearch = (EditText) findViewById(R.id.edt_friend_tag_return);

        edtSearch.addTextChangedListener(tw);

        imvComplete = (LinearLayout) findViewById(R.id.imv_friend_tag_return_next);

        imvComplete.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list_friend_tag_return);

        arr = new ArrayList<>();

        getFollowingMeList(true, 1);
    }

    private void getFollowingMeList(boolean is, int page) {
        listNavi = 0;
        if (is) {
            m_ResultList = null;
            m_ListAdapter = null;
        }

        StringTransMethod stmFollowingMeListResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
//                    Log.e("fList1", result);
                    if (j.getInt("result") == 0) {
                        JSONArray jarr = new JSONArray(j.getString("data"));
                        UserListResult ulr = new UserListResult();
                        JSONObject jd;
                        UserListItem uli;
                        for (int i = 0; i < jarr.length(); i++) {
                            jd = jarr.getJSONObject(i);

                            uli = new UserListItem();
                            uli.setEmail(jd.getString("email"));
                            uli.setName(jd.getString("id"));
                            uli.setUrl(jd.getString("url"));
                            if (jd.getString("follow_yn").equals("Y")) {
                                uli.setIsfriend(true);
                            } else {
                                uli.setIsfriend(false);
                            }

                            ulr.add(uli);

                        }

                        updateResultList(ulr);
                    }else if(j.getInt("result") == 100 || j.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);

        AppController.apiTaskNew = new APITaskNew(this, params, 404, stmFollowingMeListResult);
        AppController.apiTaskNew.execute();
        // TODO page 15

    }

    private void getUserTagSearch(boolean is, String str, int page) {
        listNavi = 1;
        if (is) {
            m_ListAdapter = null;
            m_ResultList = null;
        }

        StringTransMethod stmUserTagSearchResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
                        if (!j.isNull("friend")) {
                            JSONArray jarr = new JSONArray(j.getString("friend"));
                            UserListResult ulr = new UserListResult();
                            JSONObject jd;
                            UserListItem uli;
                            for (int i = 0; i < jarr.length(); i++) {
                                jd = jarr.getJSONObject(i);
                                uli = new UserListItem();
                                uli.setEmail(jd.getString("email"));
                                uli.setName(jd.getString("id"));
                                uli.setUrl(jd.getString("profile_picture"));
                                ulr.add(uli);
                            }
                            updateResultList(ulr);
                        }
                    }else if(j.getInt("result") == 100 || j.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("str", str);
        params.put("page", "" + page);

        AppController.apiTaskNew = new APITaskNew(this, params, 512, stmUserTagSearchResult);
        AppController.apiTaskNew.execute();
        // TODO page 10

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_friend_tag_return_back:
                setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_friend_tag_return_next:
                Intent i = new Intent();

                i.putExtra("data", arr);

                setResult(RESULT_OK, i);
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }

    protected boolean updateResultList(UserListResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new FriendAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO 터치했을때 행동
                    }
                });

            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();

                    int page = 0;
                    switch (listNavi) {
                        case 0:
                            page = view.getCount() / 15;
                            Log.e("page", page + "/" + count);
                            if (view.getCount() % 15 == 0) {
//                        getUserTagSearch(true, s.toString(), 1);
                                getFollowingMeList(false, page + 1);
                            }
                            break;
                        case 1:
                            page = view.getCount() / 10;
                            Log.e("page", page + "/" + count);
                            if (view.getCount() % 10 == 0) {
//                        getUserTagSearch(true, s.toString(), 1);
                                getUserTagSearch(false, "", page + 1);
                            }
                            break;
                    }

                    //TODO
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
            }
        });

        return true;
    }

    private void setFriendSelected() {
        String tempStr = "";

        for (int i = 0; i < arr.size(); i++) {
            if (i == arr.size() - 1) {
                tempStr = tempStr + arr.get(i);
            } else {
                tempStr = tempStr + arr.get(i) + ",";
            }
        }

        tvSelected.setText(tempStr);

    }

    class FriendAdapter extends ArrayAdapter<UserListItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public FriendAdapter(Context ctx, int txtViewId, List<UserListItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_user_list, null);

                CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_user_list);

                TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_user_list_row_name);

                final ImageView imvCheck1 = (ImageView) convertView.findViewById(R.id.imv_user_list_check1);

                final UserListItem model = getItem(pos);
                //TODO 타임라인 데이터 insert

                imvCheck1.setVisibility(View.VISIBLE);
                imvCheck1.setSelected(false);

                for (int i = 0; i < arr.size(); i++) {
                    if (model.getName().equals(arr.get(i))) {
                        imvCheck1.setSelected(true);
                    }
                }

                tvUserId.setText(model.getName());

                if (model.getUrl().equals("")) {
                    cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                } else {
                    cimvPro.setImageURLString(AppController.URL + model.getUrl());
                }

                if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                    imvCheck1.setVisibility(View.GONE);
                } else {
                    imvCheck1.setVisibility(View.VISIBLE);
                }

                imvCheck1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO 팔로우
                        if (imvCheck1.isSelected()) {
                            imvCheck1.setSelected(false);
                            arr.remove(model.getName());
                        } else {
                            imvCheck1.setSelected(true);
                            arr.add(model.getName());
                        }
                        setFriendSelected();

                    }
                });
                cimvPro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppController.getSp().getString("id", "").equals(model.getName())) {
                            startActivity(new Intent(FriendTagReturnActivity.this, MyPageActivity.class));
                            finish();
                        } else {
                            Intent i = new Intent(FriendTagReturnActivity.this, UserFeedForIdActivity.class);
                            i.putExtra("id", model.getName());
                            startActivity(i);
                        }
                    }
                });
                
            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}
