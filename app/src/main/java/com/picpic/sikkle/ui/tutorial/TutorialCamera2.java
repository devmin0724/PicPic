package com.picpic.sikkle.ui.tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.picpic.sikkle.R;

public class TutorialCamera2 extends Activity {

    ImageView imv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_tutorial_camera2);

        startActivity(new Intent(TutorialCamera2.this, TutorialCamera1.class));

        imv = (ImageView) findViewById(R.id.imv_tu_camera_2);
        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
