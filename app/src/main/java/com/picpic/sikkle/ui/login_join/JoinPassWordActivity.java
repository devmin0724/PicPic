package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JoinPassWordActivity extends Activity implements View.OnClickListener, View.OnFocusChangeListener {

    public static final int EMPTY_PASSWORD1 = 1002;
    public static final int EMPTY_PASSWORD2 = 1003;
    public static final int WRONG_PATTERN_PASSWORD1 = 2002;
    public static final int WRONG_PATTERN_PASSWORD2 = 2003;
    public static final int DISACCORD_PASSWORD = 3003;
    public static Activity ac;
    EditTextCheckIcon etci1, etci2;
    EditText edt1, edt2;
    Button btn;
    String tempEmail = "";

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join_pass_word);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 비밀번호 입력 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        tempEmail = getIntent().getExtras().getString("email");

        initViews();
    }
    private void initViews() {
        etci1 = (EditTextCheckIcon) findViewById(R.id.etci_join_password_pw1);
        etci2 = (EditTextCheckIcon) findViewById(R.id.etci_join_password_pw2);
        etci1.setOnClickListener(this);
        etci2.setOnClickListener(this);

        edt1 = (EditText) findViewById(R.id.edt_join_password_pw1);
        edt2 = (EditText) findViewById(R.id.edt_join_password_pw2);
        edt1.setOnFocusChangeListener(this);
        edt2.setOnFocusChangeListener(this);

        btn = (Button) findViewById(R.id.btn_join_password_password_next);
        btn.setOnClickListener(this);

    }

    private boolean checkPass(String pass) {
//        Pattern p = Pattern.compile("^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{6,15}$");
        Pattern p = Pattern.compile("^(?=.*[a-zA-Z]+)(?=.*[0-9]+).{6,15}$");
        Matcher m = p.matcher(pass);

        return m.matches();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etci_join_password_pw1:
                edt1.setText("");
                break;
            case R.id.etci_join_password_pw2:
                edt2.setText("");
                break;
            case R.id.btn_join_password_password_next:
                String temp1 = edt1.getText().toString();
                String temp2 = edt2.getText().toString();
                if (temp1.equals("")) {
                    startEvent(EMPTY_PASSWORD1);
                } else {
                    if (temp2.equals("")) {
                        startEvent(EMPTY_PASSWORD2);
                    } else {
                        if (temp1.length() < 4 || !checkPass(temp1)) {
                            startEvent(WRONG_PATTERN_PASSWORD1);
                        } else {
                            if (temp2.length() < 4 || !checkPass(temp2)) {
                                startEvent(WRONG_PATTERN_PASSWORD2);
                            } else {
                                if (temp1.equals(temp2)) {
//                                    changeViews();
                                    Intent i = new Intent(JoinPassWordActivity.this, JoinBirthActivity.class);
                                    i.putExtra("email", tempEmail);
                                    i.putExtra("password", edt1.getText().toString());
                                    startActivity(i);
                                } else {
                                    startEvent(DISACCORD_PASSWORD);
                                }
                            }
                        }
                    }
                }
                break;
        }
    }

    private void startEvent(int code) {
        switch (code) {
            case EMPTY_PASSWORD1:
                edt1.startAnimation(AppController.shake);
                etci1.setState(EditTextCheckIcon.CHOICE_WRONG);
                edt1.setText("");
                edt1.requestFocus();
                edt1.setHint(R.string.join_hint_password_empty);
                break;
            case EMPTY_PASSWORD2:
                edt2.startAnimation(AppController.shake);
                etci2.setState(EditTextCheckIcon.CHOICE_WRONG);
                edt2.setText("");
                edt2.requestFocus();
                edt2.setHint(R.string.join_hint_password_empty);
                break;
            case WRONG_PATTERN_PASSWORD1:
            case WRONG_PATTERN_PASSWORD2:
                edt1.startAnimation(AppController.shake);
                etci1.setState(EditTextCheckIcon.CHOICE_WRONG);
                edt1.setText("");
                edt1.requestFocus();
                edt1.setHint(R.string.join_hint_password_pattern);
                edt2.startAnimation(AppController.shake);
                etci2.setState(EditTextCheckIcon.CHOICE_WRONG);
                edt2.setText("");
                edt2.requestFocus();
                edt2.setHint(R.string.join_hint_password_pattern);
                break;
            case DISACCORD_PASSWORD:
                edt1.startAnimation(AppController.shake);
                edt2.startAnimation(AppController.shake);
                edt1.requestFocus();
                etci1.setState(EditTextCheckIcon.CHOICE_WRONG);
                etci2.setState(EditTextCheckIcon.CHOICE_WRONG);
                edt1.setText("");
                edt2.setText("");
                edt1.setHint(R.string.join_hint_password_disaccord);
                edt2.setHint(R.string.join_hint_password_disaccord);
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edt_join_password_pw1:
                etci1.setState(EditTextCheckIcon.CHOICE_X);
                etci2.setState(EditTextCheckIcon.CHOICE_NON);
                break;
            case R.id.edt_join_password_pw2:
                etci1.setState(EditTextCheckIcon.CHOICE_NON);
                etci2.setState(EditTextCheckIcon.CHOICE_X);
                break;
        }
    }
}
