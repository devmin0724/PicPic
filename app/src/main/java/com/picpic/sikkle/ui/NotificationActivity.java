package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.picpic.sikkle.R;
import com.picpic.sikkle.fragment.NotificationAllFragment;
import com.picpic.sikkle.fragment.NotificationFollowFragment;
import com.picpic.sikkle.fragment.NotificationMyFragment;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NotificationActivity extends FragmentActivity implements View.OnClickListener {

    LinearLayout imvBack, layTimeLine, layInterest, layCamera, layMypage, layNotification, layHome, laySearch, lay1, lay2, lay3;
    private ViewPager mViewPager;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_notification);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("알림 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        lay1 = (LinearLayout) findViewById(R.id.lay_tab_title2_1);
        lay1.setOnClickListener(this);
        lay2 = (LinearLayout) findViewById(R.id.lay_tab_title2_2);
        lay2.setOnClickListener(this);
        lay3 = (LinearLayout) findViewById(R.id.lay_tab_title2_3);
        lay3.setOnClickListener(this);

        initViews();

        confirmNoti();

    }

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        android.util.Log.d("TAG","TOTAL MEMORY : "+(Runtime.getRuntime().totalMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG","MAX MEMORY : "+(Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG","FREE MEMORY : "+(Runtime.getRuntime().freeMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG","ALLOCATION MEMORY : "+((Runtime.getRuntime().totalMemory() -Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + "MB");
        super.onDestroy();
    }

    private void confirmNoti() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = false;
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params,604, stm);
        AppController.apiDataTaskNew.execute();

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void initViews() {

        layTimeLine = (LinearLayout) findViewById(R.id.lay_notification_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_notification_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_notification_camera);
        layMypage = (LinearLayout) findViewById(R.id.lay_notification_mypage);
        layNotification = (LinearLayout) findViewById(R.id.lay_notification_noti);
        layNotification.setSelected(true);

        layHome = (LinearLayout) findViewById(R.id.imv_notification_home);
        layHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mViewPager.getCurrentItem()) {
                    case 0:
                        NotificationAllFragment.list.smoothScrollToPosition(0);
                        break;
                    case 1:
                        NotificationMyFragment.list.smoothScrollToPosition(0);
                        break;
                    case 2:
                        NotificationFollowFragment.list.smoothScrollToPosition(0);
                        break;
                }
            }
        });
        laySearch = (LinearLayout) findViewById(R.id.imv_notification_search);
        laySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Search Click").build());
                overridePendingTransition(0, 0);
                Intent ii = new Intent(NotificationActivity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 0);
                startActivity(ii);
            }
        });

        layTimeLine.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        mViewPager = (ViewPager) findViewById(R.id.vp_notification);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.notification_all, NotificationAllFragment.class)
                .add(R.string.notification_my, NotificationMyFragment.class)
                .add(R.string.notification_follow, NotificationFollowFragment.class)
                .create());

        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        lay1.setSelected(true);
                        lay2.setSelected(false);
                        lay3.setSelected(false);
                        break;
                    case 1:
                        lay1.setSelected(false);
                        lay2.setSelected(true);
                        lay3.setSelected(false);
                        break;
                    case 2:
                        lay1.setSelected(false);
                        lay2.setSelected(false);
                        lay3.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(0, 0);
//        startActivity(new Intent(NotificationActivity.this, NewGridTimelineActivity.class));
        startActivity(new Intent(NotificationActivity.this, TimelineV1NewActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        Intent i = null;
        switch (v.getId()) {
            case R.id.lay_tab_title2_1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.lay_tab_title2_2:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.lay_tab_title2_3:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.lay_notification_timeline:
                i = new Intent(NotificationActivity.this, TimelineV1NewActivity.class);
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                overridePendingTransition(0,0);
                break;
            case R.id.lay_notification_interest:
//                i = new Intent(NotificationActivity.this, InterestFeedActivity.class);
                i = new Intent(NotificationActivity.this, InterestFeedV1Activity.class);
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                overridePendingTransition(0,0);
                break;
            case R.id.lay_notification_camera:
//                System.gc();
//                Intent gifCamera = new Intent(NotificationActivity.this, GIFCameraActivity.class);
//                gifCamera.putExtra("navi", 0);
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
//                startActivity(gifCamera);


//                System.gc();
//                Intent gifCamera = new Intent(NotificationActivity.this, GIFCameraFilmActivity.class);
//                gifCamera.putExtra("navi", 0);
//                startActivity(gifCamera);
//                overridePendingTransition(0,0);

                startActivity(new Intent(NotificationActivity.this, CameraPopUp.class));
                break;
            case R.id.lay_notification_noti:
                break;
            case R.id.lay_notification_mypage:
                i = new Intent(NotificationActivity.this, MyPageV2Activity.class);
                startActivity(i);
                finish();
                overridePendingTransition(0,0);
                break;

        }
    }

}
