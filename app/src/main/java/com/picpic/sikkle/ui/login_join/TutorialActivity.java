package com.picpic.sikkle.ui.login_join;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;
import com.picpic.sikkle.pager.TutorialPager;
import com.picpic.sikkle.utils.MinUtils;

public class TutorialActivity extends FragmentActivity {

    ViewPager vp;

    ImageView[] navis;

    LinearLayout lay, imv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tutorial);

        lay = (LinearLayout) findViewById(R.id.lay_tutorial);

        navis = new ImageView[5];
        int size = (int) (MinUtils.density * 5);
        int margin = (int) (MinUtils.density * 7);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(size, size);
        lp.leftMargin = margin;

        for (int j = 0; j < 5; j++) {
            navis[j] = new ImageView(this);

            navis[j].setBackgroundResource(R.drawable.tutorial_unnow);

            navis[j].setLayoutParams(lp);
            lay.addView(navis[j], lp);

        }

        vp = (ViewPager) findViewById(R.id.vp_tutorial);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        vp.setAdapter(new TutorialAdapter(fm));

        vp.setPageTransformer(false, new ViewPager.PageTransformer() {

            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void transformPage(View view, float v) {
                // TODO Auto-generated method stub
                final float normalizedposition = Math.abs(Math.abs(v) - 1);
                view.setAlpha(normalizedposition);

            }
        });

        vp.setOffscreenPageLimit(5);
        vp.setAlwaysDrawnWithCacheEnabled(true);
        vp.setFadingEdgeLength(10);
        vp.setHorizontalFadingEdgeEnabled(true);
        vp.setSaveEnabled(true);

        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < 5; i++) {
                    if (i == position) {
                        navis[i].setImageResource(R.drawable.tutorial_now);
                    } else {
                        navis[i].setImageResource(R.drawable.tutorial_unnow);
                    }
                }

                if (position == 4) {
                    lay.setVisibility(View.GONE);
                } else {
                    lay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imv = (LinearLayout) findViewById(R.id.imv_tutorial_x);

        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(new Intent(TutorialActivity.this, LoginActivity.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(TutorialActivity.this, LoginActivity.class));
    }

    private class TutorialAdapter extends FragmentPagerAdapter {

        public TutorialAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new TutorialPager(position);
        }

        @Override
        public int getCount() {
            return 5;
        }
    }

}
