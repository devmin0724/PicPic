package com.picpic.sikkle.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ReportActivity extends Activity implements View.OnClickListener {

    LinearLayout lay1, lay2, lay3, lay4, layBack, imvComplete;
    EditText edt;
    ImageView imv1, imv2, imv3, imv4;

    int selectItem = 0;

    Animation shake;

    String postId = "", tempType = "";

    boolean isTrans = false;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_report);

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        postId = getIntent().getExtras().getString("postId");
        tempType = getIntent().getExtras().getString("pc");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("신고하기 페이지_" + postId + "_" + tempType);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("신고하기 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        lay1 = (LinearLayout) findViewById(R.id.lay_report_select_1);
        lay2 = (LinearLayout) findViewById(R.id.lay_report_select_2);
        lay3 = (LinearLayout) findViewById(R.id.lay_report_select_3);
        lay4 = (LinearLayout) findViewById(R.id.lay_report_select_4);
        layBack = (LinearLayout) findViewById(R.id.lay_report_back);

        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay3.setOnClickListener(this);
        lay4.setOnClickListener(this);
        layBack.setOnClickListener(this);

        imvComplete = (LinearLayout) findViewById(R.id.imv_report_next);
        imvComplete.setOnClickListener(this);

        imv1 = (ImageView) findViewById(R.id.imv_report_1);
        imv2 = (ImageView) findViewById(R.id.imv_report_2);
        imv3 = (ImageView) findViewById(R.id.imv_report_3);
        imv4 = (ImageView) findViewById(R.id.imv_report_4);

        edt = (EditText) findViewById(R.id.edt_report_input);

    }

    private void initImv(int a) {
        selectItem = a;
        imv1.setSelected(false);
        imv2.setSelected(false);
        imv3.setSelected(false);
        imv4.setSelected(false);
        switch (a) {
            case 1:
                imv1.setSelected(true);
                break;
            case 2:
                imv2.setSelected(true);
                break;
            case 3:
                imv3.setSelected(true);
                break;
            case 4:
                imv4.setSelected(true);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_report_select_1:
                initImv(1);
                break;
            case R.id.lay_report_select_2:
                initImv(2);
                break;
            case R.id.lay_report_select_3:
                initImv(3);
                break;
            case R.id.lay_report_select_4:
                initImv(4);
                break;
            case R.id.lay_report_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_report_next:
                if (selectItem == 4) {
                    if (edt.getText().toString().equals("")) {

                        lay4.startAnimation(shake);
                    } else {
                        if (!isTrans) {
                            reportIt();
                        }
                    }
                } else {
                    if (!isTrans) {
                        reportIt();
                    }
                }
                break;
        }
    }

    private void reportIt() {
        isTrans = true;
        StringTransMethod stmReport = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    if (jd.getInt("result") == 0) {
                        finish();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.report_complete), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                } finally {
                    isTrans = false;
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("call_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        switch (selectItem) {
            case 1:
                params.put("reason", "");
                params.put("reason_code", "0");
                break;
            case 2:
                params.put("reason", "");
                params.put("reason_code", "1");
                break;
            case 3:
                params.put("reason", "");
                params.put("reason_code", "2");
                break;
            case 4:
                params.put("reason", edt.getText().toString());
                params.put("reason_code", "3");
                break;
        }
        params.put("type", tempType);

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 204, stmReport);
        AppController.apiDataTaskNew.execute();

    }

}
