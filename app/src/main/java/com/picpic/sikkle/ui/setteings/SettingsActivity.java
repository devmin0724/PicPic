package com.picpic.sikkle.ui.setteings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.popup.LogoutPopUpActivity;
import com.picpic.sikkle.ui.popup.SignOutPopUpActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends Activity implements View.OnClickListener {

    public static Activity ac;
    private final int ALARM_RETURN = 1111;
    private final int PUBLIC_RETURN = 1112;
    LinearLayout layMyInfo, layPublicSetting, layProtectAccount, layAlarm, laySocial, layInvite, layFindFriend,
            layProtectIdManage, layNotice, layProgramInfo, layLogout, laySignout, layBack;
    Switch switchProtectAccount, switchAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        initViews();
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void initViews() {

        layMyInfo = (LinearLayout) findViewById(R.id.lay_settings_main_myinfo);
        layPublicSetting = (LinearLayout) findViewById(R.id.lay_settings_main_public_setting);
        layProtectAccount = (LinearLayout) findViewById(R.id.lay_settings_main_protect_account);
        layAlarm = (LinearLayout) findViewById(R.id.lay_settings_main_alarm);
        laySocial = (LinearLayout) findViewById(R.id.lay_settings_main_social);
        layInvite = (LinearLayout) findViewById(R.id.lay_settings_main_invite);
        layFindFriend = (LinearLayout) findViewById(R.id.lay_settings_main_find_friend);
        layProtectIdManage = (LinearLayout) findViewById(R.id.lay_settings_main_protect_id_manage);
        layNotice = (LinearLayout) findViewById(R.id.lay_settings_main_notice);
        layProgramInfo = (LinearLayout) findViewById(R.id.lay_settings_main_program_info);
        layLogout = (LinearLayout) findViewById(R.id.lay_settings_main_logout);
        laySignout = (LinearLayout) findViewById(R.id.lay_settings_main_signout);
        layBack = (LinearLayout) findViewById(R.id.lay_settings_main_back);

        layMyInfo.setOnClickListener(this);
        layPublicSetting.setOnClickListener(this);
        layProtectAccount.setOnClickListener(this);
        layAlarm.setOnClickListener(this);
        laySocial.setOnClickListener(this);
        layInvite.setOnClickListener(this);
        layFindFriend.setOnClickListener(this);
        layProtectIdManage.setOnClickListener(this);
        layNotice.setOnClickListener(this);
        layProgramInfo.setOnClickListener(this);
        layLogout.setOnClickListener(this);
        laySignout.setOnClickListener(this);
        layBack.setOnClickListener(this);

        switchProtectAccount = (Switch) findViewById(R.id.switch_settings_main_protect_account);
        switchProtectAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //TODO 비공개계정 여부 왔다갔다
                AppController.getSe().putBoolean("protectAccount", isChecked);
                AppController.getSe().commit();

                String yn = "";

                if (!isChecked) {
                    yn = "N";
                } else {
                    yn = "Y";
                }

                StringTransMethod stmPubicYN = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);
                            if (jd.getInt("result") == 0) {
                                String str = jd.getString("close_yn");
                                if (str.equals("N")) {
                                    switchProtectAccount.setChecked(false);
                                } else {
                                    switchProtectAccount.setChecked(true);
                                }
                            }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };

                Map<String, String> params = new HashMap<>();

                params.put("email", AppController.getSp().getString("email", ""));
                params.put("close_yn", yn);

                AppController.apiTaskNew = new APITaskNew(SettingsActivity.this, params, 219, stmPubicYN);
                AppController.apiTaskNew.execute();
            }
        });
        switchAlarm = (Switch) findViewById(R.id.switch_settings_main_alarm);
        switchAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //TODO 알림 왔다갔다
                AppController.getSe().putBoolean("alarmAll", isChecked);
                AppController.getSe().commit();
            }
        });

        switchAlarm.setChecked(AppController.getSp().getBoolean("alarmAll", true));
        switchProtectAccount.setChecked(AppController.getSp().getBoolean("protectAccount", false));
    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_main_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_main_protect_account:
                if (switchProtectAccount.isChecked()) {
                    switchProtectAccount.setChecked(false);
                } else {
                    switchProtectAccount.setChecked(true);
                }
                break;
            case R.id.lay_settings_main_myinfo:
                startActivity(new Intent(SettingsActivity.this, SettingsMyInfoActivity.class));
                break;
            case R.id.lay_settings_main_public_setting:
                startActivityForResult(new Intent(SettingsActivity.this, SettingsPublicSettingActivity.class), PUBLIC_RETURN);
                break;
            case R.id.lay_settings_main_alarm:
                startActivityForResult(new Intent(SettingsActivity.this, SettingsAlarmActivity.class), ALARM_RETURN);
                break;
            case R.id.lay_settings_main_social:
                startActivity(new Intent(SettingsActivity.this, SettingsSocialActivity.class));
                break;
            case R.id.lay_settings_main_invite:

                break;
            case R.id.lay_settings_main_find_friend:

                break;
            case R.id.lay_settings_main_protect_id_manage:
                startActivity(new Intent(SettingsActivity.this, SettingsProtectIdManageActivity.class));
                break;
            case R.id.lay_settings_main_notice:
                startActivity(new Intent(SettingsActivity.this, SettingsNoticeActivity.class));
                break;
            case R.id.lay_settings_main_program_info:
                startActivity(new Intent(SettingsActivity.this, SettingsProgramInfoActivity.class));
                break;
            case R.id.lay_settings_main_logout:
                startActivity(new Intent(SettingsActivity.this, LogoutPopUpActivity.class));
                break;
            case R.id.lay_settings_main_signout:
                startActivity(new Intent(SettingsActivity.this, SignOutPopUpActivity.class));
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PUBLIC_RETURN:
                switchProtectAccount.setChecked(AppController.getSp().getBoolean("protectAccount", false));
                break;
            case ALARM_RETURN:
                switchAlarm.setChecked(AppController.getSp().getBoolean("alarmAll", true));
                break;
        }
    }
}
