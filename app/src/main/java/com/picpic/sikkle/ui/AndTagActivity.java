package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.AndTagRankItem;
import com.picpic.sikkle.beans.AndTagTaggerItem;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.pager.AndTagRanking;
import com.picpic.sikkle.ui.tutorial.TutorialCamera3;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.FixedImageView;
import com.picpic.sikkle.widget.floating.FloatingActionLayout;
import com.picpic.sikkle.widget.floating.FloatingScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndTagActivity extends FragmentActivity implements View.OnClickListener {

    ImageView imvChatting, imvTimeline, imvInterest, imvAndtag, imvMypage, imvCamera;
    LinearLayout imvSearch, imvAlarm;
    FloatingScrollView fsv;
    FloatingActionLayout fal;

    //    ArrayList<FixedImageView> imvRans = new ArrayList<>();
    ArrayList<FixedImageView> imvWorlds = new ArrayList<>();

    //    ArrayList<TextView> tvRans = new ArrayList<>();
    ArrayList<TextView> tvWorlds = new ArrayList<>();

    ViewPager vp;

    ArrayList<AndTagRankItem> arrs;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pager_andtag);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("앤드태그 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        int tuNavi = AppController.getSp().getInt("turank", -1);

        if (tuNavi > 0) {
            tuNavi = tuNavi - 1;
            AppController.getSe().putInt("turank", tuNavi);
            AppController.getSe().commit();

            startActivity(new Intent(AndTagActivity.this, TutorialCamera3.class));
        } else {

        }

        arrs = new ArrayList<>();

        imvAlarm = (LinearLayout) findViewById(R.id.imv_and_tag_alarm);
        imvSearch = (LinearLayout) findViewById(R.id.imv_and_tag_search);
        imvChatting = (ImageView) findViewById(R.id.imv_and_tag_chatting);

        imvTimeline = (ImageView) findViewById(R.id.imv_and_tag_timeline);
        imvInterest = (ImageView) findViewById(R.id.imv_and_tag_interest);
        imvCamera = (ImageView) findViewById(R.id.imv_and_tag_camera);
        imvAndtag = (ImageView) findViewById(R.id.imv_and_tag_andtag);
        imvMypage = (ImageView) findViewById(R.id.imv_and_tag_mypage);

        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_1));
        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_2));
        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_3));
        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_4));
        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_5));
        imvWorlds.add((FixedImageView) findViewById(R.id.imv_andtag_6));

        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_1));
        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_2));
        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_3));
        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_4));
        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_5));
        tvWorlds.add((TextView) findViewById(R.id.tv_andtag_6));

        for (int i = 0; i < imvWorlds.size(); i++) {
            imvWorlds.get(i).setOnClickListener(this);
        }

        imvAlarm.setOnClickListener(this);
        imvSearch.setOnClickListener(this);
        imvChatting.setOnClickListener(this);
        imvTimeline.setOnClickListener(this);
        imvInterest.setOnClickListener(this);
        imvCamera.setOnClickListener(this);
        imvAndtag.setOnClickListener(this);
        imvMypage.setOnClickListener(this);

        imvAndtag.setSelected(true);

        fsv = (FloatingScrollView) findViewById(R.id.sv_and_tag);
        fal = (FloatingActionLayout) findViewById(R.id.fal_and_tag);

        fal.attachToScrollView(fsv);

        vp = (ViewPager) findViewById(R.id.vp_and_tag);

        vp.setPageTransformer(false, new ViewPager.PageTransformer() {

            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void transformPage(View view, float v) {
                final float normalizedposition = Math.abs(Math.abs(v) - 1);
                view.setAlpha(normalizedposition);

            }
        });

        vp.setHovered(true);

        imvAndtag.setSelected(true);

        getData();
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(AndTagActivity.this, NewGridTimelineActivity.class));
    }

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 516, stm);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject j = new JSONObject(result);

                if (j.getInt("result") == 0) {
                    JSONArray myCou = new JSONArray(j.getString("my_country"));

                    for (int i = 0; i < myCou.length(); i++) {
                        JSONObject jd = myCou.getJSONObject(i);
                        MinUtils.d("mycountry", myCou.toString());
                        AndTagRankItem ai = new AndTagRankItem();
                        ai.setId(jd.getString("id"));
                        ai.setUrl(jd.getString("url"));
                        ai.setBody(jd.getString("explanation"));
                        ai.setCount(i + 1);
                        ai.setPro(jd.getString("profile_picture"));
                        ai.setTitle("&" + jd.getString("tag_name"));
                        ai.setJoinCount(jd.getInt("tag_cnt"));
                        ai.setEmail(jd.getString("tag_founder"));
                        ai.setTag_id(jd.getString("tag_id"));

                        ArrayList<AndTagTaggerItem> taggers = new ArrayList<>();
                        if (!jd.getString("tager0").equals("null")) {
                            AndTagTaggerItem att = new AndTagTaggerItem();
                            att.setEmail(jd.getString("tager0"));
                            att.setId(jd.getString("tager0_id"));
                            att.setUrl(jd.getString("tager0_url"));
                            taggers.add(att);
                        }

                        if (!jd.getString("tager1").equals("null")) {
                            AndTagTaggerItem att = new AndTagTaggerItem();
                            att.setEmail(jd.getString("tager1"));
                            att.setId(jd.getString("tager1_id"));
                            att.setUrl(jd.getString("tager1_url"));
                            taggers.add(att);
                        }

                        if (!jd.getString("tager2").equals("null")) {
                            AndTagTaggerItem att = new AndTagTaggerItem();
                            att.setEmail(jd.getString("tager2"));
                            att.setId(jd.getString("tager2_id"));
                            att.setUrl(jd.getString("tager2_url"));
                            taggers.add(att);
                        }

                        ai.setTaggers(taggers);

                        arrs.add(ai);


                    }

//                JSONArray myRan = new JSONArray(j.getString("my_random"));
//
//                for (int i = 0; i < myRan.length(); i++) {
//                    JSONObject jd2 = myRan.getJSONObject(i);
//                    String tempStr = jd2.getString("url");
//                    int underbarCount = tempStr.lastIndexOf("_");
//                    String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());
//
//                    int lastIdx = tempUrl.lastIndexOf("_");
//
//                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//                    imvRans.get(i).setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
//                    imvRans.get(i).setTag(jd2.getString("tag_id"));
//                    imvRans.get(i).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(AndTagActivity.this, AndTagFeedActivity.class);
//                            i.putExtra("tag", v.getTag().toString());
//                            startActivity(i);
//                        }
//                    });
//                    tvRans.get(i).setText("&" + jd2.getString("tag_name"));
//                    tvRans.get(i).setTag(jd2.getString("tag_id"));
//                    tvRans.get(i).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(AndTagActivity.this, AndTagFeedActivity.class);
//                            i.putExtra("tag", v.getTag().toString());
//                            startActivity(i);
//                        }
//                    });
//                }

                    JSONArray world = new JSONArray(j.getString("world"));

                    for (int i = 0; i < world.length(); i++) {
                        final JSONObject jd2 = world.getJSONObject(i);
                        String tempStr = jd2.getString("url");
                        int underbarCount = tempStr.lastIndexOf("_");
                        final String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());
                        Log.e("url", tempUrl);
                        int lastIdx = tempUrl.lastIndexOf("_");

                        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                        final int FI = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imvWorlds.get(FI).setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                                    imvWorlds.get(FI).setTag(jd2.getString("tag_id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        imvWorlds.get(i).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(AndTagActivity.this, AndTagFeedActivity.class);
                                i.putExtra("tag", v.getTag().toString());
                                startActivity(i);
                            }
                        });
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    tvWorlds.get(FI).setText("&" + jd2.getString("tag_name"));
                                    tvWorlds.get(FI).setTag(jd2.getString("tag_id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        tvWorlds.get(i).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(AndTagActivity.this, AndTagFeedActivity.class);
                                i.putExtra("tag", v.getTag().toString());
                                startActivity(i);
                            }
                        });
                    }

                }else if(j.getInt("result") == 100 || j.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            final android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
            vp.setAdapter(new RankPagerAdapter(fm));
        }
    };

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_and_tag_search:
                overridePendingTransition(0, 0);
                Intent ii = new Intent(AndTagActivity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 2);
                startActivity(ii);
                break;
            case R.id.imv_and_tag_alarm:
                overridePendingTransition(0, 0);
                startActivity(new Intent(AndTagActivity.this, NotificationActivity.class));
                break;
            case R.id.imv_and_tag_chatting:

                break;
            case R.id.imv_and_tag_timeline:
                overridePendingTransition(0, 0);
                startActivity(new Intent(AndTagActivity.this, NewGridTimelineActivity.class));
                finish();
                break;
            case R.id.imv_and_tag_interest:
                overridePendingTransition(0, 0);
                startActivity(new Intent(AndTagActivity.this, InterestFeedActivity.class));
                finish();
                break;
            case R.id.imv_and_tag_camera:
                overridePendingTransition(0, 0);
                Intent gifCamera = new Intent(AndTagActivity.this, GIFCameraActivity.class);
                gifCamera.putExtra("navi", 0);
                startActivity(gifCamera);
                break;
            case R.id.imv_and_tag_andtag:
//                startActivity(new Intent(AndTagActivity.this, InterestFeedActivity.class));
//                finish();
                break;
            case R.id.imv_and_tag_mypage:
                overridePendingTransition(0, 0);
                startActivity(new Intent(AndTagActivity.this, MyPageActivity.class));
                finish();
                break;
        }
    }

    private class RankPagerAdapter extends FragmentPagerAdapter {

        public RankPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new AndTagRanking(arrs.get(position), arrs.size());
        }

        @Override
        public int getCount() {
            return arrs.size();
        }
    }
}
