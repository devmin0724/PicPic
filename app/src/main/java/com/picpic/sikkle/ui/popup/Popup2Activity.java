package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.EditPostActivity;
import com.picpic.sikkle.ui.EditPostActivityV1;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Popup2Activity extends Activity implements View.OnClickListener {

    // 수정하기, 삭제하기
    public static final int POPUP2_TYPE_1 = 20001;
    // 차단하기, 신고하기
    public static final int POPUP2_TYPE_2 = 20002;
    // 이메일발송하였습니다, 확인
    public static final int POPUP2_TYPE_3 = 20003;

    private static final int EDIT_POST_RETURN = 1112;
    private static final int DELETE_CONFIRM = 1113;
    private static final int BLOCK_CONFIRM = 1114;

    Button btnUp, btnDown, btnMiddle;
    int pageNavi = 0;
    String tempPostId = "", tempPC = "", tempUrl = "", tempTitle = "", tempBody = "", tempUserId = "";

    private long latestId = -1;

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Uri urlToDownload;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private BroadcastReceiver completeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, getResources().getString(R.string.download_completed), Toast.LENGTH_SHORT).show();
        }

    };

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(completeReceiver, completeFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(completeReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_popup2);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("more 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        pageNavi = getIntent().getExtras().getInt("pageNavi");
        tempPostId = getIntent().getExtras().getString("postId");
        tempPC = getIntent().getExtras().getString("pc");
        tempUrl = getIntent().getExtras().getString("url");
        tempTitle = getIntent().getExtras().getString("title");
        tempBody = getIntent().getExtras().getString("body");
        tempUserId = getIntent().getExtras().getString("email");

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        initViews();
    }

    private void initViews() {
        btnUp = (Button) findViewById(R.id.btn_popup2_up);
        btnDown = (Button) findViewById(R.id.btn_popup2_down);
        btnMiddle = (Button) findViewById(R.id.btn_popup2_middle);

        btnUp.setOnClickListener(this);
        btnDown.setOnClickListener(this);
        btnMiddle.setOnClickListener(this);

        switch (pageNavi) {
            case POPUP2_TYPE_1:
                btnUp.setText(getResources().getString(R.string.popup2_gif_download));
                btnUp.setTextColor(Color.BLACK);
                btnMiddle.setText(getResources().getString(R.string.popup2_edit));
                btnMiddle.setTextColor(Color.BLACK);
                btnDown.setText(getResources().getString(R.string.popup2_delete));
                btnUp.setTextColor(Color.BLACK);
                break;
            case POPUP2_TYPE_2:
                btnUp.setText(getResources().getString(R.string.popup2_gif_download));
                btnUp.setTextColor(Color.BLACK);
                btnMiddle.setText(getResources().getString(R.string.popup2_block));
                btnMiddle.setTextColor(Color.BLACK);
                btnDown.setText(getResources().getString(R.string.popup2_report));
                btnUp.setTextColor(Color.BLACK);
                break;
            case POPUP2_TYPE_3:
                btnUp.setText(getResources().getString(R.string.popup2_gif_download));
                btnUp.setTextColor(Color.BLACK);
                btnMiddle.setText(getResources().getString(R.string.popup2_email_send));
                btnMiddle.setTextColor(Color.BLACK);
                btnDown.setText(getResources().getString(R.string.popup_confirm));
                btnUp.setTextColor(Color.BLACK);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_popup2_up:
                //TODO GIF 다운로드
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("GIF Download Click" + "_" + tempPostId).build());

                urlToDownload = Uri.parse(tempUrl);
                List<String> pathSegments = urlToDownload.getPathSegments();
                request = new DownloadManager.Request(urlToDownload);
                request.setTitle(tempTitle);
                request.setDescription(tempBody);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pathSegments.get(pathSegments.size() - 1));
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
                latestId = downloadManager.enqueue(request);

                finish();
                overridePendingTransition(0, 0);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.download_start), Toast.LENGTH_LONG).show();

                AppController.ActionLogInsert(tempPostId, "D", this);
                break;
            case R.id.btn_popup2_down:
                switch (pageNavi) {
                    case POPUP2_TYPE_1:
                        //TODO 삭제하기
                        Intent i2 = new Intent(Popup2Activity.this, ConfirmPopupActivity.class);
                        i2.putExtra("navi", 1);
                        startActivityForResult(i2, DELETE_CONFIRM);
                        break;
                    case POPUP2_TYPE_2:
                        //TODO 신고하기
                        Intent i = new Intent();
                        i.putExtra("type", pageNavi);
                        i.putExtra("select", 3);
                        i.putExtra("pc", tempPC);
                        i.putExtra("postId", tempPostId);
                        setResult(RESULT_OK, i);
                        finish();
                        break;
                    case POPUP2_TYPE_3:
                        finish();
                        break;
                }
                break;
            case R.id.btn_popup2_middle:
                switch (pageNavi) {
                    case POPUP2_TYPE_1:
                        //TODO 수정하기
//                        Intent i = new Intent(Popup2Activity.this, EditPostActivity.class);
                        Intent i = new Intent(Popup2Activity.this, EditPostActivityV1.class);
                        i.putExtra("postId", tempPostId);
                        startActivityForResult(i, EDIT_POST_RETURN);
                        break;
                    case POPUP2_TYPE_2:
                        //TODO 차단하기
                        Intent i2 = new Intent(Popup2Activity.this, ConfirmPopupActivity.class);
                        i2.putExtra("navi", 0);
                        startActivityForResult(i2, BLOCK_CONFIRM);
                        break;
                    case POPUP2_TYPE_3:
                        // 클릭이벤트 없음
                        break;
                }
                break;
        }
    }

    private void blockExe() {

        StringTransMethod stmBlock = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        Intent i = new Intent();
                        i.putExtra("type", pageNavi);
                        i.putExtra("select", 2);
                        i.putExtra("pc", tempPC);
                        i.putExtra("postId", tempPostId);
                        setResult(RESULT_OK, i);
                        finish();
                        overridePendingTransition(0, 0);
                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> parmas = new HashMap<>();

        parmas.put("my_id", AppController.getSp().getString("email", ""));
        parmas.put("user_id", tempUserId);

        AppController.apiDataTaskNew = new APIDataTaskNew(this, parmas, 216, stmBlock);
        AppController.apiDataTaskNew.execute();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_POST_RETURN) {
            if (resultCode == RESULT_OK) {
                Intent i = new Intent();
                i.putExtra("type", pageNavi);
                i.putExtra("select", 1);
                i.putExtra("pc", tempPC);
                i.putExtra("postId", tempPostId);
                setResult(RESULT_OK, i);
                finish();
                overridePendingTransition(0, 0);
            } else {
                setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(0, 0);
            }
        } else if (requestCode == BLOCK_CONFIRM) {
            if (resultCode == RESULT_OK) {
                blockExe();
            }
        } else if (requestCode == DELETE_CONFIRM) {
            if (resultCode == RESULT_OK) {
                StringTransMethod stmDelete = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);

                            if (jd.getInt("result") == 0) {
                                Intent i = new Intent();
                                i.putExtra("type", pageNavi);
                                i.putExtra("select", 2);
                                i.putExtra("pc", tempPC);
                                i.putExtra("postId", tempPostId);
                                setResult(RESULT_OK, i);
                                finish();
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.post_delete), Toast.LENGTH_LONG).show();
                            }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                Map<String, String> params = new HashMap<>();
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("post_id", tempPostId);
                params.put("user_tags", "");
                params.put("and_tag", "");
                params.put("tags", "");
                params.put("url", "");
                params.put("body", "");
                params.put("type", "D");

                AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 232, stmDelete);
                AppController.apiDataTaskNew.execute();

            }
        }
    }
}
