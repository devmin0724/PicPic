/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;

import java.util.ArrayList;
import java.util.List;

public class TimeLineV1Activity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout lay1, lay2, lay3, laySearch;
    ImageView imvLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line_v1);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        viewPager.setOffscreenPageLimit(6);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                initTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        imvLogo = (ImageView) findViewById(R.id.imv_main_timeline_home_new);

        laySearch = (LinearLayout) findViewById(R.id.imv_timeline_search_new);

        lay1 = (LinearLayout) findViewById(R.id.lay_timeline_header_new_1);
        lay2 = (LinearLayout) findViewById(R.id.lay_timeline_header_new_2);
        lay3 = (LinearLayout) findViewById(R.id.lay_timeline_header_new_3);

        imvLogo.setOnClickListener(this);

        laySearch.setOnClickListener(this);

        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay3.setOnClickListener(this);

        lay1.setSelected(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        final PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
//        pagerAdapter.addFragment(TLHotPageFragment.createInstance(), "Hot");
//        pagerAdapter.addFragment(TLHotPageFragment.createInstance(), "Hot");
//        pagerAdapter.addFragment(TLHotPageFragment.createInstance(), "Hot");
//        pagerAdapter.addFragment(TLHotPageFragment.createInstance(), "Hot");
//        pagerAdapter.addFragment(TLAllPageFragment.createInstance(), "All");
//        pagerAdapter.addFragment(TLFollowPageFragment.createInstance(), "Follow");
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_main_timeline_home_new:
                //TODO init List;
                break;
            case R.id.imv_timeline_search_new:
                Intent i = new Intent(TimeLineV1Activity.this, InterestSearchActivity.class);
                i.putExtra("navi", 0);
                startActivity(i);
                break;
            case R.id.lay_timeline_header_new_1:
                initTab(0);
                break;
            case R.id.lay_timeline_header_new_2:
                initTab(1);
                break;
            case R.id.lay_timeline_header_new_3:
                initTab(2);
                break;
        }
    }

    private void initTab(int i) {
        lay1.setSelected(false);
        lay2.setSelected(false);
        lay3.setSelected(false);
        switch (i) {
            case 0:
                lay1.setSelected(true);
                break;
            case 1:
                lay2.setSelected(true);
                break;
            case 2:
                lay3.setSelected(true);
                break;
        }
    }

    static class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
