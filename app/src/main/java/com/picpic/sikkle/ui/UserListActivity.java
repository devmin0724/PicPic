package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.UserListItem;
import com.picpic.sikkle.beans.UserListResult;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserListActivity extends Activity implements View.OnClickListener {

    public static final int USER_LIST_LIKE = 30001;
    public static final int USER_LIST_BLOCK = 30002;
    public static final int USER_LIST_ANDTAG = 30003;
    public static final int USER_LIST_MY_FOLLOWER = 30004;
    public static final int USER_LIST_MY_FOLLOWING = 30005;
    public static final int USER_LIST_FOLLOWER = 30006;
    public static final int USER_LIST_FOLLOWING = 30007;
    public static final int USER_LIST_TAG_FOLLOWER = 30008;
    public static final int USER_LIST_AND_TAG_FOLLOWER = 30009;
    public static final int USER_LIST_COMMENT_LIKE = 30010;

    LinearLayout layBack;
    TextView tvTitle;
    ListView list;

    int pageNavi = 0;

    int pagingNavi = 1;

    private boolean lastItemVisibleFlag = false;
    String postId = "", userId = "", tagId = "";
    UserListResult m_ResultList = null;
    ArrayAdapter<UserListItem> m_ListAdapter = null;
    ArrayList<Boolean> isLikes = null;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_user_list);

        pageNavi = getIntent().getExtras().getInt("pageNavi");

        initViews();
    }
    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }
    private void initViews() {
        layBack = (LinearLayout) findViewById(R.id.lay_user_list_back);
        tvTitle = (TextView) findViewById(R.id.tv_user_list_title);

        layBack.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list_user_list);

        switch (pageNavi) {
            case USER_LIST_LIKE:
                tvTitle.setText(getResources().getString(R.string.timeline_like_user));
                postId = getIntent().getExtras().getString("postId");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("좋아하는 사람 페이지" + "_" + pageNavi + "_" + postId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("좋아하는 사람 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getPostLikeUserList();
                break;
            case USER_LIST_COMMENT_LIKE:
                tvTitle.setText(getResources().getString(R.string.timeline_like_user));
                postId = getIntent().getExtras().getString("postId");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("댓글 좋아하는 사람 페이지" + "_" + pageNavi + "_" + postId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("댓글 좋아하는 사람 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getCommentLikeUserList();
                break;
            case USER_LIST_BLOCK:
                tvTitle.setText(getResources().getString(R.string.settings_protect_id_manage));
                break;
            case USER_LIST_ANDTAG:
                tvTitle.setText(getResources().getString(R.string.join_people));
                tagId = getIntent().getExtras().getString("tagId");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("이은 사람 페이지" + "_" + pageNavi + "_" + tagId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("이은 사람 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getAndTagFollowUserList();
                break;
            case USER_LIST_MY_FOLLOWER:
                tvTitle.setText(getResources().getString(R.string.follow_me_list));
                userId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로워 리스트 페이지" + "_" + pageNavi + "_" + userId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로워 리스트 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getFollowMeList();
                break;
            case USER_LIST_MY_FOLLOWING:
                tvTitle.setText(getResources().getString(R.string.follow_he_list));
                userId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로잉 리스트 페이지" + "_" + pageNavi + "_" + userId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로잉 리스트 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getFollowingMeList();
                break;
            case USER_LIST_FOLLOWER:
                //TODO 나를 한거
                tvTitle.setText(getResources().getString(R.string.user_follow));
                userId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로워 페이지" + "_" + pageNavi + "_" + userId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로워 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getFollowMeList();
                break;
            case USER_LIST_FOLLOWING:
                // TODO 내가 한거
                tvTitle.setText(getResources().getString(R.string.user_following));
                userId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로잉 페이지" + "_" + pageNavi + "_" + userId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("팔로잉 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getFollowingMeList();
                break;
            case USER_LIST_TAG_FOLLOWER:
                tvTitle.setText(getResources().getString(R.string.tag_following));
                tagId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("태그 팔로워들 페이지" + "_" + pageNavi + "_" + tagId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("태그 팔로워들 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getTagFollowUserList();
            case USER_LIST_AND_TAG_FOLLOWER:
                tvTitle.setText("&" + getResources().getString(R.string.tag_join));
                tagId = getIntent().getExtras().getString("id");
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("앤드태그 참여자 페이지" + "_" + pageNavi + "_" + tagId);
                AppController.t.send(new HitBuilders.AppViewBuilder().build());

                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.setScreenName("앤드태그 참여자 페이지");
                AppController.t.send(new HitBuilders.AppViewBuilder().build());
                getAndTagFollowUserList();
                break;
        }

    }

    private void getTagFollowUserList() {

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_id", tagId);
        params.put("page", pagingNavi + "");

        //TODO 페이징 15개

        AppController.apiTaskNew = new APITaskNew(this, params, 409, stmFollowResult);
        AppController.apiTaskNew.execute();

    }

    private void getAndTagFollowUserList() {
        StringTransMethod stmFollowUserList = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);

                    if (j.getInt("result") == 0) {

                        JSONArray jarr = new JSONArray(j.getString("data"));
                        UserListResult ulr = new UserListResult();
                        JSONObject jd;
                        UserListItem uli;
                        for (int i = 0; i < jarr.length(); i++) {
                            jd = jarr.getJSONObject(i);

                            uli = new UserListItem();

                            uli.setEmail(jd.getString("email"));
                            uli.setName(jd.getString("id"));
                            uli.setUrl(jd.getString("url"));

                            if (jd.getString("follow_yn").equals("Y")) {
                                uli.setIsfriend(true);
                            } else {
                                uli.setIsfriend(false);
                            }

                            ulr.add(uli);

                        }

                        updateResultList(ulr);

                    }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_id", tagId);
        params.put("page", pagingNavi + "");

        //TODO 페이징 15명

        AppController.apiTaskNew = new APITaskNew(this, params, 407, stmFollowUserList);
        AppController.apiTaskNew.execute();

    }

    private void getPostLikeUserList() {
        StringTransMethod stmLikeUserList = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);

                    Log.e("kkkkkkzzkzkzkzkzk", j.toString());

                    if (j.getInt("result") == 0) {

                        JSONArray jarr = new JSONArray(j.getString("data"));
                        UserListResult ulr = new UserListResult();
                        JSONObject jd;
                        UserListItem uli;
                        for (int i = 0; i < jarr.length(); i++) {
                            jd = jarr.getJSONObject(i);

                            uli = new UserListItem();

                            uli.setEmail(jd.getString("email"));
                            uli.setName(jd.getString("id"));
                            uli.setUrl(jd.getString("profile_picture"));

                            if (jd.getString("follow_yn").equals("Y")) {
                                uli.setIsfriend(true);
                            } else {
                                uli.setIsfriend(false);
                            }

                            ulr.add(uli);

                        }

                        updateResultList(ulr);

                    }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_reply_id", postId);
        params.put("page", pagingNavi + "");
        params.put("type", "P");

        //TODO 페이징 15개씩

        AppController.apiTaskNew = new APITaskNew(this, params, 320, stmLikeUserList);
        AppController.apiTaskNew.execute();

    }
    private void getCommentLikeUserList() {
        StringTransMethod stmLikeUserList = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);

                    Log.e("kkkkkkzzkzkzkzkzk", j.toString());

                    if (j.getInt("result") == 0) {

                        JSONArray jarr = new JSONArray(j.getString("data"));
                        UserListResult ulr = new UserListResult();
                        JSONObject jd;
                        UserListItem uli;
                        for (int i = 0; i < jarr.length(); i++) {
                            jd = jarr.getJSONObject(i);

                            uli = new UserListItem();

                            uli.setEmail(jd.getString("email"));
                            uli.setName(jd.getString("id"));
                            uli.setUrl(jd.getString("profile_picture"));

                            if (jd.getString("follow_yn").equals("Y")) {
                                uli.setIsfriend(true);
                            } else {
                                uli.setIsfriend(false);
                            }

                            ulr.add(uli);

                        }

                        updateResultList(ulr);

                    }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_reply_id", postId);
        params.put("page", pagingNavi + "");
        params.put("type", "C");

        //TODO 페이징 15개씩

        AppController.apiTaskNew = new APITaskNew(this, params, 320, stmLikeUserList);
        AppController.apiTaskNew.execute();

    }

    private void getFollowMeList() {

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", userId);
        params.put("page", pagingNavi + "");

        //TODO 페이징 15개

        AppController.apiTaskNew = new APITaskNew(this, params, 408, stmFollowResult);
        AppController.apiTaskNew.execute();

    }

    private void getFollowingMeList() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", userId);
        params.put("page", pagingNavi + "");

        //TODO 페이징 15개

        AppController.apiTaskNew = new APITaskNew(this, params, 404, stmFollowResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmFollowResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject j = new JSONObject(result);

                if(j.getInt("result") == 0){
                    if (j.getString("data") != null) {
                        JSONArray jarr = new JSONArray(j.getString("data"));
                        UserListResult ulr = new UserListResult();
                        JSONObject jd;
                        UserListItem uli;
                        for (int i = 0; i < jarr.length(); i++) {
                            jd = jarr.getJSONObject(i);

                            uli = new UserListItem();
                            uli.setEmail(jd.getString("email"));
                            uli.setName(jd.getString("id"));
                            uli.setUrl(jd.getString("url"));
                            if (jd.getString("follow_yn").equals("Y")) {
                                uli.setIsfriend(true);
                            } else {
                                uli.setIsfriend(false);
                            }

                            ulr.add(uli);

                        }

                        updateResultList(ulr);
                    }
                }else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_user_list_back:
                finish();
                break;
        }
    }

    protected boolean updateResultList(UserListResult resultList) {

        isLikes = new ArrayList<>();

        for (int i = 0; i < resultList.size(); i++) {
            isLikes.add(false);
        }

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();

                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new UserListAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);

            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO 터치했을때 행동
                    }
                });

            }
        });
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    Log.e("userListCount", count + "");
                    int page = view.getCount() / 15;
                    if (view.getCount() % 15 == 0) {
                        pagingNavi = page + 1;
                        switch (pageNavi) {
                            case USER_LIST_LIKE:
                                getPostLikeUserList();
                                break;
                            case USER_LIST_COMMENT_LIKE :
                                getCommentLikeUserList();
                                break;
                            case USER_LIST_BLOCK:
                                break;
                            case USER_LIST_ANDTAG:
                                getAndTagFollowUserList();
                                break;
                            case USER_LIST_MY_FOLLOWER:
                                getFollowMeList();
                                break;
                            case USER_LIST_MY_FOLLOWING:
                                getFollowingMeList();
                                break;
                            case USER_LIST_FOLLOWER:
                                //TODO 나를 한거
                                getFollowMeList();
                                break;
                            case USER_LIST_FOLLOWING:
                                // TODO 내가 한거
                                getFollowingMeList();
                                break;
                            case USER_LIST_TAG_FOLLOWER:
                                getTagFollowUserList();
                            case USER_LIST_AND_TAG_FOLLOWER:
                                getAndTagFollowUserList();
                                break;
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
            }
        });
        return true;
    }
    class UserListAdapter extends ArrayAdapter<UserListItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public UserListAdapter(Context ctx, int txtViewId, List<UserListItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_user_list, null);

                CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_user_list);

                TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_user_list_row_name);
                final TextView tvIsBlock = (TextView) convertView.findViewById(R.id.tv_user_list_row_is_block);

                final ImageView imvCheck1 = (ImageView) convertView.findViewById(R.id.imv_user_list_check1);
                final ImageView imvCheck2 = (ImageView) convertView.findViewById(R.id.imv_user_list_check2);

                final UserListItem model = getItem(pos);

                switch (pageNavi) {
                    case USER_LIST_FOLLOWER:
                    case USER_LIST_FOLLOWING:
                    case USER_LIST_LIKE:
                    case USER_LIST_COMMENT_LIKE:
                    case USER_LIST_TAG_FOLLOWER:
                    case USER_LIST_AND_TAG_FOLLOWER:
                        imvCheck1.setVisibility(View.VISIBLE);
                        imvCheck2.setVisibility(View.GONE);
                        tvIsBlock.setVisibility(View.GONE);

                        if (model.isfriend()) {
                            imvCheck1.setSelected(true);
                        } else {
                            imvCheck1.setSelected(false);
                        }

                        tvUserId.setText(model.getName());

                        if (model.getUrl().equals("")) {
                            cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                        } else {
                            cimvPro.setImageURLString(AppController.URL + model.getUrl());
                        }

                        if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                            imvCheck1.setVisibility(View.GONE);
                        } else {
                            imvCheck1.setVisibility(View.VISIBLE);
                        }

                        break;
                    case USER_LIST_BLOCK:
                        imvCheck1.setVisibility(View.GONE);
                        imvCheck2.setVisibility(View.VISIBLE);
                        tvIsBlock.setVisibility(View.VISIBLE);

                        if (model.isfriend()) {
                            imvCheck2.setSelected(true);
                        } else {
                            imvCheck2.setSelected(false);
                        }

                        tvUserId.setText(model.getName());

                        if (model.getUrl().equals("")) {
                            cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                        } else {
                            cimvPro.setImageURLString(AppController.URL + model.getUrl());
                        }

                        if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                            imvCheck2.setVisibility(View.GONE);
                        } else {
                            imvCheck2.setVisibility(View.VISIBLE);
                        }
                        break;
                    case USER_LIST_ANDTAG:
                        imvCheck1.setVisibility(View.VISIBLE);
                        imvCheck2.setVisibility(View.GONE);
                        tvIsBlock.setVisibility(View.GONE);

                        if (model.isfriend()) {
                            imvCheck1.setSelected(true);
                        } else {
                            imvCheck1.setSelected(false);
                        }

                        tvUserId.setText(model.getName());

                        if (model.getUrl().equals("")) {
                            cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                        } else {
                            cimvPro.setImageURLString(AppController.URL + model.getUrl());
                        }

                        if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                            imvCheck1.setVisibility(View.GONE);
                        } else {
                            imvCheck1.setVisibility(View.VISIBLE);
                        }
                        break;
                    default:
                        imvCheck1.setVisibility(View.VISIBLE);
                        imvCheck2.setVisibility(View.GONE);
                        tvIsBlock.setVisibility(View.GONE);

                        if (model.isfriend()) {
                            imvCheck1.setSelected(true);
                        } else {
                            imvCheck1.setSelected(false);
                        }

                        tvUserId.setText(model.getName());

                        if (model.getUrl().equals("")) {
                            cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                        } else {
                            cimvPro.setImageURLString(AppController.URL + model.getUrl());
                        }

                        if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                            imvCheck1.setVisibility(View.GONE);
                        } else {
                            imvCheck1.setVisibility(View.VISIBLE);
                        }
                        break;
                }

                imvCheck1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringTransMethod stmFollow = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        if (jd.getString("follow").equals("Y")) {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + model.getEmail()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                            imvCheck1.setSelected(true);
//                                            isLikes.set(pos, true);
                                        } else if (jd.getString("follow").equals("N")) {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + model.getEmail()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                            imvCheck1.setSelected(false);
//                                            isLikes.set(pos, false);
                                        } else {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                        }
                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };

                        try {

                            JSONObject tempJ = new JSONObject();
                            tempJ.put("email", model.getEmail());

                            JSONArray jarr = new JSONArray();
                            jarr.put(tempJ);

                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
                            params.put("type", "N");

                            AppController.apiDataTaskNew = new APIDataTaskNew(UserListActivity.this, params, 402, stmFollow);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                //cut_yn
                imvCheck2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringTransMethod stmBlock = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        if (jd.getString("cut_yn").equals("Y")) {
                                            imvCheck2.setSelected(false);
                                            tvIsBlock.setText(getResources().getString(R.string.block_cancel));
//                                            isLikes.set(pos, false);
                                        } else if (jd.getString("follow").equals("N")) {
                                            imvCheck2.setSelected(true);
                                            tvIsBlock.setText(getResources().getString(R.string.block));
//                                            isLikes.set(pos, true);
                                        } else {

                                        }
                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("user_id", model.getEmail());

                        AppController.apiDataTaskNew = new APIDataTaskNew(UserListActivity.this, params, 216, stmBlock);
                        AppController.apiDataTaskNew.execute();

                    }
                });
                cimvPro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppController.getSp().getString("id", "").equals(model.getName())) {
                            startActivity(new Intent(UserListActivity.this, MyPageV2Activity.class));
                            finish();
                        } else {
                            Intent i = new Intent(UserListActivity.this, UserFeedForIdActivity.class);
                            i.putExtra("id", model.getName());
                            startActivity(i);
                        }
                    }
                });
                tvUserId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppController.getSp().getString("id", "").equals(model.getName())) {
                            startActivity(new Intent(UserListActivity.this, MyPageV2Activity.class));
                            finish();
                        } else {
                            Intent i = new Intent(UserListActivity.this, UserFeedForIdActivity.class);
                            i.putExtra("id", model.getName());
                            startActivity(i);
                        }
                    }
                });
                
            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}