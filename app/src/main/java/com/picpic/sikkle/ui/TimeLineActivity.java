package com.picpic.sikkle.ui;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.picpic.sikkle.R;
import com.picpic.sikkle.Test3Activity;
import com.picpic.sikkle.fragment.NewTimelineAllFragment;
import com.picpic.sikkle.fragment.TimelineAllFragment;
import com.picpic.sikkle.fragment.TimelineFollowFragment;
import com.picpic.sikkle.fragment.TimelineHotFragment;
import com.picpic.sikkle.ui.login_join.FriendResultActivity;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.NonScrollViewPager;
import com.picpic.sikkle.widget.floating.FloatingActionLayout;
import com.picpic.sikkle.widget.supertooltips.ToolTipRelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

//import com.handmark.pulltorefresh.library.PullToRefreshBase;
//import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class TimeLineActivity extends FragmentActivity implements View.OnClickListener {
    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;
    public static final int COMMENT_RETURN = 1006;
    private static final int TIMELINE_RETURN = 1001;
    private static final int FACE_SLICE_RETURN = 1007;
    public static ToolTipRelativeLayout toolTipRelativeLayout;
    public static Activity ac;
    //    LinearLayout layIntro, imvHome, imvAlarm;
//    LinearLayout imvHome;
//    ImageView imvHome;
    ImageView imvNoti;
    //    FrameLayout tabHot, tabFresh, tabFollow;
    //    ImageView imvChatting;
//    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch, layEvent;
//    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch;
    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage;
    //    public static NonScrollViewPager vp;
    public static NonScrollViewPager vp;
    LinearLayout layHeader;
    //    SmartTabLayout stl;
    int navi = 0;
    final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();


    TimelineHotFragment frag1;
    TimelineAllFragment frag2;
    TimelineFollowFragment frag3;

    public static void showToolTip() {

//        toolTipRelativeLayout.removeAllViews();
//
//        ToolTip toolTip = new ToolTip()
//                .withContentView(LayoutInflater.from(ac).inflate(R.layout.customtooltip, null))
//                .withAnimationType(ToolTip.AnimationType.FROM_MASTER_VIEW)
//                .withColor(0xffffe766);
//        ToolTipView ttv = toolTipRelativeLayout.showToolTipForView(toolTip, ac.findViewById(R.id.lay_timeline_camera));
//        ttv.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
//            @Override
//            public void onToolTipViewClicked(ToolTipView toolTipView) {
////                Toast.makeText(getApplicationContext(), "ㅋㅋㅋ", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        moveTaskToBack(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_new_timeline);

//        startActivity(new Intent(TimeLineActivity.this, Test3Activity.class));
//        startActivity(new Intent(TimeLineActivity.this, MyPageV1Activity.class));

        ac = this;
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 페이지_v1");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

//        tabHot = (FrameLayout) findViewById(R.id.lay_timeline_tab_1);
//        tabFresh = (FrameLayout) findViewById(R.id.lay_timeline_tab_2);
//        tabFollow = (FrameLayout) findViewById(R.id.lay_timeline_tab_3);
//
//        tabHot.setOnClickListener(this);
//        tabFresh.setOnClickListener(this);
//        tabFollow.setOnClickListener(this);

//        layHeader = (LinearLayout)findViewById(R.id.lay_timeline_header);

//        final FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
//                getSupportFragmentManager(), FragmentPagerItems.with(this)
//                .add(R.string.timeline_hot, TimelineHotFragment.class)
//                .add(R.string.timeline_fresh, TimelineAllFragment.class)
//                .add(R.string.timeline_follow, TimelineFollowFragment.class)
//                .create());

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());

        vp = (NonScrollViewPager) findViewById(R.id.viewpager_new_timeline);
        vp.setAdapter(adapter);
        vp.setPagingDisabled();
        vp.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
//                page.setAnimation(AnimationUtils.loadAnimation(TimeLineActivity.this, 0));
            }
        });

//        vp.setm
        vp.setOffscreenPageLimit(6);

//        frag1 = new TimelineHotFragment();
//        frag2 = new TimelineAllFragment();
//        frag3 = new TimelineFollowFragment();

        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                switch (position) {
//                    case 0:
//                        tabHot.setSelected(true);
//                        tabFresh.setSelected(false);
//                        tabFollow.setSelected(false);
//                        break;
//                    case 1:
//                        tabHot.setSelected(false);
//                        tabFresh.setSelected(true);
//                        tabFollow.setSelected(false);
//                        break;
//                    case 2:
//                        tabHot.setSelected(false);
//                        tabFresh.setSelected(false);
//                        tabFollow.setSelected(true);
//                        break;
//                }
            }

            @Override
            public void onPageSelected(int position) {
//                switch (position) {
//                    case 0:
//                        frag1.getData2();
//                        break;
//                    case 1:
//                        frag2.getData();
//                        break;
//                    case 2:
//                        frag3.getData2();
//                        break;
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        stl = (SmartTabLayout) findViewById(R.id.viewpagertab_new_timeline);
//        stl.setViewPager(vp);

        toolTipRelativeLayout = (ToolTipRelativeLayout) findViewById(R.id.tooltipRelativeLayout_new_timeline);
        imvNoti = (ImageView) findViewById(R.id.imv_timeline_noti);
//        imvHome = (ImageView) findViewById(R.id.imv_main_timeline_home);
//        imvAlarm = (LinearLayout) findViewById(R.id.imv_main_timeline_top_alarm);
//        imvChatting = (ImageView) findViewById(R.id.imv_main_timeline_top_chatting);
//        btnIntro = (Button) findViewById(R.id.btn_timeline_intro);
//        laySearch = (LinearLayout) findViewById(R.id.imv_timeline_search);
//        laySearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                overridePendingTransition(0, 0);
//                Intent ii = new Intent(TimeLineActivity.this, InterestSearchActivity.class);
//                ii.putExtra("navi", 0);
//                startActivity(ii);
//            }
//        });
        layTimeline = (LinearLayout) findViewById(R.id.lay_timeline_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_timeline_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_timeline_camera);
        layAndTag = (LinearLayout) findViewById(R.id.lay_timeline_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_timeline_mypage);

//        imvHome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TimelineFollowFragment.getData2();
//                TimelineAllFragment.getData();
//                TimelineHotFragment.getData2();
//                layHeader.startAnimation();
//                TranslateAnimation ta = new TranslateAnimation(0f,0f,0f,100f);
//                ta.setFillAfter(true);
//                layHeader.animate().setInterpolator(mInterpolator)
//                        .setDuration(200)
//                        .setListener(new Animator.AnimatorListener() {
//                            @Override
//                            public void onAnimationStart(Animator animation) {
//                                layHeader.setVisibility(View.VISIBLE);
//                            }
//
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                layHeader.setVisibility(View.GONE);
//                            }
//
//                            @Override
//                            public void onAnimationCancel(Animator animation) {
//
//                            }
//
//                            @Override
//                            public void onAnimationRepeat(Animator animation) {
//
//                            }
//                        })
//                        .translationY(layHeader.getHeight());
//            }
//        });
//        imvAlarm.setOnClickListener(this);
//        imvChatting.setOnClickListener(this);
//        btnIntro.setOnClickListener(this);

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layAndTag.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        layTimeline.setSelected(true);

//        layIntro = (LinearLayout) findViewById(R.id.lay_timeline_intro);
//        layIntro.setVisibility(View.GONE);

        layTimeline.setSelected(true);

    }

    @Override
    protected void onResume() {
        notiCheck();
        if (vp != null) {
//            ((TimelineHotFragment)vp.getAdapter().get).updateList();
            if (frag1 != null) {
//                frag1.updateList();
            }
            if (frag2 != null) {
//                frag2.updateList();
            }
            if (frag3 != null) {
//                frag3.updateList();
            }

        }
        super.onResume();
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(TimeLineActivity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
                //TODO 재로딩
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
                                //TODO 재로딩
                                break;
                            case 3:
                                Intent i = new Intent(TimeLineActivity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                //TODO 재로딩
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                //TODO 재로딩
                break;
            case FACE_SLICE_RETURN:
                //TODO 재로딩
                break;
        }
    }

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
//        AppController.videoExe.shutdownNow();

        android.util.Log.d("TAG", "TOTAL MEMORY : " + (Runtime.getRuntime().totalMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "MAX MEMORY : " + (Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "FREE MEMORY : " + (Runtime.getRuntime().freeMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "ALLOCATION MEMORY : " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + "MB");
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_main_timeline_home:
                //TODO refresh ListView
//                list.getRefreshableView().smoothScrollToPosition(0);
                break;
            case R.id.imv_main_timeline_top_alarm:
                startActivity(new Intent(TimeLineActivity.this, NotificationActivity.class));
                break;
            case R.id.imv_main_timeline_top_chatting:
//                Toast.makeText(this, "채팅 준비중", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_timeline_intro:
                Intent i = new Intent(TimeLineActivity.this, FriendResultActivity.class);
                i.putExtra("navi", 3);
                startActivityForResult(i, POST_WRITE_RETURN);
                break;
            case R.id.lay_timeline_timeline:
                break;
            case R.id.lay_timeline_interest:
//                i = new Intent(TimeLineActivity.this, InterestFeedActivity.class);
                i = new Intent(TimeLineActivity.this, InterestFeedV1Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(i, POST_WRITE_RETURN);
                finish();
                break;
            case R.id.lay_timeline_camera:
                System.gc();
//                Intent gifCamera = new Intent(TimeLineActivity.this, GIFCameraActivity.class);
//                Intent gifCamera = new Intent(TimeLineActivity.this, GIFCameraFilmActivity.class);
//                gifCamera.putExtra("navi", 0);
////                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
//                startActivityForResult(gifCamera, POST_WRITE_RETURN);
                startActivity(new Intent(TimeLineActivity.this, CameraPopUp.class));
                break;
            case R.id.lay_timeline_noti:
                i = new Intent(TimeLineActivity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_mypage:
                i = new Intent(TimeLineActivity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }

    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return frag1;
                case 1:
                    return frag2;
                case 2:
                    return frag3;
                default:
//                    return new TimelineAllFragment();
                    return new NewTimelineAllFragment();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
