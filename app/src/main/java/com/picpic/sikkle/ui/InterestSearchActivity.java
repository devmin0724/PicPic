package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.fragment.InterestSearchTagFragment;
import com.picpic.sikkle.fragment.InterestSearchUserFragment;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.widget.CircleImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InterestSearchActivity extends FragmentActivity implements View.OnClickListener {

    AutoCompleteTextView edtInput;
    //    ImageView imvIcon, imvBG;
    LinearLayout imvBack;
    //    FrameLayout lay1, lay2, lay3;
    FrameLayout lay1, lay2;

    int pageNavi = 1;
    int searchNavi = 0;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {
                //TODO 사용자 검색
                if (s.length() == 1) {
                    if (s.toString().equals("_")) {
                        edtInput.setText("");
                    } else {
                        String str = s.toString();
                        String tempStr = "";
                        int strLength = str.length();
                        if (str.substring(strLength - 1, strLength).equals(" ")) {
                            tempStr = str.substring(0, strLength - 1);
                            tempStr = tempStr.replace("#", "").replace("@", "").replace("&", "");
                        } else {
                            tempStr = str;
                            tempStr = tempStr.replace("#", "").replace("@", "").replace("&", "");
                        }

                        str = str.replace(" ", "");
                        switch (searchNavi) {
                            case 0:
                                InterestSearchTagFragment.getTagSearchData(true, tempStr, 1);
//                                getTagSearch(str);
                                break;
                            case 1:
                                InterestSearchUserFragment.getUserSearchData(true, tempStr, 1);
//                                getUSerTagSearch(str);
                                break;
//                            case 2:
//                                InterestSearchAndFragment.getAndTagSearchData(true, tempStr, 1);
////                                getAndTagSearch(str);
//                                break;
                        }
                    }
                } else {
                    String str = s.toString();
                    String tempStr = "";
                    int strLength = str.length();
                    if (str.substring(strLength - 1, strLength).equals(" ")) {
                        tempStr = str.substring(0, strLength - 1);
                        tempStr = tempStr.replace("#", "").replace("@", "").replace("&", "");
                    } else {
                        tempStr = str;
                        tempStr = tempStr.replace("#", "").replace("@", "").replace("&", "");
                    }

                    switch (searchNavi) {
                        case 0:
                            InterestSearchTagFragment.getTagSearchData(true, tempStr, 1);
//                            getTagSearch(str);
                            break;
                        case 1:
                            InterestSearchUserFragment.getUserSearchData(true, tempStr, 1);
//                            getUSerTagSearch(str);
                            break;
//                        case 2:
//                            InterestSearchAndFragment.getAndTagSearchData(true, tempStr, 1);
////                            getAndTagSearch(str);
//                            break;
                    }
                }

            } else {
                switch (searchNavi) {
                    case 0:
                        InterestSearchTagFragment.getTagSearchData(true, "", 0);
//                            getTagSearch(str);
                        break;
                    case 1:
                        InterestSearchUserFragment.getUserSearchData(true, "", 0);
//                            getUSerTagSearch(str);
                        break;
//                    case 2:
//                        InterestSearchAndFragment.getAndTagSearchData(true, "", 0);
////                            getAndTagSearch(str);
//                        break;
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_interest_search);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("검색 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void initViews() {

        lay1 = (FrameLayout) findViewById(R.id.lay_search_tab_title2_1);
        lay1.setOnClickListener(this);
        lay2 = (FrameLayout) findViewById(R.id.lay_search_tab_title2_2);
        lay2.setOnClickListener(this);
//        lay3 = (FrameLayout) findViewById(R.id.lay_tab_title3_3);
//        lay3.setOnClickListener(this);

        mViewPager = (ViewPager) findViewById(R.id.vp_interest_search);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.interest_search_tag, InterestSearchTagFragment.class)
                .add(R.string.interest_search_user, InterestSearchUserFragment.class)
//                .add(R.string.interest_search_and, InterestSearchAndFragment.class)
                .create());

        mViewPager.setAdapter(adapter);

//        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setActivated(true);
        mViewPager.setAlwaysDrawnWithCacheEnabled(true);
        mViewPager.setFadingEdgeLength(10);
        mViewPager.setHorizontalFadingEdgeEnabled(true);
        mViewPager.setSaveEnabled(true);

//        imvIcon = (ImageView) findViewById(R.id.imv_interest_search_icon);
        imvBack = (LinearLayout) findViewById(R.id.imv_search_back);
//        imvBG = (ImageView) findViewById(R.id.imv_interest_search_top_bg);
//        imvIcon = (ImageView) findViewById(R.id.imv_interest_search_icon);

        imvBack.setOnClickListener(this);
//        imvIcon.setOnClickListener(this);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
//                        imvIcon.setImageResource(R.drawable.icon_interst_hash);
//                        imvBG.setImageResource(R.drawable.bg_interest_bg1);
                        searchNavi = 0;
                        edtInput.setHint(R.string.search_tag);
                        edtInput.setText("");
//                        InterestSearchTagFragment.getTagSearchData(true, edtInput.getText().toString());
                        initLays(0);
                        break;
                    case 1:
//                        imvIcon.setImageResource(R.drawable.icon_interst_user);
//                        imvBG.setImageResource(R.drawable.bg_interst_bg2);
                        searchNavi = 1;
                        edtInput.setText("");
                        edtInput.setHint(R.string.search_user);
                        InterestSearchUserFragment.getHotUserData(true);
                        initLays(1);
                        break;
//                    case 2:
//                        imvIcon.setImageResource(R.drawable.icon_interst_and);
//                        imvBG.setImageResource(R.drawable.bg_interst_bg3);
//                        searchNavi = 2;
//                        edtInput.setText("");
//                        edtInput.setHint(R.string.search_and_tag);
//                        initLays(2);
//                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab_interest_search);
//        viewPagerTab.setSelectedIndicatorColors(0xff697fff);
//        viewPagerTab.setDividerColors(0x00ffffff);
//        viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {
//            @Override
//            public View createTabView(ViewGroup viewGroup, int i, PagerAdapter pagerAdapter) {
//                View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_title3, null, false);
//
//                TextView tv = (TextView) v.findViewById(R.id.tv_tab_title3);
//                LinearLayout lay = (LinearLayout) v.findViewById(R.id.lay_tab_title3);
//
//                switch (i) {
//                    case 0:
//                        tv.setText(getResources().getString(R.string.interest_search_tag));
//                        break;
//                    case 1:
//                        tv.setText(getResources().getString(R.string.interest_search_user));
//                        break;
//                    default:
//                        tv.setText(getResources().getString(R.string.interest_search_and));
//                        break;
//                }
//
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(MinUtils.screenWidth / 3, LinearLayout.LayoutParams.MATCH_PARENT);
//                lay.setLayoutParams(params);
//
//                return v;
//            }
//        });
//
//        viewPagerTab.setViewPager(mViewPager);
//
//        viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                switch (position) {
//                    case 0:
//                        imvIcon.setImageResource(R.drawable.icon_interst_hash);
//                        imvBG.setImageResource(R.drawable.bg_interest_bg1);
//                        searchNavi = 0;
//                        edtInput.setHint(R.string.search_tag);
//                        edtInput.setText("");
////                        InterestSearchTagFragment.getTagSearchData(true, edtInput.getText().toString());
//                        break;
//                    case 1:
//                        imvIcon.setImageResource(R.drawable.icon_interst_user);
//                        imvBG.setImageResource(R.drawable.bg_interst_bg2);
//                        searchNavi = 1;
//                        edtInput.setText("");
//                        edtInput.setHint(R.string.search_user);
////                        clearListAll();
//                        InterestSearchUserFragment.getHotUserData(true);
////                        if(InterestSearchUserFragment.)
//                        break;
//                    case 2:
//                        imvIcon.setImageResource(R.drawable.icon_interst_and);
//                        imvBG.setImageResource(R.drawable.bg_interst_bg3);
//                        searchNavi = 2;
//                        edtInput.setText("");
//                        edtInput.setHint(R.string.search_and_tag);
//                        break;
//                }
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

        edtInput = (AutoCompleteTextView) findViewById(R.id.edt_search);
        edtInput.addTextChangedListener(tw);
        edtInput.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter().getItem(position) != null) {
                    String tagName = ((TagAdapter) parent.getAdapter()).getItem(position);
                    String split = "";
                    switch (searchNavi) {
                        case 0:
                            split = "#";
                            InterestSearchTagFragment.getTagSearchData(true, tagName.replace(" ", ""), 1);
                            break;
                        case 1:
                            split = "@";
                            InterestSearchUserFragment.getUserSearchData(true, tagName.replace(" ", ""), 1);
                            break;
//                        case 2:
//                            split = "&";
//                            InterestSearchAndFragment.getAndTagSearchData(true, tagName.replace(" ", ""), 1);
//                            break;
                    }
                    edtInput.setText(tagName);
                    edtInput.setSelection(edtInput.getText().toString().length());
                }
            }
        });
        mViewPager.setCurrentItem(getIntent().getExtras().getInt("navi"));
    }

    private void initLays(int a) {
        lay1.setSelected(false);
        lay2.setSelected(false);
//        lay3.setSelected(false);
        switch (a) {
            case 0:
                lay1.setSelected(true);
                break;
            case 1:
                lay2.setSelected(true);
                break;
//            case 2:
//                lay3.setSelected(true);
//                break;
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_search_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_search_tab_title2_1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.lay_search_tab_title2_2:
                mViewPager.setCurrentItem(1);
                break;
//            case R.id.lay_tab_title3_3:
//                mViewPager.setCurrentItem(2);
//                break;
        }
    }

    class TagAdapter extends ArrayAdapter<String> {

        int typeNavi = 0;
        ArrayList<TagListItem> arr;
        LayoutInflater m_LayoutInflater = null;

        public TagAdapter(Context context, int resource, List<String> objects, int navi, ArrayList<TagListItem> arrayList) {
            super(context, resource, objects);
            typeNavi = navi;
            arr = arrayList;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position) + " ";
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            View v = convertView;

            final TagListHolder holder;

            if (v == null) {
                if (m_LayoutInflater == null)
                    m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                v = m_LayoutInflater.inflate(R.layout.row_tag_list, null);
                holder = new TagListHolder();

                holder.cimv = (CircleImageView) v.findViewById(R.id.cimv_tag_list_row);

                holder.tvTag = (TextView) v.findViewById(R.id.tv_tag_list_row);

                v.setTag(holder);

            } else {
                holder = (TagListHolder) v.getTag();
            }

            final String model = getItem(pos);
            //TODO 타임라인 데이터 insert
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.tvTag.getLayoutParams();
            switch (typeNavi) {
                case 0:
                    params.leftMargin = MinUtils.dp2px(43);
                    holder.cimv.setVisibility(View.GONE);
                    holder.tvTag.setText("#" + model);
                    break;
                case 1:
                    params.leftMargin = MinUtils.dp2px(0);
                    holder.cimv.setVisibility(View.VISIBLE);
                    holder.cimv.setImageURLString(AppController.URL + arr.get(pos).getTag_url());
                    holder.tvTag.setText("@" + model);
                    break;
//                case 2:
//                    holder.cimv.setVisibility(View.GONE);
//                    holder.tvTag.setText("&" + model);
//                    break;
            }


            return v;
        }

        class TagListHolder {
            TextView tvTag;
            CircleImageView cimv;
        }


    }

}
