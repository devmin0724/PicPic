package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.fragment.NewTimelineAllFragment;
import com.picpic.sikkle.fragment.NewTimelineFollowFragment;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.ui.popup.SharePopupActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageRun;
import com.picpic.sikkle.utils.MinThumbImageRun;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.utils.cache.GIFSingleLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTag;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;

public class SinglePostContentActivity extends Activity implements View.OnClickListener {

    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;
    public static final int COMMENT_RETURN = 1006;

    ImageView imvLike, imvAniLike;
    TextView tvId, tvPlayCnt, tvBody, tvTime, tvLikeCnt, tvComCount, tvComId, tvComBody, tvAndTagTitle, tvAndTagBody;
    CircleImageView cimv, cimvCom;
    BoundableOfflineImageView bimv;
    LinearLayout layCom, layAndTag, imvBack, imvComment, imvShare, imvMore, layLike;
    GIFSingleLoader gifLoader;
    ImageLoader imgLoader;

    ProgressBar pb;

    String postId = "", tempUrls = "";

    int gifWidth = 200, gifHeight = 200;
    String[] ms;
    String[] ds;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_single_post_content);

        gifLoader = new GIFSingleLoader(this);
        imgLoader = new ImageLoader(this);

        ms = new String[]{
                getResources().getString(R.string.m1), getResources().getString(R.string.m2), getResources().getString(R.string.m3), getResources().getString(R.string.m4), getResources().getString(R.string.m5), getResources().getString(R.string.m6), getResources().getString(R.string.m7), getResources().getString(R.string.m8), getResources().getString(R.string.m9), getResources().getString(R.string.m10), getResources().getString(R.string.m11), getResources().getString(R.string.m12)
        };

        ds = new String[]{
                getResources().getString(R.string.d1), getResources().getString(R.string.d2), getResources().getString(R.string.d3), getResources().getString(R.string.d4), getResources().getString(R.string.d5), getResources().getString(R.string.d6), getResources().getString(R.string.d7), getResources().getString(R.string.d8), getResources().getString(R.string.d9), getResources().getString(R.string.d10), getResources().getString(R.string.d11), getResources().getString(R.string.d12), getResources().getString(R.string.d13), getResources().getString(R.string.d14), getResources().getString(R.string.d15), getResources().getString(R.string.d16), getResources().getString(R.string.d17), getResources().getString(R.string.d18), getResources().getString(R.string.d19), getResources().getString(R.string.d20), getResources().getString(R.string.d21), getResources().getString(R.string.d22), getResources().getString(R.string.d23), getResources().getString(R.string.d24), getResources().getString(R.string.d25), getResources().getString(R.string.d26), getResources().getString(R.string.d27), getResources().getString(R.string.d28), getResources().getString(R.string.d29), getResources().getString(R.string.d30), getResources().getString(R.string.d31)
        };

        initViews();
    }

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }

    private void getData() {

        StringTransMethod stmPostResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    final JSONObject jd = new JSONObject(result);
                    if (jd.getInt("result") == 0) {
                        if (!jd.getString("url").equals("")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        cimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));


                                        tvId.setText(jd.getString("id"));
                                        tvId.setTag(jd.getString("email"));
                                        double playCnt = (double) jd.getInt("play_cnt");
                                        DecimalFormat df = new DecimalFormat("#,##0");
                                        tvPlayCnt.setText(df.format(playCnt) + "");

                                        if (jd.getString("like_yn").equals("Y")) {
                                            imvLike.setSelected(true);
                                        } else {
                                            imvLike.setSelected(false);
                                        }

                                        final String tempUrl = jd.getString("url");

                                        Log.e("url", tempUrl);

                                        int lastIdx = tempUrl.lastIndexOf("_");

                                        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

//                                        bimv.setImageURLString(AppController.URL + jd.getString("url"), AppController.URL + tempName, SinglePostContentActivity.this);
//                                        downloadThumbImage(bimv, AppController.URL + tempName);
                                        imgLoader.DisplayImage(AppController.URL + tempName, bimv, false);

                                        bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
                                            @Override
                                            public void onImageChanged(Drawable d) {

                                                if (d instanceof GifDrawable) {
                                                    pb.setVisibility(View.GONE);
                                                    GifDrawable gd = (GifDrawable) d;

                                                    gifHeight = gd.getIntrinsicHeight();
                                                    gifWidth = gd.getIntrinsicWidth();

                                                    if (gd != null) {
                                                        gd.addAnimationListener(new AnimationListener() {
                                                            @Override
                                                            public void onAnimationCompleted(int loopNumber) {
                                                                AppController.playCountExe.execute(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        new MinUtils.PlayCountSubmit(postId).execute();
                                                                    }
                                                                });
                                                            }

                                                        });
                                                    }
                                                } else {
//                                                    downloadGifbImage(bimv, AppController.URL + tempUrl);
                                                    gifLoader.DisplayImage(AppController.URL + tempUrl, bimv, false);
                                                }

                                            }
                                        });

                                        bimv.setOnTouchListener(new OnSwipeTouchListener(SinglePostContentActivity.this) {
                                            @Override
                                            public void onClick() {
                                                super.onClick();
                                                Drawable d = bimv.getDrawable();
                                                if (d instanceof GifDrawable) {
                                                    try {
                                                        GifDrawable g = (GifDrawable) d;
                                                        if (g.isPlaying()) {
                                                            g.pause();
                                                        } else {
                                                            g.start();
                                                        }
                                                    } catch (NullPointerException e) {

                                                    }
                                                }
                                            }

                                            @Override
                                            public void onDoubleClick() {
                                                super.onDoubleClick();

                                                try {
                                                    if (imvLike.isSelected()) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                imvLike.setSelected(false);
                                                                String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) - 1) + getResources().getString(R.string.timeline_count));
                                                            }
                                                        });
                                                        StringTransMethod stmLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    Log.e("result onselect", result);
                                                                    final JSONObject jd = new JSONObject(result);
                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {

                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    imvLike.setSelected(true);
                                                                                    String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) + 1) + getResources().getString(R.string.timeline_count));
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                        }
                                                                    });
                                                                } catch (JSONException e) {

                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostContentActivity.this, params, 303, stmLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    } else {


                                                        final Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like1);
                                                        final Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like2);
                                                        final Animation ani3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like3);

                                                        imvAniLike.setVisibility(View.VISIBLE);

                                                        ani1.setAnimationListener(new Animation.AnimationListener() {
                                                            @Override
                                                            public void onAnimationStart(Animation animation) {
                                                            }

                                                            @Override
                                                            public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                                                imvAniLike.startAnimation(ani2);
                                                            }

                                                            @Override
                                                            public void onAnimationRepeat(Animation animation) {

                                                            }
                                                        });

                                                        ani2.setAnimationListener(new Animation.AnimationListener() {
                                                            @Override
                                                            public void onAnimationStart(Animation animation) {

                                                            }

                                                            @Override
                                                            public void onAnimationEnd(Animation animation) {
                                                                imvAniLike.startAnimation(ani3);
                                                            }

                                                            @Override
                                                            public void onAnimationRepeat(Animation animation) {

                                                            }
                                                        });

                                                        ani3.setAnimationListener(new Animation.AnimationListener() {
                                                            @Override
                                                            public void onAnimationStart(Animation animation) {

                                                            }

                                                            @Override
                                                            public void onAnimationEnd(Animation animation) {
                                                                imvAniLike.setVisibility(View.GONE);
                                                            }

                                                            @Override
                                                            public void onAnimationRepeat(Animation animation) {

                                                            }
                                                        });

                                                        imvAniLike.startAnimation(ani1);

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                imvLike.setSelected(true);
                                                                String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) + 1) + getResources().getString(R.string.timeline_count));

                                                            }
                                                        });
                                                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    final JSONObject jd = new JSONObject(result);

                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {
                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    imvLike.setSelected(false);
                                                                                    String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) - 1) + getResources().getString(R.string.timeline_count));
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (JSONException e) {
                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostContentActivity.this, params, 302, stmUnLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    }
                                                } catch (NullPointerException e) {

                                                }


                                            }
                                        });


                                        tvId.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String myId = AppController.getSp().getString("email", "");
                                                String tempEmail = tvId.getTag().toString();
                                                if (tempEmail.equals(myId)) {
                                                    startActivity(new Intent(SinglePostContentActivity.this, MyPageV2Activity.class));
                                                    finish();
                                                } else {
                                                    if (!tempEmail.equals("")) {
                                                        Intent i = new Intent(SinglePostContentActivity.this, UserFeedActivity.class);
                                                        i.putExtra("email", tempEmail);
                                                        startActivity(i);
                                                    }
                                                }
                                            }
                                        });
                                        tempUrls = AppController.URL + jd.getString("url");

                                        tvBody.setText(jd.getString("contents"));

                                        String tempDate = jd.getString("day");

                                        String tempTime = "";

                                        //20151130225233
                                        int year = Integer.parseInt(tempDate.substring(0, 4));
                                        int month = Integer.parseInt(tempDate.substring(4, 6));
//            int month = 11;
                                        int day = Integer.parseInt(tempDate.substring(6, 8));
                                        int hour = Integer.parseInt(tempDate.substring(8, 10));
                                        int min = Integer.parseInt(tempDate.substring(10, 12));
                                        int sec = Integer.parseInt(tempDate.substring(12, 14));

                                        Log.e("dd", year + "/" + month + "/" + day + "/" + hour + "/" + min + "/" + sec);

                                        Calendar tempCal = Calendar.getInstance();
                                        Calendar tempNowCal = Calendar.getInstance();

                                        DateFormat stringFormat = new SimpleDateFormat("yyyyMMddHHmmss");

                                        long nowTime, oldTime, intervalTime;

                                        Calendar intervalCal = Calendar.getInstance();

                                        try {
                                            Date nowDate = stringFormat.parse(tempDate);
                                            tempCal.setTime(nowDate);

                                            oldTime = tempCal.getTimeInMillis();
                                            nowTime = System.currentTimeMillis();

                                            intervalTime = nowTime - oldTime;

                                            Log.e("intervalT", (intervalTime / 1000) + "");

                                            long it = intervalTime / 1000;

                                            intervalCal.setTimeInMillis(intervalTime);

                                            Log.e("interval", intervalCal.get(Calendar.YEAR)
                                                    + "/" + (intervalCal.get(Calendar.MONTH))
                                                    + "/" + intervalCal.get(Calendar.DAY_OF_MONTH)
                                                    + "/" + intervalCal.get(Calendar.HOUR_OF_DAY)
                                                    + "/" + intervalCal.get(Calendar.MINUTE)
                                                    + "/" + intervalCal.get(Calendar.SECOND)
                                            );

                                            long m = 60;
                                            long h = 60 * 60;
                                            long d = 60 * 60 * 24;

                                            if (it < m) {
                                                tempTime = getResources().getString(R.string.just_now);
                                            } else {
                                                if (it < h) {
                                                    tempTime = intervalCal.get(Calendar.MINUTE) + getResources().getString(R.string.timeline_before_minute);
                                                } else {
                                                    if (it < d) {
                                                        tempTime = intervalCal.get(Calendar.HOUR_OF_DAY) + getResources().getString(R.string.timeline_before_hour);
                                                    } else {
                                                        if (it < d * 2) {
                                                            tempTime = getResources().getString(R.string.yesterday);
                                                        } else {
                                                            tempTime = year + getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
                                                        }
                                                    }
                                                }
                                            }

                                            Log.e("tempTIme", tempTime);

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

//                                        Date now = new Date();
//                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//                                        String tempC = sdf.format(now);
//                                        String tempTime = "";
//
//                                        ArrayList<String> nows = new ArrayList<>();
//                                        ArrayList<String> posts = new ArrayList<>();
//
//                                        for (int i = 0; i < tempDate.length(); i = i + 2) {
//                                            nows.add(tempC.substring(i, i + 2));
//                                            posts.add(tempDate.substring(i, i + 2));
//                                        }
//
//                                        int tempNavi = 0;
//
//                                        for (int i = 0; i < nows.size(); i++) {
//                                            if (!nows.get(i).equals(posts.get(i))) {
//                                                tempNavi = i;
//                                                break;
//                                            }
//                                        }
//
//                                        int tempNow = Integer.parseInt(nows.get(tempNavi));
//                                        int tempPosts = Integer.parseInt(posts.get(tempNavi));
//
//                                        switch (tempNavi) {
//                                            case 0:
//
//                                                break;
//                                            case 1:
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_year);
//                                                } else {
//                                                    tempTime = tempPosts - tempNow + getResources().getString(R.string.timeline_before_year);
//                                                }
//                                                break;
//                                            case 2:
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_month);
//                                                } else {
//                                                    tempTime = (12 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_month);
//                                                }
//                                                break;
//                                            case 3:
//                                                Calendar c = Calendar.getInstance();
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_day);
//                                                } else {
//                                                    c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
//                                                    int lastDay = c.getActualMaximum(Calendar.DATE);
//                                                    tempTime = (lastDay - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_day);
//                                                }
//                                                break;
//                                            case 4:
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_hour);
//                                                } else {
//                                                    tempTime = (24 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_hour);
//                                                }
//                                                break;
//                                            case 5:
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_minute);
//                                                } else {
//                                                    tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_minute);
//                                                }
//                                                break;
//                                            case 6:
//                                                if (tempNow > tempPosts) {
//                                                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_second);
//                                                } else {
//                                                    tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_second);
//                                                }
//                                                break;
//                                        }

                                        tvTime.setText(tempTime);

                                        JSONObject jj = new JSONObject(jd.getString("last_com"));

                                        if (!jj.getString("id").equals("null")) {
                                            layCom.setVisibility(View.VISIBLE);
                                            tvComId.setText(jj.getString("id"));
                                            tvComBody.setText(jj.getString("body"));

                                            cimvCom.setImageURLString(AppController.URL + jj.getString("profile_picture"));
                                        } else {
                                            layCom.setVisibility(View.GONE);
                                        }

                                        if (!jd.getString("and_tag_id").equals("")) {
                                            layAndTag.setVisibility(View.VISIBLE);
                                            tvAndTagTitle.setText(jd.getString("and_tag"));
                                            tvAndTagTitle.setTag(jd.getString("and_tag_id"));
                                            tvAndTagBody.setText(jd.getString("and_tag_body"));
                                        } else {
                                            layAndTag.setVisibility(View.GONE);
                                        }

                                        tvLikeCnt.setText(getResources().getString(R.string.like) + " " + jd.getInt("like_cnt") + getResources().getString(R.string.timeline_count));

                                        tvComCount.setText(getResources().getString(R.string.comment) + " " + jd.getInt("reply_cnt") + getResources().getString(R.string.timeline_count));

                                        String commentsText = tvBody.getText().toString();

                                        ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
                                        ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
                                        ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

                                        SpannableString commentsContent = new SpannableString(commentsText);

                                        for (int i = 0; i < hashtagSpans.size(); i++) {
                                            int[] span = hashtagSpans.get(i);
                                            int hashTagStart = span[0];
                                            int hashTagEnd = span[1];

                                            commentsContent.setSpan(new HashTag(SinglePostContentActivity.this),
                                                    hashTagStart, hashTagEnd, 0);
                                        }

                                        for (int i = 0; i < calloutSpans.size(); i++) {
                                            int[] span = calloutSpans.get(i);
                                            int calloutStart = span[0];
                                            int calloutEnd = span[1];

                                            commentsContent.setSpan(new CallTag(SinglePostContentActivity.this),
                                                    calloutStart,
                                                    calloutEnd, 0);

                                        }

                                        for (int i = 0; i < andSpans.size(); i++) {
                                            int[] span = andSpans.get(i);
                                            int calloutStart = span[0];
                                            int calloutEnd = span[1];

                                            AndTag at = new AndTag(SinglePostContentActivity.this);
                                            at.setTagId(tvAndTagTitle.getText().toString());
                                            commentsContent.setSpan(at,
                                                    calloutStart,
                                                    calloutEnd, 0);

                                        }

                                        tvBody.setMovementMethod(LinkMovementMethod.getInstance());
                                        tvBody.setText(commentsContent);
                                        tvBody.setHighlightColor(0x00ffffff);

                                        StringTransMethod stm = new StringTransMethod() {

                                        };

                                        Map<String, String> paramAction = new HashMap<>();

                                        paramAction.put("my_id", AppController.getSp().getString("email", ""));
                                        paramAction.put("target_id", jd.getString("email"));
                                        paramAction.put("type", "P");

                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostContentActivity.this, paramAction, 603, stm);
                                        AppController.apiDataTaskNew.execute();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });


                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.post_not_exist), Toast.LENGTH_SHORT).show();
                                }
                            });
                            finish();
                            overridePendingTransition(0, 0);
                        }

                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);

        AppController.apiTaskNew = new APITaskNew(this, params, 504, stmPostResult);
        AppController.apiTaskNew.execute();

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(SinglePostContentActivity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
//                NewTimelineFollowFragment.getData2();
//                NewTimelineAllFragment.getData();
                getData();
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
//                                NewTimelineFollowFragment.getData2();
//                                NewTimelineAllFragment.getData();
                                getData();
                                break;
                            case 3:
                                Intent i = new Intent(SinglePostContentActivity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                getData();
//                                NewTimelineFollowFragment.getData2();
//                                NewTimelineAllFragment.getData();
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                NewTimelineFollowFragment.getData2();
                NewTimelineAllFragment.getData();
                break;
//            case COMMENT_RETURN:
//                int comCount = data.getExtras().getInt("comCount");
//                String postId = data.getExtras().getString("postId");
//                for (int i = 0; i < m_ResultList.size(); i++) {
//                    if(m_ResultList.get(i).getPsot_id().equals(postId)){
//                        m_ResultList.get(i).setComCount(comCount);
//                        m_ListAdapter.notifyDataSetChanged();
////                        view
//                    }
//                }
//                break;
        }
    }

    private void initViews() {

        postId = getIntent().getExtras().getString("postId");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("단일 글 페이지_" + postId);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("단일 글 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        imvBack = (LinearLayout) findViewById(R.id.imv_single_content_back);
        imvLike = (ImageView) findViewById(R.id.imv_sigle_post_content_like);
        layLike = (LinearLayout) findViewById(R.id.lay_sigle_post_content_like);
        imvComment = (LinearLayout) findViewById(R.id.lay_sigle_post_content_comment);
        imvShare = (LinearLayout) findViewById(R.id.lay_sigle_post_content_share);
        imvMore = (LinearLayout) findViewById(R.id.lay_sigle_post_content_more);

        imvAniLike = (ImageView) findViewById(R.id.imv_sigle_post_row_like);
        imvAniLike.setVisibility(View.GONE);

        pb = (ProgressBar) findViewById(R.id.pb_sigle_post);

        imvBack.setOnClickListener(this);
        imvLike.setOnClickListener(this);
        layLike.setOnClickListener(this);
        imvComment.setOnClickListener(this);
        imvShare.setOnClickListener(this);
        imvMore.setOnClickListener(this);

        tvId = (TextView) findViewById(R.id.tv_sigle_post_content_id);
        tvId.setOnClickListener(this);
        tvPlayCnt = (TextView) findViewById(R.id.tv_sigle_post_content_play_cnt);
        tvBody = (TextView) findViewById(R.id.tv_sigle_post_content_body);
        tvTime = (TextView) findViewById(R.id.tv_sigle_post_content_time);
        tvLikeCnt = (TextView) findViewById(R.id.tv_sigle_post_content_like_cnt);
        tvLikeCnt.setOnClickListener(this);
        tvComCount = (TextView) findViewById(R.id.tv_sigle_post_content_comment_cnt);
        tvComCount.setOnClickListener(this);
        tvComId = (TextView) findViewById(R.id.tv_sigle_post_content_comment_id);
        tvComBody = (TextView) findViewById(R.id.tv_sigle_post_content_comment_body);

        tvAndTagTitle = (TextView) findViewById(R.id.tv_sigle_post_content_andtag_title);
        tvAndTagBody = (TextView) findViewById(R.id.tv_sigle_post_content_andtag_body);

        cimv = (CircleImageView) findViewById(R.id.cimv_sigle_post_content_pro);
        cimv.setOnClickListener(this);
        cimvCom = (CircleImageView) findViewById(R.id.cimv_sigle_post_content_comment_pro);

        bimv = (BoundableOfflineImageView) findViewById(R.id.bimv_sigle_post_content_img);

        layCom = (LinearLayout) findViewById(R.id.lay_sinlge_content_last_comment);
        layCom.setOnClickListener(this);

        layAndTag = (LinearLayout) findViewById(R.id.lay_sigle_post_content_andtag);
        layAndTag.setOnClickListener(this);

        getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_single_content_back:
                finish();
                break;
            case R.id.lay_sinlge_content_last_comment:
            case R.id.tv_sigle_post_content_comment_cnt:
            case R.id.lay_sigle_post_content_comment:
                Intent i = new Intent(SinglePostContentActivity.this, SinglePostV1Activity.class);
                i.putExtra("postId", postId);
                i.putExtra("navi", 1);
                if (imvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                startActivity(i);
                break;
            case R.id.cimv_sigle_post_content_pro:
            case R.id.tv_sigle_post_content_id:
                //TODO 사용자 정보로 이동
                String tempEmail = tvId.getTag().toString();
                if (!tempEmail.equals("")) {
                    Intent i2 = new Intent(SinglePostContentActivity.this, UserFeedActivity.class);
                    i2.putExtra("email", tempEmail);
                    startActivity(i2);
                }
                break;
            case R.id.tv_sigle_post_content_like_cnt:
                Intent i3 = new Intent(SinglePostContentActivity.this, UserListActivity.class);
                i3.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i3.putExtra("postId", postId);
                startActivity(i3);
                break;
            case R.id.lay_sigle_post_content_like:
            case R.id.imv_sigle_post_content_like:
                try {
                    if (imvLike.isSelected()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imvLike.setSelected(false);
                                String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) - 1) + getResources().getString(R.string.timeline_count));
                            }
                        });
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result onselect", result);
                                    final JSONObject jd = new JSONObject(result);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {

                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    imvLike.setSelected(true);
                                                    String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) + 1) + getResources().getString(R.string.timeline_count));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", postId);
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 303, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {


                        final Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like1);
                        final Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like2);
                        final Animation ani3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like3);

                        imvAniLike.setVisibility(View.VISIBLE);

                        ani1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                imvAniLike.startAnimation(ani2);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                imvAniLike.startAnimation(ani3);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani3.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                imvAniLike.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        imvAniLike.startAnimation(ani1);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imvLike.setSelected(true);
                                String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                if (!tempStr.equals("")) {
                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) + 1) + getResources().getString(R.string.timeline_count));
                                }

                            }
                        });
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    final JSONObject jd = new JSONObject(result);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    imvLike.setSelected(false);
                                                    String tempStr = tvLikeCnt.getText().toString().replace(getResources().getString(R.string.like), "").replace(getResources().getString(R.string.timeline_count), "").replace(" ", "");
                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (Integer.parseInt(tempStr) - 1) + getResources().getString(R.string.timeline_count));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", postId);
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 302, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                } catch (NullPointerException e) {

                }
                break;
            case R.id.lay_sigle_post_content_share:
//                Intent i4 = new Intent(SinglePostContentActivity.this, SharePopupActivity.class);
                Intent i4 = new Intent(SinglePostContentActivity.this, SharePopUp.class);
                i4.putExtra("postId", postId);
                i4.putExtra("body", tvBody.getText().toString());
                i4.putExtra("url", tempUrls);
                i4.putExtra("w", gifWidth);
                i4.putExtra("h", gifHeight);
                startActivityForResult(i4, SHARE_RETURN);
                break;
            case R.id.lay_sigle_post_content_more:
                Intent i5 = new Intent(SinglePostContentActivity.this, Popup2Activity.class);
                i5.putExtra("email", tvId.getTag().toString());
                if (tvId.getTag().toString().equals(AppController.getSp().getString("email", ""))) {
                    i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i5.putExtra("postId", postId);
                    i5.putExtra("pc", "P");
                } else {
                    i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i5.putExtra("postId", postId);
                    i5.putExtra("pc", "P");
                }
                i5.putExtra("url", tempUrls);
                i5.putExtra("title", "");
                i5.putExtra("body", tvBody.getText().toString());
                startActivityForResult(i5, MORE_RETURN);

                break;
        }
    }

}
