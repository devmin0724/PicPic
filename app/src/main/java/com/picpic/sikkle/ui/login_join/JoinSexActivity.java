package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class JoinSexActivity extends Activity implements View.OnClickListener {

    public static Activity ac;
    String tempEmail = "", tempPass = "", tempBirth = "";
    LinearLayout lay;
    ImageView man, woman;
    Button btn;
    boolean isMan = true;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join_sex);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 성별 입력 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        tempEmail = getIntent().getExtras().getString("email");
        tempPass = getIntent().getExtras().getString("password");
        tempBirth = getIntent().getExtras().getString("birth");

        initViews();

    }

    private void initViews() {
        lay = (LinearLayout) findViewById(R.id.lay_join_sex_sex);

        man = (ImageView) findViewById(R.id.imv_join_sex_male);
        woman = (ImageView) findViewById(R.id.imv_join_sex_female);
        man.setOnClickListener(this);
        woman.setOnClickListener(this);

        man.setSelected(true);

        btn = (Button) findViewById(R.id.btn_join_sex_next);
        btn.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_join_sex_male:
                man.setSelected(true);
                woman.setSelected(false);
                isMan = true;
                break;
            case R.id.imv_join_sex_female:
                man.setSelected(false);
                woman.setSelected(true);
                isMan = false;
                break;
            case R.id.btn_join_sex_next:
                Intent i = new Intent(JoinSexActivity.this, JoinIdActivity.class);
                i.putExtra("email", tempEmail);
                i.putExtra("password", tempPass);
                i.putExtra("birth", tempBirth);
                i.putExtra("sex", isMan);
                startActivity(i);
                break;
        }
    }
}
