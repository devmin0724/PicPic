package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class SettingsProgramInfoActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack, layVersion, laySA, layPP, layOP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_program_info);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 앱 정보 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_program_info_back);

        layVersion = (LinearLayout) findViewById(R.id.lay_settings_program_info_version);
        laySA = (LinearLayout) findViewById(R.id.lay_settings_program_info_service_agreement);
        layPP = (LinearLayout) findViewById(R.id.lay_settings_program_info_privacy_policy);
        layOP = (LinearLayout) findViewById(R.id.lay_settings_program_info_operational_policy);

        layBack.setOnClickListener(this);

        layVersion.setOnClickListener(this);
        laySA.setOnClickListener(this);
        layPP.setOnClickListener(this);
        layOP.setOnClickListener(this);
    }
    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    @Override
    public void onClick(View v) {
        Intent i = new Intent(SettingsProgramInfoActivity.this, TermActivity.class);
        switch (v.getId()) {
            case R.id.lay_settings_program_info_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_program_info_version:
                //TODO 버전정보
                startActivity(new Intent(SettingsProgramInfoActivity.this, SettingsProgramInfoVersionActivity.class));
                break;
            case R.id.lay_settings_program_info_service_agreement:
                //TODO 서비스 이용약관
                i.putExtra("navi", 0);
                startActivity(i);
                break;
            case R.id.lay_settings_program_info_privacy_policy:
                //TODO 개인정보 보호방침
                i.putExtra("navi", 1);
                startActivity(i);
                break;
            case R.id.lay_settings_program_info_operational_policy:
                //TODO 운영정책
                i.putExtra("navi", 2);
                startActivity(i);
                break;
        }
    }
}
