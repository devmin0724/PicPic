package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.picpic.sikkle.R;
import com.picpic.sikkle.Test3Activity;
import com.picpic.sikkle.TestActivity;
import com.picpic.sikkle.fragment.NewTimelineAllFragment;
import com.picpic.sikkle.fragment.NewTimelineCategoryFragment;
import com.picpic.sikkle.fragment.NewTimelineFollowFragment;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.login_join.FriendResultActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.supertooltips.ToolTip;
import com.picpic.sikkle.widget.supertooltips.ToolTipRelativeLayout;
import com.picpic.sikkle.widget.supertooltips.ToolTipView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NewGridTimelineActivity extends FragmentActivity implements View.OnClickListener {
    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;
    public static final int COMMENT_RETURN = 1006;
    private static final int TIMELINE_RETURN = 1001;
    private static final int FACE_SLICE_RETURN = 1007;
    public static ToolTipRelativeLayout toolTipRelativeLayout;
    public static Activity ac;
    //    LinearLayout layIntro, imvHome, imvAlarm;
    LinearLayout imvHome;
    //    ImageView imvChatting;
//    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch, layEvent;
    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch;
    ViewPager vp;
    SmartTabLayout stl;
    int navi = 0;

//    public static void showToolTip() {
//
//        toolTipRelativeLayout.removeAllViews();
//
//        ToolTip toolTip = new ToolTip()
//                .withContentView(LayoutInflater.from(ac).inflate(R.layout.customtooltip, null))
//                .withAnimationType(ToolTip.AnimationType.FROM_MASTER_VIEW)
//                .withColor(0xffffe766);
//        ToolTipView ttv = toolTipRelativeLayout.showToolTipForView(toolTip, ac.findViewById(R.id.lay_timeline_camera));
//        ttv.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
//            @Override
//            public void onToolTipViewClicked(ToolTipView toolTipView) {
////                Toast.makeText(getApplicationContext(), "ㅋㅋㅋ", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_new_timeline);

//        startActivity(new Intent(NewGridTimelineActivity.this, Test3Activity.class));
//        startActivity(new Intent(NewGridTimelineActivity.this, FaceSliceCameraActivity.class));

//        try{
//            navi = getIntent().getExtras().getInt("navi");
//            if(navi == 0){
//                startActivity(new Intent(NewGridTimelineActivity.this, XmasEventPopupActivity.class));
//            }
//        }catch (NullPointerException e){
//
//        }

        ac = this;

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        final FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.timeline_top_1, NewTimelineFollowFragment.class)
                .add(R.string.timeline_top_2, NewTimelineAllFragment.class)
                .add(R.string.timeline_top_3, NewTimelineCategoryFragment.class)
                .create());

        vp = (ViewPager) findViewById(R.id.viewpager_new_timeline);
        vp.setAdapter(adapter);

        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        NewTimelineFollowFragment.getData2();
                        NewTimelineAllFragment.initData();

                        NewTimelineFollowFragment.list.setVisibility(View.VISIBLE);
                        NewTimelineAllFragment.list.setVisibility(View.GONE);
                        break;
                    case 1:
                        NewTimelineFollowFragment.initData();
                        NewTimelineAllFragment.getData();

                        NewTimelineFollowFragment.list.setVisibility(View.GONE);
                        NewTimelineAllFragment.list.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        NewTimelineFollowFragment.initData();
                        NewTimelineAllFragment.initData();

                        NewTimelineFollowFragment.list.setVisibility(View.GONE);
                        NewTimelineAllFragment.list.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        stl = (SmartTabLayout) findViewById(R.id.viewpagertab_new_timeline);
//        stl.setViewPager(vp);

        toolTipRelativeLayout = (ToolTipRelativeLayout) findViewById(R.id.tooltipRelativeLayout_new_timeline);

        imvHome = (LinearLayout) findViewById(R.id.imv_main_timeline_home);
//        imvAlarm = (LinearLayout) findViewById(R.id.imv_main_timeline_top_alarm);
//        imvChatting = (ImageView) findViewById(R.id.imv_main_timeline_top_chatting);
//        btnIntro = (Button) findViewById(R.id.btn_timeline_intro);
        laySearch = (LinearLayout) findViewById(R.id.imv_timeline_search);
        laySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(0, 0);
                Intent ii = new Intent(NewGridTimelineActivity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 0);
                startActivity(ii);
            }
        });
        layTimeline = (LinearLayout) findViewById(R.id.lay_timeline_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_timeline_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_timeline_camera);
        layAndTag = (LinearLayout) findViewById(R.id.lay_timeline_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_timeline_mypage);

        imvHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewTimelineAllFragment.getData();
                NewTimelineFollowFragment.getData2();
            }
        });
//        imvAlarm.setOnClickListener(this);
//        imvChatting.setOnClickListener(this);
//        btnIntro.setOnClickListener(this);

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layAndTag.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        layTimeline.setSelected(true);

//        layIntro = (LinearLayout) findViewById(R.id.lay_timeline_intro);
//        layIntro.setVisibility(View.GONE);

        layTimeline.setSelected(true);

//        NewTimelineAllFragment.getData();
//        NewTimelineFollowFragment.getData2();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(NewGridTimelineActivity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
                NewTimelineFollowFragment.getData2();
                NewTimelineAllFragment.getData();
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
                                NewTimelineFollowFragment.getData2();
                                NewTimelineAllFragment.getData();
                                break;
                            case 3:
                                Intent i = new Intent(NewGridTimelineActivity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                NewTimelineFollowFragment.getData2();
                                NewTimelineAllFragment.getData();
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                NewTimelineFollowFragment.getData2();
                NewTimelineAllFragment.getData();
                break;
            case FACE_SLICE_RETURN:
                NewTimelineFollowFragment.getData2();
                NewTimelineAllFragment.getData();
                break;
//            case COMMENT_RETURN:
//                int comCount = data.getExtras().getInt("comCount");
//                String postId = data.getExtras().getString("postId");
//                for (int i = 0; i < m_ResultList.size(); i++) {
//                    if(m_ResultList.get(i).getPsot_id().equals(postId)){
//                        m_ResultList.get(i).setComCount(comCount);
//                        m_ListAdapter.notifyDataSetChanged();
////                        view
//                    }
//                }
//                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_main_timeline_home:
                //TODO refresh ListView
//                list.getRefreshableView().smoothScrollToPosition(0);
                break;
            case R.id.imv_main_timeline_top_alarm:
                startActivity(new Intent(NewGridTimelineActivity.this, NotificationActivity.class));
                break;
            case R.id.imv_main_timeline_top_chatting:
//                Toast.makeText(this, "채팅 준비중", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_timeline_intro:
                Intent i = new Intent(NewGridTimelineActivity.this, FriendResultActivity.class);
                i.putExtra("navi", 3);
                startActivityForResult(i, POST_WRITE_RETURN);
                break;
            case R.id.lay_timeline_timeline:
                break;
            case R.id.lay_timeline_interest:
                i = new Intent(NewGridTimelineActivity.this, InterestFeedActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(i, POST_WRITE_RETURN);
                finish();
                break;
            case R.id.lay_timeline_camera:
                System.gc();
//                Intent gifCamera = new Intent(NewGridTimelineActivity.this, GIFCameraActivity.class);
                Intent gifCamera = new Intent(NewGridTimelineActivity.this, GIFCameraFilmActivity.class);
                gifCamera.putExtra("navi", 0);
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(gifCamera, POST_WRITE_RETURN);
                break;
            case R.id.lay_timeline_noti:
                i = new Intent(NewGridTimelineActivity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_mypage:
                i = new Intent(NewGridTimelineActivity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }

}
