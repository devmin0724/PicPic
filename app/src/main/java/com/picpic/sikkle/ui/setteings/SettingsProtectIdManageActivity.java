package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.UserListItem;
import com.picpic.sikkle.beans.UserListResult;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV1Activity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.UserFeedForIdActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingsProtectIdManageActivity extends Activity implements View.OnClickListener {

    ListView list;
    LinearLayout layBack;
    FrameLayout layComplete;
    UserListResult dataArr;
    UserListResult m_ResultList = null;
    ArrayAdapter<UserListItem> m_ListAdapter = null;
    ArrayList<Boolean> isLikes = null;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_protect_id_manage);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 차단된 아이디 관리 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        dataArr = new UserListResult();

        layBack = (LinearLayout) findViewById(R.id.lay_settings_protect_id_manage_back);
        layComplete = (FrameLayout) findViewById(R.id.lay_settings_protect_id_manage_complete);

        layBack.setOnClickListener(this);
        layComplete.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list_settings_protect_id_manage);

        getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_protect_id_manage_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_protect_id_manage_complete:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }

    private void getData() {
        StringTransMethod stmProtectList = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        dataArr = new UserListResult();
                        JSONArray jarr = new JSONArray(jd.getString("data"));

                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject j = jarr.getJSONObject(i);
                            UserListItem uli = new UserListItem();
                            uli.setUrl(j.getString("url"));
                            uli.setIsfriend(false);
                            uli.setIsBlock(true);
                            uli.setName(j.getString("id"));
                            uli.setEmail(j.getString("email"));
                            dataArr.add(uli);
                        }
                        updateResultList(dataArr);
                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 410, stmProtectList);
        AppController.apiTaskNew.execute();

    }

    protected boolean updateResultList(UserListResult resultList) {

        isLikes = new ArrayList<>();

        for (int i = 0; i < resultList.size(); i++) {
            isLikes.add(false);
        }

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();

                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new UserListAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);

            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });

            }
        });

        return true;
    }

    class UserListAdapter extends ArrayAdapter<UserListItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public UserListAdapter(Context ctx, int txtViewId, List<UserListItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_user_list, null);

                CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_user_list);

                TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_user_list_row_name);
                final TextView tvIsBlock = (TextView) convertView.findViewById(R.id.tv_user_list_row_is_block);

                final ImageView imvCheck1 = (ImageView) convertView.findViewById(R.id.imv_user_list_check1);
                final ImageView imvCheck2 = (ImageView) convertView.findViewById(R.id.imv_user_list_check2);

                final UserListItem model = getItem(pos);

                imvCheck1.setVisibility(View.GONE);
                imvCheck2.setVisibility(View.VISIBLE);
                tvIsBlock.setVisibility(View.VISIBLE);

                if (model.isBlock()) {
                    imvCheck2.setSelected(true);
                } else {
                    imvCheck2.setSelected(false);
                }

                tvUserId.setText(model.getName());

                if (model.getUrl().equals("")) {
                    cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
                } else {
                    cimvPro.setImageURLString(AppController.URL + model.getUrl());
                }

                if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                    imvCheck2.setVisibility(View.GONE);
                } else {
                    imvCheck2.setVisibility(View.VISIBLE);
                }

                imvCheck2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringTransMethod stmProtect = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        if (jd.getString("cut_yn").equals("Y")) {
                                            imvCheck2.setSelected(true);
                                            tvIsBlock.setText(getResources().getString(R.string.block));
                                            isLikes.set(pos, true);
                                        } else {
                                            imvCheck2.setSelected(false);
                                            tvIsBlock.setText(getResources().getString(R.string.block_cancel));
                                            isLikes.set(pos, false);
                                        }
                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };

                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("user_id", model.getEmail());

                        AppController.apiDataTaskNew = new APIDataTaskNew(SettingsProtectIdManageActivity.this, params, 216, stmProtect);
                        AppController.apiDataTaskNew.execute();

                    }
                });
                cimvPro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AppController.getSp().getString("id", "").equals(model.getName())) {
                            startActivity(new Intent(SettingsProtectIdManageActivity.this, MyPageV2Activity.class));
                            finish();
                        } else {
                            Intent i = new Intent(SettingsProtectIdManageActivity.this, UserFeedForIdActivity.class);
                            i.putExtra("id", model.getName());
                            startActivity(i);
                        }
                    }
                });
            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}
