package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsChangePasswordActivity extends Activity implements View.OnClickListener, View.OnFocusChangeListener {

    EditTextCheckIcon etciNow, etciNew, etciNewC;
    EditText edtNow, edtNew, edtNewC;
    ImageView imvNow, imvNew, imvNewC;
    TextView imvComplete;
    LinearLayout layBack, layNow, layNew, layNewC;
    Animation shake;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_change_password);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 비밀번호 변경 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        etciNow = (EditTextCheckIcon) findViewById(R.id.etci_settings_myinfo_change_password_now);
        etciNew = (EditTextCheckIcon) findViewById(R.id.etci_settings_myinfo_change_password_new);
        etciNewC = (EditTextCheckIcon) findViewById(R.id.etci_settings_myinfo_change_password_new_confirm);

        edtNow = (EditText) findViewById(R.id.edt_settings_change_password_now);
        edtNew = (EditText) findViewById(R.id.edt_settings_change_password_new);
        edtNewC = (EditText) findViewById(R.id.edt_settings_change_password_new_confirm);

        imvNow = (ImageView) findViewById(R.id.imv_settings_change_password_now);
        imvNew = (ImageView) findViewById(R.id.imv_settings_change_password_new);
        imvNewC = (ImageView) findViewById(R.id.imv_settings_change_password_new_confirm);
        imvComplete = (TextView) findViewById(R.id.imv_settings_change_password_next);

        layBack = (LinearLayout) findViewById(R.id.lay_settings_change_password_back);

        layNow = (LinearLayout) findViewById(R.id.lay_settings_change_password_now);
        layNew = (LinearLayout) findViewById(R.id.lay_settings_change_password_new);
        layNewC = (LinearLayout) findViewById(R.id.lay_settings_change_password_new_confirm);

        etciNow.setOnClickListener(this);
        etciNew.setOnClickListener(this);
        etciNewC.setOnClickListener(this);

        imvComplete.setOnClickListener(this);

        layBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etci_settings_myinfo_change_password_now:
                edtNow.setText("");
                break;
            case R.id.etci_settings_myinfo_change_password_new:
                edtNew.setText("");
                break;
            case R.id.etci_settings_myinfo_change_password_new_confirm:
                edtNewC.setText("");
                break;
            case R.id.imv_settings_change_password_next:
                if (edtNow.getText().toString().equals("")) {
                    layNow.startAnimation(shake);
                } else {
                    if (edtNew.getText().toString().equals("")) {
                        layNew.startAnimation(shake);
                    } else {
                        if (edtNewC.getText().toString().equals("")) {
                            layNewC.startAnimation(shake);
                        } else {
                            if (edtNow.getText().toString().equals(edtNew.getText().toString())) {
                                layNow.startAnimation(shake);
                                layNew.startAnimation(shake);
                                layNewC.startAnimation(shake);
                            } else {
                                if (!edtNew.getText().toString().equals(edtNewC.getText().toString())) {
                                    layNew.startAnimation(shake);
                                    layNewC.startAnimation(shake);
                                } else {
                                    //TODO 비밀번호 변경 ㄱㄱ
                                    StringTransMethod stmChangePass = new StringTransMethod() {
                                        @Override
                                        public void endTrans(String result) {
                                            try {
                                                JSONObject jd = new JSONObject(result);

                                                if (jd.getInt("result") == 0) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.settings_myinfo_change_password_complete), Toast.LENGTH_SHORT).show();
                                                    finish();
                                                    overridePendingTransition(0, 0);
                                                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {

                                            }
                                        }
                                    };
                                    Map<String, String> params = new HashMap<>();

                                    params.put("myId", AppController.getSp().getString("email", ""));
                                    params.put("now_pw", edtNow.getText().toString());
                                    params.put("new_pw", edtNew.getText().toString());

                                    AppController.apiDataTaskNew = new APIDataTaskNew(SettingsChangePasswordActivity.this, params, 208, stmChangePass);
                                    AppController.apiDataTaskNew.execute();

                                }
                            }
                        }
                    }
                }
                break;
            case R.id.lay_settings_change_password_back:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edt_settings_change_password_now:
                if (hasFocus) {
                    etciNow.setState(EditTextCheckIcon.CHOICE_X);
                    etciNew.setState(EditTextCheckIcon.CHOICE_NON);
                    etciNewC.setState(EditTextCheckIcon.CHOICE_NON);
                    imvNow.setSelected(true);
                    if (edtNew.getText().toString().length() > 0) {
                        imvNew.setSelected(true);
                    } else {
                        imvNew.setSelected(false);
                    }
                    if (edtNewC.getText().toString().length() > 0) {
                        imvNewC.setSelected(true);
                    } else {
                        imvNewC.setSelected(false);
                    }
                }
                break;
            case R.id.edt_settings_change_password_new:
                if (hasFocus) {
                    etciNow.setState(EditTextCheckIcon.CHOICE_NON);
                    etciNew.setState(EditTextCheckIcon.CHOICE_X);
                    etciNewC.setState(EditTextCheckIcon.CHOICE_NON);
                    imvNew.setSelected(true);
                    if (edtNow.getText().toString().length() > 0) {
                        imvNow.setSelected(true);
                    } else {
                        imvNow.setSelected(false);
                    }
                    if (edtNewC.getText().toString().length() > 0) {
                        imvNewC.setSelected(true);
                    } else {
                        imvNewC.setSelected(false);
                    }
                }
                break;
            case R.id.edt_settings_change_password_new_confirm:
                if (hasFocus) {
                    etciNow.setState(EditTextCheckIcon.CHOICE_NON);
                    etciNew.setState(EditTextCheckIcon.CHOICE_NON);
                    etciNewC.setState(EditTextCheckIcon.CHOICE_X);
                    imvNewC.setSelected(true);
                    if (edtNew.getText().toString().length() > 0) {
                        imvNew.setSelected(true);
                    } else {
                        imvNew.setSelected(false);
                    }
                    if (edtNow.getText().toString().length() > 0) {
                        imvNow.setSelected(true);
                    } else {
                        imvNow.setSelected(false);
                    }
                }
                break;
        }
    }
}
