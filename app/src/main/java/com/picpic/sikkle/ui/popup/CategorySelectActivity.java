package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.CircleImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CategorySelectActivity extends Activity {

    GridView gv;

    int[] ress = {
            R.drawable.category_non, R.drawable.category_daylife, R.drawable.category_animal, R.drawable.category_celebrities, R.drawable.category_emotion, R.drawable.category_animation, R.drawable.category_food, R.drawable.category_fashion, R.drawable.category_beauty, R.drawable.category_artdesign, R.drawable.category_sports, R.drawable.category_movie, R.drawable.category_tv, R.drawable.category_game, R.drawable.category_cartoon, R.drawable.category_reaction, R.drawable.category_vehicle, R.drawable.category_music, R.drawable.category_expression, R.drawable.category_action, R.drawable.category_interest, R.drawable.category_decades, R.drawable.category_nature, R.drawable.category_sticker, R.drawable.category_science, R.drawable.category_holidays
    };

    int[] titles = {
            R.string.category_0, R.string.category_1, R.string.category_2, R.string.category_3, R.string.category_4, R.string.category_5, R.string.category_6, R.string.category_7, R.string.category_8, R.string.category_9, R.string.category_10, R.string.category_11, R.string.category_12, R.string.category_13, R.string.category_14, R.string.category_15, R.string.category_16, R.string.category_17, R.string.category_18, R.string.category_19, R.string.category_20, R.string.category_21, R.string.category_22, R.string.category_23, R.string.category_24, R.string.category_25
    };

    ArrayList<CategoryItem> cateArr;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_category_select);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("카테고리 선택 팝업 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        cateArr = new ArrayList<>();

        gv = (GridView) findViewById(R.id.gv_category_select);

        for (int i = 0; i < ress.length; i++) {
            CategoryItem ci = new CategoryItem();
            ci.setRes(ress[i]);
            ci.setTitleRes(titles[i]);
            cateArr.add(ci);
        }

        gv.setAdapter(new CategoryAdapter(this, 0, cateArr));

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.putExtra("result", i);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

    class CategoryAdapter extends ArrayAdapter<CategoryItem> {

        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public CategoryAdapter(Context context, int resource, List<CategoryItem> objects) {
            super(context, resource, objects);
            this.viewArray = new SparseArray<WeakReference<View>>(objects.size());
            this.m_LayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null) {
                    return convertView;
                }
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_new_timeline_category_select, null);

                CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_new_timeliine_category);
                TextView tv = (TextView) convertView.findViewById(R.id.tv_new_timeline_category);

                cimv.setImageResource(cateArr.get(position).getRes());
                tv.setText(getContext().getResources().getString(cateArr.get(position).getTitleRes()));

            } finally {

            }

            return convertView;
        }
    }

    class CategoryItem {
        private int res = 0;
        private int titleRes = 0;

        public int getTitleRes() {
            return titleRes;
        }

        public void setTitleRes(int titleRes) {
            this.titleRes = titleRes;
        }

        public int getRes() {
            return res;
        }

        public void setRes(int res) {
            this.res = res;
        }

    }
}
