/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.CommentAdapter;
import com.picpic.sikkle.beans.CommentItem;
import com.picpic.sikkle.beans.CommentResult;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.PixelUtil;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.VideoSingleLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView2;
import com.yydcdut.sdlv.Menu;
import com.yydcdut.sdlv.MenuItem;
import com.yydcdut.sdlv.SlideAndDragListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SinglePostV1Activity extends Activity {

    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;

    private LinearLayout layBack, layMore;
    private CircleImageView cimv;
    private TextView tvWriterId, tvCommentSubmit;
    private SlideAndDragListView list;
    //    private ListView list;
    private List<Menu> menuList = new ArrayList<>();
    private AutoCompleteTextView edt;

    private CommentAdapter adapter;
    private CommentResult cr = new CommentResult();

    private View header;

    private TextView tvHeaderBody, tvHeaderTags, tvHeaderLikeCount, tvHeaderCommentCount, tvHeaderPlayCount;
    private FrameLayout layContent;
    private VView2 vvHeader;
    private BoundableOfflineImageView bimvHeader;
    private LikeView lvHeader;
    private LinearLayout layHeaderLike, layHeaderComment, layHeaderPlayCount;
    private Button btnHeaderShare;

    private String postId = "", videoUrl = "", tempUrl = "", tempName = "", tagList = "", tempUrls = "", tempComId;
    private long playCnt;
    private int likeCnt, commentCnt, nowPage = 1, navi = 0;
    private boolean isListLoading = false, lastItemVisibleFlag = false, isEditing = false, isFirst = true;

    private ImageFullLoader imageLoader;
    private VideoSingleLoader videoLoader;

    String userCollection = "";
    ArrayList<int[]> calloutSpans;

    String[] tags = {
            "#aaaaa1", "#bbbbb1", "#ccccc1",
            "#aaaaa2", "#bbbbb2", "#ccccc2",
            "@aaaaa1", "@bbbbb1", "@ccccc1",
            "@aaaaa2", "@bbbbb2", "@ccccc2",
            "&aaaaa1", "&bbbbb1", "&ccccc1",
            "&aaaaa2", "&bbbbb2", "&ccccc2"
    };
    String[] tags1 = {
            "#aaaaa1", "#bbbbb1", "#ccccc1",
            "#aaaaa2", "#bbbbb2", "#ccccc2"
    };
    String[] tags2 = {
            "@aaaaa1", "@bbbbb1", "@ccccc1",
            "@aaaaa2", "@bbbbb2", "@ccccc2"
    };
    String[] tags3 = {
            "&aaaaa1", "&bbbbb1", "&ccccc1",
            "&aaaaa2", "&bbbbb2", "&ccccc2"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_single_post_v1);

        postId = getIntent().getExtras().getString("postId");

        imageLoader = new ImageFullLoader(this);
        videoLoader = new VideoSingleLoader(this);

        initViews();

    }

    private void initViews() {
        layBack = (LinearLayout) findViewById(R.id.lay_single_post_v1_back);
        layMore = (LinearLayout) findViewById(R.id.lay_single_post_v1_more);

        cimv = (CircleImageView) findViewById(R.id.cimv_single_post_v1);

        tvWriterId = (TextView) findViewById(R.id.tv_single_post_v1_id);
        tvCommentSubmit = (TextView) findViewById(R.id.tv_single_post_v1_submit);

        list = (SlideAndDragListView) findViewById(R.id.list_single_post_v1);
//        list = (ListView) findViewById(R.id.list_single_post_v1);

        edt = (AutoCompleteTextView) findViewById(R.id.edt_single_post_v1);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        header = inflater.inflate(R.layout.single_post_header, null, false);

        tvHeaderBody = (TextView) header.findViewById(R.id.tv_single_post_v1_body);
        tvHeaderTags = (TextView) header.findViewById(R.id.tv_single_post_v1_tags);
        tvHeaderLikeCount = (TextView) header.findViewById(R.id.tv_single_post_v1_like_count);
        tvHeaderCommentCount = (TextView) header.findViewById(R.id.tv_single_post_v1_comment_count);
        tvHeaderPlayCount = (TextView) header.findViewById(R.id.tv_single_post_v1_play_count);

        layContent = (FrameLayout) header.findViewById(R.id.lay_single_post_v1_content);

        vvHeader = (VView2) header.findViewById(R.id.vv_single_post_v1);

        bimvHeader = (BoundableOfflineImageView) header.findViewById(R.id.bimv_single_post_v1);

        lvHeader = (LikeView) header.findViewById(R.id.imv_single_post_v1_like);

        layHeaderLike = (LinearLayout) header.findViewById(R.id.lay_single_post_v1_like);
        layHeaderComment = (LinearLayout) header.findViewById(R.id.lay_single_post_v1_comment);
        layHeaderPlayCount = (LinearLayout) header.findViewById(R.id.lay_single_post_v1_play_count);

        btnHeaderShare = (Button) header.findViewById(R.id.btn_single_post_v1_share);

        try {
            navi = getIntent().getExtras().getInt("navi");
        } catch (NullPointerException e) {
            navi = 0;
        }

        list.addHeaderView(header);

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 25;

                    Log.e("list", count + "/" + page);

                    if (count > 10) {
                        if ((count - 1) % 25 == 0) {
                            if (!isListLoading) {
                                if (nowPage < page + 1) {
                                    isListLoading = true;
                                    getCommentList(false, page + 1);

                                    nowPage = page + 1;
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        layBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent();
                i.putExtra("id", postId);
                i.putExtra("is", layHeaderLike.isSelected());
                i.putExtra("ct", likeCnt);
                setResult(RESULT_OK, i);

                finish();
            }
        });

        tvCommentSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEditing) {
                    if (edt.getText().toString().equals("")) {

                    } else {
                        commentEdit(edt.getText().toString(), tempComId);
                    }
                } else {
                    if (edt.getText().toString().equals("")) {

                    } else {
                        commentWrite(edt.getText().toString());
                    }
                }
            }
        });

        vvHeader.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                bimvHeader.setVisibility(View.INVISIBLE);
            }
        });

        edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    if (isEditing) {
                        if (edt.getText().toString().equals("")) {

                        } else {
                            commentEdit(edt.getText().toString(), tempComId);
                        }
                    } else {
                        if (edt.getText().toString().equals("")) {

                        } else {
                            commentWrite(edt.getText().toString());
                        }
                    }
                    return true;
                }
                return false;
            }
        });


//        edt.setTokenizer(token);

//        linkSetting();

        edt.addTextChangedListener(tw);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("단일 글 페이지_" + postId);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("단일 글 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        int w = PixelUtil.dpToPx(this, 69);

        Menu menu = new Menu(new ColorDrawable(Color.WHITE), false, 0);
        menu.addItem(new MenuItem.Builder().setWidth(w)
                .setBackground(new ColorDrawable(0xffd7d7d7))
                .setDirection(MenuItem.DIRECTION_RIGHT)
                .setIcon(getResources().getDrawable(R.drawable.img_edit))
                .build());
        menu.addItem(new MenuItem.Builder().setWidth(w)
                .setBackground(new ColorDrawable(0xffff5b5e))
                .setDirection(MenuItem.DIRECTION_RIGHT)
                .setIcon(getResources().getDrawable(R.drawable.img_delete))
                .build());
        Menu menu2 = new Menu(new ColorDrawable(Color.WHITE), false, 1);
        menu2.addItem(new MenuItem.Builder().setWidth(w)
                .setBackground(new ColorDrawable(0xffd7d7d7))
                .setDirection(MenuItem.DIRECTION_RIGHT)
                .setIcon(getResources().getDrawable(R.drawable.img_usertag))
                .build());
        menu2.addItem(new MenuItem.Builder().setWidth(w)
                .setBackground(new ColorDrawable(0xffff5b5e))
                .setDirection(MenuItem.DIRECTION_RIGHT)
                .setIcon(getResources().getDrawable(R.drawable.img_warning))
                .build());
        menuList.add(menu);
        menuList.add(menu2);

        list.setMenu(menuList);

        list.setOnMenuItemClickListener(new SlideAndDragListView.OnMenuItemClickListener() {
            @Override
            public int onMenuItemClick(View v, int itemPosition, int buttonPosition, int direction) {
                final int pos = itemPosition - 1;
                if (cr.get(pos).getType() == 0) {
                    if (buttonPosition == 0) {
                        //TODO 수정
                        isEditing = true;

                        edt.setAdapter(null);

                        edt.setText("");

                        tempComId = cr.get(pos).getCom_id();
                        edt.setText(cr.get(pos).getBody());
                        edt.setSelection(edt.getText().toString().length());
                    } else {
                        //TODO 삭제
                        isEditing = false;
                        commentDelete(cr.get(pos).getCom_id(), pos);
                    }
                } else {
                    if (buttonPosition == 0) {
                        //TODO 태그
                        edt.setAdapter(null);
                        if (isEditing) {
                            edt.setText("");
                        }
                        isEditing = false;
                        if (edt.getText().toString().equals("")) {
                            edt.setText("@" + cr.get(pos).getId() + " ");
                            edt.setSelection(edt.getText().toString().length());
                        } else {
                            edt.setText(edt.getText().toString() + " @" + cr.get(pos).getId() + " ");
                            edt.setSelection(edt.getText().toString().length());
                        }

                        edt.setAdapter(adapter);
                    } else {
                        //TODO 신고
                        isEditing = false;
                        if (cr.get(pos).getEmail().equals(AppController.getSp().getString("email", ""))) {

                        } else {
                            Intent i = new Intent(SinglePostV1Activity.this, ReportActivity.class);
                            i.putExtra("postId", cr.get(pos).getCom_id());
                            i.putExtra("pc", "C");
                            startActivity(i);
                        }
                    }
                }
                return Menu.ITEM_SCROLL_BACK;
            }
        });

        getData();
    }

    private void getData() {

        StringTransMethod stmPostResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    MinUtils.d("single data", result);
                    final JSONObject jd = new JSONObject(result);
                    if (jd.getInt("result") == 0) {
                        if (!jd.getString("url").equals("")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        cimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));
                                        cimv.setTag("");

                                        tagList = jd.getString("tag_list");

                                        tvWriterId.setText(jd.getString("id"));
                                        tvWriterId.setTag(jd.getString("email"));
                                        playCnt = (long) jd.getInt("play_cnt");
                                        likeCnt = jd.getInt("like_cnt");
                                        DecimalFormat df = new DecimalFormat("#,##0");
                                        tvHeaderPlayCount.setText(df.format(playCnt) + "");

                                        if (jd.getString("like_yn").equals("Y")) {
                                            layHeaderLike.setSelected(true);
                                        } else {
                                            layHeaderLike.setSelected(false);
                                        }

                                        tempUrl = jd.getString("url");

                                        Log.e("url", tempUrl);

                                        int lastIdx = tempUrl.lastIndexOf("_");

                                        tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                                        videoUrl = tempUrl.replace(".gif", ".mp4");

//                                        bimv.setImageURLString(AppController.URL + jd.getString("url"), AppController.URL + tempName, SinglePostContentActivity.this);
//                                        downloadThumbImage(bimv, AppController.URL + tempName);
                                        imageLoader.DisplayImage(AppController.URL + tempName, bimvHeader, false);

                                        bimvHeader.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
                                            @Override
                                            public void onImageChanged(Drawable d) {
//                                                bimvHeader.setVisibility(View.INVISIBLE);
//                                                vvHeader.setVideoURI(Uri.parse(AppController.URL + videoUrl));
                                                videoLoader.DisplayVideo(AppController.URL + videoUrl, vvHeader, false);
                                                vvHeader.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                    @Override
                                                    public void onPrepared(MediaPlayer mp) {
                                                        bimvHeader.setVisibility(View.INVISIBLE);
//                                                        vvHeader.start();
                                                    }
                                                });
                                                vvHeader.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                    @Override
                                                    public void onCompletion(MediaPlayer mp) {
                                                        //TODO 재생횟수증가
                                                        AppController.playCountExe.execute(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                new MinUtils.PlayCountSubmit(postId).execute();
                                                            }
                                                        });
                                                        MinUtils.setCountLong(tvHeaderPlayCount, playCnt);
                                                        vvHeader.seekTo(0);
                                                        vvHeader.start();
                                                    }
                                                });
                                            }
                                        });

                                        layContent.setOnTouchListener(new OnSwipeTouchListener(SinglePostV1Activity.this) {
                                            @Override
                                            public void onClick() {
                                                super.onClick();
//                                                Drawable d = bimvHeader.getDrawable();
//                                                if (d instanceof GifDrawable) {
//                                                    try {
//                                                        GifDrawable g = (GifDrawable) d;
//                                                        if (g.isPlaying()) {
//                                                            g.pause();
//                                                        } else {
//                                                            g.start();
//                                                        }
//                                                    } catch (NullPointerException e) {
//
//                                                    }
//                                                }
                                                if (vvHeader.isPlaying()) {
                                                    vvHeader.pause();
                                                } else {
                                                    vvHeader.start();
                                                }
                                            }

                                            @Override
                                            public void onDoubleClick() {
                                                super.onDoubleClick();

                                                try {
                                                    if (layHeaderLike.isSelected()) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                layHeaderLike.setSelected(false);
                                                                --likeCnt;
                                                                MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                            }
                                                        });
                                                        StringTransMethod stmLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    Log.e("result onselect", result);
                                                                    final JSONObject jd = new JSONObject(result);
                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {

                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    layHeaderLike.setSelected(true);
                                                                                    ++likeCnt;
                                                                                    MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                        }
                                                                    });
                                                                } catch (JSONException e) {

                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostV1Activity.this, params, 303, stmLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    } else {

                                                        lvHeader.startAni();

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                layHeaderLike.setSelected(true);
                                                                ++likeCnt;
                                                                MinUtils.setCount(tvHeaderLikeCount, likeCnt);

                                                            }
                                                        });
                                                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    final JSONObject jd = new JSONObject(result);

                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {
                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    layHeaderLike.setSelected(false);
                                                                                    --likeCnt;
                                                                                    MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (JSONException e) {
                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostV1Activity.this, params, 302, stmUnLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    }
                                                } catch (NullPointerException e) {

                                                }


                                            }
                                        });

                                        cimv.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String myId = AppController.getSp().getString("email", "");
                                                String tempEmail = tvWriterId.getTag().toString();
                                                if (tempEmail.equals(myId)) {
                                                    startActivity(new Intent(SinglePostV1Activity.this, MyPageV2Activity.class));
                                                    finish();
                                                } else {
                                                    if (!tempEmail.equals("")) {
                                                        Intent i = new Intent(SinglePostV1Activity.this, UserFeedActivity.class);
                                                        i.putExtra("email", tempEmail);
                                                        startActivity(i);
                                                    }
                                                }
                                            }
                                        });

                                        tvWriterId.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String myId = AppController.getSp().getString("email", "");
                                                String tempEmail = tvWriterId.getTag().toString();
                                                if (tempEmail.equals(myId)) {
                                                    startActivity(new Intent(SinglePostV1Activity.this, MyPageV2Activity.class));
                                                    finish();
                                                } else {
                                                    if (!tempEmail.equals("")) {
                                                        Intent i = new Intent(SinglePostV1Activity.this, UserFeedActivity.class);
                                                        i.putExtra("email", tempEmail);
                                                        startActivity(i);
                                                    }
                                                }
                                            }
                                        });

                                        layHeaderLike.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                try {
                                                    if (layHeaderLike.isSelected()) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                layHeaderLike.setSelected(false);
                                                                --likeCnt;
                                                                MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                            }
                                                        });
                                                        StringTransMethod stmLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    Log.e("result onselect", result);
                                                                    final JSONObject jd = new JSONObject(result);
                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {

                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    layHeaderLike.setSelected(true);
                                                                                    ++likeCnt;
                                                                                    MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }

                                                                        }
                                                                    });
                                                                } catch (JSONException e) {

                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostV1Activity.this, params, 303, stmLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    } else {

                                                        lvHeader.startAni();

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                layHeaderLike.setSelected(true);
                                                                ++likeCnt;
                                                                MinUtils.setCount(tvHeaderLikeCount, likeCnt);

                                                            }
                                                        });
                                                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                                            @Override
                                                            public void endTrans(final String result) {
                                                                try {
                                                                    final JSONObject jd = new JSONObject(result);

                                                                    runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            try {
                                                                                if (jd.getInt("result") == 0) {
                                                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                                                } else {
                                                                                    layHeaderLike.setSelected(false);
                                                                                    --likeCnt;
                                                                                    MinUtils.setCount(tvHeaderLikeCount, likeCnt);
                                                                                }
                                                                            } catch (JSONException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    });
                                                                } catch (JSONException e) {
                                                                }

                                                            }
                                                        };
                                                        Map<String, String> params = new HashMap<>();

                                                        params.put("post_reply_id", postId);
                                                        params.put("click_id", AppController.getSp().getString("email", ""));
                                                        params.put("like_form", "P");

                                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostV1Activity.this, params, 302, stmUnLikeResult);
                                                        AppController.apiDataTaskNew.execute();

                                                    }
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        });

                                        tempUrls = AppController.URL + jd.getString("url");

                                        tvHeaderBody.setText(jd.getString("contents"));

                                        MinUtils.setCount(tvHeaderLikeCount, jd.getInt("like_cnt"));
                                        MinUtils.setCount(tvHeaderCommentCount, jd.getInt("reply_cnt"));
                                        MinUtils.setCountLong(tvHeaderPlayCount, (long) jd.getInt("play_cnt"));


                                        MinUtils.setTags(SinglePostV1Activity.this,
                                                tvHeaderTags,
                                                tagList);

                                        layMore.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Intent i5 = new Intent(SinglePostV1Activity.this, Popup2Activity.class);
                                                i5.putExtra("email", tvWriterId.getTag().toString());
                                                if (tvWriterId.getTag().toString().equals(AppController.getSp().getString("email", ""))) {
                                                    i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                                                    i5.putExtra("postId", postId);
                                                    i5.putExtra("pc", "P");
                                                } else {
                                                    i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                                                    i5.putExtra("postId", postId);
                                                    i5.putExtra("pc", "P");
                                                }
                                                i5.putExtra("url", tempUrls);
                                                i5.putExtra("title", "");
                                                i5.putExtra("body", tvHeaderBody.getText().toString());
                                                startActivityForResult(i5, MORE_RETURN);
                                            }
                                        });

                                        btnHeaderShare.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent i4 = new Intent(SinglePostV1Activity.this, SharePopUp.class);
                                                i4.putExtra("postId", postId);
                                                i4.putExtra("body", tvHeaderBody.getText().toString());
                                                i4.putExtra("url", tempUrls);
                                                i4.putExtra("w", 200);
                                                i4.putExtra("h", 200);
                                                startActivityForResult(i4, SHARE_RETURN);
                                            }
                                        });

//                                        String commentsText = tvBody.getText().toString();
//
//                                        ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
//                                        ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
//                                        ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');
//
//                                        SpannableString commentsContent = new SpannableString(commentsText);
//
//                                        for (int i = 0; i < hashtagSpans.size(); i++) {
//                                            int[] span = hashtagSpans.get(i);
//                                            int hashTagStart = span[0];
//                                            int hashTagEnd = span[1];
//
//                                            commentsContent.setSpan(new HashTag(SinglePostContentActivity.this),
//                                                    hashTagStart, hashTagEnd, 0);
//                                        }
//
//                                        for (int i = 0; i < calloutSpans.size(); i++) {
//                                            int[] span = calloutSpans.get(i);
//                                            int calloutStart = span[0];
//                                            int calloutEnd = span[1];
//
//                                            commentsContent.setSpan(new CallTag(SinglePostContentActivity.this),
//                                                    calloutStart,
//                                                    calloutEnd, 0);
//
//                                        }
//
//                                        for (int i = 0; i < andSpans.size(); i++) {
//                                            int[] span = andSpans.get(i);
//                                            int calloutStart = span[0];
//                                            int calloutEnd = span[1];
//
//                                            AndTag at = new AndTag(SinglePostContentActivity.this);
//                                            at.setTagId(tvAndTagTitle.getText().toString());
//                                            commentsContent.setSpan(at,
//                                                    calloutStart,
//                                                    calloutEnd, 0);
//
//                                        }
//
//                                        tvBody.setMovementMethod(LinkMovementMethod.getInstance());
//                                        tvBody.setText(commentsContent);
//                                        tvBody.setHighlightColor(0x00ffffff);

                                        StringTransMethod stm = new StringTransMethod() {

                                        };

                                        Map<String, String> paramAction = new HashMap<>();

                                        paramAction.put("my_id", AppController.getSp().getString("email", ""));
                                        paramAction.put("target_id", jd.getString("email"));
                                        paramAction.put("type", "P");

                                        AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostV1Activity.this, paramAction, 603, stm);
                                        AppController.apiDataTaskNew.execute();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
//                                    GridResult gr = new GridResult();
//                                    GridItem gi = new GridItem();
//                                    gi.setIs0(true);
//                                    gr.add(gi);
//                                    list.setAdapter(new SampleAdapter(SinglePostV1Activity.this, 0, gr));

                                    CommentResult cr = new CommentResult();
                                    CommentItem ci = new CommentItem();
                                    ci.setNon(true);
                                    cr.add(ci);
                                    adapter = new CommentAdapter(SinglePostV1Activity.this, 0, cr);
                                    list.setAdapter(adapter);

                                }
                            });


                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.post_not_exist), Toast.LENGTH_SHORT).show();
                                }
                            });
                            finish();
                            overridePendingTransition(0, 0);
                        }

                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {

                } finally {
                    getCommentList(true, 1);
                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);

        AppController.apiTaskNew = new APITaskNew(this, params, 504, stmPostResult);
        AppController.apiTaskNew.execute();

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videoLoader != null && !videoUrl.equals("") && vvHeader != null) {
            videoLoader.DisplayVideo(AppController.URL + videoUrl, vvHeader, false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(SinglePostV1Activity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
                getData();
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
                                getData();
                                break;
                            case 3:
                                Intent i = new Intent(SinglePostV1Activity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                getData();
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                break;
        }
    }

    private void getCommentList(boolean is, int page) {

        if (is) {
            cr = new CommentResult();
            adapter.notifyDataSetChanged();
        }

        isListLoading = true;

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        params.put("page", page + "");

        AppController.apiTaskNew = new APITaskNew(this, params, 321, stmCommentListResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmCommentListResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            Log.e("comment", result);
            try {
                final JSONObject j = new JSONObject(result);

//                    tvWholeLikeCount.setText();
                if (j.getInt("result") == 0) {

                    if (!j.isNull("data")) {
                        JSONArray jarr = new JSONArray(j.getString("data"));
                        Log.e("data length", jarr.length() + "");

                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject jd = jarr.getJSONObject(i);
                            CommentItem ci = new CommentItem();
                            ci.setId(jd.getString("id"));
                            if (jd.getString("like_yn").equals("Y") || jd.getString("like_yn").equals("y")) {
                                ci.setIsLike(true);
                            } else {
                                ci.setIsLike(false);
                            }
                            ci.setCom_id(jd.getString("reply_id"));
                            ci.setBody(jd.getString("body"));
                            ci.setLikeCnt(jd.getInt("like"));
                            ci.setEmail(jd.getString("email"));
                            ci.setTime(jd.getString("time"));
                            Log.e("time", jd.getString("time"));
                            ci.setUrl(jd.getString("profile_picture"));

                            if (AppController.getSp().getString("email", "").equals(jd.getString("email"))) {
                                //TODO 수정 삭제
                                ci.setType(0);
                            } else {
                                //TODO 답글 신고
                                ci.setType(1);
                            }

                            cr.add(ci);
                        }
                        adapter = new CommentAdapter(SinglePostV1Activity.this, 0, cr);
                        list.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {

                        CommentItem ci = new CommentItem();
                        ci.setNon(true);
                        cr.add(ci);

                        adapter = new CommentAdapter(SinglePostV1Activity.this, 0, cr);
                        list.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    }
                } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {

            } finally {
                if (videoLoader != null && !videoUrl.equals("") && vvHeader != null) {
                    videoLoader.DisplayVideo(AppController.URL + videoUrl, vvHeader, false);
                }
                if (isFirst) {
                    list.setSelection(navi);
//                    list.smoothScrollToPosition(navi);
                    isFirst = false;
                } else {
                    list.smoothScrollToPosition(1);
                }
            }
        }
    };

    MultiAutoCompleteTextView.Tokenizer token = new MultiAutoCompleteTextView.Tokenizer() {
        @Override
        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;
            if (edt.getText().toString().length() > 1) {


                while (i > 0 && text.charAt(i - 1) != ' ') {
                    i--;
                }
                while (i < cursor && text.charAt(i) == ' ') {
                    i++;
                }
            }

            return i;
        }

        @Override
        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();
            if (edt.getText().toString().length() > 1) {
                while (i < len) {
                    if (text.charAt(i) == ' ') {
                        return i;
                    } else {
                        i++;
                    }
                }
            }
            return len;
        }

        @Override
        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();
            if (edt.getText().toString().length() > 1) {
                while (i > 0 && text.charAt(i - 1) == ' ') {
                    i--;
                }

                if (i > 0 && text.charAt(i - 1) == ' ') {
                    return text;
                } else {
                    if (text instanceof Spanned) {
                        SpannableString sp = new SpannableString(text + " ");
                        TextUtils.copySpansFrom((Spanned) text, 0, text.length(),
                                Object.class, sp, 0);
                        return sp;
                    } else {
                        return text + " ";
                    }
                }
            }
            return text;
        }
    };
    TextWatcher tw = new TextWatcher() {
        String tempS;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            tempS = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (tempS.length() > 1) {
                int startInt = tempS.lastIndexOf("@");
                int endInt = tempS.lastIndexOf(" ");

                if (startInt == -1) {

                } else {
                    if (endInt == -1) {
                        //TODO 검색
                        getUserTagSearch(tempS.substring(startInt, tempS.length()).replace("@", ""));
                    } else {
                        if (startInt > endInt) {
                            //TODO 검색
                            getUserTagSearch(tempS.substring(startInt, tempS.length()).replace("@", ""));
                        }
                    }
                }

            }
        }
    };

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void getUserTagSearch(String str) {

        StringTransMethod stmUserTagListResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {
//                            MinUtils.d("return_tag_search", result);
                        if (!j.isNull("friend")) {
                            JSONArray ja = new JSONArray(j.getString("friend"));
                            final ArrayList<TagListItem> arr = new ArrayList<>();
                            ArrayList<String> arr2 = new ArrayList<>();
                            JSONObject jd = null;
                            TagListItem tli = null;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_url(jd.getString("profile_picture"));
                                tli.setTag_name(jd.getString("id"));
                                tli.setTag_id(jd.getString("email"));

                                arr2.add(jd.getString("id"));

                                arr.add(tli);
                            }
                            final ArrayList<String> arr22 = arr2;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    edt.setAdapter(new TagAdapter(SinglePostV1Activity.this, 0, arr22, 0, arr));
                                    edt.showDropDown();
                                }
                            });

//                            Handler handler = new Handler() {
//                                @Override
//                                public void handleMessage(Message msg) {
//                                    edtGogo.showDropDown();
//                                }
//                            };
//
//                            handler.sendEmptyMessageDelayed(0, 500);

                            edt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Log.e("dd", "dd");
                                    final String tagName = "@" + arr.get(position).getTag_name();
                                    String split = "";
                                    String edt1 = edt.getText().toString();
                                    int last = 0;
                                    String oldEdt = "";
                                    last = edt1.lastIndexOf("@");
                                    if (last != -1) {
                                        oldEdt = edt1.substring(0, last - 1);
                                    }
                                    final int finalLast = last;
                                    final String finalOldEdt = oldEdt;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            edt.setAdapter(null);
                                            if (finalLast != -1) {
                                                edt.setText(finalOldEdt + " " + tagName + " ");
                                            } else {
                                                edt.setText(tagName + " ");
                                            }
                                            edt.setSelection(edt.getText().toString().length());
                                            edt.dismissDropDown();
                                        }
                                    });
                                }
                            });

                        }
                    } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> parmas = new HashMap<>();

        parmas.put("my_id", AppController.getSp().getString("email", ""));
        parmas.put("str", str);
        parmas.put("page", "1");

        AppController.apiTaskNew = new APITaskNew(this, parmas, 512, stmUserTagListResult);
        AppController.apiTaskNew.execute();
    }

    class TagAdapter extends ArrayAdapter<String> {

        int typeNavi = 0;
        ArrayList<TagListItem> arr;
        LayoutInflater m_LayoutInflater = null;

        public TagAdapter(Context context, int resource, List<String> objects, int navi, ArrayList<TagListItem> arrayList) {
            super(context, resource, objects);
            typeNavi = navi;
            arr = arrayList;
        }

        @Override
        public String getItem(int position) {
            return super.getItem(position) + " ";
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            View v = convertView;

            final TagListHolder holder;

            if (v == null) {
                if (m_LayoutInflater == null)
                    m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                v = m_LayoutInflater.inflate(R.layout.row_tag_list, null);
                holder = new TagListHolder();

                holder.cimv = (CircleImageView) v.findViewById(R.id.cimv_tag_list_row);

                holder.tvTag = (TextView) v.findViewById(R.id.tv_tag_list_row);

                v.setTag(holder);

            } else {
                holder = (TagListHolder) v.getTag();
            }

            final String model = getItem(pos);
            //TODO 타임라인 데이터 insert

            holder.cimv.setVisibility(View.VISIBLE);
            holder.cimv.setImageURLString(AppController.URL + arr.get(pos).getTag_url());
            holder.tvTag.setText("@" + model + " ");


            return v;
        }

        class TagListHolder {
            TextView tvTag;
            CircleImageView cimv;
        }


    }

    private void commentWrite(String body) {

        getTags();

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        params.put("com_id", "");
        params.put("user_tags", userCollection);
        params.put("com_form", "W");
        params.put("body", body);
        params.put("url", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    StringTransMethod stmCommentStatusResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                Log.e("commentResult", result);
                JSONObject j = new JSONObject(result);
                if (j.getInt("result") == 0) {
                    getInitData();
                } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void commentDelete(String comId, int a) {

        cr.remove(a);

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        params.put("com_id", comId);
        params.put("com_form", "D");
        params.put("body", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    private void commentEdit(String body, String comId) {
        getTags();

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        params.put("com_id", comId);
        params.put("user_tags", userCollection);
        params.put("com_form", "E");
        params.put("body", body);
        params.put("url", "");

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 231, stmCommentStatusResult);
        AppController.apiDataTaskNew.execute();

    }

    private void getTags() {
        userCollection = "";
        String commentsText = edt.getText().toString();

        calloutSpans = MinUtils.getSpans(commentsText, '@');

        for (int i = 0; i < calloutSpans.size(); i++) {
            int[] span = calloutSpans.get(i);
            int calloutStart = span[0];
            int calloutEnd = span[1];

            userCollection = userCollection + commentsText.substring(calloutStart + 1, calloutEnd) + ",";

        }

//        if (userCollection.length() > 1) {
//            userCollection = userCollection.substring(0, userCollection.length() - 1);
//        }

        for (int i = 0; i < calloutSpans.size(); i++) {
            Log.e("1", calloutSpans.get(i)[0] + "");
            Log.e("1", calloutSpans.get(i)[1] + "");
        }

//        Log.e("user Tags", userCollection);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent();
        i.putExtra("id", postId);
        i.putExtra("is", layHeaderLike.isSelected());
        i.putExtra("ct", likeCnt);
        setResult(RESULT_OK, i);

        super.onBackPressed();
    }

    private void getInitData() {

        isListLoading = true;
//        m_ListAdapter = null;
        cr = new CommentResult();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                edt.setText("");
            }
        });

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", postId);
        params.put("page", 1 + "");

        AppController.apiTaskNew = new APITaskNew(this, params, 321, stmCommentListResult);
        AppController.apiTaskNew.execute();
    }

}