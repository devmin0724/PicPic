package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.FileUploadActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProfilePictureActivity extends Activity implements View.OnClickListener {

    private static final int PICK_FROM_CAMERA = 1000;
    private static final int PICK_FROM_ALBUM = 1001;
    private static final int CROP_FROM_CAMERA = 1002;
    private static final int CROP_FROM_ALBUM = 1003;
    private static final int FILE_UPLOAD_RETURN = 2001;
    CircleImageView cimv;
    ImageView imvDumyPro, imvTakeCamera, imvTakeAlbum;
    Button btnComplete;
//    CircularProgressButton btnComplete;
    LinearLayout layProgress;
    String tempEmail = "", tempId = "", tempPw = "", tempSex = "", tempBirth = "";
    Bitmap bb;
    File f = null;
    boolean isPhoto = false;
    String fileName, imagePath, profile_url;
    SharedPreferences.Editor se;
    ArrayList<String> friendsArr;
    int joinNavi = 0;
    Activity ac;
    boolean isTrans = false;
    private Uri mImageCaptureUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_picture);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 프로필사진 등록 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        se = AppController.getSe();

        initViews();

    }

    private void initViews() {

        layProgress = (LinearLayout)findViewById(R.id.lay_profile_progress);
        layProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        cimv = (CircleImageView) findViewById(R.id.cimv_profile_picture);

        imvDumyPro = (ImageView) findViewById(R.id.imv_profile_picture);
        imvTakeAlbum = (ImageView) findViewById(R.id.imv_profile_picture_album);
        imvTakeCamera = (ImageView) findViewById(R.id.imv_profile_picture_take);

        btnComplete = (Button) findViewById(R.id.btn_profile_picture_complete);
//        btnComplete.setIndeterminateProgressMode(true);


        imvTakeAlbum.setOnClickListener(this);
        imvTakeCamera.setOnClickListener(this);
        btnComplete.setOnClickListener(this);

        try {
            Intent i = getIntent();
            tempEmail = i.getExtras().getString("email");
            tempPw = i.getExtras().getString("pw");
            tempBirth = i.getExtras().getString("birth");
            tempSex = i.getExtras().getString("sex");
            tempId = i.getExtras().getString("id");
            joinNavi = i.getExtras().getInt("navi");
            if (joinNavi == 1) {
                profile_url = "http://graph.facebook.com/" + tempPw + "/picture?type=large";
                friendsArr = i.getStringArrayListExtra("friends");
            }
        } catch (Exception e) {
            tempEmail = "";
            tempPw = "";
            tempBirth = "";
            tempSex = "";
            tempId = "";
            joinNavi = 0;
        }

        long tempCurrent = System.currentTimeMillis();

        f = new File(AppController.BASE_SRC + tempEmail + "_" + tempCurrent + ".jpg");
        fileName = AppController.getSp().getString("email", "") + tempCurrent + ".jpg";
        if (joinNavi == 1) {
            if (!profile_url.equals("")) {
                isPhoto = true;
                imvDumyPro.setVisibility(View.GONE);
                new ImageDownTask(profile_url).execute();
                cimv.setVisibility(View.VISIBLE);
            } else {
                isPhoto = false;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void doTakePhotoAction() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String url = "tmp_" + String.valueOf(System.currentTimeMillis())
                + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), url));

        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                mImageCaptureUri);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CROP_FROM_CAMERA:

            case CROP_FROM_ALBUM:

                isPhoto = true;

                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                opt.inSampleSize = 1;
                opt.inPreferredConfig = QrBitmapFactory.Options.Config.ARGB_8888;
                opt.inSampleSize = 1;

                Bitmap photo = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);

                imagePath = f.getAbsolutePath();

                cimv.setImageBitmap(photo);

                cimv.setVisibility(View.VISIBLE);
                imvDumyPro.setVisibility(View.GONE);

                break;


            case PICK_FROM_ALBUM:
                mImageCaptureUri = data.getData();
                Log.e("filepath", mImageCaptureUri.getPath());

                Intent intent = new Intent("com.android.camera.action.CROP");
                intent.setDataAndType(mImageCaptureUri, "image/*");

                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("output", Uri.fromFile(f));

                startActivityForResult(intent, CROP_FROM_ALBUM);

                break;
            case PICK_FROM_CAMERA:
                Intent intent2 = new Intent("com.android.camera.action.CROP");
                intent2.setDataAndType(mImageCaptureUri, "image/*");

                intent2.putExtra("aspectX", 1);
                intent2.putExtra("aspectY", 1);
                intent2.putExtra("output", Uri.fromFile(f));
                startActivityForResult(intent2, CROP_FROM_CAMERA);

                break;
            case FILE_UPLOAD_RETURN:
                if (resultCode == RESULT_OK) {

                    StringTransMethod stmJoin = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            isTrans = true;
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getString("result").equals("0")) {
//                                    Intent i = new Intent(ProfilePictureActivity.this, FriendResultActivity.class);
//                                    i.putExtra("navi", 2);
//                                    i.putExtra("joinNavi", joinNavi);
//                                    if (joinNavi == 1) {
//                                        i.putStringArrayListExtra("friends", friendsArr);
//                                    }
//                                    startActivity(i);
                                    Date now = new Date();
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                    String tempC = sdf.format(now);

                                    Map<String, String> params = new HashMap<>();

                                    if (AppController.getSp().getString("register_form", "").equals("10002")) {
                                        params.put("email", AppController.getSp().getString("password", ""));
                                    } else {
                                        params.put("email", AppController.getSp().getString("email", ""));
                                    }
                                    params.put("password", AppController.getSp().getString("password", ""));
                                    params.put("register_form", AppController.getSp().getString("register_form", ""));
                                    params.put("country", AppController.getSp().getString("country", ""));
                                    params.put("device_id", MinUtils.DID);
                                    params.put("push_token", MinUtils.PTK);
                                    params.put("regist_day", tempC);

                                    AppController.apiDataTaskNew = new APIDataTaskNew(ProfilePictureActivity.this, params, 202, stmAutoLoginiResult);
                                    AppController.apiDataTaskNew.execute();

//                                    Intent gifCamera = new Intent(ProfilePictureActivity.this, GIFCameraActivity.class);
//                                    gifCamera.putExtra("navi", 2);
//                                    overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
//                                    startActivity(gifCamera);
//
//                                    finish();
//                                    LoginActivity._ac.finish();
                                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_SHORT).show();
                            } finally {
                                isTrans = false;
                            }
                        }
                    };

                    Date now = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    String tempC = sdf.format(now);

                    HashMap<String, String> data2 = new HashMap<>();

                    tempBirth = tempBirth.replace("년", "/");
                    tempBirth = tempBirth.replace("월", "/");
                    tempBirth = tempBirth.replace("일", "");
                    tempBirth = tempBirth.replace(" ", "");

                    String[] tempB = tempBirth.split("/");

                    if (joinNavi == 1) {
                        data2.put("email", tempPw);
                    } else {
                        data2.put("email", tempEmail);
                    }
                    data2.put("password", tempPw);
                    data2.put("bir_year", tempB[0]);
                    data2.put("bir_mon", tempB[1]);
                    data2.put("bir_day", tempB[2]);
                    switch (joinNavi) {
                        case 0:
                            data2.put("register_form", "10001");
                            break;
                        case 1:
                            data2.put("register_form", "10002");
                            data2.put("facebook_id", tempEmail);
                            data2.put("facebook_id_key", tempPw);
                            break;
                    }
                    data2.put("profile_picture", fileName);
                    if (Boolean.parseBoolean(tempSex)) {
                        data2.put("sex", "M");
                    } else {
                        data2.put("sex", "W");
                    }
                    data2.put("country", AppController.getSp().getString("country", "ko_KR"));
                    data2.put("device_id", MinUtils.DID);
                    data2.put("push_token", MinUtils.PTK);
                    data2.put("id", tempId);
                    data2.put("regist_day", tempC);

                    if (joinNavi == 1) {
                        AppController.getSe().putString("email", tempPw);
                    } else {
                        AppController.getSe().putString("email", tempEmail);
                    }
                    AppController.getSe().putString("password", tempPw);
                    AppController.getSe().putString("bir_year", tempB[0]);
                    AppController.getSe().putString("bir_mon", tempB[1]);
                    AppController.getSe().putString("bir_day", tempB[2]);
                    switch (joinNavi) {
                        case 0:
                            AppController.getSe().putString("register_form", "10001");
                            break;
                        case 1:
                            AppController.getSe().putString("register_form", "10002");
                            AppController.getSe().putString("facebook_id", tempEmail);
                            AppController.getSe().putString("facebook_id_key", tempPw);
                            break;
                    }
                    AppController.getSe().putString("profile_picture", fileName);
                    if (Boolean.parseBoolean(tempSex)) {
                        AppController.getSe().putString("sex", "M");
                    } else {
                        AppController.getSe().putString("sex", "W");
                    }
                    AppController.getSe().putString("device_id", MinUtils.DID);
                    AppController.getSe().putString("push_token", MinUtils.PTK);
                    AppController.getSe().putString("id", tempId);
                    AppController.getSe().putString("regist_day", tempC);
                    AppController.getSe().putBoolean("isAutoLogin", true);
                    AppController.getSe().commit();

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, data2, 201, stmJoin);
                    AppController.apiDataTaskNew.execute();

                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_profile_picture_album:
                doTakeAlbumAction();
                break;
            case R.id.imv_profile_picture_take:
                doTakePhotoAction();
                break;
            case R.id.btn_profile_picture_complete:

                if (layProgress.getVisibility() == View.GONE) {
                    layProgress.setVisibility(View.VISIBLE);

                    if(!isTrans){
                        isTrans = true;
                        StringTransMethod stmProfile = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getString("result").equals("0")) {
//                                Intent i = new Intent(ProfilePictureActivity.this, FriendResultActivity.class);
//                                i.putExtra("navi", 2);
//                                i.putExtra("joinNavi", joinNavi);
//                                if (joinNavi == 1) {
//                                    i.putStringArrayListExtra("friends", friendsArr);
//                                }
//                                startActivity(i);
                                        //TODO 로그인

                                        Date now = new Date();
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                        String tempC = sdf.format(now);

                                        Map<String, String> params = new HashMap<>();

                                        if (AppController.getSp().getString("register_form", "").equals("10002")) {
                                            params.put("email", AppController.getSp().getString("password", ""));
                                        } else {
                                            params.put("email", AppController.getSp().getString("email", ""));
                                        }
                                        params.put("password", AppController.getSp().getString("password", ""));
                                        params.put("register_form", AppController.getSp().getString("register_form", ""));
                                        params.put("country", AppController.getSp().getString("country", ""));
                                        params.put("device_id", MinUtils.DID);
                                        params.put("push_token", MinUtils.PTK);
                                        params.put("regist_day", tempC);

                                        AppController.apiDataTaskNew = new APIDataTaskNew(ProfilePictureActivity.this, params, 202, stmAutoLoginiResult);
                                        AppController.apiDataTaskNew.execute();

                                    }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_SHORT).show();
                                } finally {
                                    isTrans = false;
                                }
                            }
                        };

                        //TODO
                        if (isPhoto) {

                            Intent i = new Intent(ProfilePictureActivity.this, FileUploadActivity.class);
                            if (joinNavi == 1) {
                                i.putExtra("url", f.getAbsolutePath());
                            } else {
                                i.putExtra("url", imagePath);
                            }
                            i.putExtra("file", fileName);
                            startActivityForResult(i, FILE_UPLOAD_RETURN);
                        } else {
                            Date now = new Date();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String tempC = sdf.format(now);

                            HashMap<String, String> data2 = new HashMap<>();

                            tempBirth = tempBirth.replace("년", "/");
                            tempBirth = tempBirth.replace("월", "/");
                            tempBirth = tempBirth.replace("일", "");
                            tempBirth = tempBirth.replace(" ", "");

                            String[] tempB = tempBirth.split("/");

                            if (joinNavi == 1) {
                                data2.put("email", tempPw);
                            } else {
                                data2.put("email", tempEmail);
                            }
                            data2.put("password", tempPw);
                            data2.put("bir_year", tempB[0]);
                            data2.put("bir_mon", tempB[1]);
                            data2.put("bir_day", tempB[2]);
                            switch (joinNavi) {
                                case 0:
                                    data2.put("register_form", "10001");
                                    break;
                                case 1:
                                    data2.put("register_form", "10002");
                                    data2.put("facebook_id", tempEmail);
                                    data2.put("facebook_id_key", tempPw);
                                    break;
                            }
                            data2.put("profile_picture", "noprofile.png");
                            if (Boolean.parseBoolean(tempSex)) {
                                data2.put("sex", "M");
                            } else {
                                data2.put("sex", "W");
                            }
                            data2.put("country", AppController.getSp().getString("country", "ko_KR"));
                            data2.put("device_id", MinUtils.DID);
                            data2.put("push_token", MinUtils.PTK);
                            data2.put("id", tempId);
                            data2.put("regist_day", tempC);

                            if (joinNavi == 1) {
                                se.putString("email", tempPw);
                            } else {
                                se.putString("email", tempEmail);
                            }
                            se.putString("password", tempPw);
                            se.putString("bir_year", tempB[0]);
                            se.putString("bir_mon", tempB[1]);
                            se.putString("bir_day", tempB[2]);
                            switch (joinNavi) {
                                case 0:
                                    se.putString("register_form", "10001");
                                    break;
                                case 1:
                                    se.putString("register_form", "10002");
                                    se.putString("facebook_id", tempEmail);
                                    se.putString("facebook_id_key", tempPw);
                                    break;
                            }
                            se.putString("profile_picture", "");
                            if (Boolean.parseBoolean(tempSex)) {
                                se.putString("sex", "M");
                            } else {
                                se.putString("sex", "W");
                            }
                            se.putString("device_id", MinUtils.DID);
                            se.putString("push_token", MinUtils.PTK);
                            se.putString("id", tempId);
                            se.putString("regist_day", tempC);
                            se.commit();

                            AppController.apiDataTaskNew = new APIDataTaskNew(this, data2, 201, stmProfile);
                            AppController.apiDataTaskNew.execute();

                        }
                    }
                }

                break;
        }
    }

    StringTransMethod stmAutoLoginiResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    MinUtils.d("profile result", result);
                    JSONObject loginData = new JSONObject(jd.getString("data"));
                    AppController.getSe().putString("bir_year", loginData.getString("bir_year"));
                    AppController.getSe().putString("bir_mon", loginData.getString("bir_mon"));
                    AppController.getSe().putString("bir_day", loginData.getString("bir_day"));
                    AppController.getSe().putString("country", loginData.getString("country"));
                    AppController.getSe().putString("cut_day", loginData.getString("cut_day"));
                    AppController.getSe().putString("cut_reason", loginData.getString("cut_reason"));
                    AppController.getSe().putString("declare_cnt", loginData.getString("declare_cnt"));
                    AppController.getSe().putString("device_id", loginData.getString("device_id"));
                    AppController.getSe().putString("email", loginData.getString("email"));
                    AppController.getSe().putString("facebook_id", loginData.getString("facebook_id"));
                    AppController.getSe().putString("fri_open_yn", loginData.getString("fri_open_yn"));
                    AppController.getSe().putString("id", loginData.getString("id"));
                    AppController.getSe().putString("m_id", loginData.getString("m_id"));
                    AppController.getSe().putString("pinterest_id", loginData.getString("pinterest_id"));
                    AppController.getSe().putString("posts_open_form", loginData.getString("posts_open_form"));
                    AppController.getSe().putString("profile_picture", loginData.getString("profile_picture"));
                    AppController.getSe().putString("push_token", loginData.getString("push_token"));
                    AppController.getSe().putString("regist_day", loginData.getString("regist_day"));
                    AppController.getSe().putString("register_form", loginData.getString("register_form"));
                    AppController.getSe().putString("sex", loginData.getString("sex"));
                    AppController.getSe().putString("tumbler_id", loginData.getString("tumbler_id"));
                    AppController.getSe().putString("twitter_id", loginData.getString("twitter_id"));
                    AppController.getSe().putBoolean("isAutoLogin", true);
                    AppController.getSe().commit();
//                    startActivity(new Intent(InitActivity.this, TimeLineActivity.class));
//                    startActivity(new Intent(InitActivity.this, TestActivity.class));
//                    startActivity(new Intent(InitActivity.this, Test2Activity.class));

//                    btnComplete.setProgress(100);
                    layProgress.setVisibility(View.GONE);

                    AppController.t.set("&uid", loginData.getString("email"));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory("FirstLogin").build());

                    System.gc();
//                    Intent gifCamera = new Intent(ProfilePictureActivity.this, GIFCameraActivity.class);
                    Intent gifCamera = new Intent(ProfilePictureActivity.this, GIFCameraFilmActivity.class);
                    gifCamera.putExtra("navi", 2);
                    overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                    startActivity(gifCamera);

                    finish();
                    try{
                        LoginActivity._ac.finish();
                    }catch (NullPointerException e){

                    }
                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                } else {
                    AppController.getSe().putBoolean("isAutoLogin", false);
                    AppController.getSe().commit();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "로그인에 실패했습니다. 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {

            }finally {
                layProgress.setVisibility(View.GONE);
            }

        }
    };

    class ImageDownTask extends AsyncTask<Void, Void, Void> {

        String _url = "";

        public ImageDownTask(String url) {
            this._url = url;
        }

        @Override
        protected Void doInBackground(Void... params) {


            HttpGet httpRequest = null;

            try {
                URL url = new URL(_url);

                httpRequest = new HttpGet(url.toURI());

                HttpClient httpclient = new DefaultHttpClient();

                HttpResponse response = httpclient.execute(httpRequest);

                HttpEntity entity = response.getEntity();

                BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);

                InputStream instream = bufHttpEntity.getContent();

                bb = BitmapFactory.decodeStream(instream);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cimv.setImageBitmap(bb);
                }
            });

            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();

            QrBitmapFactory.compressToFile(bb, QrBitmapFactory.QrJPEG, f.getAbsolutePath(), 100, opt);

            return null;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

}
