package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class SettingsProgramInfoVersionActivity extends Activity {

    private final String TAG = "versionInfo";

    LinearLayout layBack;
    TextView tvNow, tvNew;
    Button btnUpdate;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_program_info_version);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 버전정보 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_program_info_version_back);

        layBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        tvNow = (TextView) findViewById(R.id.tv_settings_program_info_version_info_now);
        tvNew = (TextView) findViewById(R.id.tv_settings_program_info_version_info_new);

        tvNow.setText(getVersionName());
        tvNew.setText(AppController.getSp().getString("new_ver", ""));

        btnUpdate = (Button) findViewById(R.id.btn_settings_program_info_version_info_update);

        if (tvNow.getText().toString().equals(tvNew.getText().toString())) {
            btnUpdate.setBackgroundResource(R.drawable.btn_version_update);
            btnUpdate.setTextColor(0xffb1b1b1);
            btnUpdate.setText(getResources().getString(R.string.settings_program_info_version_info_no_update));
        } else {
            btnUpdate.setBackgroundResource(R.drawable.btn_version_update_c);
            btnUpdate.setTextColor(0xffffffff);
            btnUpdate.setText(getResources().getString(R.string.settings_program_info_version_info_update));
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvNow.getText().toString().equals(tvNew.getText().toString())) {
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_VIEW);
                    i.setData(Uri
                            .parse(getResources().getString(R.string.market_link)));
                    startActivity(i);
                    finish();
                }
            }
        });

    }

    public String getVersionName() {

        try {
            PackageInfo packageInfo = getApplicationContext()
                    .getPackageManager().getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.i(TAG, "NameNotFoundException => " + e.toString());
            return "";
        }

    }

}
