package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class TermActivity extends Activity {

    TextView tvTitle;
    LinearLayout layBack;
    WebView wv;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_term);

        tvTitle = (TextView) findViewById(R.id.tv_term_title);

        layBack = (LinearLayout) findViewById(R.id.lay_settings_term_back);
        layBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        wv = (WebView) findViewById(R.id.wv_term);
        int navi = getIntent().getExtras().getInt("navi");
        switch (navi) {
            case 0:
                tvTitle.setText(getResources().getString(R.string.settings_program_info_service_agreement));
                if (AppController.getSp().getString("country", "").equals("ko_KR")) {
                    wv.loadUrl(getResources().getString(R.string.term_link_1_ko));
                } else {
                    wv.loadUrl(getResources().getString(R.string.term_link_1_en));
                }
                break;
            case 1:
                tvTitle.setText(getResources().getString(R.string.settings_program_info_privacy_policy));
                if (AppController.getSp().getString("country", "").equals("ko_KR")) {
                    wv.loadUrl(getResources().getString(R.string.term_link_2_ko));
                } else {
                    wv.loadUrl(getResources().getString(R.string.term_link_2_en));
                }
                break;
            case 2:
                tvTitle.setText(getResources().getString(R.string.settings_program_info_operational_policy));
                if (AppController.getSp().getString("country", "").equals("ko_KR")) {
                    wv.loadUrl(getResources().getString(R.string.term_link_3_ko));
                } else {
                    wv.loadUrl(getResources().getString(R.string.term_link_3_en));
                }
                break;
        }

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("약관페이지 " + navi);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

    }
}
