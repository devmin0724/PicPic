package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.InitActivity;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV1Activity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.setteings.SettingsActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignOutPopUpActivity extends Activity {

    TextView tvLeft, tvRight;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_out_pop_up);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("탈퇴 확인 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        tvLeft = (TextView) findViewById(R.id.tv_signout_left);
        tvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });
        tvRight = (TextView) findViewById(R.id.tv_signout_right);
        tvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("SignOut Click").build());
                StringTransMethod stmSignOut = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);

                            if (jd.getInt("result") == 0) {
                                AppController.getSe().clear();
                                AppController.getSe().commit();
                                finish();
//                                MyPageActivity.ac.finish();
                                MyPageV2Activity.ac.finish();
                                SettingsActivity.ac.finish();
                                overridePendingTransition(0, 0);
                                startActivity(new Intent(SignOutPopUpActivity.this, InitActivity.class));
                            }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                Map<String, String> params = new HashMap<>();

                params.put("email", AppController.getSp().getString("email", ""));

                AppController.apiDataTaskNew = new APIDataTaskNew(SignOutPopUpActivity.this, params, 218, stmSignOut);
                AppController.apiDataTaskNew.execute();

            }
        });
    }
}
