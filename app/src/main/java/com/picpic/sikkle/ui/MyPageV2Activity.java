package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.SampleAdapter;
import com.picpic.sikkle.adapter.TimeLineAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.MyPageItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.ui.setteings.SettingsActivity;
import com.picpic.sikkle.ui.setteings.SettingsChangeProfileActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedUserImageView;
import com.picpic.sikkle.widget.HashTagForInterest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyPageV2Activity extends Activity implements View.OnClickListener {

    private static final int PROFILE_PICTURE_CHANGE_RETURN = 1004;

    public static Activity ac;
    LinearLayout laySort1, laySort2, imvShare, imvSettings;
    ImageView imvEdit;
    LinearLayout layTimeline, layInterest, layCamera, layNoti, layMypage;
    FixedUserImageView imvBg;
    TextView tvId, tvPostCnt, tvFollowerCnt, tvFollowingCnt, tvBestTags, tvBestTagsCnt;
    CircleImageView cimv1;
    StaggeredGridView gv;
    EditText edtSearch;
    boolean isGrid = true;
    int nowPage = 1;
    GridResult m_ResultList3 = null;
    SampleAdapter m_ListAdapter3 = null;
    int lastListCount = 0;
    boolean isLoading = false;
    ImageView imvNoti;
    private boolean lastItemVisibleFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_page_v2);

        ac = this;

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = inflater2.inflate(R.layout.haeder_mypage, null, false);

        imvNoti = (ImageView) findViewById(R.id.imv_mypage_noti);

        layTimeline = (LinearLayout) findViewById(R.id.lay_mypage_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_mypage_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_mypage_camera);
        layNoti = (LinearLayout) findViewById(R.id.lay_mypage_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_mypage_mypage);

        laySort1 = (LinearLayout) header.findViewById(R.id.lay_mypage_top1);
        laySort2 = (LinearLayout) header.findViewById(R.id.lay_mypage_top2);

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layNoti.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        laySort1.setOnClickListener(this);
        laySort2.setOnClickListener(this);

        imvEdit = (ImageView) header.findViewById(R.id.imv_mypage_user_edit);
        imvShare = (LinearLayout) header.findViewById(R.id.imv_mypage_share);
        imvSettings = (LinearLayout) header.findViewById(R.id.imv_mypage_settings);
        imvBg = (FixedUserImageView) header.findViewById(R.id.imv_mypage_bg);

        imvEdit.setOnClickListener(this);
        imvShare.setOnClickListener(this);
        imvSettings.setOnClickListener(this);
//        imvSort1.setOnClickListener(this);
//        imvSort2.setOnClickListener(this);
        imvBg.setOnClickListener(this);

        tvId = (TextView) header.findViewById(R.id.tv_mypage_user_id);
        tvPostCnt = (TextView) header.findViewById(R.id.tv_mypage_user_post_cnt);
        tvFollowerCnt = (TextView) header.findViewById(R.id.tv_mypage_user_follower_cnt);
        tvFollowingCnt = (TextView) header.findViewById(R.id.tv_mypage_user_following_cnt);
        tvBestTags = (TextView) header.findViewById(R.id.tv_mypage_user_popular);
        tvBestTagsCnt = (TextView) header.findViewById(R.id.tv_mypage_count);

        tvPostCnt.setOnClickListener(this);
        tvFollowerCnt.setOnClickListener(this);
        tvFollowingCnt.setOnClickListener(this);
        tvBestTags.setOnClickListener(this);
        tvBestTagsCnt.setOnClickListener(this);

        edtSearch = (EditText) header.findViewById(R.id.edt_mypage_input);
        edtSearch.addTextChangedListener(tw);

        edtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edtSearch.setCursorVisible(true);
                } else {
                    edtSearch.setCursorVisible(false);
                }
            }
        });

        cimv1 = (CircleImageView) header.findViewById(R.id.cimv_mypage_user_1);

        gv = (StaggeredGridView) findViewById(R.id.gv_mypage);

        gv.addHeaderView(header);
//        View empty = inflater2.inflate(R.layout.page_mypage_null, null, false);
//        gv.setEmptyView(findViewById(R.id.lay_mypage_v2_empty));

        gv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 25;

                    Log.e("list", count + "/" + page);

                    if ((count - 1) % 25 == 0) {
                        if (!isLoading) {
                            if (nowPage < page + 1) {
                                isLoading = true;
                                if (edtSearch.getText().toString().equals("")) {
                                    getGridData(false, page + 1);
                                } else {
                                    getGridSearch(false, page + 1, edtSearch.getText().toString());
                                }

                                lastListCount = count;
                                nowPage = page + 1;
                            }
                        }
                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        layMypage.setSelected(true);

        laySort1.setSelected(true);
        laySort1.setSelected(true);
        isGrid = true;

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("마이페이지 " + AppController.getSp().getString("email", ""));
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("마이페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        getData();
    }

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 406, stmMypageResult);
        AppController.apiTaskNew.execute();

    }

    private void getGridData(boolean is, int page) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
        params.put("str", "");
        if (laySort1.isSelected()) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }
        params.put("timeline", "F");

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();

    }

    private void getGridSearch(boolean is, int page, String str) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
        params.put("str", str);
        if (laySort1.isSelected()) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();
    }

    StringTransMethod stmMypageResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                final JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    final JSONArray p_tag_1 = new JSONArray(jd.getString("p_tag_1"));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                cimv1.setImageURLString(AppController.URL + jd.getString("profile_picture"));
                                imvBg.setImageURLString(AppController.URL + jd.getString("profile_picture"));

                                tvId.setText(jd.getString("id"));

                                int p_tag_length = p_tag_1.length();

                                if (!p_tag_1.getJSONObject(0).getString("tag_id").equals("null")) {
                                    tvBestTags.setText("#" + p_tag_1.getJSONObject(0).getString("tag_name"));
                                    tvBestTags.setTag(p_tag_1.getJSONObject(0).getString("tag_id"));
                                } else {
                                    p_tag_length = 0;
                                }

                                tvBestTagsCnt.setText("+" + p_tag_length);

                                tvFollowerCnt.setText(jd.getInt("follower_cnt") + "");
                                tvFollowingCnt.setText(jd.getInt("follow_cnt") + "");

                                tvPostCnt.setText(jd.getInt("post_cnt") + "");

                                String commentsText = tvBestTags.getText().toString();

                                ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');

                                SpannableString commentsContent = new SpannableString(commentsText);

                                for (int i = 0; i < hashtagSpans.size(); i++) {
                                    int[] span = hashtagSpans.get(i);
                                    int hashTagStart = span[0];
                                    int hashTagEnd = span[1];

                                    commentsContent.setSpan(new HashTagForInterest(MyPageV2Activity.this),
                                            hashTagStart, hashTagEnd, 0);
                                }

                                tvBestTags.setMovementMethod(LinkMovementMethod.getInstance());
                                tvBestTags.setText(commentsContent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            } finally {
                getGridData(true, 1);
            }
        }
    };

    StringTransMethod stmInitListRestult2 = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                MinUtils.d("rr", result);
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
                    if(jd.isNull("data")){
                        if(m_ListAdapter3 == null){
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    }
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    Log.e("length", jarr.length() + "");

                    if (jarr.length() < 1) {
                        Log.e("length", jarr.length() + "");
                        if(m_ListAdapter3.getCount() < 2){
                            Log.e("length", jarr.length() + "");
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    } else {
                        GridResult gr = new GridResult();
                        GridItem gi;
                        JSONObject j;
                        for (int i = 0; i < jarr.length(); i++) {
                            gi = new GridItem();

                            j = jarr.getJSONObject(i);

                            gi.setUrl(j.getString("url"));
                            gi.setPost_id(j.getString("post_id"));

                            if (!j.isNull("height1")) {
                                gi.setHeight1(j.getInt("height1"));

                            }
                            if (!j.isNull("height2")) {

                                gi.setHeight2(j.getInt("height2"));
                            }
                            if (!j.isNull("heightTh")) {
                                gi.setHeightTH(j.getInt("heightTh"));

                            }
                            if (!j.isNull("width1")) {

                                gi.setWidth1(j.getInt("width1"));
                            }
                            if (!j.isNull("width2")) {

                                gi.setWidth2(j.getInt("width2"));
                            }
                            if (!j.isNull("widthTh")) {
                                gi.setWidthTH(j.getInt("widthTh"));

                            }
                            if (!j.isNull("size1")) {

                                gi.setSize1(j.getInt("size1"));
                            }
                            if (!j.isNull("size2")) {
                                gi.setSize2(j.getInt("size2"));

                            }
                            if (!j.isNull("sizeTh")) {
                                gi.setSizeTH(j.getInt("sizeTh"));

                            }


                            gr.add(gi);

                        }
                        updateResultList3(gr);
                    }
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            } finally {
                isLoading = false;
            }
        }
    };
    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getGridSearch(true, 1, edtSearch.getText().toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    protected boolean updateResultList3(GridResult resultList) {

        Log.e("o1", "o1");

        if (resultList == null || resultList.size() == 0) {
            Log.e("o2", "o2");
            return false;
        }

        if (m_ResultList3 != null && gv != null && m_ListAdapter3 != null) {
            Log.e("o3", "o3");
            if (m_ResultList3 != resultList) {
                Log.e("o4", "o4");
                m_ResultList3.addAll(resultList);
                m_ListAdapter3.notifyDataSetChanged();

                m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

                gv.setAdapter(m_ListAdapter3);
            }
            return true;
        }
        Log.e("o5", "o5");
        m_ResultList3 = resultList;

        m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

        gv.setAdapter(m_ListAdapter3);

        isLoading = false;

        return true;
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(MyPageV2Activity.this, UserListActivity.class);
        switch (v.getId()) {
            case R.id.lay_mypage_timeline:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MyPageV2Activity.this, TimelineV1NewActivity.class));
                finish();
                break;
            case R.id.lay_mypage_interest:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MyPageV2Activity.this, InterestFeedV1Activity.class));
                finish();
                break;
            case R.id.lay_mypage_camera:
                startActivity(new Intent(MyPageV2Activity.this, CameraPopUp.class));
                break;
            case R.id.lay_mypage_noti:
                overridePendingTransition(0, 0);
                startActivity(new Intent(MyPageV2Activity.this, NotificationActivity.class));
                finish();
                break;
            case R.id.lay_mypage_mypage:
                break;
//            case R.id.imv_mypage_non_user_edit:
            case R.id.imv_mypage_user_edit:
                //TODO 사용자 정보 변경페이지로 이동
                startActivityForResult(new Intent(MyPageV2Activity.this, SettingsChangeProfileActivity.class), PROFILE_PICTURE_CHANGE_RETURN);
                break;
            case R.id.tv_mypage_user_follower_cnt:
                i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWER);
                i.putExtra("id", AppController.getSp().getString("email", ""));
                startActivity(i);
                break;
            case R.id.tv_mypage_user_following_cnt:
                i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWING);
                i.putExtra("id", AppController.getSp().getString("email", ""));
                startActivity(i);
                break;
            case R.id.imv_mypage_share:
                break;
            case R.id.imv_mypage_settings:
                //TODO 설정페이지 이동
                startActivity(new Intent(MyPageV2Activity.this, SettingsActivity.class));
                break;
            case R.id.tv_mypage_count:
            case R.id.tv_mypage_user_popular:
                //TODO 인기태그 페이지 이동
                break;
            case R.id.lay_mypage_top1:
                if (!laySort1.isSelected()) {
                    laySort1.setSelected(true);
                    laySort2.setSelected(false);
                    getData();
                }
                break;
            case R.id.lay_mypage_top2:
                if (!laySort2.isSelected()) {
                    laySort1.setSelected(false);
                    laySort2.setSelected(true);
                    getData();
                }
                break;
            case R.id.imv_mypage_sort_1:
                isGrid = false;
                if (laySort1.isSelected()) {
                    getData();
                } else {
                    getData();
                }
                break;
            case R.id.imv_mypage_sort_2:
                isGrid = true;
                if (laySort1.isSelected()) {
                    getData();
                } else {
                    getData();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
//        startActivity(new Intent(MyPageActivity.this, NewGridTimelineActivity.class));
        startActivity(new Intent(MyPageV2Activity.this, TimelineV1NewActivity.class));
    }
}
