package com.picpic.sikkle.ui;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.flowpadview.GlowPadView;

public class NewLockActivity extends Activity implements GlowPadView.OnTriggerListener {

    GlowPadView mGlowPadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_lock);

        Log.e("lock", "activity");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("잠금화면 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        mGlowPadView = (GlowPadView) findViewById(R.id.glow_pad_view);

        mGlowPadView.setOnTriggerListener(this);

        // uncomment this to make sure the glowpad doesn't vibrate on touch
        // mGlowPadView.setVibrateEnabled(false);

        // uncomment this to hide targets
        mGlowPadView.setShowTargetsOnIdle(true);
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public void onGrabbed(View v, int handle) {

    }

    @Override
    public void onReleased(View v, int handle) {

    }

    @Override
    public void onTrigger(View v, int target) {
        final int resId = mGlowPadView.getResourceIdForTarget(target);
        switch (resId) {
            case R.drawable.ic_item_camera:
                Toast.makeText(this, "Camera selected", Toast.LENGTH_SHORT).show();
                break;

            case R.drawable.ic_item_google:
                Toast.makeText(this, "Google selected", Toast.LENGTH_SHORT).show();

                break;
            default:
                // Code should never reach here.
        }
    }

    @Override
    public void onGrabbedStateChange(View v, int handle) {

    }

    @Override
    public void onFinishFinalAnimation() {

    }
}
