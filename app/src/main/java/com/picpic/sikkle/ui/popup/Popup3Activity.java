package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class Popup3Activity extends Activity implements View.OnClickListener {

//    LinearLayout laySave, layFace, layTwitter, layTumblr, layPinterest;
    LinearLayout laySave, layFace, layTumblr, layPinterest;
//    ImageView imvFace, imvTwitter, imvTumblr, imvPinter;

    String tempPostId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_popup3);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("Popup3Activity");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void initViews() {
        laySave = (LinearLayout) findViewById(R.id.lay_popup3_1);
        layFace = (LinearLayout) findViewById(R.id.lay_popup3_2);
//        layTwitter = (LinearLayout) findViewById(R.id.lay_popup3_3);
//        layTumblr = (LinearLayout) findViewById(R.id.lay_popup3_4);
//        layPinterest = (LinearLayout) findViewById(R.id.lay_popup3_5);

//        imvFace = (ImageView) findViewById(R.id.imv_popup3_facebook);
//        imvTwitter = (ImageView) findViewById(R.id.imv_popup3_twitter);
//        imvTumblr = (ImageView) findViewById(R.id.imv_popup3_tumblr);
//        imvPinter = (ImageView) findViewById(R.id.imv_popup3_pinterest);

        laySave.setOnClickListener(this);
        layFace.setOnClickListener(this);
//        layTwitter.setOnClickListener(this);
        layTumblr.setOnClickListener(this);
        layPinterest.setOnClickListener(this);

        tempPostId = getIntent().getExtras().getString("postId");
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        switch (v.getId()) {
            case R.id.lay_popup3_1:
                i.putExtra("result", 0);
                i.putExtra("postId", tempPostId);
                setResult(RESULT_OK, i);
                finish();
                break;
//            case R.id.lay_popup3_2:
//                if (imvFace.isSelected()) {
//                    imvFace.setSelected(false);
//                } else {
//                    imvFace.setSelected(true);
//                }
//                break;
//            case R.id.lay_popup3_3:
//                if (imvTwitter.isSelected()) {
//                    imvTwitter.setSelected(false);
//                } else {
//                    imvTwitter.setSelected(true);
//                }
//                break;
//            case R.id.lay_popup3_4:
//                if (imvTumblr.isSelected()) {
//                    imvTumblr.setSelected(false);
//                } else {
//                    imvTumblr.setSelected(true);
//                }
//                break;
//            case R.id.lay_popup3_5:
//                if (imvPinter.isSelected()) {
//                    imvPinter.setSelected(false);
//                } else {
//                    imvPinter.setSelected(true);
//                }
//                break;
        }
    }
}
