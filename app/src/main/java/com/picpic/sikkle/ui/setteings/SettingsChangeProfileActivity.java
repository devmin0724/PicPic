package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.login_join.ProfilePicture2Activity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsChangeProfileActivity extends Activity implements View.OnClickListener {

    private final int PROFILE_PICTURE_RETURN = 1113;
//    EditTextCheckIcon etciId, etciEmail;
    EditText edtId, edtEmail;
    CircleImageView cimv;
    ImageView imvEdit;
    LinearLayout layBack;
    FrameLayout imvComplete;
    ProgressBar pb;
    TextWatcher idTW = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {

                switch (s.charAt(s.length() - 1)) {
                    case ' ':
                        edtId.setText(s.toString().replace(" ", "_"));
                        edtId.setSelection(edtId.length());
                        break;
                    case ',':
                    case '.':
                    case '/':
                    case '?':
                    case '`':
                    case '&':
                    case '#':
                    case '@':
                    case '\\':
                    case '|':
                    case '"':
                        edtId.setText(edtId.getText().toString().substring(0, edtId.getText().toString().length() - 2));
                        edtId.setSelection(edtId.length());
                        break;
                    default:
                        if (s.length() > 20) {
                            edtId.setText(s.toString().substring(0, 20));
                            edtId.setSelection(edtId.length());
                        }
                        break;
                }

            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_change_profile);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 프로필 변경 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        pb = (ProgressBar) findViewById(R.id.pb_change_profile);
        pb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

//        etciId = (EditTextCheckIcon) findViewById(R.id.etci_settings_change_profile_id);
//        etciEmail = (EditTextCheckIcon) findViewById(R.id.etci_settings_change_profile_email);

        edtId = (EditText) findViewById(R.id.edt_settings_change_profile_id);
        edtEmail = (EditText) findViewById(R.id.edt_settings_change_profile_email);

        cimv = (CircleImageView) findViewById(R.id.cimv_settings_change_profile);

        imvEdit = (ImageView) findViewById(R.id.imv_settings_change_profile_edit);

        layBack = (LinearLayout) findViewById(R.id.lay_settings_change_profile_back);

        imvComplete = (FrameLayout) findViewById(R.id.imv_settings_change_profile_next);

        imvEdit.setOnClickListener(this);

        layBack.setOnClickListener(this);

        imvComplete.setOnClickListener(this);

//        etciId.setOnClickListener(this);

        edtId.setText(AppController.getSp().getString("id", ""));
        if (AppController.getSp().getString("register_form", "10001").equals("10001")) {
            edtEmail.setText(AppController.getSp().getString("email", ""));
        } else {
            edtEmail.setText(AppController.getSp().getString("facebook_id", ""));
        }

        edtId.addTextChangedListener(idTW);

        edtId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
//                    if (edtId.getText().toString().equals("")) {
//                        etciId.setState(EditTextCheckIcon.CHOICE_NON);
//                    } else {
//                        etciId.setState(EditTextCheckIcon.CHOICE_X);
//                    }
                    edtId.setCursorVisible(true);
                } else {
                    edtId.setCursorVisible(false);
//                    etciId.setState(EditTextCheckIcon.CHOICE_NON);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        cimv.setImageURLString(AppController.URL + AppController.getSp().getString("profile_picture", ""));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_settings_change_profile_edit:
                startActivityForResult(new Intent(SettingsChangeProfileActivity.this, ProfilePicture2Activity.class), PROFILE_PICTURE_RETURN);
                break;
            case R.id.lay_settings_change_profile_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_settings_change_profile_next:
                pb.setVisibility(View.VISIBLE);
                if (edtId.getText().toString().equals("")) {

                } else {
                    StringTransMethod stmChangeProfile = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);

                                if (jd.getInt("result") == 0) {
                                    StringTransMethod stmChangeID = new StringTransMethod() {
                                        @Override
                                        public void endTrans(String result) {
                                            try {
                                                JSONObject jd2 = new JSONObject(result);

                                                if (jd2.getInt("result") == 0) {
                                                    AppController.getSe().putString("id", edtId.getText().toString());
                                                    AppController.getSe().commit();
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.id_change_return_success), Toast.LENGTH_SHORT).show();
                                                    finish();
                                                    overridePendingTransition(0, 0);
                                                } else if (jd2.getInt("result") == 100 || jd2.getInt("result") == 1000) {
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {

                                            }finally {
                                                pb.setVisibility(View.GONE);
                                            }
                                        }
                                    };
                                    Map<String, String> params2 = new HashMap<>();

                                    params2.put("my_id", AppController.getSp().getString("email", ""));
                                    params2.put("new_id", edtId.getText().toString());

                                    AppController.apiDataTaskNew = new APIDataTaskNew(SettingsChangeProfileActivity.this, params2, 215, stmChangeID);
                                    AppController.apiDataTaskNew.execute();
                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.id_check_return_exists), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    Map<String, String> params = new HashMap<>();
                    params.put("id", edtId.getText().toString());

                    AppController.apiTaskNew = new APITaskNew(this, params, 214, stmChangeProfile);
                    AppController.apiTaskNew.execute();

                }
                break;
//            case R.id.etci_settings_change_profile_id:
//                edtId.setText("");
//                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PROFILE_PICTURE_RETURN:
                if (resultCode == RESULT_OK) {
                    finish();
                    setResult(RESULT_OK);
                }
                break;
        }
    }
}
