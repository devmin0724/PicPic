package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.messenger.MessengerThreadParams;
import com.facebook.messenger.MessengerUtils;
import com.facebook.messenger.ShareToMessengerParams;
import com.picpic.sikkle.R;

import java.util.List;

public class FaceBookMessengerActivity extends Activity {

    private boolean mPicking;
    private final int PICK_FROM_ALBUM = 1111;
    private final int REQUEST_CODE_SHARE_TO_MESSENGER = 1112;

    String mimeType = "image/gif";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_book_messenger);

        Intent intent = getIntent();

        if (Intent.ACTION_PICK.equals(intent.getAction())) {
            mPicking = true;
            MessengerThreadParams mThreadParams = MessengerUtils.getMessengerThreadParamsForIntent(intent);

            String metadata = mThreadParams.metadata;
            List<String> participantIds = mThreadParams.participants;
        }

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        i.setDataAndType(data, mimeType);

        startActivityForResult(i, PICK_FROM_ALBUM);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICK_FROM_ALBUM :
                if(resultCode == RESULT_OK){

                    Uri contentUri = data.getData();

                    ShareToMessengerParams shareToMessengerParams =
                            ShareToMessengerParams.newBuilder(contentUri, "image/gif")
                                    .setMetaData("{ \"image\" : \"trees\" }")
                                    .build();

                    if (mPicking) {
                        MessengerUtils.finishShareToMessenger(this, shareToMessengerParams);
                    } else {
                        MessengerUtils.shareToMessenger(
                                this,
                                REQUEST_CODE_SHARE_TO_MESSENGER,
                                shareToMessengerParams);
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }
}
