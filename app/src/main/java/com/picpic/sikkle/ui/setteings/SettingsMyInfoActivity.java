package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class SettingsMyInfoActivity extends Activity implements View.OnClickListener {

    LinearLayout layProfile, layPassword, layNation, layBack;
    TextView tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_my_info);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 내정보 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_myinfo_back);
        layProfile = (LinearLayout) findViewById(R.id.lay_settings_myinfo_change_profile);
        layPassword = (LinearLayout) findViewById(R.id.lay_settings_myinfo_change_password);
        layNation = (LinearLayout) findViewById(R.id.lay_settings_myinfo_change_nation);

        layBack.setOnClickListener(this);
        layProfile.setOnClickListener(this);
        layPassword.setOnClickListener(this);
        layNation.setOnClickListener(this);

        tvEmail = (TextView) findViewById(R.id.tv_settings_myinfo_email);

        if(AppController.getSp().getString("register_form", "10001").equals("10001")){
            tvEmail.setText(AppController.getSp().getString("email", ""));
        }else{
            tvEmail.setText(AppController.getSp().getString("facebook_id", ""));
        }

    }
    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_myinfo_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_myinfo_change_profile:
                startActivity(new Intent(SettingsMyInfoActivity.this, SettingsChangeProfileActivity.class));
                break;
            case R.id.lay_settings_myinfo_change_password:
                startActivity(new Intent(SettingsMyInfoActivity.this, SettingsChangePasswordActivity.class));
                break;
            case R.id.lay_settings_myinfo_change_nation:
//                startActivity(new Intent(SettingsMyInfoActivity.this, SettingsChangeProfileActivity.class));
                break;
        }
    }
}
