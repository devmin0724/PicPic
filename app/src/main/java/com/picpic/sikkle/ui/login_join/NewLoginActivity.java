package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.NewGridTimelineActivity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.ui.popup.ForgotPopUpActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class NewLoginActivity extends Activity implements View.OnClickListener, View.OnFocusChangeListener {
    EditText edtEmail, edtPw;
    LinearLayout layId, layPw, layForgot;
    TextView tvError;
    EditTextCheckIcon etciID, etciPW;
    Animation aniShake;
    Button btnLogin;
    ImageView imvEmail, imvPass, imvBG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_login);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("로그인 2번째 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        aniShake = AnimationUtils.loadAnimation(this, R.anim.shake);

        imvEmail = (ImageView) findViewById(R.id.imv_new_login_email);
        imvPass = (ImageView) findViewById(R.id.imv_new_login_pass);

        imvBG = (ImageView) findViewById(R.id.imv_new_login);

//        try {
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("login_bg_1.gif"));
//            imvBG.setImageDrawable(gd);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        imvBG.setImageResource(R.drawable.loginimv);

        edtEmail = (EditText) findViewById(R.id.edt_new_login_email);
        edtPw = (EditText) findViewById(R.id.edt_new_login_pass);

        etciID = (EditTextCheckIcon) findViewById(R.id.etci_new_login_email);
        etciPW = (EditTextCheckIcon) findViewById(R.id.etci_new_login_pass);

        etciID.setOnClickListener(this);
        etciPW.setOnClickListener(this);

        edtEmail.setOnFocusChangeListener(this);
        edtPw.setOnFocusChangeListener(this);

        btnLogin = (Button) findViewById(R.id.btn_new_login_log_in);
        btnLogin.setOnClickListener(this);

        layId = (LinearLayout) findViewById(R.id.lay_new_login_email);
        layPw = (LinearLayout) findViewById(R.id.lay_new_login_pass);

        layForgot = (LinearLayout) findViewById(R.id.lay_new_login_forgot);
        layForgot.setOnClickListener(this);
        tvError = (TextView) findViewById(R.id.tv_new_login_error);

    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private int isEmpty() {
        if (edtEmail.getText().toString().equals("") || edtEmail.getText().toString().equals(null)) {
            return 1;
        } else {
            if (edtPw.getText().toString().equals("") || edtEmail.getText().toString().equals(null)) {
                return 2;
            } else {
                return 0;
            }
        }
    }

    StringTransMethod stmLogin = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    JSONObject loginData = new JSONObject(jd.getString("data"));
                    SharedPreferences.Editor se = AppController.getSe();
                    se.putString("bir_year", loginData.getString("bir_year"));
                    se.putString("bir_mon", loginData.getString("bir_mon"));
                    se.putString("bir_day", loginData.getString("bir_day"));
                    se.putString("country", loginData.getString("country"));
                    se.putString("cut_day", loginData.getString("cut_day"));
                    se.putString("cut_reason", loginData.getString("cut_reason"));
                    se.putString("declare_cnt", loginData.getString("declare_cnt"));
                    se.putString("device_id", loginData.getString("device_id"));
                    se.putString("email", loginData.getString("email"));
                    se.putString("facebook_id", loginData.getString("facebook_id"));
                    se.putString("fri_open_yn", loginData.getString("fri_open_yn"));
                    se.putString("id", loginData.getString("id"));
                    se.putString("m_id", loginData.getString("m_id"));
                    se.putString("pinterest_id", loginData.getString("pinterest_id"));
                    se.putString("posts_open_form", loginData.getString("posts_open_form"));
                    se.putString("profile_picture", loginData.getString("profile_picture"));
                    se.putString("push_token", loginData.getString("push_token"));
                    se.putString("regist_day", loginData.getString("regist_day"));
                    se.putString("register_form", loginData.getString("register_form"));
                    se.putString("sex", loginData.getString("sex"));
                    se.putString("tumbler_id", loginData.getString("tumbler_id"));
                    se.putString("twitter_id", loginData.getString("twitter_id"));
                    se.putBoolean("isAutoLogin", true);
                    se.commit();

                    AppController.t.set("&uid", loginData.getString("email"));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory("Login").build());

//                    Intent i = new Intent(NewLoginActivity.this, NewGridTimelineActivity.class);
                    Intent i = new Intent(NewLoginActivity.this, TimelineV1NewActivity.class);
                    i.putExtra("navi", 0);
                    startActivity(i);
                    LoginActivity._ac.finish();
                    finish();
                }else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                } else {
                    tvError.setVisibility(View.VISIBLE);
                    layId.startAnimation(aniShake);
                    layPw.startAnimation(aniShake);
                    tvError.startAnimation(aniShake);
                    AppController.getSe().putBoolean("isAutoLogin", false);
                    AppController.getSe().commit();
                }
            } catch (JSONException e) {

            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_new_login_log_in:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Login Click").build());
                switch (isEmpty()) {
                    case 0:
                        Date now = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                        String tempC = sdf.format(now);

                        HashMap<String, String> params = new HashMap<>();

                        params.put("email", edtEmail.getText().toString());
                        params.put("password", edtPw.getText().toString());

                        AppController.getSe().putString("password", edtPw.getText().toString());
                        AppController.getSe().commit();

                        params.put("register_form", "10001");
                        params.put("country", AppController.getSp().getString("country", "ko_KR"));
                        params.put("device_id", MinUtils.DID);
                        params.put("push_token", MinUtils.PTK);
                        params.put("regist_day", tempC);

                        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 202, stmLogin);
                        AppController.apiDataTaskNew.execute();

                        break;
                    case 1:
                        tvError.setVisibility(View.VISIBLE);
                        layId.startAnimation(aniShake);
                        tvError.startAnimation(aniShake);
                        break;
                    case 2:
                        tvError.setVisibility(View.VISIBLE);
                        layPw.startAnimation(aniShake);
                        tvError.startAnimation(aniShake);
                        break;
                }
                break;
            case R.id.etci_new_login_email:
                Log.e("aaa", "ok");
                Log.e("ccc", etciID.getState() + "");
                if (edtEmail.isFocused()) {
                    edtEmail.setText("");
                }
                break;
            case R.id.etci_new_login_pass:
                Log.e("bbb", "ok");
                if (edtPw.isFocused()) {
                    edtPw.setText("");
                }
                break;
            case R.id.lay_new_login_forgot:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Forgot Click").build());
                startActivity(new Intent(NewLoginActivity.this, ForgotPopUpActivity.class));
                break;
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.edt_new_login_email:
                if (hasFocus) {
                    etciID.setState(EditTextCheckIcon.CHOICE_X);
                    etciPW.setState(EditTextCheckIcon.CHOICE_NON);
                    imvEmail.setSelected(true);
                    if (edtPw.getText().toString().length() > 0) {
                        imvPass.setSelected(true);
                    } else {
                        imvPass.setSelected(false);
                    }
                }
                break;
            case R.id.edt_new_login_pass:
                if (hasFocus) {
                    etciID.setState(EditTextCheckIcon.CHOICE_NON);
                    etciPW.setState(EditTextCheckIcon.CHOICE_X);
                    imvPass.setSelected(true);
                    if (edtEmail.getText().toString().length() > 0) {
                        imvEmail.setSelected(true);
                    } else {
                        imvEmail.setSelected(false);
                    }
                }
                break;
        }
    }
}
