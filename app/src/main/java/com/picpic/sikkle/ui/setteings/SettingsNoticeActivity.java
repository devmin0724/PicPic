package com.picpic.sikkle.ui.setteings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.NoticeChildItem;
import com.picpic.sikkle.beans.NoticeGroupItem;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettingsNoticeActivity extends Activity implements View.OnClickListener {

    AnimatedExpandableListView list;
    LinearLayout imvBack;
    NoticeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_notice);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 공지사항 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        list = (AnimatedExpandableListView) findViewById(R.id.list_settings_notice);
        imvBack = (LinearLayout) findViewById(R.id.lay_settings_notice_back);
        imvBack.setOnClickListener(this);

        getData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void getData() {

        StringTransMethod stmNoticeList = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    Log.e("notice", result);
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        JSONArray jarr = new JSONArray(jd.getString("notice"));

                        List<NoticeGroupItem> items = new ArrayList<NoticeGroupItem>();

                        for (int i = 0; i < jarr.length(); i++) {
                            NoticeGroupItem item = new NoticeGroupItem();

                            String tempDate = jarr.getJSONObject(i).getString("date");

                            item.setDate(tempDate.substring(5, 10));
                            item.setTitle(jarr.getJSONObject(i).getString("title"));

                            NoticeChildItem child = new NoticeChildItem();
                            child.setBody(jarr.getJSONObject(i).getString("body"));

                            item.items.add(child);

                            items.add(item);
                        }

                        adapter = new NoticeAdapter(getApplicationContext());
                        adapter.setData(items);

                        list = (AnimatedExpandableListView) findViewById(R.id.list_settings_notice);
                        list.setAdapter(adapter);

                        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                            @Override
                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                ImageView imv = (ImageView) v.findViewById(R.id.imv_notice_row_group_title);
                                if (list.isGroupExpanded(groupPosition)) {
                                    list.collapseGroupWithAnimation(groupPosition);
                                    imv.setSelected(false);
                                } else {
                                    list.expandGroupWithAnimation(groupPosition);
                                    imv.setSelected(true);
                                }
                                final int pos = groupPosition;
                                list.post(new Runnable() {
                                    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                                    @Override
                                    public void run() {
                                        list.deferNotifyDataSetChanged();
                                        list.clearFocus();
                                        list.smoothScrollToPositionFromTop(pos, 0, 500);
                                    }
                                });
                                return true;
                            }

                        });
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("myId", AppController.getSp().getString("email", ""));
        params.put("locale", AppController.getSp().getString("country", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 211, stmNoticeList);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_notice_back:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }

    private static class groupHolder {
        ImageView imv;
        TextView date;
        TextView title;
    }

    private static class childHolder {
        TextView body;
    }

    class NoticeAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<NoticeGroupItem> items;

        public NoticeAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<NoticeGroupItem> items) {
            this.items = items;
        }

        @Override
        public NoticeChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public void notifyGroupExpanded(int groupPosition) {
            super.notifyGroupExpanded(groupPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            childHolder holder;
            NoticeChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new childHolder();
                convertView = inflater.inflate(R.layout.row_notice_child, parent, false);
                holder.body = (TextView) convertView.findViewById(R.id.tv_notice_row_child_body);

                convertView.setTag(holder);
            } else {
                holder = (childHolder) convertView.getTag();
            }

            holder.body.setText(item.getBody());

            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public NoticeGroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            groupHolder holder;
            NoticeGroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new groupHolder();
                convertView = inflater.inflate(R.layout.row_notice_group, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.tv_notice_row_group_title);

                holder.imv = (ImageView) convertView.findViewById(R.id.imv_notice_row_group_title);

                convertView.setTag(holder);
            } else {
                holder = (groupHolder) convertView.getTag();
            }

            holder.title.setText(item.getTitle());
            holder.title.setTag(item.getDate());
//            holder.date.setText(item.getDate());

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }
}
