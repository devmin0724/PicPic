package com.picpic.sikkle.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.Test2Activity;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopupActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTag;
import com.picpic.sikkle.widget.NewTimelineScrollView;
import com.picpic.sikkle.widget.supertooltips.ToolTip;
import com.picpic.sikkle.widget.supertooltips.ToolTipRelativeLayout;
import com.picpic.sikkle.widget.supertooltips.ToolTipView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;

public class NewTimeLineActivity extends FragmentActivity implements View.OnClickListener {

    public static TimelineResult m_ResultList;
    boolean isEnd = false;
    BoundableOfflineImageView bimv;
    NewTimelineScrollView sv;
    ImageView imvMargin;

    int gifWidth = 200, gifHeight = 200;

    String tempDate = "";
    int tempPostCount = 0;

    int edge = 0;
    boolean firstDragFlag = true;
    boolean dragFlag = false;   //현재 터치가 드래그 인지 확인
    boolean motionFlag = false;   //현재 터치가 드래그 인지 확인
    float startYPosition = 0;
    float endYPosition = 0;

    int pageNavi = 0;
    int page = 1;
    int svHeight;
    int viewHeight;
    boolean isFull = true;
    LinearLayout layContent, layMargin;

    Animation up1, up2, down1, down2;

    private static final int TIMELINE_RETURN = 1001;
    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;
    public static final int COMMENT_RETURN = 1006;

    LinearLayout layIntro, imvHome, imvAlarm;
    ImageView imvChatting, imvNoti;
    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch;

    ProgressBar pb;
    ToolTipRelativeLayout toolTipRelativeLayout;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_time_line);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 구버전2");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

//        startActivity(new Intent(NewTimeLineActivity.this, FingerPaint.class));
//        startActivity(new Intent(NewTimeLineActivity.this, TestActivity.class));
        startActivity(new Intent(NewTimeLineActivity.this, Test2Activity.class));

//        vp = (VerticalViewPager) findViewById(R.id.vp_new_timeline);
//
//        vp.setOffscreenPageLimit(10);


        toolTipRelativeLayout = (ToolTipRelativeLayout) findViewById(R.id.tooltipRelativeLayout_new_timeline);
        imvMargin = (ImageView) findViewById(R.id.imv_new_timeline_margin);

        try {
            GifDrawable gd = new GifDrawable(getResources().getAssets().open("slide_top.gif"));
            gd.setSpeed(2.0f);
            imvMargin.setImageDrawable(gd);
        } catch (IOException e) {
            e.printStackTrace();
        }
//
//        Handler handler = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//
//            }
//        };
//
//        handler.sendEmptyMessageDelayed(0, 1000);

        pb = (ProgressBar) findViewById(R.id.pb_new_timeline);

        imvHome = (LinearLayout) findViewById(R.id.imv_main_new_timeline_home);

        imvNoti = (ImageView) findViewById(R.id.imv_new_timeline_noti);

        laySearch = (LinearLayout) findViewById(R.id.imv_new_timeline_search);
        laySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Search Click").build());
                overridePendingTransition(0, 0);
                Intent ii = new Intent(NewTimeLineActivity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 0);
                startActivity(ii);
            }
        });
        layTimeline = (LinearLayout) findViewById(R.id.lay_new_timeline_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_new_timeline_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_new_timeline_camera);
        layAndTag = (LinearLayout) findViewById(R.id.lay_new_timeline_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_new_timeline_mypage);
        layContent = (LinearLayout) findViewById(R.id.lay_new_timeline_content);

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layAndTag.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        layTimeline.setSelected(true);

//        up1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_to_top);
//        up1.setFillAfter(true);
//        up2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_bottom);
//        up2.setFillAfter(true);
//        down1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_to_bottom);
//        down1.setFillAfter(true);
//        down2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_top);
//        down2.setFillAfter(true);

        up1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sv.startAnimation(up2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        down1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sv.startAnimation(down2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        up2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                AppController.gifExe.shutdownNow();
                //TODO 데이터 넣기
                if (pageNavi == m_ResultList.size() - 3) {
                    page = page + 1;
                    getData(false, page);
                } else {
                    pageNavi = pageNavi + 1;
                }

                settingData(pageNavi);
                pb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sv.scrollTo(0, 0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        down2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                AppController.gifExe.shutdownNow();
                //TODO 데이터 넣기
                if (pageNavi != 0) {
                    pageNavi = pageNavi - 1;
                } else {
                    pageNavi = 0;
                }

                settingData(pageNavi);
                pb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sv.scrollTo(0, 0);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        sv = (NewTimelineScrollView) findViewById(R.id.sv_test);

        layMargin = (LinearLayout) findViewById(R.id.lay_new_timeline_margin);


        sv.setOnEdgeTouchListener(new NewTimelineScrollView.OnEdgeTouchListener() {
            @Override
            public void onEdgeTouch(NewTimelineScrollView.DIRECTION direction) {
                if (direction == NewTimelineScrollView.DIRECTION.TOP) {
                    edge = 1;
                } else if (direction == NewTimelineScrollView.DIRECTION.BOTOM) {
                    edge = 2;
                } else if (direction == NewTimelineScrollView.DIRECTION.NONE) {
                    edge = 0;
                }
            }
        });
        sv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent ev) {
                switch (ev.getAction()) {
                    case MotionEvent.ACTION_MOVE:       //터치를 한 후 움직이고 있으면
                        dragFlag = true;
                        if (firstDragFlag) {     //터치후 계속 드래그 하고 있다면 ACTION_MOVE가 계속 일어날 것임으로 무브를 시작한 첫번째 터치만 값을 저장함
                            startYPosition = ev.getY(); //첫번째 터치의 Y(높이)를 저장
                            firstDragFlag = false;   //두번째 MOVE가 실행되지 못하도록 플래그 변경
                        }

                        break;

                    case MotionEvent.ACTION_UP:
                        endYPosition = ev.getY();
                        firstDragFlag = true;

//                        int svHeight = sv.getHeight();
//                        int viewHeight = layContent.getHeight();
//                        layContent.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                svHeight = sv.getHeight();
//                                viewHeight = layContent.getHeight();
//                            }
//                        });

//                        int svHeight = sv.getMeasuredHeight();
//                        int viewHeight = layContent.getMeasuredHeight();


                        Log.e("svIs", isFull + "");

                        if (dragFlag) {  //드래그를 하다가 터치를 실행
                            // 시작Y가 끝 Y보다 크다면 터치가 아래서 위로 이루어졌다는 것이고, 스크롤은 아래로내려갔다는 뜻이다.
                            // (startYPosition - endYPosition) > 10 은 터치로 이동한 거리가 10픽셀 이상은 이동해야 스크롤 이동으로 감지하겠다는 뜻임으로 필요하지 않으면 제거해도 된다.
                            if ((startYPosition > endYPosition) && (startYPosition - endYPosition) > 10) {

                                if (edge == 2) {
                                    sv.startAnimation(up1);
                                }
                                //TODO 스크롤 다운 시 작업
                            }
                            //시작 Y가 끝 보다 작다면 터치가 위에서 아래로 이러우졌다는 것이고, 스크롤이 올라갔다는 뜻이다.
                            else if ((startYPosition < endYPosition) && (endYPosition - startYPosition) > 10) {
                                //TODO 스크롤 업 시 작업
                                if (edge == 1) {
                                    if (pageNavi != 0) {
                                        sv.startAnimation(down1);
                                    }
                                }

                            }
                        }

                        startYPosition = 0.0f;
                        endYPosition = 0.0f;
                        motionFlag = false;
                        break;
                }
                return false;
            }
        });

        getData(true, 1);

    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
////        super.onWindowFocusChanged(hasFocus);
//        svHeight = sv.getHeight();
//        viewHeight = layContent.getHeight();
//    }

    @Override
    protected void onResume() {
        notiCheck();
        super.onResume();
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            isEnd = false;
            String return_msg = result;
            Log.e("return_data2", return_msg);
            MinUtils.d("return_data2", return_msg);
            try {
                JSONObject jd = new JSONObject(return_msg);

                if(jd.getInt("result") == 0){
                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineResult tr = new TimelineResult();
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;
                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

                        tr.add(ti);
                    }

//                isEnd = jarr.length() != 10;
//null

                    tempPostCount = jd.getInt("posting_cnt");

                    if (tempPostCount == 0) {
                        //TODO 띄워라
                        showToolTip();
                    } else {
                        if (!jd.isNull("last_post")) {
                            tempDate = jd.getString("last_post");
                        }
                        if (!tempDate.equals("")) {
                            // 현재날짜와 일수 비교

                            int year = Integer.parseInt(tempDate.substring(0, 3));
                            int month = Integer.parseInt(tempDate.substring(3, 5));
                            int day = Integer.parseInt(tempDate.substring(5, 7));

                            Calendar c = Calendar.getInstance();
                            int doy = c.get(Calendar.DAY_OF_YEAR);

                            Calendar c2 = Calendar.getInstance();
                            c2.set(year, month, day);

                            int doy2 = c2.get(Calendar.DAY_OF_YEAR);

                            if (doy2 > doy) {
                                if (doy2 - doy >= 10) {
                                    //TODO 띄워라
                                    showToolTip();
                                }
                            }
                        }
                    }


                    updateResultList(tr);

                    isEnd = true;
                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void showToolTip() {

        ToolTip toolTip = new ToolTip()
                .withContentView(LayoutInflater.from(NewTimeLineActivity.this).inflate(R.layout.customtooltip, null))
                .withAnimationType(ToolTip.AnimationType.FROM_MASTER_VIEW)
                .withColor(0xffffe766);
        ToolTipView ttv = toolTipRelativeLayout.showToolTipForView(toolTip, findViewById(R.id.lay_new_timeline_camera));
        ttv.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
            @Override
            public void onToolTipViewClicked(ToolTipView toolTipView) {
//                Toast.makeText(getApplicationContext(), "ㅋㅋㅋ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData(boolean is, int page) {

        if (is) {
            m_ResultList = null;
        }

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);

        AppController.apiTaskNew = new APITaskNew(this, params, 508, stm);
        AppController.apiTaskNew.execute();

    }

    protected boolean updateResultList(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
                settingData(pageNavi);
            }
            return true;
        }

        m_ResultList = resultList;

        settingData(pageNavi);
//        m_ResultList = resultList;
//
////        m_ListAdapter = new PagerAdapterClass(this, m_ResultList);
//        m_ListAdapter = new PagerAdapterClass(getSupportFragmentManager());
//        vp.setAdapter(m_ListAdapter);

        return true;
    }

    private void settingData(int a) {
        bimv = (BoundableOfflineImageView) findViewById(R.id.imv_timeline_row_img);
        bimv.setTag(0);

        final CircleImageView cimvPro = (CircleImageView) findViewById(R.id.cimv_timeline_row_pro);
        final CircleImageView cimvComPro = (CircleImageView) findViewById(R.id.cimv_timeline_row_comment_pro);

        final LinearLayout imvLike = (LinearLayout) findViewById(R.id.lay_timeline_row_like);
        LinearLayout imvComment = (LinearLayout) findViewById(R.id.lay_timeline_row_comment);
        LinearLayout imvShare = (LinearLayout) findViewById(R.id.lay_timeline_row_share);
        LinearLayout imvMore = (LinearLayout) findViewById(R.id.lay_timeline_row_more);

        TextView tvUserId = (TextView) findViewById(R.id.tv_timeline_row_id);
        TextView tvTime = (TextView) findViewById(R.id.tv_timeline_row_time);
        TextView tvComId = (TextView) findViewById(R.id.tv_timeline_row_comment_id);
        TextView tvComBody = (TextView) findViewById(R.id.tv_timeline_row_comment_body);
        TextView tvBody = (TextView) findViewById(R.id.tv_timeline_row_body);
        final TextView tvLikeCnt = (TextView) findViewById(R.id.tv_timeline_row_like_cnt);
        TextView tvComCnt = (TextView) findViewById(R.id.tv_timeline_row_comment_cnt);
        TextView tvPlayCnt = (TextView) findViewById(R.id.tv_timeline_row_play_cnt);
        final TextView tvAndTagTitle = (TextView) findViewById(R.id.tv_timeline_row_andtag_title);
        TextView tvAndTagBody = (TextView) findViewById(R.id.tv_timeline_row_andtag_body);

        LinearLayout layAndTag = (LinearLayout) findViewById(R.id.lay_timeline_row_andtag);
        LinearLayout layLastComment = (LinearLayout) findViewById(R.id.lay_last_comment);

        final ImageView like = (ImageView) findViewById(R.id.imv_new_timeline_heart);
        like.setVisibility(View.GONE);
        final ImageView follow = (ImageView) findViewById(R.id.imv_timeline_row_follow);

//            final TimelineItem model = m_ResultList.get(position);
        final TimelineItem model = m_ResultList.get(a);

        final int pos = a;

        if (model.isFollow()) {
            follow.setSelected(true);
        } else {
            follow.setSelected(false);
        }

        String tempUrl = model.getUrl();

        int lastIdx = tempUrl.lastIndexOf("_");

        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

        downloadThumbImage(bimv, AppController.URL + tempName);

        if (model.getTlci().isExist()) {
            layLastComment.setVisibility(View.VISIBLE);
        } else {
            layLastComment.setVisibility(View.GONE);
        }

        if (model.isAndTag()) {
            layAndTag.setVisibility(View.VISIBLE);
            tvAndTagTitle.setText(model.getAndTagName());
            tvAndTagTitle.setTag(model.getAndTagId());
            tvAndTagBody.setText(model.getAndTagBody());
        } else {
            layAndTag.setVisibility(View.GONE);
        }

        follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    StringTransMethod stmFollow = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getInt("result") == 0) {
                                    if (jd.getString("follow").equals("Y")) {
                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + model.getOwnerId()).build());
                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                        follow.setSelected(true);
                                    } else if (jd.getString("follow").equals("N")) {
                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + model.getOwnerId()).build());
                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                        follow.setSelected(false);
                                    } else {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                    }
                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    JSONObject tempJ = new JSONObject();
                    tempJ.put("email", model.getOwnerId());

                    JSONArray jarr = new JSONArray();
                    jarr.put(tempJ);

                    Map<String, String> params = new HashMap<>();

                    params.put("myId", AppController.getSp().getString("email", ""));
                    params.put("email", jarr.toString());
                    params.put("type", "N");

                    AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 402, stmFollow);
                    AppController.apiDataTaskNew.execute();

                } catch (JSONException e) {

                }
            }
        });

        bimv.setOnTouchListener(new OnSwipeTouchListener(NewTimeLineActivity.this) {
            @Override
            public void onClick() {
                super.onClick();
                Drawable d = bimv.getDrawable();
                if (d instanceof GifDrawable) {
                    try {
                        GifDrawable g = (GifDrawable) d;
                        if (g.isPlaying()) {
                            g.pause();
                        } else {
                            g.start();
                        }
                    } catch (NullPointerException e) {

                    }
                }

            }

            @Override
            public void onDoubleClick() {
                super.onDoubleClick();

                try {
                    if (imvLike.isSelected()) {
                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Umlike Click" + "_" + model.getPsot_id()).build());
                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Umlike Click").build());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc - 1);
                                m_ResultList.get(pos).setIsLike(false);

                                imvLike.setSelected(false);
                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc - 1) + getResources().getString(R.string.timeline_count));
                            }
                        });
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result onselect", result);
                                    final JSONObject jd = new JSONObject(result);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {

                                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                                    m_ResultList.get(pos).setIsLike(true);

                                                    imvLike.setSelected(true);
                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc + 1) + getResources().getString(R.string.timeline_count));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 303, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {
                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Like Click" + "_" + model.getPsot_id()).build());
                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Like Click").build());

                        final Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like1);
                        final Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like2);
                        final Animation ani3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like3);

                        like.setVisibility(View.VISIBLE);

                        ani1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                like.startAnimation(ani2);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.startAnimation(ani3);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani3.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        like.startAnimation(ani1);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc + 1);
                                m_ResultList.get(pos).setIsLike(true);

                                imvLike.setSelected(true);
                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc + 1) + getResources().getString(R.string.timeline_count));

                            }
                        });
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    final JSONObject jd = new JSONObject(result);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                                    m_ResultList.get(pos).setIsLike(false);

                                                    imvLike.setSelected(false);
                                                    tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc - 1) + getResources().getString(R.string.timeline_count));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 302, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                } catch (NullPointerException e) {

                }
            }

            @Override
            public void onLongClick() {
                super.onLongClick();
                Intent i = new Intent(NewTimeLineActivity.this, GIFDownloadConfirmActivity.class);
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("body", model.getBody());
                i.putExtra("pid", model.getPsot_id());
                startActivity(i);
            }
        });

        if (model.getPro_url().equals("")) {
            cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
        } else {
            cimvPro.setImageURLString(AppController.URL + model.getPro_url());
        }

        cimvPro.setTag(model.getOwnerId());

        String tempTime = "";

        String tempDate = model.getTime();
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String tempC = sdf.format(now);

        ArrayList<String> nows = new ArrayList<>();
        ArrayList<String> posts = new ArrayList<>();

        for (int i = 0; i < tempDate.length(); i = i + 2) {
            nows.add(tempC.substring(i, i + 2));
            posts.add(tempDate.substring(i, i + 2));
        }

        int tempNavi = 0;

        for (int i = 0; i < nows.size(); i++) {
            if (!nows.get(i).equals(posts.get(i))) {
                tempNavi = i;
                break;
            }
        }

        int tempNow = Integer.parseInt(nows.get(tempNavi));
        int tempPosts = Integer.parseInt(posts.get(tempNavi));

        switch (tempNavi) {
            case 0:

                break;
            case 1:
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_year);
                } else {
                    tempTime = tempPosts - tempNow + getResources().getString(R.string.timeline_before_year);
                }
                break;
            case 2:
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_month);
                } else {
                    tempTime = (12 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_month);
                }
                break;
            case 3:
                Calendar c = Calendar.getInstance();
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_day);
                } else {
                    c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
                    int lastDay = c.getActualMaximum(Calendar.DATE);
                    tempTime = (lastDay - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_day);
                }
                break;
            case 4:
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_hour);
                } else {
                    tempTime = (24 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_hour);
                }
                break;
            case 5:
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_minute);
                } else {
                    tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_minute);
                }
                break;
            case 6:
                if (tempNow > tempPosts) {
                    tempTime = tempNow - tempPosts + getResources().getString(R.string.timeline_before_second);
                } else {
                    tempTime = (60 - tempPosts) + tempNow + getResources().getString(R.string.timeline_before_second);
                }
                break;
        }

        tvUserId.setText(model.getId());
        tvTime.setText(tempTime);
        tvBody.setText(model.getBody());
        tvLikeCnt.setText(getResources().getString(R.string.like) + " " + model.getLikeCount() + getResources().getString(R.string.timeline_count));
        tvComCnt.setText(getResources().getString(R.string.comment) + " " + model.getComCount() + getResources().getString(R.string.timeline_count));
        double playCnt = (double) model.getPlayCnt();
        DecimalFormat df = new DecimalFormat("#,##0");
        tvPlayCnt.setText(df.format(playCnt) + "");

        if (model.getTlci().isExist()) {
            tvComId.setText(model.getTlci().getId());
            tvComBody.setText(model.getTlci().getBody());
            cimvComPro.setImageURLString(AppController.URL + model.getTlci().getPro_url());
//                downloadProbImage(cimvComPro, AppController.URL + model.getTlci().getPro_url());
//                downloadImage(cimvComPro, AppController.URL + model.getTlci().getPro_url());
//                    new ImageTask(position, holder, AppController.URL + model.getTlci().getPro_url(), "", 1)
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            cimvComPro.setTag(model.getTlci().getEmail());
        } else {
            tvComId.setText("");
            tvComBody.setText("");
            cimvComPro.setTag("");
        }

        if (model.isLike()) {
            imvLike.setSelected(true);
        } else {
            imvLike.setSelected(false);
        }

        String commentsText = tvBody.getText().toString();
        String commentsText2 = tvComBody.getText().toString();

        ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
        ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
        ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

        ArrayList<int[]> hashtagSpans2 = getSpans(commentsText2, '#');
        ArrayList<int[]> calloutSpans2 = getSpans(commentsText2, '@');

        SpannableString commentsContent = new SpannableString(commentsText);
        SpannableString commentsContent2 = new SpannableString(commentsText2);

        for (int i = 0; i < hashtagSpans.size(); i++) {
            int[] span = hashtagSpans.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            commentsContent.setSpan(new HashTag(this),
                    hashTagStart, hashTagEnd, 0);
        }

        for (int i = 0; i < hashtagSpans2.size(); i++) {
            int[] span = hashtagSpans2.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            commentsContent2.setSpan(new HashTag(this),
                    hashTagStart, hashTagEnd, 0);
        }

        for (int i = 0; i < calloutSpans.size(); i++) {
            int[] span = calloutSpans.get(i);
            int calloutStart = span[0];
            int calloutEnd = span[1];

            commentsContent.setSpan(new CallTag(this),
                    calloutStart,
                    calloutEnd, 0);

        }
        for (int i = 0; i < calloutSpans2.size(); i++) {
            int[] span = calloutSpans2.get(i);
            int calloutStart = span[0];
            int calloutEnd = span[1];

            commentsContent2.setSpan(new CallTag(this),
                    calloutStart,
                    calloutEnd, 0);

        }

        for (int i = 0; i < andSpans.size(); i++) {
            int[] span = andSpans.get(i);
            int calloutStart = span[0];
            int calloutEnd = span[1];


            AndTag at = new AndTag(this);
            at.setTagId(model.getAndTagId());
            commentsContent.setSpan(at,
                    calloutStart,
                    calloutEnd, 0);

        }

        tvBody.setMovementMethod(LinkMovementMethod.getInstance());
        tvBody.setText(commentsContent);
        tvBody.setHighlightColor(getResources().getColor(android.R.color.transparent));
        tvComBody.setMovementMethod(LinkMovementMethod.getInstance());
        tvComBody.setText(commentsContent2);
        tvComBody.setHighlightColor(getResources().getColor(android.R.color.transparent));

        imvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (imvLike.isSelected()) {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unlike Click" + "_" + model.getPsot_id()).build());
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unlike Click").build());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc - 1);
                            m_ResultList.get(pos).setIsLike(false);

                            imvLike.setSelected(false);
                            tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc - 1) + getResources().getString(R.string.timeline_count));
                        }
                    });
                    StringTransMethod stmLikeResult = new StringTransMethod() {
                        @Override
                        public void endTrans(final String result) {
                            try {
                                Log.e("result onselect", result);
                                final JSONObject jd = new JSONObject(result);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (jd.getInt("result") == 0) {

                                            }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                            } else {
                                                int lc = model.getLikeCount();
                                                m_ResultList.get(pos).setLikeCount(lc + 1);
                                                m_ResultList.get(pos).setIsLike(true);

                                                imvLike.setSelected(true);
                                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc + 1) + getResources().getString(R.string.timeline_count));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                            } catch (JSONException e) {

                            }

                        }
                    };
                    Map<String, String> params = new HashMap<>();

                    params.put("post_reply_id", model.getPsot_id());
                    params.put("click_id", AppController.getSp().getString("email", ""));
                    params.put("like_form", "P");

                    AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 303, stmLikeResult);
                    AppController.apiDataTaskNew.execute();

                } else {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Like Click" + "_" + model.getPsot_id()).build());
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Like Click").build());
                    final Animation ani1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like1);
                    final Animation ani2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like2);
                    final Animation ani3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.like3);

                    ani1.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            like.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                            like.startAnimation(ani2);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    ani2.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            like.startAnimation(ani3);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    ani3.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            like.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    like.startAnimation(ani1);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc + 1);
                            m_ResultList.get(pos).setIsLike(true);

                            imvLike.setSelected(true);
                            tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc + 1) + getResources().getString(R.string.timeline_count));
                        }
                    });
                    StringTransMethod stmUnLikeResult = new StringTransMethod() {
                        @Override
                        public void endTrans(final String result) {
                            try {
                                Log.e("result nonselect", result);
                                final JSONObject jd = new JSONObject(result);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (jd.getInt("result") == 0) {
                                            }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                            } else {
                                                int lc = model.getLikeCount();
                                                m_ResultList.get(pos).setLikeCount(lc - 1);
                                                m_ResultList.get(pos).setIsLike(false);

                                                imvLike.setSelected(false);
                                                tvLikeCnt.setText(getResources().getString(R.string.like) + " " + (lc - 1) + getResources().getString(R.string.timeline_count));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } catch (JSONException e) {
                            }

                        }
                    };
                    Map<String, String> params = new HashMap<>();

                    params.put("post_reply_id", model.getPsot_id());
                    params.put("click_id", AppController.getSp().getString("email", ""));
                    params.put("like_form", "P");

                    AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 302, stmUnLikeResult);
                    AppController.apiDataTaskNew.execute();

                }
            }
        });
        imvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Comment Click" + "_" + model.getPsot_id()).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Comment Click").build());
                Intent i = new Intent(NewTimeLineActivity.this, SinglePostV1Activity.class);
                i.putExtra("last", model.getComCount());
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || imvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                i.putExtra("last", model.getComCount());
                startActivityForResult(i, COMMENT_RETURN);
            }
        });

        layLastComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Comment Click" + "_" + model.getPsot_id()).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Comment Click").build());
                Intent i = new Intent(NewTimeLineActivity.this, SinglePostV1Activity.class);
                i.putExtra("last", model.getComCount());
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || imvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                startActivityForResult(i, COMMENT_RETURN);
            }
        });
        imvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewTimeLineActivity.this, SharePopupActivity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("body", model.getBody());
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("w", gifWidth);
                i.putExtra("h", gifHeight);
                startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
            }
        });
        imvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewTimeLineActivity.this, Popup2Activity.class);
                i.putExtra("email", model.getOwnerId());
                if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                } else {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                }
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("title", model.getId());
                i.putExtra("body", model.getBody());
                startActivityForResult(i, TimeLineActivity.MORE_RETURN);
            }
        });

        tvComCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewTimeLineActivity.this, SinglePostV1Activity.class);
                i.putExtra("last", model.getComCount());
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || imvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                startActivityForResult(i, COMMENT_RETURN);
            }
        });
        tvLikeCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NewTimeLineActivity.this, UserListActivity.class);
                i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i.putExtra("postId", model.getPsot_id());
                startActivity(i);
            }
        });

        cimvComPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = cimvComPro.getTag().toString();
                if (tempEmail.equals(myId)) {
                    startActivity(new Intent(NewTimeLineActivity.this, MyPageV2Activity.class));
                    finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(NewTimeLineActivity.this, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        startActivity(i);
                    }
                }
            }
        });

        cimvPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = cimvPro.getTag().toString();
                if (tempEmail.equals(myId)) {
                    startActivity(new Intent(NewTimeLineActivity.this, MyPageV2Activity.class));
                    finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(NewTimeLineActivity.this, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        startActivity(i);
                    }
                }
            }
        });

        tvUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = cimvPro.getTag().toString();
                if (tempEmail.equals(myId)) {
                    startActivity(new Intent(NewTimeLineActivity.this, MyPageV2Activity.class));
                    finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(NewTimeLineActivity.this, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        startActivity(i);
                    }
                }
            }
        });

        tvAndTagTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvAndTagTitle.getTag().toString().equals("")) {
                    Intent i = new Intent(NewTimeLineActivity.this, AndTagFeedActivity.class);
                    i.putExtra("tag", tvAndTagTitle.getTag().toString());
                    startActivity(i);
                }
            }
        });

        tvAndTagBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!tvAndTagTitle.getTag().toString().equals("")) {
                    Intent i = new Intent(NewTimeLineActivity.this, AndTagFeedActivity.class);
                    i.putExtra("tag", tvAndTagTitle.getTag().toString());
                    startActivity(i);
                }
            }
        });

        bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
            @Override
            public void onImageChanged(Drawable d) {
                if (d instanceof GifDrawable) {
                    pb.setVisibility(View.GONE);

                    GifDrawable gd = (GifDrawable) d;

                    gifHeight = gd.getIntrinsicHeight();
                    gifWidth = gd.getIntrinsicWidth();
                    if (gd != null) {
                        gd.addAnimationListener(new AnimationListener() {

                            @Override
                            public void onAnimationCompleted(int loopNumber) {

//                                StringTransMethod stmCount = new StringTransMethod() {
//
//                                };
//
//                                Map<String, String> params = new HashMap<>();
//                                params.put("post_id", model.getPsot_id());

//                                Log.e("count" + pos, loopNumber + "");

                                AppController.playCountExe.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                                    }
                                });

                            }

                        });
                    }
                } else {
                    downloadGifbImage(bimv, AppController.URL + model.getUrl());
                }

            }
        });

//        svHeight = sv.getHeight();
//        viewHeight = layContent.getHeight();

//        Log.e("aaaaaaaaaaaaa", svHeight + " / " + viewHeight);
//        Log.e("aaaaaaaaaaaaa1", sv.canScrollVertically(-1) + "");
//        Log.e("aaaaaaaaaaaaa2", sv.canScrollVertically(0) + "");
//        Log.e("aaaaaaaaaaaaa3", sv.canScrollVertically(1) + "");
//        Log.e("aaaaaaaaaaaaa4", sv.canScrollVertically(2) + "");


//        if (svHeight > viewHeight) {
//
//            int ab = svHeight - viewHeight;
//
//            LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) layMargin.getLayoutParams();
//            param.height = ab + 20;
//
//            layMargin.setLayoutParams(param);
//
//            layMargin.setVisibility(View.VISIBLE);
//        } else {
//            layMargin.setVisibility(View.GONE);
//        }

//        if(sv.canScrollVertically(-1)){
//            layMargin.setVisibility(View.VISIBLE);
//        }else{
//            layMargin.setVisibility(View.GONE);
//        }

    }

    @Override
    protected void onDestroy() {
//        AppController.imageExe.shutdownNow();
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(NewTimeLineActivity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }else if(j.getInt("result") == 100 || j.getInt("result") == 1000){
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);

                            AppController.apiDataTaskNew = new APIDataTaskNew(NewTimeLineActivity.this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
                pageNavi = 0;
                getData(true, 1);
//                settingData(pageNavi);
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
                                pageNavi = 0;
                                settingData(pageNavi);
                                break;
                            case 3:
                                Intent i = new Intent(NewTimeLineActivity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                pageNavi = 0;
                                settingData(pageNavi);
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                pageNavi = 0;
                settingData(pageNavi);
                break;
            case COMMENT_RETURN:
                int comCount = data.getExtras().getInt("comCount");
                String postId = data.getExtras().getString("postId");
                for (int i = 0; i < m_ResultList.size(); i++) {
                    if(m_ResultList.get(i).getPsot_id().equals(postId)){
                        m_ResultList.get(i).setComCount(comCount);
//                        view
                    }
                }
                break;
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public void downloadThumbImage(ImageView imageView, String url) {

        Log.e("downloadThumbImageStart", url);

//        if(AppController.thumbThread != null && AppController.thumbThread.isAlive()){
//            AppController.thumbThread.interrupt();
//        }

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.thumbRun = new MinThumbImageRun(this, url, imageView, image, file_size2, 1);
//
//        if (AppController.thumbExe.isTerminated() || AppController.thumbExe.isShutdown()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
//
//        AppController.thumbExe.submit(AppController.thumbRun);

//        AppController.thumbThread = new Thread(AppController.thumbRun);
//
//        AppController.thumbThread.start();
//
//        AppController.thumbThread = new MinImageThread((Activity) con, url, imageView, image, file_size2, 1);
//        AppController.thumbThread.run();

    }

    public void downloadGifbImage(ImageView imageView, String url) {

        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun(this, url, imageView, image, file_size2, 1);
//
//        if (AppController.gifExe.isShutdown() || AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.imv_main_timeline_top_alarm:
//                startActivity(new Intent(TimeLineActivity.this, NotificationActivity.class));
//                break;
            case R.id.imv_main_new_timeline_top_chatting:
//                Toast.makeText(this, "채팅 준비중", Toast.LENGTH_SHORT).show();
                break;
//            case R.id.btn_new_timeline_intro:
//                Intent i = new Intent(NewTimeLineActivity.this, FriendResultActivity.class);
//                i.putExtra("navi", 3);
//                startActivityForResult(i, FOLLOW_RETURN);
//                break;
            case R.id.lay_new_timeline_timeline:
                break;
            case R.id.lay_new_timeline_interest:
                Intent i = new Intent(NewTimeLineActivity.this, InterestFeedActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_new_timeline_camera:
//                Intent gifCamera = new Intent(NewTimeLineActivity.this, GIFCameraActivity.class);
                Intent gifCamera = new Intent(NewTimeLineActivity.this, GIFCameraFilmActivity.class);
                gifCamera.putExtra("navi", 0);
//                gifCamera.putExtra("navi", 2);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(gifCamera, POST_WRITE_RETURN);
                break;
            case R.id.lay_new_timeline_noti:
                i = new Intent(NewTimeLineActivity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_new_timeline_mypage:
                i = new Intent(NewTimeLineActivity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }
}
