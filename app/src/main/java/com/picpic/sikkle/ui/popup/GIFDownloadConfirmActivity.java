package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

import java.util.List;

public class GIFDownloadConfirmActivity extends Activity {

    Button left, right;
    String url = "", pid = "", body = "";

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Uri urlToDownload;
    private long latestId = -1;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gifdownload_confirm);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("GIF 다운로드 확인 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        url = getIntent().getExtras().getString("url");
        pid = getIntent().getExtras().getString("pid");
        body = getIntent().getExtras().getString("body");

        left = (Button) findViewById(R.id.btn_confirm_popup_2_left);
        right = (Button) findViewById(R.id.btn_confirm_popup_2_right);
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                urlToDownload = Uri.parse(url);
                List<String> pathSegments = urlToDownload.getPathSegments();
                request = new DownloadManager.Request(urlToDownload);
                request.setTitle(url);
                request.setDescription(body);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pathSegments.get(pathSegments.size() - 1));
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
                latestId = downloadManager.enqueue(request);

                finish();
                overridePendingTransition(0, 0);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.download_start), Toast.LENGTH_LONG).show();

                AppController.ActionLogInsert(pid, "D", GIFDownloadConfirmActivity.this);

            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
