package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MakeAndTagActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack, imvComplete;
    EditText edtTitle, edtBody;
    Animation shake;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    TextWatcher titleTW = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {

                switch (s.charAt(s.length() - 1)) {
                    case ' ':
                        edtTitle.setText(s.toString().replace(" ", "_"));
                        edtTitle.setSelection(edtTitle.length());
                        break;
                    case ',':
                    case '.':
                    case '/':
                    case '?':
                    case '`':
                    case '&':
                    case '#':
                    case '@':
                    case '\\':
                    case '|':
                    case '"':
                        edtTitle.setText(edtTitle.getText().toString().substring(0, edtTitle.getText().toString().length() - 2));
                        edtTitle.setSelection(edtTitle.length());
                        break;
                    default:
                        if (s.length() > 21) {
                            edtTitle.setText(s.toString().substring(0, 21));
                            edtTitle.setSelection(edtTitle.length());
                        }
                        break;
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    TextWatcher bodyTW = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {
                if (s.length() > 70) {
                    edtTitle.setText(s.toString().substring(0, 70));
                    edtTitle.setSelection(edtTitle.length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_make_and_tag);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("앤드태그 생성 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        imvComplete = (LinearLayout) findViewById(R.id.imv_make_andtag_next);
        imvComplete.setOnClickListener(this);

        layBack = (LinearLayout) findViewById(R.id.lay_make_andtag_back);
        layBack.setOnClickListener(this);

        edtTitle = (EditText) findViewById(R.id.edt_make_andtag_title);
        edtTitle.addTextChangedListener(titleTW);
        edtBody = (EditText) findViewById(R.id.edt_make_andtag_body);
        edtBody.addTextChangedListener(bodyTW);

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_make_andtag_next:
                if (edtTitle.getText().toString().length() == 0) {
                    edtTitle.startAnimation(shake);
                } else {
                    if (edtBody.getText().toString().length() == 0) {
                        edtBody.startAnimation(shake);
                    } else {

                        StringTransMethod stmMakeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);

                                    if (jd.getInt("result") == 0) {
                                        Intent i = new Intent();
                                        i.putExtra("title", edtTitle.getText().toString());
                                        i.putExtra("body", edtBody.getText().toString());
                                        setResult(RESULT_OK, i);
                                        finish();
                                        overridePendingTransition(0, 0);
                                    } else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                edtTitle.startAnimation(shake);
                                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.write_andtag_isexists), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };

                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("str", edtTitle.getText().toString());

                        AppController.apiTaskNew = new APITaskNew(this, params, 217, stmMakeResult);
                        AppController.apiTaskNew.execute();

                    }
                }
                break;
            case R.id.lay_make_andtag_back:
                finish();
                setResult(RESULT_CANCELED);
                overridePendingTransition(0, 0);
                break;
        }
    }
}
