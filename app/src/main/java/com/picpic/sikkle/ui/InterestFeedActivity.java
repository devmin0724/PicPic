package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.tutorial.TutorialCamera5;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageRun;
import com.picpic.sikkle.utils.MinThumbImageRun;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

public class InterestFeedActivity extends Activity implements View.OnClickListener {

    LinearLayout imvSearch, imvAlarm;
    //    FloatingScrollView fsv;
//    FloatingActionLayout fal;
//    ImageView imvTimeLine, imvInterest, imvCamera, imvAndtag, imvMypage, imvChatting;
    LinearLayout layTimeline, layCamera, layNotification, layMypage, layInterest, layHome;
    ScrollView sv;

    ImageView imvNoti;

    ArrayList<TextView> inters1 = new ArrayList<>();
    ArrayList<TextView> inters2 = new ArrayList<>();

    ArrayList<FixedImageView> imvs1 = new ArrayList<>();
    ArrayList<FixedImageView> imvs2 = new ArrayList<>();

    ArrayList<TextView> users = new ArrayList<>();

    ArrayList<CircleImageView> usersC = new ArrayList<>();

    ArrayList<ImageView> usersI = new ArrayList<>();
    ArrayList<LinearLayout> usersL = new ArrayList<>();

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pager_interest);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("관심사 피드 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        int tuNavi = AppController.getSp().getInt("tuinter", -1);

        if (tuNavi > 0) {
            tuNavi = tuNavi - 1;
            AppController.getSe().putInt("tuinter", tuNavi);
            AppController.getSe().commit();

            startActivity(new Intent(InterestFeedActivity.this, TutorialCamera5.class));
        } else {

        }

        sv = (ScrollView) findViewById(R.id.fsv_interest);

        layHome = (LinearLayout) findViewById(R.id.lay_interest_home);
        layHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.smoothScrollTo(0, 0);
            }
        });

        imvNoti = (ImageView) findViewById(R.id.imv_new_interest_noti);

        layTimeline = (LinearLayout) findViewById(R.id.lay_interest_timeline);
        layCamera = (LinearLayout) findViewById(R.id.lay_interest_camera);
        layNotification = (LinearLayout) findViewById(R.id.lay_interest_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_interest_mypage);
        layInterest = (LinearLayout) findViewById(R.id.lay_interest_interest);

        layInterest.setSelected(true);

        imvSearch = (LinearLayout) findViewById(R.id.imv_interest_search);
//        imvChatting = (ImageView) findViewById(R.id.imv_interest_chatting);


        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_1));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_2));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_3));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_4));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_5));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_1_6));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_1));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_2));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_3));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_4));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_5));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_2_6));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_1));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_2));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_3));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_4));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_5));
        inters1.add((TextView) findViewById(R.id.tv_interest_interest_3_6));

        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_1));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_2));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_3));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_4));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_5));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_6));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_7));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_8));
        inters2.add((TextView) findViewById(R.id.tv_interest_interest_4_9));

        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_1));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_2));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_3));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_4));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_5));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_1_6));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_1));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_2));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_3));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_4));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_5));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_2_6));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_1));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_2));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_3));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_4));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_5));
        imvs1.add((FixedImageView) findViewById(R.id.imv_interest_interest_3_6));

        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_1));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_2));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_3));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_4));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_5));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_6));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_7));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_8));
        imvs2.add((FixedImageView) findViewById(R.id.imv_interest_interest_4_9));

        users.add((TextView) findViewById(R.id.tv_interest_user_name_1));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_2));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_3));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_4));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_5));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_6));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_7));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_8));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_9));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_10));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_11));
        users.add((TextView) findViewById(R.id.tv_interest_user_name_12));

        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_1));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_2));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_3));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_4));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_5));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_6));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_7));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_8));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_9));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_10));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_11));
        usersC.add((CircleImageView) findViewById(R.id.cimv_interest_user_12));

        usersI.add((ImageView) findViewById(R.id.imv_interest_user_1_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_2_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_3_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_4_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_5_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_6_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_7_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_8_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_9_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_10_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_11_follow));
        usersI.add((ImageView) findViewById(R.id.imv_interest_user_12_follow));

        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_1));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_2));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_3));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_4));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_5));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_6));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_7));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_8));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_9));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_10));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_11));
        usersL.add((LinearLayout) findViewById(R.id.lay_interest_user_12));

        for (int i = 0; i < imvs1.size(); i++) {
            imvs1.get(i).setOnClickListener(this);

            inters1.get(i).setOnClickListener(this);
        }

        for (int i = 0; i < imvs2.size(); i++) {
            imvs2.get(i).setOnClickListener(this);

            inters2.get(i).setOnClickListener(this);
        }

        for (int i = 0; i < users.size(); i++) {
            users.get(i).setOnClickListener(this);

            usersC.get(i).setOnClickListener(this);

            usersI.get(i).setOnClickListener(this);

            usersL.get(i).setOnClickListener(this);
        }

        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layNotification.setOnClickListener(this);
        layMypage.setOnClickListener(this);

        imvSearch.setOnClickListener(this);
//        imvAlarm.setOnClickListener(this);

        getData();
    }

    @Override
    protected void onResume() {
        notiCheck();
        super.onResume();
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
//        startActivity(new Intent(InterestFeedActivity.this, NewGridTimelineActivity.class));
        startActivity(new Intent(InterestFeedActivity.this, TimeLineActivity.class));
    }

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 514, stmListResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmListResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
                    JSONArray jarr1 = new JSONArray(jd.getString("interest"));

                    JSONObject j;

                    for (int i = 0; i < jarr1.length(); i++) {
                        j = jarr1.getJSONObject(i);

                        inters1.get(i).setTag(j.getString("tag_name"));
                        imvs1.get(i).setTag(j.getString("tag_name"));

                        String tempStr = j.getString("url");
                        int underbarCount = tempStr.lastIndexOf("_");
                        final String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                        int lastIdx = tempUrl.lastIndexOf("_");

                        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                        final JSONObject fj = j;
                        final int fi = i;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    inters1.get(fi).setText("#"+fj.getString("tag_name"));
                                    imvs1.get(fi).setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                    }

                    JSONArray jarr2 = new JSONArray(jd.getString("locale"));

                    for (int i = 0; i < jarr2.length(); i++) {
                        j = jarr2.getJSONObject(i);

                        inters2.get(i).setTag(j.getString("tag_name"));
                        imvs2.get(i).setTag(j.getString("tag_name"));

                        String tempStr = j.getString("url");
                        int underbarCount = tempStr.lastIndexOf("_");
                        final String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                        int lastIdx = tempUrl.lastIndexOf("_");

                        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                        final JSONObject fj = j;
                        final int fi = i;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    inters2.get(fi).setText("#"+fj.getString("tag_name"));
                                    imvs2.get(fi).setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }

                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("er3", e.toString());
            } catch (Exception e) {
                Log.e("er4", e.toString());
            } finally {
                getFriendData();
            }
        }
    };

    private void getFriendData() {

        StringTransMethod stmgetFriend1Result = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if(jd.getInt("result") == 0){
                        JSONArray jarr = new JSONArray(jd.getString("friends"));

                        JSONObject j;

                        for (int i = 0; i < 6; i++) {

                            j = jarr.getJSONObject(i);

                            users.get(i).setTag(j.getString("email"));
                            usersC.get(i).setTag(j.getString("email"));
                            usersI.get(i).setTag(j.getString("email"));
                            usersL.get(i).setTag(j.getString("email"));

                            final int fi = i;
                            final JSONObject fj = j;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (fj.getString("follow_yn").equals("Y")) {
                                            usersI.get(fi).setSelected(true);
                                        } else {
                                            usersI.get(fi).setSelected(false);
                                        }
                                        users.get(fi).setText(fj.getString("id"));

                                        usersC.get(fi).setImageURLString(AppController.URL + fj.getString("profile_picture"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        }
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    getFriendData2();
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 411, stmgetFriend1Result);
        AppController.apiTaskNew.execute();

    }

    private void getFriendData2() {
        StringTransMethod stmgetFriend2Result = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if(jd.getInt("result") == 0){
                        JSONArray jarr = new JSONArray(jd.getString("friends"));

                        JSONObject j;

                        for (int i = 0; i < jarr.length(); i++) {

                            j = jarr.getJSONObject(i);

                            users.get(i + 6).setTag(j.getString("email"));
                            usersC.get(i + 6).setTag(j.getString("email"));
                            usersI.get(i + 6).setTag(j.getString("email"));
                            usersL.get(i).setTag(j.getString("email"));

                            final int fi = i;
                            final JSONObject fj = j;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (fj.getString("follow_yn").equals("Y")) {
                                            usersI.get(fi + 6).setSelected(true);
                                        } else {
                                            usersI.get(fi + 6).setSelected(false);
                                        }
                                        users.get(fi + 6).setText(fj.getString("id"));

                                        usersC.get(fi + 6).setImageURLString(AppController.URL + fj.getString("profile_picture"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        }
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 411, stmgetFriend2Result);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onClick(final View v) {
        Intent i = new Intent(InterestFeedActivity.this, UserFeedActivity.class);
        switch (v.getId()) {
            case R.id.lay_interest_timeline:
                overridePendingTransition(0, 0);
//                startActivity(new Intent(InterestFeedActivity.this, NewGridTimelineActivity.class));
                startActivity(new Intent(InterestFeedActivity.this, TimeLineActivity.class));
                finish();
                break;
            case R.id.lay_interest_interest:
//                startActivity(new Intent(InterestFeedActivity.this, NewTimeLineActivity.class));
//                finish();
                break;
            case R.id.lay_interest_camera:
//                System.gc();
//                overridePendingTransition(0, 0);
//                Intent gifCamera = new Intent(InterestFeedActivity.this, GIFCameraActivity.class);
//                gifCamera.putExtra("navi", 0);
//                startActivity(gifCamera);

                System.gc();
                Intent gifCamera = new Intent(InterestFeedActivity.this, GIFCameraFilmActivity.class);
                gifCamera.putExtra("navi", 0);
                startActivity(gifCamera);
                break;
            case R.id.lay_interest_noti:
                overridePendingTransition(0, 0);
                startActivity(new Intent(InterestFeedActivity.this, NotificationActivity.class));
                finish();
                break;
            case R.id.lay_interest_mypage:
                overridePendingTransition(0, 0);
                startActivity(new Intent(InterestFeedActivity.this, MyPageV2Activity.class));
                finish();
                break;
            case R.id.imv_interest_search:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Search Click").build());
                overridePendingTransition(0, 0);
                Intent ii = new Intent(InterestFeedActivity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 0);
                startActivity(ii);
                break;
            case R.id.tv_interest_user_name_1:
            case R.id.tv_interest_user_name_2:
            case R.id.tv_interest_user_name_3:
            case R.id.tv_interest_user_name_4:
            case R.id.tv_interest_user_name_5:
            case R.id.tv_interest_user_name_6:
            case R.id.tv_interest_user_name_7:
            case R.id.tv_interest_user_name_8:
            case R.id.tv_interest_user_name_9:
            case R.id.tv_interest_user_name_10:
            case R.id.tv_interest_user_name_11:
            case R.id.tv_interest_user_name_12:
            case R.id.cimv_interest_user_1:
            case R.id.cimv_interest_user_2:
            case R.id.cimv_interest_user_3:
            case R.id.cimv_interest_user_4:
            case R.id.cimv_interest_user_5:
            case R.id.cimv_interest_user_6:
            case R.id.cimv_interest_user_7:
            case R.id.cimv_interest_user_8:
            case R.id.cimv_interest_user_9:
            case R.id.cimv_interest_user_10:
            case R.id.cimv_interest_user_11:
            case R.id.cimv_interest_user_12:
            case R.id.lay_interest_user_1:
            case R.id.lay_interest_user_2:
            case R.id.lay_interest_user_3:
            case R.id.lay_interest_user_4:
            case R.id.lay_interest_user_5:
            case R.id.lay_interest_user_6:
            case R.id.lay_interest_user_7:
            case R.id.lay_interest_user_8:
            case R.id.lay_interest_user_9:
            case R.id.lay_interest_user_10:
            case R.id.lay_interest_user_11:
            case R.id.lay_interest_user_12:
                try {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Interest UserFeed Click" + "_" + v.getTag().toString()).build());
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Interest UserFeed Click").build());
                    i.putExtra("email", v.getTag().toString());
                    startActivity(i);
                } catch (NullPointerException e) {

                }
                break;
            case R.id.imv_interest_user_1_follow:
            case R.id.imv_interest_user_2_follow:
            case R.id.imv_interest_user_3_follow:
            case R.id.imv_interest_user_4_follow:
            case R.id.imv_interest_user_5_follow:
            case R.id.imv_interest_user_6_follow:
            case R.id.imv_interest_user_7_follow:
            case R.id.imv_interest_user_8_follow:
            case R.id.imv_interest_user_9_follow:
            case R.id.imv_interest_user_10_follow:
            case R.id.imv_interest_user_11_follow:
            case R.id.imv_interest_user_12_follow:
                StringTransMethod stmFollowResult = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);
                            if (jd.getInt("result") == 0) {
                                if (jd.getString("follow").equals("Y")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + v.getTag().toString()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                            v.setSelected(true);
                                        }
                                    });
                                } else if (jd.getString("follow").equals("N")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + v.getTag().toString()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                            v.setSelected(false);
                                        }
                                    });
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                try {
                    JSONObject tempJ = new JSONObject();
                    tempJ.put("email", v.getTag().toString());

                    JSONArray jarr = new JSONArray();
                    jarr.put(tempJ);

                    Map<String, String> params = new HashMap<>();

                    params.put("myId", AppController.getSp().getString("email", ""));
                    params.put("email", jarr.toString());
                    params.put("type", "N");

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 402, stmFollowResult);
                    AppController.apiDataTaskNew.execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                if (v.getTag() != null) {
                    Intent i2 = new Intent(InterestFeedActivity.this, TagFeedActivity.class);
                    i2.putExtra("tag", v.getTag().toString());
                    startActivity(i2);
                }
                break;
        }
    }

}
