package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.EditTextCheckIcon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JoinIdActivity extends Activity implements View.OnClickListener {

    public static Activity ac;
    EditTextCheckIcon etci;
    EditText edt;
    Button btn;
    String tempEmail = "", tempPass = "", tempBirth = "";
    boolean isMan = true;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    TextWatcher idTW = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() > 0) {

                switch (s.charAt(s.length() - 1)) {
                    case ' ':
                        edt.setText(s.toString().replace(" ", "_"));
                        edt.setSelection(edt.length());
                        break;
                    case ',':
                    case '.':
                    case '/':
                    case '?':
                    case '`':
                    case '&':
                    case '#':
                    case '@':
                    case '\\':
                    case '|':
                    case '"':
                        edt.setText(edt.getText().toString().substring(0, edt.getText().toString().length() - 2));
                        edt.setSelection(edt.length());
                        break;
                    default:
                        if (s.length() > 20) {
                            edt.setText(s.toString().substring(0, 20));
                            edt.setSelection(edt.length());
                        }
                        break;
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join_id);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("회원가입 아이디 입력페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        ac = this;

        tempEmail = getIntent().getExtras().getString("email");
        tempPass = getIntent().getExtras().getString("password");
        tempBirth = getIntent().getExtras().getString("birth");
        isMan = getIntent().getExtras().getBoolean("sex");

        initViews();
    }

    private void initViews() {
        etci = (EditTextCheckIcon) findViewById(R.id.etci_join_id_id);
        etci.setOnClickListener(this);

        edt = (EditText) findViewById(R.id.edt_join_id_id);
        edt.addTextChangedListener(idTW);

        btn = (Button) findViewById(R.id.btn_join_id_next);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etci_join_id_id:
                edt.setText("");
                break;
            case R.id.btn_join_id_next:
                String temp1 = edt.getText().toString();
                if (temp1.equals("")) {
                    edt.startAnimation(AppController.shake);
                    etci.setState(EditTextCheckIcon.CHOICE_WRONG);
                    edt.setText("");
                    edt.setHint(R.string.join_hint_id_empty);
                } else {
                    StringTransMethod stmIdCheck = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);
                                if (jd.getString("data").equals("0")) {
                                    Intent i = new Intent(JoinIdActivity.this, ProfilePictureActivity.class);
                                    i.putExtra("email", tempEmail);
                                    i.putExtra("pw", tempPass);
                                    i.putExtra("birth", tempBirth);
                                    i.putExtra("sex", isMan + "");
                                    i.putExtra("id", edt.getText().toString());
                                    i.putExtra("navi", 0);

                                    try {
                                        JoinEmailActivity.ac.finish();
                                    } catch (NullPointerException e) {

                                    }

                                    try {
                                        JoinPassWordActivity.ac.finish();
                                    } catch (NullPointerException e) {

                                    }

                                    try {
                                        JoinBirthActivity.ac.finish();
                                    } catch (NullPointerException e) {

                                    }

                                    try {
                                        JoinSexActivity.ac.finish();
                                    } catch (NullPointerException e) {

                                    }

                                    startActivity(i);
                                    finish();
                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                } else {
                                    edt.startAnimation(AppController.shake);
                                    etci.setState(EditTextCheckIcon.CHOICE_WRONG);
                                    edt.setText("");
                                    edt.setHint(R.string.join_hint_id_already);
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    Map<String, String> params = new HashMap<>();

                    params.put("id", temp1);

                    AppController.apiTaskNew = new APITaskNew(this, params, 214, stmIdCheck);
                    AppController.apiTaskNew.execute();

                }
                break;
        }
    }
}
