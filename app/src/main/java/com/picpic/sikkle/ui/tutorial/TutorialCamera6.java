package com.picpic.sikkle.ui.tutorial;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.picpic.sikkle.R;

public class TutorialCamera6 extends Activity {

    ImageView imv;
    int pageNavi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tutorial_camera6);

        imv = (ImageView) findViewById(R.id.imv_tu_camera_6);
        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (pageNavi) {
                    case 0:
                        finish();
                        overridePendingTransition(0, 0);
//                        imv.setBackgroundResource(R.drawable.tu_camera2);
//                        pageNavi = pageNavi + 1;
                        break;
                    case 1:
                        pageNavi = pageNavi + 1;
                        break;
                    case 2:
                        pageNavi = pageNavi + 1;
                        break;
                    case 3:
                        finish();
                        overridePendingTransition(0, 0);
                        break;
                    case 4:
                        finish();
                        overridePendingTransition(0, 0);
                        break;
                    case 5:
                        finish();
                        overridePendingTransition(0, 0);
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        switch (pageNavi) {
            case 0:
                pageNavi = pageNavi + 1;
                break;
            case 1:
                pageNavi = pageNavi + 1;
                break;
            case 2:
                pageNavi = pageNavi + 1;
                break;
            case 3:
                finish();
                overridePendingTransition(0, 0);
                break;
            case 4:
                finish();
                overridePendingTransition(0, 0);
                break;
            case 5:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }
}
