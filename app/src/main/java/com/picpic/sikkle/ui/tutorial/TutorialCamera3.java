package com.picpic.sikkle.ui.tutorial;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.picpic.sikkle.R;

public class TutorialCamera3 extends Activity {

    ImageView imv;
    int pageNavi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tutorial_camera3);

        imv = (ImageView) findViewById(R.id.imv_tu_camera_3);
        imv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (pageNavi) {
                    case 0:
                        pageNavi = pageNavi + 1;
                        break;
                    case 1:
                        finish();
                        overridePendingTransition(0, 0);
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        switch (pageNavi) {
            case 0:
                pageNavi = pageNavi + 1;
                break;
            case 1:
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }
}
