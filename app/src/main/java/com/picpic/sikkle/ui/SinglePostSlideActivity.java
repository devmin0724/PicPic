package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopupForTimelineActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;

//public class SinglePostSlideActivity extends Activity implements View.OnClickListener {
public class SinglePostSlideActivity extends Activity {

    private final int TIMELINE_FOLLOW_LIST = 1;
    private final int TIMELINE_ALL_LIST = 2;
    private final int CATEGORY_LIST = 3;
    private final int USERFEED_LIST = 4;
    private final int TAGFEED_LIST = 5;
    private final int MYPAGE_LIST = 6;

    ViewPager vp;
    LinearLayout layLike, layComment, layShare, layMore, layBack, layWhole;
    CircleImageView cimv;
    TextView tvName, tvPlaycount;
    ImageView imvGuide;
    ProgressBar pb;

    int navi = 0;
    int page = 1;
    int nowPosition = 0;
    int wholePage = 0;
    int pageNavi = 0;
    String userId, type, tempTag;
    String[] ms;
    String[] ds;
    PagerAdapter adapter;
    TimelineResult tr;
    TimelineItem tempTI;
    boolean isLoading = false;

    int draws[] = {
            R.drawable.swip_arrow_00000,
            R.drawable.swip_arrow_00001,
            R.drawable.swip_arrow_00002,
            R.drawable.swip_arrow_00003,
            R.drawable.swip_arrow_00004,
            R.drawable.swip_arrow_00005,
            R.drawable.swip_arrow_00006,
            R.drawable.swip_arrow_00007,
            R.drawable.swip_arrow_00008,
            R.drawable.swip_arrow_00009,
            R.drawable.swip_arrow_00010,
            R.drawable.swip_arrow_00011,
            R.drawable.swip_arrow_00012,
            R.drawable.swip_arrow_00013,
            R.drawable.swip_arrow_00014,
            R.drawable.swip_arrow_00015,
            R.drawable.swip_arrow_00016,
            R.drawable.swip_arrow_00017,
            R.drawable.swip_arrow_00018,
            R.drawable.swip_arrow_00019,
            R.drawable.swip_arrow_00020,
            R.drawable.swip_arrow_00021,
            R.drawable.swip_arrow_00022,
            R.drawable.swip_arrow_00023,
            R.drawable.swip_arrow_00024,
            R.drawable.swip_arrow_00025,
            R.drawable.swip_arrow_00026,
            R.drawable.swip_arrow_00027,
            R.drawable.swip_arrow_00028,
            R.drawable.swip_arrow_00029
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_single_post_slide);

        vp = (ViewPager) findViewById(R.id.vp_single_post_content_slide);

        layWhole = (LinearLayout) findViewById(R.id.lay_single_post_whole);

        pb = (ProgressBar) findViewById(R.id.pb_single_post_slide);

//        vp.setAdapter(new PagerAdapter(new TimelineResult()));

        layLike = (LinearLayout) findViewById(R.id.lay_single_post_content_slide_like);
        layComment = (LinearLayout) findViewById(R.id.lay_single_post_content_slide_comment);
        layShare = (LinearLayout) findViewById(R.id.lay_single_post_content_slide_share);
        layMore = (LinearLayout) findViewById(R.id.lay_single_post_content_slide_more);

        layBack = (LinearLayout) findViewById(R.id.imv_single_slide_back);
        cimv = (CircleImageView) findViewById(R.id.cimv_single_slide_pro);
        tvName = (TextView) findViewById(R.id.tv_single_slide_id);
        tvPlaycount = (TextView) findViewById(R.id.tv_single_slide_play_cnt);

        imvGuide = (ImageView) findViewById(R.id.imv_single_post_guide);

        AnimationDrawable previewAnimation = new AnimationDrawable();
        for (int i = 0; i < draws.length; i++) {
            previewAnimation.addFrame(new BitmapDrawable(BitmapFactory.decodeResource(getResources(), draws[i])), 33);
        }

        previewAnimation.setOneShot(false);

        imvGuide.setImageDrawable(previewAnimation);

        previewAnimation.start();

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (imvGuide.getVisibility() == View.VISIBLE) {
                    imvGuide.setVisibility(View.GONE);
                }
            }
        };

        handler.sendEmptyMessageDelayed(0, 5000);

//        layLike.setOnClickListener(this);
//        layComment.setOnClickListener(this);
//        layShare.setOnClickListener(this);
//        layMore.setOnClickListener(this);

        navi = getIntent().getExtras().getInt("navi");
        nowPosition = getIntent().getExtras().getInt("now");

        try {
            pageNavi = getIntent().getExtras().getInt("pageNavi");
        } catch (Exception e) {
            pageNavi = 0;
        }

        try {
            tempTag = getIntent().getExtras().getString("tag");
        } catch (Exception e) {
            tempTag = "";
        }

        try {
            userId = getIntent().getExtras().getString("user_id");
            type = getIntent().getExtras().getString("type");
        } catch (Exception e) {
            userId = "";
            type = "";
        }

        wholePage = nowPosition / 10 + 1;

//        tr = (TimelineResult) getIntent().getExtras().get("tr");


        getDataFirst();
    }

    private void getDataFirst() {
        Map<String, String> params = new HashMap<>();

        switch (navi) {
            case TIMELINE_FOLLOW_LIST:
            case TIMELINE_ALL_LIST:
            case CATEGORY_LIST:
                tr = AppController.tempTR;

                updateResultList(tr);

                vp.setCurrentItem(nowPosition, false);
                break;
//            case TIMELINE_FOLLOW_LIST:
//
//                params.put("my_id", AppController.getSp().getString("email", ""));
//                params.put("page", "" + page);
//
//                AppController.apiTaskNew = new APITaskNew(SinglePostSlideActivity.this, params, 508, stmDataFristResult);
//                AppController.apiTaskNew.execute();
//                break;
//            case TIMELINE_ALL_LIST:
//                params.put("my_id", AppController.getSp().getString("email", ""));
//                params.put("page", "" + page);
//
//                AppController.apiTaskNew = new APITaskNew(SinglePostSlideActivity.this, params, 521, stmDataFristResult);
//                AppController.apiTaskNew.execute();
//                break;
//            case CATEGORY_LIST:
//                params.put("my_id", AppController.getSp().getString("email", ""));
//                params.put("page", "" + page);
//                params.put("category_num", "" + (pageNavi + 1));
//
//                AppController.apiTaskNew = new APITaskNew(this, params, 522, stmDataFristResult);
//                AppController.apiTaskNew.execute();
//                break;
            case USERFEED_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("user_id", userId);
                params.put("page", "" + page);
                params.put("range", "N");
                params.put("str", "");
                params.put("type", type);
                params.put("timeline", "T");

                AppController.apiTaskNew = new APITaskNew(this, params, 520, stmDataFristResult);
                AppController.apiTaskNew.execute();
                break;
            case TAGFEED_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", "" + page);
                params.put("range", "N");
                params.put("str", tempTag);
                params.put("type", "T");
                params.put("timeline", "T");

                AppController.apiTaskNew = new APITaskNew(this, params, 520, stmDataFristResult);
                AppController.apiTaskNew.execute();
                break;
        }
    }

    StringTransMethod stmDataFristResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                JSONObject jd = new JSONObject(result);
                MinUtils.d("dataFollow", result);
                if (jd.getInt("result") == 0) {

                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineResult tr = new TimelineResult();
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;

                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        ti.setWidth(j.getInt("width1"));
                        ti.setHeight(j.getInt("height1"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

                        tr.add(ti);
                    }

                    if (page < wholePage) {
                        layWhole.setVisibility(View.GONE);
                        pb.setVisibility(View.VISIBLE);
                        updateResultList(tr);
                        page = page + 1;
                        getDataFirst();
                    } else {
                        layWhole.setVisibility(View.VISIBLE);
                        pb.setVisibility(View.GONE);
                        updateResultList(tr);
                        vp.setCurrentItem(nowPosition, false);
                    }
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                } else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    StringTransMethod stmDataResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                JSONObject jd = new JSONObject(result);
                MinUtils.d("dataFollow", result);
                if (jd.getInt("result") == 0) {

                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineResult tr = new TimelineResult();
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;

                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        ti.setWidth(j.getInt("width1"));
                        ti.setHeight(j.getInt("height1"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

                        tr.add(ti);
                    }

//                                isEnd = jarr.length() != 10;
//
                    isLoading = false;
                    updateResultList(tr);
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                } else {

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private void getData() {
        isLoading = true;
        Map<String, String> params = new HashMap<>();

        switch (navi) {
            case TIMELINE_FOLLOW_LIST:

                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", "" + page);

                AppController.apiTaskNew = new APITaskNew(SinglePostSlideActivity.this, params, 508, stmDataResult);
                AppController.apiTaskNew.execute();
                break;
            case TIMELINE_ALL_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", "" + page);

                AppController.apiTaskNew = new APITaskNew(this, params, 521, stmDataResult);
                AppController.apiTaskNew.execute();
                break;
            case CATEGORY_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", "" + page);
                params.put("category_num", "" + (pageNavi + 1));

                AppController.apiTaskNew = new APITaskNew(this, params, 522, stmDataResult);
                AppController.apiTaskNew.execute();
                break;
            case USERFEED_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("user_id", userId);
                params.put("page", "" + page);
                params.put("range", "N");
                params.put("str", "");
                params.put("type", type);
                params.put("timeline", "T");

                AppController.apiTaskNew = new APITaskNew(this, params, 520, stmDataResult);
                AppController.apiTaskNew.execute();
                break;
            case TAGFEED_LIST:
                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", "" + page);
                params.put("range", "N");
                params.put("str", tempTag);
                params.put("type", "T");
                params.put("timeline", "T");

                AppController.apiTaskNew = new APITaskNew(this, params, 520, stmDataResult);
                AppController.apiTaskNew.execute();
                break;
        }
    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.lay_single_post_content_slide_like:
//
//                break;
//            case R.id.lay_single_post_content_slide_comment:
//
//                break;
//            case R.id.lay_single_post_content_slide_share:
//
//                break;
//            case R.id.lay_single_post_content_slide_more:
//
//                break;
//        }
//    }

    public boolean updateResultList(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (tr != null && adapter != null) {
            if (tr != resultList) {
                tr.addAll(resultList);
                adapter.notifyDataSetChanged();
            }
            return true;
        }
        tr = resultList;

        adapter = new PagerAdapter();

        vp.setAdapter(adapter);

        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {

                int count = tr.size();

                if (count % 10 == 0) {
                    if (position >= tr.size() - 3) {
                        page = tr.size() / 10 + 1;
                        Log.e("test_1_pageIndex", page + "");
                        Log.e("test_1_count", count + "");
                        Log.e("test_1_position", position + "");
                        Log.e("test_1_isloading", isLoading + "");
                        if (!isLoading) {
                            getData();
                        }
                    }
                }

                final TimelineItem model = tr.get(position);
                tempTI = model;
                if (model.getPro_url().equals("")) {
                    cimv.setImageResource(R.drawable.icon_timeline_noprofile);
                } else {
                    cimv.setImageURLString(AppController.URL + model.getPro_url());
                }
//72,223
                //
                cimv.setTag(model.getOwnerId());

                double playCnt = (double) model.getPlayCnt();
                DecimalFormat df = new DecimalFormat("#,##0");

                tvName.setText(model.getId());
                tvPlaycount.setText(df.format(playCnt) + "");

                cimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = cimv.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            startActivity(new Intent(SinglePostSlideActivity.this, MyPageV2Activity.class));
                            finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(SinglePostSlideActivity.this, UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                startActivity(i);
                            }
                        }
                    }
                });

                tvName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = cimv.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            startActivity(new Intent(SinglePostSlideActivity.this, MyPageV2Activity.class));
                            finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(SinglePostSlideActivity.this, UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                startActivity(i);
                            }
                        }
                    }
                });

                if (model.isLike()) {
                    layLike.setSelected(true);
                } else {
                    layLike.setSelected(false);
                }


                layComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(SinglePostSlideActivity.this, SinglePostV1Activity.class);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("navi", 1);
                        i.putExtra("likeCount", model.getLikeCount() + "");
                        i.putExtra("comCount", model.getComCount() + "");
                        if (model.isLike() || layLike.isSelected()) {
                            i.putExtra("isLike", true);
                        } else {
                            i.putExtra("isLike", false);
                        }
                        i.putExtra("last", model.getComCount());
                        startActivityForResult(i, NewGridTimelineActivity.COMMENT_RETURN);
                    }
                });

                layShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(SinglePostSlideActivity.this, SharePopupForTimelineActivity.class);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("body", model.getBody());
                        i.putExtra("url", AppController.URL + model.getUrl());
                        i.putExtra("w", model.getWidth());
                        i.putExtra("h", model.getHeight());
                        startActivityForResult(i, NewGridTimelineActivity.SHARE_RETURN);
                    }
                });

                layMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i5 = new Intent(SinglePostSlideActivity.this, Popup2Activity.class);
                        i5.putExtra("email", model.getOwnerId());
                        if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                            i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                            i5.putExtra("postId", model.getPsot_id());
                            i5.putExtra("pc", "P");
                        } else {
                            i5.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                            i5.putExtra("postId", model.getPsot_id());
                            i5.putExtra("pc", "P");
                        }
                        i5.putExtra("url", AppController.URL + model.getUrl());
                        i5.putExtra("title", "");
                        i5.putExtra("body", model.getBody());
                        startActivityForResult(i5, NewGridTimelineActivity.MORE_RETURN);
                    }
                });

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case NewGridTimelineActivity.SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(SinglePostSlideActivity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
//            case POST_WRITE_RETURN:
////                NewTimelineFollowFragment.getData2();
////                NewTimelineAllFragment.getData();
//                getData();
//                break;
            case NewGridTimelineActivity.MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
//                                NewTimelineFollowFragment.getData2();
//                                NewTimelineAllFragment.getData();
                                getData();
                                break;
                            case 3:
                                Intent i = new Intent(SinglePostSlideActivity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                getData();
//                                NewTimelineFollowFragment.getData2();
//                                NewTimelineAllFragment.getData();
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
//            case COMMENT_RETURN:
//                int comCount = data.getExtras().getInt("comCount");
//                String postId = data.getExtras().getString("postId");
//                for (int i = 0; i < m_ResultList.size(); i++) {
//                    if(m_ResultList.get(i).getPsot_id().equals(postId)){
//                        m_ResultList.get(i).setComCount(comCount);
//                        m_ListAdapter.notifyDataSetChanged();
////                        view
//                    }
//                }
//                break;
        }
    }

    private class PagerAdapter extends android.support.v4.view.PagerAdapter {

        LayoutInflater m_LayoutInflater = null;
//        public SparseArray<WeakReference<View>> viewArray;

        public PagerAdapter() {
//            this.viewArray = new SparseArray<WeakReference<View>>(tr.size());
            this.m_LayoutInflater = (LayoutInflater) SinglePostSlideActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            ms = new String[]{
                    getResources().getString(R.string.m1), getResources().getString(R.string.m2), getResources().getString(R.string.m3), getResources().getString(R.string.m4), getResources().getString(R.string.m5), getResources().getString(R.string.m6), getResources().getString(R.string.m7), getResources().getString(R.string.m8), getResources().getString(R.string.m9), getResources().getString(R.string.m10), getResources().getString(R.string.m11), getResources().getString(R.string.m12)
            };

            ds = new String[]{
                    getResources().getString(R.string.d1), getResources().getString(R.string.d2), getResources().getString(R.string.d3), getResources().getString(R.string.d4), getResources().getString(R.string.d5), getResources().getString(R.string.d6), getResources().getString(R.string.d7), getResources().getString(R.string.d8), getResources().getString(R.string.d9), getResources().getString(R.string.d10), getResources().getString(R.string.d11), getResources().getString(R.string.d12), getResources().getString(R.string.d13), getResources().getString(R.string.d14), getResources().getString(R.string.d15), getResources().getString(R.string.d16), getResources().getString(R.string.d17), getResources().getString(R.string.d18), getResources().getString(R.string.d19), getResources().getString(R.string.d20), getResources().getString(R.string.d21), getResources().getString(R.string.d22), getResources().getString(R.string.d23), getResources().getString(R.string.d24), getResources().getString(R.string.d25), getResources().getString(R.string.d26), getResources().getString(R.string.d27), getResources().getString(R.string.d28), getResources().getString(R.string.d29), getResources().getString(R.string.d30), getResources().getString(R.string.d31)
            };
        }

        @Override
        public int getCount() {
            return tr.size();
        }

        @Override
        public Object instantiateItem(View container, int position) {
            View convertView = null;

            convertView = m_LayoutInflater.inflate(R.layout.row_pager, null);

            final BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_single_slide_img);
            final ProgressBar pb = (ProgressBar) convertView.findViewById(R.id.pb_single_post);
            final ImageView imvHeart = (ImageView) convertView.findViewById(R.id.imv_single_post_row_like);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tv_single_slide_body);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tv_single_slide_time);
            final TextView tvLikeCount = (TextView) convertView.findViewById(R.id.tv_single_slide_like_cnt);
            TextView tvCommentCount = (TextView) convertView.findViewById(R.id.tv_single_slide_comment_cnt);
            LinearLayout layLastComment = (LinearLayout) convertView.findViewById(R.id.lay_sinlge_content_last_comment);
            CircleImageView cimvLast = (CircleImageView) convertView.findViewById(R.id.cimv_single_slide_comment_pro);
            TextView tvNameLast = (TextView) convertView.findViewById(R.id.tv_single_slide_comment_id);
            TextView tvBodyLast = (TextView) convertView.findViewById(R.id.tv_single_slide_comment_body);

            convertView.setOnTouchListener(new OnSwipeTouchListener(SinglePostSlideActivity.this) {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (imvGuide.getVisibility() == View.VISIBLE) {
                        imvGuide.setVisibility(View.GONE);
                    }
                    return super.onTouch(view, motionEvent);
                }
            });

            imvHeart.setVisibility(View.GONE);

            layBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            final int pos = position;

            final TimelineItem model = tr.get(pos);

            String tempUrl = model.getUrl();

            int lastIdx = tempUrl.lastIndexOf("_");

            final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

//                bimv.measure(model.getWidth(), model.getHeight());

            TimelineLastCommentItem t = model.getTlci();

            if (t.isExist()) {
                layLastComment.setVisibility(View.VISIBLE);
            } else {
                layLastComment.setVisibility(View.GONE);
            }

            cimvLast.setImageURLString(AppController.URL + t.getPro_url());

//            tvNameLast.setText(t.getCom_id());
            tvNameLast.setText(t.getId());

            tvBodyLast.setText(t.getBody());

            layLastComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(SinglePostSlideActivity.this, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || layLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    i.putExtra("last", model.getComCount());
                    startActivityForResult(i, NewGridTimelineActivity.COMMENT_RETURN);
                }
            });

            downloadThumbImage(bimv, AppController.URL + tempName);

            final View finalConvertView = convertView;

            layLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (layLike.isSelected()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = tempTI.getLikeCount();
                                        tr.get(pos).setLikeCount(lc - 1);
                                        tr.get(pos).setIsLike(false);
                                        notifyDataSetChanged();

                                        layLike.setSelected(false);
                                        tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc - 1) + getResources().getString(R.string.timeline_count));
                                        notifyDataSetChanged();
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        Log.e("result onselect", result);
                                        final JSONObject jd = new JSONObject(result);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = tempTI.getLikeCount();
                                                            tr.get(pos).setLikeCount(lc + 1);
                                                            tr.get(pos).setIsLike(true);
                                                            notifyDataSetChanged();

                                                            layLike.setSelected(true);

                                                            tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc + 1) + getResources().getString(R.string.timeline_count));
                                                            notifyDataSetChanged();
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }

                                            }
                                        });
                                    } catch (JSONException e) {

                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", tempTI.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostSlideActivity.this, params, 303, stmLikeResult);
                            AppController.apiDataTaskNew.execute();


                        } else {

                            final Animation ani1 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like3);

                            imvHeart.setVisibility(View.VISIBLE);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    imvHeart.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    imvHeart.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    imvHeart.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            imvHeart.startAnimation(ani1);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = tempTI.getLikeCount();
                                        tr.get(pos).setLikeCount(lc + 1);
                                        tr.get(pos).setIsLike(true);

                                        layLike.setSelected(true);
                                        tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc + 1) + getResources().getString(R.string.timeline_count));
                                        notifyDataSetChanged();
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        final JSONObject jd = new JSONObject(result);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = tempTI.getLikeCount();
                                                            tr.get(pos).setLikeCount(lc - 1);
                                                            tr.get(pos).setIsLike(false);

                                                            layLike.setSelected(false);
                                                            tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc - 1) + getResources().getString(R.string.timeline_count));
                                                            notifyDataSetChanged();
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", tempTI.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostSlideActivity.this, params, 302, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    } catch (NullPointerException e) {

                    }
                }
            });

            bimv.setOnTouchListener(new OnSwipeTouchListener(SinglePostSlideActivity.this) {
                @Override
                public void onClick() {
                    super.onClick();
                    Drawable d = bimv.getDrawable();
                    if (d instanceof GifDrawable) {
                        try {
                            GifDrawable g = (GifDrawable) d;
                            if (g.isPlaying()) {
                                g.pause();
                            } else {
                                g.start();
                            }
                        } catch (NullPointerException e) {

                        }
                    }
                }

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (imvGuide.getVisibility() == View.VISIBLE) {
                        imvGuide.setVisibility(View.GONE);
                    }
                    return super.onTouch(view, motionEvent);
                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();

                    try {
                        if (layLike.isSelected()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = tempTI.getLikeCount();
                                        tr.get(pos).setLikeCount(lc - 1);
                                        tr.get(pos).setIsLike(false);
                                        notifyDataSetChanged();

                                        layLike.setSelected(false);
                                        tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc - 1) + getResources().getString(R.string.timeline_count));
                                        notifyDataSetChanged();
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        Log.e("result onselect", result);
                                        final JSONObject jd = new JSONObject(result);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = tempTI.getLikeCount();
                                                            tr.get(pos).setLikeCount(lc + 1);
                                                            tr.get(pos).setIsLike(true);
                                                            notifyDataSetChanged();

                                                            layLike.setSelected(true);

                                                            tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc + 1) + getResources().getString(R.string.timeline_count));
                                                            notifyDataSetChanged();
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }

                                            }
                                        });
                                    } catch (JSONException e) {

                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", tempTI.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostSlideActivity.this, params, 303, stmLikeResult);
                            AppController.apiDataTaskNew.execute();


                        } else {

                            final Animation ani1 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(SinglePostSlideActivity.this, R.anim.like3);

                            imvHeart.setVisibility(View.VISIBLE);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    imvHeart.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    imvHeart.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    imvHeart.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            imvHeart.startAnimation(ani1);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = tempTI.getLikeCount();
                                        tr.get(pos).setLikeCount(lc + 1);
                                        tr.get(pos).setIsLike(true);

                                        layLike.setSelected(true);
                                        tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc + 1) + getResources().getString(R.string.timeline_count));
                                        notifyDataSetChanged();
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        final JSONObject jd = new JSONObject(result);

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = tempTI.getLikeCount();
                                                            tr.get(pos).setLikeCount(lc - 1);
                                                            tr.get(pos).setIsLike(false);

                                                            layLike.setSelected(false);
                                                            tvLikeCount.setText(getResources().getString(R.string.like) + " " + "" + (lc - 1) + getResources().getString(R.string.timeline_count));
                                                            notifyDataSetChanged();
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", tempTI.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(SinglePostSlideActivity.this, params, 302, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    } catch (NullPointerException e) {

                    }
                }

            });

            String tempTime = "";

            String tempDate = model.getTime();
            //20151130225233
            int year = Integer.parseInt(tempDate.substring(0, 4));
            int month = Integer.parseInt(tempDate.substring(4, 6));
//            int month = 11;
            int day = Integer.parseInt(tempDate.substring(6, 8));
            int hour = Integer.parseInt(tempDate.substring(8, 10));
            int min = Integer.parseInt(tempDate.substring(10, 12));
            int sec = Integer.parseInt(tempDate.substring(12, 14));

            Log.e("dd", year + "/" + month + "/" + day + "/" + hour + "/" + min + "/" + sec);

            Calendar tempCal = Calendar.getInstance();
            Calendar tempNowCal = Calendar.getInstance();

            DateFormat stringFormat = new SimpleDateFormat("yyyyMMddHHmmss");

            long nowTime, oldTime, intervalTime;

            Calendar intervalCal = Calendar.getInstance();

            try {
                Date nowDate = stringFormat.parse(tempDate);
                tempCal.setTime(nowDate);

                oldTime = tempCal.getTimeInMillis();
                nowTime = System.currentTimeMillis();

                intervalTime = nowTime - oldTime;

                long it = intervalTime / 1000;

                Log.e("intervalT", it + "");

                intervalCal.setTimeInMillis(intervalTime);

                Log.e("interval", intervalCal.get(Calendar.YEAR)
                        + "/" + (intervalCal.get(Calendar.MONTH))
                        + "/" + intervalCal.get(Calendar.DAY_OF_MONTH)
                        + "/" + intervalCal.get(Calendar.HOUR_OF_DAY)
                        + "/" + intervalCal.get(Calendar.MINUTE)
                        + "/" + intervalCal.get(Calendar.SECOND)
                );

                long m = 60;
                long h = 60 * 60;
                long d = 60 * 60 * 24;

                if (it < m) {
                    tempTime = getResources().getString(R.string.just_now);
                } else {
                    if (it < h) {
                        tempTime = it / m + getResources().getString(R.string.timeline_before_minute);
                    } else {
                        if (it < d) {
                            tempTime = it / h + getResources().getString(R.string.timeline_before_hour);
                        } else {
                            if (it < d * 2) {
                                tempTime = getResources().getString(R.string.yesterday);
                            } else {
                                tempTime = year + getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
                            }
                        }
                    }
                }
                Log.e("tempTIme", tempTime);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            tvTime.setText(tempTime);
            tvBody.setText(model.getBody());
//            tvLikeCnt.setText(con.getResources().getString(R.string.like) + " " + model.getLikeCount() + con.getResources().getString(R.string.timeline_count));
//            tvComCnt.setText(con.getResources().getString(R.string.comment) + " " + model.getComCount() + con.getResources().getString(R.string.timeline_count));
            tvLikeCount.setText(getResources().getString(R.string.like) + " " + model.getLikeCount() + getResources().getString(R.string.timeline_count));
            tvCommentCount.setText(getResources().getString(R.string.comment) + " " + model.getComCount() + getResources().getString(R.string.timeline_count));

            String commentsText = tvBody.getText().toString();

            ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
            ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
            ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

            SpannableString commentsContent = new SpannableString(commentsText);

            for (int i = 0; i < hashtagSpans.size(); i++) {
                int[] span = hashtagSpans.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent.setSpan(new HashTag(SinglePostSlideActivity.this),
                        hashTagStart, hashTagEnd, 0);
            }

            for (int i = 0; i < calloutSpans.size(); i++) {
                int[] span = calloutSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];

                commentsContent.setSpan(new CallTag(SinglePostSlideActivity.this),
                        calloutStart,
                        calloutEnd, 0);

            }

            for (int i = 0; i < andSpans.size(); i++) {
                int[] span = andSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];


                AndTag at = new AndTag(SinglePostSlideActivity.this);
                at.setTagId(model.getAndTagId());
                commentsContent.setSpan(at,
                        calloutStart,
                        calloutEnd, 0);

            }

            tvBody.setMovementMethod(LinkMovementMethod.getInstance());
            tvBody.setText(commentsContent);
            tvBody.setHighlightColor(getResources().getColor(android.R.color.transparent));

            tvCommentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(SinglePostSlideActivity.this, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || layLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    i.putExtra("last", model.getComCount());
                    startActivityForResult(i, NewGridTimelineActivity.COMMENT_RETURN);
                }
            });
            tvLikeCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(SinglePostSlideActivity.this, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    startActivity(i);
                }
            });


            bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
                @Override
                public void onImageChanged(Drawable d) {
                    if (d instanceof GifDrawable) {

                        pb.setVisibility(View.GONE);
                        final GifDrawable gd = (GifDrawable) d;

                        if (gd != null) {
                            gd.addAnimationListener(new AnimationListener() {
                                @Override
                                public void onAnimationCompleted(int loopNumber) {

//                                    StringTransMethod stmCount = new StringTransMethod() {
//
//                                    };
//
//                                    Map<String, String> params = new HashMap<>();
//                                    params.put("post_id", model.getPsot_id());
//
//                                    Log.e("counttttttt", loopNumber + "");

                                    AppController.playCountExe.execute(new Runnable() {
                                        @Override
                                        public void run() {
//                                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                                            new MinUtils.PlayCountSubmit(tr.get(vp.getCurrentItem()).getPsot_id()).execute();
                                        }
                                    });

                                }

                            });
                        }
                    } else {

                        String tempUrl2 = model.getUrl();

                        int lastIdx2 = tempUrl2.lastIndexOf("_");

                        final String tempName2 = tempUrl2.substring(0, lastIdx2) + "_2.gif";

                        downloadGifbImage(bimv, AppController.URL + tempName2);

                    }

                }
            });

            ((ViewPager) container).addView(convertView, 0);

            return convertView;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == object;
        }


        @Override
        public void finishUpdate(View arg0) {
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public void downloadThumbImage(ImageView imageView, String url) {

        Log.e("downloadThumbImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.thumbRun = new MinThumbImageRun(SinglePostSlideActivity.this, url, imageView, image, file_size2, 1);
//
//        if (AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
//
//        AppController.thumbExe.submit(AppController.thumbRun);

    }

    public void downloadGifbImage(ImageView imageView, String url) {

        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun(SinglePostSlideActivity.this, url, imageView, image, file_size2, 1);
//
//        if (AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }
}
