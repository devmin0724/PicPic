/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.popup.CategorySelectPopUp;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageRun;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.widget.HashTagV1;
import com.picpic.sikkle.widget.tags.Link;
import com.picpic.sikkle.widget.tags.TagEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.GifDrawable;

public class EditPostActivityV1 extends Activity implements View.OnClickListener {

    private static final int FILE_UPLOAD_RETURN_1 = 1111;
    private static final int FILE_UPLOAD_RETURN_2 = 1112;
    private static final int FILE_UPLOAD_RETURN_4 = 1115;
    private static final int CATEGORY_SELECT_RETURN = 2222;
    private final int TEXT_LIMIT_COUNT = 140;

    LinearLayout layBack, layUpload, layGuide, layCategory;
    ImageView imv;
    TagEditText input, edtTags;
    TextView tvCount, tvCategory;
    String url1, url2, fileName2, thumb;
    ArrayList<int[]> hashtagSpans, calloutSpans, andSpans;
    String hashCollection = "", userCollection = "", andCollection = "";
    int intentNavi = 0, count = 0;
    String tempUrl, tempPostId, tagList;
    GIFLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_write_post_activity_v1);

        tempPostId = getIntent().getExtras().getString("postId");

        layBack = (LinearLayout) findViewById(R.id.lay_write_post_v1_back);
        layUpload = (LinearLayout) findViewById(R.id.lay_write_post_v1_upload);
        layCategory = (LinearLayout) findViewById(R.id.lay_write_post_v1_category);

        imv = (ImageView) findViewById(R.id.imv_write_post_v1_preview);

        input = (TagEditText) findViewById(R.id.edt_write_post_v1_input);

        edtTags = (TagEditText) findViewById(R.id.edt_write_post_v1_tags);

        tvCategory = (TextView) findViewById(R.id.tv_write_post_v1_category);
        tvCount = (TextView) findViewById(R.id.tv_write_post_v1_count);

        layBack.setOnClickListener(this);
        layUpload.setOnClickListener(this);
        layCategory.setOnClickListener(this);
        tvCategory.setOnClickListener(this);

        layGuide = (LinearLayout) findViewById(R.id.lay_write_post_v1_guide);
        layGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layGuide.setVisibility(View.GONE);
            }
        });

        linkSetting();

        loader = new GIFLoader(EditPostActivityV1.this);

//        startExtMediaScan(getApplicationContext());

        input.addTextChangedListener(new TextWatcher() {
            String lastest = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                lastest = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tempS = s.toString();
                int byteLength = tempS.getBytes().length;
                if (byteLength > 140) {
                    input.setText(lastest);
                }
                count = 140 - byteLength;
                tvCount.setText(count + "");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtTags.addTextChangedListener(new TextWatcher() {
            String tempStr = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                tempStr = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String tempS = s.toString();
                if (tempS.length() == 0) {
                    edtTags.setText("#");
                    edtTags.setSelection(edtTags.getText().toString().length());
                    Log.e("navi", "1");
                } else {
                    Log.e("navi", "2");
                    if (tempStr.length() < tempS.length()) {
                        Log.e("navi", "3");
                        if (tempS.substring(tempS.length() - 1).equals(" ")) {
                            Log.e("navi", "4");
                            edtTags.setText(edtTags.getText().toString() + " #");
                            edtTags.setSelection(edtTags.getText().toString().length());
                        }
                    }
                }

                linkSetting();
                Log.e("navi", "5");

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    input.setCursorVisible(true);
                } else {
                    input.setCursorVisible(false);
                }

            }
        });

        edtTags.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edtTags.setCursorVisible(true);
                } else {
                    edtTags.setCursorVisible(false);
                }
            }
        });

        getData();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_write_post_v1_back:
                onBackPressed();
                break;
            case R.id.lay_write_post_v1_upload:
                layUpload.setClickable(false);

                getTags();

                StringTransMethod stmEditPostResult = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        setResult(RESULT_OK);
                        finish();
                    }
                };

//                Map<String, String> data2 = new HashMap<>();
//
//                data2.put("my_id", AppController.getSp().getString("email", ""));
//                data2.put("body", input.getText().toString());
//                data2.put("url", tempUrl);
//                data2.put("type", "E");
//                data2.put("tags", hashCollection);
//                data2.put("and_tag", andCollection);
//                data2.put("user_tags", userCollection);
//                data2.put("post_id", tempPostId);
//
//                AppController.apiDataTaskNew = new APIDataTaskNew(EditPostActivityV1.this, data2, 232, stmEditPostResult);
//                AppController.apiDataTaskNew.execute();

                Map<String, String> data2 = new HashMap<>();

                data2.put("my_id", AppController.getSp().getString("email", ""));
                data2.put("body", input.getText().toString());
                data2.put("url", tempUrl);
                data2.put("type", "E");
                data2.put("tags", hashCollection);
                data2.put("and_tag", "");
                data2.put("user_tags", "");
                data2.put("post_id", tempPostId);

                AppController.apiDataTaskNew = new APIDataTaskNew(EditPostActivityV1.this, data2, 232, stmEditPostResult);
                AppController.apiDataTaskNew.execute();
                break;
            case R.id.lay_write_post_v1_category:
            case R.id.tv_write_post_v1_category:
                //TODO
                Intent ii = new Intent(EditPostActivityV1.this, CategorySelectPopUp.class);
                ii.putExtra("txt", tvCategory.getText().toString());
                startActivityForResult(ii, CATEGORY_SELECT_RETURN);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_UPLOAD_RETURN_1) {
            if (resultCode == RESULT_OK) {

                Intent i = new Intent(EditPostActivityV1.this, FileUploadActivity.class);
                i.putExtra("url", url2);
                i.putExtra("file", fileName2);
                startActivityForResult(i, FILE_UPLOAD_RETURN_2);
            }
        } else if (requestCode == FILE_UPLOAD_RETURN_2) {

            if (resultCode == RESULT_OK) {
                StringTransMethod stmFileSetting = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        StringTransMethod stmWrite = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);

                                    if (jd.getInt("result") == 0) {
                                        setResult(RESULT_OK);
                                        finish();
                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };
                        Map<String, String> data2 = new HashMap<>();

                        data2.put("my_id", AppController.getSp().getString("email", ""));
                        data2.put("body", input.getText().toString());
                        data2.put("url", fileName2);
                        data2.put("type", "W");
                        data2.put("tags", hashCollection);
                        data2.put("and_tag", andCollection);
                        data2.put("user_tags", userCollection);

                        AppController.apiDataTaskNew = new APIDataTaskNew(EditPostActivityV1.this, data2, 232, stmWrite);
                        AppController.apiDataTaskNew.execute();
                    }
                };

                Map<String, String> params = new HashMap<>();
                params.put("url", fileName2);

                AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 233, stmFileSetting);
                AppController.apiDataTaskNew.execute();

            }

        } else if (requestCode == FILE_UPLOAD_RETURN_4) {
            if (resultCode == RESULT_OK) {
                //TODO post쓰기
                StringTransMethod stmWrite = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);

                            if (jd.getInt("result") == 0) {
                                File f1 = new File(url1);

                                f1.delete();

                                setResult(RESULT_OK);
                                finish();
                            } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                Map<String, String> data2 = new HashMap<>();

                data2.put("my_id", AppController.getSp().getString("email", ""));
                data2.put("body", input.getText().toString());
                data2.put("url", fileName2);
                data2.put("type", "W");
                data2.put("tags", hashCollection);
                data2.put("and_tag", andCollection);
                data2.put("user_tags", userCollection);

                AppController.apiDataTaskNew = new APIDataTaskNew(this, data2, 221, stmWrite);
                AppController.apiDataTaskNew.execute();

            }
        } else if (requestCode == CATEGORY_SELECT_RETURN) {
            if (resultCode == RESULT_OK) {
                tvCategory.setText(data.getExtras().getString("category"));
            }
        }
        layUpload.setClickable(true);
    }

    MultiAutoCompleteTextView.Tokenizer customT = new MultiAutoCompleteTextView.Tokenizer() {
        @Override
        public int findTokenStart(CharSequence text, int cursor) {
            int i = cursor;

            while (i > 0 && text.charAt(i - 1) != ' ') {
                i--;
            }
            while (i < cursor && text.charAt(i) == ' ') {
                i++;
            }

            return i;
        }

        @Override
        public int findTokenEnd(CharSequence text, int cursor) {
            int i = cursor;
            int len = text.length();

            while (i < len) {
                if (text.charAt(i) == ' ') {
                    return i;
                } else {
                    i++;
                }
            }

            return len;
        }

        @Override
        public CharSequence terminateToken(CharSequence text) {
            int i = text.length();

            while (i > 0 && text.charAt(i - 1) == ' ') {
                i--;
            }

            if (i > 0 && text.charAt(i - 1) == ' ') {
                return text;
            } else {
                return text + " ";
            }
        }
    };

    public void startExtMediaScan(Context mContext) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(new File(url2)); //out is your output file
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://"
                            + Environment.getExternalStorageDirectory())));
        }


    }

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("post_id", tempPostId);

        AppController.apiTaskNew = new APITaskNew(this, params, 504, stmPostDataResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmPostDataResult = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                final JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
//                                MinUtils.d("test", result);
                                Log.e("asjkdbnqo", result);
                                Log.e("asjkdbnqo2", jd.toString());
//                                Log.e("jd1", jd.getString("url") + "/" + jd.getString("contents"));
//                                Log.e("jd2", );
//                                Log.e("jd3", jd.getString("tag_list"));

                                tempUrl = jd.getString("url");

                                loader.DisplayImage(AppController.URL + jd.getString("url"), imv, false);
////                                downloadGifbImage(imv, AppController.URL + jd.getString("url"));
//
//                                input.setText(jd.getString("contents") + " ");
//
//                                input.requestFocus();
//                                edtTags.setText(jd.getString("tag_list"));

                                tagList = jd.getString("tag_list");

                                if(tagList.equals(",")||tagList.length() < 1){

                                }else{
                                    String tagLists[] = tagList.split(",");

                                    for (int i = 0; i < tagLists.length; i++) {
                                        edtTags.setText(edtTags.getText().toString() + tagLists[i]+" ");
                                    }
                                }

                                input.setText(jd.getString("contents"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (JSONException e) {

            }

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    private void getTags() {
        String commentsText = edtTags.getText().toString();

        hashtagSpans = getSpans(commentsText, '#');
        for (int i = 0; i < hashtagSpans.size(); i++) {
            hashCollection = hashCollection + commentsText.substring(hashtagSpans.get(i)[0] + 1, hashtagSpans.get(i)[1]) + ",";
        }

        if (hashCollection.length() > 1) {
            hashCollection = hashCollection.substring(0, hashCollection.length() - 1);
        }

        if (!tvCategory.getText().toString().equals(getResources().getString(R.string.category_0))) {
            if (hashCollection.length() != 0) {
                hashCollection += "," + tvCategory.getText().toString();
            } else {
                hashCollection += tvCategory.getText().toString();
            }
        }
    }

    public void downloadGifbImage(ImageView imageView, String url) {

        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun(EditPostActivityV1.this, url, imageView, image, file_size2, 1);
//
//        if (AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }

    private void linkSetting() {
        Link linkHashtag = new Link(Pattern.compile("(#\\w+)"))
                .setUnderlined(false)
                .setTextColor(0xfff5f5f5);

        List<Link> links = new ArrayList<>();
        links.add(linkHashtag);

        edtTags.addLinks(links);
    }

}
