package com.picpic.sikkle.ui.login_join;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.FriendResultItem;
import com.picpic.sikkle.fragment.FindFriendFaceBookFragment;
import com.picpic.sikkle.fragment.FindFriendGooglePlusFragment;
import com.picpic.sikkle.fragment.FindFriendRecommandFragment;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.utils.AppController;

import java.util.ArrayList;

public class FriendResultActivity extends FragmentActivity implements View.OnClickListener {
    TextView title1;
    ArrayList<FriendResultItem> arr;
    LinearLayout imvNext;
    int pageNavi = 0, joinNavi = 0;
    ArrayList<String> friendsArr;
    FrameLayout lay1, lay2, lay3;
    private ViewPager mViewPager;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_friend_result);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("친구찾기 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        pageNavi = getIntent().getExtras().getInt("navi");
        joinNavi = getIntent().getExtras().getInt("joinNavi");
        if (joinNavi == 1) {
            friendsArr = getIntent().getStringArrayListExtra("friends");
        }

        mViewPager = (ViewPager) findViewById(R.id.vp_friend_result);

        title1 = (TextView) findViewById(R.id.tv_friend_result_title);

        imvNext = (LinearLayout) findViewById(R.id.imv_friend_result_next);
        imvNext.setOnClickListener(this);

        lay1 = (FrameLayout) findViewById(R.id.lay_tab_title_1);
        lay1.setOnClickListener(this);
        lay2 = (FrameLayout) findViewById(R.id.lay_tab_title_2);
        lay2.setOnClickListener(this);
        lay3 = (FrameLayout) findViewById(R.id.lay_tab_title_3);
        lay3.setOnClickListener(this);

        FragmentPagerItemAdapter adapter;
        if (pageNavi == 2) {
            lay1.setVisibility(View.GONE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            adapter = new FragmentPagerItemAdapter(
                    getSupportFragmentManager(), FragmentPagerItems.with(this)
                    .add(R.string.find_friend_facebook, FindFriendFaceBookFragment.class)
                    .add(R.string.find_friend_google, FindFriendGooglePlusFragment.class)
                    .create());
        } else {
            lay1.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.VISIBLE);
            adapter = new FragmentPagerItemAdapter(
                    getSupportFragmentManager(), FragmentPagerItems.with(this)
                    .add(R.string.find_friend_recommend, FindFriendRecommandFragment.class)
                    .add(R.string.find_friend_facebook, FindFriendFaceBookFragment.class)
                    .add(R.string.find_friend_google, FindFriendGooglePlusFragment.class)
                    .create());
        }

        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (pageNavi == 2) {
                    if (position == 0) {
                        lay2.setSelected(true);
                        lay3.setSelected(false);
                    } else {
                        lay2.setSelected(false);
                        lay3.setSelected(true);
                    }
                } else {
                    if (position == 0) {
                        lay1.setSelected(true);
                        lay2.setSelected(false);
                        lay3.setSelected(false);
                    } else if (position == 1) {
                        lay1.setSelected(false);
                        lay2.setSelected(true);
                        lay3.setSelected(false);
                    } else {
                        lay1.setSelected(false);
                        lay2.setSelected(false);
                        lay3.setSelected(true);
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.tab_indicator);
//        viewPagerTab.setSelectedIndicatorColors(0xff697fff);
//        viewPagerTab.setDividerColors(0x00ffffff);
//        viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {
//            @Override
//            public View createTabView(ViewGroup viewGroup, int i, android.support.v4.view.PagerAdapter pagerAdapter) {
//                View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_title, null, false);
//
//                ImageView imv = (ImageView) v.findViewById(R.id.imv_tab_title);
//                TextView tv = (TextView) v.findViewById(R.id.tv_tab_title);
//                LinearLayout lay = (LinearLayout) v.findViewById(R.id.lay_tab_title);
//
//                int size = pagerAdapter.getCount();
//
//                switch (i) {
//                    case 0:
//                        if (pageNavi == 2) {
//                            imv.setImageResource(R.drawable.btn_onclick_find_friend_facebook);
//                            tv.setText(getResources().getString(R.string.find_friend_facebook));
//                        } else {
//                            imv.setImageResource(R.drawable.btn_onclick_find_friend_recommend);
//                            tv.setText(getResources().getString(R.string.find_friend_recommend));
//                        }
//                        break;
//                    case 1:
//                        if (pageNavi == 2) {
//                            imv.setImageResource(R.drawable.btn_onclick_find_friend_google);
//                            tv.setText(getResources().getString(R.string.find_friend_google));
//                        } else {
//                            imv.setImageResource(R.drawable.btn_onclick_find_friend_facebook);
//                            tv.setText(getResources().getString(R.string.find_friend_facebook));
//                        }
//                        break;
//                    case 2:
//                        imv.setImageResource(R.drawable.btn_onclick_find_friend_google);
//                        tv.setText(getResources().getString(R.string.find_friend_google));
//                        break;
//                    default:
//                        imv.setImageResource(R.drawable.btn_onclick_find_friend_recommend);
//                        tv.setText(getResources().getString(R.string.find_friend_recommend));
//                        break;
//                }
//
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(MinUtils.screenWidth / size, LinearLayout.LayoutParams.MATCH_PARENT);
////
//                lay.setLayoutParams(params);
//
//                return v;
//            }
//
//        });
//        viewPagerTab.setViewPager(mViewPager);

    }

    @Override
    public void onBackPressed() {
        if (pageNavi == 2) {
            finish();
            overridePendingTransition(0, 0);
            startActivity(new Intent(FriendResultActivity.this, TimeLineActivity.class));
        } else {
            finish();
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_friend_result_next:
                startActivity(new Intent(FriendResultActivity.this, TimeLineActivity.class));
                finish();
                break;
            case R.id.lay_tab_title_1:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.lay_tab_title_2:
                if (pageNavi == 2) {
                    mViewPager.setCurrentItem(0);
                } else {
                    mViewPager.setCurrentItem(1);
                }
                break;
            case R.id.lay_tab_title_3:
                if (pageNavi == 2) {
                    mViewPager.setCurrentItem(1);
                } else {
                    mViewPager.setCurrentItem(2);
                }
                break;
        }
    }

}
