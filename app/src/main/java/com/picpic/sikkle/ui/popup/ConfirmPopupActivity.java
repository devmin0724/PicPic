package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class ConfirmPopupActivity extends Activity {

    TextView tv;
    Button left, right;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_confirm_popup);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("확인 팝업");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        tv = (TextView) findViewById(R.id.tv_confirm_popup_title);

        left = (Button) findViewById(R.id.btn_confirm_popup_left);
        right = (Button) findViewById(R.id.btn_confirm_popup_right);

        switch (getIntent().getExtras().getInt("navi")) {
            case 0:
                tv.setText(getResources().getString(R.string.is_block));
                break;
            case 1:
                tv.setText(getResources().getString(R.string.is_delete));
                break;
        }
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
