package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.picpic.sikkle.R;
import com.picpic.sikkle.Test3Activity;
import com.picpic.sikkle.ui.NewGridTimelineActivity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.MomentUtil;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;


public class LoginActivity extends FragmentActivity implements View.OnClickListener {
//        GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener,
//        GoogleApiClient.ServerAuthCodeCallbacks {

    private static final String TAG = "LoginActivity";
    private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;
    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_ERROR_DIALOG = 2;
    private static final String KEY_NEW_CODE_REQUIRED = "codeRequired";
    private static final String KEY_SIGN_IN_CLICKED = "signInClicked";
    private static final String KEY_INTENT_IN_PROGRESS = "intentInProgress";
    public static Activity _ac;
    private final AtomicBoolean mServerAuthCodeRequired = new AtomicBoolean(false);
    Button btnSignin;
    LinearLayout layLogin;
    List<String> permissionNeeds = new ArrayList<>();
    ImageView imvBG;
    ArrayList<String> friendsArr;
    LinearLayout loginButton;
    CallbackManager callbackManager;
    String id = "", birth = "", gender = "", name = "", pw = "", profile_url = "";
    //    private GoogleApiClient mGoogleApiClient;
//    private LinearLayout mSignInButton;
    private boolean mSignInClicked;
    private boolean mIntentInProgress;
    private LoginManager loginManager;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
//        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
//        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

//        GoogleApiClient

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("로그인 첫번째 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        friendsArr = new ArrayList<>();

        _ac = this;
        permissionNeeds.add("email");
        permissionNeeds.add("user_birthday");
        permissionNeeds.add("public_profile");
        permissionNeeds.add("user_friends");

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();

        loginButton = (LinearLayout) findViewById(R.id.fb_login_button);
        loginButton.setOnClickListener(this);

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                final GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                if (object == null) {

                                } else {
                                    Log.e("aaa", object.toString());
                                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Login Click").build());
                                    try {
                                        if (object.isNull("email")) {
                                            id = object.getString("id");
                                        } else {
                                            id = object.getString("email");
                                        }
                                        if (object.isNull("birthday")) {
                                            birth = "01/01/1900";
                                        } else {
                                            birth = object.getString("birthday");
                                        }
                                        gender = object.getString("gender");
                                        name = object.getString("name");
                                        pw = object.getString("id");
                                        JSONObject j = new JSONObject(object.getString("picture"));
                                        JSONObject jj = new JSONObject(j.getString("data"));
                                        profile_url = jj.getString("url");

                                        HashMap<String, String> params = new HashMap<>();
                                        params.put("mId", id);

                                        String[] births = birth.split("/");
                                        String realBirth = "";
                                        realBirth = births[2] + "/" + births[1] + "/" + births[0];

                                        Log.e("realBirth", realBirth);

                                        boolean realGender = true;

                                        realGender = gender.equals("male");

                                        JSONObject j1 = new JSONObject(object.toString());

                                        JSONObject j2 = new JSONObject(j1.getString("friends"));

                                        JSONArray jarr = new JSONArray(j2.getString("data"));

                                        for (int ii = 0; ii < jarr.length(); ii++) {
                                            friendsArr.add(jarr.getJSONObject(ii).getString("id"));
                                        }

                                        final String finalRealBirth = realBirth;
                                        final boolean finalRealGender = realGender;
                                        StringTransMethod stmFJoin = new StringTransMethod() {
                                            @Override
                                            public void endTrans(String result) {
                                                try {
                                                    JSONObject jd = new JSONObject(result);

                                                    if (jd.getString("data").equals("0")) {
                                                        Intent i = new Intent(LoginActivity.this, ProfilePictureActivity.class);
                                                        i.putExtra("email", id);
                                                        i.putExtra("pw", pw);
                                                        i.putExtra("birth", finalRealBirth);
                                                        i.putExtra("sex", finalRealGender + "");
                                                        i.putExtra("id", name.replace(" ", "_").replace(",", "").replace(".", ""));
                                                        i.putExtra("navi", 1);
                                                        i.putExtra("profile_url", profile_url);
                                                        i.putStringArrayListExtra("friends", friendsArr);

                                                        MinUtils.friendsArr = friendsArr;

                                                        if (friendsArr.size() != 0 && friendsArr != null) {
                                                            AppController.getSe().putInt("fFriendsSize", friendsArr.size());

                                                            for (int k = 0; k < friendsArr.size(); k++) {
                                                                AppController.getSe().putString("fFriends" + (k + 1), friendsArr.get(k));
                                                            }
                                                        }

                                                        Log.e("profile_url", profile_url);
                                                        startActivity(i);
                                                    } else if (jd.getString("data").equals("1")) {
                                                        if (jd.getString("register_form").equals("10002")) {
                                                            //로그인

                                                            Date now = new Date();
                                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                                                            String tempC = sdf.format(now);

                                                            HashMap<String, String> params2 = new HashMap<>();

                                                            params2.put("email", pw);
                                                            params2.put("password", pw);

                                                            AppController.getSe().putString("password", pw);
                                                            AppController.getSe().commit();

                                                            params2.put("register_form", "10002");
                                                            params2.put("country", AppController.getSp().getString("country", "ko_KR"));
                                                            params2.put("device_id", MinUtils.DID);
                                                            params2.put("push_token", MinUtils.PTK);
                                                            params2.put("regist_day", tempC);

                                                            AppController.apiDataTaskNew = new APIDataTaskNew(LoginActivity.this, params2, 202, stmLogin);
                                                            AppController.apiDataTaskNew.execute();

                                                        } else {
                                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.already_email_with_facebook), Toast.LENGTH_LONG).show();
                                                        }
                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {

                                                }
                                            }
                                        };

                                        Map<String, String> params3 = new HashMap<>();
                                        params3.put("email", pw);

                                        AppController.apiTaskNew = new APITaskNew(LoginActivity.this, params3, 213, stmFJoin);
                                        AppController.apiTaskNew.execute();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture,friends");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

//        mGoogleApiClient = buildGoogleApiClient(true);

//        mSignInButton = (LinearLayout) findViewById(R.id.sign_in_button);
//        mSignInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
//                    mSignInClicked = true;
//                    mGoogleApiClient.connect();
//                }
//            }
//        });

        restoreState(savedInstanceState);

        imvBG = (ImageView) findViewById(R.id.imv_login_bg);

        imvBG.setImageResource(R.drawable.loginimv);

//        try {
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("login_bg_1.gif"));
//            imvBG.setImageDrawable(gd);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        initViews();

    }

    StringTransMethod stmLogin = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    JSONObject loginData = new JSONObject(jd.getString("data"));
                    SharedPreferences.Editor se = AppController.getSe();
                    se.putString("bir_year", loginData.getString("bir_year"));
                    se.putString("bir_mon", loginData.getString("bir_mon"));
                    se.putString("bir_day", loginData.getString("bir_day"));
                    se.putString("country", loginData.getString("country"));
                    se.putString("cut_day", loginData.getString("cut_day"));
                    se.putString("cut_reason", loginData.getString("cut_reason"));
                    se.putString("declare_cnt", loginData.getString("declare_cnt"));
                    se.putString("device_id", loginData.getString("device_id"));
                    se.putString("email", loginData.getString("email"));
                    se.putString("facebook_id", loginData.getString("facebook_id"));
                    se.putString("fri_open_yn", loginData.getString("fri_open_yn"));
                    se.putString("id", loginData.getString("id"));
                    se.putString("m_id", loginData.getString("m_id"));
                    se.putString("pinterest_id", loginData.getString("pinterest_id"));
                    se.putString("posts_open_form", loginData.getString("posts_open_form"));
                    se.putString("profile_picture", loginData.getString("profile_picture"));
                    se.putString("push_token", loginData.getString("push_token"));
                    se.putString("regist_day", loginData.getString("regist_day"));
                    se.putString("register_form", loginData.getString("register_form"));
                    se.putString("sex", loginData.getString("sex"));
                    se.putString("tumbler_id", loginData.getString("tumbler_id"));
                    se.putString("twitter_id", loginData.getString("twitter_id"));
                    se.putBoolean("isAutoLogin", true);
                    se.commit();

                    AppController.t.set("&uid", loginData.getString("email"));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory("FaceBookLogin").build());

//                    startActivity(new Intent(LoginActivity.this, NewGridTimelineActivity.class));
//                    LoginActivity._ac.finish();
//                    Intent i = new Intent(LoginActivity.this, NewGridTimelineActivity.class);
                    Intent i = new Intent(LoginActivity.this, TimelineV1NewActivity.class);
//                    Intent i = new Intent(LoginActivity.this, Test3Activity.class);
                    i.putExtra("navi", 0);
                    startActivity(i);
                    LoginActivity._ac.finish();
                    finish();
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                } else {
                    AppController.getSe().putBoolean("isAutoLogin", false);
                    AppController.getSe().commit();
                }
            } catch (JSONException e) {

            }
        }
    };

    private void initViews() {
        btnSignin = (Button) findViewById(R.id.btn_login_sign_in);
        btnSignin.setOnClickListener(this);

        layLogin = (LinearLayout) findViewById(R.id.lay_login_lgoin);
        layLogin.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SIGN_IN) {

//            if (resultCode == RESULT_OK) {
//                if (!mGoogleApiClient.isConnected()) {
//                    mGoogleApiClient.reconnect();
//                }
//            } else {
//
//                if (resultCode == RESULT_CANCELED) {
//                    Log.e("Google+ state", "sign out");
//                } else {
//                    Log.e("Google+ state", "sign in error");
//                    Log.w(TAG, "Error during resolving recoverable error.");
//                }
//            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_sign_in:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Join Click").build());
                overridePendingTransition(0, 0);
                startActivity(new Intent(LoginActivity.this, JoinEmailActivity.class));
                break;
            case R.id.lay_login_lgoin:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Login Click").build());
                overridePendingTransition(0, 0);
                startActivity(new Intent(LoginActivity.this, NewLoginActivity.class));
                break;
            case R.id.fb_login_button:
                loginManager.logInWithReadPermissions(this, Arrays.asList("email", "user_birthday", "public_profile", "user_friends"));
                break;
        }
    }

//    private GoogleApiClient buildGoogleApiClient(boolean useProfileScope) {
//        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this);
//
//        String serverClientId = getString(R.string.server_client_id);
//
//        if (!TextUtils.isEmpty(serverClientId)) {
//            builder.requestServerAuthCode(serverClientId, this);
//        }
//
//        if (useProfileScope) {
//            builder.addApi(Plus.API)
//                    .addScope(new Scope("profile"));
//        } else {
//            builder.addApi(Plus.API, Plus.PlusOptions.builder()
//                    .addActivityTypes(MomentUtil.ACTIONS).build())
//                    .addScope(Plus.SCOPE_PLUS_LOGIN);
//        }
//
//        return builder.build();
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_NEW_CODE_REQUIRED, mServerAuthCodeRequired.get());
        outState.putBoolean(KEY_SIGN_IN_CLICKED, mSignInClicked);
        outState.putBoolean(KEY_INTENT_IN_PROGRESS, mIntentInProgress);
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mServerAuthCodeRequired.set(isUsingOfflineAccess());
        } else {
            mServerAuthCodeRequired.set(
                    savedInstanceState.getBoolean(KEY_NEW_CODE_REQUIRED, false));
            mSignInClicked = savedInstanceState.getBoolean(KEY_SIGN_IN_CLICKED, false);
            mIntentInProgress = savedInstanceState.getBoolean(KEY_INTENT_IN_PROGRESS, false);
        }
    }

    private boolean isUsingOfflineAccess() {
        return !TextUtils.isEmpty(getString(R.string.server_client_id));
    }

//    @Override
//    public CheckResult onCheckServerAuthorization(String idToken, Set<Scope> scopeSet) {
//        if (mServerAuthCodeRequired.get()) {
//            Set<Scope> scopes = new HashSet<>();
//            scopes.add(Plus.SCOPE_PLUS_LOGIN);
//
//            scopes.add(new Scope(Scopes.DRIVE_APPFOLDER));
//            return CheckResult.newAuthRequiredResult(scopes);
//        } else {
//            return CheckResult.newAuthNotRequiredResult();
//        }
//    }
//
//    @Override
//    public boolean onUploadServerAuthCode(String idToken, String serverAuthCode) {
//        Log.d(TAG, "upload server auth code " + serverAuthCode + " requested, faking success");
//        mServerAuthCodeRequired.set(false);
//        return true;
//    }
//
//    @Override
//    public void onConnected(Bundle connectionHint) {
//        Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//        String currentPersonName = person != null
//                ? person.getDisplayName()
//                : "unknown";
//        Toast.makeText(getApplicationContext(), currentPersonName, Toast.LENGTH_LONG).show();
//        Log.e("personName", currentPersonName);
//        mSignInClicked = false;
//    }
//
//    @Override
//    public void onConnectionSuspended(int cause) {
//        mGoogleApiClient.connect();
//    }
//
//    @Override
//    public void onConnectionFailed(ConnectionResult result) {
//        if (!mIntentInProgress && mSignInClicked) {
//            if (result.hasResolution()) {
//                try {
//                    result.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
//                    mIntentInProgress = true;
//                } catch (IntentSender.SendIntentException e) {
//                    mIntentInProgress = false;
//                    Log.w(TAG, "Error sending the resolution Intent, connect() again.");
//                    mGoogleApiClient.connect();
//                }
//            } else {
//                GoogleApiAvailability.getInstance().showErrorDialogFragment(
//                        this, result.getErrorCode(), REQUEST_CODE_ERROR_DIALOG);
//            }
//        }
//    }
}
