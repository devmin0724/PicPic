/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.messenger.MessengerUtils;
import com.facebook.messenger.ShareToMessengerParams;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.FileCache2;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShareDeletePopUp extends Activity implements View.OnClickListener {

    public static final String TAG = "TumblrActivity";
    private static final int REQUEST_CODE_TUMBLR_LOGIN = 5593;
    private static final int REQUEST_CODE_SHARE_TO_MESSENGER = 10001;
    public static ProgressDialog mProgressDialog;
    LinearLayout laySave, layFace, layKakao, layDownLoad, layMore, layFM;
    ShareDialog shareDialog;
    String strPostId, strBody, strUrl;
    CallbackManager callbackManager;
    int w = 0, h = 0;
    boolean isTrans = false;

    private long latestId = -1;

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Uri urlToDownload;
    BroadcastReceiver onComlete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, getResources().getString(R.string.downlaod_complete), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_share_delete_pop_up);
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("공유하기 리픽삭제 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        strPostId = getIntent().getExtras().getString("postId");
        strBody = getIntent().getExtras().getString("body");
        strUrl = getIntent().getExtras().getString("url");

        w = getIntent().getExtras().getInt("w");
        h = getIntent().getExtras().getInt("h");

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        registerReceiver(onComlete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("SNS에 공유중입니다.");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        initViews();
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void shareItAll() {
        Intent sendFile = new Intent(Intent.ACTION_SEND);

        FileCache2 fc2 = new FileCache2(getApplicationContext());

        File tempF = fc2.getFile(strUrl);

        String tempFileName = AppController.BASE_SRC + tempF.getName() + "gif";

        copyFile(tempF, tempFileName);


        if (new File(tempFileName) == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_message), Toast.LENGTH_SHORT);
            return;
        }

        Uri uri = Uri.fromFile(new File(tempFileName));

        sendFile.putExtra(Intent.EXTRA_STREAM, uri);
        sendFile.setType("image/gif");
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(sendFile, 0);
        sendFile.putExtra(Intent.EXTRA_STREAM, uri);
        sendFile.setType("image/gif");

        startActivity(sendFile);
    }

    private boolean copyFile(File file, String save_file) {
        boolean result;
        if (file != null && file.exists()) {
            FileInputStream fis = null;
            FileOutputStream newfos = null;
            try {
                fis = new FileInputStream(file);
                newfos = new FileOutputStream(save_file);
                int readcount = 0;
                byte[] buffer = new byte[1024];
                while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
                    newfos.write(buffer, 0, readcount);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    newfos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    private void shareIt() {
        Intent sendFile = new Intent(Intent.ACTION_SEND);

        FileCache2 fc2 = new FileCache2(getApplicationContext());

        File tempF = fc2.getFile(strUrl);

        String tempFileName = AppController.BASE_SRC + tempF.getName() + ".gif";

        copyFile(tempF, tempFileName);

        if (new File(tempFileName) == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_message), Toast.LENGTH_SHORT);
            return;
        }

        Uri uri = Uri.fromFile(new File(tempFileName));

        sendFile.putExtra(Intent.EXTRA_STREAM, uri);
        sendFile.setType("image/gif");
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(sendFile, 0);
        for (int i = 0; i > activityList.size(); i++) {
            Log.e("list" + i, activityList.get(i).activityInfo.name);
        }
        for (final ResolveInfo app : activityList) {
            if ((app.activityInfo.name).contains("kakao.talk")) {
                Log.e("name", app.activityInfo.name);
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                sendFile.setComponent(name);
                sendFile.putExtra(Intent.EXTRA_STREAM, uri);
                sendFile.setType("image/gif");

                startActivity(sendFile);
                break;
            }
        }
    }

    private void shareItFacebookMessenger() {
        Intent sendFile = new Intent(Intent.ACTION_SEND);

        FileCache2 fc2 = new FileCache2(getApplicationContext());

        File tempF = fc2.getFile(strUrl);

        String tempFileName = AppController.BASE_SRC + tempF.getName() + ".gif";

        copyFile(tempF, tempFileName);

        if (new File(tempFileName) == null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_message), Toast.LENGTH_SHORT);
            return;
        }

        Uri uri = Uri.fromFile(new File(tempFileName));

        ShareToMessengerParams shareToMessengerParams = ShareToMessengerParams.newBuilder(
                uri, "image/gif").build();

        MessengerUtils.shareToMessenger(this, REQUEST_CODE_SHARE_TO_MESSENGER, shareToMessengerParams);

        mProgressDialog.dismiss();
    }

    private void initViews() {
        laySave = (LinearLayout) findViewById(R.id.lay_share_delete_popup_repic);
        layFace = (LinearLayout) findViewById(R.id.lay_share_delete_popup_facebook);
        layFM = (LinearLayout) findViewById(R.id.lay_share_delete_popup_facebook_messenger);

        layKakao = (LinearLayout) findViewById(R.id.lay_share_delete_popup_kakao);
        layDownLoad = (LinearLayout) findViewById(R.id.lay_share_delete_popup_download);
        layMore = (LinearLayout) findViewById(R.id.lay_share_delete_popup_more);

        layFM.setOnClickListener(this);
        laySave.setOnClickListener(this);
        layKakao.setOnClickListener(this);
        layFace.setOnClickListener(this);
        layDownLoad.setOnClickListener(this);
        layMore.setOnClickListener(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void repicDelete() {

        StringTransMethod stmRepicDelete = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    //repic_add_aleady
                    if (jd.getInt("result") == 0) {
                        finish();
                        overridePendingTransition(0, 0);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.repic_del_complete), Toast.LENGTH_LONG).show();
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    } else {
                        finish();
                        overridePendingTransition(0, 0);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if (mProgressDialog != null) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    }
                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("myId", AppController.getSp().getString("email", ""));
        params.put("post_id", strPostId);

        AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 206, stmRepicDelete);
        AppController.apiDataTaskNew.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_share_delete_popup_repic:
                if (!isTrans) {
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Repic Save Click" + "_" + strPostId).build());
                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Repic Save Click").build());
                    repicDelete();
                }
                break;
            case R.id.lay_share_delete_popup_kakao:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click").build());
                shareIt();
                AppController.ActionLogInsert(strPostId, "K", this);
                break;
            case R.id.lay_share_delete_popup_facebook:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Share Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Share Click").build());
                if (strUrl.equals("")) {
                    strUrl = AppController.LOGO_LINK;
                }
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle("PicPic")
                        .setContentDescription(
                                strBody)
                        .setContentUrl(Uri.parse(AppController.SHARE_LINK + strPostId))
                        .setImageUrl(Uri.parse(strUrl))
                        .build();

                shareDialog.show(linkContent);

                AppController.ActionLogInsert(strPostId, "F", this);

                //TODO Facebook:F Kakao:K Download:D LinkCopy:I
                break;
            case R.id.lay_share_delete_popup_download:
                urlToDownload = Uri.parse(strUrl);
                List<String> pathSegments = urlToDownload.getPathSegments();
                request = new DownloadManager.Request(urlToDownload);
                request.setTitle("PicPic");
                request.setDescription(strBody);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pathSegments.get(pathSegments.size() - 1));
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
                latestId = downloadManager.enqueue(request);

                finish();
                overridePendingTransition(0, 0);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.download_start), Toast.LENGTH_LONG).show();

                AppController.ActionLogInsert(strPostId, "D", this);
                break;
            case R.id.lay_share_delete_popup_more:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Kakao Click").build());
                shareItAll();
                AppController.ActionLogInsert(strPostId, "M", this);
                break;
            case R.id.lay_share_delete_popup_facebook_messenger:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Messenger Click" + "_" + strPostId).build());
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Facebook Messenger Click").build());

                shareItFacebookMessenger();

                AppController.ActionLogInsert(strPostId, "FM", this);
                break;
        }
    }
}