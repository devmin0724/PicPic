/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import com.mikepenz.fastadapter.adapters.FooterAdapter;
import com.mikepenz.fastadapter.adapters.HeaderAdapter;
import com.mikepenz.fastadapter_extensions.items.ProgressItem;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.MyPageHeader;
import com.picpic.sikkle.beans.MyPageItem;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.ui.setteings.SettingsActivity;
import com.picpic.sikkle.ui.setteings.SettingsChangeProfileActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.GIFLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyPageV1Activity extends Activity {

    private static final int PROFILE_PICTURE_CHANGE_RETURN = 1004;

    ImageView imvNoti;
    LinearLayout layTimeline, layInterest, layCamera, layNoti, layMypage, layLoading;

    RecyclerView rv;
    StaggeredGridLayoutManager lm;

    public static Activity ac;

    GIFLoader gifLoader;

    private HeaderAdapter<MyPageHeader> mHeaderAdapter;
    private FastItemAdapter<MyPageItem> mFastAdapter;
    private FooterAdapter<ProgressItem> footerAdapter;

    private MyPageHeader myPageHeader;

    int sortNavi = 0, nowPage = 1;
    boolean isLoading = false;
    String strSearch = "";
    private Bundle sis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_page_v1);

        sis = savedInstanceState;

        ac = this;

        gifLoader = new GIFLoader(getApplicationContext());

        rv = (RecyclerView) findViewById(R.id.rv_mypage);

        lm = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);

//        lm.set

        mHeaderAdapter = new HeaderAdapter<>();
        mFastAdapter = new FastItemAdapter<>();
        footerAdapter = new FooterAdapter<>();

//        rv.setHasFixedSize(false);
//        lm.setAutoMeasureEnabled(true);
        rv.setLayoutManager(lm);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(mHeaderAdapter.wrap(footerAdapter.wrap(mFastAdapter)));

//        rv.setAdapter(mFastAdapter);

        imvNoti = (ImageView) findViewById(R.id.imv_mypage_noti);

        layTimeline = (LinearLayout) findViewById(R.id.lay_mypage_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_mypage_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_mypage_camera);
        layNoti = (LinearLayout) findViewById(R.id.lay_mypage_noti);
        layMypage = (LinearLayout) findViewById(R.id.lay_mypage_mypage);

        layLoading = (LinearLayout) findViewById(R.id.lay_mypage_loading);

        layLoading.setVisibility(View.VISIBLE);

        layTimeline.setOnClickListener(onClickListener);
        layInterest.setOnClickListener(onClickListener);
        layCamera.setOnClickListener(onClickListener);
        layNoti.setOnClickListener(onClickListener);
        layMypage.setOnClickListener(onClickListener);

        rv.addOnScrollListener(new EndlessRecyclerOnScrollListener(lm) {
            @Override
            public void onLoadMore(int currentPage) {
                Log.e("cp", currentPage + " / " + nowPage);
                footerAdapter.clear();
                footerAdapter.add(new ProgressItem().withEnabled(false));
                if (nowPage < currentPage) {
                    getGridSearch(false, currentPage, strSearch);
                }
            }
        });

        getData();
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
//        startActivity(new Intent(MyPageActivity.this, NewGridTimelineActivity.class));
        startActivity(new Intent(MyPageV1Activity.this, TimelineV1NewActivity.class));
    }

    private void getGridData(boolean is, int page) {
        if (is) {
            mFastAdapter.clear();
            page = 1;
        }

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
        params.put("str", "");
        if (sortNavi == 0) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }
        params.put("timeline", "F");

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();

    }

    private void getGridSearch(boolean is, int page, String str) {
        if (is) {
            mFastAdapter.clear();
            page = 1;
        }

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
        params.put("str", str);
        if (sortNavi == 0) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();
    }

    StringTransMethod stmInitListRestult2 = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                MinUtils.d("rr", result);
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    Log.e("length", jarr.length() + "");

//                gv.setVisibility(View.VISIBLE);

                    if (jarr.length() < 1) {
                        Log.e("length", jarr.length() + "");
//                        initList(2);
                    } else {
                        final GridResult gr = new GridResult();
                        GridItem gi;
                        JSONObject j;
                        for (int i = 0; i < jarr.length(); i++) {
                            gi = new GridItem();

                            j = jarr.getJSONObject(i);

                            gi.setUrl(j.getString("url"));
                            gi.setPost_id(j.getString("post_id"));

                            if (!j.isNull("height1")) {
                                gi.setHeight1(j.getInt("height1"));

                            }
                            if (!j.isNull("height2")) {

                                gi.setHeight2(j.getInt("height2"));
                            }
                            if (!j.isNull("heightTh")) {
                                gi.setHeightTH(j.getInt("heightTh"));

                            }
                            if (!j.isNull("width1")) {

                                gi.setWidth1(j.getInt("width1"));
                            }
                            if (!j.isNull("width2")) {

                                gi.setWidth2(j.getInt("width2"));
                            }
                            if (!j.isNull("widthTh")) {
                                gi.setWidthTH(j.getInt("widthTh"));

                            }
                            if (!j.isNull("size1")) {

                                gi.setSize1(j.getInt("size1"));
                            }
                            if (!j.isNull("size2")) {
                                gi.setSize2(j.getInt("size2"));

                            }
                            if (!j.isNull("sizeTh")) {
                                gi.setSizeTH(j.getInt("sizeTh"));

                            }
                            mFastAdapter.add(new MyPageItem().withActivity(MyPageV1Activity.this).withModel(gi));
                            gr.add(gi);

                        }
                        mFastAdapter.withSavedInstanceState(sis);
//                        rv.setAdapter(mFastAdapter);
                        mFastAdapter.notifyDataSetChanged();
//                        mFastAdapter.withOnClickListener(new FastAdapter.OnClickListener<MyPageItem>() {
//                            @Override
//                            public boolean onClick(View v, IAdapter<MyPageItem> adapter, MyPageItem item, int position) {
//                                Intent i = new Intent(ac, SinglePostContentActivity.class);
//                                i.putExtra("postId", gr.get(position).getPost_id());
//                                ac.startActivity(i);
//                                return false;
//                            }
//                        });
//                        initList(1);
                        footerAdapter.clear();
                        isLoading = false;
                    }
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            }
        }
    };

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            if (laySort1.isSelected()) {
//                getInitListSearchData(edtSearch.getText().toString(), true);
//            } else {
//                getInitListSearchData2(edtSearch.getText().toString(), true);
//            }
            if (!isLoading) {
                isLoading = true;
                strSearch = s.toString();
                getGridSearch(true, 1, strSearch);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void getData() {
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 406, stmMypageResult);
        AppController.apiTaskNew.execute();

    }

    StringTransMethod stmMypageResult = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                final JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    mHeaderAdapter.clear();
                    myPageHeader = new MyPageHeader().withAcivity(MyPageV1Activity.this).withJD(jd).withListener(onClickListener).withTW(tw);
                    mHeaderAdapter.add(myPageHeader);
                    myPageHeader.setSort(sortNavi);
                    mHeaderAdapter.notifyDataSetChanged();

                    layLoading.setVisibility(View.GONE);

                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

//                getInitListData();
            } catch (JSONException e) {

            } finally {
                getGridData(true, 1);
            }
        }
    };

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(MyPageV1Activity.this, UserListActivity.class);
            switch (v.getId()) {
                case R.id.lay_mypage_timeline:
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(MyPageV1Activity.this, TimelineV1NewActivity.class));
                    finish();
                    break;
                case R.id.lay_mypage_interest:
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(MyPageV1Activity.this, InterestFeedV1Activity.class));
                    finish();
                    break;
                case R.id.lay_mypage_camera:
                    startActivity(new Intent(MyPageV1Activity.this, CameraPopUp.class));
                    break;
                case R.id.lay_mypage_noti:
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(MyPageV1Activity.this, NotificationActivity.class));
                    finish();
                    break;
                case R.id.lay_mypage_mypage:
                    break;

                case R.id.imv_mypage_user_edit:
                    //TODO 사용자 정보 변경페이지로 이동
                    startActivityForResult(new Intent(MyPageV1Activity.this, SettingsChangeProfileActivity.class), PROFILE_PICTURE_CHANGE_RETURN);
                    break;
                case R.id.tv_mypage_user_follower_cnt:
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWER);
                    i.putExtra("id", AppController.getSp().getString("email", ""));
                    startActivity(i);
                    break;
                case R.id.tv_mypage_user_following_cnt:
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWING);
                    i.putExtra("id", AppController.getSp().getString("email", ""));
                    startActivity(i);
                    break;
                case R.id.imv_mypage_share:
                    break;
                case R.id.imv_mypage_settings:
                    //TODO 설정페이지 이동
                    startActivity(new Intent(MyPageV1Activity.this, SettingsActivity.class));
                    break;
                case R.id.tv_mypage_count:
                case R.id.tv_mypage_user_popular:
                    //TODO 인기태그 페이지 이동
                    break;
                case R.id.lay_mypage_top1:
                    if (sortNavi == 1) {
                        myPageHeader.setSort(0);
                        sortNavi = 0;
                        mHeaderAdapter.notifyDataSetChanged();
                        getData();
//                    getInitListData();
                    }
                    break;
                case R.id.lay_mypage_top2:
                    if (sortNavi == 0) {
                        myPageHeader.setSort(1);
                        sortNavi = 1;
                        mHeaderAdapter.notifyDataSetChanged();
                        getData();
//                    getInitListData2();
                    }
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();

        android.util.Log.d("TAG", "TOTAL MEMORY : " + (Runtime.getRuntime().totalMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "MAX MEMORY : " + (Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "FREE MEMORY : " + (Runtime.getRuntime().freeMemory() / (1024 * 1024)) + "MB");

        android.util.Log.d("TAG", "ALLOCATION MEMORY : " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + "MB");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        notiCheck();
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PROFILE_PICTURE_CHANGE_RETURN:
                getData();
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState = mFastAdapter.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

}
