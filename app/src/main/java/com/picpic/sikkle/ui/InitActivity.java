package com.picpic.sikkle.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.picpic.sikkle.R;
import com.picpic.sikkle.Test3Activity;
import com.picpic.sikkle.TestActivity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.login_join.TutorialActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import pl.droidsonroids.gif.GifDrawable;

public class InitActivity extends Activity {
    private int navi = 0;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String TAG = "PicPic Initialize";
    private static final String PROPERTY_APP_VERSION = "1.0.0";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    String regid = "";
    ImageView imvBG;
    Context context;
    Handler endHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            finish();
        }
    };
    StringTransMethod stmAutoLoginiResult = new StringTransMethod() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void endTrans(final String result) {
//            Log.e("result", result);
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
//                    MinUtils.d("auto Login", result);
                    JSONObject loginData = new JSONObject(jd.getString("data"));
                    if (!loginData.getString("cut_yn").equals("N")) {

                        String tempMsg = getResources().getString(R.string.warning_cut_1) + loginData.getString("cut_reason") + getResources().getString(R.string.warning_cut_2);
                        Toast.makeText(getApplicationContext(), tempMsg, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), loginData.getString("cut_reason"), Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }

                    AppController.getSe().putString("bir_year", loginData.getString("bir_year"));
                    AppController.getSe().putString("bir_mon", loginData.getString("bir_mon"));
                    AppController.getSe().putString("bir_day", loginData.getString("bir_day"));
                    AppController.getSe().putString("country", loginData.getString("country"));
                    AppController.getSe().putString("cut_day", loginData.getString("cut_day"));
                    AppController.getSe().putString("cut_reason", loginData.getString("cut_reason"));
                    AppController.getSe().putString("declare_cnt", loginData.getString("declare_cnt"));
                    AppController.getSe().putString("device_id", loginData.getString("device_id"));
                    AppController.getSe().putString("email", loginData.getString("email"));
                    AppController.getSe().putString("facebook_id", loginData.getString("facebook_id"));
                    AppController.getSe().putString("fri_open_yn", loginData.getString("fri_open_yn"));
                    AppController.getSe().putString("id", loginData.getString("id"));
                    AppController.getSe().putString("m_id", loginData.getString("m_id"));
                    AppController.getSe().putString("pinterest_id", loginData.getString("pinterest_id"));
                    AppController.getSe().putString("posts_open_form", loginData.getString("posts_open_form"));
                    AppController.getSe().putString("profile_picture", loginData.getString("profile_picture"));
                    AppController.getSe().putString("push_token", loginData.getString("push_token"));
                    AppController.getSe().putString("regist_day", loginData.getString("regist_day"));
                    AppController.getSe().putString("register_form", loginData.getString("register_form"));
                    AppController.getSe().putString("sex", loginData.getString("sex"));
                    AppController.getSe().putString("tumbler_id", loginData.getString("tumbler_id"));
                    AppController.getSe().putString("twitter_id", loginData.getString("twitter_id"));
                    AppController.getSe().putBoolean("isAutoLogin", true);
                    AppController.getSe().commit();

                    AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                    AppController.t.send(new HitBuilders.EventBuilder().setCategory("AutoLogin").build());

//                    startActivity(new Intent(InitActivity.this, TimeLineActivity.class));
//                    startActivity(new Intent(InitActivity.this, NewGridTimelineActivity.class));
//                    Intent i = new Intent(InitActivity.this, NewGridTimelineActivity.class);
//                    Intent i = new Intent(InitActivity.this, TimeLineActivity.class);
                    if(navi == 0){
                        Intent i = new Intent(InitActivity.this, TimelineV1NewActivity.class);
//                    Intent i = new Intent(InitActivity.this, TimeLineV1Activity.class);
//                    Intent i = new Intent(InitActivity.this, Test3Activity.class);
//                    Intent i = new Intent(InitActivity.this, TestActivity.class);
                        i.putExtra("navi", 0);
//                    startActivity(i, ActivityOptions.makeSceneTransitionAnimation(InitActivity.this).toBundle());
                        startActivity(i);
//                    startActivity(new Intent(InitActivity.this, TestActivity.class));
//                    startActivity(new Intent(InitActivity.this, Test2Activity.class));
                        finish();
                        overridePendingTransition(0, 0);
                    }else if(navi == 1){
                        Intent i = new Intent(InitActivity.this, GIFCameraFilmActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.putExtra("navi", 0);
                        startActivity(i);
                        finish();
                    }

                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                } else {
                    AppController.getSe().putBoolean("isAutoLogin", false);
                    AppController.getSe().commit();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "로그인에 실패했습니다. 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (JSONException e) {

            }

        }
    };
    StringTransMethod stmVersionCheck = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            try {
                Log.e("test", result);
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
//                    Toast.makeText(getApplicationContext(), "통신상태가 불안정합니다. 다시 체크해주세요.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    finish();
                    return;
                } else {
                    String version = jd.getString("new_ver");
                    String nowVersion = getVersionName();

                    AppController.getSe().putString("new_ver", version);
                    AppController.getSe().commit();

                    String[] versions = version.split("/");

                    for (int i = 0; i < versions.length; i++) {
                        Log.e("verstion" + i, versions[i]);
                    }

                    if (versions.length == 1) {
                        if (versions[0].equals(nowVersion)) {
                            initialize();
                        } else {
                            Intent i = new Intent();
                            i.setAction(Intent.ACTION_VIEW);
                            //TODO 패키지명 변경
                            i.setData(Uri
                                    .parse(getResources().getString(R.string.market_link)));
                            startActivity(i);
                            finish();
                        }
                    } else if (versions.length == 2) {
                        if (versions[0].equals(nowVersion) || versions[1].equals(nowVersion)) {
                            initialize();
                        } else {
                            Intent i = new Intent();
                            i.setAction(Intent.ACTION_VIEW);
                            //TODO 패키지명 변경
                            i.setData(Uri
                                    .parse(getResources().getString(R.string.market_link)));
                            startActivity(i);
                            finish();
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_stop), Toast.LENGTH_LONG).show();
                            }
                        });

                        endHandler.sendEmptyMessageDelayed(0, 1000);
                    }
                }

            } catch (JSONException e) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_stop), Toast.LENGTH_LONG).show();
//                    }
//                });

                endHandler.sendEmptyMessageDelayed(0, 1000);
                e.printStackTrace();
            } catch (NullPointerException e) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_stop), Toast.LENGTH_LONG).show();
//                    }
//                });
                //TODO 서버연결 실패 msg

                endHandler.sendEmptyMessageDelayed(0, 1000);
            }

        }
    };

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private static String getDeviceSerialNumBer() {
        try {
            AppController.getSe().putString("DID", (String) Build.class.getField("SERIAL")
                    .get(null));
            AppController.getSe().commit();
            return (String) Build.class.getField("SERIAL").get(null);
        } catch (Exception e) {
            return null;

        }
    }

    private void getAppKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash key", something);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("name not found", e.toString());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_init);

        getAppKeyHash();
//TODO 잠금화면
//        Intent intent = new Intent(this, NewLockService.class);
//        startService(intent);
//        startActivity(new Intent(InitActivity.this, Test3Activity.class));
//        finish();

        try{
            navi = getIntent().getExtras().getInt("navi");
        }catch (NullPointerException e){
            navi = 0;
        }

        context = getApplication();

        mkdirPicPic();

//        AppController.sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        AppController.se = sp.edit();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        MinUtils.screenWidth = metrics.widthPixels;
        MinUtils.screenHeight = metrics.heightPixels;
        MinUtils.density = metrics.density;

//        if (AppController.getSp().getInt("tutime", -1) == -1) {
//            AppController.getSe().putInt("tutime", 0);
//            AppController.getSe().putInt("tuinter", 0);
//            AppController.getSe().putInt("turank", 0);
//            AppController.getSe().putInt("tucame", 0);
//            AppController.getSe().putInt("tucame2", 3);
//            AppController.getSe().commit();
//        }145290

        FacebookSdk.sdkInitialize(getApplicationContext());

        Locale systemLocale = getResources().getConfiguration().locale;
        String tempCountry = systemLocale.toString();
        AppController.getSe().putString("country", tempCountry + "");
        AppController.getSe().commit();

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(getApplicationContext());

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

        imvBG = (ImageView) findViewById(R.id.imv_init);

        try {
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("login_bg_1.gif"));
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("loading_bg_2.gif"));
//            GifDrawable gd = new GifDrawable(getResources().getAssets().open("loading_bg_3.gif"));
            GifDrawable gd = new GifDrawable(getResources().getAssets().open("loading_bg_4.gif"));
            imvBG.setImageDrawable(gd);
        } catch (IOException e) {
            e.printStackTrace();
        }

        gcm();

//        StringTransMethod test = new StringTransMethod(){
//            @Override
//            public void endTrans(String result) {
////                MinUtils.d("test", result);
//                Log.e("test", result);
//            }
//        };
//
//        Map<String, String> params = new HashMap<>();
//
//        params.put("my_id", AppController.getSp().getString("email", ""));
////        params.put("page", "1");
//
//        AppController.apiDataTaskNew = new APIDataTaskNew(InitActivity.this, params, 524, test);
//        AppController.apiDataTaskNew.execute();


//        String testStr1 = "PIC000001003";
//        String testStr2 = System.currentTimeMillis() + "";
////
//        String hex1 = MinUtils.fromMId(testStr1);
//        String hex2 = MinUtils.fromCurrnet(testStr2);
////
//        Log.e("hex12", hex1 + "/" + hex2);
////        Log.e("hex2", Base64.encodeToString(testStr1.getBytes(),0));
//
////        String hash = new String(new BigInteger(testStr, 13).toByteArray());
////
////        Log.e("hash2", new String(Hex.encodeHexString(DigestUtils.md5(testStr))));
////
////        StringBuffer sb = new StringBuffer();
////        String hex = null;
////
//        String hex = Base64.encodeToString(testStr1.getBytes(), 0, 12, Base64.NO_CLOSE);
//        Log.e("hex3", hex);
//
//
//        sb.append(hex);
//        Log.e("hash1", sb.toString());
//        try {
//            Log.e("hash", MinUtils.makeSHA1Hash(testStr));
//            //00beb93578d6a873ee16b403cd81ea002ad76a52
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("로딩화면");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        AppEventsLogger.deactivateApp(this);

    }

    private void mkdirPicPic() {
        File photo = new File(AppController.BASE_SRC);
        File tmp = new File(AppController.TEMP_SRC);
        File tmp2 = new File(AppController.TEMP_SRC2);
        if (photo.exists()) {

        } else {
            photo.mkdirs();
        }
        if (tmp.exists()) {

        } else {
            tmp.mkdirs();
        }

        if (tmp2.exists()) {

        } else {
            tmp2.mkdirs();
        }

        photo = new File(AppController.BASE_SRC+"part/");
        if (photo.exists()) {

        } else {
            photo.mkdirs();
        }
    }

    private void initialize() {

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (AppController.getSp().getBoolean("isAutoLogin", false)) {

                    //TODO 자동로그인
                    Date now = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    String tempC = sdf.format(now);

                    Map<String, String> params = new HashMap<>();

                    if (AppController.getSp().getString("register_form", "").equals("10002")) {
                        params.put("email", AppController.getSp().getString("password", ""));
                    } else {
                        params.put("email", AppController.getSp().getString("email", ""));
                    }
                    params.put("password", AppController.getSp().getString("password", ""));
                    params.put("register_form", AppController.getSp().getString("register_form", ""));
                    params.put("country", AppController.getSp().getString("country", ""));
                    params.put("device_id", MinUtils.DID);
                    params.put("push_token", MinUtils.PTK);
                    params.put("regist_day", tempC);

                    AppController.apiDataTaskNew = new APIDataTaskNew(InitActivity.this, params, 202, stmAutoLoginiResult);
                    AppController.apiDataTaskNew.execute();

                } else {
                    //TODO 로그인페이지 이동후 카메라
                    startActivity(new Intent(InitActivity.this, TutorialActivity.class));
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        };

        timer.schedule(timerTask, 10);

    }

    public String getVersionName() {

        try {
            PackageInfo packageInfo = getApplicationContext()
                    .getPackageManager().getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
//            Log.i(TAG, "NameNotFoundException => " + e.toString());
            return "";
        }

    }

    private void gcm() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    Bundle data = new Bundle();
                    data.putString("my_message", "Hello World");
                    data.putString("my_action",
                            "com.google.android.gcm.demo.app.ECHO_NOW");
                    String id = Integer.toString(msgId.incrementAndGet());
                    gcm.send(AppController.SENDER_ID + "@gcm.googleapis.com", id, data);
                    msg = "Sent message";
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {

                final SharedPreferences prefs = getGcmPreferences(getApplicationContext());
                MinUtils.PTK = prefs.getString(PROPERTY_REG_ID, "");
                MinUtils.DID = getDeviceSerialNumBer()
                        + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

                AppController.getSe().putString("DID", MinUtils.DID);
                AppController.getSe().commit();

                Map<String, String> params = new HashMap<>();

                params.put("my_id", "");

                AppController.apiTaskNew = new APITaskNew(InitActivity.this, params, 212, stmVersionCheck);
                AppController.apiTaskNew.execute();

            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
//        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGcmPreferences(Context context) {
        return getSharedPreferences(InitActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private void sendRegistrationIdToBackend() {
        // Your implementation here.
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }

                    regid = gcm.register(AppController.SENDER_ID);

                    msg = "Device registered, registration ID=" + regid;

                    sendRegistrationIdToBackend();

                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

}
