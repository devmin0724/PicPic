/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fingram.agifEncoder.QAGIFEncoder;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.gifcamera.GIFCameraEdit1Activity;
import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifDrawable;

public class CameraPopUp extends Activity implements View.OnClickListener {

    private static final int PICK_FROM_ALBUM = 1111;
    public static final int POST_WRITE_RETURN = 1003;
    LinearLayout layCamera, layAlbum, layBlank;
    Uri mImageCaptureUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_camera_pop_up);

        layCamera = (LinearLayout) findViewById(R.id.lay_camera_popup_film);
        layAlbum = (LinearLayout) findViewById(R.id.lay_camera_popup_load);

        layBlank = (LinearLayout) findViewById(R.id.lay_camera_popup_blank);
        layBlank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        layCamera.setOnClickListener(this);
        layAlbum.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_camera_popup_film:
                Intent gifCamera = new Intent(CameraPopUp.this, GIFCameraFilmActivity.class);
                gifCamera.putExtra("navi", 0);
//                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(gifCamera, POST_WRITE_RETURN);
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_camera_popup_load:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Camera Load To Album").build());
                doTakePictureAlbumAction();
                break;
        }
    }

    private void doTakePictureAlbumAction() {

        AppController.initCamera(true);

        String tempFileName = String.valueOf(System.currentTimeMillis());
        AppController.cameraOutput1 = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/picpic/"
                + AppController.getSp().getString("email", "") + "_" + tempFileName + "_1.gif"));
        AppController.cameraOutput2 = Uri.fromFile(new File(AppController.BASE_SRC
                + MinUtils.fromMId(AppController.getSp().getString("m_id", "")) + "_"
                + MinUtils.fromCurrnet(tempFileName) + "_2.gif"));
        Log.e("output", AppController.cameraOutput2.getPath() + "");
        AppController.cameraF = new File(Environment.getExternalStorageDirectory() + "/PicPic/" + tempFileName + ".jpg");
        AppController.cameraAtomFF = new File(Environment.getExternalStorageDirectory() + "/PicPic/atom_" + tempFileName + ".jpg");

        File f = new File(AppController.BASE_SRC
                + MinUtils.fromMId(AppController.getSp().getString("m_id", "")) + "_"
                + MinUtils.fromCurrnet(tempFileName) + "_2.gif");

        AppController.cameraPath2 = getPath2();

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        String type = "image/gif";
        intent.setDataAndType(data, type);

        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_FROM_ALBUM:
                    try {
                        mImageCaptureUri = data.getData();

                        Log.e("uri", mImageCaptureUri.toString());

                        if (isGIF(mImageCaptureUri)) {
                            if (AppController.originFrames.size() < 1) {
                                long nowCurrent = System.currentTimeMillis();

                                final File f2 = new File(AppController.cacheDir + "/" + nowCurrent + "_2.gif");

                                if (f2.length() <= 5 * 1024 * 1024) {
                                    GifDrawable gd = new GifDrawable(getContentResolver(), mImageCaptureUri);

                                    QAGIFEncoder encoder2 = new QAGIFEncoder();
                                    encoder2.setRepeat(0);
                                    encoder2.setDelay(1000 / AppController.speedNavi);
                                    encoder2.start(f2.getAbsolutePath());

                                    ArrayList<Frame> tempFrames = new ArrayList<>();

                                    for (int i = 0; i < gd.getNumberOfFrames(); i++) {
                                        final int finalI = i;
                                        Bitmap b = gd.seekToFrameAndGet(finalI);
                                        final File tempFile = new File(AppController.cacheDir + "/" + nowCurrent + "_" + i);
                                        b.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(tempFile));
                                        final Bitmap finalB = b;
                                        tempFrames.add(new Frame() {
                                            @Override
                                            public File getFile() {
                                                return tempFile;
                                            }

                                            @Override
                                            public Bitmap getbitmap() {
                                                return finalB;
                                            }
                                        });
                                        encoder2.addFrame(b);
                                    }

                                    encoder2.finish();

                                    AppController.originFrames.add(tempFrames);

                                    //TODO 편집 ㄱㄱ

                                    AppController.initCamera(false);

                                    AppController.sectionArr = new ArrayList<int[]>();

                                    AppController.editFrames = AppController.originFrames;

                                    Intent i = new Intent(CameraPopUp.this, GIFCameraEdit1Activity.class);
                                    AppController.is11 = false;
                                    AppController.isPart = false;
                                    AppController.isLoadGIF = true;
                                    startActivityForResult(i, POST_WRITE_RETURN);
                                    finish();

                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif_size), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.cant_load_image), Toast.LENGTH_LONG).show();
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e("e", e.toString());
                    }finally {
                        finish();
                        overridePendingTransition(0, 0);
                    }
                    break;
                case POST_WRITE_RETURN:
                    setResult(RESULT_OK);
                    finish();
                    overridePendingTransition(0, 0);
                    break;
            }
        }
    }

    private boolean isGIF(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = cR.getType(uri);
        Log.e("mime_extention2", type);
        return type.equals("image/gif");
    }

    private String getPath2() {

//        return output.getPath();
        if ("file".equals(AppController.cameraOutput2.getScheme())) {
            return AppController.cameraOutput2.getPath();
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(AppController.cameraOutput2, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(idx);
        } finally {
            cursor.close();
        }
    }
}
