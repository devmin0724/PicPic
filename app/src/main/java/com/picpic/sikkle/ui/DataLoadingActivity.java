package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.SocketService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import pl.droidsonroids.gif.GifDrawable;

public class DataLoadingActivity extends Activity {

    ImageView imv;

    SocketService mService;
    boolean mBound = false;
    HashMap<String, String> params;
    int serviceCode = 0;
    int visibleNavi = 0;

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (mBound) {
                JSONObject json = new JSONObject();

                Iterator<String> iter = params.keySet().iterator();
                try {
                    while (iter.hasNext()) {
                        String key = iter.next();
                        String value = params.get(key);
//                        Log.e(key, value);

                        json.put(key, value);
//                Log.d("fureun", "key : " + key + ", value : " + value);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("json", json.toString());


                String num = mService.getData(json, serviceCode);
                Log.e("num", num + "");
                Intent i = new Intent();
                i.putExtra("return", num);
                setResult(RESULT_OK, i);
                finish();

            }
        }
    };
    GifDrawable gd;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SocketService.LocalBinder binder = (SocketService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND,
                WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        setContentView(R.layout.activity_data_loading);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("데이터 로딩 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

//        imv = (ImageView) findViewById(R.id.imv_loading);
//        imv.startAnimation(rotate);

        imv = (ImageView) findViewById(R.id.imv_loading_1);

        imv.setBackgroundResource(R.drawable.loading_animation);
        AnimationDrawable frameAnimation = (AnimationDrawable) imv.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();
//        Animation ani = AnimationUtils.loadAnimation(this, R.anim.loading_animation);
//        imv.startAnimation(ani);

        params = (HashMap<String, String>) getIntent().getSerializableExtra("data");

        serviceCode = getIntent().getExtras().getInt("code");

        handler.sendEmptyMessageDelayed(0, 500);

    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
        // Bind to LocalService
        Intent intent = new Intent(this, SocketService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void onBackPressed() {
    }

}
