package com.picpic.sikkle.ui.login_join;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

import net.simonvt.numberpicker.NumberPicker;

import java.util.Calendar;

public class BirthDialogActivity extends Activity {

    NumberPicker year, month, day;

    int dayMax[] = {
            31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    Calendar _c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_birth_dialog);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("생일선택 팝업 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        year = (NumberPicker) findViewById(R.id.np_birth_dialog_year);
        month = (NumberPicker) findViewById(R.id.np_birth_dialog_month);
        day = (NumberPicker) findViewById(R.id.np_birth_dialog_day);


        _c = Calendar.getInstance();
        year.setMaxValue(_c.get(Calendar.YEAR));
        year.setMinValue(1900);
        year.setValue(1998);
        year.setFocusable(true);
        year.setFocusableInTouchMode(true);

        month.setMaxValue(12);
        month.setMinValue(1);
        month.setFocusable(true);
        month.setFocusableInTouchMode(true);

        day.setMaxValue(31);
        day.setMinValue(1);
        day.setFocusable(true);
        day.setFocusableInTouchMode(true);

        year.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal >= _c.get(Calendar.YEAR)) {
                    month.setMaxValue(_c.get(Calendar.MONTH) + 1);
                } else {
                    month.setMaxValue(12);
                    day.setMaxValue(31);
                }
            }
        });

        month.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal >= _c.get(Calendar.MONTH) + 1 && year.getValue() >= _c.get(Calendar.YEAR)) {
                    day.setMaxValue(_c.get(Calendar.DAY_OF_MONTH));
                } else {
                    day.setMaxValue(dayMax[newVal - 1]);
                }
            }
        });

    }

    public void minOnClick(View v) {
        switch (v.getId()) {
            case R.id.btn_birth_dialog_ok:
                Intent i = new Intent();
                i.putExtra("year", year.getValue());
                i.putExtra("month", month.getValue());
                i.putExtra("day", day.getValue());
                setResult(RESULT_OK, i);
                finish();
                overridePendingTransition(0, 0);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }
}
