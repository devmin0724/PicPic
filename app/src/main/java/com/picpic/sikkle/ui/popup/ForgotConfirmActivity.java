package com.picpic.sikkle.ui.popup;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class ForgotConfirmActivity extends Activity {

    LinearLayout lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot_confirm);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("잊어버렸어 확인 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        lay = (LinearLayout) findViewById(R.id.lay_forgot_confirm);
        lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

}
