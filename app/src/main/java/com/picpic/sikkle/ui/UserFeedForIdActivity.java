package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.EmptyAdapter;
import com.picpic.sikkle.adapter.SampleAdapter;
import com.picpic.sikkle.adapter.TimeLineAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter2;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedUserImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserFeedForIdActivity extends Activity implements View.OnClickListener {

    private static final int USER_INFO_RETURN = 1111;
    private static final int USER_INFO_LIST_RETURN = 1112;
    private static final int USER_INFO_LIST_RETURN_2 = 1113;
    String tempEmail = "", tempId = "";
    //    ImageView imvCheck, imvList, imvGrid;
    ImageView imvCheck;
    FixedUserImageView imvBg;
    CircleImageView cimv;
    TextView tvId, tvHotTag, tvPostCount, tvFollowerCount, tvFollowingCount, tvUserFeedCount;
    EditText edtSearch;
    LinearLayout layUser, layTag, imvBack, imvShare;
    StaggeredGridView gv;
    boolean isGrid = true;
    int gifWidth = 200, gifHeight = 200, nowPage = 1;
    TimelineResult m_ResultList = null;
    TimeLineAdapter m_ListAdapter = null;
    TimelineResult m_ResultList2 = null;
    TimeLineGridAdapter m_ListAdapter2 = null;
    private boolean lastItemVisibleFlag = false;
    GridResult m_ResultList3 = null;
    SampleAdapter m_ListAdapter3 = null;
    int lastListCount = 0;
    boolean isLoading = false, isPublic = true, isFollow = false;

    public static AnimationAdapter mAnimAdapter;
    View header;
    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            getInitListSearchData(edtSearch.getText().toString(), true);
            getGridSearch(true, 1, edtSearch.getText().toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_user_feed);

        tempId = getIntent().getExtras().getString("id");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("유저 피드 페이지_" + tempId);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("유저 피드 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();

    }

    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }

    private void initViews() {

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        header = inflater2.inflate(R.layout.header_user_feed, null, false);

        imvBack = (LinearLayout) header.findViewById(R.id.imv_user_feed_back);
        imvBack.setOnClickListener(this);
        imvShare = (LinearLayout) header.findViewById(R.id.imv_user_feed_share);
        imvCheck = (ImageView) header.findViewById(R.id.imv_user_feed_is_follow);
        imvCheck.setOnClickListener(this);
//        imvList = (ImageView) header.findViewById(R.id.imv_user_feed_sort_1);
//        imvList.setOnClickListener(this);
//        imvGrid = (ImageView) header.findViewById(R.id.imv_user_feed_sort_2);
//        imvGrid.setOnClickListener(this);
        imvBg = (FixedUserImageView) header.findViewById(R.id.imv_user_feed_bg);
//        imvListNon = (ImageView) findViewById(R.id.imv_non_user_feed_sort_1);
//        imvListNon.setOnClickListener(this);
//        imvGridNon = (ImageView) findViewById(R.id.imv_non_user_feed_sort_2);
//        imvGridNon.setOnClickListener(this);

        cimv = (CircleImageView) header.findViewById(R.id.cimv_user_feed_user_1);

        edtSearch = (EditText) header.findViewById(R.id.edt_user_feed_input);

        edtSearch.addTextChangedListener(tw);

        edtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edtSearch.setCursorVisible(true);
                } else {
                    edtSearch.setCursorVisible(false);
                }
            }
        });

        layUser = (LinearLayout) header.findViewById(R.id.lay_user_feed_top1);
        layUser.setSelected(true);
        layTag = (LinearLayout) header.findViewById(R.id.lay_user_feed_top2);

        layUser.setOnClickListener(this);
        layTag.setOnClickListener(this);

        tvId = (TextView) header.findViewById(R.id.tv_user_feed_user_id);
        tvHotTag = (TextView) header.findViewById(R.id.tv_user_feed_user_popular);
        tvPostCount = (TextView) header.findViewById(R.id.tv_user_feed_user_post_cnt);
        tvFollowerCount = (TextView) header.findViewById(R.id.tv_user_feed_user_follower_cnt);
        tvFollowingCount = (TextView) header.findViewById(R.id.tv_user_feed_user_following_cnt);
        tvUserFeedCount = (TextView) header.findViewById(R.id.tv_user_feed_count);

        tvPostCount.setOnClickListener(this);
        tvFollowerCount.setOnClickListener(this);
        tvFollowingCount.setOnClickListener(this);

        gv = (StaggeredGridView) findViewById(R.id.gv_user_feed);
        gv.addHeaderView(header);
//        gv.setEmptyView(header);
//        gv.setEmptyView(findViewById(R.id.lay_user_feed_empty));

        getData();

        isGrid = true;
//        imvGrid.setSelected(true);
//        imvGridNon.setSelected(true);
        layUser.setSelected(true);
    }

    private void getData() {
        StringTransMethod stmDataResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {

                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        if (jd.getString("email").equals("")) {
                            finish();
                            overridePendingTransition(0, 0);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_not_exists_user), Toast.LENGTH_LONG).show();
                        } else {

                            if (jd.getString("profile_picture").equals("")) {
                                cimv.setImageResource(R.drawable.icon_timeline_noprofile);
                                imvBg.setImageResource(R.drawable.icon_timeline_noprofile);
                            } else {
                                cimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));
                                imvBg.setImageURLString(AppController.URL + jd.getString("profile_picture"));
                            }

                            tempEmail = jd.getString("email");

                            if (jd.getString("follow_yn").equals("Y")) {
                                imvCheck.setSelected(true);
                                isFollow = true;
                            } else {
                                imvCheck.setSelected(false);
                                isFollow = false;
                            }

                            JSONArray p_tag_1 = new JSONArray(jd.getString("p_tag_1"));

                            int p_tag_length = p_tag_1.length();

                            if (!p_tag_1.getJSONObject(0).getString("tag_id").equals("null")) {
                                tvHotTag.setText("#" + p_tag_1.getJSONObject(0).getString("tag_name"));
                                tvHotTag.setTag(p_tag_1.getJSONObject(0).getString("tag_id"));
                            } else {
                                p_tag_length = 0;
                            }

                            tvUserFeedCount.setText("+" + p_tag_length);

                            tvId.setText("@" + jd.getString("id"));

                            tvFollowerCount.setText(jd.getInt("follower_cnt") + "");
                            tvFollowingCount.setText(jd.getInt("follow_cnt") + "");

                            tvPostCount.setText(jd.getInt("post_cnt") + "");

                            if (jd.getString("public_yn").equals("N")) {
                                if (isFollow) {
                                    isPublic = true;
                                } else {
                                    isPublic = false;
                                }
                            } else {
                                isPublic = true;
                            }

//                        getListData();
                            Map<String, String> paramAction = new HashMap<>();

                            paramAction.put("my_id", AppController.getSp().getString("email", ""));
                            paramAction.put("target_id", jd.getString("email"));
                            paramAction.put("type", "M");

                            StringTransMethod stm = new StringTransMethod() {

                            };

                            AppController.apiDataTaskNew = new APIDataTaskNew(UserFeedForIdActivity.this, paramAction, 603, stm);
                            AppController.apiDataTaskNew.execute();
                        }
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } catch (JSONException e) {

                } finally {
//                    if(isPublic){
                    getGridData(true, 1);
//                    }
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", tempId);

        AppController.apiTaskNew = new APITaskNew(this, params, 518, stmDataResult);
        AppController.apiTaskNew.execute();

    }


    private void getGridData(boolean is, int page) {
//        initList(1);
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }
//        initList(1);
//        gv.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();

//        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("str", "");
//
//        AppController.apiTaskNew = new APITaskNew(params, 511, stmInitListRestult);
//        AppController.apiTaskNew.execute();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", tempEmail);
        params.put("page", "" + page);
        params.put("range", "N");
//        params.put("str", AppController.getSp().getString("email", ""));
        params.put("str", "");
        if (layUser.isSelected()) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }
        params.put("timeline", "F");

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();

    }

    private void getGridSearch(boolean is, int page, String str) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

//        gv.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();

//        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("str", "");
//
//        AppController.apiTaskNew = new APITaskNew(params, 511, stmInitListRestult);
//        AppController.apiTaskNew.execute();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("user_id", tempEmail);
        params.put("page", "" + page);
        params.put("range", "N");
//        params.put("str", AppController.getSp().getString("email", ""));
        params.put("str", str);
        if (layUser.isSelected()) {
            params.put("type", "U");
        } else {
            params.put("type", "R");
        }

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();
    }

    StringTransMethod stmInitListRestult2 = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
//            initList(1);
            try {
                JSONObject jd = new JSONObject(result);

                if (jd.getInt("result") == 0) {
                    if(jd.isNull("data")){
                        if(m_ListAdapter3 == null){
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    }
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    Log.e("length", jarr.length() + "");

//                gv.setVisibility(View.VISIBLE);

                    if (!isPublic) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_is_closed_y), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (jarr.length() < 1) {
                        if(m_ListAdapter3.getCount() < 2){
                            Log.e("length", jarr.length() + "");
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    } else {
                        GridResult gr = new GridResult();
                        GridItem gi;
                        JSONObject j;
                        for (int i = 0; i < jarr.length(); i++) {
                            gi = new GridItem();

                            j = jarr.getJSONObject(i);

                            gi.setUrl(j.getString("url"));
                            gi.setPost_id(j.getString("post_id"));

                            if (!j.isNull("height1")) {
                                gi.setHeight1(j.getInt("height1"));

                            }
                            if (!j.isNull("height2")) {

                                gi.setHeight2(j.getInt("height2"));
                            }
                            if (!j.isNull("heightTh")) {
                                gi.setHeightTH(j.getInt("heightTh"));

                            }
                            if (!j.isNull("width1")) {

                                gi.setWidth1(j.getInt("width1"));
                            }
                            if (!j.isNull("width2")) {

                                gi.setWidth2(j.getInt("width2"));
                            }
                            if (!j.isNull("widthTh")) {
                                gi.setWidthTH(j.getInt("widthTh"));

                            }
                            if (!j.isNull("size1")) {

                                gi.setSize1(j.getInt("size1"));
                            }
                            if (!j.isNull("size2")) {
                                gi.setSize2(j.getInt("size2"));

                            }
                            if (!j.isNull("sizeTh")) {
                                gi.setSizeTH(j.getInt("sizeTh"));

                            }


                            gr.add(gi);

                        }
//                        initList(1);
                        updateResultList3(gr);
                    }
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            }finally {
                isLoading = false;
            }
        }
    };

    protected boolean updateResultList3(GridResult resultList) {

        Log.e("o1", "o1");
//        initList(1);

        if (resultList == null || resultList.size() == 0) {
            Log.e("o2", "o2");
            return false;
        }

        if (m_ResultList3 != null && gv != null && m_ListAdapter3 != null) {
            Log.e("o3", "o3");
            if (m_ResultList3 != resultList) {
                Log.e("o4", "o4");
                m_ResultList3.addAll(resultList);
                m_ListAdapter3.notifyDataSetChanged();
                m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

//        gv.setVisibility(View.VISIBLE);


                mAnimAdapter = new AlphaInAnimationAdapter(m_ListAdapter3);
                mAnimAdapter.setAbsListView(gv);
                gv.setAdapter(mAnimAdapter);
//                m_ListAdapter3.setResultList(m_ResultList3);
            }
            return true;
        }
        Log.e("o5", "o5");
        m_ResultList3 = resultList;

        m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

//        gv.setVisibility(View.VISIBLE);


        mAnimAdapter = new AlphaInAnimationAdapter(m_ListAdapter3);
        mAnimAdapter.setAbsListView(gv);
        gv.setAdapter(mAnimAdapter);
//        gv.setAdapter(m_ListAdapter3);

//        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(final AdapterView<?> parent, View view,
//                                    final int position, long id) {
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent i = new Intent(UserFeedForIdActivity.this, SinglePostContentActivity.class);
//                        i.putExtra("postId", m_ResultList3.get(position - 1).getPost_id());
//                        startActivity(i);
//
////                        Intent i = new Intent(UserFeedForIdActivity.this, SinglePostSlideActivity.class);
////                        i.putExtra("navi", 4);
////                        i.putExtra("now", position-1);
////                        i.putExtra("user_id", tempEmail);
////                        if (layUser.isSelected()) {
////                            i.putExtra("type", "U");
////                        } else {
////                            i.putExtra("type", "R");
////                        }
////                        startActivity(i);
//                        //TODO 터치했을때 해동
//
//                    }
//                });
//
//            }
//        });

        gv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 25;

                    if (lastListCount != count) {
                        Log.e("list", count + "/" + page);

                        if ((view.getCount() - 1) % 25 == 0) {
                            if (!isLoading) {
                                if (nowPage < page + 1) {
                                    isLoading = true;

                                    if (edtSearch.getText().toString().equals("")) {
                                        getGridData(false, page + 1);
                                    } else {
                                        getGridSearch(false, page + 1, edtSearch.getText().toString());
                                    }

                                    lastListCount = count;
                                    nowPage = page + 1;
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        return true;
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }


    @Override
    public void onClick(View v) {
        Intent i = new Intent(UserFeedForIdActivity.this, UserListActivity.class);
        switch (v.getId()) {
            case R.id.lay_user_feed_top1:
                if (!layUser.isSelected()) {
                    layUser.setSelected(true);
                    layTag.setSelected(false);
                    getGridData(true, 1);
                }
                break;
            case R.id.lay_user_feed_top2:
                if (!layTag.isSelected()) {
                    layUser.setSelected(false);
                    layTag.setSelected(true);
                    getGridData(true, 1);
                }
                break;
            case R.id.imv_user_feed_back:
                finish();
                break;
            case R.id.imv_user_feed_is_follow:
                StringTransMethod stmFollow = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);
                            if (jd.getInt("result") == 0) {
                                if (jd.getString("follow").equals("Y")) {
                                    imvCheck.setSelected(true);
                                } else if (jd.getString("follow").equals("N")) {
                                    imvCheck.setSelected(false);
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                }
                            } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                try {

                    JSONObject tempJ = new JSONObject();
                    tempJ.put("email", tempEmail);

                    JSONArray jarr = new JSONArray();
                    jarr.put(tempJ);

                    Map<String, String> params = new HashMap<>();

                    params.put("myId", AppController.getSp().getString("email", ""));
                    params.put("email", jarr.toString());
                    params.put("type", "N");

                    AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 402, stmFollow);
                    AppController.apiDataTaskNew.execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tv_user_feed_user_follower_cnt:
            case R.id.tv_user_feed_user_follower_cnt_:
                i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWER);
                i.putExtra("id", tempEmail);
                startActivity(i);
                break;
            case R.id.tv_user_feed_user_following_cnt:
            case R.id.tv_user_feed_user_following_cnt_:
                i.putExtra("pageNavi", UserListActivity.USER_LIST_FOLLOWING);
                i.putExtra("id", tempEmail);
                startActivity(i);
                break;
        }
    }

}
