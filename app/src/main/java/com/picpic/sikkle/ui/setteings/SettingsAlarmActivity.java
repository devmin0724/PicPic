package com.picpic.sikkle.ui.setteings;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class SettingsAlarmActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack;
    ImageView imvAll, imvPL, imvCL, imvPC, imvFM, imvRM, imvCMP, imvCMC, imvFP, imvFI, imvFJA, imvFCA;
    TextView imvComplete;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_alarm);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 알림페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_alarm_back);

        imvComplete = (TextView) findViewById(R.id.imv_settings_alarm_next);

        imvAll = (ImageView) findViewById(R.id.imv_settings_alarm_all);
        imvPL = (ImageView) findViewById(R.id.imv_settings_alarm_pl);
        imvCL = (ImageView) findViewById(R.id.imv_settings_alarm_cl);
        imvPC = (ImageView) findViewById(R.id.imv_settings_alarm_pc);
        imvFM = (ImageView) findViewById(R.id.imv_settings_alarm_fm);
        imvRM = (ImageView) findViewById(R.id.imv_settings_alarm_rm);
        imvCMP = (ImageView) findViewById(R.id.imv_settings_alarm_cmp);
        imvCMC = (ImageView) findViewById(R.id.imv_settings_alarm_cmc);
        imvFP = (ImageView) findViewById(R.id.imv_settings_alarm_fp);
        imvFI = (ImageView) findViewById(R.id.imv_settings_alarm_fi);
        imvFJA = (ImageView) findViewById(R.id.imv_settings_alarm_fja);
        imvFCA = (ImageView) findViewById(R.id.imv_settings_alarm_fca);

        layBack.setOnClickListener(this);

        imvComplete.setOnClickListener(this);

        imvAll.setOnClickListener(this);
        imvPL.setOnClickListener(this);
        imvCL.setOnClickListener(this);
        imvPC.setOnClickListener(this);
        imvFM.setOnClickListener(this);
        imvRM.setOnClickListener(this);
        imvCMP.setOnClickListener(this);
        imvCMC.setOnClickListener(this);
        imvFP.setOnClickListener(this);
        imvFI.setOnClickListener(this);
        imvFJA.setOnClickListener(this);
        imvFCA.setOnClickListener(this);

        Log.e("boolean", AppController.getSp().getBoolean("alarmAll", true) + "/"
                        + AppController.getSp().getBoolean("alarmPL", true) + "/"
                        + AppController.getSp().getBoolean("alarmCL", true) + "/"
                        + AppController.getSp().getBoolean("alarmPC", true) + "/"
                        + AppController.getSp().getBoolean("alarmFM", true) + "/"
                        + AppController.getSp().getBoolean("alarmRM", true) + "/"
                        + AppController.getSp().getBoolean("alarmCMP", true) + "/"
                        + AppController.getSp().getBoolean("alarmCMC", true) + "/"
                        + AppController.getSp().getBoolean("alarmFP", true) + "/"
                        + AppController.getSp().getBoolean("alarmFI", true) + "/"
                        + AppController.getSp().getBoolean("alarmFJA", true) + "/"
                        + AppController.getSp().getBoolean("alarmFCA", true) + "/"
        );

        imvAll.setSelected(AppController.getSp().getBoolean("alarmAll", true));
        imvPL.setSelected(AppController.getSp().getBoolean("alarmPL", true));
        imvCL.setSelected(AppController.getSp().getBoolean("alarmCL", true));
        imvPC.setSelected(AppController.getSp().getBoolean("alarmPC", true));
        imvFM.setSelected(AppController.getSp().getBoolean("alarmFM", true));
        imvRM.setSelected(AppController.getSp().getBoolean("alarmRM", true));
        imvCMP.setSelected(AppController.getSp().getBoolean("alarmCMP", true));
        imvCMC.setSelected(AppController.getSp().getBoolean("alarmCMC", true));
        imvFP.setSelected(AppController.getSp().getBoolean("alarmFP", true));
        imvFI.setSelected(AppController.getSp().getBoolean("alarmFI", true));
        imvFJA.setSelected(AppController.getSp().getBoolean("alarmFJA", true));
        imvFCA.setSelected(AppController.getSp().getBoolean("alarmFCA", true));

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_alarm_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_settings_alarm_next:
                //TODO 알림 ok
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.imv_settings_alarm_all:
                imvAll.setSelected(!AppController.getSp().getBoolean("alarmAll", true));
                AppController.getSe().putBoolean("alarmAll", !AppController.getSp().getBoolean("alarmAll", true));
                AppController.getSe().commit();
                if (AppController.getSp().getBoolean("alarmAll", true)) {
                    imvPL.setSelected(true);
                    imvCL.setSelected(true);
                    imvPC.setSelected(true);
                    imvFM.setSelected(true);
                    imvRM.setSelected(true);
                    imvCMP.setSelected(true);
                    imvCMC.setSelected(true);
                    imvFP.setSelected(true);
                    imvFI.setSelected(true);
                    imvFJA.setSelected(true);
                    imvFCA.setSelected(true);
                } else {
                    imvPL.setSelected(false);
                    imvCL.setSelected(false);
                    imvPC.setSelected(false);
                    imvFM.setSelected(false);
                    imvRM.setSelected(false);
                    imvCMP.setSelected(false);
                    imvCMC.setSelected(false);
                    imvFP.setSelected(false);
                    imvFI.setSelected(false);
                    imvFJA.setSelected(false);
                    imvFCA.setSelected(false);
                }
                break;
            case R.id.imv_settings_alarm_pl:
                imvPL.setSelected(!AppController.getSp().getBoolean("alarmPL", true));
                AppController.getSe().putBoolean("alarmPL", !AppController.getSp().getBoolean("alarmPL", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_cl:
                imvCL.setSelected(!AppController.getSp().getBoolean("alarmCL", true));
                AppController.getSe().putBoolean("alarmCL", !AppController.getSp().getBoolean("alarmCL", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_pc:
                imvPC.setSelected(!AppController.getSp().getBoolean("alarmPC", true));
                AppController.getSe().putBoolean("alarmPC", !AppController.getSp().getBoolean("alarmPC", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_fm:
                imvFM.setSelected(!AppController.getSp().getBoolean("alarmFM", true));
                AppController.getSe().putBoolean("alarmFM", !AppController.getSp().getBoolean("alarmFM", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_rm:
                imvRM.setSelected(!AppController.getSp().getBoolean("alarmRM", true));
                AppController.getSe().putBoolean("alarmRM", !AppController.getSp().getBoolean("alarmRM", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_cmp:
                imvCMP.setSelected(!AppController.getSp().getBoolean("alarmCMP", true));
                AppController.getSe().putBoolean("alarmCMP", !AppController.getSp().getBoolean("alarmCMP", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_cmc:
                imvCMC.setSelected(!AppController.getSp().getBoolean("alarmCMC", true));
                AppController.getSe().putBoolean("alarmCMC", !AppController.getSp().getBoolean("alarmCMC", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_fp:
                imvFP.setSelected(!AppController.getSp().getBoolean("alarmFP", true));
                AppController.getSe().putBoolean("alarmFP", !AppController.getSp().getBoolean("alarmFP", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_fi:
                imvFI.setSelected(!AppController.getSp().getBoolean("alarmFI", true));
                AppController.getSe().putBoolean("alarmFI", !AppController.getSp().getBoolean("alarmFI", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_fja:
                imvFJA.setSelected(!AppController.getSp().getBoolean("alarmFJA", true));
                AppController.getSe().putBoolean("alarmFJA", !AppController.getSp().getBoolean("alarmFJA", true));
                AppController.getSe().commit();
                break;
            case R.id.imv_settings_alarm_fca:
                imvFCA.setSelected(!AppController.getSp().getBoolean("alarmFCA", true));
                AppController.getSe().putBoolean("alarmFCA", !AppController.getSp().getBoolean("alarmFCA", true));
                AppController.getSe().commit();
                break;
        }
    }
}
