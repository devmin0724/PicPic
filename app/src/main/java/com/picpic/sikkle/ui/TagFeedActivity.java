package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.EmptyAdapter;
import com.picpic.sikkle.adapter.SampleAdapter;
import com.picpic.sikkle.adapter.TimeLineAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter;
import com.picpic.sikkle.adapter.TimeLineGridAdapter2;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.FixedStopImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagFeedActivity extends Activity implements View.OnClickListener {

    String tempTag = "", tempTagId = "", tempFounderId = "";
    //    ImageView imvList, imvGrid;
    TextView tvId, tvHotTag, tvPostCount, tvFollowerCount, tvFollow, tvFounder, tvPostCount_, tvFollowerCount_;
    FixedStopImageView fimvBg;
    EditText edtSearch;
    LinearLayout imvBack, imvShare;

    StaggeredGridView gv;
    boolean isGrid = true, isFollow = false;
    int gifWidth = 200, gifHeight = 200, nowPage = 1;
    GridResult m_ResultList3 = null;
    SampleAdapter m_ListAdapter3 = null;
    private boolean lastItemVisibleFlag = false;
    int lastListCount = 0;
    boolean isLoading = false;
    View header;

    public static AnimationAdapter mAnimAdapter;

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getGridSearch(true, 1, edtSearch.getText().toString());
//            getInitListSearchData(edtSearch.getText().toString(), true);
//            if(edtSearch.getText().toString().equals("")){
//                getGridData(true, 1);
//            }else{
//                getGridSearch(true, 1, edtSearch.getText().toString());
//            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tag_feed);
        tempTag = getIntent().getExtras().getString("tag");

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("태그 피드 페이지_" + tempTag);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        initViews();

    }


    @Override
    protected void onDestroy() {
//        AppController.gifExe.shutdownNow();
//        AppController.thumbExe.shutdownNow();
        super.onDestroy();
    }

    private void initViews() {

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        header = inflater2.inflate(R.layout.header_tag_feed, null, false);

        imvBack = (LinearLayout) header.findViewById(R.id.imv_tag_feed_back);
        imvBack.setOnClickListener(this);
//        imvNonBack = (LinearLayout) findViewById(R.id.imv_non_tag_feed_back);
//        imvNonBack.setOnClickListener(this);
        imvShare = (LinearLayout) header.findViewById(R.id.imv_tag_feed_share);
//        imvNonShare = (LinearLayout) findViewById(R.id.imv_non_tag_feed_share);
//        imvList = (ImageView) header.findViewById(R.id.imv_tag_feed_sort_1);
//        imvList.setOnClickListener(this);
//        imvGrid = (ImageView) header.findViewById(R.id.imv_tag_feed_sort_2);
//        imvGrid.setOnClickListener(this);
//        imvNonList = (ImageView) findViewById(R.id.imv_non_tag_feed_sort_1);
//        imvNonList.setOnClickListener(this);
//        imvNonGrid = (ImageView) findViewById(R.id.imv_non_tag_feed_sort_2);
//        imvNonGrid.setOnClickListener(this);
        fimvBg = (FixedStopImageView) header.findViewById(R.id.imv_tag_feed_bg);
//        fimvNonBg = (FixedStopImageView) findViewById(R.id.imv_non_tag_feed_bg);

        edtSearch = (EditText) header.findViewById(R.id.edt_tag_feed_input);
        edtSearch.addTextChangedListener(tw);

        edtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    edtSearch.setCursorVisible(true);
                } else {
                    edtSearch.setCursorVisible(false);
                }
            }
        });

//        edtNonSearch = (EditText) findViewById(R.id.edt_non_tag_feed_input);
//        edtNonSearch.addTextChangedListener(tw);

//        layNon = (LinearLayout) findViewById(R.id.lay_tag_feed_non);

        tvId = (TextView) header.findViewById(R.id.tv_tag_feed_user_id);
        tvPostCount = (TextView) header.findViewById(R.id.tv_tag_feed_user_post_cnt);
        tvFollowerCount = (TextView) header.findViewById(R.id.tv_tag_feed_user_follower_cnt);
        tvPostCount_ = (TextView) header.findViewById(R.id.tv_tag_feed_user_post_cnt_);
        tvFollowerCount_ = (TextView) header.findViewById(R.id.tv_tag_feed_user_follower_cnt_);
        tvFollowerCount.setOnClickListener(this);
        tvFollowerCount_.setOnClickListener(this);
        tvFollow = (TextView) header.findViewById(R.id.tv_tag_feed_follow);
        tvFollow.setOnClickListener(this);
        tvFounder = (TextView) header.findViewById(R.id.tv_tag_feed_founder);
        tvFounder.setOnClickListener(this);

//        tvNonId = (TextView) findViewById(R.id.tv_non_tag_feed_user_id);
//        tvNonPostCount = (TextView) findViewById(R.id.tv_non_tag_feed_user_post_cnt);
//        tvNonPostCount.setOnClickListener(this);
//        tvNonFollowerCount = (TextView) findViewById(R.id.tv_non_tag_feed_user_follower_cnt);
//        tvNonFollowerCount.setOnClickListener(this);
//        tvNonFollow = (TextView) findViewById(R.id.tv_non_tag_feed_follow);
//        tvNonFollow.setOnClickListener(this);
//        tvNonFounder = (TextView) findViewById(R.id.tv_non_tag_feed_founder);
//        tvNonFounder.setOnClickListener(this);

        gv = (StaggeredGridView) findViewById(R.id.gv_tag_feed);
        gv.addHeaderView(header);

        getData();

        isGrid = true;
//        imvGrid.setSelected(true);
//        imvNonGrid.setSelected(true);
    }

    private void getData() {
        StringTransMethod stmTagResult = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    MinUtils.d("tag", result);
                    final JSONObject jd = new JSONObject(result);
                    if (jd.getInt("result") == 0) {
                        if (jd.getString("tag_id").equals("")) {
                            finish();
                            overridePendingTransition(0, 0);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_msg_not_exists_tag), Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        fimvBg.setImageURLString(AppController.URL + jd.getString("url"));
//                                        fimvNonBg.setImageURLString(AppController.URL + jd.getString("url"));
                                        if (jd.getString("follow_yn").equals("Y")) {
                                            followCheck(true);
                                        } else {
                                            followCheck(false);
                                        }

                                        tvFollowerCount.setText(jd.getInt("follow_cnt") + "");
                                        tvPostCount.setText(jd.getInt("post_cnt") + "");
                                        tvId.setText("#" + jd.getString("tag_name"));
                                        tvFounder.setText("@" + jd.getString("id"));

//                                        tvNonFollowerCount.setText(jd.getInt("follow_cnt") + "");
//                                        tvNonPostCount.setText(jd.getInt("post_cnt") + "");
//                                        tvNonId.setText("#" + jd.getString("tag_name"));
//                                        tvNonFounder.setText("@" + jd.getString("id"));

                                        tempFounderId = jd.getString("id");

                                        tempTagId = jd.getString("tag_id");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });
                        }
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (JSONException e) {

                } finally {
                    getGridData(true, 1);
                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_str", tempTag);

        AppController.apiTaskNew = new APITaskNew(this, params, 517, stmTagResult);
        AppController.apiTaskNew.execute();

    }

    private void getGridData(boolean is, int page) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

        Map<String, String> params = new HashMap<>();

//        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("str", "");
//
//        AppController.apiTaskNew = new APITaskNew(params, 511, stmInitListRestult);
//        AppController.apiTaskNew.execute();

        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
        params.put("page", "" + page);
        params.put("range", "N");
//        params.put("str", AppController.getSp().getString("email", ""));
        params.put("str", tempTag);
        params.put("type", "T");
        params.put("timeline", "F");

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();

    }

    private void getGridSearch(boolean is, int page, String str) {
        if (is) {

            m_ListAdapter3 = null;
            m_ResultList3 = null;
            page = 1;

        }

        Map<String, String> params = new HashMap<>();

//        params.put("my_id", AppController.getSp().getString("email", ""));
//        params.put("user_id", AppController.getSp().getString("email", ""));
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("str", "");
//
//        AppController.apiTaskNew = new APITaskNew(params, 511, stmInitListRestult);
//        AppController.apiTaskNew.execute();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("tag_id", tempTagId);
        params.put("page", "" + page);
        params.put("range", "N");
//        params.put("str", AppController.getSp().getString("email", ""));
        params.put("str", str);
        params.put("type", "TT");
        params.put("timeline", "F");

        //tag_id -> 태그아이디를주고 TT

        AppController.apiTaskNew = new APITaskNew(this, params, 520, stmInitListRestult2);
        AppController.apiTaskNew.execute();
    }

    StringTransMethod stmInitListRestult2 = new StringTransMethod() {
        @Override
        public void endTrans(String result) {
            try {
                MinUtils.d("rr", result);
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    if(jd.isNull("data")){
                        if(m_ListAdapter3 == null){
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    }
                    JSONArray jarr = new JSONArray(jd.getString("data"));

                    Log.e("length", jarr.length() + "");

                    if (jarr.length() < 1) {
                        Log.e("length", jarr.length() + "");
                        if(m_ListAdapter3.getCount() < 2){
                            Log.e("length", jarr.length() + "");
                            GridResult gr = new GridResult();
                            GridItem gi = new GridItem();
                            gi.setIs0(true);
                            gr.add(gi);
                            updateResultList3(gr);
                        }
                    } else {
                        GridResult gr = new GridResult();
                        GridItem gi;
                        JSONObject j;
                        for (int i = 0; i < jarr.length(); i++) {
                            gi = new GridItem();

                            j = jarr.getJSONObject(i);

                            gi.setUrl(j.getString("url"));
                            gi.setPost_id(j.getString("post_id"));

                            if (!j.isNull("height1")) {
                                gi.setHeight1(j.getInt("height1"));

                            }
                            if (!j.isNull("height2")) {

                                gi.setHeight2(j.getInt("height2"));
                            }
                            if (!j.isNull("heightTh")) {
                                gi.setHeightTH(j.getInt("heightTh"));

                            }
                            if (!j.isNull("width1")) {

                                gi.setWidth1(j.getInt("width1"));
                            }
                            if (!j.isNull("width2")) {

                                gi.setWidth2(j.getInt("width2"));
                            }
                            if (!j.isNull("widthTh")) {
                                gi.setWidthTH(j.getInt("widthTh"));

                            }

//                            int w = gi.getWidth1();
//                            int h = gi.getHeight1();
//
//                            int w2 = MinUtils.screenWidth / 3;
//                            int h2 = w2 * h / w;
//
//                            gi.setWidth1(w2);
//                            gi.setHeight1(h2);

                            if (!j.isNull("size1")) {

                                gi.setSize1(j.getInt("size1"));
                            }
                            if (!j.isNull("size2")) {
                                gi.setSize2(j.getInt("size2"));

                            }
                            if (!j.isNull("sizeTh")) {
                                gi.setSizeTH(j.getInt("sizeTh"));

                            }


                            gr.add(gi);

                        }
//                        initList(1);
                        updateResultList3(gr);
                    }
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {

            }finally {

                isLoading = false;
            }
        }
    };

    protected boolean updateResultList3(GridResult resultList) {

        Log.e("o1", "o1");

        if (resultList == null || resultList.size() == 0) {
            Log.e("o2", "o2");
            return false;
        }

        if (m_ResultList3 != null && gv != null && m_ListAdapter3 != null) {
            Log.e("o3", "o3");
            if (m_ResultList3 != resultList) {
                Log.e("o4", "o4");
                m_ResultList3.addAll(resultList);
                m_ListAdapter3.notifyDataSetChanged();
                m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

//        gv.setAdapter(m_ListAdapter3);

                mAnimAdapter = new AlphaInAnimationAdapter(m_ListAdapter3);
                mAnimAdapter.setAbsListView(gv);
                gv.setAdapter(mAnimAdapter);
//                m_ListAdapter3.setResultList(m_ResultList3);
            }
            return true;
        }
        Log.e("o5", "o5");
        m_ResultList3 = resultList;

        m_ListAdapter3 = new SampleAdapter(this, 0, m_ResultList3);

//        gv.setAdapter(m_ListAdapter3);

        mAnimAdapter = new AlphaInAnimationAdapter(m_ListAdapter3);
        mAnimAdapter.setAbsListView(gv);
        gv.setAdapter(mAnimAdapter);

//        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(final AdapterView<?> parent, View view,
//                                    final int position, long id) {
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent i = new Intent(TagFeedActivity.this, SinglePostContentActivity.class);
//                        i.putExtra("postId", m_ResultList3.get(position - 1).getPost_id());
//                        startActivity(i);
//
////                        Intent i = new Intent(TagFeedActivity.this, SinglePostSlideActivity.class);
////                        i.putExtra("navi", 5);
////                        i.putExtra("now", position-1);
////                        i.putExtra("tag", tempTag);
////                        startActivity(i);
//                        //TODO 터치했을때 해동
//
//                    }
//                });
//
//            }
//        });

        gv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 25;

                    if (lastListCount != count) {
                        Log.e("list", count + "/" + page);

                        if ((view.getCount() - 1) % 25 == 0) {
                            if (!isLoading) {
                                if (nowPage < page + 1) {
                                    isLoading = true;

                                    if (edtSearch.getText().toString().equals("")) {
                                        getGridData(false, page + 1);
                                    } else {
                                        getGridSearch(false, page + 1, edtSearch.getText().toString());
                                    }

                                    lastListCount = count;
                                    nowPage = page + 1;
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount);
            }
        });

        return true;
    }
//    private void getListData() {
//        Map<String, String> params = new HashMap<>();
//
//        params.put("tag_id", tempTagId);
//        params.put("page", "1");
//        params.put("range", "N");
//        params.put("my_id", AppController.getSp().getString("email", ""));
//
//        AppController.apiTask = new APITask(params, 507);
//
//        try {
//            processListData(AppController.apiTask.execute().get());
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }


    private void followCheck(boolean isFollowCheck) {
        if (isFollowCheck) {
            isFollow = true;
            tvFollow.setSelected(true);
            tvFollow.setText(getResources().getString(R.string.btn_following_2));
            tvFollow.setTextColor(Color.WHITE);
//            tvNonFollow.setSelected(true);
//            tvNonFollow.setText(getResources().getString(R.string.btn_following_2));
//            tvNonFollow.setTextColor(Color.WHITE);
        } else {
            isFollow = false;
            tvFollow.setSelected(false);
            tvFollow.setText(getResources().getString(R.string.btn_following_1));
            tvFollow.setTextColor(Color.WHITE);
//            tvNonFollow.setSelected(false);
//            tvNonFollow.setText(getResources().getString(R.string.btn_following_1));
//            tvNonFollow.setTextColor(Color.WHITE);
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }


    @Override
    public void onClick(View v) {
        Intent i = new Intent(TagFeedActivity.this, UserListActivity.class);
        switch (v.getId()) {
            case R.id.imv_tag_feed_back:
                finish();
                break;
            case R.id.tv_tag_feed_follow:
                StringTransMethod stmFollowResult = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        try {
                            JSONObject jd = new JSONObject(result);
                            if (jd.getInt("result") == 0) {
                                if (jd.getString("follow").equals("Y")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + tempTagId).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                            followCheck(true);
                                            tvFollowerCount.setText((Integer.parseInt(tvFollowerCount.getText().toString()) + 1) + "");
                                        }
                                    });

                                } else if (jd.getString("follow").equals("N")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + tempTagId).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                            followCheck(false);
                                            tvFollowerCount.setText((Integer.parseInt(tvFollowerCount.getText().toString()) - 1) + "");
                                        }
                                    });
                                }
                            } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                };
                Map<String, String> params = new HashMap<>();

                params.put("myId", AppController.getSp().getString("email", ""));
                params.put("tag_id", tempTagId);

                AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 403, stmFollowResult);
                AppController.apiDataTaskNew.execute();

                break;
            case R.id.tv_tag_feed_user_follower_cnt:
            case R.id.tv_tag_feed_user_follower_cnt_:
                i.putExtra("pageNavi", UserListActivity.USER_LIST_TAG_FOLLOWER);
                i.putExtra("id", tempTagId);
                startActivity(i);
                break;
            case R.id.tv_tag_feed_founder:
                Intent ii = new Intent(TagFeedActivity.this, UserFeedForIdActivity.class);
                ii.putExtra("id", tempFounderId);
                startActivity(ii);
                break;
        }
    }

}
