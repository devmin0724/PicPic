/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.InterestFeedItem;
import com.picpic.sikkle.beans.InterestFeedResult;
import com.picpic.sikkle.beans.InterestSearchUserItem;
import com.picpic.sikkle.beans.SimplePostItem;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.login_join.FriendResultActivity;
import com.picpic.sikkle.ui.popup.CameraPopUp;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinThumbImageRun;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.MinVideoRun;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;
import com.picpic.sikkle.widget.FixedStopImageView;
import com.picpic.sikkle.widget.HashTag;
import com.picpic.sikkle.widget.HashTagV1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InterestFeedV1Activity extends Activity implements View.OnClickListener {

    LinearLayout laySearch, layTimeline, layInterest, layCamera, layNoti, layMyPage;
    ImageView imvNoti;
    //    public static Activity ac;
//    InterestFeedAdapter ifa;
    SearchAdapter adapter;
    ListView list;

    int titles[] = {
//            R.string.category_2, R.string.category_3, R.string.category_4, R.string.category_5,
//            R.string.category_6, R.string.category_7, R.string.category_8, R.string.category_9,
//            R.string.category_10, R.string.category_11, R.string.category_12, R.string.category_13,
//            R.string.category_14, R.string.category_15, R.string.category_16, R.string.category_17,
//            R.string.category_18, R.string.category_19, R.string.category_20, R.string.category_21,
//            R.string.category_22, R.string.category_23, R.string.category_24, R.string.category_25
            R.string.cate_1, R.string.cate_2, R.string.cate_3, R.string.cate_4,
            R.string.cate_5, R.string.cate_6, R.string.cate_7, R.string.cate_8,
            R.string.cate_9, R.string.cate_10, R.string.cate_11, R.string.cate_12,
            R.string.cate_13, R.string.cate_14, R.string.cate_15, R.string.cate_16,
            R.string.cate_17, R.string.cate_18, R.string.cate_19
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_interest_feed_v1);

//        ac = this;

        list = (ListView) findViewById(R.id.list_interest_feed_v1);

        laySearch = (LinearLayout) findViewById(R.id.imv_interest_feed_v1_search);
        laySearch.setOnClickListener(this);

        layTimeline = (LinearLayout) findViewById(R.id.lay_interest_feed_timeline);
        layInterest = (LinearLayout) findViewById(R.id.lay_interest_feed_interest);
        layCamera = (LinearLayout) findViewById(R.id.lay_interest_feed_camera);
        layNoti = (LinearLayout) findViewById(R.id.lay_interest_feed_noti);
        layMyPage = (LinearLayout) findViewById(R.id.lay_interest_feed_mypage);
        layTimeline.setOnClickListener(this);
        layInterest.setOnClickListener(this);
        layCamera.setOnClickListener(this);
        layNoti.setOnClickListener(this);
        layMyPage.setOnClickListener(this);
        layInterest.setSelected(true);

        imvNoti = (ImageView) findViewById(R.id.imv_interest_feed_noti);
        imvNoti.setOnClickListener(this);

        StringTransMethod test = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                MinUtils.d("test", result);
                try {
                    JSONObject jd = new JSONObject(result);

                    InterestFeedResult ifr = new InterestFeedResult();
                    InterestFeedItem ifi;
                    ArrayList<String> posts, urls;

                    for (int i = 0; i < titles.length; i++) {
                        ifi = new InterestFeedItem();
                        posts = new ArrayList<>();
                        urls = new ArrayList<>();
                        JSONArray ja = new JSONArray(jd.getString("category_" + (i+1)));
                        for (int j = 0; j < ja.length(); j++) {
                            JSONObject jason = ja.getJSONObject(j);

                            posts.add(jason.getString("post_id"));
                            urls.add(jason.getString("url"));
                        }

                        ifi.setPostIds(posts);
                        ifi.setUrls(urls);

                        ifr.add(ifi);
                    }

//                    settingViews(ifr);
//                    ifa = new InterestFeedAdapter(ifr);
                    adapter = new SearchAdapter(InterestFeedV1Activity.this, 0, ifr);

//                    list.setAdapter(ifa);
                    list.setAdapter(adapter);

//                    list.setOnScrollListener(mScrollListener);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiDataTaskNew = new APIDataTaskNew(InterestFeedV1Activity.this, params, 524, test);
        AppController.apiDataTaskNew.execute();

    }

//    AbsListView.OnScrollListener mScrollListener = new AbsListView.OnScrollListener() {
//        @Override
//        public void onScrollStateChanged(AbsListView view, int scrollState) {
//            switch (scrollState) {
//                case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
//                    ifa.setFlagBusy(true);
//                    break;
//                case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
//                    ifa.setFlagBusy(false);
//                    break;
//                case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
//                    ifa.setFlagBusy(false);
//                    break;
//                default:
//                    break;
//            }
//            ifa.notifyDataSetChanged();
//        }
//
//        @Override
//        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//        }
//    };

    @Override
    protected void onResume() {
        notiCheck();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
//        ImageLoader imageLoader = ifa.getImageLoader();
//        if(imageLoader != null){
//            imageLoader.clearCache();
//        }
        super.onDestroy();
    }

    private void notiCheck() {
        StringTransMethod stm = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        AppController.isNoti = jd.getString("new_yn").equals("Y");
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                    if (AppController.isNoti) {
                        imvNoti.setVisibility(View.VISIBLE);
                    } else {
                        imvNoti.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();
        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(this, params, 605, stm);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(InterestFeedV1Activity.this, TimelineV1NewActivity.class));
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.imv_interest_feed_v1_search:
                overridePendingTransition(0, 0);
                Intent ii = new Intent(InterestFeedV1Activity.this, InterestSearchActivity.class);
                ii.putExtra("navi", 0);
                startActivity(ii);
                break;
            case R.id.lay_interest_feed_timeline:
                overridePendingTransition(0, 0);
//                startActivity(new Intent(MyPageActivity.this, NewGridTimelineActivity.class));
                startActivity(new Intent(InterestFeedV1Activity.this, TimelineV1NewActivity.class));
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_interest_feed_interest:
//                i = new Intent(interest_feedActivity.this, InterestFeedActivity.class);
                break;
            case R.id.lay_interest_feed_camera:
                startActivity(new Intent(InterestFeedV1Activity.this, CameraPopUp.class));
                break;
            case R.id.lay_interest_feed_noti:
                i = new Intent(InterestFeedV1Activity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_interest_feed_mypage:
                i = new Intent(InterestFeedV1Activity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }

//    private void settingViews(InterestFeedResult ifr){
//
//        for(int i=0; i<ifr.size(); i++){
//
//
//
//
//            InterestFeedItem model = ifr.get(i);
//
//            final ArrayList<String> posts = model.getPostIds();
//            ArrayList<String> urls = model.getUrls();
//
//            String tempUrl = urls.get(0);
//            int lastIdx = tempUrl.lastIndexOf("_");
//            String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//
//            Object image = AppController.memoryCache.get(AppController.URL + tempName);
//
//            File gifFile = AppController.fileCache.getFile(AppController.URL + tempName);
//
//            //TODO 파일비교
//
//            long file_size2 = gifFile.length();
//
//            new MinUtils.ThumbTask(ac, AppController.URL + tempName, imv1, image, file_size2, 1).execute();
//
//            tempUrl = urls.get(1);
//            lastIdx = tempUrl.lastIndexOf("_");
//            tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//
//            image = AppController.memoryCache.get(AppController.URL + tempName);
//
//            gifFile = AppController.fileCache.getFile(AppController.URL + tempName);
//
//            //TODO 파일비교
//
//            file_size2 = gifFile.length();
//
//            new MinUtils.ThumbTask(ac, AppController.URL + tempName, imv2, image, file_size2, 1).execute();
//
//            tempUrl = urls.get(2);
//            lastIdx = tempUrl.lastIndexOf("_");
//            tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//
//            image = AppController.memoryCache.get(AppController.URL + tempName);
//
//            gifFile = AppController.fileCache.getFile(AppController.URL + tempName);
//
//            //TODO 파일비교
//
//            file_size2 = gifFile.length();
//
//            new MinUtils.ThumbTask(ac, AppController.URL + tempName, imv3, image, file_size2, 1).execute();
//
//            tempUrl = urls.get(3);
//            lastIdx = tempUrl.lastIndexOf("_");
//            tempName = tempUrl.substring(0, lastIdx) + ".jpg";
//
//            image = AppController.memoryCache.get(AppController.URL + tempName);
//
//            gifFile = AppController.fileCache.getFile(AppController.URL + tempName);
//
//            //TODO 파일비교
//
//            file_size2 = gifFile.length();
//
//            new MinUtils.ThumbTask(ac, AppController.URL + tempName, imv4, image, file_size2, 1).execute();
//
//            imv1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i = new Intent(InterestFeedV1Activity.this, SinglePostContentActivity.class);
//                    i.putExtra("postId", posts.get(0));
//                    startActivity(i);
//                }
//            });
//
//            imv2.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i = new Intent(InterestFeedV1Activity.this, SinglePostContentActivity.class);
//                    i.putExtra("postId", posts.get(1));
//                    startActivity(i);
//                }
//            });
//
//            imv3.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i = new Intent(InterestFeedV1Activity.this, SinglePostContentActivity.class);
//                    i.putExtra("postId", posts.get(2));
//                    startActivity(i);
//                }
//            });
//
//            imv4.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i = new Intent(InterestFeedV1Activity.this, SinglePostContentActivity.class);
//                    i.putExtra("postId", posts.get(3));
//                    startActivity(i);
//                }
//            });
//
//            final int finalI = i;
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent intent = new Intent(InterestFeedV1Activity.this, CategoryListActivity.class);
//                    intent.putExtra("pageNavi", finalI + 1);
//                    startActivity(intent);
//                }
//            });
//
//
//
//        }
//
//    }

    class InterestFeedAdapter extends BaseAdapter {

        String TAG = "LoaderAdapter";
        private boolean mBusy = false;

        public void setFlagBusy(boolean busy) {
            this.mBusy = busy;
        }

        InterestFeedResult ifr;
        LayoutInflater m_LayoutInflater;
        //        private ImageLoader mImageLoader;
        private GIFLoader gifLoader;
        int mCount;

        public InterestFeedAdapter(InterestFeedResult ifr) {
            this.ifr = ifr;
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mCount = ifr.size();
//            this.mImageLoader = new ImageLoader(InterestFeedV1Activity.this);
            this.gifLoader = new GIFLoader(InterestFeedV1Activity.this);
        }

        public GIFLoader getImageLoader() {
            return gifLoader;
        }

        @Override
        public int getCount() {
            return mCount;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return super.getViewTypeCount();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (position <= 3) {
                v = m_LayoutInflater.inflate(R.layout.row_interest_feed, parent, false);

                TextView title = ViewHolder.get(v, R.id.tv_row_interest_feed_category);

                final ImageView imv1 = ViewHolder.get(v, R.id.imv_row_interest_feed_1);
                final ImageView imv2 = ViewHolder.get(v, R.id.imv_row_interest_feed_2);
                final ImageView imv3 = ViewHolder.get(v, R.id.imv_row_interest_feed_3);
                final ImageView imv4 = ViewHolder.get(v, R.id.imv_row_interest_feed_4);

                imv1.setImageResource(R.drawable.non);
                imv2.setImageResource(R.drawable.non);
                imv3.setImageResource(R.drawable.non);
                imv4.setImageResource(R.drawable.non);

                InterestFeedItem model = ifr.get(position);

                title.setText(getResources().getString(titles[position]));

                final ArrayList<String> posts = model.getPostIds();
                ArrayList<String> urls = model.getUrls();

                String tempUrl = urls.get(0);
//                int lastIdx = tempUrl.lastIndexOf("_");
//                String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                String tempName = tempUrl;

                if (!mBusy) {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv1, false);
                } else {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv1, false);
                }

                tempUrl = urls.get(1);
//                lastIdx = tempUrl.lastIndexOf("_");
//                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                tempName = tempUrl;

                if (!mBusy) {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv2, false);
                } else {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv2, false);
                }

                tempUrl = urls.get(2);
//                lastIdx = tempUrl.lastIndexOf("_");
//                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                tempName = tempUrl;

                if (!mBusy) {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv3, false);
                } else {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv3, false);
                }

                tempUrl = urls.get(3);
//                lastIdx = tempUrl.lastIndexOf("_");
//                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                tempName = tempUrl;

                if (!mBusy) {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv4, false);
                } else {
                    gifLoader.DisplayImage(AppController.URL + tempName, imv4, false);
                }

            } else {
                v = m_LayoutInflater.inflate(R.layout.row_category_select, null);

                TextView tv = ViewHolder.get(v, R.id.tv_category_row);

                tv.setText(getResources().getString(titles[position]));
            }

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(InterestFeedV1Activity.this, CategoryListActivity.class);
                    intent.putExtra("pageNavi", position);
                    startActivity(intent);
                }
            });

            return v;
        }
    }

    public static class ViewHolder {
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {
                viewHolder = new SparseArray<View>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }
            return (T) childView;
        }
    }

    public class SearchAdapter extends ArrayAdapter<InterestFeedItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;
        InterestFeedResult ifr;
        GIFLoader gifLoader;
        ImageLoader imageLoader;

        public SearchAdapter(Context ctx, int txtViewId, List<InterestFeedItem> modles) {
            super(ctx, txtViewId, modles);
            ifr = (InterestFeedResult) modles;
            gifLoader = new GIFLoader(ctx);
            imageLoader = new ImageLoader(ctx);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) InterestFeedV1Activity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
//                if (position % 2 == 1) {
                convertView = m_LayoutInflater.inflate(R.layout.row_interest_feed, parent, false);

                TextView title = (TextView) convertView.findViewById(R.id.tv_row_interest_feed_category);
                TextView selectAll = (TextView) convertView.findViewById(R.id.tv_row_interest_feed_category_select_all);

                final FixedImageView imv1 = (FixedImageView) convertView.findViewById(R.id.imv_row_interest_feed_1);
                final FixedImageView imv2 = (FixedImageView) convertView.findViewById(R.id.imv_row_interest_feed_2);
                final FixedImageView imv3 = (FixedImageView) convertView.findViewById(R.id.imv_row_interest_feed_3);
                final FixedImageView imv4 = (FixedImageView) convertView.findViewById(R.id.imv_row_interest_feed_4);

                LinearLayout layImages = (LinearLayout) convertView.findViewById(R.id.lay_row_interest_images);

                if (position <= 3) {
                    layImages.setVisibility(View.VISIBLE);
                    selectAll.setVisibility(View.VISIBLE);
                } else {
                    layImages.setVisibility(View.GONE);
                    selectAll.setVisibility(View.GONE);
                }

                imv1.setImageResource(R.drawable.non);
                imv2.setImageResource(R.drawable.non);
                imv3.setImageResource(R.drawable.non);
                imv4.setImageResource(R.drawable.non);

                InterestFeedItem model = ifr.get(position);

                title.setText(getResources().getString(titles[position]));

                ArrayList<String> urls = model.getUrls();

                String tempUrl = urls.get(0).replace("_2", "_1");
                int lastIdx = tempUrl.lastIndexOf("_");
                String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                gifLoader.DisplayImage(AppController.URL + tempUrl, imv1, false);
//                imv1.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);

                tempUrl = urls.get(1).replace("_2", "_1");
                lastIdx = tempUrl.lastIndexOf("_");
                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                gifLoader.DisplayImage(AppController.URL + tempUrl, imv2, false);

//                imv2.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);

                tempUrl = urls.get(2).replace("_2", "_1");
                lastIdx = tempUrl.lastIndexOf("_");
                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                gifLoader.DisplayImage(AppController.URL + tempUrl, imv3, false);

//                imv3.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);

                tempUrl = urls.get(3).replace("_2", "_1");
                lastIdx = tempUrl.lastIndexOf("_");
                tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                gifLoader.DisplayImage(AppController.URL + tempUrl, imv4, false);

//                imv4.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
//                } else {
//                    convertView = m_LayoutInflater.inflate(R.layout.row_category_select, null);
//
//                    TextView tv = (TextView) convertView.findViewById(R.id.tv_category_row);
//
//                    tv.setText(getResources().getString(titles[position]));
//                }

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(InterestFeedV1Activity.this, CategoryListActivity.class);
                        intent.putExtra("pageNavi", pos);
                        startActivity(intent);
                    }
                });
            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }

}
