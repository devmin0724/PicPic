package com.picpic.sikkle.ui.setteings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

public class SettingsSocialActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack, layFace, layTwitter, layTumblr, layPinterest;
    FrameLayout layComplete;
    Switch swFace, swTwitter, swTumblr, swPinterest;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings_social);

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("설정 SNS 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        layBack = (LinearLayout) findViewById(R.id.lay_settings_social_back);
        layComplete = (FrameLayout) findViewById(R.id.lay_settings_social_complete);

        layFace = (LinearLayout) findViewById(R.id.lay_settings_social_facebook);
        layTwitter = (LinearLayout) findViewById(R.id.lay_settings_social_twitter);
        layTumblr = (LinearLayout) findViewById(R.id.lay_settings_social_tumblr);
        layPinterest = (LinearLayout) findViewById(R.id.lay_settings_social_pinterest);

        layBack.setOnClickListener(this);
        layComplete.setOnClickListener(this);
        layFace.setOnClickListener(this);
        layTwitter.setOnClickListener(this);
        layTumblr.setOnClickListener(this);
        layPinterest.setOnClickListener(this);

        swFace = (Switch) findViewById(R.id.switch_settings_social_facebook);
        swTwitter = (Switch) findViewById(R.id.switch_settings_social_twitter);
        swTumblr = (Switch) findViewById(R.id.switch_settings_social_tumblr);
        swPinterest = (Switch) findViewById(R.id.switch_settings_social_pinter);

        swFace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //TODO 연동
                } else {
                    //TODO 연동 해제
                }
            }
        });
        swTwitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //TODO 연동
                } else {
                    //TODO 연동 해제
                }
            }
        });
        swTumblr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //TODO 연동
                } else {
                    //TODO 연동 해제
                }
            }
        });
        swPinterest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //TODO 연동
                } else {
                    //TODO 연동 해제
                }
            }
        });


    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_settings_social_back:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_social_complete:
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_settings_social_facebook:
                swFace.setChecked(!swFace.isChecked());
                break;
            case R.id.lay_settings_social_twitter:
                swTwitter.setChecked(!swTwitter.isChecked());
                break;
            case R.id.lay_settings_social_tumblr:
                swTumblr.setChecked(!swTumblr.isChecked());
                break;
            case R.id.lay_settings_social_pinterest:
                swPinterest.setChecked(!swPinterest.isChecked());
                break;
        }
    }
}
