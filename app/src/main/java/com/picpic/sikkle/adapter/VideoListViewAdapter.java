package com.picpic.sikkle.adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.video_list_demo.adapter.holders.VideoViewHolder;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.BaseVideoItem;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.TimelineVideoItem;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by devmin on 2016-04-21.
 */
public class VideoListViewAdapter extends BaseAdapter {

    private final VideoPlayerManager mVideoPlayerManager;
    private final List<BaseVideoItem> mList;
    SparseArray<WeakReference<View>> viewArray;
    private final Context mContext;
    private final List<View> vList;
    private ImageLoader imageLoader;

    public VideoListViewAdapter(VideoPlayerManager videoPlayerManager, Context context, List<BaseVideoItem> list) {
        mVideoPlayerManager = videoPlayerManager;
        mContext = context;
        this.viewArray = new SparseArray<WeakReference<View>>();
        mList = list;
        vList = new ArrayList<>();
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        if (viewArray != null && viewArray.get(position) != null) {
//            convertView = viewArray.get(position).get();
//            if (convertView != null){
//                BaseVideoItem videoItem = mList.get(position);
//                videoItem.update(position, (VideoViewHolder) convertView.getTag(), mVideoPlayerManager);
//                return convertView;
//            }
//        }

//        try{
        BaseVideoItem videoItem = mList.get(position);

//        if (vList.size() - 1 >= position) {
//            convertView = vList.get(position);
//        } else {
//            convertView = videoItem.createView(parent, mContext.getResources().getDisplayMetrics().widthPixels);
//            vList.add(position, convertView);
//        }
//            if (convertView == null) {
        convertView = videoItem.createView(parent, mContext.getResources().getDisplayMetrics().widthPixels);
//            }

//            VideoViewHolder holder = (VideoViewHolder) convertView.getTag();
//        holder.mCover.setImageResource(R.drawable.non_p);
//        try{
//            TimelineVideoItem item = (TimelineVideoItem) videoItem;
//            String url = item.getUrl();
//
//            imageLoader.DisplayImage(url, holder.mCover, false);
//        }catch (ClassCastException e){
//
//        }

//        BoundableOfflineImageView bimv = (BoundableOfflineImageView)convertView.findViewById(R.id.bimv_recycler_row_timeline);

//        imageLoader.DisplayImage(AppController.URL + urls.get(position).replace("_2.gif", ".jpg"), bimv, false);

//        mVideoPlayerManager.resetMediaPlayer();
        videoItem.update(position, (VideoViewHolder) convertView.getTag(), mVideoPlayerManager);

//        if (position % 11 == 0) {
//            mVideoPlayerManager.resetMediaPlayer();
//        }
//        }finally {
//            viewArray.put(position, new WeakReference<View>(convertView));
//        }

        return convertView;
    }

}