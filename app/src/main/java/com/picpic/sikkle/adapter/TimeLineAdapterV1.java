/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdView;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapterV1 extends ArrayAdapter<TimelineItem> {
    //    LayoutInflater m_LayoutInflater = null;
    Context con;
    TimelineResult m_ResultList;
    int gifHeight = 0, gifWidth = 0;
    int lastListCount = 0;
    SurfaceTexture mSurfaceTexture;
    Activity ac;
    VideoLoader videoLoader;
    //    SparseArray<WeakReference<View>> viewArray;
    public static boolean isStop = false;
    private NativeAd nativeAd;
    private List<WeakReference<View>> mRecycleList = new ArrayList<WeakReference<View>>();
//    View inflate;

    public TimeLineAdapterV1(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        con = ctx;
        ac = (Activity) con;
        m_ResultList = (TimelineResult) modles;
//        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
//        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        inflate = m_LayoutInflater.inflate(R.layout.row_timeline_v1, null);
        videoLoader = new VideoLoader(ctx);

    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    private static class TimelineViewHolder {
        public LinearLayout layBlank;
        public VView vv;
        //        com.low
        //        public TextureView vv;
        public BoundableOfflineImageView bimv;

        public CircleImageView cimvPro;

        public LinearLayout imvLike;

        public LinearLayout imvComment;
        public LinearLayout imvMore;

        public FrameLayout layContent;

        public Button btnShare;

        public TextView tvUserId;
        public TextView tvBody;
        public TextView tvTags;

        public TextView tvLikeCnt;
        public TextView tvComCnt;
        public TextView tvPlayCnt;

        public LikeView like;

    }

    //MediaPlayer를 TimelineAdapterV1에 2개만들고 position이 홀수냐 짝수냐에따라서 각각 설정하고
    //MediaPlayer를 재활용해서 써본다.

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        if(position % 4 != 0){
        if (m_ResultList.get(position).isAd()) {

            LayoutInflater m_LayoutInflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = m_LayoutInflater.inflate(R.layout.ad_row, null);

            nativeAd = new NativeAd(getContext(), "1610072682575169_1668032706779166");
            final View finalConvertView = convertView;
            nativeAd.setAdListener(new AdListener() {

                @Override
                public void onError(Ad ad, AdError error) {
                    Log.e("error", error.getErrorMessage() + " / " + error.getErrorCode());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (ad != nativeAd) {
                        return;
                    }
                    Log.e("face book ad",  "fbad / " + nativeAd.getAdBody());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesLinkUrl());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdSocialContext());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdSubtitle());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdTitle());
                    Log.e("face book ad", "fbad / " + nativeAd.getId());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesIcon().getUrl());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdCoverImage().getUrl());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdIcon().getUrl());
                    Log.e("face book ad", "fbad / " + nativeAd.getAdStarRating().getValue() + "");
                    Log.e("face book ad", "fbad / " + nativeAd.getAdViewAttributes().getAutoplay() + "");

//                    LinearLayout nativeAdContainer = (LinearLayout) finalConvertView.findViewById(R.id.lay_ad_row);
//                    TextView tv = (TextView)finalConvertView.findViewById(R.id.tv_ad_row);
//                    tv.setText(nativeAd.getAdTitle());
//
//                    nativeAd.registerViewForInteraction(nativeAdContainer);

                    View adView = NativeAdView.render(getContext(), nativeAd, NativeAdView.Type.HEIGHT_100);

                    // Add adView to the layout
                    LinearLayout templateContainer = (LinearLayout) finalConvertView.findViewById(R.id.lay_ad_row);
                    templateContainer.addView(adView);
                }

                @Override
                public void onAdClicked(Ad ad) {

                }
            });

            nativeAd.loadAd();

            return convertView;
        } else {
            final TimelineViewHolder holder;

            if (convertView == null) {

                LayoutInflater m_LayoutInflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = m_LayoutInflater.inflate(R.layout.row_timeline_v1, null);

                holder = new TimelineViewHolder();

                holder.layBlank = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_blank);

//            holder.vv = (TextureView) convertView.findViewById(R.id.vv_timeline_v1);
                holder.vv = (VView) convertView.findViewById(R.id.vv_timeline_v1);

                holder.bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_timeline_v1);
//
//            lastVV = holder.vv;
//                lastVV = (VView) convertView.findViewById(R.id.vv_timeline_v1);

                holder.cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_v1);

                holder.imvLike = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_like);

                holder.imvComment = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_comment);
                holder.imvMore = (LinearLayout) convertView.findViewById(R.id.imv_timeline_v1_more);

                holder.layContent = (FrameLayout) convertView.findViewById(R.id.lay_timeline_v1_content);

                holder.btnShare = (Button) convertView.findViewById(R.id.btn_timeline_v1_share);

                holder.tvUserId = (TextView) convertView.findViewById(R.id.tv_timeline_v1_id);
                holder.tvBody = (TextView) convertView.findViewById(R.id.tv_timeline_v1_body);
                holder.tvTags = (TextView) convertView.findViewById(R.id.tv_timeline_v1_tags);

                holder.tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_like_count);
                holder.tvComCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_comment_count);
                holder.tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_play_count);

                holder.like = (LikeView) convertView.findViewById(R.id.imv_timeline_v1_like);

                convertView.setTag(holder);

            } else {
                holder = (TimelineViewHolder) convertView.getTag();
            }

//            if (lastVV != null) {
//                holder.vv = lastVV;
//            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (position == lastListCount || position == lastListCount + 1) {
                    holder.vv.setVisibility(View.VISIBLE);
                } else {
                    holder.vv.setVisibility(View.GONE);
                }
            } else {
                holder.vv.setVisibility(View.VISIBLE);
            }

            if (position == 0) {
                holder.layBlank.setVisibility(View.INVISIBLE);
            } else {
                holder.layBlank.setVisibility(View.GONE);
            }

            holder.like.setVisibility(View.INVISIBLE);

//            final TimelineItem model = m_ResultList.get(position);
            final TimelineItem model = m_ResultList.get(position);

            final int pos = position;

            String tempUrl = model.getUrl();

            Log.e("tempUrl", "asjfbqlkwf / " + tempUrl + " / " + m_ResultList.size());

            int lastIdx = tempUrl.lastIndexOf("_");

            final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

            holder.cimvPro.setTag(model.getOwnerId());
//        new ImgLoad(holder, position, AppController.URL + model.getPro_url()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

            final VView tempV = holder.vv;
            final LinearLayout tempImvLike = holder.imvLike;
            final LikeView tempLike = holder.like;
            final CircleImageView tempCimv = holder.cimvPro;

            holder.layContent.setOnTouchListener(new OnSwipeTouchListener(con) {
                @Override
                public void onClick() {
                    super.onClick();

                    if (tempV.isPlaying()) {
                        tempV.pause();
                    } else {
                        tempV.start();
                    }
                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();
                    if (tempImvLike.isSelected()) {
                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (m_ResultList.get(pos).isLike()) {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                    m_ResultList.get(pos).setIsLike(false);
                                    update();
                                }
                            }
                        });
                        MinUtils.like(true, (Activity) con, model.getPsot_id());

                    } else {

                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!m_ResultList.get(pos).isLike()) {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                    m_ResultList.get(pos).setIsLike(true);
                                    update();
                                }
                            }
                        });
                        tempLike.startAni();
                        MinUtils.like(false, (Activity) con, model.getPsot_id());

                    }

                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(con, GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    con.startActivity(i);
                }
            });
//
//
//            if (model.getPro_url().equals("")) {
//                cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
//            } else {
//                cimvPro.setImageURLString(AppController.URL + model.getPro_url());
//            }
//

            holder.tvUserId.setText(model.getId());

            if (model.getBody().equals("") || model.getBody().length() <= 0) {
                holder.tvBody.setVisibility(View.GONE);
            } else {
                holder.tvBody.setVisibility(View.VISIBLE);
            }

            holder.tvBody.setText(model.getBody());

            MinUtils.setCount(holder.tvLikeCnt, model.getLikeCount());
            MinUtils.setCount(holder.tvComCnt, model.getComCount());
            MinUtils.setCount(holder.tvPlayCnt, model.getPlayCnt());

            MinUtils.setTags(con, holder.tvTags, model.getTags());

            holder.imvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tempImvLike.isSelected()) {
                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (m_ResultList.get(pos).isLike()) {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                    m_ResultList.get(pos).setIsLike(false);
                                    update();
                                }
                            }
                        });
                        MinUtils.like(true, (Activity) con, model.getPsot_id());

                    } else {

                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!m_ResultList.get(pos).isLike()) {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                    m_ResultList.get(pos).setIsLike(true);
                                    update();
                                }
                            }
                        });
                        tempLike.startAni();
                        MinUtils.like(false, (Activity) con, model.getPsot_id());

                    }
                }
            });

            if (model.isLike()) {
                holder.imvLike.setSelected(true);
            } else {
                holder.imvLike.setSelected(false);
            }

            holder.imvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || tempImvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });

            holder.btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent i = new Intent(con, SharePopupActivity.class);
                    Intent i = new Intent(con, SharePopUp.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", gifWidth);
                    i.putExtra("h", gifHeight);
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });

            holder.imvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            holder.tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || tempImvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });
            holder.tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    con.startActivity(i);
                }
            });

            holder.cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = tempCimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            holder.tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = tempCimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            int w = model.getWidth();
            int h = model.getHeight();

            int height = 0;

            try {
                height = h * MinUtils.screenWidth / w;
            } catch (ArithmeticException e) {

            }

            holder.vv.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));

//        holder.bimv.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
//
            holder.vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
//                m_ResultList.get(pos).setPlayCnt(model.getPlayCnt() + 1);
//                update();
                    AppController.playCountExe.execute(new Runnable() {
                        @Override
                        public void run() {
                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                        }
                    });
//                    vv.seekTo(0);
                    tempV.seekTo(0);
                    tempV.start();
                }
            });

            final String tempUrl2 = model.getUrl().replace(".gif", ".mp4");
            String tempThubUrl = model.getUrl().replace("_2.gif", ".jpg");

//        AppController.videoGIFLoader.DisplayVideo(AppController.URL + tempUrl2, holder.bimv, true);
            AppController.imageLoader.DisplayImage(AppController.URL + model.getPro_url(), holder.cimvPro, true);
            AppController.imageLoader.DisplayImage(AppController.URL + tempThubUrl, holder.bimv, true);
//            AppController.videoLoader.DisplayVideo(AppController.URL + tempUrl2, holder.vv, true);

//        if(position % 2 == 0){
//            holder.mp1.reset();
//            holder.mp1.release();
//            holder.mp1 = new MediaPlayer();
////            holder.mp1.setSurface(new Surface(holder.vv.getmSurfaceTexture()));
//            AppController.videoLoader.DisplayVideo(AppController.URL + tempUrl2, holder.mp1, true);
//            holder.vv.initVideoView(holder.mp1);
//        }else{
//            holder.mp2.reset();
//            holder.mp2.release();
//            holder.mp2 = new MediaPlayer();
////            holder.mp2.setSurface(new Surface(holder.vv.getmSurfaceTexture()));
//            AppController.videoLoader.DisplayVideo(AppController.URL + tempUrl2, holder.mp2, true);
//            holder.vv.initVideoView(holder.mp2);
//        }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 0);
                    con.startActivity(i);
                }
            });
//        }else{
////            if (mRecycleList.size() <= parent.getChildCount()) {
////                Log.e(this + "", "size:" + mRecycleList.size());
////                throw e;
////            }
////            Log.w(this + "", e.toString());
//            recycleHalf();
//            System.gc();
//            return getView(position, convertView, parent);
//        }
//
//        mRecycleList.add(new WeakReference<View>(convertView));

            return convertView;
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void update() {
//        viewArray.clear();
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

//    public void recycle() {
//        RecycleUtils.recursiveRecycle(mRecycleList);
//    }
//
//    //만들었던 뷰 목록 중 반을 지우는 메소드
//    public void recycleHalf() {
//        int halfSize = mRecycleList.size() / 2;
//        List<WeakReference<View>> recycleHalfList = mRecycleList.subList(0,
//                halfSize);
//        RecycleUtils.recursiveRecycle(recycleHalfList);
//        for (int i = 0; i < halfSize; i++)
//            mRecycleList.remove(0);
//    }

    public void setIDX(int idx) {
        this.lastListCount = idx;
        update();
    }

}
