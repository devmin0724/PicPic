package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.beans.NewTimelineItem;
import com.picpic.sikkle.beans.NewTimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView;
import com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by devmin on 2016-05-06.
 */
public class NewTimelineVideoAdapter extends ArrayAdapter<NewTimelineItem> {


    private static final String TAG = "SampleAdapter";
    private Context con;
    private NewTimelineResult models;
    SparseArray<WeakReference<View>> viewArray;
    private ImageFullLoader imageFullLoader;
    private VideoLoader videoLoader;
    private ImageLoader imageLoader;

    private final LayoutInflater mLayoutInflater;
    private final Activity ac;
    private int playCnt;

    public NewTimelineVideoAdapter(Context context, int resource, NewTimelineResult objects) {
        super(context, resource, objects);
        mLayoutInflater = LayoutInflater.from(context);
        this.con = context;
        this.models = objects;
        ac = (Activity) context;
        this.viewArray = new SparseArray<WeakReference<View>>(objects.size());
        imageFullLoader = new ImageFullLoader(context);
        imageLoader = new ImageLoader(context);
        videoLoader = new VideoLoader(context);

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null) {
                final NewTimelineItem model = models.get(position);

                FrameLayout layContent = (FrameLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_content);

                final TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_play_count);

                final VView vv = new VView(getContext());
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                vv.setLayoutParams(params);

                layContent.addView(vv, 1);

                videoLoader.DisplayVideo(AppController.URL + model.getUrlVideo(), vv, false);

                vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        AppController.playCountExe.execute(new Runnable() {
                            @Override
                            public void run() {
                                new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                            }
                        });

                        MinUtils.setCount(tvPlayCnt, model.getPlayCnt() + 1);
                        model.setPlayCnt(model.getPlayCnt() + 1);
                    }
                });

                return convertView;
            }

        }

        try {
            convertView = mLayoutInflater.inflate(R.layout.row_new_timeline_video, parent, false);

            LinearLayout layTopBlank = (LinearLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_top_blank);
            LinearLayout layRow = (LinearLayout) convertView.findViewById(R.id.lay_recycler_row_timeline);
            TextView mTitle = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_body);
            BoundableOfflineImageView mCover = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_recycler_row_timeline);
            final LikeView likeView = (LikeView) convertView.findViewById(R.id.imv_recycler_row_timeline_like);
            FrameLayout layContent = (FrameLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_content);
            final CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_recycler_row_timeline);
            TextView tvId = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_id);
            LinearLayout layMore = (LinearLayout) convertView.findViewById(R.id.imv_recycler_row_timeline_more);
            TextView tvTags = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_tags);
            final LinearLayout layLike = (LinearLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_like);
            final TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_like_count);
            LinearLayout layComment = (LinearLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_comment);
            TextView tvComCnt = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_comment_count);
            LinearLayout layPlayCnt = (LinearLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_play_count);
            final TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_play_count);
            Button btnShare = (Button) convertView.findViewById(R.id.btn_recycler_row_timeline_share);

            LinearLayout layAd = (LinearLayout) convertView.findViewById(R.id.lay_ad_row);
            TextView tvAdTitle = (TextView) convertView.findViewById(R.id.tv_ad_row_title);
            BoundableOfflineImageView bimvAd = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_ad_row);
            ImageView imvAdIcon = (ImageView) convertView.findViewById(R.id.imv_ad_row);
            TextView tvAdSubTitle = (TextView) convertView.findViewById(R.id.tv_ad_row_sub_title);
            Button btnAdAction = (Button) convertView.findViewById(R.id.btn_ad_row_do_it);

            final NewTimelineItem model = models.get(position);

            String tempThubUrl = AppController.URL + model.getUrlThumb();
            imageFullLoader.DisplayImage(tempThubUrl, mCover, false);

            if (position == 0) {
                layTopBlank.setVisibility(View.VISIBLE);
            } else {
                layTopBlank.setVisibility(View.GONE);
            }

            layRow.setVisibility(View.VISIBLE);
            layAd.setVisibility(View.GONE);

            likeView.setVisibility(View.INVISIBLE);


            layContent.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
                @Override
                public void onClick() {
                    super.onClick();

//                    if (vv.isPlaying()) {
//                        vv.pause();
//                    } else {
//                        vv.start();
//                    }
                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();
                    if (layLike.isSelected()) {
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                tvLikeCnt.setText(lc - 1 + "");
                                layLike.setSelected(false);
                                model.setLikeCount(lc - 1);
                            }
                        });
                        MinUtils.like(true, ac, model.getPsot_id());

                    } else {

                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                tvLikeCnt.setText(lc + 1 + "");
                                layLike.setSelected(true);
                                model.setLikeCount(lc + 1);
                            }
                        });
                        likeView.startAni();
                        MinUtils.like(false, ac, model.getPsot_id());

                    }

                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(ac, GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrlGif());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    ac.startActivity(i);
                }
            });

            cimv.setTag(model.getOwnerId());

            imageLoader.DisplayImage(AppController.URL + model.getPro_url(), cimv, false);
            tvId.setText(model.getId());

            MinUtils.setCount(tvLikeCnt, model.getLikeCount());
            MinUtils.setCount(tvComCnt, model.getComCount());
            MinUtils.setCount(tvPlayCnt, model.getPlayCnt());

            MinUtils.setTags(ac, tvTags, model.getTags());

            if (model.isLike()) {
                layLike.setSelected(true);
            } else {
                layLike.setSelected(false);
            }

            layLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (layLike.isSelected()) {
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                tvLikeCnt.setText(lc - 1 + "");
                                layLike.setSelected(false);
                                model.setLikeCount(lc - 1);
                            }
                        });
                        MinUtils.like(true, ac, model.getPsot_id());

                    } else {

                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                tvLikeCnt.setText(lc + 1 + "");
                                layLike.setSelected(true);
                                model.setLikeCount(lc + 1);
                            }
                        });
                        likeView.startAni();
                        MinUtils.like(false, ac, model.getPsot_id());

                    }
                }
            });

            layComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    ac.startActivity(i);
                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SharePopUp.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrlGif());
                    i.putExtra("w", 0);
                    i.putExtra("h", 0);
                    ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });

            layMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrlGif());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    ac.startActivity(i);
                }
            });
            tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    ac.startActivity(i);
                }
            });

            cimv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });

            tvId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });


            if (model.getBody().equals("") || model.getBody().length() <= 0) {
                mTitle.setVisibility(View.GONE);
            } else {
                mTitle.setVisibility(View.VISIBLE);
            }

            mTitle.setText(model.getBody());

            int w = model.getWidth();
            int h = model.getHeight();

            int height = 0;

            try {
                height = h * MinUtils.screenWidth / w;
            } catch (ArithmeticException e) {

            }

            imageLoader.DisplayImage(AppController.URL + model.getPro_url(), cimv, true);
//            Glide.with(ac).load(AppController.URL + model.getPro_url()).into(cimv);

//            mCover.setVisibility(View.VISIBLE);
//            mImageLoader.load(Uri.parse(AppController.URL + tempThubUrl)).into(mCover);
//            imageFullLoader.DisplayImage(AppController.URL + tempThubUrl, mCover, false);

            layRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    ac.startActivity(i);
                }
            });

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        final NewTimelineItem model = models.get(position);

        FrameLayout layContent = (FrameLayout) convertView.findViewById(R.id.lay_recycler_row_timeline_content);

        final TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_recycler_row_timeline_play_count);

        final VView vv = new VView(getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        vv.setLayoutParams(params);

        layContent.addView(vv, 1);

        videoLoader.DisplayVideo(AppController.URL + model.getUrlVideo(), vv, false);

        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                AppController.playCountExe.execute(new Runnable() {
                    @Override
                    public void run() {
                        new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                    }
                });

                MinUtils.setCount(tvPlayCnt, model.getPlayCnt() + 1);
                model.setPlayCnt(model.getPlayCnt() + 1);
            }
        });

        return convertView;
    }
}
