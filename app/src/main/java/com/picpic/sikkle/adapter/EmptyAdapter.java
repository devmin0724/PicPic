/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.MinUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by devmin on 2016-02-16.
 */
public class EmptyAdapter extends ArrayAdapter<String> {

    ArrayList<String> arr;
    Context con;
    LayoutInflater m_LayoutInflater;

    public EmptyAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.arr = (ArrayList<String>) objects;
        this.con = context;
        this.m_LayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = m_LayoutInflater.inflate(R.layout.page_mypage_null, null);
        }

        TextView tv = MinUtils.ViewHolder.get(convertView, R.id.tv_empty_list);

        tv.setText(arr.get(position));


        return convertView;
    }
}
