/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.BitmapUtils;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapterV3 extends ArrayAdapter<TimelineItem> {
    LayoutInflater m_LayoutInflater = null;
    Context con;
    TimelineResult m_ResultList;
    int gifHeight = 0, gifWidth = 0;
    int lastListCount = 0;
    Activity ac;
    VideoLoader videoLoader;
    public static boolean isStop = false;

    public TimeLineAdapterV3(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        con = ctx;
        ac = (Activity) con;
        m_ResultList = (TimelineResult) modles;
//        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        videoLoader = new VideoLoader(ctx);

    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = m_LayoutInflater.inflate(R.layout.row_timeline_v1, null);

        LinearLayout layBlank = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_blank);

        VView vv = (VView) convertView.findViewById(R.id.vv_timeline_v1);

        CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_v1);

        LinearLayout imvLike = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_like);

        LinearLayout imvComment = (LinearLayout) convertView.findViewById(R.id.lay_timeline_v1_comment);
        LinearLayout imvMore = (LinearLayout) convertView.findViewById(R.id.imv_timeline_v1_more);

        FrameLayout layContent = (FrameLayout) convertView.findViewById(R.id.lay_timeline_v1_content);

        Button btnShare = (Button) convertView.findViewById(R.id.btn_timeline_v1_share);

        TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_timeline_v1_id);
        TextView tvBody = (TextView) convertView.findViewById(R.id.tv_timeline_v1_body);
        TextView tvTags = (TextView) convertView.findViewById(R.id.tv_timeline_v1_tags);

        TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_like_count);
        TextView tvComCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_comment_count);
        TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_timeline_v1_play_count);

        LikeView like = (LikeView) convertView.findViewById(R.id.imv_timeline_v1_like);

        vv.setVisibility(View.INVISIBLE);

        if (position == 0) {
            layBlank.setVisibility(View.INVISIBLE);
        } else {
            layBlank.setVisibility(View.GONE);
        }

        like.setVisibility(View.INVISIBLE);

//            final TimelineItem model = m_ResultList.get(position);
        final TimelineItem model = m_ResultList.get(position);

        final int pos = position;

        String tempUrl = model.getUrl();

        Log.e("tempUrl", "asjfbqlkwf / " + tempUrl + " / " + m_ResultList.size());

        int lastIdx = tempUrl.lastIndexOf("_");

        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

        cimvPro.setTag(model.getOwnerId());

//        new ImgLoad(holder, position, AppController.URL + model.getPro_url()).execute();
//        new ImgLoad(cimvPro, position, AppController.URL + model.getPro_url()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

        final VView tempV = vv;
        final LinearLayout tempImvLike = imvLike;
        final LikeView tempLike = like;
        final CircleImageView tempCimv = cimvPro;

        vv.setOnTouchListener(new OnSwipeTouchListener(con) {
            @Override
            public void onClick() {
                super.onClick();

                if (tempV.isPlaying()) {
                    tempV.pause();
                } else {
                    tempV.start();
                }
            }

            @Override
            public void onDoubleClick() {
                super.onDoubleClick();
                if (tempImvLike.isSelected()) {
                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc - 1);
                            m_ResultList.get(pos).setIsLike(false);
                            update();
                        }
                    });
                    MinUtils.like(true, (Activity) con, model.getPsot_id());

                } else {

                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc + 1);
                            m_ResultList.get(pos).setIsLike(true);
                            update();
                        }
                    });
                    tempLike.startAni();
                    MinUtils.like(false, (Activity) con, model.getPsot_id());

                }

            }

            @Override
            public void onLongClick() {
                super.onLongClick();
                Intent i = new Intent(con, GIFDownloadConfirmActivity.class);
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("body", model.getBody());
                i.putExtra("pid", model.getPsot_id());
                con.startActivity(i);
            }
        });
//
//
//            if (model.getPro_url().equals("")) {
//                cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
//            } else {
//                cimvPro.setImageURLString(AppController.URL + model.getPro_url());
//            }
//

        tvUserId.setText(model.getId());

        if (model.getBody().equals("") || model.getBody().length() <= 0) {
            tvBody.setVisibility(View.GONE);
        } else {
            tvBody.setVisibility(View.VISIBLE);
        }

        tvBody.setText(model.getBody());

        MinUtils.setCount(tvLikeCnt, model.getLikeCount());
        MinUtils.setCount(tvComCnt, model.getComCount());
        MinUtils.setCount(tvPlayCnt, model.getPlayCnt());

        MinUtils.setTags(con, tvTags, model.getTags());

        imvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempImvLike.isSelected()) {
                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc - 1);
                            m_ResultList.get(pos).setIsLike(false);
                            update();
                        }
                    });
                    MinUtils.like(true, (Activity) con, model.getPsot_id());

                } else {

                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            m_ResultList.get(pos).setLikeCount(lc + 1);
                            m_ResultList.get(pos).setIsLike(true);
                            update();
                        }
                    });
                    tempLike.startAni();
                    MinUtils.like(false, (Activity) con, model.getPsot_id());

                }
            }
        });

        if (model.isLike()) {
            imvLike.setSelected(true);
        } else {
            imvLike.setSelected(false);
        }

        imvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || tempImvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                con.startActivity(i);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Intent i = new Intent(con, SharePopupActivity.class);
                Intent i = new Intent(con, SharePopUp.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("body", model.getBody());
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("w", gifWidth);
                i.putExtra("h", gifHeight);
                ((Activity) con).startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
            }
        });

        imvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, Popup2Activity.class);
                i.putExtra("email", model.getOwnerId());
                if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                } else {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                }
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("title", model.getId());
                i.putExtra("body", model.getBody());
                ((Activity) con).startActivityForResult(i, TimeLineActivity.MORE_RETURN);
            }
        });

        tvComCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || tempImvLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                con.startActivity(i);
            }
        });
        tvLikeCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, UserListActivity.class);
                i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i.putExtra("postId", model.getPsot_id());
                con.startActivity(i);
            }
        });

        cimvPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = tempCimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    con.startActivity(new Intent(con, MyPageV2Activity.class));
                    ((Activity) con).finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(con, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        con.startActivity(i);
                    }
                }
            }
        });

        tvUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = tempCimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    con.startActivity(new Intent(con, MyPageV2Activity.class));
                    ((Activity) con).finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(con, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        con.startActivity(i);
                    }
                }
            }
        });

        int w = model.getWidth();
        int h = model.getHeight();

        int height = 0;

        try {
            height = h * MinUtils.screenWidth / w;
        } catch (ArithmeticException e) {

        }

        vv.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
//
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
//                    m_ResultList.get(pos).setPlayCnt(model.getPlayCnt() + 1);
//                    update();
                AppController.playCountExe.execute(new Runnable() {
                    @Override
                    public void run() {
                        new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                    }
                });
//                    vv.seekTo(0);
                tempV.seekTo(0);
                tempV.start();
            }
        });

        String tempUrl2 = model.getUrl().replace(".gif", ".mp4");
//            videoLoader.DisplayVideo(AppController.URL + tempUrl2, vv, false);
//        new VideoLoad(holder, position, AppController.URL + tempUrl2).execute();
//        new VideoLoad(vv, position, AppController.URL + tempUrl2).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 0);
                con.startActivity(i);
            }
        });

        return convertView;
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void update() {
//        viewArray.clear();
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public void stopLoading() {
        isStop = true;
    }

//    public static class ViewHolder {
//        @SuppressWarnings("unchecked")
//        public static <T extends View> T get(View view, int id) {
//            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
//            if (viewHolder == null) {
//
//                viewHolder = new SparseArray<View>();
//                view.setTag(viewHolder);
//            }
//            View childView = viewget(id);
//            if (childView == null) {
//                childView = view.findViewById(id);
//                viewput(id, childView);
//            }
//
//            return (T) childView;
//        }
//
//        public static int getIndex(View v, int id) {
//            SparseArray<View> viewHolder = (SparseArray<View>) v.getTag();
//            if (viewHolder == null) {
//
//                viewHolder = new SparseArray<View>();
//                v.setTag(viewHolder);
//            }
//
//            return viewindexOfKey(id);
//        }
//
//    }

    private class ImgLoad extends AsyncTask<Integer, Void, Bitmap> {

        private CircleImageView cimv;
        private int position;
        private String url;

        public ImgLoad(CircleImageView cimv, int position, String url) {
            this.cimv = cimv;
            this.position = position;
            this.url = url;
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {

            Bitmap bm = null;

            HttpURLConnection conn = null;
            OutputStream os = null;
            byte[] buffer = new byte[50 * 1024];
            try {
                File f = AppController.fileCache.getFile(url);
                URL imageUrl = null;
                imageUrl = new URL(url);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);

                try {
                    InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);

                    FileOutputStream fos = new FileOutputStream(f);
                    int red2 = 0;
                    byte[] buf2 = new byte[50 * 1024];
                    while ((red2 = bis.read(buf2)) != -1) {
                        fos.write(buf2, 0, red2);
                    }
                    fos.flush();
                    fos.close();

                } catch (FileNotFoundException e) {
                    return null;
                }

                bm = BitmapUtils.decodeFile(f.getAbsolutePath());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (!isStop) {
                if (url.equals("")) {
                    cimv.setImageResource(R.drawable.icon_timeline_noprofile);
                } else {
                    if (bitmap != null) {
                        cimv.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }

    private class VideoLoad extends AsyncTask<Integer, Void, File> {
        private VView vv;
        private int position;
        private String url;

        public VideoLoad(VView vv, int position, String url) {
            this.vv = vv;
            this.position = position;
            this.url = url;
        }

        @Override
        protected File doInBackground(Integer... params) {
            File f = AppController.fileCache.getFile(url);

            File ff = null;
            if (f != null && f.exists()) {
                ff = f;
            }
            if (ff != null) {
                return ff;
            }
            try {
                File file = null;
                Log.e("Download url", url);
                URL fileUrl = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) fileUrl
                        .openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                InputStream is = conn.getInputStream();
                OutputStream os = new FileOutputStream(f);
                CopyStream(is, os);
                os.close();
                file = f;
                return file;
            } catch (Exception ex) {
                Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
                return null;
            }
        }

        public void CopyStream(InputStream is, OutputStream os) {
            final int buffer_size = 1024;
            try {
                byte[] bytes = new byte[buffer_size];
                for (; ; ) {
                    int count = is.read(bytes, 0, buffer_size);
                    if (count == -1)
                        break;
                    os.write(bytes, 0, count);
                }
            } catch (Exception ex) {
                Log.e("", "CopyStream catch Exception...");
            }
        }

        @Override
        protected void onPostExecute(File file) {
            if (!isStop) {
                vv.setVideoPath(file.getAbsolutePath());
                vv.setVisibility(View.VISIBLE);
                vv.start();
//                    videoLoader.DisplayVideo(url, vv, false);
            }
        }
    }
}
