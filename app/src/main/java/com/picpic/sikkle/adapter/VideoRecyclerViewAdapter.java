/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.NewTimelineItem;
import com.picpic.sikkle.beans.NewTimelineResult;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.utils.video_list_demo.adapter.holders.VideoViewHolder;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.BaseVideoItem;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by danylo.volokh on 9/20/2015.
 */
public class VideoRecyclerViewAdapter extends RecyclerView.Adapter<VideoViewHolder> {

    private static final String TAG = "SampleAdapter";
    private Context con;
    private TimelineResult models;
    SparseArray<WeakReference<View>> viewArray;
    private ImageFullLoader imageFullLoader;
    private VideoLoader videoLoader;
    private ImageLoader imageLoader;

    private final LayoutInflater mLayoutInflater;
    private final Activity ac;
    private int playCnt;

    public VideoRecyclerViewAdapter(Context context, TimelineResult objects){
        mLayoutInflater = LayoutInflater.from(context);
        this.con = context;
        this.models = objects;
        ac = (Activity) context;
        this.viewArray = new SparseArray<WeakReference<View>>(objects.size());
        imageFullLoader = new ImageFullLoader(context);
        imageLoader = new ImageLoader(context);
        videoLoader = new VideoLoader(context);
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
//        BaseVideoItem videoItem = mList.get(position);
//        View resultView = videoItem.createView(viewGroup, mContext.getResources().getDisplayMetrics().widthPixels);
//        return new VideoViewHolder(resultView);
        View convertView = mLayoutInflater.inflate(R.layout.row_new_timeline_video, null, false);

        return new VideoViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final VideoViewHolder viewHolder, int position) {

        View convertView = mLayoutInflater.inflate(R.layout.row_new_timeline_video, null, false);

        final TimelineItem model = models.get(position);

        String tempThubUrl = AppController.URL + model.getUrl().replace("_2.gif", ".jpg");
        imageFullLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);

        if (position == 0) {
            viewHolder.layTopBlank.setVisibility(View.VISIBLE);
        } else {
            viewHolder.layTopBlank.setVisibility(View.GONE);
        }

        viewHolder.layRow.setVisibility(View.VISIBLE);
        viewHolder.layAd.setVisibility(View.GONE);

        viewHolder.likeView.setVisibility(View.INVISIBLE);

        viewHolder.layContent.setOnTouchListener(new OnSwipeTouchListener(con) {
            @Override
            public void onClick() {
                super.onClick();

//                    if (vv.isPlaying()) {
//                        vv.pause();
//                    } else {
//                        vv.start();
//                    }
            }

            @Override
            public void onDoubleClick() {
                super.onDoubleClick();
                if (viewHolder.layLike.isSelected()) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            viewHolder.tvLikeCnt.setText(lc - 1 + "");
                            viewHolder.layLike.setSelected(false);
                            model.setLikeCount(lc - 1);
                        }
                    });
                    MinUtils.like(true, ac, model.getPsot_id());

                } else {

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            viewHolder.tvLikeCnt.setText(lc + 1 + "");
                            viewHolder.layLike.setSelected(true);
                            model.setLikeCount(lc + 1);
                        }
                    });
                    viewHolder.likeView.startAni();
                    MinUtils.like(false, ac, model.getPsot_id());

                }

            }

            @Override
            public void onLongClick() {
                super.onLongClick();
                Intent i = new Intent(ac, GIFDownloadConfirmActivity.class);
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("body", model.getBody());
                i.putExtra("pid", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        viewHolder.cimv.setTag(model.getOwnerId());

        imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimv, false);
        viewHolder.tvId.setText(model.getId());

        MinUtils.setCount(viewHolder.tvLikeCnt, model.getLikeCount());
        MinUtils.setCount(viewHolder.tvComCnt, model.getComCount());
        MinUtils.setCount(viewHolder.tvPlayCnt, model.getPlayCnt());

        MinUtils.setTags(ac, viewHolder.tvTags, model.getTags());

        if (model.isLike()) {
            viewHolder.layLike.setSelected(true);
        } else {
            viewHolder.layLike.setSelected(false);
        }

        viewHolder.layLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.layLike.isSelected()) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            viewHolder.tvLikeCnt.setText(lc - 1 + "");
                            viewHolder.layLike.setSelected(false);
                            model.setLikeCount(lc - 1);
                        }
                    });
                    MinUtils.like(true, ac, model.getPsot_id());

                } else {

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            viewHolder.tvLikeCnt.setText(lc + 1 + "");
                            viewHolder.layLike.setSelected(true);
                            model.setLikeCount(lc + 1);
                        }
                    });
                    viewHolder.likeView.startAni();
                    MinUtils.like(false, ac, model.getPsot_id());

                }
            }
        });

        viewHolder.layComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                ac.startActivity(i);
            }
        });

        viewHolder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SharePopUp.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("body", model.getBody());
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("w", 0);
                i.putExtra("h", 0);
                ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
            }
        });

        viewHolder.layMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, Popup2Activity.class);
                i.putExtra("email", model.getOwnerId());
                if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                } else {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                }
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("title", model.getId());
                i.putExtra("body", model.getBody());
                ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
            }
        });

        viewHolder.tvComCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                ac.startActivity(i);
            }
        });
        viewHolder.tvLikeCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, UserListActivity.class);
                i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i.putExtra("postId", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        viewHolder.cimv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = viewHolder.cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });

        viewHolder.tvId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = viewHolder.cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });

        if (model.getBody().equals("") || model.getBody().length() <= 0) {
            viewHolder.mTitle.setVisibility(View.GONE);
        } else {
            viewHolder.mTitle.setVisibility(View.VISIBLE);
        }

        viewHolder.mTitle.setText(model.getBody());

        int w = model.getWidth();
        int h = model.getHeight();

        int height = 0;

        try {
            height = h * MinUtils.screenWidth / w;
        } catch (ArithmeticException e) {

        }

        imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimv, true);
//            Glide.with(ac).load(AppController.URL + model.getPro_url()).into(cimv);

//            mCover.setVisibility(View.VISIBLE);
//            mImageLoader.load(Uri.parse(AppController.URL + tempThubUrl)).into(mCover);
//            imageFullLoader.DisplayImage(AppController.URL + tempThubUrl, mCover, false);

        viewHolder.layRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        final VView vv = new VView(con);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        vv.setLayoutParams(params);

        viewHolder.layContent.addView(vv, 1);

        videoLoader.DisplayVideo(AppController.URL + model.getUrl().replace(".gif", ".mp4"), vv, false);

        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                AppController.playCountExe.execute(new Runnable() {
                    @Override
                    public void run() {
                        new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                    }
                });

                MinUtils.setCount(viewHolder.tvPlayCnt, model.getPlayCnt() + 1);
                model.setPlayCnt(model.getPlayCnt() + 1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
