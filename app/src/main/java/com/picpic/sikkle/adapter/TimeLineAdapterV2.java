/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.lang.ref.WeakReference;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapterV2 extends BaseAdapter {
    SparseArray<WeakReference<View>> viewArray;

    public TimeLineAdapterV2(SparseArray<WeakReference<View>> viewArray) {
        this.viewArray = viewArray;
    }

    @Override
    public int getCount() {
        return viewArray.size();
    }

    @Override
    public Object getItem(int position) {
        return viewArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = viewArray.get(position).get();

        return viewArray.get(position).get();
    }

    public void update(SparseArray<WeakReference<View>> viewArray) {
        this.viewArray.clear();
        this.viewArray = viewArray;
        notifyDataSetChanged();
    }

}
