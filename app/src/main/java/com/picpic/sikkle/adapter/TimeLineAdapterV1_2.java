/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.picpic.sikkle.beans.TimelineResult;

import java.lang.ref.WeakReference;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapterV1_2 extends BaseAdapter {
    SparseArray<WeakReference<View>> viewArray;
    public Context con;
    public TimelineResult m_ResultList;

    public TimeLineAdapterV1_2(Context con, SparseArray<WeakReference<View>> viewArray){
        this.con = con;
        this.viewArray = viewArray;
    }

    @Override
    public int getCount() {
        return viewArray.size();
    }

    @Override
    public Object getItem(int position) {
        return viewArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

//        if (viewArray != null && viewArray.get(position) != null) {
//            convertView = viewArray.get(position).get();
//            if (convertView != null) {
//                return convertView;
//            }
//        }
//        return convertView;
        return viewArray.get(position).get();

    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void refresh(SparseArray<WeakReference<View>> viewArray){
        this.viewArray.clear();
        this.viewArray = viewArray;
        notifyDataSetChanged();
    }

    public void update() {
        viewArray.clear();
        notifyDataSetChanged();
    }
}
