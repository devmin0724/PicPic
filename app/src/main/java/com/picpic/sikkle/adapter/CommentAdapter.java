package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.CommentItem;
import com.picpic.sikkle.beans.CommentResult;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by devmin on 2016-05-02.
 */
public class CommentAdapter extends ArrayAdapter<CommentItem> {

    private Context con;
    private CommentResult models;
    SparseArray<WeakReference<View>> viewArray;
    private ImageLoader imageLoader;

    static class ViewHolder {
        BoundableOfflineImageView bimv;
    }

    private final LayoutInflater mLayoutInflater;

    public CommentAdapter(final Context context, final int textViewResourceId, CommentResult models) {
        super(context, textViewResourceId, models);
        mLayoutInflater = LayoutInflater.from(context);
        this.con = context;
        imageLoader = new ImageLoader(context);
        this.models = models;
        this.viewArray = new SparseArray<WeakReference<View>>(models.size());
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getType();//current menu type
//        return position % 2;//current menu type
    }

    @Override
    public int getViewTypeCount() {
        return 2;//menu type count
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null)
                return convertView;
        }

        try {
            convertView = mLayoutInflater.inflate(R.layout.row_comment_v1, parent, false);

            final CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_row_comment_v1);

            TextView tvId = (TextView) convertView.findViewById(R.id.tv_row_comment_v1_id);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tv_row_comment_v1_body);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tv_row_comment_v1_time);
            final TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_row_comment_v1_like_count);

            final ImageView imvLike = (ImageView) convertView.findViewById(R.id.imv_row_comment_v1_like);

            LinearLayout layLike = (LinearLayout) convertView.findViewById(R.id.lay_row_comment_v1_like);

            final CommentItem model = models.get(position);

            if (!model.isNon()) {
                String profile = AppController.URL + model.getUrl();
                imageLoader.DisplayImage(profile, cimv, false);

                tvId.setText(model.getId());
                cimv.setTag(model.getEmail());
                tvBody.setText(model.getBody());

                tvId.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = cimv.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            con.startActivity(new Intent(con, MyPageV2Activity.class));
                            ((Activity) con).finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(con, UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                con.startActivity(i);
                            }
                        }
                    }
                });

                cimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = cimv.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            con.startActivity(new Intent(con, MyPageV2Activity.class));
                            ((Activity) con).finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(con, UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                con.startActivity(i);
                            }
                        }
                    }
                });

                if (model.isLike()) {
                    imvLike.setSelected(true);
                } else {
                    imvLike.setSelected(false);
                }

                String tempTime = "";
                String tempDate = model.getTime();
                Date now = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                final String tempC = sdf.format(now);

                ArrayList<String> nows = new ArrayList<>();
                ArrayList<String> posts = new ArrayList<>();
//                Log.e("aa", tempDate.substring(0,2));
//                Log.e("aa", tempDate.substring(2,4));

                for (int i = 0; i < tempDate.length(); i = i + 2) {
                    nows.add(tempC.substring(i, i + 2));
                    posts.add(tempDate.substring(i, i + 2));
                }

//                for (int i = 0; i < nows.size(); i++) {
//                    Log.e("now" + i, nows.get(i));
//                    Log.e("post" + i, posts.get(i));
//                }

                int tempNavi = 0;

                for (int i = 0; i < nows.size(); i++) {
                    if (!nows.get(i).equals(posts.get(i))) {
                        tempNavi = i;
                        break;
                    }
                }

                int tempNow = Integer.parseInt(nows.get(tempNavi));
                int tempPosts = Integer.parseInt(posts.get(tempNavi));

                switch (tempNavi) {
                    case 0:

                        break;
                    case 1:
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_year);
                        } else {
                            tempTime = tempPosts - tempNow + con.getResources().getString(R.string.timeline_before_year);
                        }
                        break;
                    case 2:
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_month);
                        } else {
                            tempTime = (12 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_month);
                        }
                        break;
                    case 3:
                        Calendar c = Calendar.getInstance();
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_day);
                        } else {
                            c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
                            int lastDay = c.getActualMaximum(Calendar.DATE);
                            tempTime = (lastDay - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_day);
                        }
                        break;
                    case 4:
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_hour);
                        } else {
                            tempTime = (24 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_hour);
                        }
                        break;
                    case 5:
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_minute);
                        } else {
                            tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_minute);
                        }
                        break;
                    case 6:
                        if (tempNow > tempPosts) {
                            tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_second);
                        } else {
                            tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_second);
                        }
                        break;
                }

                tvTime.setText(tempTime);

                MinUtils.setCount(tvLikeCnt, model.getLikeCnt());

                String commentsText = tvBody.getText().toString();

                ArrayList<int[]> calloutSpans = MinUtils.getSpans(commentsText, '@');

                SpannableString commentsContent = new SpannableString(commentsText);

                for (int i = 0; i < calloutSpans.size(); i++) {
                    int[] span = calloutSpans.get(i);
                    int calloutStart = span[0];
                    int calloutEnd = span[1];

                    commentsContent.setSpan(new CallTag(getContext()),
                            calloutStart,
                            calloutEnd, 0);

                }

                tvBody.setMovementMethod(LinkMovementMethod.getInstance());
                tvBody.setText(commentsContent);
                tvBody.setHighlightColor(con.getResources().getColor(android.R.color.transparent));

                layLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!model.isLike()) {
                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imvLike.setSelected(true);
                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt() + 1);
                                    models.get(position).setLikeCnt(model.getLikeCnt() + 1);
                                    models.get(position).setIsLike(true);
                                    update();
                                }
                            });

                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(String result) {
                                    try {
                                        JSONObject jd = new JSONObject(result);
                                        if (jd.getInt("result") == 0) {

                                        } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                            Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        } else {
                                            ((Activity) con).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    imvLike.setSelected(false);
                                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt() - 1);
//                                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt()+1);
                                                    models.get(position).setLikeCnt(model.getLikeCnt() - 1);
                                                    models.get(position).setIsLike(false);
                                                    update();
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {

                                    }
                                }
                            };

                            Map<String, String> params2 = new HashMap<>();

                            params2.put("post_reply_id", model.getCom_id());
                            params2.put("click_id", AppController.getSp().getString("email", ""));
                            params2.put("like_form", "R");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params2, 302, stmLikeResult);
                            AppController.apiDataTaskNew.execute();

                        } else {
                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    imvLike.setSelected(false);
                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt() - 1);
                                    models.get(position).setLikeCnt(model.getLikeCnt() - 1);
                                    models.get(position).setIsLike(false);
                                    update();
                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(String result) {
                                    try {
                                        JSONObject jd = new JSONObject(result);
                                        if (jd.getInt("result") == 0) {


                                        } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                            Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        } else {
                                            ((Activity) con).runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt()+1);
                                                    imvLike.setSelected(true);
                                                    MinUtils.setCount(tvLikeCnt, model.getLikeCnt() + 1);
                                                    models.get(position).setLikeCnt(model.getLikeCnt() + 1);
                                                    models.get(position).setIsLike(true);
                                                    update();
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {

                                    }
                                }
                            };

                            Map<String, String> params1 = new HashMap<>();

                            params1.put("post_reply_id", model.getCom_id());
                            params1.put("click_id", AppController.getSp().getString("email", ""));
                            params1.put("like_form", "R");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params1, 303, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    }
                });
                convertView.setTag(model.getType());
            } else {
                convertView.setVisibility(View.GONE);
            }

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        return convertView;
    }

    public void update() {
        viewArray.clear();
        notifyDataSetChanged();
    }

}