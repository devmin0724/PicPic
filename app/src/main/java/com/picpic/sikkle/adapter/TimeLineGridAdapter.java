package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageRun;
import com.picpic.sikkle.widget.BoundableOfflineImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineGridAdapter extends ArrayAdapter<TimelineItem> {
    LayoutInflater m_LayoutInflater = null;
    SparseArray<WeakReference<View>> viewArray;
    Context con;
    public static TimelineResult m_ResultList;

    public TimeLineGridAdapter(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        this.con = ctx;
        m_ResultList = (TimelineResult) modles;
        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int pos = position;

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null)
                return convertView;
        }

        try {
            convertView = m_LayoutInflater.inflate(R.layout.row_grid, null);

            BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_grid_row);

            final TimelineItem model = getItem(pos);

            String tempStr = model.getUrl();
            int underbarCount = tempStr.lastIndexOf("_");
            String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

            String tempUrl2 = model.getUrl();

            int lastIdx = tempUrl2.lastIndexOf("_");

            final String tempName = tempUrl2.substring(0, lastIdx) + ".jpg";
            downloadGifbImage(bimv, AppController.URL + tempUrl);
//            bimv.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName, (Activity) con);
//            bimv.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
            bimv.setTag(model.getPsot_id());

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }
        return convertView;
    }

    public void downloadGifbImage(ImageView imageView, String url) {

        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun((Activity) con, url, imageView, image, file_size2, 1);
//
//        if (AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(5);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }
}