/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTagV1;
import com.picpic.sikkle.widget.VView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapterV1Category extends ArrayAdapter<TimelineItem> {
    LayoutInflater m_LayoutInflater = null;
    SparseArray<WeakReference<View>> viewArray;
    public static Context con;
    public static TimelineResult m_ResultList;
    int gifHeight = 0, gifWidth = 0;
    int lastListCount = 0;
    Activity ac;
    VideoLoader videoLoader;

    public TimeLineAdapterV1Category(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        con = ctx;
        ac = (Activity) con;
        m_ResultList = (TimelineResult) modles;
        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        videoLoader = new VideoLoader(ctx);
    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null) {
                return convertView;
            }
        }

        try {
            if (convertView == null) {
                convertView = m_LayoutInflater.inflate(R.layout.row_timeline_v1, null);
            }

            LinearLayout layBlank = ViewHolder.get(convertView, R.id.lay_timeline_v1_blank);

            layBlank.setVisibility(View.GONE);

//            final VideoView vv = ViewHolder.get(convertView, R.id.vv_timeline_v1);
            final VView vv = ViewHolder.get(convertView, R.id.vv_timeline_v1);

            final CircleImageView cimvPro = ViewHolder.get(convertView, R.id.cimv_timeline_v1);

            final LinearLayout imvLike = ViewHolder.get(convertView, R.id.lay_timeline_v1_like);

            final LinearLayout imvComment = ViewHolder.get(convertView, R.id.lay_timeline_v1_comment);
            LinearLayout imvMore = ViewHolder.get(convertView, R.id.imv_timeline_v1_more);

            FrameLayout layContent = ViewHolder.get(convertView, R.id.lay_timeline_v1_content);

            Button btnShare = ViewHolder.get(convertView, R.id.btn_timeline_v1_share);

            TextView tvUserId = ViewHolder.get(convertView, R.id.tv_timeline_v1_id);
            TextView tvBody = ViewHolder.get(convertView, R.id.tv_timeline_v1_body);
            TextView tvTags = ViewHolder.get(convertView, R.id.tv_timeline_v1_tags);

            TextView tvLikeCnt = ViewHolder.get(convertView, R.id.tv_timeline_v1_like_count);
            TextView tvComCnt = ViewHolder.get(convertView, R.id.tv_timeline_v1_comment_count);
            TextView tvPlayCnt = ViewHolder.get(convertView, R.id.tv_timeline_v1_play_count);

            final ImageView like = ViewHolder.get(convertView, R.id.imv_timeline_v1_like);

            like.setVisibility(View.GONE);

//            final TimelineItem model = m_ResultList.get(position);
            final TimelineItem model = m_ResultList.get(position);

            final int pos = position;

            String tempUrl = model.getUrl();

            Log.e("tempUrl", "asjfbqlkwf / " + tempUrl + " / " + m_ResultList.size());

            int lastIdx = tempUrl.lastIndexOf("_");

            final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

//            downloadThumbImage(bimv, AppController.URL + tempName);
//            downloadThumbVideoImage(vv, AppController.URL + tempName);

            final View finalConvertView = convertView;
            vv.setOnTouchListener(new OnSwipeTouchListener(con) {
                @Override
                public void onClick() {
                    super.onClick();

//                    if (vv.isPlaying()) {
//                        vv.pause();
//                    } else {
//                        vv.start();
//                    }
////
//
//                    Drawable d = bimv.getDrawable();
//                    if (d instanceof GifDrawable) {
//                        try {
//                            GifDrawable g = (GifDrawable) d;
//                            if (g.isPlaying()) {
//                                g.pause();
//                            } else {
//                                g.start();
//                            }
//                        } catch (NullPointerException e) {
//
//                        }
//                    }

                }

                @Override
                public void onDoubleClick() {
//                    super.onDoubleClick();

                    try {
                        if (imvLike.isSelected()) {
                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                    m_ResultList.get(pos).setIsLike(false);
                                    imvLike.setSelected(true);
                                    update();

                                }
                            });
                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        Log.e("result onselect", result);
                                        final JSONObject jd = new JSONObject(result);
                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        int lc = model.getLikeCount();
                                                        m_ResultList.get(pos).setLikeCount(lc + 1);
                                                        m_ResultList.get(pos).setIsLike(true);
                                                        update();

                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                            AppController.apiDataTaskNew.execute();


                        } else {

                            final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    like.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    like.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            like.startAnimation(ani1);

                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                    m_ResultList.get(pos).setIsLike(true);
                                    update();

                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        final JSONObject jd = new JSONObject(result);

                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {
                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        int lc = model.getLikeCount();
                                                        m_ResultList.get(pos).setLikeCount(lc - 1);
                                                        m_ResultList.get(pos).setIsLike(false);
                                                        update();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    } catch (NullPointerException e) {

                    }

                    return;
                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(con, GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    con.startActivity(i);
                }
            });


            if (model.getPro_url().equals("")) {
                cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
            } else {
                cimvPro.setImageURLString(AppController.URL + model.getPro_url());
            }

            cimvPro.setTag(model.getOwnerId());

            tvUserId.setText(model.getId());
            tvBody.setText(model.getBody());

            if (model.getTags().length() <= 0) {
                tvTags.setVisibility(View.GONE);
            } else {
                tvTags.setVisibility(View.VISIBLE);
                String tempTags = model.getTags();
                Log.e("tempTags1", tempTags);
                if (tempTags.substring(0).toString().equals(",")) {
                    tempTags = tempTags.substring(1, tempTags.length());
                    Log.e("tempTags2", tempTags);
                }

                tempTags = "#" + tempTags;
                tempTags = tempTags.replace(",", "  #");

                Log.e("tempTags3", tempTags);
                tvTags.setText(tempTags);
            }

//            tvLikeCnt.setText(con.getResources().getString(R.string.like) + " " + model.getLikeCount() + con.getResources().getString(R.string.timeline_count));
//            tvComCnt.setText(con.getResources().getString(R.string.comment) + " " + model.getComCount() + con.getResources().getString(R.string.timeline_count));
            DecimalFormat countDF = new DecimalFormat("0.#");

            String likeCount, comCount, playCount;

//            int likeCount = model.getLikeCount();
//            int comCount = model.getComCount();

            if (model.getLikeCount() >= 1000) {
                likeCount = countDF.format(model.getLikeCount() / 1000) + "K";
            } else {
                likeCount = model.getLikeCount() + "";
            }

            if (model.getComCount() >= 1000) {
                comCount = countDF.format(model.getComCount() / 1000) + "K";
            } else {
                comCount = model.getComCount() + "";
            }

            if (model.getPlayCnt() >= 1000) {
                playCount = countDF.format(model.getPlayCnt() / 1000) + "K";
            } else {
                playCount = model.getPlayCnt() + "";
            }

            tvLikeCnt.setText(likeCount);
            tvComCnt.setText(comCount);
            tvPlayCnt.setText(playCount);

//            double playCnt = (double) model.getPlayCnt();
//            DecimalFormat df = new DecimalFormat("#,##0");
//            tvPlayCnt.setText(df.format(playCnt) + "");

            if (model.isLike()) {
                imvLike.setSelected(true);
            } else {
                imvLike.setSelected(false);
            }

            String tagsText = tvTags.getText().toString();

            ArrayList<int[]> hashtagSpans2 = getSpans(tagsText, '#');

            SpannableString commentsContent2 = new SpannableString(tagsText);

            for (int i = 0; i < hashtagSpans2.size(); i++) {
                int[] span = hashtagSpans2.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent2.setSpan(new HashTagV1(con),
                        hashTagStart, hashTagEnd, 0);
            }

            tvTags.setMovementMethod(LinkMovementMethod.getInstance());
            tvTags.setText(commentsContent2);
            tvTags.setHighlightColor(con.getResources().getColor(android.R.color.transparent));
//            tvTags.setHighlightColor(0xfff5f5f5);

            imvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imvLike.isSelected()) {
                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc - 1);
                                m_ResultList.get(pos).setIsLike(false);
                                update();
                            }
                        });
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result onselect", result);
                                    final JSONObject jd = new JSONObject(result);
                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();

                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                                    m_ResultList.get(pos).setIsLike(true);
                                                    update();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {
                        final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                        final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                        final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                        ani1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                like.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                like.startAnimation(ani2);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.startAnimation(ani3);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani3.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        like.startAnimation(ani1);

                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc + 1);
                                m_ResultList.get(pos).setIsLike(true);
                                update();
                            }
                        });
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result nonselect", result);
                                    final JSONObject jd = new JSONObject(result);

                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                                    m_ResultList.get(pos).setIsLike(false);
                                                    update();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                }
            });
            imvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });

            btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent i = new Intent(con, SharePopupActivity.class);
                    Intent i = new Intent(con, SharePopUp.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", gifWidth);
                    i.putExtra("h", gifHeight);
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });

            imvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });
            tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    con.startActivity(i);
                }
            });

            cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            int w = model.getWidth();
            int h = model.getHeight();

            int height = h * MinUtils.screenWidth / w;

            vv.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));

//            vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    AppController.playCountExe.execute(new Runnable() {
//                        @Override
//                        public void run() {
//                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
//                        }
//                    });
//                    vv.seekTo(0);
//                    vv.start();
//                }
//            });

            String tempUrl2 = model.getUrl().replace(".gif", ".mp4");
//            videoLoader.DisplayVideo(AppController.URL + tempUrl2, vv, false);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 0);
                    con.startActivity(i);
                }
            });

//            videoLoader.DisplayVideo(AppController.URL+tempUrl2, vv, false);
/* TODO 동영상
            vv.setZOrderOnTop(false);

            vv.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));

            String tempUrl2 = model.getUrl().replace(".gif", ".mp4");
            vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    AppController.playCountExe.execute(new Runnable() {
                        @Override
                        public void run() {
                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                        }
                    });
                    vv.seekTo(0);
                    vv.start();
                }
            });

            MinUtils.downloadMP4((Activity) con, vv, AppController.URL + tempUrl2);

 */

//                vv.setVisibility(View.
// ppController.URL + tempUrl2);

//            bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
//                @Override
//                public void onImageChanged(Drawable d) {
//
////                    vv.get
//
//                    vv.setVisibility(View.VISIBLE);
//
//                    vv.setLayoutParams(new FrameLayout.LayoutParams(bimv.getWidth(), bimv.getHeight()));
//
//                    String tempUrl = model.getUrl().replace(".gif", ".mp4");
//                    vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mp) {
//                            vv.seekTo(0);
//                            vv.start();
//                        }
//                    });
//                    vv.setVideoPath(AppController.URL + tempUrl);
//                    vv.start();
//
////                    downloadMP4(vv, AppController.URL + tempUrl);
//
////                    downloadMP4(vv, "http://www.tapgifs.com/test.mp4");
////                    if (pos % 2 == 1) {
////                        downloadMP4(vv, "http://www.tapgifs.com/test.mp4");
////                    } else {
////                        downloadMP4(vv, "http://www.tapgifs.com/test2.mp4");
////                    }
//
//
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        return convertView;

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public void update() {
        viewArray.clear();
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {

                viewHolder = new SparseArray<View>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }

            return (T) childView;
        }
    }
}
