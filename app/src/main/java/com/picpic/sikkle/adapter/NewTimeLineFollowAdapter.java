package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.NewGridTimelineActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.SharePopupForTimelineActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class NewTimeLineFollowAdapter extends ArrayAdapter<TimelineItem> {
    public static Context con;
    public static TimelineResult m_ResultList;
    LayoutInflater m_LayoutInflater = null;
    public static SparseArray<WeakReference<View>> viewArray;
    int gifHeight = 0, gifWidth = 0;
    int lastListCount = 0;
    Activity ac;
    String[] ms;
    String[] ds;

    public NewTimeLineFollowAdapter(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        con = ctx;
        ac = (Activity) con;
        m_ResultList = (TimelineResult) modles;
        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ms = new String[]{
                con.getResources().getString(R.string.m1), con.getResources().getString(R.string.m2), con.getResources().getString(R.string.m3), con.getResources().getString(R.string.m4), con.getResources().getString(R.string.m5), con.getResources().getString(R.string.m6), con.getResources().getString(R.string.m7), con.getResources().getString(R.string.m8), con.getResources().getString(R.string.m9), con.getResources().getString(R.string.m10), con.getResources().getString(R.string.m11), con.getResources().getString(R.string.m12)
        };

        ds = new String[]{
                con.getResources().getString(R.string.d1), con.getResources().getString(R.string.d2), con.getResources().getString(R.string.d3), con.getResources().getString(R.string.d4), con.getResources().getString(R.string.d5), con.getResources().getString(R.string.d6), con.getResources().getString(R.string.d7), con.getResources().getString(R.string.d8), con.getResources().getString(R.string.d9), con.getResources().getString(R.string.d10), con.getResources().getString(R.string.d11), con.getResources().getString(R.string.d12), con.getResources().getString(R.string.d13), con.getResources().getString(R.string.d14), con.getResources().getString(R.string.d15), con.getResources().getString(R.string.d16), con.getResources().getString(R.string.d17), con.getResources().getString(R.string.d18), con.getResources().getString(R.string.d19), con.getResources().getString(R.string.d20), con.getResources().getString(R.string.d21), con.getResources().getString(R.string.d22), con.getResources().getString(R.string.d23), con.getResources().getString(R.string.d24), con.getResources().getString(R.string.d25), con.getResources().getString(R.string.d26), con.getResources().getString(R.string.d27), con.getResources().getString(R.string.d28), con.getResources().getString(R.string.d29), con.getResources().getString(R.string.d30), con.getResources().getString(R.string.d31)
        };
    }

    public static void downloadThumbImage(ImageView imageView, String url) {

//        Log.e("downloadThumbImageStart", url);

//        if(AppController.thumbThread != null && AppController.thumbThread.isAlive()){
//            AppController.thumbThread.interrupt();
//        }

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.thumbRun = new MinThumbImageRun((Activity) con, url, imageView, image, file_size2, 1);
//
//        if (AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
//
//        AppController.thumbExe.submit(AppController.thumbRun);

//        AppController.thumbThread = new Thread(AppController.thumbRun);
//        AppController.thumbThread.start();
//        AppController.thumbThread = new MinImageThread((Activity) con, url, imageView, image, file_size2, 1);
//        AppController.thumbThread.run();

    }

    public static void downloadGifbImage(ImageView imageView, String url) {

//        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun((Activity) con, url, imageView, image, file_size2, 1);
//
//        if (AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null) {
                try {
                    BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.imv_timeline_row_img);
                    final TimelineItem model = m_ResultList.get(position);

                    String tempUrl = model.getUrl();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                    downloadThumbImage(bimv, AppController.URL + tempName);

                    return convertView;
                } catch (StringIndexOutOfBoundsException e) {
                    return convertView;
                } catch (NullPointerException e) {
                    return convertView;
                }
            }
        }

        try {
            convertView = m_LayoutInflater.inflate(R.layout.row_new_time_line, null);
            final BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.imv_timeline_row_img);
            bimv.setTag(0);

            final CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_row_pro);
//            final CircleImageView cimvComPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_row_comment_pro);

            final LinearLayout imvLike = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_like);
            LinearLayout imvComment = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_comment);
            LinearLayout imvShare = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_share);
            LinearLayout layTop = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_top);

            TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_timeline_row_id);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tv_timeline_row_time);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tv_timeline_row_body);
            final TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_like_cnt);
            TextView tvComCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_comment_cnt);
            TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_play_cnt);
            TextView tvTop = (TextView) convertView.findViewById(R.id.tv_timeline_row_top);

            ImageView like = (ImageView) convertView.findViewById(R.id.imv_timeline_row_like);
            like.setVisibility(View.GONE);
            final ImageView follow = (ImageView) convertView.findViewById(R.id.imv_timeline_row_follow);

            final TimelineItem model = m_ResultList.get(position);

            final int pos = getPosition(model);

            String tempUrl = model.getUrl();

            int lastIdx = tempUrl.lastIndexOf("_");

            final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

            bimv.measure(model.getWidth(), model.getHeight());

            downloadThumbImage(bimv, AppController.URL + tempName);


            if (model.isFollow()) {
                follow.setSelected(true);
            } else {
                follow.setSelected(false);
            }

//            if(pos%2 == 0){
//                layTop.setVisibility(View.GONE);
//            }else{
//                layTop.setVisibility(View.VISIBLE);
//            }

            if (model.getLikeCount() >= 3 && model.getLikeCount() < 9) {
                layTop.setVisibility(View.VISIBLE);
                tvTop.setText(con.getResources().getString(R.string.timeline_3likes));
            } else if (model.getLikeCount() >= 10) {
                layTop.setVisibility(View.VISIBLE);
                tvTop.setText(con.getResources().getString(R.string.timeline_10likes));
            } else {
                layTop.setVisibility(View.GONE);
            }

//                new ImageTask(position, AppController.URL + model.getUrl(), AppController.URL + tempName, 2, bimv)
//                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

//            try {
//                if (pos == m_ResultList.size() - 1) {
//                    layBlank.setVisibility(View.VISIBLE);
//                } else {
//                    layBlank.setVisibility(View.GONE);
//                }
//            } catch (NullPointerException e) {
//                layBlank.setVisibility(View.GONE);
//            }

            follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(follow.isSelected()){
                        follow.setSelected(false);
                    }else{
                        follow.setSelected(true);

                    }
                    try {
                        StringTransMethod stmFollow = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        if (jd.getString("follow").equals("Y")) {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + model.getOwnerId()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                            for (int i = 0; i < m_ResultList.size(); i++) {
                                                if (m_ResultList.get(i).getOwnerId().equals(model.getOwnerId())) {
                                                    m_ResultList.get(i).setIsFollow(true);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(i).get();
                                                    ImageView check = (ImageView) v.findViewById(R.id.imv_timeline_row_follow);
                                                    check.setSelected(true);
//                                                    notifyDataSetChanged();
                                                }
                                            }
//                                            follow.setSelected(true);
                                        } else if (jd.getString("follow").equals("N")) {
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + model.getOwnerId()).build());
                                            AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                            AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                            for (int i = 0; i < m_ResultList.size(); i++) {
                                                if (m_ResultList.get(i).getOwnerId().equals(model.getOwnerId())) {
                                                    m_ResultList.get(i).setIsFollow(false);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(i).get();
                                                    ImageView check = (ImageView) v.findViewById(R.id.imv_timeline_row_follow);
                                                    check.setSelected(false);
                                                }
                                            }
//                                            follow.setSelected(false);
                                        } else {
                                            Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        return;
                                    } else {

                                    }
                                } catch (JSONException e) {

                                } catch (NullPointerException e) {

                                }
                            }
                        };
                        JSONObject tempJ = new JSONObject();
                        tempJ.put("email", model.getOwnerId());

                        JSONArray jarr = new JSONArray();
                        jarr.put(tempJ);

                        Map<String, String> params = new HashMap<>();

                        params.put("myId", AppController.getSp().getString("email", ""));
                        params.put("email", jarr.toString());
                        params.put("type", "N");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 402, stmFollow);
                        AppController.apiDataTaskNew.execute();

                    } catch (JSONException e) {

                    } catch (NullPointerException ex) {

                    }
                }
            });

            bimv.setOnTouchListener(new OnSwipeTouchListener(con) {
                @Override
                public void onClick() {
                    super.onClick();
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 0);
                    con.startActivity(i);

//                    Intent i = new Intent(con, SinglePostSlideActivity.class);
//                    i.putExtra("navi", 1);
//                    i.putExtra("now", pos);
//                    i.putExtra("tr", m_ResultList);
//                    AppController.tempTR = m_ResultList;
//                    con.startActivity(i);
//                    Drawable d = bimv.getDrawable();
//                    if (d instanceof GifDrawable) {
//                        try {
//                            GifDrawable g = (GifDrawable) d;
//                            if (g.isPlaying()) {
//                                g.pause();
//                            } else {
//                                g.start();
//                            }
//
//                        } catch (NullPointerException e) {
//
//                        }
//                    }

                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();

                    try {
                        if (imvLike.isSelected()) {
                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = model.getLikeCount();
                                        m_ResultList.get(pos).setLikeCount(lc - 1);
                                        m_ResultList.get(pos).setIsLike(false);
                                        notifyDataSetChanged();

                                        View v = viewArray.get(position).get();

                                        LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                        TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                        tempLay.setSelected(false);
//                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                        tempTv.setText("" + (lc - 1));
                                        viewArray.setValueAt(position, new WeakReference<View>(v));
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
//                                        Log.e("result onselect", result);
                                        final JSONObject jd = new JSONObject(result);
                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = model.getLikeCount();
                                                            m_ResultList.get(pos).setLikeCount(lc + 1);
                                                            m_ResultList.get(pos).setIsLike(true);
                                                            notifyDataSetChanged();

                                                            View v = viewArray.get(position).get();

                                                            LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                            TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                            tempLay.setSelected(true);
//                                                        tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));

                                                            tempTv.setText("" + (lc + 1));
                                                            viewArray.setValueAt(position, new WeakReference<View>(v));
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }

                                            }
                                        });
                                    } catch (JSONException e) {

                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                            AppController.apiDataTaskNew.execute();


                        } else {

                            View v = viewArray.get(pos).get();

                            final ImageView like = (ImageView) v.findViewById(R.id.imv_timeline_row_like);

                            final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                            like.setVisibility(View.VISIBLE);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    like.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            like.startAnimation(ani1);

                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        int lc = model.getLikeCount();
                                        m_ResultList.get(pos).setLikeCount(lc + 1);
                                        m_ResultList.get(pos).setIsLike(true);
                                        notifyDataSetChanged();

                                        View v = viewArray.get(position).get();

                                        LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                        TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                        tempLay.setSelected(true);
//                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                        tempTv.setText("" + (lc + 1));
                                        viewArray.setValueAt(position, new WeakReference<View>(v));
                                    } catch (ArrayIndexOutOfBoundsException e) {

                                    } catch (NullPointerException e) {

                                    }
                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        final JSONObject jd = new JSONObject(result);

                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                        return;
                                                    } else {
                                                        try {
                                                            int lc = model.getLikeCount();
                                                            m_ResultList.get(pos).setLikeCount(lc - 1);
                                                            m_ResultList.get(pos).setIsLike(false);
                                                            notifyDataSetChanged();

                                                            View v = viewArray.get(position).get();

                                                            LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                            TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                            tempLay.setSelected(false);
//                                                        tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                                            tempTv.setText("" + (lc - 1));
                                                            viewArray.setValueAt(position, new WeakReference<View>(v));
                                                        } catch (ArrayIndexOutOfBoundsException e) {

                                                        } catch (NullPointerException e) {

                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (NullPointerException e) {

                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                    } catch (NullPointerException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    } catch (NullPointerException e) {

                    }
                }

            });

            if (model.getPro_url().equals("")) {
                cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
            } else {
                cimvPro.setImageURLString(AppController.URL + model.getPro_url());
//                downloadProbImage(cimvPro, AppController.URL + model.getPro_url());
//                downloadImage(cimvPro, AppController.URL + model.getPro_url());
//                    new ProfileTask(position, holder, AppController.URL + model.getPro_url())
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

//                    new ImageTask(position, holder, AppController.URL + model.getPro_url(), "", 0)
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            }

            cimvPro.setTag(model.getOwnerId());

            String tempTime = "";

            String tempDate = model.getTime();
            //20151130225233
            int year = Integer.parseInt(tempDate.substring(0, 4));
            int month = Integer.parseInt(tempDate.substring(4, 6));
//            int month = 11;
            int day = Integer.parseInt(tempDate.substring(6, 8));
            int hour = Integer.parseInt(tempDate.substring(8, 10));
            int min = Integer.parseInt(tempDate.substring(10, 12));
            int sec = Integer.parseInt(tempDate.substring(12, 14));

//            Log.e("dd", year + "/" + month + "/" + day + "/" + hour + "/" + min + "/" + sec);

            Calendar tempCal = Calendar.getInstance();
            Calendar tempNowCal = Calendar.getInstance();

            DateFormat stringFormat = new SimpleDateFormat("yyyyMMddHHmmss");

            long nowTime, oldTime, intervalTime;

            Calendar intervalCal = Calendar.getInstance();

            try {
                Date nowDate = stringFormat.parse(tempDate);
                tempCal.setTime(nowDate);

                oldTime = tempCal.getTimeInMillis();
                nowTime = System.currentTimeMillis();

                intervalTime = nowTime - oldTime;

                long it = intervalTime / 1000;

//                Log.e("intervalT", it + "");

                intervalCal.setTimeInMillis(intervalTime);

//                Log.e("interval", intervalCal.get(Calendar.YEAR)
//                        + "/" + (intervalCal.get(Calendar.MONTH))
//                        + "/" + intervalCal.get(Calendar.DAY_OF_MONTH)
//                        + "/" + intervalCal.get(Calendar.HOUR_OF_DAY)
//                        + "/" + intervalCal.get(Calendar.MINUTE)
//                        + "/" + intervalCal.get(Calendar.SECOND)
//                );

                long m = 60;
                long h = 60 * 60;
                long d = 60 * 60 * 24;

                if (it < m) {
                    tempTime = con.getResources().getString(R.string.just_now);
                } else {
                    if (it < h) {
                        tempTime = it / m + con.getResources().getString(R.string.timeline_before_minute);
                    } else {
                        if (it < d) {
                            tempTime = it / h + con.getResources().getString(R.string.timeline_before_hour);
                        } else {
                            if (it < d * 2) {
                                tempTime = con.getResources().getString(R.string.yesterday);
                            } else {
                                tempTime = year + con.getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
                            }
                        }
                    }
                }
//                if (intervalCal.get(Calendar.YEAR) != 1970
//                        || intervalCal.get(Calendar.MONTH) != 0
//                        || intervalCal.get(Calendar.DAY_OF_MONTH) > 1) {
//                    if (intervalCal.get(Calendar.DAY_OF_MONTH) == 1) {
//                        tempTime = con.getResources().getString(R.string.yesterday);
//                    } else {
//                        if (intervalCal.get(Calendar.HOUR_OF_DAY) == 1) {
//                            if (intervalCal.get(Calendar.MINUTE) == 1) {
//                                tempTime = con.getResources().getString(R.string.just_now);
//                            } else {
//                                tempTime = intervalCal.get(Calendar.MINUTE) + con.getResources().getString(R.string.timeline_before_minute);
//                            }
//                        } else {
//                            tempTime = intervalCal.get(Calendar.HOUR_OF_DAY) + con.getResources().getString(R.string.timeline_before_hour);
//                        }
//                    }
//                } else {
//                    tempTime = year + con.getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
//                }

//                Log.e("tempTIme", tempTime);

            } catch (ParseException e) {
                e.printStackTrace();
            }

//            if(tempDOYNow - tempDOY == 1){
//                if(tempHour - hour >= 0){
//                    tempTime = con.getResources().getString(R.string.yesterday);
//                }else{
//                    tempTime = ((24-hour)+tempHour) + con.getResources().getString(R.string.timeline_before_hour);
//                }
//            }else if(tempDOYNow - tempDOY == 0){
//                if(tempHour == hour){
//                    if(tempMin == min){
//
//                    }else{
//                        if(tempMin - min == 1){
//
//                        }else{
//
//                        }
//                    }
//                }else{
//                    if(tempHour - hour == 1 && tempMin < min){
//                        tempTime = ((60-min)+tempMin) +con.getResources().getString(R.string.timeline_before_minute);
//                    }else{
//                        tempTime = (tempHour - hour) + con.getResources().getString(R.string.timeline_before_hour);
//                    }
//                }
//            }else{
//                tempTime = year + con.getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
//            }
//
//            if (tempDOYNow - tempDOY <= 1) {
//
//                if(tempHour == hour){
//                    //
//                }else if(tempHour > hour){
//
//                }else if(tempHour < hour){
//
//                }
//
//
//                if (tempHour < hour ) {
//                    //TODO 하루지남
//                    tempTime = con.getResources().getString(R.string.yesterday);
//                } else {
//                    //TODO 하루 안지남
//                    if (24 - hour - tempHour > 0) {
//                        //TODO 한시간이후
//                        tempTime = 24 - hour - tempHour + con.getResources().getString(R.string.timeline_before_hour);
//                    } else {
//                        if (60 - min - tempMin > 0) {
//                            //TODO 일분이후
//                            tempTime = 60 - min - tempMin + con.getResources().getString(R.string.timeline_before_minute);
//                        } else {
//                            tempTime = 60 - sec - tempSec + con.getResources().getString(R.string.timeline_before_second);
//                        }
//                    }
//                }
//
//            } else {
//                tempTime = year + con.getResources().getString(R.string.year) + " " + ms[month - 1] + " " + ds[day - 1];
//            }

            /*
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String tempC = sdf.format(now);

            ArrayList<String> nows = new ArrayList<>();
            ArrayList<String> posts = new ArrayList<>();

            for (int i = 0; i < tempDate.length(); i = i + 2) {
                nows.add(tempC.substring(i, i + 2));
                posts.add(tempDate.substring(i, i + 2));
            }

            int tempNavi = 0;

            for (int i = 0; i < nows.size(); i++) {
                if (!nows.get(i).equals(posts.get(i))) {
                    tempNavi = i;
                    break;
                }
            }

            int tempNow = Integer.parseInt(nows.get(tempNavi));
            int tempPosts = Integer.parseInt(posts.get(tempNavi));

            switch (tempNavi) {
                case 0:

                    break;
                case 1:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_year);
                    } else {
                        tempTime = tempPosts - tempNow + con.getResources().getString(R.string.timeline_before_year);
                    }
                    break;
                case 2:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_month);
                    } else {
                        tempTime = (12 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_month);
                    }
                    break;
                case 3:
                    Calendar c = Calendar.getInstance();
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_day);
                    } else {
                        c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
                        int lastDay = c.getActualMaximum(Calendar.DATE);
                        tempTime = (lastDay - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_day);
                    }
                    break;
                case 4:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_hour);
                    } else {
                        tempTime = (24 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_hour);
                    }
                    break;
                case 5:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_minute);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_minute);
                    }
                    break;
                case 6:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_second);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_second);
                    }
                    break;
            }
            */
            tvUserId.setText(model.getId());
            tvTime.setText(tempTime);
            tvBody.setText(model.getBody());
//            tvLikeCnt.setText(con.getResources().getString(R.string.like) + " " + model.getLikeCount() + con.getResources().getString(R.string.timeline_count));
//            tvComCnt.setText(con.getResources().getString(R.string.comment) + " " + model.getComCount() + con.getResources().getString(R.string.timeline_count));
            tvLikeCnt.setText(model.getLikeCount() + "");
            tvComCnt.setText(model.getComCount() + "");
            double playCnt = (double) model.getPlayCnt();
            DecimalFormat df = new DecimalFormat("#,##0");
            tvPlayCnt.setText(df.format(playCnt) + "");

            if (model.isLike()) {
                imvLike.setSelected(true);
            } else {
                imvLike.setSelected(false);
            }

            String commentsText = tvBody.getText().toString();

            ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
            ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
            ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

            SpannableString commentsContent = new SpannableString(commentsText);

            for (int i = 0; i < hashtagSpans.size(); i++) {
                int[] span = hashtagSpans.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent.setSpan(new HashTag(con),
                        hashTagStart, hashTagEnd, 0);
            }

            for (int i = 0; i < calloutSpans.size(); i++) {
                int[] span = calloutSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];

                commentsContent.setSpan(new CallTag(con),
                        calloutStart,
                        calloutEnd, 0);

            }

            for (int i = 0; i < andSpans.size(); i++) {
                int[] span = andSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];


                AndTag at = new AndTag(con);
                at.setTagId(model.getAndTagId());
                commentsContent.setSpan(at,
                        calloutStart,
                        calloutEnd, 0);

            }

            tvBody.setMovementMethod(LinkMovementMethod.getInstance());
            tvBody.setText(commentsContent);
            tvBody.setHighlightColor(con.getResources().getColor(android.R.color.transparent));

            imvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imvLike.isSelected()) {
                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc - 1);
                                m_ResultList.get(pos).setIsLike(false);
                                notifyDataSetChanged();

                                View v = viewArray.get(position).get();

                                LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                tempLay.setSelected(false);
//                                tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                tempTv.setText("" + (lc - 1));
                                tvLikeCnt.setText("" + (lc - 1));
                                viewArray.setValueAt(position, new WeakReference<View>(v));
                            }
                        });
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
//                                    Log.e("result onselect", result);
                                    final JSONObject jd = new JSONObject(result);
                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {

                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    return;
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                                    m_ResultList.get(pos).setIsLike(true);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(position).get();

                                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                    tempLay.setSelected(true);
//                                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                                    tempTv.setText("" + (lc + 1));
                                                    tvLikeCnt.setText("" + (lc + 1));
                                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {
                        try {
                            View vi = viewArray.get(pos).get();

                            final ImageView like = (ImageView) vi.findViewById(R.id.imv_timeline_row_like);

                            final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    like.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    like.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            like.startAnimation(ani1);
                        } catch (NullPointerException e) {

                        }

                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                    m_ResultList.get(pos).setIsLike(true);
                                    notifyDataSetChanged();

                                    View v = viewArray.get(position).get();

                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                    tempLay.setSelected(true);
//                                tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                    tempTv.setText("" + (lc + 1));
                                    tvLikeCnt.setText("" + (lc + 1));
                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                } catch (ArrayIndexOutOfBoundsException e) {

                                } catch (NullPointerException e) {

                                }
                            }
                        });
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
//                                    Log.e("result nonselect", result);
                                    final JSONObject jd = new JSONObject(result);

                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {

                                                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    return;
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                                    m_ResultList.get(pos).setIsLike(false);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(position).get();

                                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                    tempLay.setSelected(false);
//                                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                                    tempTv.setText("" + (lc - 1));
                                                    tvLikeCnt.setText("" + (lc - 1));
                                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            } catch (ArrayIndexOutOfBoundsException e) {

                                            } catch (NullPointerException e) {

                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                } catch (ArrayIndexOutOfBoundsException e) {

                                } catch (NullPointerException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                }
            });
            imvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    i.putExtra("last", model.getComCount());
                    ((Activity) con).startActivityForResult(i, NewGridTimelineActivity.COMMENT_RETURN);
                }
            });

            imvShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SharePopupForTimelineActivity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", gifWidth);
                    i.putExtra("h", gifHeight);
                    ((Activity) con).startActivityForResult(i, NewGridTimelineActivity.SHARE_RETURN);
                }
            });


            tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    i.putExtra("last", model.getComCount());
                    ((Activity) con).startActivityForResult(i, NewGridTimelineActivity.COMMENT_RETURN);
                }
            });
            tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    con.startActivity(i);
                }
            });


            cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });


            bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
                @Override
                public void onImageChanged(Drawable d) {
                    if (d instanceof GifDrawable) {

                        GifDrawable gd = (GifDrawable) d;

                        gifHeight = gd.getIntrinsicHeight();
                        gifWidth = gd.getIntrinsicWidth();
                        if (gd != null) {
                            gd.addAnimationListener(new AnimationListener() {
                                @Override
                                public void onAnimationCompleted(int loopNumber) {

                                    StringTransMethod stmCount = new StringTransMethod() {

                                    };

                                    Map<String, String> params = new HashMap<>();
                                    params.put("post_id", model.getPsot_id());

//                                    AppController.apiTaskSubNew = new APITaskSubNew(params, 0, stmCount);
//                                    AppController.apiTaskSubNew.execute();

//                                    AppController.playCountExe.execute(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
//                                        }
//                                    });
                                }

                            });
                        }
                    } else {

                        String tempUrl2 = model.getUrl();

                        int lastIdx2 = tempUrl2.lastIndexOf("_");

                        final String tempName2 = tempUrl2.substring(0, lastIdx2) + "_1.gif";

                        downloadThumbImage(bimv, AppController.URL + tempName2);

//                        downloadGifbImage(bimv, AppController.URL + model.getUrl());
                    }

                }
            });

//            if((int)bimv.getTag() == 0){
//                downloadThumbImage(bimv, AppController.URL + tempName);
//            }else if((int)bimv.getTag() == 1){
//                downloadGifbImage(bimv, AppController.URL + model.getUrl());
//            }else{
//
//            }
//            if(model.isLoading()){
//                downloadThumbImage(bimv, AppController.URL + tempName);
//            }else{
//                if(AppController.thumbThread != null && AppController.thumbThread.isAlive()){
//                    AppController.thumbThread.interrupt();
//                }
//                if(AppController.gifThread != null && AppController.gifThread.isAlive()){
//                    AppController.gifThread.interrupt();
//                }
//            }
        } catch (NullPointerException e) {

        } catch (StringIndexOutOfBoundsException e) {

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        return convertView;

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }


}
