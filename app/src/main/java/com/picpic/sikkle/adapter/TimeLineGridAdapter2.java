package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageRun;
import com.picpic.sikkle.utils.MinThumbImageRun;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineGridAdapter2 extends ArrayAdapter<GridItem> {
    LayoutInflater m_LayoutInflater = null;
    SparseArray<WeakReference<View>> viewArray;
    Context con;
    public static GridResult m_ResultList;
    GIFLoader gifLoader;
    RequestManager glide;

    public TimeLineGridAdapter2(Context ctx, int txtViewId, List<GridItem> modles) {
        super(ctx, txtViewId, modles);
        this.con = ctx;
        m_ResultList = (GridResult) modles;
        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        gifLoader = new GIFLoader(ctx);
        this.glide = Glide.with(ctx);
    }

    public GridResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(GridResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int pos = position;

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null)
                return convertView;
        }
//
        try {
        if (convertView == null) {
            convertView = m_LayoutInflater.inflate(R.layout.row_grid, null);
        }
//            StaggeredGridLayoutManager
        final BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_grid_row);

        final GridItem model = getItem(pos);
        String tempStr = model.getUrl();
        int underbarCount = tempStr.lastIndexOf("_");
        final String tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

        String tempUrl2 = model.getUrl();

        int lastIdx = tempUrl2.lastIndexOf("_");

        String tempName;
        try {
            tempName = tempUrl2.substring(0, lastIdx) + ".jpg";
        } catch (StringIndexOutOfBoundsException e) {
            tempName = "";
        }
//            downloadThumbImage(bimv, AppController.URL + tempName);

//            bimv.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName, (Activity) con);
//            bimv.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
        bimv.setTag(model.getPost_id());

//            Glide.with(con)
//                    .load(AppController.URL + tempUrl)
//                    .animate(R.anim.alpha_on)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .into(bimv);

        glide.load(AppController.URL + tempUrl)
                .into(bimv);

//            gifLoader.DisplayImage(AppController.URL + tempUrl, bimv, false);
//            Glide.with(con)
//                    .load(AppController.URL+tempUrl)
//                    .asGif()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(bimv);

//            bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
//                @Override
//                public void onImageChanged(Drawable d) {
//                    if (d instanceof GifDrawable) {
//
//                    } else {
//                        downloadGifbImage(bimv, AppController.URL + tempUrl);
//                    }
//                }
//            });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPost_id());
                i.putExtra("navi", 0);
                con.startActivity(i);
            }
        });

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }
        return convertView;
    }

}