package com.picpic.sikkle.adapter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.etsy.android.grid.util.DynamicHeightTextView;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.GridItem;
import com.picpic.sikkle.beans.GridResult;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.BoundableOfflineImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

/***
 * ADAPTER
 */

public class SampleAdapter extends ArrayAdapter<GridItem> {

    private static final String TAG = "SampleAdapter";
    private Context con;
    private GridResult models;
    SparseArray<WeakReference<View>> viewArray;

    static class ViewHolder {
        BoundableOfflineImageView bimv;
    }

    private final LayoutInflater mLayoutInflater;

    public SampleAdapter(final Context context, final int textViewResourceId, GridResult models) {
        super(context, textViewResourceId, models);
        mLayoutInflater = LayoutInflater.from(context);
        this.con = context;
        this.models = models;
        this.viewArray = new SparseArray<WeakReference<View>>(models.size());
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

//        ViewHolder vh;
//        if (convertView == null) {
//            convertView = mLayoutInflater.inflate(R.layout.row_grid, parent, false);
//            vh = new ViewHolder();
//            vh.bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_grid_row);
//
//            convertView.setTag(vh);
//        } else {
//            vh = (ViewHolder) convertView.getTag();
//        }

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null)
                return convertView;
        }

        try {
            convertView = mLayoutInflater.inflate(R.layout.row_grid, parent, false);

            BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.bimv_grid_row);

            final GridItem model = models.get(position);

            String tempStr = model.getUrl();
            if(tempStr.length()>3){
                int underbarCount = tempStr.lastIndexOf("_");
                final String tempUrl = AppController.URL + tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                String tempUrl2 = model.getUrl();

                int lastIdx = tempUrl2.lastIndexOf("_");

                String tempName;
                try {
                    tempName = AppController.URL + tempUrl2.substring(0, lastIdx) + ".jpg";
                } catch (StringIndexOutOfBoundsException ee) {
                    tempName = "";
                }
//        vh.bimv.setTag(tempUrl);
                bimv.setTag(tempUrl);

//        vh.bimv.setImageBitmap(null);

//        Glide.with(con)
//                .load(tempUrl)
//                .animate(R.anim.alpha_on)
//                .into(holder.bimv);
//        }
//        glide.load(tempUrl)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.bimv);

//        Glide.with(con)
//                .load(tempUrl)
//                .asGif()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(vh.bimv);
//        AppController.gifLoader.DisplayImage(tempUrl, vh.bimv, false);
                AppController.gifLoader.DisplayImage(tempUrl, bimv, false);
//        gifLoader.DisplayImage(tempUrl, holder.bimv, false);
            }else{
                bimv.setTag("");
            }
            if (!model.is0()) {
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(con, SinglePostV1Activity.class);
                        i.putExtra("postId", model.getPost_id());
                        i.putExtra("navi", 0);
                        con.startActivity(i);
                    }
                });
            }

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        return convertView;
    }

}