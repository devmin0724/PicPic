package com.picpic.sikkle.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.AndTagFeedActivity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopupActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CallTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.HashTag;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-10-28.
 */
public class TimeLineAdapter extends ArrayAdapter<TimelineItem> {
    LayoutInflater m_LayoutInflater = null;
    SparseArray<WeakReference<View>> viewArray;
    public static Context con;
    public static TimelineResult m_ResultList;
    int gifHeight = 0, gifWidth = 0;
    int lastListCount = 0;
    Activity ac;

    public TimeLineAdapter(Context ctx, int txtViewId, List<TimelineItem> modles) {
        super(ctx, txtViewId, modles);
        con = ctx;
        ac = (Activity) con;
        m_ResultList = (TimelineResult) modles;
        this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
        this.m_LayoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public TimelineResult getResultList() {
        return m_ResultList;
    }

    public void setResultList(TimelineResult result) {
        m_ResultList = result;
        ((Activity) con).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (viewArray != null && viewArray.get(position) != null) {
            convertView = viewArray.get(position).get();
            if (convertView != null){
                BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.imv_timeline_row_img);
                final TimelineItem model = m_ResultList.get(position);

                String tempUrl = model.getUrl();

                int lastIdx = tempUrl.lastIndexOf("_");

                final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                downloadThumbImage(bimv, AppController.URL + tempName);

                return convertView;
            }
        }

        try {
            convertView = m_LayoutInflater.inflate(R.layout.row_timeline, null);
            final BoundableOfflineImageView bimv = (BoundableOfflineImageView) convertView.findViewById(R.id.imv_timeline_row_img);
            bimv.setTag(0);

            final CircleImageView cimvPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_row_pro);
            final CircleImageView cimvComPro = (CircleImageView) convertView.findViewById(R.id.cimv_timeline_row_comment_pro);

            final LinearLayout imvLike = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_like);
            LinearLayout imvComment = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_comment);
            LinearLayout imvShare = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_share);
            LinearLayout imvMore = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_more);

            TextView tvUserId = (TextView) convertView.findViewById(R.id.tv_timeline_row_id);
            TextView tvTime = (TextView) convertView.findViewById(R.id.tv_timeline_row_time);
            TextView tvComId = (TextView) convertView.findViewById(R.id.tv_timeline_row_comment_id);
            TextView tvComBody = (TextView) convertView.findViewById(R.id.tv_timeline_row_comment_body);
            TextView tvBody = (TextView) convertView.findViewById(R.id.tv_timeline_row_body);
            TextView tvLikeCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_like_cnt);
            TextView tvComCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_comment_cnt);
            TextView tvPlayCnt = (TextView) convertView.findViewById(R.id.tv_timeline_row_play_cnt);
            final TextView tvAndTagTitle = (TextView) convertView.findViewById(R.id.tv_timeline_row_andtag_title);
            TextView tvAndTagBody = (TextView) convertView.findViewById(R.id.tv_timeline_row_andtag_body);

            LinearLayout layAndTag = (LinearLayout) convertView.findViewById(R.id.lay_timeline_row_andtag);
            LinearLayout layLastComment = (LinearLayout) convertView.findViewById(R.id.lay_last_comment);

            ImageView like = (ImageView) convertView.findViewById(R.id.imv_timeline_row_like);
            like.setVisibility(View.GONE);

//            final TimelineItem model = m_ResultList.get(position);
            final TimelineItem model = m_ResultList.get(position);

            final int pos = getPosition(model);

            bimv.measure(model.getWidth(), model.getHeight());

            String tempUrl = model.getUrl();

            int lastIdx = tempUrl.lastIndexOf("_");

            final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

//                holder.bimv.setImageURLString(AppController.URL + model.getUrl(), AppController.URL + tempName);

//            bimv.setImageURLString(AppController.URL + model.getUrl(), AppController.URL + tempName, (Activity) con);
//            bimv.setImageURLString(AppController.URL + model.getUrl(), AppController.URL + tempName, (Activity) con);
//            Log.e("url", AppController.URL + model.getUrl());
//            Log.e("thumb", AppController.URL + tempName);
//            downloadImage(bimv, AppController.URL + tempName);

            downloadThumbImage(bimv, AppController.URL + tempName);

//                new ImageTask(position, AppController.URL + model.getUrl(), AppController.URL + tempName, 2, bimv)
//                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

//            try {
//                if (pos == m_ResultList.size() - 1) {
//                    layBlank.setVisibility(View.VISIBLE);
//                } else {
//                    layBlank.setVisibility(View.GONE);
//                }
//            } catch (NullPointerException e) {
//                layBlank.setVisibility(View.GONE);
//            }

            if (model.getTlci().isExist()) {
                layLastComment.setVisibility(View.VISIBLE);
            } else {
                layLastComment.setVisibility(View.GONE);
            }

            if (model.isAndTag()) {
                layAndTag.setVisibility(View.VISIBLE);
                tvAndTagTitle.setText(model.getAndTagName());
                tvAndTagTitle.setTag(model.getAndTagId());
                tvAndTagBody.setText(model.getAndTagBody());
            } else {
                layAndTag.setVisibility(View.GONE);
            }

            bimv.setOnTouchListener(new OnSwipeTouchListener(con) {
                @Override
                public void onClick() {
                    super.onClick();
                    Drawable d = bimv.getDrawable();
                    if (d instanceof GifDrawable) {
                        try {
                            GifDrawable g = (GifDrawable) d;
                            if (g.isPlaying()) {
                                g.pause();
                            } else {
                                g.start();
                            }
                        } catch (NullPointerException e) {

                        }
                    }

                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();

                    try {
                        if (imvLike.isSelected()) {
                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                    m_ResultList.get(pos).setIsLike(false);
                                    notifyDataSetChanged();

                                    View v = viewArray.get(position).get();

                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                    tempLay.setSelected(false);
                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                }
                            });
                            StringTransMethod stmLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        Log.e("result onselect", result);
                                        final JSONObject jd = new JSONObject(result);
                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {

                                                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        int lc = model.getLikeCount();
                                                        m_ResultList.get(pos).setLikeCount(lc + 1);
                                                        m_ResultList.get(pos).setIsLike(true);
                                                        notifyDataSetChanged();

                                                        View v = viewArray.get(position).get();

                                                        LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                        TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                        tempLay.setSelected(true);
                                                        tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                                        viewArray.setValueAt(position, new WeakReference<View>(v));
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                            AppController.apiDataTaskNew.execute();


                        } else {


                            View v = viewArray.get(pos).get();

                            final ImageView like = (ImageView) v.findViewById(R.id.imv_timeline_row_like);

                            final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                            final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                            final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                            like.setVisibility(View.VISIBLE);

                            ani1.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                    like.startAnimation(ani2);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani2.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.startAnimation(ani3);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            ani3.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    like.setVisibility(View.GONE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });

                            like.startAnimation(ani1);

                            ((Activity) con).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int lc = model.getLikeCount();
                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                    m_ResultList.get(pos).setIsLike(true);
                                    notifyDataSetChanged();

                                    View v = viewArray.get(position).get();

                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                    tempLay.setSelected(true);
                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                }
                            });
                            StringTransMethod stmUnLikeResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        final JSONObject jd = new JSONObject(result);

                                        ((Activity) con).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    if (jd.getInt("result") == 0) {
                                                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                        Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        int lc = model.getLikeCount();
                                                        m_ResultList.get(pos).setLikeCount(lc - 1);
                                                        m_ResultList.get(pos).setIsLike(false);
                                                        notifyDataSetChanged();

                                                        View v = viewArray.get(position).get();

                                                        LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                        TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                        tempLay.setSelected(false);
                                                        tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                                        viewArray.setValueAt(position, new WeakReference<View>(v));
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("post_reply_id", model.getPsot_id());
                            params.put("click_id", AppController.getSp().getString("email", ""));
                            params.put("like_form", "P");

                            AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                            AppController.apiDataTaskNew.execute();

                        }
                    } catch (NullPointerException e) {

                    }
                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(con, GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    con.startActivity(i);
                }
            });

            if (model.getPro_url().equals("")) {
                cimvPro.setImageResource(R.drawable.icon_timeline_noprofile);
            } else {
                cimvPro.setImageURLString(AppController.URL + model.getPro_url());
//                downloadProbImage(cimvPro, AppController.URL + model.getPro_url());
//                downloadImage(cimvPro, AppController.URL + model.getPro_url());
//                    new ProfileTask(position, holder, AppController.URL + model.getPro_url())
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);

//                    new ImageTask(position, holder, AppController.URL + model.getPro_url(), "", 0)
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
            }

            cimvPro.setTag(model.getOwnerId());

            String tempTime = "";

            String tempDate = model.getTime();
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            String tempC = sdf.format(now);

            ArrayList<String> nows = new ArrayList<>();
            ArrayList<String> posts = new ArrayList<>();

            for (int i = 0; i < tempDate.length(); i = i + 2) {
                nows.add(tempC.substring(i, i + 2));
                posts.add(tempDate.substring(i, i + 2));
            }

            int tempNavi = 0;

            for (int i = 0; i < nows.size(); i++) {
                if (!nows.get(i).equals(posts.get(i))) {
                    tempNavi = i;
                    break;
                }
            }

            int tempNow = Integer.parseInt(nows.get(tempNavi));
            int tempPosts = Integer.parseInt(posts.get(tempNavi));

            switch (tempNavi) {
                case 0:

                    break;
                case 1:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_year);
                    } else {
                        tempTime = tempPosts - tempNow + con.getResources().getString(R.string.timeline_before_year);
                    }
                    break;
                case 2:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_month);
                    } else {
                        tempTime = (12 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_month);
                    }
                    break;
                case 3:
                    Calendar c = Calendar.getInstance();
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_day);
                    } else {
                        c.set(c.get(Calendar.YEAR), tempPosts, c.get(Calendar.DAY_OF_MONTH));
                        int lastDay = c.getActualMaximum(Calendar.DATE);
                        tempTime = (lastDay - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_day);
                    }
                    break;
                case 4:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_hour);
                    } else {
                        tempTime = (24 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_hour);
                    }
                    break;
                case 5:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_minute);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_minute);
                    }
                    break;
                case 6:
                    if (tempNow > tempPosts) {
                        tempTime = tempNow - tempPosts + con.getResources().getString(R.string.timeline_before_second);
                    } else {
                        tempTime = (60 - tempPosts) + tempNow + con.getResources().getString(R.string.timeline_before_second);
                    }
                    break;
            }

            tvUserId.setText(model.getId());
            tvTime.setText(tempTime);
            tvBody.setText(model.getBody());
            tvLikeCnt.setText(con.getResources().getString(R.string.like) + " " + model.getLikeCount() + con.getResources().getString(R.string.timeline_count));
            tvComCnt.setText(con.getResources().getString(R.string.comment) + " " + model.getComCount() + con.getResources().getString(R.string.timeline_count));
            double playCnt = (double) model.getPlayCnt();
            DecimalFormat df = new DecimalFormat("#,##0");
            tvPlayCnt.setText(df.format(playCnt) + "");

            if (model.getTlci().isExist()) {
                tvComId.setText(model.getTlci().getId());
                tvComBody.setText(model.getTlci().getBody());
                cimvComPro.setImageURLString(AppController.URL + model.getTlci().getPro_url());
//                downloadProbImage(cimvComPro, AppController.URL + model.getTlci().getPro_url());
//                downloadImage(cimvComPro, AppController.URL + model.getTlci().getPro_url());
//                    new ImageTask(position, holder, AppController.URL + model.getTlci().getPro_url(), "", 1)
//                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
                cimvComPro.setTag(model.getTlci().getEmail());
            } else {
                tvComId.setText("");
                tvComBody.setText("");
                cimvComPro.setTag("");
            }

            if (model.isLike()) {
                imvLike.setSelected(true);
            } else {
                imvLike.setSelected(false);
            }

            String commentsText = tvBody.getText().toString();
            String commentsText2 = tvComBody.getText().toString();

            ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');
            ArrayList<int[]> calloutSpans = getSpans(commentsText, '@');
            ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

            ArrayList<int[]> hashtagSpans2 = getSpans(commentsText2, '#');
            ArrayList<int[]> calloutSpans2 = getSpans(commentsText2, '@');

            SpannableString commentsContent = new SpannableString(commentsText);
            SpannableString commentsContent2 = new SpannableString(commentsText2);

            for (int i = 0; i < hashtagSpans.size(); i++) {
                int[] span = hashtagSpans.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent.setSpan(new HashTag(con),
                        hashTagStart, hashTagEnd, 0);
            }

            for (int i = 0; i < hashtagSpans2.size(); i++) {
                int[] span = hashtagSpans2.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent2.setSpan(new HashTag(con),
                        hashTagStart, hashTagEnd, 0);
            }

            for (int i = 0; i < calloutSpans.size(); i++) {
                int[] span = calloutSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];

                commentsContent.setSpan(new CallTag(con),
                        calloutStart,
                        calloutEnd, 0);

            }
            for (int i = 0; i < calloutSpans2.size(); i++) {
                int[] span = calloutSpans2.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];

                commentsContent2.setSpan(new CallTag(con),
                        calloutStart,
                        calloutEnd, 0);

            }

            for (int i = 0; i < andSpans.size(); i++) {
                int[] span = andSpans.get(i);
                int calloutStart = span[0];
                int calloutEnd = span[1];


                AndTag at = new AndTag(con);
                at.setTagId(model.getAndTagId());
                commentsContent.setSpan(at,
                        calloutStart,
                        calloutEnd, 0);

            }

            tvBody.setMovementMethod(LinkMovementMethod.getInstance());
            tvBody.setText(commentsContent);
            tvBody.setHighlightColor(con.getResources().getColor(android.R.color.transparent));
            tvComBody.setMovementMethod(LinkMovementMethod.getInstance());
            tvComBody.setText(commentsContent2);
            tvComBody.setHighlightColor(con.getResources().getColor(android.R.color.transparent));

            imvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imvLike.isSelected()) {
                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc - 1);
                                m_ResultList.get(pos).setIsLike(false);
                                notifyDataSetChanged();

                                View v = viewArray.get(position).get();

                                LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                tempLay.setSelected(false);
                                tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                viewArray.setValueAt(position, new WeakReference<View>(v));
                            }
                        });
                        StringTransMethod stmLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result onselect", result);
                                    final JSONObject jd = new JSONObject(result);
                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();

                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc + 1);
                                                    m_ResultList.get(pos).setIsLike(true);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(position).get();

                                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                    tempLay.setSelected(true);
                                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                } catch (JSONException e) {

                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 303, stmLikeResult);
                        AppController.apiDataTaskNew.execute();

                    } else {
                        View vi = viewArray.get(pos).get();

                        final ImageView like = (ImageView) vi.findViewById(R.id.imv_timeline_row_like);

                        final Animation ani1 = AnimationUtils.loadAnimation(con, R.anim.like1);
                        final Animation ani2 = AnimationUtils.loadAnimation(con, R.anim.like2);
                        final Animation ani3 = AnimationUtils.loadAnimation(con, R.anim.like3);

                        ani1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                like.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
//                                    like.setVisibility(View.GONE);
                                like.startAnimation(ani2);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.startAnimation(ani3);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        ani3.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                like.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        like.startAnimation(ani1);

                        ((Activity) con).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                m_ResultList.get(pos).setLikeCount(lc + 1);
                                m_ResultList.get(pos).setIsLike(true);
                                notifyDataSetChanged();

                                View v = viewArray.get(position).get();

                                LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                tempLay.setSelected(true);
                                tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc + 1) + con.getResources().getString(R.string.timeline_count));
                                viewArray.setValueAt(position, new WeakReference<View>(v));
                            }
                        });
                        StringTransMethod stmUnLikeResult = new StringTransMethod() {
                            @Override
                            public void endTrans(final String result) {
                                try {
                                    Log.e("result nonselect", result);
                                    final JSONObject jd = new JSONObject(result);

                                    ((Activity) con).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (jd.getInt("result") == 0) {
                                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                                    Toast.makeText(con.getApplicationContext(), con.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    int lc = model.getLikeCount();
                                                    m_ResultList.get(pos).setLikeCount(lc - 1);
                                                    m_ResultList.get(pos).setIsLike(false);
                                                    notifyDataSetChanged();

                                                    View v = viewArray.get(position).get();

                                                    LinearLayout tempLay = (LinearLayout) v.findViewById(R.id.lay_timeline_row_like);

                                                    TextView tempTv = (TextView) v.findViewById(R.id.tv_timeline_row_like_cnt);

                                                    tempLay.setSelected(false);
                                                    tempTv.setText(con.getResources().getString(R.string.like) + " " + (lc - 1) + con.getResources().getString(R.string.timeline_count));
                                                    viewArray.setValueAt(position, new WeakReference<View>(v));
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                }

                            }
                        };
                        Map<String, String> params = new HashMap<>();

                        params.put("post_reply_id", model.getPsot_id());
                        params.put("click_id", AppController.getSp().getString("email", ""));
                        params.put("like_form", "P");

                        AppController.apiDataTaskNew = new APIDataTaskNew(con, params, 302, stmUnLikeResult);
                        AppController.apiDataTaskNew.execute();

                    }
                }
            });
            imvComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });

            layLastComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });
            imvShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SharePopupActivity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", gifWidth);
                    i.putExtra("h", gifHeight);
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });
            imvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ((Activity) con).startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || imvLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    con.startActivity(i);
                }
            });
            tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(con, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    con.startActivity(i);
                }
            });

            cimvComPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvComPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        con.startActivity(new Intent(con, MyPageV2Activity.class));
                        ((Activity) con).finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(con, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            con.startActivity(i);
                        }
                    }
                }
            });

            tvAndTagTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tvAndTagTitle.getTag().toString().equals("")) {
                        Intent i = new Intent(con, AndTagFeedActivity.class);
                        i.putExtra("tag", tvAndTagTitle.getTag().toString());
                        con.startActivity(i);
                    }
                }
            });

            tvAndTagBody.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tvAndTagTitle.getTag().toString().equals("")) {
                        Intent i = new Intent(con, AndTagFeedActivity.class);
                        i.putExtra("tag", tvAndTagTitle.getTag().toString());
                        con.startActivity(i);
                    }
                }
            });

            bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
                @Override
                public void onImageChanged(Drawable d) {
                    if (d instanceof GifDrawable) {

                        GifDrawable gd = (GifDrawable) d;

                        gifHeight = gd.getIntrinsicHeight();
                        gifWidth = gd.getIntrinsicWidth();
                        if (gd != null) {
                            gd.addAnimationListener(new AnimationListener() {
                                @Override
                                public void onAnimationCompleted(int loopNumber) {

                                    StringTransMethod stmCount = new StringTransMethod() {

                                    };

                                    Map<String, String> params = new HashMap<>();
                                    params.put("post_id", model.getPsot_id());

//                                    AppController.apiTaskSubNew = new APITaskSubNew(params, 0, stmCount);
//                                    AppController.apiTaskSubNew.execute();

//                                    AppController.playCountExe.execute(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
//                                        }
//                                    });
                                }

                            });
                        }
                    } else {
                        downloadGifbImage(bimv, AppController.URL + model.getUrl());
                    }

                }
            });

//            if((int)bimv.getTag() == 0){
//                downloadThumbImage(bimv, AppController.URL + tempName);
//            }else if((int)bimv.getTag() == 1){
//                downloadGifbImage(bimv, AppController.URL + model.getUrl());
//            }else{
//
//            }
//            if(model.isLoading()){
//                downloadThumbImage(bimv, AppController.URL + tempName);
//            }else{
//                if(AppController.thumbThread != null && AppController.thumbThread.isAlive()){
//                    AppController.thumbThread.interrupt();
//                }
//                if(AppController.gifThread != null && AppController.gifThread.isAlive()){
//                    AppController.gifThread.interrupt();
//                }
//            }

        } finally {
            viewArray.put(position, new WeakReference<View>(convertView));
        }

        return convertView;

    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public static void downloadThumbImage(ImageView imageView, String url) {

        Log.e("downloadThumbImageStart", url);

//        if(AppController.thumbThread != null && AppController.thumbThread.isAlive()){
//            AppController.thumbThread.interrupt();
//        }

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.thumbRun = new MinThumbImageRun((Activity) con, url, imageView, image, file_size2, 1);
//
//        if(AppController.thumbExe.isTerminated()){
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
//
//        AppController.thumbExe.submit(AppController.thumbRun);

//        AppController.thumbThread = new Thread(AppController.thumbRun);
//
//        AppController.thumbThread.start();
//
//        AppController.thumbThread = new MinImageThread((Activity) con, url, imageView, image, file_size2, 1);
//        AppController.thumbThread.run();

    }

    public static void downloadGifbImage(ImageView imageView, String url) {

        Log.e("downloadGifImageStart", url);

        Object image = AppController.memoryCache.get(url);

        File gifFile = AppController.fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

//        AppController.gifRun = new MinImageRun((Activity) con, url, imageView, image, file_size2, 1);
//
//        if(AppController.gifExe.isTerminated()){
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        AppController.gifExe.submit(AppController.gifRun);

    }


}
