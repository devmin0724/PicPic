package com.picpic.sikkle;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;

public class ShortcutCreatorActivity extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        final Intent shortcutIntent = new Intent(ShortcutCreatorActivity.this, GIFCameraFilmActivity.class);
//        shortcutIntent.putExtra("navi", 0);
//        final Intent.ShortcutIconResource iconResource = Intent.ShortcutIconResource.fromContext(this, R.mipmap.widget_camera);
//        final Intent intent = new Intent();
//        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Pic Camera");
//        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource);
//        setResult(RESULT_OK, intent);
////        Toast.makeText(this,"shortcut created", Toast.LENGTH_SHORT).show();
//        finish();


//        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
//
//
//        shortcutIntent.putExtra("cName", "코리아");
//
//
//        //이 부분은 빼셔도 별 의미는 없습니다. (putextra로 실어보내지는 것은 아이콘을 통해
//        //실행된 액티비티에 데이터를 실어보내주는 역활을 해줍니다.)
////        shortcutIntent.putExtra(EXTRA_KEY, "ApiDemos Provided This Shortcut");
//
//// 맨위의 ACTION_MAIN과 마찬가지로 필터쪽인데, 런처는 실행. DEFAULT는 기본값을
//        //가집니다. 보통 아이콘을 통해서 액티비티를 실행하므로 LAUNCHER를 해주시면 됩니다.
//        shortcutIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//
//        // 이건 실행될 액티비티의 클래스와 이름을 가져오는 부분입니다.
//        //이 소스를 실행하는 액티비티의 이름을 가져가며, 임의로 정하실 수 있습니다.
//        //또한 컨텍스트는 특별한 경우가 아니면 this로 처리해주시면됩니다.
//        shortcutIntent.setClassName(this, this.getClass().getName());
//
//// 아이콘을 통해 실행하면 기존에 켜져 있던 원래 앱을 꺼버리거나 재시작함.
//        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
//                Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//
//
//        Intent intent = new Intent();
//        // 위에서 인텐트에 담은 정보들을 이 인텐트에 실어 보냅니다.
//// 두번째의 NAME은 아이콘의 "이름"이 됩니다.
//// 세번째는 바탕화면에 적용될 아이콘을 정의 합니다. 앱 내부의 이미지를 가져오므로
//        //반드시 아래와 같은 방법으로 정의 하셔야 됩니다.
//// 마지막은 아이콘을 생성합니다.
//// setResult/sendBroadcast의 경우 이 아이콘을 만들어 보내는 액티비티에 shortcutintent에 실어보낸 정보를 다시받아올때 사용하시면 됩니다.
//        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "코리아");
//        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
//                Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher));
//        intent.putExtra("duplicate", false);
//        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
//
//        sendBroadcast(intent);
//        setResult(RESULT_OK, intent);


//        Intent shortcutIntent;
//        shortcutIntent = new Intent();
//        shortcutIntent.setComponent(new ComponentName(getPackageName(), ".classname"));
//
//        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        final Intent putShortCutIntent = new Intent();
//        putShortCutIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//
//// Sets the custom shortcut's title
//        putShortCutIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Title");
//        putShortCutIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(this, R.mipmap.widget_camera));
//        putShortCutIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
//        sendBroadcast(putShortCutIntent);

//        Intent.ShortcutIconResource icon = Intent.ShortcutIconResource.fromContext(this, R.mipmap.widget_camera);
//
//        Intent intent = new Intent();
//
//        Intent launchIntent = new Intent(this, GIFCameraFilmActivity.class);
//        launchIntent.putExtra("navi", 0);
//        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, launchIntent);
//        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Your Shortcut");
//        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
//
//        setResult(RESULT_OK, intent);
//        finish();super.onCreate(icicle);
        // 수신된 Intent 취득
        final Intent intent = getIntent();
        final String action = intent.getAction();
        // Intent.ACTION_CREATE_SHORTCUT 수신이 이하 수행
        // Home에서 shortcut작성을 클릭했을 경우 수신된다.
        if (Intent.ACTION_CREATE_SHORTCUT.equals(action)) {
            setupShortcut();
            finish();
            return;
        }
        // CREATE_SHORTCUT intent를 수신하지 않고 실행되었을 경우 메시지 출력
    }

    private void setupShortcut() {
        //새로운 Intent 생성, shortcut클릭시 발생하는 Intent로 사용
//        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
        Intent shortcutIntent = new Intent(ShortcutCreatorActivity.this, GIFCameraFilmActivity.class);
//        shortcutIntent.setClassName(this, this.getClass().getName());
//        shortcutIntent.putExtra(EXTRA_KEY, "ApiDemos Provided This Shortcut");

        // Home Activity에서 shortcut작성을 클릭했을 경우 Home Activity로의  리턴 값을 정의한다.
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);  //shortcut을 클릭했을때 Target Activity로 전송할 action을 지정한다.
         intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Pic Camera"); //shortcut 명 지정
        Parcelable iconResource = Intent.ShortcutIconResource.fromContext(
                this, R.mipmap.widget_camera);
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource); //shortcut으로 사용할 아이콘 지정
        // Now, return the result to the launcher
        setResult(RESULT_OK, intent); // 호출한 화면(Home Activity)로 리턴
        finish();
    }
}
