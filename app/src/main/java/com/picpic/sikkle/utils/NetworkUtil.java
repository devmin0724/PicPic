package com.picpic.sikkle.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by devmin-sikkle on 2015-12-02.
 */
public class NetworkUtil {
    public static int TYPE_NON_CONNECTED = 0;
    public static int TYPE_WIFI = 1;
    public static int TYPE_4G = 2;
    public static int TYPE_3G = 3;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIMAX)
                return TYPE_4G;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                if(activeNetwork.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE){
                    return TYPE_4G;
                }else{
                    return TYPE_3G;
                }
            }
        }
        return TYPE_NON_CONNECTED;
    }

//    public static String getConnectivityStatusString(Context context) {
//        int conn = NetworkUtil.getConnectivityStatus(context);
//        String status = null;
//        if (conn == NetworkUtil.TYPE_WIFI) {
//            status = "Wifi enabled";
//        } else if (conn == NetworkUtil.TYPE_4G) {
//            status = "Mobile data enabled";
//        } else if (conn == NetworkUtil.TYPE_NON_CONNECTED) {
//            status = "Not connected to Internet";
//        }
//        return status;
//    }

}
