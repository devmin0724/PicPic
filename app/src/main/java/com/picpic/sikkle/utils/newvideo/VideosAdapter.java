package com.picpic.sikkle.utils.newvideo;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Video;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;

import java.util.ArrayList;

/**
 * Created by devmin on 2016-05-06.
 */
public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {

    private static String TAG = "VideosAdapter";

    Context context;
    private ArrayList<Video> urls;
    public VideoPlayerController videoPlayerController;
    private ImageLoader imageLoader;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ProgressBar progressBar;
        public RelativeLayout layout;
        public BoundableOfflineImageView bimv;
        public FrameLayout lay;

        public ViewHolder(View v) {
            super(v);
            lay = (FrameLayout)v.findViewById(R.id.test_lay);
            bimv = (BoundableOfflineImageView)v.findViewById(R.id.bimv_test);
            layout = (RelativeLayout) v.findViewById(R.id.layout);
            textView = (TextView) v.findViewById(R.id.textView);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        }
    }

    public VideosAdapter(Context context, final ArrayList<Video> urls) {

        this.context = context;
        this.urls = urls;
        videoPlayerController = new VideoPlayerController(context);
        imageLoader = new ImageLoader(context);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_main, parent, false);

        Configuration configuration = context.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        ViewHolder viewHolder = new ViewHolder(v);

//        int screenWidthPixels = PixelUtil.dpToPx(context, screenWidthDp);
//        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT, screenWidthPixels);
//        viewHolder.layout.setLayoutParams(rel_btn);

        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Video video = urls.get(position);
        holder.textView.setText("Video " + video.getId());

//        String tempName = video.getUrl().replace("_2.mp4", ".jpg");
//        imageLoader.DisplayImage(tempName, holder.bimv, false);

        final VideoPlayer videoPlayer = new VideoPlayer(context);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        videoPlayer.setLayoutParams(params);

        holder.layout.addView(videoPlayer);
        videoPlayerController.loadVideo(video, videoPlayer, holder.progressBar);
        videoPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                videoPlayer.changePlayState();
            }
        });
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        Log.d(TAG, "onViewRecycledCalled");
        holder.layout.removeAllViews();

    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

}