package com.picpic.sikkle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.picpic.sikkle.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class MinThumbImageRun implements Runnable {

    String url = "", contentType = "";
    ImageView imv;
    Object obj;
    long nowSize;
    Activity ac;
    int navi;
    File f;

    public MinThumbImageRun(Activity activity, String url, ImageView imv, Object obj, long size, int nav) {
        this.url = url;
        this.imv = imv;
        this.obj = obj;
        this.nowSize = size;
        this.ac = activity;
        this.navi = nav;

//        if (AppController.gifExe.isShutdown() || AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        if (AppController.thumbExe.isShutdown() || AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
    }

    @Override
    public void run() {
        HttpURLConnection conn = null;
        OutputStream os = null;
        byte[] buffer = new byte[50 * 1024];
        int bufferSize = buffer.length;
        try {
            f = AppController.fileCache.getFile(url);
            URL imageUrl = null;
            imageUrl = new URL(url);
            conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            contentType = conn.getContentType();

            try {
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

                FileOutputStream fos = new FileOutputStream(f);
                int red2 = 0;
                byte[] buf2 = new byte[50 * 1024];
                while ((red2 = bis.read(buf2)) != -1) {
                    fos.write(buf2, 0, red2);
                }
                long endTime = System.currentTimeMillis(); //maybe
                fos.flush();
                fos.close();

            } catch (FileNotFoundException e) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageResource(R.drawable.non_interest);
                    }
                });
                return;
            }

            if (!"image/gif".equals(contentType)) {
                final Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageBitmap(b);
                    }
                });
            } else {
                try {
                    final GifDrawable gd = new GifDrawable(f);
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imv.setImageDrawable(gd);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
