package com.picpic.sikkle.utils.filter.util;

import android.graphics.Bitmap;

import com.picpic.sikkle.utils.filter.Amaro;
import com.picpic.sikkle.utils.filter.BicubicScaleFilter;
import com.picpic.sikkle.utils.filter.BlockFilter;
import com.picpic.sikkle.utils.filter.BlurFilter;
import com.picpic.sikkle.utils.filter.BoxBlurFilter;
import com.picpic.sikkle.utils.filter.BumpFilter;
import com.picpic.sikkle.utils.filter.CellularFilter;
import com.picpic.sikkle.utils.filter.ChannelMixFilter;
import com.picpic.sikkle.utils.filter.CircleFilter;
import com.picpic.sikkle.utils.filter.ColorHalftoneFilter;
import com.picpic.sikkle.utils.filter.ContourFilter;
import com.picpic.sikkle.utils.filter.ContrastFilter;
import com.picpic.sikkle.utils.filter.ConvolveFilter;
import com.picpic.sikkle.utils.filter.CropFilter;
import com.picpic.sikkle.utils.filter.CrystallizeFilter;
import com.picpic.sikkle.utils.filter.CurvesFilter;
import com.picpic.sikkle.utils.filter.DespeckleFilter;
import com.picpic.sikkle.utils.filter.DiffuseFilter;
import com.picpic.sikkle.utils.filter.DiffusionFilter;
import com.picpic.sikkle.utils.filter.DisplaceFilter;
import com.picpic.sikkle.utils.filter.DissolveFilter;
import com.picpic.sikkle.utils.filter.DoGFilter;
import com.picpic.sikkle.utils.filter.EarlyBird;
import com.picpic.sikkle.utils.filter.EdgeFilter;
import com.picpic.sikkle.utils.filter.EmbossFilter;
import com.picpic.sikkle.utils.filter.ExposureFilter;
import com.picpic.sikkle.utils.filter.FieldWarpFilter;
import com.picpic.sikkle.utils.filter.FlipFilter;
import com.picpic.sikkle.utils.filter.GainFilter;
import com.picpic.sikkle.utils.filter.GammaFilter;
import com.picpic.sikkle.utils.filter.GaussianFilter;
import com.picpic.sikkle.utils.filter.GlowFilter;
import com.picpic.sikkle.utils.filter.GrayFilter;
import com.picpic.sikkle.utils.filter.GrayscaleFilter;
import com.picpic.sikkle.utils.filter.HSBAdjustFilter;
import com.picpic.sikkle.utils.filter.HalftoneFilter;
import com.picpic.sikkle.utils.filter.HighPassFilter;
import com.picpic.sikkle.utils.filter.InvertAlphaFilter;
import com.picpic.sikkle.utils.filter.InvertFilter;
import com.picpic.sikkle.utils.filter.KaleidoscopeFilter;
import com.picpic.sikkle.utils.filter.LaplaceFilter;
import com.picpic.sikkle.utils.filter.LevelsFilter;
import com.picpic.sikkle.utils.filter.LomoFi;
import com.picpic.sikkle.utils.filter.MarbleFilter;
import com.picpic.sikkle.utils.filter.MaskFilter;
import com.picpic.sikkle.utils.filter.MaximumFilter;
import com.picpic.sikkle.utils.filter.MedianFilter;
import com.picpic.sikkle.utils.filter.MinimumFilter;
import com.picpic.sikkle.utils.filter.MotionBlurFilter;
import com.picpic.sikkle.utils.filter.MotionBlurOp;
import com.picpic.sikkle.utils.filter.NoiseFilter;
import com.picpic.sikkle.utils.filter.OffsetFilter;
import com.picpic.sikkle.utils.filter.OilFilter;
import com.picpic.sikkle.utils.filter.OpacityFilter;
import com.picpic.sikkle.utils.filter.PerspectiveFilter;
import com.picpic.sikkle.utils.filter.PinchFilter;
import com.picpic.sikkle.utils.filter.PointillizeFilter;
import com.picpic.sikkle.utils.filter.PolarFilter;
import com.picpic.sikkle.utils.filter.PosterizeFilter;
import com.picpic.sikkle.utils.filter.QuantizeFilter;
import com.picpic.sikkle.utils.filter.RGBAdjustFilter;
import com.picpic.sikkle.utils.filter.ReduceNoiseFilter;
import com.picpic.sikkle.utils.filter.RescaleFilter;
import com.picpic.sikkle.utils.filter.RippleFilter;
import com.picpic.sikkle.utils.filter.RotateFilter;
import com.picpic.sikkle.utils.filter.ScaleFilter;
import com.picpic.sikkle.utils.filter.SharpenFilter;
import com.picpic.sikkle.utils.filter.ShearFilter;
import com.picpic.sikkle.utils.filter.SmartBlurFilter;
import com.picpic.sikkle.utils.filter.SmearFilter;
import com.picpic.sikkle.utils.filter.SolarizeFilter;
import com.picpic.sikkle.utils.filter.SphereFilter;
import com.picpic.sikkle.utils.filter.StampFilter;
import com.picpic.sikkle.utils.filter.SwimFilter;
import com.picpic.sikkle.utils.filter.ThresholdFilter;
import com.picpic.sikkle.utils.filter.TritoneFilter;
import com.picpic.sikkle.utils.filter.TwirlFilter;
import com.picpic.sikkle.utils.filter.UnsharpFilter;
import com.picpic.sikkle.utils.filter.WaterFilter;
import com.picpic.sikkle.utils.filter.WeaveFilter;

/**
 * Created by devmin-sikkle on 2015-11-26.
 */
public class MinFilterUtils {

    public static final int FILTER_AMARO = 1;
    public static final int FILTER_BICUBICSCALE = 2;
    public static final int FILTER_BLOCK = 3;
    public static final int FILTER_BLUR = 4;
    public static final int FILTER_BOXBLUR = 5;
    public static final int FILTER_BUMP = 6;
    public static final int FILTER_CELLULAR = 7;
    public static final int FILTER_CHANNELMIX = 8;
    public static final int FILTER_CIRCLE = 9;
    public static final int FILTER_COLORHALFTONE = 10;
    public static final int FILTER_CONTOUR = 11;
    public static final int FILTER_CONSTRAST = 12;
    public static final int FILTER_CONVOLVE = 13;
    public static final int FILTER_CROP = 14;
    public static final int FILTER_CRYSTALLIZE = 15;
    public static final int FILTER_CURVES = 16;
    public static final int FILTER_DESPECKLE = 17;
    public static final int FILTER_DIFFUSE = 18;
    public static final int FILTER_DIFFUSION = 19;
    public static final int FILTER_DISPLACE = 20;
    public static final int FILTER_DISSOLVE = 21;
    public static final int FILTER_DOG = 22;
    public static final int FILTER_EARLYBIRD = 23;
    public static final int FILTER_EDGE = 24;
    public static final int FILTER_EMBOSS = 25;
    public static final int FILTER_EXPOSURE = 26;
    public static final int FILTER_FIELDWARP = 27;
    public static final int FILTER_FILIP = 28;
    public static final int FILTER_GAIN = 29;
    public static final int FILTER_GAMMA = 30;
    public static final int FILTER_GAUSSIAN = 31;
    public static final int FILTER_GLOW = 32;
    public static final int FILTER_GRAY = 33;
    public static final int FILTER_GRAYSCALE = 34;
    public static final int FILTER_HALFTONE = 35;
    public static final int FILTER_HIGHPASS = 36;
    public static final int FILTER_HSBADJUST = 37;
    public static final int FILTER_INVERTALPHA = 38;
    public static final int FILTER_INVERT = 39;
    public static final int FILTER_KALEIDOSCOPE = 40;
    public static final int FILTER_LAPLACE = 41;
    public static final int FILTER_LEVELS = 42;
    public static final int FILTER_LOMOFI = 43;
    public static final int FILTER_MARBLE = 44;
    public static final int FILTER_MASK = 45;
    public static final int FILTER_MAXUMUM = 46;
    public static final int FILTER_MEDIAN = 47;
    public static final int FILTER_MINIMUM = 48;
    public static final int FILTER_MOTIONBLUR = 49;
    public static final int FILTER_MOTIONBLUROP = 50;
    public static final int FILTER_NOISE = 51;
    public static final int FILTER_OFFSET = 52;
    public static final int FILTER_OIL = 53;
    public static final int FILTER_OPACITY = 54;
    public static final int FILTER_PERSPECTIVE = 55;
    public static final int FILTER_PINCH = 56;
    public static final int FILTER_POINT = 57;
    public static final int FILTER_POINTILLIZE = 58;
    public static final int FILTER_POLAR = 59;
    public static final int FILTER_POSTERIZE = 61;
    public static final int FILTER_QUANTIZE = 62;
    public static final int FILTER_REDUCENOISE = 63;
    public static final int FILTER_RESCALE = 64;
    public static final int FILTER_RGBADJUST = 65;
    public static final int FILTER_RIPPLE = 66;
    public static final int FILTER_ROTATE = 67;
    public static final int FILTER_SCALE = 68;
    public static final int FILTER_SHARPEN = 69;
    public static final int FILTER_SHEAR = 70;
    public static final int FILTER_SMARTBLUR = 71;
    public static final int FILTER_SMEAR = 72;
    public static final int FILTER_SOLARIZE = 73;
    public static final int FILTER_SPHERE = 74;
    public static final int FILTER_STAMP = 75;
    public static final int FILTER_SWIM = 76;
    public static final int FILTER_THRESHOLD = 77;
    public static final int FILTER_TRANSFER = 78;
    public static final int FILTER_TRANSFORM = 79;
    public static final int FILTER_TRITONE = 80;
    public static final int FILTER_TWIRL = 81;
    public static final int FILTER_UNSHARP = 82;
    public static final int FILTER_WATER = 83;
    public static final int FILTER_WEAVE = 84;
    public static final int FILTER_WHOLEIMAGE = 85;

    public static Bitmap setFilter(int filterNumber, Bitmap bitmap) {
        switch (filterNumber) {
            case FILTER_AMARO:
                Amaro filter = new Amaro();
                return filter.transform(bitmap);
            case FILTER_BICUBICSCALE:
                BicubicScaleFilter filter4 = new BicubicScaleFilter();
                return filter4.transform(bitmap);
            case FILTER_BLOCK:
                BlockFilter filter5 = new BlockFilter();
                return filter5.transform(bitmap);
            case FILTER_BLUR:
                BlurFilter blurFilter = new BlurFilter();
                return blurFilter.getBitmap(bitmap);
            case FILTER_BOXBLUR :
                BoxBlurFilter boxBlurFilter = new BoxBlurFilter();
                return boxBlurFilter.transform(bitmap);
            case FILTER_BUMP :
                BumpFilter bumpFilter = new BumpFilter();
                return bumpFilter.getBitmap(bitmap);
            case FILTER_CELLULAR :
                CellularFilter cellularFilter = new CellularFilter();
                return cellularFilter.getBitmap(bitmap);
            case FILTER_CHANNELMIX :
                ChannelMixFilter channelMixFilter = new ChannelMixFilter();
                return channelMixFilter.getBitmap(bitmap);
            case FILTER_CIRCLE :
                CircleFilter circleFilter = new CircleFilter();
                return circleFilter.getBitmap(bitmap);
            case FILTER_COLORHALFTONE :
                ColorHalftoneFilter colorHalftoneFilter = new ColorHalftoneFilter();
                colorHalftoneFilter.getBitmap(bitmap);
            case FILTER_CONTOUR :
                ContourFilter contourFilter = new ContourFilter();
                contourFilter.getBitmap(bitmap);
            case FILTER_CONSTRAST :
                ContrastFilter contrastFilter = new ContrastFilter();
                return contrastFilter.getBitmap(bitmap);
            case FILTER_CONVOLVE :
                ConvolveFilter convolveFilter = new ConvolveFilter();
                return convolveFilter.getBitmap(bitmap);
            case FILTER_CROP :
                CropFilter cropFilter = new CropFilter();
                return cropFilter.getBitmap(bitmap);
            case FILTER_CRYSTALLIZE :
                CrystallizeFilter crystallizeFilter = new CrystallizeFilter();
                return crystallizeFilter.getBitmap(bitmap);
            case FILTER_CURVES :
                CurvesFilter curvesFilter = new CurvesFilter();
                return curvesFilter.getBitmap(bitmap);
            case FILTER_DESPECKLE :
                DespeckleFilter despeckleFilter = new DespeckleFilter();
                return despeckleFilter.getBitmap(bitmap);
            case FILTER_DIFFUSE :
                DiffuseFilter diffuseFilter = new DiffuseFilter();
                return diffuseFilter.getBitmap(bitmap);
            case FILTER_DIFFUSION :
                DiffusionFilter diffusionFilter = new DiffusionFilter();
                return diffusionFilter.getBitmap(bitmap);
            case FILTER_DISPLACE :
                DisplaceFilter displaceFilter = new DisplaceFilter();
                return displaceFilter.getBitmap(bitmap);
            case FILTER_DISSOLVE :
                DissolveFilter dissolveFilter = new DissolveFilter();
                return dissolveFilter.getBitmap(bitmap);
            case FILTER_DOG :
                DoGFilter doGFilter = new DoGFilter();
                doGFilter.getBitmap(bitmap);
            case FILTER_EARLYBIRD:
                EarlyBird filter2 = new EarlyBird();
                return filter2.transform(bitmap);
            case FILTER_EDGE :
                EdgeFilter edgeFilter = new EdgeFilter();
                return edgeFilter.getBitmap(bitmap);
            case FILTER_EMBOSS :
                EmbossFilter embossFilter = new EmbossFilter();
                return embossFilter.getBitmap(bitmap);
            case FILTER_EXPOSURE :
                ExposureFilter exposureFilter = new ExposureFilter();
                return exposureFilter.getBitmap(bitmap);
            case FILTER_FIELDWARP :
                FieldWarpFilter fieldWarpFilter = new FieldWarpFilter();
                return fieldWarpFilter.getBitmap(bitmap);
            case FILTER_FILIP :
                FlipFilter flipFilter = new FlipFilter();
                return flipFilter.getBitmap(bitmap);
            case FILTER_GAIN :
                GainFilter gainFilter = new GainFilter();
                return gainFilter.getBitmap(bitmap);
            case FILTER_GAMMA :
                GammaFilter gammaFilter = new GammaFilter();
                return gammaFilter.getBitmap(bitmap);
            case FILTER_GAUSSIAN :
                GaussianFilter gaussianFilter = new GaussianFilter();
                return gaussianFilter.getBitmap(bitmap);
            case FILTER_GLOW :
                GlowFilter glowFilter = new GlowFilter();
                return glowFilter.getBitmap(bitmap);
            case FILTER_GRAY :
                GrayFilter grayFilter = new GrayFilter();
                return grayFilter.getBitmap(bitmap);
            case FILTER_GRAYSCALE :
                GrayscaleFilter grayscaleFilter = new GrayscaleFilter();
                return grayscaleFilter.getBitmap(bitmap);
            case FILTER_HALFTONE :
                HalftoneFilter halftoneFilter = new HalftoneFilter();
                return halftoneFilter.getBitmap(bitmap);
            case FILTER_HIGHPASS :
                HighPassFilter highPassFilter = new HighPassFilter();
                return highPassFilter.getBitmap(bitmap);
            case FILTER_HSBADJUST :
                HSBAdjustFilter hsbAdjustFilter = new HSBAdjustFilter();
                return hsbAdjustFilter.getBitmap(bitmap);
            case FILTER_INVERTALPHA :
                InvertAlphaFilter invertAlphaFilter = new InvertAlphaFilter();
                return invertAlphaFilter.getBitmap(bitmap);
            case FILTER_INVERT :
                InvertFilter invertFilter = new InvertFilter();
                return invertFilter.getBitmap(bitmap);
            case FILTER_KALEIDOSCOPE :
                KaleidoscopeFilter kaleidoscopeFilter = new KaleidoscopeFilter();
                return kaleidoscopeFilter.getBitmap(bitmap);
            case FILTER_LAPLACE :
                LaplaceFilter laplaceFilter = new LaplaceFilter();
                return laplaceFilter.getBitmap(bitmap);
            case FILTER_LEVELS :
                LevelsFilter levelsFilter = new LevelsFilter();
                return levelsFilter.getBitmap(bitmap);
            case FILTER_LOMOFI:
                LomoFi filter3 = new LomoFi();
                return filter3.transform(bitmap);
            case FILTER_MARBLE :
                MarbleFilter marbleFilter = new MarbleFilter();
                return marbleFilter.getBitmap(bitmap);
            case FILTER_MASK :
                MaskFilter maskFilter = new MaskFilter();
                return maskFilter.getBitmap(bitmap);
            case FILTER_MAXUMUM :
                MaximumFilter maximumFilter = new MaximumFilter();
                return maximumFilter.getBitmap(bitmap);
            case FILTER_MEDIAN :
                MedianFilter medianFilter = new MedianFilter();
                return medianFilter.getBitmap(bitmap);
            case FILTER_MINIMUM :
                MinimumFilter minimumFilter = new MinimumFilter();
                return minimumFilter.getBitmap(bitmap);
            case FILTER_MOTIONBLUR :
                MotionBlurFilter motionBlurFilter = new MotionBlurFilter();
                return motionBlurFilter.getBitmap(bitmap);
            case FILTER_MOTIONBLUROP :
                MotionBlurOp motionBlurOp = new MotionBlurOp();
                return motionBlurOp.getBitmap(bitmap);
            case FILTER_NOISE :
                NoiseFilter noiseFilter = new NoiseFilter();
                return noiseFilter.getBitmap(bitmap);
            case FILTER_OFFSET :
                OffsetFilter offsetFilter = new OffsetFilter();
                return offsetFilter.getBitmap(bitmap);
            case FILTER_OIL :
                OilFilter oilFilter = new OilFilter();
                return oilFilter.getBitmap(bitmap);
            case FILTER_OPACITY :
                OpacityFilter opacityFilter = new OpacityFilter();
                return opacityFilter.getBitmap(bitmap);
            case FILTER_PERSPECTIVE :
                PerspectiveFilter perspectiveFilter = new PerspectiveFilter();
                return perspectiveFilter.getBitmap(bitmap);
            case FILTER_PINCH :
                PinchFilter pinchFilter = new PinchFilter();
                return pinchFilter.getBitmap(bitmap);
            case FILTER_POINT :
                return bitmap;
            case FILTER_POINTILLIZE :
                PointillizeFilter pointillizeFilter = new PointillizeFilter();
                return pointillizeFilter.getBitmap(bitmap);
            case FILTER_POLAR :
                PolarFilter polarFilter = new PolarFilter();
                return polarFilter.getBitmap(bitmap);
            case FILTER_POSTERIZE :
                PosterizeFilter posterizeFilter = new PosterizeFilter();
                return posterizeFilter.getBitmap(bitmap);
            case FILTER_QUANTIZE :
                QuantizeFilter quantizeFilter = new QuantizeFilter();
                return quantizeFilter.getBitmap(bitmap);
            case FILTER_REDUCENOISE:
                ReduceNoiseFilter reduceNoiseFilter = new ReduceNoiseFilter();
                return reduceNoiseFilter.getBitmap(bitmap);
            case FILTER_RESCALE :
                RescaleFilter rescaleFilter = new RescaleFilter();
                return rescaleFilter.getBitmap(bitmap);
            case FILTER_RGBADJUST :
                RGBAdjustFilter rgbAdjustFilter = new RGBAdjustFilter();
                return rgbAdjustFilter.getBitmap(bitmap);
            case FILTER_RIPPLE :
                RippleFilter rippleFilter = new RippleFilter();
                return rippleFilter.getBitmap(bitmap);
            case FILTER_ROTATE :
                RotateFilter rotateFilter = new RotateFilter();
                return rotateFilter.getBitmap(bitmap);
            case FILTER_SCALE :
                ScaleFilter scaleFilter = new ScaleFilter();
                return scaleFilter.getBitmap(bitmap);
            case FILTER_SHARPEN :
                SharpenFilter sharpenFilter = new SharpenFilter();
                return sharpenFilter.getBitmap(bitmap);
            case FILTER_SHEAR :
                ShearFilter shearFilter = new ShearFilter();
                return shearFilter.getBitmap(bitmap);
            case FILTER_SMARTBLUR :
                SmartBlurFilter smartBlurFilter = new SmartBlurFilter();
                return smartBlurFilter.getBitmap(bitmap);
            case FILTER_SMEAR :
                SmearFilter smearFilter = new SmearFilter();
                return smearFilter.getBitmap(bitmap);
            case FILTER_SOLARIZE :
                SolarizeFilter solarizeFilter = new SolarizeFilter();
                return solarizeFilter.getBitmap(bitmap);
            case FILTER_SPHERE :
                SphereFilter sphereFilter = new SphereFilter();
                return sphereFilter.getBitmap(bitmap);
            case FILTER_STAMP :
                StampFilter stampFilter = new StampFilter();
                return stampFilter.getBitmap(bitmap);
            case FILTER_SWIM :
                SwimFilter swimFilter = new SwimFilter();
                return swimFilter.getBitmap(bitmap);
            case FILTER_THRESHOLD :
                ThresholdFilter thresholdFilter = new ThresholdFilter();
                return thresholdFilter.getBitmap(bitmap);
            case FILTER_TRANSFER:
                return bitmap;
            case FILTER_TRANSFORM :
                return bitmap;
            case FILTER_TRITONE :
                TritoneFilter tritoneFilter = new TritoneFilter();
                return tritoneFilter.getBitmap(bitmap);
            case FILTER_TWIRL :
                TwirlFilter twirlFilter = new TwirlFilter();
                return twirlFilter.getBitmap(bitmap);
            case FILTER_UNSHARP :
                UnsharpFilter unsharpFilter = new UnsharpFilter();
                return unsharpFilter.getBitmap(bitmap);
            case FILTER_WATER :
                WaterFilter waterFilter = new WaterFilter();
                return waterFilter.getBitmap(bitmap);
            case FILTER_WEAVE :
                WeaveFilter weaveFilter = new WeaveFilter();
                return weaveFilter.getBitmap(bitmap);
            case FILTER_WHOLEIMAGE :
                return bitmap;
            default:
                return bitmap;
        }


    }

}
