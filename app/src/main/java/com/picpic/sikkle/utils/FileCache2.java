package com.picpic.sikkle.utils;

import android.content.Context;

import java.io.File;

/**
 * Created by Jong-min on 2015-04-21.
 */
public class FileCache2 {
    private File cacheDir;

    public FileCache2(Context context) {
        //Find the dir to save cached images
        //        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        //            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"LazyList");
        //        else
        cacheDir = context.getCacheDir();
        if (!cacheDir.exists()) cacheDir.mkdirs();
    }

    public File getFile(Object tag) {
        File f = new File(cacheDir, Integer.toString(tag.hashCode()));
        return f;
    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null) return;
        for (File f : files)
            f.delete();
    }
}
