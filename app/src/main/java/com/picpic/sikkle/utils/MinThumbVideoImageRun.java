/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.VideoView;

import com.picpic.sikkle.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class MinThumbVideoImageRun implements Runnable {

    String url = "", contentType = "";
    VideoView vv;
    Object obj;
    long nowSize;
    Activity ac;
    int navi;
    File f;

    public MinThumbVideoImageRun(Activity activity, String url, VideoView vv, Object obj, long size, int nav) {
        this.url = url;
        this.vv = vv;
        this.obj = obj;
        this.nowSize = size;
        this.ac = activity;
        this.navi = nav;

//        if (AppController.gifExe.isShutdown() || AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        if (AppController.thumbExe.isShutdown() || AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }
    }

    @Override
    public void run() {
        HttpURLConnection conn = null;
        OutputStream os = null;
        byte[] buffer = new byte[50 * 1024];
        int bufferSize = buffer.length;
        try {
            f = AppController.fileCache.getFile(url);
            URL imageUrl = null;
            imageUrl = new URL(url);
            conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            contentType = conn.getContentType();

            try {
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

                FileOutputStream fos = new FileOutputStream(f);
                int red2 = 0;
                byte[] buf2 = new byte[50 * 1024];
                while ((red2 = bis.read(buf2)) != -1) {
                    fos.write(buf2, 0, red2);
                }
                long endTime = System.currentTimeMillis(); //maybe
                fos.flush();
                fos.close();

            } catch (FileNotFoundException e) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vv.setBackgroundResource((R.drawable.non_interest));
                    }
                });
                return;
            }

            if (!"image/gif".equals(contentType)) {
                final Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        vv.setBackground(new BitmapDrawable(b));
                    }
                });
            } else {
                try {
                    final GifDrawable gd = new GifDrawable(f);
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            vv.setBackground(new BitmapDrawable(String.valueOf(gd)));
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
