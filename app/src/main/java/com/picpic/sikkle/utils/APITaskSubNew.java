package com.picpic.sikkle.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jong-min on 2015-10-08.
 */
public class APITaskSubNew extends AsyncTask<Void, Void, Void> {

    Map<String, String> params;
    int serviceCode = 0;
    JSONObject inputJO;
    StringTransMethod stm;

    String return_msg = "";
    private String sendMSG = "abcdeabcdeabcde";

    public APITaskSubNew(Map<String, String> param, int code, StringTransMethod stmm) {

//        if (AppController.apiTaskSubNew != null) {
//            AppController.apiTaskSubNew.cancel(true);
//        }

        this.params = param;
        this.serviceCode = code;
        this.stm = stmm;

        inputJO = new JSONObject();
        Iterator<String> iter = params.keySet().iterator();
        try {
            String key = null, value = null;
            while (iter.hasNext()) {
                key = iter.next();
                value = params.get(key);

                inputJO.put(key, value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("input", inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));

        sendMSG = inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]");

    }

    @Override
    protected Void doInBackground(Void... params) {


        OutputStream outputStream = null;
        BufferedReader in = null;
        ObjectInputStream objectInputStream = null;
        char[] buffer = new char[1024];
        CharArrayReader charArrayReader = new CharArrayReader(buffer);

        try {
            AppController.apiSocket = new Socket(AppController.ip, AppController.port);
            outputStream = AppController.apiSocket.getOutputStream();

            byte[] data = sendMSG.getBytes("UTF-8");
            int data_size = data.length;

            byte[] sendData = new byte[data_size + 9];

            sendData[0] = 0x02;

            byte[] tempA = intToByteArray(serviceCode);

            System.arraycopy(tempA, 0, sendData, 1, tempA.length);

            byte[] tempB = intToByteArray(data_size);

            System.arraycopy(tempB, 0, sendData, 5, tempB.length);

            System.arraycopy(data, 0, sendData, 9, data_size);

            outputStream.write(sendData);

            in = new BufferedReader(new InputStreamReader(AppController.apiSocket.getInputStream()));

            String tempS;

//            while ((tempS = in.readLine()) != null){
//                return_msg = tempS;
//            }

//            while ((return_msg = in.readLine()) != null){

//            }

            return_msg = in.readLine();


//        } catch (Exception e) {
//            Log.e("TCP", "C: Error1 ", e);
        } catch (SocketTimeoutException e) {
            Log.e("TCP", "C: Error1-1 ", e);
        } catch (Exception e) {
            Log.e("TCP", "C: Error1-2 ", e);
        } finally {
            try {
                AppController.apiSocket.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            }

            try {
                outputStream.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error3 ", e);
            }

            try {
                in.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error4 ", e);
            }
        }

        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void result) {
        stm.set_result(return_msg);
        super.onPostExecute(result);
    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }
}
