package com.picpic.sikkle.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

import java.util.ArrayList;

/**
 * Created by devmin on 2015-12-30.
 */
public class GIFAnimationThread extends Thread {

    SurfaceHolder holder;
    private boolean running = false;
    int delay = 30;
    ArrayList<Bitmap> arrAni = new ArrayList<Bitmap>();
    int navi = 0;

    public GIFAnimationThread(SurfaceHolder holder) {
        this.holder = holder;
    }

    public void setAni(ArrayList<Bitmap> arr, int delay) {
        this.arrAni = arr;
        this.delay = delay;
        this.running = true;
    }

    public void setRunning(boolean run) {
        this.running = run;
    }

    public boolean getRunning() {
        return running;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public void run() {
        try {
            while (running) {
                if (arrAni != null) {
                    synchronized (arrAni) {
//                        myView.setImage(arrAni.get(navi));
                        Canvas canvas = holder.lockCanvas();

                        Bitmap tempB = Bitmap.createScaledBitmap(arrAni.get(navi), canvas.getWidth(), canvas.getHeight(), false);

                        // 리소스 이미지를 Canvas 객체에 그리기(배경화면에 이미지를 그린다.)
                        canvas.drawBitmap(tempB, 0, 0, null);
                        // Canvas 객체 잠금 해제
                        holder.unlockCanvasAndPost(canvas);
                    }
                }
                try {
                    sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (navi < arrAni.size() - 1) {
                    navi = navi + 1;
                } else {
                    navi = 0;
                }
            }
        } catch (IndexOutOfBoundsException e) {

        }
    }
}