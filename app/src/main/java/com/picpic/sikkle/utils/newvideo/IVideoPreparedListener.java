package com.picpic.sikkle.utils.newvideo;

import android.provider.MediaStore;

import com.picpic.sikkle.beans.Video;

/**
 * Created by devmin on 2016-05-06.
 */
public interface IVideoPreparedListener {

    public void onVideoPrepared(Video video0);
}
