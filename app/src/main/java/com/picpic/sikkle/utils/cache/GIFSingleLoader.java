/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.cache;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

public class GIFSingleLoader {

    private GIFMemoryCache memoryCache = new GIFMemoryCache();
    private AbstractFileCache fileCache;
    private Map<ImageView, String> imageViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());
    private ExecutorService executorService;

    public GIFSingleLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }

    public void DisplayImage(String url, ImageView imageView, boolean isLoadOnlyFromCache) {
        imageViews.put(imageView, url);

        File f = memoryCache.get(url);
        if (f != null) {
            try {
                GifDrawable gd = new GifDrawable(f.getAbsolutePath());
                imageView.setImageDrawable(gd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (!isLoadOnlyFromCache) {
            queuePhoto(url, imageView);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private GifDrawable getGD(String url) {
        File f = fileCache.getFile(url);

        GifDrawable gd = null;
        if (f != null && f.exists()) {
            gd = decodeFile(f);
        }
        if (gd != null) {
            return gd;
        }
        try {
            GifDrawable gd2 = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            gd2 = decodeFile(f);
            return gd2;
        } catch (Exception ex) {
            Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
            return null;
        }
    }

    private GifDrawable decodeFile(File f) {
        try {
            GifDrawable gd = new GifDrawable(f.getAbsolutePath());
            return gd;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            GifDrawable gd = getGD(photoToLoad.url);
            memoryCache.put(photoToLoad.url, new File(photoToLoad.url));
            if (imageViewReused(photoToLoad))
                return;
            GIFDisplayer gifd = new GIFDisplayer(gd, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();
            a.runOnUiThread(gifd);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    class GIFDisplayer implements Runnable {
        GifDrawable gd;
        PhotoToLoad photoToLoad;

        public GIFDisplayer(GifDrawable gd, PhotoToLoad p) {
            this.gd = gd;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (gd != null)
                photoToLoad.imageView.setImageDrawable(gd);

        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
        imageViews.clear();
        executorService.shutdownNow();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e("", "CopyStream catch Exception...");
        }
    }
}