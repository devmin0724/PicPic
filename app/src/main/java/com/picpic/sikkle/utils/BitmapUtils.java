package com.picpic.sikkle.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;

//import org.droidparts.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BitmapUtils {

    public static ExecutorService threadPool = Executors.newCachedThreadPool();

    /* convert yuv to rgb */
    private static int convertPixel(int y, int u, int v) {
        int r = (int) (y + 1.13983f * v);
        int g = (int) (y - .39485f * u - .58060f * v);
        int b = (int) (y + 2.03211f * u);
        r = (r > 255) ? 255 : (r < 0) ? 0 : r;
        g = (g > 255) ? 255 : (g < 0) ? 0 : g;
        b = (b > 255) ? 255 : (b < 0) ? 0 : b;

        return 0xFF000000 | (r << 16) | (g << 8) | b;
    }

    public static Bitmap decodeFile(String pathName) {
        try {
            return rotateBitmap(pathName, BitmapFactory.decodeFile(pathName));
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap decodeFile(String pathName, Options opts) {
        return rotateBitmap(pathName, BitmapFactory.decodeFile(pathName, opts));
    }

/*
    public static Bitmap decodeYUV(byte[] data, int width, int height, Rect crop){
		if( crop == null ){
			crop = new Rect( 0, 0, width, height );
		}
		Bitmap image = Bitmap.createBitmap( crop.width(), crop.height(), Bitmap.Config.RGB_565 );
		int yv = 0, uv = 0, vv = 0;

		for(int y = crop.top; y < crop.bottom; y++){
			for(int x = crop.left; x < crop.right; x++){
				yv = data[y * width + x] & 0xff;
				uv = ( data[width * height + ( x / 2 ) * 2 + ( y / 2 ) * width + 1] & 0xff ) - 128;
				vv = ( data[width * height + ( x / 2 ) * 2 + ( y / 2 ) * width] & 0xff ) - 128;
				image.setPixel( x, y, convertPixel( yv, uv, vv ) );
			}
		}
		return image;
	}
	*/

    public static Bitmap decodeYUV(byte[] data, int width, int height, Rect crop) {
        if (crop == null) {
            crop = new Rect(0, 0, width, height);
        }
        Bitmap image = Bitmap.createBitmap(crop.height(), crop.width(), Bitmap.Config.RGB_565);
        int yv = 0, uv = 0, vv = 0;

        for (int y = crop.top; y < crop.bottom; y++) {
            for (int x = crop.left; x < crop.right; x++) {
                yv = data[y * width + x] & 0xff;
                uv = (data[width * height + (x / 2) * 2 + (y / 2) * width + 1] & 0xff) - 128;
                vv = (data[width * height + (x / 2) * 2 + (y / 2) * width] & 0xff) - 128;
                image.setPixel(y, x, convertPixel(yv, uv, vv));
            }
        }
        return image;
    }

//    public static Bitmap decodeYUV(File file, int width, int height, Rect crop) throws IOException {
//        return decodeYUV(IOUtils.readToByteArray(new FileInputStream(file)), width, height, crop);
//    }

    public static Bitmap getPreview(String pathName, int maxWidth, int maxHeight) {
        Options bounds = new Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) return null;

        Options opts = new Options();
        opts.inSampleSize = Math.max(bounds.outWidth / maxWidth, bounds.outHeight / maxHeight);
        return decodeFile(pathName, opts);
    }

    public static Bitmap getSquare(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int size = Math.min(width, height);

        int x = (width - size) / 2;
        int y = (height - size) / 2;
        return Bitmap.createBitmap(bmp, x, y, size, size);
    }

    public static Bitmap makeSquare(byte[] data, boolean frontFacing) {
        return makeSquare(data, frontFacing, null);
    }

    public static Bitmap makeSquare(byte[] data, boolean frontFacing, Options options) {
        int width;
        int height;
        Matrix matrix = new Matrix();
        // Convert ByteArray to Bitmap
        Bitmap bitPic = BitmapFactory.decodeByteArray(data, 0, data.length, options);
        width = bitPic.getWidth();
        height = bitPic.getHeight();

        // Perform matrix rotations/mirrors depending on camera that took the photo
        if (frontFacing) {
            float[] mirrorY = {-1, 0, 0, 0, 1, 0, 0, 0, 1};
            Matrix matrixMirrorY = new Matrix();
            matrixMirrorY.setValues(mirrorY);

            matrix.postConcat(matrixMirrorY);
        }

        matrix.postRotate(90);

        // Create new Bitmap out of the old one
        Bitmap bitPicFinal = Bitmap.createBitmap(bitPic, 0, 0, width, height, matrix, true);
        bitPic.recycle();
        return bitPicFinal;
    }

    public static Bitmap rotateBitmap(String src, Bitmap bitmap) {
        if (bitmap == null) return null;

        try {
            int orientation = getExifOrientation(src);

            if (orientation == 1) {
                return bitmap;
            }

            Matrix matrix = new Matrix();
            switch (orientation) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return bitmap;
            }

            try {
                Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
                return oriented;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;

        try {
            /**
             * if your are targeting only api level >= 5 ExifInterface exif =
             * new ExifInterface(src); orientation =
             * exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
             */
            if (Build.VERSION.SDK_INT >= 5) {
                Class<?> exifClass = Class.forName("android.media.ExifInterface");
                Constructor<?> exifConstructor = exifClass.getConstructor(String.class);
                Object exifInstance = exifConstructor.newInstance(src);
                Method getAttributeInt = exifClass.getMethod("getAttributeInt", String.class, int.class);
                Field tagOrientationField = exifClass.getField("TAG_ORIENTATION");
                String tagOrientation = (String) tagOrientationField.get(null);
                orientation = (Integer) getAttributeInt.invoke(exifInstance, tagOrientation, 1);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return orientation;
    }
}