package com.picpic.sikkle.utils;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by Jong-min on 2015-09-18.
 */
public class BoundedReader extends BufferedReader {

    private final int bufferSize;
    private char buffer[];

    public BoundedReader(final BufferedReader in, final int bufferSize) {
        super(in);
        this.bufferSize = bufferSize;
        this.buffer = new char[bufferSize];
    }

    @Override
    public String readLine() throws IOException {
        int no;

        if ((no = this.read(buffer, 0, bufferSize)) == -1) return null;
        String input = new String(buffer, 0, no).trim();

        while (no >= bufferSize && ready()) {
            if ((no = read(buffer, 0, bufferSize)) == -1) break;
        }
        return input;
    }
}
