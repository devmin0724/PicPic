package com.picpic.sikkle.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.ImageView;

import com.picpic.sikkle.widget.BoundableImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifIOException;

/**
 * Created by L on 2015-04-02.
 */
public class MinImageLoader {

    public int count = 0;
    FileCache2 fileCache;
    MemoryCache memoryCache = new MemoryCache();
    //    ExecutorService executorService = Executors.newFixedThreadPool(5);
    //    ExecutorService exe;
    Handler handler = new Handler();//handler to display images in UI thread
    String gifUrl = "";
    ImageView tempImv;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private Context mContext;
    Activity ac;

    public MinImageLoader(Context context) {
        mContext = context;
        fileCache = new FileCache2(context);
    }

//    public void DisplayImage(final String url, final ImageView imageView) {
//        this.tempImv = imageView;
//        this.gifUrl = url;
//
//        imageViews.put(imageView, url);
//        final Object image = memoryCache.get(url);
//
//        if (image instanceof Bitmap) {
//            imageView.setImageBitmap((Bitmap) image);
//        } else if (image instanceof Drawable) {
//            imageView.setImageDrawable((Drawable) image);
//        } else {
//            queuePhoto(url, imageView);
//        }
//
//    }

    public void DisplayImage(String url, BoundableImageView imageView, Activity ac, ExecutorService e) {
        this.ac = ac;
        imageViews.put(imageView, url);
        Object image = memoryCache.get(url);

        File gifFile = fileCache.getFile(url);

        //TODO 파일비교

        long file_size2 = gifFile.length();

        new FileSize(url, imageView, image, file_size2, e).execute();

    }

    private void queuePhoto(String url, ImageView imageView, ExecutorService e) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
//        executorService.submit(new PhotosLoader(p));
        e.submit(new PhotosLoader(p));
//        exe.submit(new PhotosLoader(p));
//        AppController.imageExe.submit(new PhotosLoader(p));
    }

    public Bitmap getBitmap(String url) throws IOException {
        File f = fileCache.getFile(url);

        Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
        if (b == null) {
            download(url);

            b = BitmapUtils.decodeFile(f.getAbsolutePath());
        }
        memoryCache.put(url, b);
        return b;
    }

    public Drawable getDrawable(String url) throws IOException {

        File f = fileCache.getFile(url);
        if (!f.exists()) {
            String contentType = download(url);
            if (!"image/gif".equals(contentType)) {
                return new BitmapDrawable(mContext.getResources(), getBitmap(url));
            }
        }

        try {
            GifDrawable d = new GifDrawable(f);
            memoryCache.put(url, d);
            return d;
        } catch (GifIOException e) {
            return new BitmapDrawable(mContext.getResources(), getBitmap(url));
        } catch (IOException e) {
            download(url);
            return getDrawable(url);
        }
    }


    public String download(String url) throws IOException {
        File f = fileCache.getFile(url);
        URL imageUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
        conn.setConnectTimeout(30000);
        conn.setReadTimeout(30000);
        conn.setInstanceFollowRedirects(true);
        String contentType = conn.getContentType();
        InputStream is = conn.getInputStream();
        OutputStream os = new FileOutputStream(f);
        AppController.CopyStream(is, os);
//        org.droidparts.util.IOUtils.readToStream(is, os);
        os.close();
        conn.disconnect();

        return contentType;
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        return tag == null || !tag.equals(photoToLoad.url);
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    //Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;

        }

        @Override
        public void run() {
            try {
                if (imageViewReused(photoToLoad)) return;

                Drawable d = null;

                try {
                    d = getDrawable(photoToLoad.url);
                } catch (FileNotFoundException e) {
                    d = getDrawable(AppController.NONURLINTEREST);
                }

                if (imageViewReused(photoToLoad)) return;
                BitmapDisplayer bd = new BitmapDisplayer(d, photoToLoad);
                handler.post(bd);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Drawable drawable;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Drawable d, PhotoToLoad p) {
            drawable = d;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad)) return;
            if (drawable != null) photoToLoad.imageView.setImageDrawable(drawable);
        }
    }

    public class FileSize extends AsyncTask<String, Void, Long> {

        String url = "";
        BoundableImageView imv;
        Object obj;
        long nowSize;
        ExecutorService e;

        public FileSize(String url, BoundableImageView imv, Object obj, long size, ExecutorService exe) {
            this.url = url;
            this.imv = imv;
            this.obj = obj;
            this.nowSize = size;
            this.e = exe;
        }

        @Override
        protected Long doInBackground(String... params) {

            URL urls = null;
            URLConnection urlConnection = null;
            try {
                urls = new URL(url);
                urlConnection = urls.openConnection();
                urlConnection.connect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            long file_size1 = urlConnection.getContentLength();

            if (file_size1 == nowSize) {
                if (obj instanceof Bitmap) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imv.setImageBitmap((Bitmap) obj);
                        }
                    });
                } else if (obj instanceof Drawable) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imv.setImageDrawable((Drawable) obj);
                        }
                    });
                } else {
                    queuePhoto(url, imv, e);
                }
            } else {
                queuePhoto(url, imv, e);
            }

            return file_size1;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

}