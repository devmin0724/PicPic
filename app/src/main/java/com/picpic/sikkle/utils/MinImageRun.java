package com.picpic.sikkle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;

import com.picpic.sikkle.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class MinImageRun implements Runnable {

    String url = "", contentType = "";
    ImageView imv;
    Object obj;
    long nowSize;
    Activity ac;
    int navi;
    File f;

    public MinImageRun(Activity activity, String url, ImageView imv, Object obj, long size, int nav) {
        this.url = url;
        this.imv = imv;
        this.obj = obj;
        this.nowSize = size;
        this.ac = activity;
        this.navi = nav;

//        if (AppController.gifExe.isShutdown() || AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        if (AppController.thumbExe.isShutdown() || AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }

    }

    @Override
    public void run() {
        URL urls = null;
        URLConnection urlConnection = null;
        try {
            urls = new URL(url);
            urlConnection = urls.openConnection();
            urlConnection.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final long file_size1 = urlConnection.getContentLength();

        if (file_size1 == nowSize) {
            if (obj instanceof Bitmap) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageBitmap((Bitmap) obj);
//                        Im
                    }
                });
            } else if (obj instanceof Drawable) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageDrawable((Drawable) obj);
                    }
                });
            } else {
                //TODO 다운로드
                Log.e("down1", "ok");
                HttpURLConnection conn = null;
                OutputStream os = null;
                byte[] buffer = new byte[50*1024];
                int bufferSize = buffer.length;
                try {
                    f = AppController.fileCache.getFile(url);
                    URL imageUrl = null;
                    imageUrl = new URL(url);
                    conn = (HttpURLConnection) imageUrl.openConnection();
                    conn.setConnectTimeout(30000);
                    conn.setReadTimeout(30000);
                    conn.setInstanceFollowRedirects(true);
                    contentType = conn.getContentType();


                    try{
//                        InputStream is = conn.getInputStream();
//                        BufferedInputStream bufferedInputStream = new BufferedInputStream(is);
//                        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(bufferSize);
//                        byte[] buf = new byte[bufferSize];
//                        int read;
//                        do{
//                            read = bufferedInputStream.read(buf, 0, buf.length);
//                            if(read>0){
//                                byteArrayBuffer.append(buf, 0, read);
//                            }
//                        }while (read >= 0);
//                        String result = new String(byteArrayBuffer.toByteArray());

//                        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(os);
//                        bufferedOutputStream.

//                        os = new FileOutputStream(f);
//                        org.droidparts.util.IOUtils.readToStream(is, os);

                        InputStream is = conn.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

//I usually use a ByteArrayOutputStream, as it is more common.
//                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                        int red = 0;
// This size can be changed
//                        byte[] buf = new byte[1024];
//                        while ((red = bis.read(buf)) != -1) {
//                            baos.write(buf, 0, red);
//                        }

                        FileOutputStream fos = new FileOutputStream(f);
                        int red2 = 0;
// This size can be changed
                        byte[] buf2 = new byte[50*1024];
                        while ((red2 = bis.read(buf2)) != -1) {
                            // And directly write to it.
                            fos.write(buf2, 0, red2);
                        }
                        long endTime = System.currentTimeMillis(); //maybe
// Flush after, as this may trigger a commit to disk.
                        fos.flush();
                        fos.close();

                    }catch (FileNotFoundException e){
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imv.setImageResource(R.drawable.non_interest);
                            }
                        });
                        return;
                    }

                    Log.e("contype", contentType);

                    if (!"image/gif".equals(contentType)) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);
                        final Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imv.setImageBitmap(b);
                            }
                        });
                    } else {
                        try {
                            final GifDrawable gd = new GifDrawable(f);
                            ac.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imv.setImageDrawable(gd);
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
//                    try {
//                        os.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
                    conn.disconnect();
                }
            }
        } else {
            //TODO 다운로드
            Log.e("down2", "ok");
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                f = AppController.fileCache.getFile(url);
                URL imageUrl = null;
                imageUrl = new URL(url);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                contentType = conn.getContentType();
                try{
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();
                }catch (FileNotFoundException e){
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imv.setImageResource(R.drawable.non_interest);
                        }
                    });
                    return;
                }

                if (!"image/gif".equals(contentType)) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);
                    final Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            imv.setImageBitmap(b);
                        }
                    });
                } else {
                    try {
                        final GifDrawable gd = new GifDrawable(f);
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imv.setImageDrawable(gd);
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
        }
    }
}
