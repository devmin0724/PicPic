/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.cache;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.picpic.sikkle.widget.VView;
import com.picpic.sikkle.widget.VView2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VideoSingleLoader {

    private VideoMemoryCache memoryCache = new VideoMemoryCache();
    private AbstractFileCache fileCache;
    private Map<VView2, String> vViews = Collections
            .synchronizedMap(new WeakHashMap<VView2, String>());
    private ExecutorService executorService;
    private Context con;

    public VideoSingleLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(3);
        this.con = context;
    }

    public void DisplayVideo(String url, VView2 vView, boolean isLoadOnlyFromCache) {
        vViews.put(vView, url);

        File file = memoryCache.get(url);

        if (file != null) {
            vView.setVideoPath(file.getAbsolutePath());
            vView.start();
        } else if (!isLoadOnlyFromCache) {
            queueVideo(url, vView);
        }
    }

    private void queueVideo(String url, VView2 vView) {
        VideoToLoad p = new VideoToLoad(url, vView);
        executorService.submit(new VViewLoader(p));
    }

    // Task for the queue
    private class VideoToLoad {
        public String url;
        public VView2 vView;

        public VideoToLoad(String u, VView2 i) {
            url = u;
            vView = i;
        }
    }

    class VViewLoader implements Runnable {
        VideoToLoad videoToLoad;

        VViewLoader(VideoToLoad videoToLoad) {
            this.videoToLoad = videoToLoad;
        }

        @Override
        public void run() {
            if (VViewReused(videoToLoad))
                return;
            File f = getFile(videoToLoad.url);
            memoryCache.put(videoToLoad.url, f);
            if (VViewReused(videoToLoad))
                return;
            VideoDisplayer bd = new VideoDisplayer(f, videoToLoad);
            Activity a = (Activity) videoToLoad.vView.getContext();
            a.runOnUiThread(bd);
        }
    }

    private File getFile(String url) {
        File f = fileCache.getFile(url);

        File ff = null;
        if (f != null && f.exists()) {
            ff = f;
        }
        if (ff != null) {
            return ff;
        }
        try {
            File file = null;
            Log.e("Download url", url);
            URL fileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) fileUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            file = f;
            return file;
        } catch (Exception ex) {
            Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
            return null;
        }
    }

    boolean VViewReused(VideoToLoad videoToLoad) {
        String tag = vViews.get(videoToLoad.vView);
        if (tag == null || !tag.equals(videoToLoad.url))
            return true;
        return false;
    }

    class VideoDisplayer implements Runnable {
        File file;
        VideoToLoad videoToLoad;

        public VideoDisplayer(File f, VideoToLoad v) {
            file = f;
            videoToLoad = v;
        }

        public void run() {
            if (VViewReused(videoToLoad))
                return;
            if (file != null) {
                videoToLoad.vView.setVideoPath(file.getAbsolutePath());
                videoToLoad.vView.start();
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
        vViews.clear();
        executorService.shutdownNow();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e("", "CopyStream catch Exception...");
        }
    }

}