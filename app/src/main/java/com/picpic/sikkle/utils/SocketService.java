package com.picpic.sikkle.utils;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by Jong-min on 2015-08-07.
 */
public class SocketService extends Service {
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    String return_msg = "";
    JSONObject jd, jInterruptedException, jExecutionException, jException1, jException2;
    private String sendMSG = "abcdeabcdeabcde";

    @Override
    public void onCreate() {
        super.onCreate();
        jd = new JSONObject();
        jInterruptedException = new JSONObject();
        jExecutionException = new JSONObject();
        jException1 = new JSONObject();
        jException2 = new JSONObject();

        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String tempC = sdf.format(now);

        try {

            jInterruptedException.put("result", "9991");
            jInterruptedException.put("msg", "InterruptedException");

            jExecutionException.put("result", "9992");
            jExecutionException.put("msg", "ExecutionException");

            jException1.put("result", "9993");
            jException1.put("msg", "Exception1");

            jException2.put("result", "9994");
            jException2.put("msg", "Exception2");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendMSG = jd.toString();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public String getData(JSONObject j, int code) {
        try {
            return new SocketRun(j, code).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return jInterruptedException.toString();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return jExecutionException.toString();
        }
    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }

    public class LocalBinder extends Binder {
        public SocketService getService() {
            return SocketService.this;
        }
    }

    class SocketRun extends AsyncTask<String, Void, String> {
        int serviceCode = 0;
        private Socket inetSocket = null;

        public SocketRun(JSONObject inJ, int code) {
            jd = inJ;
            sendMSG = jd.toString().replace("\"[", "[").replace("]\"", "]").replace("\\", "").replace("\"[", "[").replace("]\"", "]").replace("\"\\", "").replace("\\\"", "");
            serviceCode = code;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                Log.e("TCP", "C: Conneting..");

                inetSocket = new Socket(AppController.ip, AppController.port);
                OutputStream outputStream = null;
                BufferedReader in = null;

                try {
                    Log.e("TCP", "C: Sending '" + sendMSG + "'");

                    Log.e("serviceCode", serviceCode + "");

                    outputStream = inetSocket.getOutputStream();

                    byte[] data = sendMSG.getBytes("UTF-8");
                    int data_size = data.length;

                    Log.e("aaa", data_size + "");

                    byte[] sendData = new byte[data_size + 9];

                    sendData[0] = 0x02;

                    byte[] tempA = intToByteArray(serviceCode);

                    System.arraycopy(tempA, 0, sendData, 1, tempA.length);

                    byte[] tempB = intToByteArray(data_size);

                    System.arraycopy(tempB, 0, sendData, 5, tempB.length);

                    System.arraycopy(data, 0, sendData, 9, data_size);

                    outputStream.write(sendData);

                    in = new BufferedReader(new InputStreamReader(inetSocket.getInputStream()));
                    return_msg = in.readLine();

                    Log.e("return_msg", return_msg);

                    JSONObject jj = new JSONObject(return_msg);

                    Log.e("jja", jj.getString("msg"));

                    Log.e("TCP", "return msg : " + return_msg);

                } catch (Exception e) {
                    Log.e("TCP", "C: Error1 ", e);
                    return jException1.toString();
                } finally {
                    try {
                        inetSocket.close();
                    } catch (Exception e) {
                        Log.e("TCP", "C: Error2 ", e);
                    }

                    try {
                        outputStream.close();
                    } catch (Exception e) {
                        Log.e("TCP", "C: Error3 ", e);
                    }

                    try {
                        in.close();
                    } catch (Exception e) {
                        Log.e("TCP", "C: Error4 ", e);
                    }
                }

            } catch (Exception e) {
                Log.e("TCP", "C: Error5 ", e);
                return jException2.toString();
            }

            return return_msg;

        }
    }

}
