/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.video_list_demo.adapter.items;

import android.net.Uri;
import android.view.View;

import com.picpic.sikkle.utils.video_list_demo.adapter.holders.VideoViewHolder;
import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.Config;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;
import com.volokh.danylo.video_player_manager.utils.Logger;

public class UrlVideoItem extends BaseVideoItem{

    private static final String TAG = UrlVideoItem.class.getSimpleName();
    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;

    private final String mTitle;

    private final Picasso mImageLoader;
//    private final int mImageResource;
    private final String url;
    private final String imgUrl;

    public UrlVideoItem(String title, String url, VideoPlayerManager<MetaData> videoPlayerManager, Picasso imageLoader, String imgUrl) {
        super(videoPlayerManager);
        mTitle = title;
        this.url = url;
        mImageLoader = imageLoader;
        this.imgUrl = imgUrl;
    }

    @Override
    public void update(int position, final VideoViewHolder viewHolder, VideoPlayerManager videoPlayerManager) {
        if(SHOW_LOGS) Logger.v(TAG, "update, position " + position);

        viewHolder.mTitle.setText(mTitle);
//        viewHolder.mCover.setVisibility(View.VISIBLE);
//        viewHolder.mCover.
//        mImageLoader.load(mImageResource).into(viewHolder.mCover);
//        mImageLoader.load(Uri.parse(imgUrl)).into(viewHolder.mCover);
//        mImageLoader.lo
    }


    @Override
    public void playNewVideo(MetaData currentItemMetaData, VideoPlayerView player, VideoPlayerManager<MetaData> videoPlayerManager) {
        videoPlayerManager.playNewVideo(currentItemMetaData, player, url);
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback();
    }

    @Override
    public String toString() {
        return getClass() + ", mTitle[" + mTitle + "]";
    }
}
