/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.video_list_demo.adapter.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;


public class VideoViewHolder extends RecyclerView.ViewHolder {

    public final LinearLayout layTopBlank;
    public final LinearLayout layRow;
    public final TextView mTitle;
    public final VideoPlayerView mPlayer;
    public final BoundableOfflineImageView mCover;
    public final LikeView likeView;
    public final FrameLayout layContent;
    public final CircleImageView cimv;
    public final TextView tvId;
    public final LinearLayout layMore;
    public final TextView tvTags;
    public final LinearLayout layLike;
    public final TextView tvLikeCnt;
    public final LinearLayout layComment;
    public final TextView tvComCnt;
    public final LinearLayout layPlayCnt;
    public final TextView tvPlayCnt;
    public final Button btnShare;

    public final LinearLayout layAd;
    public final TextView tvAdTitle;
    public final BoundableOfflineImageView bimvAd;
    public final ImageView imvAdIcon;
    public final TextView tvAdSubTitle;
    public final Button btnAdAction;

    public VideoViewHolder(View view) {
        super(view);
        layTopBlank = (LinearLayout)view.findViewById(R.id.lay_recycler_row_timeline_top_blank);
        layRow = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline);
        mPlayer = (VideoPlayerView) view.findViewById(R.id.vv_recycler_row_timeline);
        mTitle = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_body);
        mCover = (BoundableOfflineImageView) view.findViewById(R.id.bimv_recycler_row_timeline);
        likeView = (LikeView) view.findViewById(R.id.imv_recycler_row_timeline_like);
        layContent = (FrameLayout) view.findViewById(R.id.lay_recycler_row_timeline_content);
        cimv = (CircleImageView) view.findViewById(R.id.cimv_recycler_row_timeline);
        tvId = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_id);
        layMore = (LinearLayout) view.findViewById(R.id.imv_recycler_row_timeline_more);
        tvTags = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_tags);
        layLike = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline_like);
        tvLikeCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_like_count);
        layComment = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline_comment);
        tvComCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_comment_count);
        layPlayCnt = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline_play_count);
        tvPlayCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_play_count);
        btnShare = (Button) view.findViewById(R.id.btn_recycler_row_timeline_share);

        layAd = (LinearLayout) view.findViewById(R.id.lay_ad_row);
        tvAdTitle = (TextView) view.findViewById(R.id.tv_ad_row_title);
        bimvAd = (BoundableOfflineImageView) view.findViewById(R.id.bimv_ad_row);
        imvAdIcon = (ImageView) view.findViewById(R.id.imv_ad_row);
        tvAdSubTitle = (TextView) view.findViewById(R.id.tv_ad_row_sub_title);
        btnAdAction = (Button) view.findViewById(R.id.btn_ad_row_do_it);
    }
}