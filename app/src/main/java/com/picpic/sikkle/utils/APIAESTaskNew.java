package com.picpic.sikkle.utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.picpic.sikkle.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jong-min on 2015-10-08.
 */
public class APIAESTaskNew extends AsyncTask<Void, Void, Void> {

    Map<String, String> params;
    int serviceCode = 0;
    JSONObject inputJO;
    StringTransMethod stm;
    Context con;
    JSONObject timeOutResult;
    JSONObject nonNetworkResult;

    String return_msg = "";
    private String sendMSG = "abcdeabcdeabcde";

    public APIAESTaskNew(Context context, Map<String, String> param, int code, StringTransMethod stmm) {

        timeOutResult = new JSONObject();
        try {
            timeOutResult.put("result", 100);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nonNetworkResult = new JSONObject();
        try {
            nonNetworkResult.put("result", 1000);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.con = context;

        this.params = param;
        this.serviceCode = code;
        this.stm = stmm;

        if (AppController.NETWORK_STATUS == 0) {
            Handler handler = new Handler(AppController.context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AppController.context, AppController.context.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();

                    stm.set_result(return_msg);
                }
            });
            return_msg = nonNetworkResult.toString();
            return;
        } else if (AppController.NETWORK_STATUS == NetworkUtil.TYPE_3G) {
            Handler handler = new Handler(AppController.context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AppController.context, AppController.context.getResources().getString(R.string.warning_3g), Toast.LENGTH_SHORT).show();
                }
            });
        }

        inputJO = new JSONObject();
        Iterator<String> iter = params.keySet().iterator();
        try {
            String key = null, value = null;
            while (iter.hasNext()) {
                key = iter.next();
                value = params.get(key);

                inputJO.put(key, value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("input", inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));

        sendMSG = inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]");
    }

    @Override
    protected Void doInBackground(Void... params) {

        OutputStream outputStream = null;
        BufferedReader in = null;
        InputStream inputStream;

        if (AppController.apiSocket != null) {
            try {
                AppController.apiSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            AppController.apiSocket = new Socket("210.122.9.21", 34200);
            AppController.apiSocket.setSoTimeout(30000);
            outputStream = AppController.apiSocket.getOutputStream();
            inputStream = AppController.apiSocket.getInputStream();

//            byte[] data = MinUtils.AES_Encode(sendMSG.getBytes("UTF-8"));
            Log.e("msg", sendMSG);
            byte[] data = MinUtils.AES_Encode(sendMSG.getBytes("UTF-8"));

            String temp = "";

            for (int i = 0; i < data.length; i++) {
//                char tempc =

//                System.out.format("", temp);
                temp += data[i];
            }

            Log.e("teaaaaaa", temp);

            int data_size = data.length;

            byte[] sendData = new byte[data_size + 9];

            sendData[0] = 0x02;


            byte[] tempA = intToByteArray(serviceCode);

            System.arraycopy(tempA, 0, sendData, 1, tempA.length);

            byte[] tempB = intToByteArray(data_size);

            System.arraycopy(tempB, 0, sendData, 5, tempB.length);

            System.arraycopy(data, 0, sendData, 9, data_size);


            outputStream.write(sendData);

            in = new BufferedReader(new InputStreamReader(AppController.apiSocket.getInputStream()));

            inputStream = AppController.apiSocket.getInputStream();

            int size = 50 * 1024;

            BufferedInputStream bis = new BufferedInputStream(inputStream);
//
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int red = 0;
            byte[] buf = new byte[size];
            while ((red = bis.read(buf)) != -1) {
                baos.write(buf, 0, red);
                String tempS = new String(baos.toByteArray());
                if (tempS.indexOf("\r\n\n") != -1) {
                    break;
                }
            }
//
            return_msg = new String(baos.toByteArray());

        } catch (SocketTimeoutException e) {
            Log.e("TCP", "C: Error1-1 ", e);
            Handler handler = new Handler(AppController.context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AppController.context, AppController.context.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();

                    stm.set_result(return_msg);
                }
            });
            return_msg = nonNetworkResult.toString();
        } catch (Exception e) {
            Log.e("TCP", e.toString());
            return_msg = "";
        } finally {
            try {
                if (AppController.apiSocket != null)
                    AppController.apiSocket.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            }

            try {
                if (outputStream != null)
                    outputStream.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error3 ", e);
            }

            try {
                if (in != null)
                    in.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error4 ", e);
            }

            try {
                if (!return_msg.equals("")) {
                    ((Activity) con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stm.set_result(return_msg);
                        }
                    });
                }
            } catch (NullPointerException e) {
                ((Activity) con).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stm.set_result(timeOutResult.toString());
                    }
                });
            }
        }

        return null;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

//    @Override
//    protected void onPostExecute(Void result) {
//        try{
//            if (!return_msg.equals("")) {
//                stm.set_result(return_msg);
//            }
//        }catch (NullPointerException e){
//            stm.set_result(timeOutResult.toString());
//        }
//        super.onPostExecute(result);
//    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }
}
