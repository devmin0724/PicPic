package com.picpic.sikkle.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class ImageDownload extends AsyncTask<String, Void, Long> {
    String url = "", contentType = "";
    ImageView imv;
    File f;

    public ImageDownload(String url, ImageView imv) {
        this.url = url;
        this.imv = imv;
    }

    @Override
    protected Long doInBackground(String... params) {
        HttpURLConnection conn = null;
        OutputStream os = null;
        try {
            f = AppController.fileCache.getFile(url);
            URL imageUrl = null;
            imageUrl = new URL(url);
            conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            contentType = conn.getContentType();
            InputStream is = conn.getInputStream();
            os = new FileOutputStream(f);
            AppController.CopyStream(is, os);
            os.close();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            conn.disconnect();
        }

        return 0L;

    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
//            Obhj = AppController.fileCache.getFile(url);

        Log.e("ct", contentType);
        Log.e("downurl", url);

        if (!"image/gif".equals(contentType)) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);
            Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
            imv.setImageBitmap(b);
        } else {
            try {
                GifDrawable gd = new GifDrawable(f);
                imv.setImageDrawable(gd);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //"image/gif".equals(contentType)


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
