/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.cache;

import android.app.Activity;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fingram.agifEncoder.QAGIFEncoder;
import com.fingram.qrb.QrBitmapFactory;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

public class VGLoader {

    private VGMemoryCache memoryCache = new VGMemoryCache();
    private AbstractFileCache fileCache;
    private Map<ImageView, String> vViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());
    private ExecutorService executorService;
    private Context con;

    public VGLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(3);
        this.con = context;

    }

    public void DisplayVG(String url, ImageView imv, boolean isLoadOnlyFromCache) {
        vViews.put(imv, url);

        File file = memoryCache.get(url);

        if (isLoadOnlyFromCache) {
            if (file != null) {
                Glide.with(con)
                        .load(file)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.nowloading)
                        .error(R.drawable.non_interest)
                        .into(imv);
//                try {
//                    GifDrawable gd = new GifDrawable(file);
//                    gd.setLoopCount(0);
//                    imv.setImageDrawable(gd);
//                    gd.start();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else {
                imv.setImageResource(R.drawable.non_p);
            }
        } else {
            if (file != null) {
                Glide.with(con)
                        .load(file)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.nowloading)
                        .error(R.drawable.non_interest)
                        .into(imv);
//                try {
//                    GifDrawable gd = new GifDrawable(file);
//                    gd.setLoopCount(0);
//                    imv.setImageDrawable(gd);
//                    gd.start();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } else {
                queueVG(url, imv);
            }
        }

    }

    private void queueVG(String url, ImageView imv) {
        VGToLoad p = new VGToLoad(url, imv);
        executorService.submit(new VViewLoader(p));
    }

    // Task for the queue
    private class VGToLoad {
        public String url;
        public ImageView imv;

        public VGToLoad(String u, ImageView i) {
            url = u;
            imv = i;
        }
    }

    class VViewLoader implements Runnable {
        VGToLoad vgToLoad;

        VViewLoader(VGToLoad vgToLoad) {
            this.vgToLoad = vgToLoad;
        }

        @Override
        public void run() {
            if (VViewReused(vgToLoad))
                return;
            File f = getFile(vgToLoad.url);
            memoryCache.put(vgToLoad.url, f);
            if (VViewReused(vgToLoad))
                return;
            VGDisplayer bd = new VGDisplayer(f, vgToLoad);
            Activity a = (Activity) vgToLoad.imv.getContext();
            a.runOnUiThread(bd);
        }
    }

    private File getFile(String url) {
        File f = fileCache.getFile(url);

        File ff = null;
        if (f != null && f.exists()) {
            ff = f;
        }
        if (ff != null) {
            return ff;
        }

        try {
            File file = null;

            File tempFile1 = new File(AppController.TEMP_SRC + String.valueOf(System.currentTimeMillis()) + "1");
            File tempFile2 = new File(AppController.TEMP_SRC + String.valueOf(System.currentTimeMillis()) + "2");
            Log.e("srcccc", AppController.TEMP_SRC + String.valueOf(System.currentTimeMillis()) + "2");
//            File tempFile2 = new File(AppController.BASE_SRC + String.valueOf(System.currentTimeMillis()) + "2");
            Log.e("Download url", url);
            URL fileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) fileUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(tempFile1);
            CopyStream(is, os);
            os.close();

            QAGIFEncoder encoder2 = new QAGIFEncoder();
            encoder2.setRepeat(0);
            encoder2.setDelay(50);

            encoder2.start(tempFile2.getAbsolutePath());

            MediaPlayer mp = MediaPlayer.create(con, Uri.parse(tempFile1.getAbsolutePath()));
            int duration = mp.getDuration()*1000;


            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(tempFile1.getAbsolutePath());

            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();

            for (int i = 0; i <= duration; i = i + 50000) {
                encoder2.addFrame(mmr.getFrameAtTime(i));
//                QrBitmapFactory.compressToFile(mmr.getFrameAtTime(i), QrBitmapFactory.QrJPEG, AppController.BASE_SRC + "test/tt" + i, 100, opt);
//                QrBitmapFactory.compressToFile(mmr.getFrameAtTime(i), QrBitmapFactory.QrJPEG, Environment.getExternalStorageDirectory() + "/tt" + i+".jpg", 100, opt);
//                QrBitmapFactory.compressToFile(mmr.getFrameAtTime(i), QrBitmapFactory.QrJPEG, AppController.TEMP_SRC2 + "tt" + i+".jpg", 100, opt);
            }

            encoder2.finish();

            return tempFile2;
        } catch (Exception ex) {
            Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
            return null;
        }
    }

    boolean VViewReused(VGToLoad vgToLoad) {
        String tag = vViews.get(vgToLoad.imv);
        if (tag == null || !tag.equals(vgToLoad.url))
            return true;
        return false;
    }

    class VGDisplayer implements Runnable {
        File file;
        VGToLoad vgToLoad;

        public VGDisplayer(File f, VGToLoad v) {
            file = f;
            vgToLoad = v;
        }

        public void run() {
            if (VViewReused(vgToLoad))
                return;
            if (file != null) {
                Glide.with(con)
                        .load(file)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.nowloading)
                        .error(R.drawable.non_interest)
                        .into(vgToLoad.imv);
//                try {
//                    GifDrawable gd = new GifDrawable(file);
//                    gd.setLoopCount(0);
//                    vgToLoad.imv.setImageDrawable(gd);
//                    gd.start();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
        vViews.clear();
        executorService.shutdownNow();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e("", "CopyStream catch Exception...");
        }
    }

}