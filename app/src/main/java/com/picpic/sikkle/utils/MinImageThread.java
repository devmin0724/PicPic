package com.picpic.sikkle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class MinImageThread extends Thread{
    String url = "", contentType = "";
    ImageView imv;
    Object obj;
    long nowSize;
    Activity ac;
    int navi;
    File f;

    /**
     * Constructs a new {@code Thread} with no {@code Runnable} object and a
     * newly generated name. The new {@code Thread} will belong to the same
     * {@code ThreadGroup} as the {@code Thread} calling this constructor.
     *
     * @see ThreadGroup
     * @see Runnable
     */
    public MinImageThread(Activity activity, String url, ImageView imv, Object obj, long size, int nav) {
        this.imv = imv;
        this.obj = obj;
        this.nowSize = size;
        this.ac = activity;
        this.navi = nav;
        this.url = url;
    }

    @Override
    public void run() {
        URL urls = null;
        URLConnection urlConnection = null;
        try {
            urls = new URL(url);
            urlConnection = urls.openConnection();
            urlConnection.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final long file_size1 = urlConnection.getContentLength();

        if (file_size1 == nowSize) {
            if (obj instanceof Bitmap) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageBitmap((Bitmap) obj);
                    }
                });
            } else if (obj instanceof Drawable) {
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imv.setImageDrawable((Drawable) obj);
                    }
                });
            } else {
                //TODO 다운로드
                Log.e("down1", "ok");
                HttpURLConnection conn = null;
                OutputStream os = null;
                try {
                    f = AppController.fileCache.getFile(url);
                    URL imageUrl = null;
                    imageUrl = new URL(url);
                    conn = (HttpURLConnection) imageUrl.openConnection();
                    conn.setConnectTimeout(30000);
                    conn.setReadTimeout(30000);
                    conn.setInstanceFollowRedirects(true);
                    contentType = conn.getContentType();
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();

                    Log.e("contype", contentType);

                    if (!"image/gif".equals(contentType)) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);
                        Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                        imv.setImageBitmap(b);
                    } else {
                        try {
                            GifDrawable gd = new GifDrawable(f);
                            imv.setImageDrawable(gd);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    conn.disconnect();
                }
            }
        } else {
            //TODO 다운로드
            Log.e("down2", "ok");
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                f = AppController.fileCache.getFile(url);
                URL imageUrl = null;
                imageUrl = new URL(url);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                contentType = conn.getContentType();
                InputStream is = conn.getInputStream();
                os = new FileOutputStream(f);
                AppController.CopyStream(is, os);
                os.close();

                if (!"image/gif".equals(contentType)) {
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap b = QrBitmapFactory.decodeFile(f.getAbsolutePath(), opt);
                    Bitmap b = BitmapUtils.decodeFile(f.getAbsolutePath());
                    imv.setImageBitmap(b);
                } else {
                    try {
                        GifDrawable gd = new GifDrawable(f);
                        imv.setImageDrawable(gd);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
        }
    }
}
