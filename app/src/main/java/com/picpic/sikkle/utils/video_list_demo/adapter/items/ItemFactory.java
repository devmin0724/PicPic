/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.video_list_demo.adapter.items;

import android.app.Activity;

import com.picpic.sikkle.beans.TimelineItem;
import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;

import java.io.IOException;

public class ItemFactory {

    public static BaseVideoItem createItemFromAsset(String assetName, int imageResource, Activity activity, VideoPlayerManager<MetaData> videoPlayerManager) throws IOException {
        return new AssetVideoItem(assetName, activity.getAssets().openFd(assetName), videoPlayerManager, Picasso.with(activity), imageResource);
    }

    public static BaseVideoItem createItemFromUrl(String url, String imgUrl, Activity activity, VideoPlayerManager<MetaData> videoPlayerManager) {
        return new UrlVideoItem(url, url, videoPlayerManager, Picasso.with(activity), imgUrl);
    }

    public static BaseVideoItem createItemFromModel(Activity ac, TimelineItem item, VideoPlayerManager videoPlayerManager, int navi) {
        return new TimelineVideoItem(videoPlayerManager, item, Picasso.with(ac), ac, navi);
    }
    public static BaseVideoItem createItemFromModel2(Activity ac, TimelineItem item, VideoPlayerManager videoPlayerManager, int navi) {
        return new TimelineVideoItem2(videoPlayerManager, item, Picasso.with(ac), ac, navi);
    }
}
