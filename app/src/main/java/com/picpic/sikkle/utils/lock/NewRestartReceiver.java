package com.picpic.sikkle.utils.lock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NewRestartReceiver extends BroadcastReceiver {

	static public final String ACTION_RESTART_SERVICE = "RestartReceiver.restart"; // 값은
																					// 맘대로

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("test Lof", "NewRestartReceiver_onReceive");
		if (intent.getAction().equals(ACTION_RESTART_SERVICE)) {
			Intent i = new Intent(context, NewLockService.class);
			context.startService(i);
		}

		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Log.d("RestartService", "ACTION_BOOT_COMPLETED");
			Intent i = new Intent(context, NewLockService.class);
			context.startService(i);
		}

	}
}