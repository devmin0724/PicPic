package com.picpic.sikkle.utils.lock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class NewPersistentService extends Service {

	@Override
	public void onCreate() {
		Log.d("PersistentService", "onCreate");
		unregisterRestartAlarm(); // 이미 등록된 알람이 있으면 제거
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		Log.d("PersistentService", "onDestroy");
		registerRestartAlarm(); // 서비스가 죽을때 알람을 등록
		super.onDestroy();
	}

	// support persistent of Service
	public void registerRestartAlarm() {
		Log.d("PersistentService", "registerRestartAlarm");
		Intent intent = new Intent(NewPersistentService.this,
				NewRestartReceiver.class);
		intent.setAction(NewRestartReceiver.ACTION_RESTART_SERVICE);
//		PendingIntent sender = PendingIntent.getBroadcast(
//				NewPersistentService.this, 0, intent, 0);
//		long firstTime = SystemClock.elapsedRealtime();
//		firstTime += 100; // 0.1초 후에 알람이벤트 발생
//		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
//		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 100,
//				sender);
		startService(intent);
	}

	public void unregisterRestartAlarm() {
		Log.d("PersistentService", "unregisterRestartAlarm");
		Intent intent = new Intent(NewPersistentService.this,
				NewRestartReceiver.class);
		intent.setAction(NewRestartReceiver.ACTION_RESTART_SERVICE);
		PendingIntent sender = PendingIntent.getBroadcast(
				NewPersistentService.this, 0, intent, 0);
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		am.cancel(sender);
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.d("RestartService", "onBindonBindonBindonBindonBindonBindonBind");
		return null;
	}

}
