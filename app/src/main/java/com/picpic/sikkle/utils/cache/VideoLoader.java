/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.cache;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.picpic.sikkle.adapter.TimeLineAdapterV1;
import com.picpic.sikkle.widget.VView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VideoLoader {

    private VideoMemoryCache memoryCache = new VideoMemoryCache();
    private AbstractFileCache fileCache;
    private Map<VView, String> vViews = Collections
            .synchronizedMap(new WeakHashMap<VView, String>());
    //    private Map<VView, String> vViews = Collections
//            .synchronizedMap(new WeakHashMap<VView, String>());
    private ExecutorService executorService;
    private Context con;
    private Activity ac;

    public VideoLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(3);
        this.con = context;
    }

    public String checkUrl(String url) {
        File f = memoryCache.get(url);

        if (f != null) {
            return f.getAbsolutePath();
        } else {
            f = fileCache.getFile(url);
            if (f != null) {
                return f.getAbsolutePath();
            } else {
                f = getFile(url);
                memoryCache.put(url, f);
                return f.getAbsolutePath();
            }
        }
    }

    public void DisplayVideo(String url, VView vView, boolean isLoadOnlyFromCache) {
        vViews.put(vView, url);

        File file = memoryCache.get(url);

//        if (isLoadOnlyFromCache) {
//            if (file != null) {
//                vView.setVideoPath(file.getAbsolutePath());
////            vView.setVideoURI(Uri.parse(file.getAbsolutePath()));
//                vView.start();
//            } else {
//                Uri uri = Uri.parse("android.resource://" + con.getPackageName() + "/" + R.raw.blank);
//                vView.setVideoURI(uri);
////                String fileName = "android.resource://" + con.getPackageName() + "/raw/blank.mp4";
////                vView.setVideoPath(fileName);
////                vView.start();
////                Log.e("blank", "blank");
//            }
//        } else {
//            if (file != null) {
//                vView.setVideoPath(file.getAbsolutePath());
////            vView.setVideoURI(Uri.parse(file.getAbsolutePath()));
//                vView.start();
//            } else {
//                queueVideo(url, vView);
//            }
//        }

        if (file != null) {
//            vView.setVideoPath(file.getAbsolutePath());
//            vView.setContent(getUriFromPath(file.getAbsolutePath()));
//            vView.setVideoURI(Uri.parse(file.getAbsolutePath()));
            vView.setVideoPath(file.getAbsolutePath());
            vView.start();
            vView.setVisibility(View.VISIBLE);
//            vView.setSource(file.getAbsolutePath());
//            try {
//                vView.setDataSource(file.getAbsolutePath());
//                vView.start();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            vView.setVideoURI(Uri.fromFile(file));
        } else if (!isLoadOnlyFromCache) {
            queueVideo(url, vView);
        }
    }

    public void DownloadVideo(String url, Activity ac) {
        this.ac = ac;

        File file = memoryCache.get(url);

        if (file == null) {
            queueVideo2(url);
        }
    }

    private void queueVideo(String url, VView vView) {
        VideoToLoad p = new VideoToLoad(url, vView);
        executorService.submit(new VViewLoader(p));
    }

    private void queueVideo2(String url) {
        VideoToLoad2 p = new VideoToLoad2(url);
        executorService.submit(new VViewLoader2(p));
    }

    // Task for the queue
    private class VideoToLoad {
        public String url;
        public VView vView;

        public VideoToLoad(String u, VView i) {
            url = u;
            vView = i;
        }
    }

    private class VideoToLoad2 {
        public String url;

        public VideoToLoad2(String u) {
            url = u;
        }
    }

    class VViewLoader implements Runnable {
        VideoToLoad videoToLoad;

        VViewLoader(VideoToLoad videoToLoad) {
            this.videoToLoad = videoToLoad;
        }

        @Override
        public void run() {
            if (VViewReused(videoToLoad))
                return;
//			File f = fileCache.getFile(videoToLoad.url);
            File f = getFile(videoToLoad.url);
            memoryCache.put(videoToLoad.url, f);
            if (VViewReused(videoToLoad))
                return;
            VideoDisplayer bd = new VideoDisplayer(f, videoToLoad);
//            Activity a = (Activity) videoToLoad.vView.getContext();
//            a.runOnUiThread(bd);
            ac.runOnUiThread(bd);
        }
    }

    class VViewLoader2 implements Runnable {
        VideoToLoad2 videoToLoad;

        VViewLoader2(VideoToLoad2 videoToLoad) {
            this.videoToLoad = videoToLoad;
        }

        @Override
        public void run() {
            memoryCache.put(videoToLoad.url, getFile2(videoToLoad.url));
        }
    }

    private File getFile(String url) {
        File f = fileCache.getFile(url);

        File ff = null;
        if (f != null && f.exists()) {
            ff = f;
        }
        if (ff != null) {
            return ff;
        }
        try {
            File file = null;
            Log.e("Download url", url);
            URL fileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) fileUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            file = f;
            return file;
        } catch (Exception ex) {
            Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
            return null;
        }
    }

    private File getFile2(String url) {
        File f = fileCache.getFile(url);

        File ff = null;
        if (f != null && f.exists()) {
            ff = f;
        }
        if (ff != null) {
            return ff;
        }
        try {
            File file = null;
            Log.e("Download url", url);
            URL fileUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) fileUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            CopyStream(is, os);
            os.close();
            file = f;
            return file;
        } catch (Exception ex) {
            Log.e("", "getBitmap catch Exception...\nmessage = " + ex.getMessage());
            return null;
        }
    }

    boolean VViewReused(VideoToLoad videoToLoad) {
        String tag = vViews.get(videoToLoad.vView);
        if (tag == null || !tag.equals(videoToLoad.url))
            return true;
        return false;
    }

    class VideoDisplayer implements Runnable {
        File file;
        VideoToLoad videoToLoad;

        public VideoDisplayer(File f, VideoToLoad v) {
            file = f;
            videoToLoad = v;
        }

        public void run() {
            if (VViewReused(videoToLoad))
                return;
            if (file != null) {
//                videoToLoad.vView.setVideoPath(file.getAbsolutePath());
//                videoToLoad.vView.setSource(file.getAbsolutePath());
//                videoToLoad.vView.startPlayback();
//                videoToLoad.vView.setVideoURI(Uri.fromFile(file));
                videoToLoad.vView.setVideoPath(file.getAbsolutePath());
                videoToLoad.vView.start();
                videoToLoad.vView.setVisibility(View.VISIBLE);

//                try {
//                    videoToLoad.vView.setDataSource(file.getAbsolutePath());
//                    videoToLoad.vView.start();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                videoToLoad.vView.setContent(getUriFromPath(file.getAbsolutePath()));
//                videoToLoad.vView.setVideoURI(Uri.parse(file.getAbsolutePath()));
//                videoToLoad.vView.start();
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
        vViews.clear();
        executorService.shutdownNow();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e("", "CopyStream catch Exception...");
        }
    }

    public Uri getUriFromPath(String path) {

        Uri fileUri = Uri.parse(path);
        String filePath = fileUri.getPath();
        Cursor cursor = con.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, "_data = '" + filePath + "'", null, null);
        cursor.moveToNext();
        int id = cursor.getInt(cursor.getColumnIndex("_id"));
        Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);

        return uri;
    }
}