/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils.video_list_demo.adapter.items;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.ui.CategoryListActivity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.utils.video_list_demo.adapter.holders.VideoViewHolder;
import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.Config;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.MediaPlayerWrapper;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;
import com.volokh.danylo.video_player_manager.utils.Logger;

public class TimelineVideoItem2 extends BaseVideoItem {

    private static final String TAG = TimelineVideoItem2.class.getSimpleName();
    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;

    private final TimelineItem model;
    private final Picasso mImageLoader;
    private final Activity ac;
    private final VideoLoader videoLoader;

    private NativeAd nativeAd;
    private ImageLoader imageLoader;
    private ImageFullLoader imageFullLoader;

    private int playCnt;
    private final int navi;

    public String getUrl() {
        return AppController.URL + model.getUrl();
    }

    private Activity getActivity() {
        return ac;
    }

    public TimelineVideoItem2(VideoPlayerManager<MetaData> videoPlayerManager, TimelineItem model, Picasso mImageLoader, Activity ac, int navi) {
        super(videoPlayerManager);
        this.model = model;
        this.mImageLoader = mImageLoader;
        this.ac = ac;
        this.navi = navi;

        this.imageLoader = new ImageLoader(getActivity());
        this.imageFullLoader = new ImageFullLoader(getActivity());
        this.videoLoader = new VideoLoader(getActivity());
//        mGestureDetector = new GestureDetector(this, TestGestureListener);

        playCnt = model.getPlayCnt();
    }

    public TimelineItem getItem(){
        return model;
    }

    public void setLike(String id, boolean is, int ct){
        model.setPsot_id(id);
        model.setLike(is);
        model.setLikeCount(ct);
    }

    @Override
    public void update(final int position, final VideoViewHolder viewHolder, final VideoPlayerManager videoPlayerManager) {
        if (SHOW_LOGS) Logger.v(TAG, "update, position " + position);

        if (!model.isAd()) {

            String tempThubUrl = AppController.URL + model.getUrl().replace("_2.gif", ".jpg");
//            Glide.with(ac)
//                    .load(tempThubUrl)
//                    .placeholder(R.drawable.nowloading)
//                    .error(R.drawable.non_interest)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .fitCenter()
//                    .into(viewHolder.mCover);
            imageFullLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//            imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//            viewHolder.mCover.setTag(tempThubUrl);

//            try {
//                String tag = viewHolder.mCover.getTag().toString();
//                if (tag.equals(tempThubUrl)) {
//
//                } else {
//                    imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//                    viewHolder.mCover.setTag(tempThubUrl);
//                }
//            } catch (NullPointerException e) {
//                imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//                viewHolder.mCover.setTag(tempThubUrl);
//            }

//            imageFullLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//            imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//            viewHolder.mCover.setTag(tempThubUrl);


//            if (position == 0) {
//                viewHolder.layTopBlank.setVisibility(View.VISIBLE);
//            } else {
                viewHolder.layTopBlank.setVisibility(View.GONE);
//            }

            String tempUrl = model.getUrl().replace(".gif", ".mp4");

            viewHolder.layRow.setVisibility(View.VISIBLE);
            viewHolder.layAd.setVisibility(View.GONE);

            viewHolder.likeView.setVisibility(View.INVISIBLE);


            viewHolder.layContent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                @Override
                public void onClick() {
                    super.onClick();
                    switch (viewHolder.mPlayer.isPlaying()) {
                        case 0:
                            viewHolder.mPlayer.pause();
                            break;
                        case 1:
                            viewHolder.mPlayer.start();
                            break;
                        case 2:

                            break;
                    }
                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();
                    if (viewHolder.layLike.isSelected()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                viewHolder.tvLikeCnt.setText(lc - 1 + "");
                                viewHolder.layLike.setSelected(false);
                                model.setLikeCount(lc - 1);
                            }
                        });
                        MinUtils.like(true, getActivity(), model.getPsot_id());

                    } else {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                viewHolder.tvLikeCnt.setText(lc + 1 + "");
                                viewHolder.layLike.setSelected(true);
                                model.setLikeCount(lc + 1);
                            }
                        });
                        viewHolder.likeView.startAni();
                        MinUtils.like(false, getActivity(), model.getPsot_id());

                    }

                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(getActivity(), GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    getActivity().startActivity(i);
                }
            });

            viewHolder.cimv.setTag(model.getOwnerId());

            imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimv, false);
            viewHolder.tvId.setText(model.getId());

            MinUtils.setCount(viewHolder.tvLikeCnt, model.getLikeCount());
            MinUtils.setCount(viewHolder.tvComCnt, model.getComCount());
            MinUtils.setCount(viewHolder.tvPlayCnt, model.getPlayCnt());

            MinUtils.setTags(ac, viewHolder.tvTags, model.getTags());

            if (model.isLike()) {
                viewHolder.layLike.setSelected(true);
            } else {
                viewHolder.layLike.setSelected(false);
            }

            viewHolder.layLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewHolder.layLike.isSelected()) {
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                viewHolder.tvLikeCnt.setText(lc - 1 + "");
                                viewHolder.layLike.setSelected(false);
                                model.setLikeCount(lc - 1);
                            }
                        });
                        MinUtils.like(true, ac, model.getPsot_id());

                    } else {

                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int lc = model.getLikeCount();
                                viewHolder.tvLikeCnt.setText(lc + 1 + "");
                                viewHolder.layLike.setSelected(true);
                                model.setLikeCount(lc + 1);
                            }
                        });
                        viewHolder.likeView.startAni();
                        MinUtils.like(false, ac, model.getPsot_id());

                    }
                }
            });

            viewHolder.layComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    ac.startActivityForResult(i, CategoryListActivity.CATEGORY_RETURN);
                }
            });

            viewHolder.btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SharePopUp.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", 0);
                    i.putExtra("h", 0);
                    ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });

            viewHolder.layMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            viewHolder.tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    ac.startActivityForResult(i, CategoryListActivity.CATEGORY_RETURN);
                }
            });
            viewHolder.tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    ac.startActivity(i);
                }
            });

            viewHolder.cimv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = viewHolder.cimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });

            viewHolder.tvId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = viewHolder.cimv.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });


            if (model.getBody().equals("") || model.getBody().length() <= 0) {
                viewHolder.mTitle.setVisibility(View.GONE);
            } else {
                viewHolder.mTitle.setVisibility(View.VISIBLE);
            }

            viewHolder.mTitle.setText(model.getBody());

            int w = model.getWidth();
            int h = model.getHeight();

            int height = 0;

            try {
                height = h * MinUtils.screenWidth / w;
            } catch (ArithmeticException e) {

            }

            AppController.imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimv, true);
//            Glide.with(ac).load(AppController.URL + model.getPro_url()).into(viewHolder.cimv);

//            viewHolder.mCover.setVisibility(View.VISIBLE);
//            mImageLoader.load(Uri.parse(AppController.URL + tempThubUrl)).into(viewHolder.mCover);
//            imageFullLoader.DisplayImage(AppController.URL + tempThubUrl, viewHolder.mCover, false);

            viewHolder.layRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    ac.startActivityForResult(i, CategoryListActivity.CATEGORY_RETURN);
//                    getActivity().startActivity(i);
                }
            });

            viewHolder.mPlayer.addMediaPlayerListener(new MediaPlayerWrapper.MainThreadMediaPlayerListener() {
                @Override
                public void onVideoSizeChangedMainThread(int width, int height) {

                }

                @Override
                public void onVideoPreparedMainThread() {
//                    viewHolder.mCover.setVisibility(View.INVISIBLE);
//                    viewHolder.mPlayer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onVideoCompletionMainThread() {
                    AppController.playCountExe.execute(new Runnable() {
                        @Override
                        public void run() {
                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                        }
                    });

                    MinUtils.setCount(viewHolder.tvPlayCnt, playCnt + 1);
                    playCnt = playCnt + 1;
                }

                @Override
                public void onErrorMainThread(int what, int extra) {

                }

                @Override
                public void onBufferingUpdateMainThread(int percent) {

                }

                @Override
                public void onVideoStoppedMainThread() {
//                    viewHolder.mCover.setVisibility(View.VISIBLE);
//                    viewHolder.mPlayer.setVisibility(View.INVISIBLE);
                }
            });

//            viewHolder.mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    AppController.playCountExe.execute(new Runnable() {
//                        @Override
//                        public void run() {
//                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
//                        }
//                    });
//
//                    MinUtils.setCount(viewHolder.tvPlayCnt, playCnt + 1);
//                    playCnt = playCnt + 1;
//                }
//            });
//
//            videoLoader.DisplayVideo(AppController.URL + model.getUrl().replace(".gif", ".mp4"), viewHolder.mPlayer, false);

//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
//
//            viewHolder.layContent.setLayoutParams(params);

//            String tempThubUrl = AppController.URL + model.getUrl().replace("_2.gif", ".jpg");
//            imageFullLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//            imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);

//            TimelineV1NewActivity.ac.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    String tempThubUrl = AppController.URL + model.getUrl().replace("_2.gif", ".jpg");
////            imageFullLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
////            imageLoader.DisplayImage(tempThubUrl, viewHolder.mCover, false);
//                    try {
//                        Bitmap b = mImageLoader.load(Uri.parse(tempThubUrl)).get();
//                        BitmapDrawable bitmapDrawable = new BitmapDrawable(b);
//                        viewHolder.mPlayer.setBackgroundDrawable(bitmapDrawable);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            });


//            viewHolder.mCover.setTag(tempThubUrl);
//            mImageLoader.load(Uri.parse(tempThubUrl)).into(viewHolder.mCover);
//            mImageLoader.
//            Glide.with(TimelineV1NewActivity.ac).load(Uri.parse(tempThubUrl)).into(viewHolder.mCover);
//            Glide
//                    .with(getActivity())
//                    .load(tempThubUrl)
//                    .centerCrop()
//                    .placeholder(R.drawable.non_p)
//                    .crossFade()
//                    .into(viewHolder.mCover);
        } else {
            viewHolder.layRow.setVisibility(View.GONE);
            viewHolder.layTopBlank.setVisibility(View.GONE);

            if (model.isAdVisible()) {

                viewHolder.layAd.setVisibility(View.VISIBLE);

                nativeAd = new NativeAd(getActivity(), getActivity().getResources().getString(R.string.facenook_ad_id));

                nativeAd.setAdListener(new AdListener() {

                    @Override
                    public void onError(Ad ad, AdError error) {
                        Log.e("error", error.getErrorMessage() + " / " + error.getErrorCode());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        if (ad != nativeAd) {
                            viewHolder.layAd.setVisibility(View.GONE);
                            return;
                        }
//                        if (BuildConfig.DEBUG) {
                        Log.e("face book ad", "fbad / " + nativeAd.getAdBody());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesLinkUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdSocialContext());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdSubtitle());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdTitle());
                        Log.e("face book ad", "fbad / " + nativeAd.getId());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesIcon().getUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCoverImage().getUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdIcon().getUrl());
//                        }

                        viewHolder.tvAdTitle.setText(nativeAd.getAdTitle());

                        imageFullLoader.DisplayImage(nativeAd.getAdCoverImage().getUrl(), viewHolder.bimvAd, false);
//                        Glide.with(ac).load(nativeAd.getAdCoverImage().getUrl()).into(viewHolder.bimvAd);

                        imageLoader.DisplayImage(nativeAd.getAdChoicesIcon().getUrl(), viewHolder.imvAdIcon, false);
//                        Glide.with(ac).load(nativeAd.getAdChoicesIcon().getUrl()).into(viewHolder.imvAdIcon);

                        viewHolder.tvAdSubTitle.setText(nativeAd.getAdSubtitle());

                        viewHolder.btnAdAction.setText(nativeAd.getAdCallToAction());

                        nativeAd.registerViewForInteraction(viewHolder.layAd);

                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }
                });

                viewHolder.tvAdTitle.setText(model.getAdTitle());

                try {
                    imageFullLoader.DisplayImage(nativeAd.getAdCoverImage().getUrl(), viewHolder.bimvAd, false);
//                Glide.with(ac).load(nativeAd.getAdCoverImage().getUrl()).into(viewHolder.bimvAd);

                    imageLoader.DisplayImage(nativeAd.getAdChoicesIcon().getUrl(), viewHolder.imvAdIcon, false);
//                Glide.with(ac).load(nativeAd.getAdChoicesIcon().getUrl()).into(viewHolder.imvAdIcon);

                } catch (NullPointerException e) {

                }

                viewHolder.tvAdSubTitle.setText(model.getAdSubTitle());

                viewHolder.btnAdAction.setText(model.getAdCallToAction());

                nativeAd.registerViewForInteraction(viewHolder.layAd);

                nativeAd.loadAd();


            } else {
                viewHolder.layAd.setVisibility(View.GONE);
            }
        }

    }



    @Override
    public void playNewVideo(final MetaData currentItemMetaData, final VideoPlayerView player, final VideoPlayerManager<MetaData> videoPlayerManager) {
//        Handler handler = new Handler(){
//            @Override
//            public void handleMessage(Message msg) {
//
//                player.setVisibility(View.VISIBLE);
//            }
//        };
//
//        handler.sendEmptyMessageDelayed(0,500);
//        videoPlayerManager.playNewVideo(currentItemMetaData, player, url);


        videoPlayerManager.stopAnyPlayback();
        String tempUrl = AppController.URL + model.getUrl().replace(".gif", ".mp4");
        Log.e("playnewvideourl", tempUrl);
//        String url = videoLoader.checkUrl(tempUrl);
//        Log.e("timelineRowURL", url);
        videoPlayerManager.playNewVideo(currentItemMetaData, player, tempUrl);
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback();
    }

    @Override
    public String toString() {
        return getClass() + ", postId[" + model.getPsot_id() + "]";
    }
}
