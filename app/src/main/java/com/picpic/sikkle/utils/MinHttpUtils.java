package com.picpic.sikkle.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinHttpUtils extends AsyncTask<Void, String, String> {

    String szResult = "";
    HashMap<String, String> _params;
    String _url;
    MinHttpEnd _mhe;
//    AlertDialog ad;

    public MinHttpUtils(String url, HashMap<String, String> params,
                        MinHttpEnd mhe) {
        _url = url;
        _params = params;
        _mhe = mhe;
    }

    @Override
    protected String doInBackground(Void... params) {

        try {

//            ad = createDialog();
//            ad.show();

            String postURL = _url;
            HttpPost hp = new HttpPost(postURL);

            List<NameValuePair> nvp = new ArrayList<NameValuePair>(
                    _params.size());

            if (_params != null) {
                for (Map.Entry<String, String> entry : _params.entrySet()) {
                    nvp.add(new BasicNameValuePair(entry.getKey(), entry
                            .getValue()));
                    // Log.e("url", postURL);
                    // Log.e("ee", entry.getKey() + "/" + entry.getValue());
                }
            } else {

            }

            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(nvp, "UTF-8");
            hp.setEntity(ent);

            HttpResponse res = AppController.getHc().execute(hp);

            HttpEntity resEnt = res.getEntity();

            if (resEnt != null) {
                szResult = EntityUtils.toString(resEnt);
            }

            Log.w("" + _url, szResult + "");
        } catch (Exception e) {
            Log.e("error", e + "");
        }

        return szResult;

    }

    @Override
    protected void onPostExecute(String result) {

        super.onPostExecute(result);

        try {
            JSONObject jd = new JSONObject(result);
            String tempResult = jd.getString("result");

            _mhe.set_result(result);

        } catch (JSONException e) {
            _mhe.set_result(result);
        }

//        setDismiss(ad);

    }

}
