package com.picpic.sikkle.utils.lock;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.picpic.sikkle.ui.NewLockActivity;

public class NewLockReceiver extends BroadcastReceiver {

	private KeyguardManager km = null;
	private KeyguardManager.KeyguardLock keyLock = null;
	private TelephonyManager telephonyManager = null;
	public static boolean isPhoneIdle = true;
	Context _con;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("test Lof", "NewLockReceiver_onReceive");
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)
				&& (!NewLockService.tf)) {

			if (km == null) {
				km = (KeyguardManager) context
						.getSystemService(Context.KEYGUARD_SERVICE);
			}

			if (keyLock == null)
				keyLock = km.newKeyguardLock(Context.KEYGUARD_SERVICE);

			if (telephonyManager == null) {
				telephonyManager = (TelephonyManager) context
						.getSystemService(Context.TELEPHONY_SERVICE);
				telephonyManager.listen(phoneListener,
						PhoneStateListener.LISTEN_CALL_STATE);
			}

			SharedPreferences localSharedPreferences = PreferenceManager
					.getDefaultSharedPreferences(NewLockService._con);

			Log.e("on/offasknflaskf",
					localSharedPreferences.getBoolean("LockOnOff", false) + "");
//			Log.e("on/offasknflaskf2", MinUtil.isBlock + "");

//			if (isPhoneIdle
//					&& !localSharedPreferences.getBoolean("LockOnOff", false)) {
//				_con = context;
//				handelr.sendEmptyMessageDelayed(0, 100);
//			}

			_con = context;
			handelr.sendEmptyMessageDelayed(0, 100);
		}
	}
	
	Handler handelr = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// disableKeyguard();
			reenableKeyguard();

			Intent i = new Intent(_con, NewLockActivity.class);
//			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			// i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			// i.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
			_con.startActivity(i);
		}
		
	};

	public void reenableKeyguard() {
		Log.e("test Lof", "NewLockReceiver_reenableKeyguard");
		keyLock.reenableKeyguard();
	}

	public void disableKeyguard() {
		Log.e("test Lof", "NewLockReceiver_disableKeyguard");
		keyLock.disableKeyguard();
	}

	private PhoneStateListener phoneListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			Log.e("test Lof", "NewLockReceiver_onCallStateChanged");
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				isPhoneIdle = true;
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				isPhoneIdle = false;
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				isPhoneIdle = false;
				break;
			}
		}
	};

}
