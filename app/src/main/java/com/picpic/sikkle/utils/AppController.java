package com.picpic.sikkle.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bumptech.glide.request.target.ViewTarget;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.fitness.data.Application;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.utils.filter.IFAmaroFilter;
import com.picpic.sikkle.utils.filter.IFBrannanFilter;
import com.picpic.sikkle.utils.filter.IFEarlybirdFilter;
import com.picpic.sikkle.utils.filter.IFHefeFilter;
import com.picpic.sikkle.utils.filter.IFHudsonFilter;
import com.picpic.sikkle.utils.filter.IFInkwellFilter;
import com.picpic.sikkle.utils.filter.IFLomoFilter;
import com.picpic.sikkle.utils.filter.IFNashvilleFilter;
import com.picpic.sikkle.utils.filter.IFSierraFilter;
import com.picpic.sikkle.utils.filter.IFToasterFilter;
import com.picpic.sikkle.utils.filter.IFValenciaFilter;
import com.picpic.sikkle.utils.filter.IFWaldenFilter;
import com.picpic.sikkle.utils.filter.IFXprollFilter;
import com.picpic.sikkle.widget.DrawView;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageRenderer;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.PixelBuffer;
import jp.co.cyberagent.android.gpuimage.Rotation;

/**
 * Created by Jong-min on 2015-07-21.
 */
public class AppController extends android.support.multidex.MultiDexApplication {
    //public class AppController extends android.app.Application {
    //    private Tracker mTracker;

    public static Animation fadeIn;

    public static int NETWORK_STATUS = 1;
    public static final String TAG = AppController.class.getSimpleName();
    public static Context context;
    public static Animation shake;
    public static boolean isNoti = false;
    public static HttpClient hc;
    public static ExecutorService playCountExe = Executors.newFixedThreadPool(1);
    public static ExecutorService likeCountExe = Executors.newFixedThreadPool(1);
    public static ExecutorService timelineExe = Executors.newFixedThreadPool(1);
//    public static ExecutorService gifExe = Executors.newFixedThreadPool(1);
//    public static ExecutorService videoExe = Executors.newFixedThreadPool(1);
//    public static ExecutorService imageExe = Executors.newFixedThreadPool(1);
//    public static ExecutorService thumbExe = Executors.newFixedThreadPool(1);

    public static String cacheDir;

//    public static ExecutorService apiExe = Executors.newFixedThreadPool(1);
//    public static ExecutorService apiSubExe = Executors.newFixedThreadPool(1);

    //    public static ExecutorService imageExe = Executors.newFixedThreadPool(5);

    public static ArrayList<ArrayList<Frame>> originFrames = new ArrayList<ArrayList<Frame>>();
    public static ArrayList<ArrayList<Frame>> editFrames = new ArrayList<ArrayList<Frame>>();
    public static ArrayList<int[]> sectionArr = new ArrayList<>();

    public static ArrayList<Path> dvPaths = new ArrayList<>();
    public static Bitmap dvBitmap = null;
    public static Bitmap dvOriginBitmap = null;
    public static Bitmap stvBitmap = null;

    public static int nowCameraNum = 0;

    public static int frameNavi = 4, speedNavi = 13, waterNavi = 1, tagNavi = 0, filterIndex = 0, playerWidth = 0, playerHeight = 0;
    public static GPUImageFilter mFilter = null;
    public static GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    public static String cameraPath2 = "", tag_id, tag_title, tag_body;
    public static String stvString = "PicPic";

    public static File cameraF, cameraAtomFF, cameraThumbFile;
    public static Uri cameraOutput1, cameraOutput2;
    public static boolean isText = false, isReverse = false, is11 = false, isPart = false, isLoadGIF = false;
    public static Matrix mMatrix;
    public static Paint mPaint;
    public static float[] mPoint;
    public static int stickerColorNavi = 0;

    public static Socket apiSocket = null;
    public static APITaskNew apiTaskNew;
    public static APITaskNew apiTaskNew2;
    public static APITaskNew apiTaskNew3;
    public static APIDataTaskNew apiDataTaskNew;
    public static String SENDER_ID;
    public static String URL = "";
    public static String NONURL = "";
    public static String NONURLINTEREST = "";
    public static String LOGOURL = "";
    public static String BASE_SRC = "";
    public static String TEMP_SRC = "";
    public static String TEMP_SRC2 = "";
    public static String SHARE_LINK = "";
    public static String LOGO_LINK = "";
    public static String upload_server_url;
    public static String PART_OUTPUT_SRC;
    private static AppController mInstance;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor se;
    private MinImageLoader minImageLoader;
    private MinFixImageLoader minFixImageLoader;
    private MinUserImageLoader minUserImageLoader;
    public static FileCache2 fileCache;
    public static MemoryCache memoryCache;

//    public static MinThumbImageRun thumbRun;
//    public static MinImageRun gifRun;
//    public static MinVideoRun videoRun;

    public static TimelineResult tempTR;

    public static String ip;
    public static String ip_2;
    public static String ip_playcount;
    public static int port;
    public static int port_2;


    public static VideoLoader videoLoader;
    //    public static VideoGIFLoader videoGIFLoader;
    public static GIFLoader gifLoader;
    public static ImageLoader imageLoader;

    public static Tracker t;

    public static String FACE_SLICE_SRC;

    private static String PROPERTY_ID = "";
    public static String SECRETKEY = "";
    public static String SECRETKEYIV = "";

    public enum TrackerName {
        APP_TRACKER,           // 앱 별로 트래킹
        GLOBAL_TRACKER,        // 모든 앱을 통틀어 트래킹
        ECOMMERCE_TRACKER,     // 아마 유료 결재 트래킹 개념 같음
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID) :
                    (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker) :
                            analytics.newTracker(R.xml.ecommerse_tracker);
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

    public static Bitmap overlayMark(Bitmap baseBmp, Bitmap overlayBmp) {
        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth(), baseBmp.getHeight(),
                baseBmp.getConfig());
        Canvas canvas = new Canvas(resultBmp);
        canvas.drawBitmap(baseBmp, 0, 0, null);
        canvas.drawBitmap(overlayBmp, baseBmp.getWidth() - overlayBmp.getWidth() - 14, baseBmp.getHeight() - overlayBmp.getHeight() - 14, null);
        return resultBmp;
    }

    public static Bitmap overlayPart(Bitmap baseBmp, Bitmap overlayBmp) {
        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth(), baseBmp.getHeight(),
                baseBmp.getConfig());
        Canvas canvas = new Canvas(resultBmp);
        canvas.drawBitmap(baseBmp, 0, 0, null);
        canvas.drawBitmap(overlayBmp, 0, 0, null);
        return resultBmp;
    }

    public static void initCamera(boolean is) {
        if (is) {
            originFrames = new ArrayList<ArrayList<Frame>>();
            cameraOutput1 = null;
            cameraOutput2 = null;
            cameraPath2 = "";
        }
        editFrames = new ArrayList<ArrayList<Frame>>();
        sectionArr = new ArrayList<>();

        dvPaths = new ArrayList<>();
        isText = false;
        dvBitmap = null;
        stvBitmap = null;
        frameNavi = 4;
        speedNavi = 13;
        waterNavi = 1;
        mFilter = null;
        filterIndex = 0;

        isReverse = false;
        is11 = false;
        isPart = false;
        isLoadGIF = false;
        stickerColorNavi = 0;
        tag_id = "";
        tag_title = "";
        tag_body = "";
        stvString = "PicPic";

        GPUImageFilter filter = new GPUImageFilter();

        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);
            mFilterAdjuster.adjust(50);
        }

        cameraF = null;
        cameraAtomFF = null;
        cameraThumbFile = null;
    }

    public static void switchFilterTo(final GPUImageFilter filter, GPUImageView imv) {
        if (filter != null && !AppController.mFilter.getClass().equals(filter.getClass())) {
            imv.setFilter(filter);
            AppController.mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(filter);
            AppController.mFilterAdjuster.adjust(50);
        }
    }

    public static GPUImageFilter createFilterForType(final Context context, final FilterType type) {
        switch (type) {
            case CONTRAST:
                return new GPUImageContrastFilter(2.0f);
            case BRIGHTNESS:
                return new GPUImageBrightnessFilter(0.1f);
            case VIGNETTE:
                PointF centerPoint = new PointF();
                centerPoint.x = 0.5f;
                centerPoint.y = 0.5f;
                return new GPUImageVignetteFilter(centerPoint, new float[]{0.0f, 0.0f, 0.0f}, 0.3f, 0.75f);
            case TONE_CURVE:
                GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
                toneCurveFilter.setFromCurveFileInputStream(
                        context.getResources().openRawResource(R.raw.tone_cuver_sample));
                return toneCurveFilter;
            case LOOKUP_AMATORKA:
                GPUImageLookupFilter amatorka = new GPUImageLookupFilter();
                amatorka.setBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.lookup_amatorka));
                return amatorka;
            case I_AMARO:
                return new IFAmaroFilter(context);
            case I_BRANNAN:
                return new IFBrannanFilter(context);
            case I_EARLYBIRD:
                return new IFEarlybirdFilter(context);
            case I_HEFE:
                return new IFHefeFilter(context);
            case I_HUDSON:
                return new IFHudsonFilter(context);
            case I_INKWELL:
                return new IFInkwellFilter(context);
            case I_LOMO:
                return new IFLomoFilter(context);
            case I_NASHVILLE:
                return new IFNashvilleFilter(context);
            case I_SIERRA:
                return new IFSierraFilter(context);
            case I_TOASTER:
                return new IFToasterFilter(context);
            case I_VALENCIA:
                return new IFValenciaFilter(context);
            case I_WALDEN:
                return new IFWaldenFilter(context);
            case I_XPROII:
                return new IFXprollFilter(context);

            default:
                throw new IllegalStateException("No filter of that type!");
        }

    }

    enum FilterType {
        I_AMARO, I_HEFE, I_NASHVILLE,
        I_SIERRA, I_INKWELL, I_VALENCIA,
        I_WALDEN, I_XPROII, BRIGHTNESS,
        I_BRANNAN, I_EARLYBIRD, I_HUDSON,
        I_LOMO, I_TOASTER,
        CONTRAST, VIGNETTE, TONE_CURVE,
        LOOKUP_AMATORKA
    }

    public static FilterType[] ff = {
            FilterType.I_AMARO, FilterType.I_AMARO, FilterType.I_HEFE, FilterType.I_NASHVILLE,
            FilterType.I_SIERRA, FilterType.I_INKWELL, FilterType.I_VALENCIA,
            FilterType.I_WALDEN, FilterType.I_XPROII, FilterType.BRIGHTNESS,
            FilterType.I_BRANNAN, FilterType.I_EARLYBIRD, FilterType.I_HUDSON,
            FilterType.I_LOMO, FilterType.I_TOASTER,
            FilterType.CONTRAST, FilterType.VIGNETTE, FilterType.TONE_CURVE,
            FilterType.LOOKUP_AMATORKA
    };

    public static Bitmap getBitmapWithFilterApplied(final Bitmap bitmap) {

        GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
        renderer.setRotation(Rotation.NORMAL,
                renderer.isFlippedHorizontally(), renderer.isFlippedVertically());
        renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
        PixelBuffer buffer = new PixelBuffer(bitmap.getWidth(), bitmap.getHeight());
        buffer.setRenderer(renderer);
//        renderer.setImageBitmap(bitmap, false);
        renderer.setImageBitmap(bitmap, false);
        Bitmap result = buffer.getBitmap();
        mFilter.destroy();
        renderer.deleteImage();
        buffer.destroy();

        DrawView dv = new DrawView(getmInstance());
        dv.setmBitmap(result);
        dv.reset(result);
        dv.invalidate();
        dv.setPaths(dvPaths);

        result = dv.getmBitmap();

        return result;
    }

    public static void initFilter(Context con, GPUImageView imv) {

        if (filterIndex != 0) {
            switchFilterTo(createFilterForType(con, ff[filterIndex]), imv);
            imv.requestRender();
            AppController.mFilter = createFilterForType(con, ff[filterIndex]);

        } else {
            switchFilterTo(new GPUImageFilter(), imv);
            imv.requestRender();
            AppController.mFilter = new GPUImageFilter();

        }
//        if(dvBitmap != null){
//            Log.e("sizessss", AppController.dvBitmap.getWidth() + "/" + AppController.dvBitmap.getHeight());
//
//            Bitmap scaleB2 = Bitmap.createScaledBitmap(AppController.dvBitmap, AppController.playerWidth, AppController.playerHeight, false);
//
//            AppController.dvBitmap = scaleB2;
//        }

//        return AppController.dvBitmap;
    }

    public static HttpClient getHc() {
        return hc;
    }

    public static AppController getmInstance() {
        return mInstance;
    }

    public static SharedPreferences getSp() {
        return sp;
    }

    public static SharedPreferences.Editor getSe() {
        return se;
    }

    public static StringTransMethod stm = new StringTransMethod() {

    };

    public static void ActionLogInsert(String target, String type, Context con) {
        Map<String, String> paramAction = new HashMap<>();

        paramAction.put("my_id", AppController.getSp().getString("email", ""));
        paramAction.put("target_id", target);
        paramAction.put("type", type);

        AppController.apiDataTaskNew = new APIDataTaskNew(con, paramAction, 603, stm);
        AppController.apiDataTaskNew.execute();
    }

    private static AppController sApp;

    public static SharedPreferences pref() {
        return sApp.getSharedPreferences("toro_pref", Context.MODE_PRIVATE);
    }

    public static String packageName() {
        return sApp.getPackageName();
    }

    @Override
    public void onCreate() {
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        se = sp.edit();

        MultiDex.install(this);
        super.onCreate();
        sApp = this;

        ViewTarget.setTagId(R.id.glide_tag);

        fadeIn = AnimationUtils.loadAnimation(this, R.anim.pade_in);

        PROPERTY_ID = getResources().getString(R.string.google_analytics_key);
        t = getTracker(TrackerName.APP_TRACKER);

        videoLoader = new VideoLoader(this);
//        videoGIFLoader = new VideoGIFLoader(this);
        imageLoader = new ImageLoader(this);
        gifLoader = new GIFLoader(this);

        cacheDir = getCacheDir().getAbsolutePath();
        memoryCache = new MemoryCache();
        fileCache = new FileCache2(getApplicationContext());
        ip = getResources().getString(R.string.ip);
        ip_2 = getResources().getString(R.string.ip_playcount);
        ip_playcount = getResources().getString(R.string.ip_playcount);
        SECRETKEY = getResources().getString(R.string.sk);
        SECRETKEYIV = getResources().getString(R.string.sk_iv);
        port = getResources().getInteger(R.integer.port);
        port_2 = getResources().getInteger(R.integer.port2);
        mInstance = this;
        context = getApplicationContext();
        NETWORK_STATUS = NetworkUtil.getConnectivityStatus(context);
        URL = getResources().getString(R.string.s_url);
        NONURL = getResources().getString(R.string.non_url);
        NONURLINTEREST = getResources().getString(R.string.non_url_interest);
        SHARE_LINK = getResources().getString(R.string.share_link);
        BASE_SRC = Environment.getExternalStorageDirectory() + "/PicPic/";
        TEMP_SRC = getExternalCacheDir() + "/";
        TEMP_SRC2 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/test/";
        PART_OUTPUT_SRC = AppController.BASE_SRC + "part/part.png";
        LOGO_LINK = getResources().getString(R.string.logo_link);
        upload_server_url = getResources().getString(R.string.upload_server_url);
        SENDER_ID = getResources().getString(R.string.sender_id);
        minImageLoader = new MinImageLoader(getApplicationContext());
        minFixImageLoader = new MinFixImageLoader(getApplicationContext());
        minUserImageLoader = new MinUserImageLoader(getApplicationContext());
        fileCache = new FileCache2(context);
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        if (hc == null) {
            hc = getThreadSafeClient();
        }
    }

    public DefaultHttpClient getThreadSafeClient() {
        DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams params = client.getParams();
        client = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
                mgr.getSchemeRegistry()), params);

        return client;
    }

    public MinImageLoader getMinImageLoader() {
        return minImageLoader;
    }

    public MinFixImageLoader getMinFixImageLoader() {
        return minFixImageLoader;
    }

    public MinUserImageLoader getMinUserImageLoader() {
        return minUserImageLoader;
    }


    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Log.e("", "CopyStream catch Exception...");
        }
    }

    public static String readPreferences(Context con, String str, String result) {
        return sp.getBoolean(str, Boolean.valueOf(result)) + "";
    }

    public static void savePreferences(Context con, String str, String result){
        se.putBoolean(str, Boolean.valueOf(result));
        se.commit();
    }
}
