package com.picpic.sikkle.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jong-min on 2015-11-06.
 */
public class MinAPIRun implements Runnable {

    Map<String, String> params;
    int serviceCode = 0;
    JSONObject inputJO;
    StringTransMethod stm;

    String return_msg = "";
    private String sendMSG = "abcdeabcdeabcde";

    public MinAPIRun(Map<String, String> param, int code, StringTransMethod stmm) {
        this.params = param;
        this.serviceCode = code;
        this.stm = stmm;

        inputJO = new JSONObject();
        Iterator<String> iter = params.keySet().iterator();
        try {
            String key = null, value = null;
            while (iter.hasNext()) {
                key = iter.next();
                value = params.get(key);

                inputJO.put(key, value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("input", inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));

        sendMSG = inputJO.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]");

//        if (AppController.apiExe.isShutdown() || AppController.apiExe.isTerminated()) {
//            AppController.apiExe = Executors.newFixedThreadPool(1);
//        }
//        if (AppController.apiSubExe.isShutdown() || AppController.apiSubExe.isTerminated()) {
//            AppController.apiSubExe = Executors.newFixedThreadPool(5);
//        }
    }

    @Override
    public void run() {

        OutputStream outputStream = null;
        BufferedReader in = null;

        if (AppController.apiSocket != null) {
            try {
                AppController.apiSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            AppController.apiSocket = new Socket(AppController.ip, AppController.port);
            outputStream = AppController.apiSocket.getOutputStream();

            byte[] data = sendMSG.getBytes("UTF-8");
            int data_size = data.length;

            byte[] sendData = new byte[data_size + 9];

            sendData[0] = 0x02;

            byte[] tempA = intToByteArray(serviceCode);

            System.arraycopy(tempA, 0, sendData, 1, tempA.length);

            byte[] tempB = intToByteArray(data_size);

            System.arraycopy(tempB, 0, sendData, 5, tempB.length);

            System.arraycopy(data, 0, sendData, 9, data_size);

            outputStream.write(sendData);

            in = new BufferedReader(new InputStreamReader(AppController.apiSocket.getInputStream()));

            return_msg = in.readLine();

            stm.set_result(return_msg);
        } catch (SocketTimeoutException e) {
            Log.e("TCP", "C: Error1-1 ", e);
        } catch (Exception e) {
            Log.e("TCP", "C: Error1-2 ", e);
        } finally {
            try {
                AppController.apiSocket.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            }

            try {
                outputStream.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error3 ", e);
            }

            try {
                in.close();
            } catch (Exception e) {
                Log.e("TCP", "C: Error4 ", e);
            }
        }

    }

    private byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }
}
