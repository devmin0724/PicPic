package com.picpic.sikkle.utils.lock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NewBootLockReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			Intent i = new Intent(context, NewLockService.class);
			context.startService(i);
		}
	}
}