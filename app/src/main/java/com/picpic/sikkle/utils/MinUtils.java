package com.picpic.sikkle.utils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v4.util.TimeUtils;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.widget.HashTagV1;
import com.picpic.sikkle.widget.VView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by L on 2015-03-09.
 */
public class MinUtils {

    public static String PTK;
    public static String DID;
    public static int screenWidth;
    public static int screenHeight;
    public static float density;
    public static ArrayList<String> friendsArr;

    public static boolean liking = false;

    private static final String baseDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static int dpToPx2(Context context, float dp) {
        // Took from http://stackoverflow.com/questions/8309354/formula-px-to-dp-dp-to-px-android
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) ((dp * scale) + 0.5f);
    }

    public static void setCount(TextView tv, int count) {
        DecimalFormat countDF = new DecimalFormat("0.#");

        String countStr;

        if (count >= 1000) {
            countStr = countDF.format(count / 1000) + "K";
        } else {
            countStr = count + "";
        }

        tv.setText(countStr);
    }
    public static void setCountLong(TextView tv, long count) {
        if(count > 1000){
            DecimalFormat countDF = new DecimalFormat("0.#");

            String countStr;

            if (count >= 1000) {
                countStr = countDF.format(count / 1000) + "K";
            } else {
                countStr = count + "";
            }

            tv.setText(countStr);
        }else{
            tv.setText(count + "");
        }

    }

    public static void setTags(Context con, TextView tv, String tags) {
        if (tags.length() <= 0) {
            tv.setVisibility(View.GONE);
        } else {
            tv.setVisibility(View.VISIBLE);
            Log.e("tempTags1", tags);
            Log.e("first", tags.substring(0));
            if (tags.substring(0).toString().equals(",")) {
                tags = tags.substring(1, tags.length());
                Log.e("tempTags2", tags);
            }

            tags = "#" + tags;
            tags = tags.replace(",", "  #");

            Log.e("tempTags3", tags);
            tv.setText(tags);
        }

        String tagsText = tv.getText().toString();

        ArrayList<int[]> hashtagSpans2 = getSpans(tagsText, '#');

        SpannableString commentsContent2 = new SpannableString(tagsText);

        for (int i = 0; i < hashtagSpans2.size(); i++) {
            int[] span = hashtagSpans2.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            commentsContent2.setSpan(new HashTagV1(con),
                    hashTagStart, hashTagEnd, 0);
        }

        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setText(commentsContent2);
        tv.setHighlightColor(0x00ffffff);
    }

    public static void like(final boolean like, final Activity ac, String postID) {
        Log.e("liking", liking + "");
        Log.e("params", postID + "/" + AppController.getSp().getString("email", "") + "/" + "P");
        if (!liking) {
            liking = true;
            if (like) {
                Log.e("like", "likelikelikelikelikelike");
                StringTransMethod stmLikeResult = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        Log.e("result like", result);
                        liking = false;
                        super.endTrans(result);
                    }
                };
                Map<String, String> params = new HashMap<>();

                params.put("post_reply_id", postID);
                params.put("click_id", AppController.getSp().getString("email", ""));
                params.put("like_form", "P");

                AppController.apiDataTaskNew = new APIDataTaskNew(ac, params, 303, stmLikeResult);
                AppController.apiDataTaskNew.execute();
            } else {
                Log.e("unlike", "unlikeunlikeunlikeunlikeunlikeunlikeunlikeunlike");
                StringTransMethod stmUnLikeResult = new StringTransMethod() {
                    @Override
                    public void endTrans(String result) {
                        Log.e("result unlike", result);
                        liking = false;
                        super.endTrans(result);
                    }
                };
                Map<String, String> params = new HashMap<>();

                params.put("post_reply_id", postID);
                params.put("click_id", AppController.getSp().getString("email", ""));
                params.put("like_form", "P");

                AppController.apiDataTaskNew = new APIDataTaskNew(ac, params, 302, stmUnLikeResult);
                AppController.apiDataTaskNew.execute();
            }
        }

    }

    public static class ViewHolder {
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
            if (viewHolder == null) {
                viewHolder = new SparseArray<View>();
                view.setTag(viewHolder);
            }
            View childView = viewHolder.get(id);
            if (childView == null) {
                childView = view.findViewById(id);
                viewHolder.put(id, childView);
            }

            return (T) childView;
        }
    }


    public static int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                AppController.context.getResources().getDisplayMetrics());
    }

    public static String toPostId(String base62Number) {
        return "POST" + fromBase62(base62Number);
    }

    public static String toCurrent(String base62Number) {
        return fromBase62(base62Number) + "";
    }

    public static String fromPostId(String base62Number) {
        return toBase62(Integer.parseInt(base62Number.replaceAll("POST", "")));
    }

    public static String fromMId(String base62Number) {
        return toBase62(Integer.parseInt(base62Number.replaceAll("PIC", "")));
    }

    public static String fromCurrnet(String base62Number) {
        return toBase62Long(Long.parseLong(base62Number.replaceAll("PIC", "")));
    }

    public static String toBase62(int decimalNumber) {
        return fromDecimalToOtherBase(62, decimalNumber);
    }

    public static String toBase62Long(long decimalNumber) {
        return fromDecimalToOtherBaseLong(62, decimalNumber);
    }

    public static int fromBase62(String base62Number) {
        return fromOtherBaseToDecimal(62, base62Number);
    }


    private static String fromDecimalToOtherBase(int base, int decimalNumber) {
        String tempVal = decimalNumber == 0 ? "0" : "";
        int mod = 0;
        while (decimalNumber != 0) {
            mod = decimalNumber % base;
            tempVal = baseDigits.substring(mod, mod + 1) + tempVal;
            decimalNumber = decimalNumber / base;
        }

        return tempVal;
    }

    private static String fromDecimalToOtherBaseLong(int base, long decimalNumber) {
        String tempVal = decimalNumber == 0 ? "0" : "";
        long mod = 0;
        while (decimalNumber != 0) {
            mod = decimalNumber % base;
            tempVal = baseDigits.substring((int) mod, (int) mod + 1) + tempVal;
            decimalNumber = decimalNumber / base;
        }

        return tempVal;
    }

    private static int fromOtherBaseToDecimal(int base, String number) {
        int iterator = number.length();
        int returnValue = 0;
        int multiplier = 1;

        while (iterator > 0) {
            returnValue = returnValue + (baseDigits.indexOf(number.substring(iterator - 1, iterator)) * multiplier);
            multiplier = multiplier * base;
            --iterator;
        }
        return returnValue;
    }

    public static ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    private static byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value};
    }

    public static void d(String TAG, String message) {
        int maxLogSize = 2000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            Log.e(TAG, message.substring(start, end));
        }
    }

//    public static String AES_Encode(byte[] input) throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
//        byte[] keyData = AppController.SECRETKEY.getBytes();
//
//        SecretKey secureKey = new SecretKeySpec(keyData, "AES");
//
//        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(AppController.SECRETKEYIV.getBytes()));
//
//        byte[] encrypted = c.doFinal(input);
//        return new String(Base64.encode(encrypted, 0));
//
////        Base64.
//
////        org.apache.commons.codec.binary.Ba
//
////        Base64.en
////        String enStr = new String();
//    }

    public static byte[] AES_Encode(byte[] in) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        byte[] keyData = AppController.SECRETKEY.getBytes();

        SecretKey secureKey = new SecretKeySpec(keyData, "AES");

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(AppController.SECRETKEYIV.getBytes()));

        return c.doFinal(in);
    }

    public static class PlayCountSubmit extends AsyncTask<String, Void, String> {

        String tempPostId;
        String return_msg = "";
        private String sendMSG = "abcdeabcdeabcde";
        Socket socket;

        public PlayCountSubmit(String postId) {
            tempPostId = postId;
            JSONObject jd = new JSONObject();
            try {
                jd.put("post_id", postId);

//                Date now = new Date();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//                String tt = sdf.format(now);
//
//                jd.put("ct", tt);
                sendMSG = jd.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Log.e("input", sendMSG.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));
        }

        @Override
        protected String doInBackground(String... params) {

            OutputStream outputStream = null;
            BufferedReader in = null;
            try {
                socket = new Socket(AppController.ip_playcount, AppController.port);

                try {
                    outputStream = socket.getOutputStream();

                    byte[] data = sendMSG.getBytes("UTF-8");
//                    byte[] data = MinUtils.AES_Encode(sendMSG.getBytes("UTF-8"));
                    int data_size = data.length;

                    byte[] sendData = new byte[data_size + 9];

                    sendData[0] = 0x02;

                    byte[] tempA = intToByteArray(301);

                    System.arraycopy(tempA, 0, sendData, 1, tempA.length);

                    byte[] tempB = intToByteArray(data_size);

                    System.arraycopy(tempB, 0, sendData, 5, tempB.length);

                    System.arraycopy(data, 0, sendData, 9, data_size);

                    outputStream.write(sendData);

                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    return_msg = in.readLine();

                    JSONObject jj = new JSONObject(return_msg);

                } catch (Exception e) {
                    Log.e("TCP", "C: Error1 ", e);
                }

            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            } finally {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {

                }
            }

            return return_msg;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public static class LikeSubmit extends AsyncTask<String, Void, String> {

        String tempPostId;
        String return_msg = "";
        private String sendMSG = "abcdeabcdeabcde";
        Socket socket;

        public LikeSubmit(String postId) {

            tempPostId = postId;
            JSONObject jd = new JSONObject();
            try {
                jd.put("post_reply_id", postId);
                jd.put("click_id", AppController.getSp().getString("email", ""));
                jd.put("like_form", "P");

                sendMSG = jd.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Log.e("input", sendMSG.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));
        }

        @Override
        protected String doInBackground(String... params) {

            OutputStream outputStream = null;
            BufferedReader in = null;
            try {
                socket = new Socket(AppController.ip_playcount, AppController.port);

                try {
                    outputStream = socket.getOutputStream();

                    byte[] data = sendMSG.getBytes("UTF-8");
//                    byte[] data = MinUtils.AES_Encode(sendMSG.getBytes("UTF-8"));
                    int data_size = data.length;

                    byte[] sendData = new byte[data_size + 9];

                    sendData[0] = 0x02;

                    byte[] tempA = intToByteArray(303);

                    System.arraycopy(tempA, 0, sendData, 1, tempA.length);

                    byte[] tempB = intToByteArray(data_size);

                    System.arraycopy(tempB, 0, sendData, 5, tempB.length);

                    System.arraycopy(data, 0, sendData, 9, data_size);

                    outputStream.write(sendData);

                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    return_msg = in.readLine();

                    JSONObject jj = new JSONObject(return_msg);

                    Log.e("like result", return_msg);

                } catch (Exception e) {
                    Log.e("TCP", "C: Error1 ", e);
                }

            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            } finally {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {

                }
            }

            return return_msg;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public static class UnlikeSubmit extends AsyncTask<String, Void, String> {

        String tempPostId;
        String return_msg = "";
        private String sendMSG = "abcdeabcdeabcde";
        Socket socket;

        public UnlikeSubmit(String postId) {
            tempPostId = postId;
            JSONObject jd = new JSONObject();
            try {
                jd.put("post_reply_id", postId);
                jd.put("click_id", AppController.getSp().getString("email", ""));
                jd.put("like_form", "P");

                sendMSG = jd.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Log.e("input", sendMSG.toString().replace("\\\"", "\"").replace("\"\\", "\"").replace("\"[", "[").replace("]\"", "]"));
        }

        @Override
        protected String doInBackground(String... params) {

            OutputStream outputStream = null;
            BufferedReader in = null;
            try {
                socket = new Socket(AppController.ip_playcount, AppController.port);

                try {
                    outputStream = socket.getOutputStream();

                    byte[] data = sendMSG.getBytes("UTF-8");
//                    byte[] data = MinUtils.AES_Encode(sendMSG.getBytes("UTF-8"));
                    int data_size = data.length;

                    byte[] sendData = new byte[data_size + 9];

                    sendData[0] = 0x02;

                    byte[] tempA = intToByteArray(302);

                    System.arraycopy(tempA, 0, sendData, 1, tempA.length);

                    byte[] tempB = intToByteArray(data_size);

                    System.arraycopy(tempB, 0, sendData, 5, tempB.length);

                    System.arraycopy(data, 0, sendData, 9, data_size);

                    outputStream.write(sendData);

                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    return_msg = in.readLine();

                    JSONObject jj = new JSONObject(return_msg);

                    Log.e("unlike result", return_msg);

                } catch (Exception e) {
                    Log.e("TCP", "C: Error1 ", e);
                }

            } catch (Exception e) {
                Log.e("TCP", "C: Error2 ", e);
            } finally {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {

                }
            }

            return return_msg;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    public static String timeStamp(int position, int duration) {
        StringBuilder posTime = new StringBuilder();
        TimeUtils.formatDuration(position, posTime);
        StringBuilder durationTime = new StringBuilder();
        TimeUtils.formatDuration(duration, durationTime);

        return posTime + " / " + durationTime.toString();
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    // Copy from AOSP
    /**
     * Generate a value suitable for use in .
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return a generated ID value
     */
    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

}
