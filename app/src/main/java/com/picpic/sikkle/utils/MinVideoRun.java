/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.VideoView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.widget.VView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-11-04.
 */
public class MinVideoRun implements Runnable {

    String url = "", contentType = "";
    VView vv;
    String path;
    long nowSize;
    Activity ac;
    int navi;
    File f;

    public MinVideoRun(Activity activity, String url, VView vv, String obj, long size, int nav) {
        this.url = url;
        this.vv = vv;
        this.path= obj;
        this.nowSize = size;
        this.ac = activity;
        this.navi = nav;

//        if (AppController.gifExe.isShutdown() || AppController.gifExe.isTerminated()) {
//            AppController.gifExe = Executors.newFixedThreadPool(1);
//        }
//        if (AppController.thumbExe.isShutdown() || AppController.thumbExe.isTerminated()) {
//            AppController.thumbExe = Executors.newFixedThreadPool(5);
//        }

//        this.vv.setVideoURI(null);
    }

    @Override
    public void run() {
        URL urls = null;
        URLConnection urlConnection = null;
        try {
            urls = new URL(url);
            urlConnection = urls.openConnection();
            urlConnection.connect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final long file_size1 = urlConnection.getContentLength();

        if (file_size1 == nowSize) {
                //TODO 다운로드
                Log.e("down1", "ok");
                HttpURLConnection conn = null;
                OutputStream os = null;
                byte[] buffer = new byte[50*1024];
                int bufferSize = buffer.length;
                try {
                    f = AppController.fileCache.getFile(url);
                    URL imageUrl = null;
                    imageUrl = new URL(url);
                    conn = (HttpURLConnection) imageUrl.openConnection();
                    conn.setConnectTimeout(30000);
                    conn.setReadTimeout(30000);
                    conn.setInstanceFollowRedirects(true);
                    contentType = conn.getContentType();


                    try{
                        InputStream is = conn.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

                        FileOutputStream fos = new FileOutputStream(f);
                        int red2 = 0;
// This size can be changed
                        byte[] buf2 = new byte[50*1024];
                        while ((red2 = bis.read(buf2)) != -1) {
                            fos.write(buf2, 0, red2);
                        }
                        long endTime = System.currentTimeMillis(); //maybe
// Flush after, as this may trigger a commit to disk.
                        fos.flush();
                        fos.close();

                    }catch (FileNotFoundException e){
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                imv.setImageResource(R.drawable.non_interest);
                            }
                        });
                        return;
                    }

                    Log.e("contype", contentType);

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            vv.setVideoPath(f.getAbsolutePath());
//                            vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                                @Override
//                                public void onPrepared(MediaPlayer mp) {
//                                    mp.start();
//                                }
//                            });
//                            try {
//                                vv.setDataSource(f.getAbsolutePath());
//                                vv.start();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
                        }
                    });

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
//                    try {
//                        os.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (NullPointerException e) {
//                        e.printStackTrace();
//                    }
                    conn.disconnect();
                }
        } else {
            //TODO 다운로드
            Log.e("down2", "ok");
            HttpURLConnection conn = null;
            OutputStream os = null;
            try {
                f = AppController.fileCache.getFile(url);
                URL imageUrl = null;
                imageUrl = new URL(url);
                conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                contentType = conn.getContentType();
                try{
                    InputStream is = conn.getInputStream();
                    os = new FileOutputStream(f);
                    AppController.CopyStream(is, os);
                    os.close();
                }catch (FileNotFoundException e){
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            imv.setImageResource(R.drawable.non_interest);
                        }
                    });
                    return;
                }

                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        vv.setVideoPath(f.getAbsolutePath());
//                        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                            @Override
//                            public void onPrepared(MediaPlayer mp) {
//                                mp.start();
//                            }
//                        });
                    }
                });

            } catch (MalformedURLException e) {
                Log.e("malform", "ㅠ");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                Log.e("FileNot", "ㅠ");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("IOE1", "ㅠ");
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e("IOE2", "ㅠ");
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Log.e("NullPoe", "ㅠ");
                    e.printStackTrace();
                }
                conn.disconnect();
            }
        }
    }
}
