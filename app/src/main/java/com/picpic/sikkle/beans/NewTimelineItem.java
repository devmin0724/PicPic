package com.picpic.sikkle.beans;

/**
 * Created by devmin on 2016-05-06.
 */
public class NewTimelineItem {

    private String pro_url = "";
    private String id = "";
    private String time = "";
    private int playCnt = 0;
    private String urlVideo = "";
    private String urlThumb = "";
    private String urlGif = "";
    private String body = "";
    private int likeCount = 0;
    private int comCount = 0;
    private boolean isLike = false;
    private String psot_id = "";
    private String ownerId = "";
    private String andTagId = "";
    private String andTagName = "";
    private String andTagBody = "";
    private boolean isAndTag = false;
    private boolean isFollow = false;
    private TimelineLastCommentItem tlci = new TimelineLastCommentItem();
    private boolean isLoading = false;
    private int width = 0;
    private int height = 0;
    private int width2 = 0;
    private int height2 = 0;
    private String tags = "";
    private boolean isAd = false;
    private boolean isAdVisible = true;
    private String adTitle = "";
    private String adCoverImg = "";
    private String adSubTitle = "";
    private String adCallToAction = "";
    private String adChoicesIcon = "";
    private String adChoiceLinkUrl = "";

    public String getPro_url() {
        return pro_url;
    }

    public void setPro_url(String pro_url) {
        this.pro_url = pro_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPlayCnt() {
        return playCnt;
    }

    public void setPlayCnt(int playCnt) {
        this.playCnt = playCnt;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }

    public String getUrlGif() {
        return urlGif;
    }

    public void setUrlGif(String urlGif) {
        this.urlGif = urlGif;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getComCount() {
        return comCount;
    }

    public void setComCount(int comCount) {
        this.comCount = comCount;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public String getPsot_id() {
        return psot_id;
    }

    public void setPsot_id(String psot_id) {
        this.psot_id = psot_id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getAndTagId() {
        return andTagId;
    }

    public void setAndTagId(String andTagId) {
        this.andTagId = andTagId;
    }

    public String getAndTagName() {
        return andTagName;
    }

    public void setAndTagName(String andTagName) {
        this.andTagName = andTagName;
    }

    public String getAndTagBody() {
        return andTagBody;
    }

    public void setAndTagBody(String andTagBody) {
        this.andTagBody = andTagBody;
    }

    public boolean isAndTag() {
        return isAndTag;
    }

    public void setAndTag(boolean andTag) {
        isAndTag = andTag;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public TimelineLastCommentItem getTlci() {
        return tlci;
    }

    public void setTlci(TimelineLastCommentItem tlci) {
        this.tlci = tlci;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth2() {
        return width2;
    }

    public void setWidth2(int width2) {
        this.width2 = width2;
    }

    public int getHeight2() {
        return height2;
    }

    public void setHeight2(int height2) {
        this.height2 = height2;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public boolean isAd() {
        return isAd;
    }

    public void setAd(boolean ad) {
        isAd = ad;
    }

    public boolean isAdVisible() {
        return isAdVisible;
    }

    public void setAdVisible(boolean adVisible) {
        isAdVisible = adVisible;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getAdCoverImg() {
        return adCoverImg;
    }

    public void setAdCoverImg(String adCoverImg) {
        this.adCoverImg = adCoverImg;
    }

    public String getAdSubTitle() {
        return adSubTitle;
    }

    public void setAdSubTitle(String adSubTitle) {
        this.adSubTitle = adSubTitle;
    }

    public String getAdCallToAction() {
        return adCallToAction;
    }

    public void setAdCallToAction(String adCallToAction) {
        this.adCallToAction = adCallToAction;
    }

    public String getAdChoicesIcon() {
        return adChoicesIcon;
    }

    public void setAdChoicesIcon(String adChoicesIcon) {
        this.adChoicesIcon = adChoicesIcon;
    }

    public String getAdChoiceLinkUrl() {
        return adChoiceLinkUrl;
    }

    public void setAdChoiceLinkUrl(String adChoiceLinkUrl) {
        this.adChoiceLinkUrl = adChoiceLinkUrl;
    }
}
