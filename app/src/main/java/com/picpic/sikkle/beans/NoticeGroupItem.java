package com.picpic.sikkle.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by L on 2015-03-18.
 */
public class NoticeGroupItem {
    public List<NoticeChildItem> items = new ArrayList<NoticeChildItem>();
    private String title;
    private String date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<NoticeChildItem> getItems() {
        return items;
    }

    public void setItems(List<NoticeChildItem> items) {
        this.items = items;
    }
}
