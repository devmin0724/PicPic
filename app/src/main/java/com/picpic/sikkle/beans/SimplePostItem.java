package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-14.
 */
public class SimplePostItem {
    private String postId = "";
    private String url = "";

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
