package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-19.
 */
public class ProgressItem {
    public int color;
    public float progressItemPercentage;
}
