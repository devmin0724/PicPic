package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-17.
 */
public class AndTagTaggerItem {
    private String email = "";
    private String id = "";
    private String url = "";

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
