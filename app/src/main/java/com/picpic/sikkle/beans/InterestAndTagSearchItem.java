package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-17.
 */
public class InterestAndTagSearchItem {

    private String url = "";
    private String title = "";
    private String body = "";
    private String tag_id = "";

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
