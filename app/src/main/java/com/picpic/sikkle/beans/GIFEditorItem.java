package com.picpic.sikkle.beans;

import android.graphics.Bitmap;

/**
 * Created by 종민 on 2015-07-27.
 */
public class GIFEditorItem {

    private Bitmap thumbnail;
    private String time;
    private String url;

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
