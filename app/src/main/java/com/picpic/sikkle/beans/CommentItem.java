package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-04.
 */
public class CommentItem {

    private String url = "";
    private String id = "";
    private String time = "";
    private String body = "";
    private String email = "";
    private int likeCnt = 0;
    private String com_id = "";
    private boolean isLike = false;
    private boolean isNon = false;
    private int type = 0;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isNon() {
        return isNon;
    }

    public void setNon(boolean non) {
        isNon = non;
    }

    public String getCom_id() {
        return com_id;
    }

    public void setCom_id(String com_id) {
        this.com_id = com_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public int getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(int likeCnt) {
        this.likeCnt = likeCnt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
