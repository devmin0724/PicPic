package com.picpic.sikkle.beans;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedUserImageView;
import com.picpic.sikkle.widget.HashTagForInterest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-04-28.
 */
public class MyPageHeader extends AbstractItem<MyPageHeader, MyPageHeader.ViewHolder> {

    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public JSONObject jd;
    public TextWatcher tw;
    public View.OnClickListener onClickListener;
    public Activity ac;
    private RequestManager glide;
    private boolean sort = true;

    public void setSort(int a){
        if(a == 0){
            sort = true;
        }else{
            sort = false;
        }
    }

    public MyPageHeader withAcivity(Activity ac){
        this.ac = ac;
        glide = Glide.with(ac);
        return this;
    }

    public MyPageHeader withJD(JSONObject jd) {
        this.jd = jd;
        return this;
    }

    public MyPageHeader withTW(TextWatcher tw) {
        this.tw = tw;
        return this;
    }

    public MyPageHeader withListener(View.OnClickListener listener) {
        this.onClickListener = listener;
        return this;
    }

    @Override
    public int getType() {
        return R.id.item_mypage_header;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.haeder_mypage;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        Context con = holder.itemView.getContext();
        StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        if(params == null) {
            params = new StaggeredGridLayoutManager.LayoutParams(StaggeredGridLayoutManager.LayoutParams.MATCH_PARENT, StaggeredGridLayoutManager.LayoutParams.WRAP_CONTENT);
        }
//        params.setFullSpan(getItemType(i) == ITEM_TYPE_HEADER || getItemType(i) == ITEM_TYPE_FOOTER);
        params.setFullSpan(true);
        holder.itemView.setLayoutParams(params);
        try {
            final JSONArray p_tag_1 = new JSONArray(jd.getString("p_tag_1"));

            glide.load(AppController.URL + jd.getString("profile_picture"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.cimv);

            glide.load(AppController.URL + jd.getString("profile_picture"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.bimv);

//            holder.cimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));
//            holder.bimv.setImageURLString(AppController.URL + jd.getString("profile_picture"));

            holder.tvUserId.setText(jd.getString("id"));

            int p_tag_length = p_tag_1.length();

            if (!p_tag_1.getJSONObject(0).getString("tag_id").equals("null")) {
                holder.tvPopular.setText("#" + p_tag_1.getJSONObject(0).getString("tag_name"));
                holder.tvPopular.setTag(p_tag_1.getJSONObject(0).getString("tag_id"));
            } else {
                p_tag_length = 0;
            }

            holder.tvCount.setText("+" + p_tag_length);

            holder.tvFollowerCnt.setText(jd.getInt("follower_cnt") + "");
            holder.tvFollowingCnt.setText(jd.getInt("follow_cnt") + "");

            holder.tvPostCnt.setText(jd.getInt("post_cnt") + "");

            String commentsText = holder.tvPopular.getText().toString();

            ArrayList<int[]> hashtagSpans = getSpans(commentsText, '#');

            SpannableString commentsContent = new SpannableString(commentsText);

            for (int i = 0; i < hashtagSpans.size(); i++) {
                int[] span = hashtagSpans.get(i);
                int hashTagStart = span[0];
                int hashTagEnd = span[1];

                commentsContent.setSpan(new HashTagForInterest(con),
                        hashTagStart, hashTagEnd, 0);
            }

            holder.tvPopular.setMovementMethod(LinkMovementMethod.getInstance());
            holder.tvPopular.setText(commentsContent);

            if(sort){
                holder.layTop1.setSelected(true);
                holder.layTop2.setSelected(false);
            }else{
                holder.layTop1.setSelected(false);
                holder.layTop2.setSelected(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.edtInput.addTextChangedListener(tw);

        holder.bimv.setOnClickListener(onClickListener);

        holder.tvPostCnt.setOnClickListener(onClickListener);
        holder.tvUserId.setOnClickListener(onClickListener);
        holder.tvPopular.setOnClickListener(onClickListener);
        holder.tvCount.setOnClickListener(onClickListener);
        holder.tvFollowerCnt.setOnClickListener(onClickListener);
        holder.tvFollowingCnt.setOnClickListener(onClickListener);

        holder.imvEdit.setOnClickListener(onClickListener);
        holder.imvSearch.setOnClickListener(onClickListener);

        holder.layBack.setOnClickListener(onClickListener);
        holder.laySettings.setOnClickListener(onClickListener);
        holder.layShare.setOnClickListener(onClickListener);
        holder.layTop1.setOnClickListener(onClickListener);
        holder.layTop2.setOnClickListener(onClickListener);
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    /**
     * our ViewHolder
     */
    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imv_mypage_bg)
        protected FixedUserImageView bimv;

        @Bind(R.id.lay_mypage_back)
        protected LinearLayout layBack;
        @Bind(R.id.imv_mypage_share)
        protected LinearLayout layShare;
        @Bind(R.id.imv_mypage_settings)
        protected LinearLayout laySettings;

        @Bind(R.id.cimv_mypage_user_1)
        protected CircleImageView cimv;

        @Bind(R.id.imv_mypage_user_edit)
        protected ImageView imvEdit;

        @Bind(R.id.tv_mypage_user_id)
        protected TextView tvUserId;

        @Bind(R.id.tv_mypage_user_popular)
        protected TextView tvPopular;
        @Bind(R.id.tv_mypage_count)
        protected TextView tvCount;
        @Bind(R.id.tv_mypage_user_post_cnt)
        protected TextView tvPostCnt;
        @Bind(R.id.tv_mypage_user_follower_cnt)
        protected TextView tvFollowerCnt;
        @Bind(R.id.tv_mypage_user_following_cnt)
        protected TextView tvFollowingCnt;

        @Bind(R.id.imv_mypage_search)
        protected ImageView imvSearch;

        @Bind(R.id.edt_mypage_input)
        protected EditText edtInput;

        @Bind(R.id.lay_mypage_top1)
        protected LinearLayout layTop1;
        @Bind(R.id.lay_mypage_top2)
        protected LinearLayout layTop2;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
