package com.picpic.sikkle.beans;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class UserListResult extends ArrayList<UserListItem> {

    private static final long serialVersionUID = 1320728264846501071L;
}
