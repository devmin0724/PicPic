package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-06.
 */
public class AndTagSearchItem {

    private String tag_id = "";
    private String title = "";
    private int count = 0;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
