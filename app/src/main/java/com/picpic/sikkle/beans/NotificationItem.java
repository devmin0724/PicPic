package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-06.
 */
public class NotificationItem {

    public static final int NOTIFIACIONT_POST_LIKE = 0;//"PL";
    public static final int NOTIFIACIONT_COMMENT_LIKE = 1;//"CL";
    public static final int NOTIFIACIONT_POST_COMMENT = 2;//"PC";
    public static final int NOTIFIACIONT_FOLLOW_ME = 3;//"FM";
    public static final int NOTIFIACIONT_REPIC_ME = 4;//"RM";
    public static final int NOTIFIACIONT_REPORT_POST = 5;//"CMP";
    public static final int NOTIFIACIONT_REPORT_COMMENT = 6;//"CMC";
    public static final int NOTIFIACIONT_FOLLOW_PROFILE = 7;//"FP";
    public static final int NOTIFIACIONT_FOLLOW_ID = 8;//"FI";
    public static final int NOTIFIACIONT_FOLLOW_ANDTAG_JOIN = 9;//"FJA";
    public static final int NOTIFIACIONT_FOLLOW_ANDTAG_CREATE = 10;//"FCA";
    public static final int NOTIFIACIONT_NOTICE = 11;//"NO";
    public static final int NOTIFIACIONT_TAG_ME_POST = 12;//"TMP";
    public static final int NOTIFIACIONT_TAG_ME_COMMENT = 13;//"TMC";
    public static final int NOTIFIACIONT_REPLY_POST = 14;//"RP";

    private String pro = "";
    private String id = "";
    private String email = "";
    private String url = "";
    private String time = "";
    private int type = 0;
    private boolean isConfirm = false;
    private boolean isFollow = false;
    private int notiId = 0;
    private String targetId1 = "";
    private String targetId2 = "";
    private String targetName = "";

    public boolean isFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isConfirm() {
        return isConfirm;
    }

    public void setIsConfirm(boolean isConfirm) {
        this.isConfirm = isConfirm;
    }

    public int getNotiId() {
        return notiId;
    }

    public void setNotiId(int notiId) {
        this.notiId = notiId;
    }

    public String getTargetId1() {
        return targetId1;
    }

    public void setTargetId1(String targetId1) {
        this.targetId1 = targetId1;
    }

    public String getTargetId2() {
        return targetId2;
    }

    public void setTargetId2(String targetId2) {
        this.targetId2 = targetId2;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
