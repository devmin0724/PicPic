/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.beans;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.NativeAd;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VideoLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;
import com.picpic.sikkle.widget.VView;
import com.squareup.picasso.Picasso;

/**
 * Created by devmin-sikkle on 2016-03-25.
 */
public class TimelineRowItem extends AbstractItem<TimelineRowItem, TimelineRowItem.ViewHolder> {

    private final TimelineItem model;
    private final Picasso mImageLoader;
    private final Activity ac;

    private NativeAd nativeAd;
    private ImageLoader imageLoader;
    private ImageFullLoader imageFullLoader;
    private VideoLoader videoLoader;

    private int playCnt;

    public TimelineRowItem(TimelineItem model, Picasso mImageLoader, Activity ac) {
        this.model = model;
        this.mImageLoader = mImageLoader;
        this.ac = ac;

        this.imageLoader = new ImageLoader(getActivity());
        this.imageFullLoader = new ImageFullLoader(getActivity());
        this.videoLoader = new VideoLoader(getActivity());

        playCnt = model.getPlayCnt();
    }

    private Activity getActivity() {
        return ac;
    }

    private final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    protected class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_recycler_timeline;
    }

    public void animateHeart(View imageLovedOn, View imageLovedOff, boolean on) {
        imageLovedOn.setVisibility(View.VISIBLE);
        imageLovedOff.setVisibility(View.VISIBLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            viewPropertyStartCompat(imageLovedOff.animate().scaleX(on ? 0 : 1).scaleY(on ? 0 : 1).alpha(on ? 0 : 1));
            viewPropertyStartCompat(imageLovedOn.animate().scaleX(on ? 1 : 0).scaleY(on ? 1 : 0).alpha(on ? 1 : 0));
        }
    }

    /**
     * helper method for the animator on APIs < 14
     *
     * @param animator
     */
    public static void viewPropertyStartCompat(ViewPropertyAnimator animator) {
        if (Build.VERSION.SDK_INT >= 14) {
            animator.start();
        }
    }

    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    @Override
    public void bindView(final ViewHolder viewHolder) {
        super.bindView(viewHolder);

        if (!model.isAd()) {

            String tempUrl = model.getUrl().replace(".gif", ".mp4");

            viewHolder.lay.setVisibility(View.VISIBLE);
            viewHolder.layAd.setVisibility(View.GONE);

            viewHolder.like.setVisibility(View.INVISIBLE);

            final LikeView tempLike = viewHolder.like;

            viewHolder.layContent.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                @Override
                public void onClick() {
                    super.onClick();
//                    viewHolder.mCover.setVisibility(View.INVISIBLE);
                    viewHolder.mPlayer.start();
                    if (viewHolder.mPlayer.isPlaying()) {
                        viewHolder.mPlayer.pause();
                    } else {
                        viewHolder.mPlayer.start();
                    }
//                    switch (vv.isPlaying()) {
//                        case 0:
//                            vv.pause();
//                            break;
//                        case 1:
//                            vv.start();
//                            break;
//                        case 2:
//
//                            break;
//                    }
                }

                @Override
                public void onDoubleClick() {
                    super.onDoubleClick();
                    if (viewHolder.layLike.isSelected()) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (model.isLike()) {
                                    int lc = model.getLikeCount();
                                    viewHolder.tvLikeCnt.setText(lc - 1 + "");
                                    viewHolder.layLike.setSelected(false);
                                }
                            }
                        });
                        MinUtils.like(true, getActivity(), model.getPsot_id());

                    } else {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!model.isLike()) {
                                    int lc = model.getLikeCount();
                                    viewHolder.tvLikeCnt.setText(lc + 1 + "");
                                    viewHolder.layLike.setSelected(true);
                                }
                            }
                        });
                        tempLike.startAni();
                        MinUtils.like(false, getActivity(), model.getPsot_id());

                    }

                }

                @Override
                public void onLongClick() {
                    super.onLongClick();
                    Intent i = new Intent(getActivity(), GIFDownloadConfirmActivity.class);
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("body", model.getBody());
                    i.putExtra("pid", model.getPsot_id());
                    getActivity().startActivity(i);
                }
            });

            viewHolder.cimvPro.setTag(model.getOwnerId());

            imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimvPro, false);

            viewHolder.tvUserId.setText(model.getId());

            MinUtils.setCount(viewHolder.tvLikeCnt, model.getLikeCount());
            MinUtils.setCount(viewHolder.tvComCnt, model.getComCount());
            MinUtils.setCount(viewHolder.tvPlayCnt, model.getPlayCnt());

            MinUtils.setTags(ac, viewHolder.tvTags, model.getTags());

            if (model.isLike()) {
                viewHolder.layLike.setSelected(true);
            } else {
                viewHolder.layLike.setSelected(false);
            }

            viewHolder.layLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewHolder.layLike.isSelected()) {
                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (model.isLike()) {
                                    int lc = model.getLikeCount();
                                    viewHolder.tvLikeCnt.setText(lc - 1 + "");
                                    viewHolder.layLike.setSelected(false);
                                }
                            }
                        });
                        MinUtils.like(true, ac, model.getPsot_id());

                    } else {

                        ac.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!model.isLike()) {
                                    int lc = model.getLikeCount();
                                    viewHolder.tvLikeCnt.setText(lc + 1 + "");
                                    viewHolder.layLike.setSelected(true);
                                }
                            }
                        });
                        viewHolder.like.startAni();
                        MinUtils.like(false, ac, model.getPsot_id());

                    }
                }
            });

            viewHolder.layComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || viewHolder.layLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    ac.startActivity(i);
                }
            });

            viewHolder.btnShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SharePopUp.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("body", model.getBody());
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("w", 0);
                    i.putExtra("h", 0);
                    ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
                }
            });

            viewHolder.layMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, Popup2Activity.class);
                    i.putExtra("email", model.getOwnerId());
                    if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    } else {
                        i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                        i.putExtra("postId", model.getPsot_id());
                        i.putExtra("pc", "P");
                    }
                    i.putExtra("url", AppController.URL + model.getUrl());
                    i.putExtra("title", model.getId());
                    i.putExtra("body", model.getBody());
                    ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
                }
            });

            viewHolder.tvComCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 1);
                    i.putExtra("likeCount", model.getLikeCount() + "");
                    i.putExtra("comCount", model.getComCount() + "");
                    if (model.isLike() || viewHolder.layLike.isSelected()) {
                        i.putExtra("isLike", true);
                    } else {
                        i.putExtra("isLike", false);
                    }
                    ac.startActivity(i);
                }
            });
            viewHolder.tvLikeCnt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ac, UserListActivity.class);
                    i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                    i.putExtra("postId", model.getPsot_id());
                    ac.startActivity(i);
                }
            });

            viewHolder.cimvPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = viewHolder.cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });

            viewHolder.tvUserId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String myId = AppController.getSp().getString("email", "");
                    String tempEmail = viewHolder.cimvPro.getTag().toString();
                    if (tempEmail.equals(myId)) {
                        ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                        ac.finish();
                    } else {
                        if (!tempEmail.equals("")) {
                            Intent i = new Intent(ac, UserFeedActivity.class);
                            i.putExtra("email", tempEmail);
                            ac.startActivity(i);
                        }
                    }
                }
            });


            if (model.getBody().equals("") || model.getBody().length() <= 0) {
                viewHolder.tvBody.setVisibility(View.GONE);
            } else {
                viewHolder.tvBody.setVisibility(View.VISIBLE);
            }

            viewHolder.tvBody.setText(model.getBody());

            int w = model.getWidth();
            int h = model.getHeight();

            int height = 0;

            try {
                height = h * MinUtils.screenWidth / w;
            } catch (ArithmeticException e) {

            }

            String tempThubUrl = model.getUrl().replace("_2.gif", ".jpg");

            AppController.imageLoader.DisplayImage(AppController.URL + model.getPro_url(), viewHolder.cimvPro, true);

            viewHolder.mCover.setVisibility(View.VISIBLE);
            mImageLoader.load(Uri.parse(AppController.URL + tempThubUrl)).into(viewHolder.mCover);

            viewHolder.lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("navi", 0);
                    getActivity().startActivity(i);
                }
            });

            viewHolder.layContent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));

            viewHolder.like.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));

//            vv.addMediaPlayerListener(new MediaPlayerWrapper.MainThreadMediaPlayerListener() {
//                @Override
//                public void onVideoSizeChangedMainThread(int width, int height) {
//
//                }
//
//                @Override
//                public void onVideoPreparedMainThread() {
//
//                }
//
//                @Override
//                public void onVideoCompletionMainThread() {
//                    AppController.playCountExe.execute(new Runnable() {
//                        @Override
//                        public void run() {
//                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
//                        }
//                    });
//
//                    MinUtils.setCount(viewHolder.tvPlayCnt, playCnt + 1);
//                    playCnt = playCnt + 1;
//                }
//
//                @Override
//                public void onErrorMainThread(int what, int extra) {
//
//                }
//
//                @Override
//                public void onBufferingUpdateMainThread(int percent) {
//
//                }
//
//                @Override
//                public void onVideoStoppedMainThread() {
//
//                }
//            });

            viewHolder.mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    AppController.playCountExe.execute(new Runnable() {
                        @Override
                        public void run() {
                            new MinUtils.PlayCountSubmit(model.getPsot_id()).execute();
                        }
                    });

                    MinUtils.setCount(viewHolder.tvPlayCnt, playCnt + 1);
                    playCnt = playCnt + 1;
                }
            });

            videoLoader.DisplayVideo(AppController.URL + model.getUrl().replace(".gif", ".mp4"), viewHolder.mPlayer, false);

        } else {
            viewHolder.lay.setVisibility(View.GONE);

            if (model.isAdVisible()) {

                viewHolder.layAd.setVisibility(View.VISIBLE);

                nativeAd = new NativeAd(getActivity(), getActivity().getResources().getString(R.string.facenook_ad_id));

                nativeAd.setAdListener(new AdListener() {

                    @Override
                    public void onError(Ad ad, AdError error) {
                        Log.e("error", error.getErrorMessage() + " / " + error.getErrorCode());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        if (ad != nativeAd) {
                            viewHolder.layAd.setVisibility(View.GONE);
                            return;
                        }
//                        if (BuildConfig.DEBUG) {
                        Log.e("face book ad", "fbad / " + nativeAd.getAdBody());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesLinkUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdSocialContext());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdSubtitle());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdTitle());
                        Log.e("face book ad", "fbad / " + nativeAd.getId());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesIcon().getUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdCoverImage().getUrl());
                        Log.e("face book ad", "fbad / " + nativeAd.getAdIcon().getUrl());
//                        }

                        viewHolder.tvTitle.setText(nativeAd.getAdTitle());

                        imageFullLoader.DisplayImage(nativeAd.getAdCoverImage().getUrl(), viewHolder.bimv, false);

                        imageLoader.DisplayImage(nativeAd.getAdChoicesIcon().getUrl(), viewHolder.imvIcon, false);

                        viewHolder.tvSubTitle.setText(nativeAd.getAdSubtitle());

                        viewHolder.btnAction.setText(nativeAd.getAdCallToAction());

                        nativeAd.registerViewForInteraction(viewHolder.layAd);

                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }
                });

                viewHolder.tvTitle.setText(model.getAdTitle());

                imageFullLoader.DisplayImage(model.getAdCoverImg(), viewHolder.bimv, false);

                imageLoader.DisplayImage(model.getAdChoicesIcon(), viewHolder.imvIcon, false);

                viewHolder.tvSubTitle.setText(model.getAdSubTitle());

                viewHolder.btnAction.setText(model.getAdCallToAction());

                nativeAd.registerViewForInteraction(viewHolder.layAd);

                nativeAd.loadAd();


            } else {
                viewHolder.layAd.setVisibility(View.GONE);
            }
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final VView mPlayer;
        public final BoundableOfflineImageView mCover;
        public final LinearLayout lay;

        public final CircleImageView cimvPro;
        public final LinearLayout layLike;
        public final LinearLayout layComment;
        public final LinearLayout layMore;
        public final FrameLayout layContent;
        public final Button btnShare;
        public final TextView tvUserId;
        public final TextView tvBody;
        public final TextView tvTags;
        public final TextView tvLikeCnt;
        public final TextView tvComCnt;
        public final TextView tvPlayCnt;
        public final LikeView like;
        public final TextView tvTitle;
        public final ImageView imvIcon;
        public final TextView tvSubTitle;
        public final BoundableOfflineImageView bimv;
        public final Button btnAction;
        public final LinearLayout layAd;

        public ViewHolder(View view) {
            super(view);

            lay = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline);

//        mPlayer = (VideoPlayerView) view.findViewById(R.id.player);
            mPlayer = (VView) view.findViewById(R.id.vv_recycler_row_timeline);

            cimvPro = (CircleImageView) view.findViewById(R.id.cimv_recycler_row_timeline);

            layLike = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline_like);

            layComment = (LinearLayout) view.findViewById(R.id.lay_recycler_row_timeline_comment);

            layMore = (LinearLayout) view.findViewById(R.id.imv_recycler_row_timeline_more);

            layContent = (FrameLayout) view.findViewById(R.id.lay_recycler_row_timeline_content);

            btnShare = (Button) view.findViewById(R.id.btn_recycler_row_timeline_share);

            tvUserId = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_id);

            tvBody = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_body);

            tvTags = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_tags);

            tvLikeCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_like_count);

            tvComCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_comment_count);

            tvPlayCnt = (TextView) view.findViewById(R.id.tv_recycler_row_timeline_play_count);

            like = (LikeView) view.findViewById(R.id.imv_recycler_row_timeline_like);

            layAd = (LinearLayout) view.findViewById(R.id.lay_ad_row);

            tvTitle = (TextView) view.findViewById(R.id.tv_ad_row_title);

            bimv = (BoundableOfflineImageView) view.findViewById(R.id.bimv_ad_row);

//        mCover = (BoundableOfflineImageView) view.findViewById(R.id.cover);
            mCover = (BoundableOfflineImageView) view.findViewById(R.id.bimv_recycler_row_timeline);

            imvIcon = (ImageView) view.findViewById(R.id.imv_ad_row);

            tvSubTitle = (TextView) view.findViewById(R.id.tv_ad_row_sub_title);

            btnAction = (Button) view.findViewById(R.id.btn_ad_row_do_it);
        }
    }

}
