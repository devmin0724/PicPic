package com.picpic.sikkle.beans;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.items.GenericAbstractItem;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.cache.GIFLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by devmin on 2016-04-29.
 */
public class GenericItem extends GenericAbstractItem<GridItem, GenericItem, GenericItem.ViewHolder> {
    private static final ViewHolderFactory<? extends ViewHolder> FACTORY = new ItemFactory();

    public GenericItem(GridItem icon) {
        super(icon);
        this.model = icon;
    }

    public GridItem model;
    private GIFLoader gifLoader;

    @Override
    public int getType() {
        return R.id.item_mypage_single;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_grid;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);

        final Context con = holder.itemView.getContext();
        GIFLoader gifLoader = new GIFLoader(con);

//        try {
//            String tag = holder.bimv.getTag().toString();
//
//        } catch (NullPointerException e) {
        String tempStr = model.getUrl();
        int underbarCount = tempStr.lastIndexOf("_");
        final String tempUrl = AppController.URL + tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

        String tempUrl2 = model.getUrl();

        int lastIdx = tempUrl2.lastIndexOf("_");

        String tempName;
        try {
            tempName = AppController.URL + tempUrl2.substring(0, lastIdx) + ".jpg";
        } catch (StringIndexOutOfBoundsException ee) {
            tempName = "";
        }
        holder.bimv.setTag(tempUrl);

        holder.bimv.setImageBitmap(null);

//        Glide.clear(holder.bimv);
//        Glide.with(con)
//                .load(tempUrl)
//                .animate(R.anim.alpha_on)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.bimv);
        gifLoader.DisplayImage(tempUrl, holder.bimv, false);
//        }

//        glide.load(tempUrl)
//                .asGif()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.bimv);
//        gifLoader.DisplayImage(tempUrl, holder.bimv, false);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(con, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPost_id());
                i.putExtra("navi", 0);
                con.startActivity(i);
            }
        });
    }

    /**
     * our ItemFactory implementation which creates the ViewHolder for our adapter.
     * It is highly recommended to implement a ViewHolderFactory as it is 0-1ms faster for ViewHolder creation,
     * and it is also many many times more efficient if you define custom listeners on views within your item.
     */
    protected static class ItemFactory implements ViewHolderFactory<ViewHolder> {
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    /**
     * return our ViewHolderFactory implementation here
     *
     * @return
     */
    @Override
    public ViewHolderFactory<? extends ViewHolder> getFactory() {
        return FACTORY;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.bimv_grid_row)
        protected BoundableOfflineImageView bimv;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
