package com.picpic.sikkle.beans;

import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;

import java.util.ArrayList;

/**
 * Created by devmin-sikkle on 2015-11-20.
 */
public class StickerData {

    private Matrix mMatrix;
    private float mStickerScaleSize = 1.0f;
    private float cX;
    private float cY;
    private ArrayList<MotionEvent> motionArr;
    private float[] mPoints;
    private float[] mOriginPoints;
    private Paint mPaint;
    private RectF mOriginContentRect;
    private RectF mContentRect;

    public float[] getmOriginPoints() {
        return mOriginPoints;
    }

    public void setmOriginPoints(float[] mOriginPoints) {
        this.mOriginPoints = mOriginPoints;
    }

    public RectF getmOriginContentRect() {
        return mOriginContentRect;
    }

    public void setmOriginContentRect(RectF mOriginContentRect) {
        this.mOriginContentRect = mOriginContentRect;
    }

    public RectF getmContentRect() {
        return mContentRect;
    }

    public void setmContentRect(RectF mContentRect) {
        this.mContentRect = mContentRect;
    }

    public Paint getmPaint() {
        return mPaint;
    }

    public void setmPaint(Paint mPaint) {
        this.mPaint = mPaint;
    }

    public float[] getmPoints() {
        return mPoints;
    }

    public void setmPoints(float[] mPoints) {
        this.mPoints = mPoints;
    }

    public ArrayList<MotionEvent> getMotionArr() {
        return motionArr;
    }

    public void setMotionArr(ArrayList<MotionEvent> motionArr) {
        this.motionArr = motionArr;
    }

    public Matrix getmMatrix() {
        return mMatrix;
    }

    public void setmMatrix(Matrix mMatrix) {
        this.mMatrix = mMatrix;
    }

    public float getmStickerScaleSize() {
        return mStickerScaleSize;
    }

    public void setmStickerScaleSize(float mStickerScaleSize) {
        this.mStickerScaleSize = mStickerScaleSize;
    }

    public float getcX() {
        return cX;
    }

    public void setcX(float cX) {
        this.cX = cX;
    }

    public float getcY() {
        return cY;
    }

    public void setcY(float cY) {
        this.cY = cY;
    }
}
