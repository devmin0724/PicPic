package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class FriendResultItem {
    private String proUrl = "";
    private String email = "";
    private String name = "";
    private boolean isFollow = false;
    private String url1 = "";
    private String url2 = "";
    private String url3 = "";
    private String url4 = "";

    private String postId1 = "";
    private String postId2 = "";
    private String postId3 = "";
    private String postId4 = "";

    public String getPostId1() {
        return postId1;
    }

    public void setPostId1(String postId1) {
        this.postId1 = postId1;
    }

    public String getPostId2() {
        return postId2;
    }

    public void setPostId2(String postId2) {
        this.postId2 = postId2;
    }

    public String getPostId3() {
        return postId3;
    }

    public void setPostId3(String postId3) {
        this.postId3 = postId3;
    }

    public String getPostId4() {
        return postId4;
    }

    public void setPostId4(String postId4) {
        this.postId4 = postId4;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProUrl() {
        return proUrl;
    }

    public void setProUrl(String proUrl) {
        this.proUrl = proUrl;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public String getUrl4() {
        return url4;
    }

    public void setUrl4(String url4) {
        this.url4 = url4;
    }
}
