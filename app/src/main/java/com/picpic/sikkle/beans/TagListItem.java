package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-17.
 */
public class TagListItem {
    private String tag_id = "";
    private String tag_name = "";
    private String tag_body = "";
    private String tag_url = "";
    private int join_cnt = 0;
    private boolean isFolow = false;

    public boolean isFolow() {
        return isFolow;
    }

    public void setIsFolow(boolean isFolow) {
        this.isFolow = isFolow;
    }

    public String getTag_body() {
        return tag_body;
    }

    public void setTag_body(String tag_body) {
        this.tag_body = tag_body;
    }

    public int getJoin_cnt() {
        return join_cnt;
    }

    public void setJoin_cnt(int join_cnt) {
        this.join_cnt = join_cnt;
    }

    public String getTag_url() {
        return tag_url;
    }

    public void setTag_url(String tag_url) {
        this.tag_url = tag_url;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }
}
