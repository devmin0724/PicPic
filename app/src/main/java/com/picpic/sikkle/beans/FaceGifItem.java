package com.picpic.sikkle.beans;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by devmin-sikkle on 2015-12-17.
 */
public class FaceGifItem {
    private GifDrawable gd;
    private String path;
    private String path2;

    public String getPath2() {
        return path2;
    }

    public void setPath2(String path2) {
        this.path2 = path2;
    }

    public GifDrawable getGd() {
        return gd;
    }

    public void setGd(GifDrawable gd) {
        this.gd = gd;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
