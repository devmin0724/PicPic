package com.picpic.sikkle.beans;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikepenz.fastadapter.items.AbstractItem;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.GIFDownloadConfirmActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.OnSwipeTouchListener;
import com.picpic.sikkle.utils.cache.ImageLoader;
import com.picpic.sikkle.utils.cache.VGLoader;
import com.picpic.sikkle.widget.BoundableOfflineImageView;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.LikeView;

/**
 * Created by devmin on 2016-04-26.
 */
public class TestItem extends AbstractItem<TestItem, TestItem.ViewHolder> {

    private final TimelineItem model;
    private final Activity ac;
    private final ImageLoader imageLoader;
    private final VGLoader vgLoader;

    public TestItem(TimelineItem model, Activity ac) {
        this.model = model;
        this.ac = ac;
        imageLoader = new ImageLoader(ac);
        vgLoader = new VGLoader(ac);
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_recycler;
    }

    @Override
    public void bindView(final ViewHolder holder) {
        super.bindView(holder);

        holder.layTopBlank.setVisibility(View.GONE);

        final String gifUrl = AppController.URL + model.getUrl();
        String thumbUrl = gifUrl.replace("_2.gif", ".jpg");
        String mp4Url = gifUrl.replace(".gif", ".mp4");

        Log.e("url", gifUrl + "\n" + thumbUrl);

//            gifLoader.DisplayImage(gifUrl, holder.mCover, false);
//        vgLoader.DisplayVG(mp4Url, holder.mCover, false);

        Glide.with(ac)
                .load(gifUrl)
                .asGif()
                .placeholder(R.drawable.nowloading)
                .error(R.drawable.non_interest)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(holder.mCover);

        String tempPath = "/storage/emulated/0/test.mp4.mp4";
        String tempPath2 = "/storage/emulated/0/Download/c4ca4238a0b923820dcc509a6f75849b.mp4";


//        Glide.with(ac)
//                .load(Uri.fromFile(new File(tempPath)))
//                .into(holder.mCover);
//        Glide
//                .with(ac)
//                .load(Uri.fromFile(new File(tempPath2)))
//                .asGif()
//                .placeholder(R.drawable.nowloading)
//                .error(R.drawable.non_interest)
//                .into(holder.mCover);
//
//        holder.mCover.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
//            @Override
//            public void onImageChanged(Drawable d) {
////                int a = 1;
////                TransitionDrawable dd = (TransitionDrawable) d;
////                dd.start
//            }
//        });
//
//            BitmapPool bitmapPool = Glide.get(getApplicationContext()).getBitmapPool();
//            FileDescriptorBitmapDecoder decoder = new FileDescriptorBitmapDecoder(
//                    new VideoBitmapDecoder(10000),
//                    bitmapPool,
//                    DecodeFormat.PREFER_ARGB_8888);
//
//            loader.load(mp4Url)
////                    .crossFade()
////                    .placeholder(R.drawable.nowloading)
////                    .error(R.drawable.non_interest)
////                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .asGif()
//                    .decoder(new ResourceDecoder<InputStream, GifDrawable>() {
//                        @Override
//                        public Resource<GifDrawable> decode(InputStream inputStream, int i, int i1) throws IOException {
//                            return null;
//                        }
//
//                        @Override
//                        public String getId() {
//                            return null;
//                        }
//                    })
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .placeholder(R.drawable.nowloading)
//                    .error(R.drawable.non_interest)
//                    .into(holder.mCover);
////                    .asBitmap()
////                    .videoDecoder(decoder)
////                    .into(holder.mCover);
//        vgLoader.DisplayVG(mp4Url, holder.mCover, false);

//            loader.load(gifUrl)
//                    .crossFade()
//                    .placeholder(R.drawable.nowloading)
//                    .error(R.drawable.non_interest)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(holder.mCover);

        holder.layRow.setVisibility(View.VISIBLE);

        holder.likeView.setVisibility(View.INVISIBLE);


        holder.layContent.setOnTouchListener(new OnSwipeTouchListener(ac) {
            @Override
            public void onClick() {
                super.onClick();
//                    if (holder.mCover.getDrawable() instanceof GifDrawable) {
//                        pl.droidsonroids.gif.GifDrawable gd = (pl.droidsonroids.gif.GifDrawable) holder.mCover.getDrawable();
//
////                        if(gd.isPlaying()){
////                            gd.pause();
////                        }else{
//                        gd.start();
////                        }
//
//                    }
//                    switch (holder.mPlayer.isPlaying()) {
//                        case 0:
//                            holder.mPlayer.pause();
//                            break;
//                        case 1:
//                            holder.mPlayer.start();
//                            break;
//                        case 2:
//
//                            break;
//                    }
            }

            @Override
            public void onDoubleClick() {
                super.onDoubleClick();
                if (holder.layLike.isSelected()) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            holder.tvLikeCnt.setText(lc - 1 + "");
                            holder.layLike.setSelected(false);
                            model.setLikeCount(lc - 1);
                        }
                    });
                    MinUtils.like(true, ac, model.getPsot_id());

                } else {

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            holder.tvLikeCnt.setText(lc + 1 + "");
                            holder.layLike.setSelected(true);
                            model.setLikeCount(lc + 1);
                        }
                    });
                    holder.likeView.startAni();
                    MinUtils.like(false, ac, model.getPsot_id());

                }

            }

            @Override
            public void onLongClick() {
                super.onLongClick();
                Intent i = new Intent(ac, GIFDownloadConfirmActivity.class);
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("body", model.getBody());
                i.putExtra("pid", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        holder.cimv.setTag(model.getOwnerId());

        imageLoader.DisplayImage(AppController.URL + model.getPro_url(), holder.cimv, false);
        holder.tvId.setText(model.getId());

        MinUtils.setCount(holder.tvLikeCnt, model.getLikeCount());
        MinUtils.setCount(holder.tvComCnt, model.getComCount());
        MinUtils.setCount(holder.tvPlayCnt, model.getPlayCnt());

        MinUtils.setTags(ac, holder.tvTags, model.getTags());

        if (model.isLike()) {
            holder.layLike.setSelected(true);
        } else {
            holder.layLike.setSelected(false);
        }

        holder.layLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.layLike.isSelected()) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            holder.tvLikeCnt.setText(lc - 1 + "");
                            holder.layLike.setSelected(false);
                            model.setLikeCount(lc - 1);
                        }
                    });
                    MinUtils.like(true, ac, model.getPsot_id());

                } else {

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int lc = model.getLikeCount();
                            holder.tvLikeCnt.setText(lc + 1 + "");
                            holder.layLike.setSelected(true);
                            model.setLikeCount(lc + 1);
                        }
                    });
                    holder.likeView.startAni();
                    MinUtils.like(false, ac, model.getPsot_id());

                }
            }
        });

        holder.layComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || holder.layLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                ac.startActivity(i);
            }
        });

        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SharePopUp.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("body", model.getBody());
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("w", 0);
                i.putExtra("h", 0);
                ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
            }
        });

        holder.layMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, Popup2Activity.class);
                i.putExtra("email", model.getOwnerId());
                if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                } else {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                }
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("title", model.getId());
                i.putExtra("body", model.getBody());
                ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
            }
        });

        holder.tvComCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || holder.layLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                ac.startActivity(i);
            }
        });
        holder.tvLikeCnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, UserListActivity.class);
                i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i.putExtra("postId", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        holder.cimv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = holder.cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });

        holder.tvId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = holder.cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });


        if (model.getBody().equals("") || model.getBody().length() <= 0) {
            holder.mTitle.setVisibility(View.GONE);
        } else {
            holder.mTitle.setVisibility(View.VISIBLE);
        }

        holder.mTitle.setText(model.getBody());

        int w = model.getWidth();
        int h = model.getHeight();

        int height = 0;

        try {
            height = h * MinUtils.screenWidth / w;
        } catch (ArithmeticException e) {

        }

//            AppController.imageLoader.DisplayImage(AppController.URL + model.getPro_url(), holder.cimv, true);
//            Glide.with(ac).load(AppController.URL + model.getPro_url()).into(holder.cimv);

//            holder.mCover.setVisibility(View.VISIBLE);
//            mImageLoader.load(Uri.parse(AppController.URL + tempThubUrl)).into(holder.mCover);
//            imageFullLoader.DisplayImage(AppController.URL + tempThubUrl, holder.mCover, false);

        holder.layRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 0);
                ac.startActivity(i);
            }
        });
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public final LinearLayout layTopBlank;
        public final LinearLayout layRow;
        public final TextView mTitle;
        public final BoundableOfflineImageView mCover;
        public final LikeView likeView;
        public final FrameLayout layContent;
        public final CircleImageView cimv;
        public final TextView tvId;
        public final LinearLayout layMore;
        public final TextView tvTags;
        public final LinearLayout layLike;
        public final TextView tvLikeCnt;
        public final LinearLayout layComment;
        public final TextView tvComCnt;
        public final LinearLayout layPlayCnt;
        public final TextView tvPlayCnt;
        public final Button btnShare;

        public ViewHolder(View view) {
            super(view);
            layTopBlank = (LinearLayout) view.findViewById(R.id.lay_row_recycler_top_blank);
            layRow = (LinearLayout) view.findViewById(R.id.lay_row_recycler);
            mTitle = (TextView) view.findViewById(R.id.tv_row_recycler_body);
            mCover = (BoundableOfflineImageView) view.findViewById(R.id.bimv_row_recycler);
            likeView = (LikeView) view.findViewById(R.id.imv_row_recycler_like);
            layContent = (FrameLayout) view.findViewById(R.id.lay_row_recycler_content);
            cimv = (CircleImageView) view.findViewById(R.id.cimv_row_recycler);
            tvId = (TextView) view.findViewById(R.id.tv_row_recycler_id);
            layMore = (LinearLayout) view.findViewById(R.id.imv_row_recycler_more);
            tvTags = (TextView) view.findViewById(R.id.tv_row_recycler_tags);
            layLike = (LinearLayout) view.findViewById(R.id.lay_row_recycler_like);
            tvLikeCnt = (TextView) view.findViewById(R.id.tv_row_recycler_like_count);
            layComment = (LinearLayout) view.findViewById(R.id.lay_row_recycler_comment);
            tvComCnt = (TextView) view.findViewById(R.id.tv_row_recycler_comment_count);
            layPlayCnt = (LinearLayout) view.findViewById(R.id.lay_row_recycler_play_count);
            tvPlayCnt = (TextView) view.findViewById(R.id.tv_row_recycler_play_count);
            btnShare = (Button) view.findViewById(R.id.btn_row_recycler_share);
        }
    }
}
