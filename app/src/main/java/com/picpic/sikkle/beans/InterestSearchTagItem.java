package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-05.
 */
public class InterestSearchTagItem {

//    private ArrayList<String> ids;
//    private ArrayList<String> urls;
//    private ArrayList<String> names;
//
//    public ArrayList<String> getIds() {
//        return ids;
//    }
//
//    public void setIds(ArrayList<String> ids) {
//        this.ids = ids;
//    }
//
//    public ArrayList<String> getNames() {
//        return names;
//    }
//
//    public void setNames(ArrayList<String> names) {
//        this.names = names;
//    }
//
//    public ArrayList<String> getUrls() {
//        return urls;
//    }
//
//    public void setUrls(ArrayList<String> urls) {
//        this.urls = urls;
//    }

    private String id;
    private String url;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
