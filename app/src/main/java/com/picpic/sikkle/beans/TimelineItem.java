/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class TimelineItem {
    private String pro_url = "";
    private String id = "";
    private String time = "";
    private int playCnt = 0;
    private String url = "";
    private String body = "";
    private int likeCount = 0;
    private int comCount = 0;
    private boolean isLike = false;
    private String psot_id = "";
    private String ownerId = "";
    private String andTagId = "";
    private String andTagName = "";
    private String andTagBody = "";
    private boolean isAndTag = false;
    private boolean isFollow = false;
    private TimelineLastCommentItem tlci = new TimelineLastCommentItem();
    private boolean isLoading = false;
    private int width = 0;
    private int height = 0;
    private int width2 = 0;
    private int height2 = 0;
    private String tags = "";
    private boolean isAd = false;
    private boolean isAdVisible = true;
    private String adTitle = "";
    private String adCoverImg = "";
    private String adSubTitle = "";
    private String adCallToAction = "";
    private String adChoicesIcon = "";
    private String adChoiceLinkUrl = "";
    private int topNavi = 0;

    public int getTopNavi() {
        return topNavi;
    }

    public void setTopNavi(int topNavi) {
        this.topNavi = topNavi;
    }

    public boolean isAdVisible() {
        return isAdVisible;
    }

    public void setAdVisible(boolean adVisible) {
        isAdVisible = adVisible;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getAdCoverImg() {
        return adCoverImg;
    }

    public void setAdCoverImg(String adCoverImg) {
        this.adCoverImg = adCoverImg;
    }

    public String getAdSubTitle() {
        return adSubTitle;
    }

    public void setAdSubTitle(String adSubTitle) {
        this.adSubTitle = adSubTitle;
    }

    public String getAdCallToAction() {
        return adCallToAction;
    }

    public void setAdCallToAction(String adCallToAction) {
        this.adCallToAction = adCallToAction;
    }

    public String getAdChoicesIcon() {
        return adChoicesIcon;
    }

    public void setAdChoicesIcon(String adChoicesIcon) {
        this.adChoicesIcon = adChoicesIcon;
    }

    public String getAdChoiceLinkUrl() {
        return adChoiceLinkUrl;
    }

    public void setAdChoiceLinkUrl(String adChoiceLinkUrl) {
        this.adChoiceLinkUrl = adChoiceLinkUrl;
    }

    public boolean isAd() {
        return isAd;
    }

    public void setAd(boolean ad) {
        isAd = ad;
    }

    public int getWidth2() {
        return width2;
    }

    public void setWidth2(int width2) {
        this.width2 = width2;
    }

    public int getHeight2() {
        return height2;
    }

    public void setHeight2(int height2) {
        this.height2 = height2;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setAndTag(boolean andTag) {
        isAndTag = andTag;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public String getAndTagBody() {
        return andTagBody;
    }

    public void setAndTagBody(String andTagBody) {
        this.andTagBody = andTagBody;
    }

    public String getAndTagName() {
        return andTagName;
    }

    public void setAndTagName(String andTagName) {
        this.andTagName = andTagName;
    }

    public String getAndTagId() {
        return andTagId;
    }

    public void setAndTagId(String andTagId) {
        this.andTagId = andTagId;
    }

    public boolean isAndTag() {
        return isAndTag;
    }

    public void setIsAndTag(boolean isAndTag) {
        this.isAndTag = isAndTag;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getComCount() {
        return comCount;
    }

    public void setComCount(int comCount) {
        this.comCount = comCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getPlayCnt() {
        return playCnt;
    }

    public void setPlayCnt(int playCnt) {
        this.playCnt = playCnt;
    }

    public String getPro_url() {
        return pro_url;
    }

    public void setPro_url(String pro_url) {
        this.pro_url = pro_url;
    }

    public String getPsot_id() {
        return psot_id;
    }

    public void setPsot_id(String psot_id) {
        this.psot_id = psot_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public TimelineLastCommentItem getTlci() {
        return tlci;
    }

    public void setTlci(TimelineLastCommentItem tlci) {
        this.tlci = tlci;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
