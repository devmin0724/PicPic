package com.picpic.sikkle.beans;

/**
 * Created by devmin on 2016-05-06.
 */
public class Video {

    String id, url, indexPosition;

    public Video(String indexPosition, String id, String url) {
        this.id = id;
        this.url = url;
        this.indexPosition = indexPosition;
    }

    public String getIndexPosition() {
        return indexPosition;
    }

    public void setIndexPosition(String indexPosition) {
        this.indexPosition = indexPosition;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
