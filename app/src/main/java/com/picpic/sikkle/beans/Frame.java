package com.picpic.sikkle.beans;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by devmin on 2016-01-19.
 */
public interface Frame {
    File getFile();

    Bitmap getbitmap();
}
