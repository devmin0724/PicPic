package com.picpic.sikkle.beans;

import android.graphics.Bitmap;

/**
 * Created by Jong-min on 2015-08-21.
 */
public class LoadToTempItem {
    private String folderUrl = "";
    private Bitmap thumb;
    private int size = 0;
    private String folderName = "";

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderUrl() {
        return folderUrl;
    }

    public void setFolderUrl(String folderUrl) {
        this.folderUrl = folderUrl;
    }

    public Bitmap getThumb() {
        return thumb;
    }

    public void setThumb(Bitmap thumb) {
        this.thumb = thumb;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
