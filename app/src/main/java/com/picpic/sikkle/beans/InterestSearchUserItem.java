package com.picpic.sikkle.beans;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-05.
 */
public class InterestSearchUserItem {
    private String id = "";
    private String email = "";
    private String pro = "";
    private boolean isFollow = false;
    private boolean isPublic = false;
    private boolean isCut = false;
    private String tag1 = "";
    private String tag2 = "";
    private String tag3 = "";
    private ArrayList<SimplePostItem> postArr;

    public boolean isCut() {
        return isCut;
    }

    public void setIsCut(boolean isCut) {
        this.isCut = isCut;
    }

    public ArrayList<SimplePostItem> getPostArr() {
        return postArr;
    }

    public void setPostArr(ArrayList<SimplePostItem> postArr) {
        this.postArr = postArr;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public void setTag3(String tag3) {
        this.tag3 = tag3;
    }
}
