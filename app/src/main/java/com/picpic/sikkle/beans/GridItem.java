package com.picpic.sikkle.beans;

/**
 * Created by Jong-min on 2015-11-06.
 */
public class GridItem {

    private String post_id = "";
    private String url = "";
    private int height1 = 0;
    private int height2 = 0;
    private int heightTH = 0;
    private int width1 = 0;
    private int width2 = 0;
    private int widthTH = 0;
    private int size1 = 0;
    private int size2 = 0;
    private int sizeTH = 0;
    private boolean is0 = false;

    public boolean is0() {
        return is0;
    }

    public void setIs0(boolean is0) {
        this.is0 = is0;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public int getHeight1() {
        return height1;
    }

    public void setHeight1(int height1) {
        this.height1 = height1;
    }

    public int getHeight2() {
        return height2;
    }

    public void setHeight2(int height2) {
        this.height2 = height2;
    }

    public int getHeightTH() {
        return heightTH;
    }

    public void setHeightTH(int heightTH) {
        this.heightTH = heightTH;
    }

    public int getSize1() {
        return size1;
    }

    public void setSize1(int size1) {
        this.size1 = size1;
    }

    public int getSize2() {
        return size2;
    }

    public void setSize2(int size2) {
        this.size2 = size2;
    }

    public int getSizeTH() {
        return sizeTH;
    }

    public void setSizeTH(int sizeTH) {
        this.sizeTH = sizeTH;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth1() {
        return width1;
    }

    public void setWidth1(int width1) {
        this.width1 = width1;
    }

    public int getWidth2() {
        return width2;
    }

    public void setWidth2(int width2) {
        this.width2 = width2;
    }

    public int getWidthTH() {
        return widthTH;
    }

    public void setWidthTH(int widthTH) {
        this.widthTH = widthTH;
    }
}
