package com.picpic.sikkle.beans;

/**
 * Created by devmin-sikkle on 2015-12-15.
 */
public class FaceSliceItem {
    int bgw = 0;
    int bgh = 0;
    int facew = 0;
    int faceh = 0;
    int[] xs;
    int[] ys;
    int[] rotates;
    int[] bgs;

    public int[] getBgs() {
        return bgs;
    }

    public void setBgs(int[] bgs) {
        this.bgs = bgs;
    }

    public int getBgh() {
        return bgh;
    }

    public void setBgh(int bgh) {
        this.bgh = bgh;
    }

    public int getBgw() {
        return bgw;
    }

    public void setBgw(int bgw) {
        this.bgw = bgw;
    }

    public int getFaceh() {
        return faceh;
    }

    public void setFaceh(int faceh) {
        this.faceh = faceh;
    }

    public int getFacew() {
        return facew;
    }

    public void setFacew(int facew) {
        this.facew = facew;
    }

    public int[] getRotates() {
        return rotates;
    }

    public void setRotates(int[] rotates) {
        this.rotates = rotates;
    }

    public int[] getXs() {
        return xs;
    }

    public void setXs(int[] xs) {
        this.xs = xs;
    }

    public int[] getYs() {
        return ys;
    }

    public void setYs(int[] ys) {
        this.ys = ys;
    }
}
