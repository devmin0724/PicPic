package com.picpic.sikkle.beans;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-07.
 */
public class AndTagRankItem {
    private String pro = "";
    private String email = "";
    private String id = "";
    private String pro1 = "";
    private String pro2 = "";
    private String pro3 = "";
    private String url = "";
    private int count = 0;
    private int joinCount = 0;

    private String title = "";
    private String body = "";
    private String tag_id = "";
    private String first_post_id = "";
    private ArrayList<AndTagTaggerItem> taggers;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<AndTagTaggerItem> getTaggers() {
        return taggers;
    }

    public void setTaggers(ArrayList<AndTagTaggerItem> taggers) {
        this.taggers = taggers;
    }

    public String getFirst_post_id() {
        return first_post_id;
    }

    public void setFirst_post_id(String first_post_id) {
        this.first_post_id = first_post_id;
    }

    public int getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(int joinCount) {
        this.joinCount = joinCount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPro1() {
        return pro1;
    }

    public void setPro1(String pro1) {
        this.pro1 = pro1;
    }

    public String getPro2() {
        return pro2;
    }

    public void setPro2(String pro2) {
        this.pro2 = pro2;
    }

    public String getPro3() {
        return pro3;
    }

    public void setPro3(String pro3) {
        this.pro3 = pro3;
    }

    public String getPro() {
        return pro;
    }

    public void setPro(String pro) {
        this.pro = pro;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
