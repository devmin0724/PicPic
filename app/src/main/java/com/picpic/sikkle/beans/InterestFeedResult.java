/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.beans;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class InterestFeedResult extends ArrayList<InterestFeedItem> {

    private static final long serialVersionUID = 1320728264846501071L;
}
