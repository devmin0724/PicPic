package com.picpic.sikkle.beans;

/**
 * Created by L on 2015-03-18.
 */
public class NoticeChildItem {

    private String body = "";

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
