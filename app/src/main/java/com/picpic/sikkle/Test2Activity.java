package com.picpic.sikkle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.adapter.NewTimeLineAdapter;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.ui.InterestFeedActivity;
import com.picpic.sikkle.ui.InterestFeedV1Activity;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.NotificationActivity;
import com.picpic.sikkle.ui.ReportActivity;
import com.picpic.sikkle.ui.login_join.FriendResultActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.tutorial.TutorialCamera4;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test2Activity extends Activity implements View.OnClickListener {
    private static final int TIMELINE_RETURN = 1001;
    public static final int SHARE_RETURN = 1002;
    public static final int POST_WRITE_RETURN = 1003;
    public static final int MORE_RETURN = 1004;
    public static final int FOLLOW_RETURN = 1005;
//    LinearLayout layIntro, imvHome, imvAlarm;
//    ImageView imvChatting;
//    LinearLayout layTimeline, layInterest, layCamera, layAndTag, layMypage, laySearch;
//    PullToRefreshListView list;
//    Button btnIntro;
    //    FloatingActionLayout fal;
    //    int pageNavi = 1;
    boolean isEnd = false, lastItemVisibleFlag = false, isLoading = false;
    TimelineResult m_ResultList = null;
    NewTimeLineAdapter m_ListAdapter = null;
    int gifWidth = 200, gifHeight = 200;
    int lastListCount = 0;
    StaggeredGridView list;

    @Override
    protected void onStart(){
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop(){
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

//    @Override
//    public void onBackPressed() {
//        moveTaskToBack(true);
//    }

    StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            isEnd = false;
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    list.onRefreshComplete();
//                }
//            });
            String return_msg = result;
            Log.e("return_data2", return_msg);
            MinUtils.d("return_data2", return_msg);
            try {
                JSONObject jd = new JSONObject(return_msg);
                JSONArray jarr = new JSONArray(jd.getString("data"));
                TimelineResult tr = new TimelineResult();
                TimelineItem ti;
                JSONObject j;
                TimelineLastCommentItem tlci;
                JSONObject jj;
                for (int i = 0; i < jarr.length(); i++) {
                    ti = new TimelineItem();

                    j = jarr.getJSONObject(i);

                    ti.setBody(j.getString("body"));
                    ti.setId(j.getString("id"));
                    ti.setComCount(j.getInt("com_cnt"));
                    if (j.getString("like_yn").equals("Y")) {
                        ti.setIsLike(true);
                    } else if (j.getString("like_yn").equals("N")) {
                        ti.setIsLike(false);
                    } else {
                        ti.setIsLike(false);
                    }
                    ti.setLikeCount(j.getInt("like_cnt"));
                    if (!j.getString("and_tag_id").equals("")) {
                        ti.setIsAndTag(true);
                        ti.setAndTagId(j.getString("and_tag_id"));
                        ti.setAndTagName(j.getString("and_tag"));
                        ti.setAndTagBody(j.getString("and_tag_body"));
                    } else {
                        ti.setIsAndTag(false);
                        ti.setAndTagId("");
                        ti.setAndTagName("");
                        ti.setAndTagBody("");
                    }
                    ti.setOwnerId(j.getString("email"));
                    ti.setPlayCnt(j.getInt("play_cnt"));
                    ti.setPro_url(j.getString("profile_picture"));
                    ti.setPsot_id(j.getString("post_id"));
                    ti.setUrl(j.getString("url"));
                    ti.setTime(j.getString("date"));

                    tlci = new TimelineLastCommentItem();

                    jj = new JSONObject(j.getString("last_com"));

                    if (jj.getString("id").equals("null")) {
                        tlci.setIsExist(false);
                    } else {
                        tlci.setIsExist(true);
                        tlci.setId(jj.getString("id"));
                        tlci.setPro_url(jj.getString("profile_picture"));
                        tlci.setBody(jj.getString("body"));
                        tlci.setCom_id(jj.getString("com_id"));
                        tlci.setEmail(jj.getString("email"));
                    }

                    ti.setTlci(tlci);

                    tr.add(ti);
                }

//                isEnd = jarr.length() != 10;

                updateResultList(tr);

                isEnd = true;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
    private GestureDetector.OnGestureListener mNullListener = new GestureDetector.OnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_test2);

        AppController.t.setScreenName(getClass().getName());
        AppController.t.send(new HitBuilders.AppViewBuilder().build());


        //tooltipRelativeLayout_timeline

//        final ToolTipRelativeLayout toolTipRelativeLayout = (ToolTipRelativeLayout) findViewById(R.id.tooltipRelativeLayout_timeline);
//
//        Handler handler = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                ToolTip toolTip = new ToolTip()
////                        .withText("gif만들어볼래?")
////                        .withTextColor(Color.BLACK)
////                .withContentView(tv)
////                .withColor(Color.RED)
//                        .withContentView(LayoutInflater.from(TimeLineActivity.this).inflate(R.layout.customtooltip, null))
//                        .withAnimationType(ToolTip.AnimationType.FROM_MASTER_VIEW)
//                        .withColor(0xffffe766);
////                        .withShadow();
//                ToolTipView ttv = toolTipRelativeLayout.showToolTipForView(toolTip, findViewById(R.id.lay_timeline_camera));
//                ttv.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
//                    @Override
//                    public void onToolTipViewClicked(ToolTipView toolTipView) {
////                Toast.makeText(getApplicationContext(), "ㅋㅋㅋ", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        };
//
//        handler.sendEmptyMessageDelayed(0, 1000);

//        startActivity(new Intent(Test2Activity.this, NewTimeLineActivity.class));

        int tuNavi = AppController.getSp().getInt("tutime", -1);

        if (tuNavi > 0) {
            tuNavi = tuNavi - 1;
            AppController.getSe().putInt("tutime", tuNavi);
            AppController.getSe().commit();

            startActivity(new Intent(Test2Activity.this, TutorialCamera4.class));
        } else {

        }

//        imvHome = (LinearLayout) findViewById(R.id.imv_main_timeline_home);
//        imvAlarm = (LinearLayout) findViewById(R.id.imv_main_timeline_top_alarm);
//        imvChatting = (ImageView) findViewById(R.id.imv_main_timeline_top_chatting);
//        btnIntro = (Button) findViewById(R.id.btn_timeline_intro);
//
//        laySearch = (LinearLayout) findViewById(R.id.imv_timeline_search);
//        laySearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                overridePendingTransition(0, 0);
//                Intent ii = new Intent(Test2Activity.this, InterestSearchActivity.class);
//                ii.putExtra("navi", 0);
//                startActivity(ii);
//            }
//        });
//        layTimeline = (LinearLayout) findViewById(R.id.lay_timeline_timeline);
//        layInterest = (LinearLayout) findViewById(R.id.lay_timeline_interest);
//        layCamera = (LinearLayout) findViewById(R.id.lay_timeline_camera);
//        layAndTag = (LinearLayout) findViewById(R.id.lay_timeline_noti);
//        layMypage = (LinearLayout) findViewById(R.id.lay_timeline_mypage);
//
////        fal = (FloatingActionLayout) findViewById(R.id.fal_timeline);
//
//        imvHome.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                list.getRefreshableView().smoothScrollToPosition(0);
//            }
//        });
//        imvAlarm.setOnClickListener(this);
//        imvChatting.setOnClickListener(this);
//        btnIntro.setOnClickListener(this);
//
//        layTimeline.setOnClickListener(this);
//        layInterest.setOnClickListener(this);
//        layCamera.setOnClickListener(this);
//        layAndTag.setOnClickListener(this);
//        layMypage.setOnClickListener(this);
//
//        layTimeline.setSelected(true);
//
//        layIntro = (LinearLayout) findViewById(R.id.lay_timeline_intro);
//        layIntro.setVisibility(View.GONE);
//
//        list = (PullToRefreshListView) findViewById(R.id.list_pager_timeline);
//        list.setVisibility(View.VISIBLE);
//        list.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
//            @Override
//            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//                getData();
//            }
//        });

        getData();
        list = (StaggeredGridView)findViewById(R.id.grid_view);
//        layTimeline.setSelected(true);
    }


    private void getData() {

        m_ListAdapter = null;
        m_ResultList = null;

        StringTransMethod stmDataResult = new StringTransMethod() {
            @Override
            public void endTrans(final String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    MinUtils.d("timelinedata", result);
                    if (jd.isNull("data")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                list.setVisibility(View.GONE);
//                                layIntro.setVisibility(View.VISIBLE);
                            }
                        });

                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                list.setVisibility(View.VISIBLE);
//                                layIntro.setVisibility(View.GONE);
                            }
                        });

                        JSONArray jarr = new JSONArray(jd.getString("data"));
                        TimelineResult tr = new TimelineResult();
                        TimelineItem ti;
                        JSONObject j;
                        TimelineLastCommentItem tlci;
                        JSONObject jj;
                        for (int i = 0; i < jarr.length(); i++) {
                            ti = new TimelineItem();

                            j = jarr.getJSONObject(i);

                            ti.setBody(j.getString("body"));
                            ti.setId(j.getString("id"));
                            ti.setComCount(j.getInt("com_cnt"));
                            if (j.getString("like_yn").equals("Y")) {
                                ti.setIsLike(true);
                            } else if (j.getString("like_yn").equals("N")) {
                                ti.setIsLike(false);
                            } else {
                                ti.setIsLike(false);
                            }
                            ti.setLikeCount(j.getInt("like_cnt"));
                            if (!j.getString("and_tag_id").equals("")) {
                                ti.setIsAndTag(true);
                                ti.setAndTagId(j.getString("and_tag_id"));
                                ti.setAndTagName(j.getString("and_tag"));
                                ti.setAndTagBody(j.getString("and_tag_body"));
                            } else {
                                ti.setIsAndTag(false);
                                ti.setAndTagId("");
                                ti.setAndTagName("");
                                ti.setAndTagBody("");
                            }
                            ti.setOwnerId(j.getString("email"));
                            ti.setPlayCnt(j.getInt("play_cnt"));
                            ti.setPro_url(j.getString("profile_picture"));
                            ti.setPsot_id(j.getString("post_id"));
                            ti.setUrl(j.getString("url"));
                            ti.setTime(j.getString("date"));

                            tlci = new TimelineLastCommentItem();

                            jj = new JSONObject(j.getString("last_com"));

                            if (jj.getString("id").equals("null")) {
                                tlci.setIsExist(false);
                            } else {
                                tlci.setIsExist(true);
                                tlci.setId(jj.getString("id"));
                                tlci.setPro_url(jj.getString("profile_picture"));
                                tlci.setBody(jj.getString("body"));
                                tlci.setCom_id(jj.getString("com_id"));
                                tlci.setEmail(jj.getString("email"));
                            }

                            ti.setTlci(tlci);

                            tr.add(ti);
                        }

                        isEnd = jarr.length() != 10;
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                list.onRefreshComplete();
//                            }
//                        });

                        updateResultList(tr);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("page", "1");

        AppController.apiTaskNew = new APITaskNew(this, params, 508, stmDataResult);
        AppController.apiTaskNew.execute();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SHARE_RETURN:
                if (resultCode == RESULT_OK) {
                    int retultMSG = data.getExtras().getInt("result");
                    String postId = data.getExtras().getString("postId");
                    String result = "";

                    switch (retultMSG) {
                        case 0:
                            StringTransMethod stmRepicResult = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject j = new JSONObject(result);

                                        if (j.getInt("result") == 0) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(Test2Activity.this, getResources().getString(R.string.timeline_repic_ok), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("post_id", postId);


                            AppController.apiDataTaskNew = new APIDataTaskNew(this, params, 205, stmRepicResult);
                            AppController.apiDataTaskNew.execute();

                            break;
                    }

                }
                break;
            case POST_WRITE_RETURN:
                getData();
                break;
            case MORE_RETURN:
                if (resultCode == RESULT_OK) {
                    int type = data.getExtras().getInt("type");
                    int select = data.getExtras().getInt("select");
                    String pc = data.getExtras().getString("pc");
                    String postId = data.getExtras().getString("postId");

                    if (type == Popup2Activity.POPUP2_TYPE_2) {
                        switch (select) {
                            case 1:

                                break;
                            case 2:
                                //TODO 차단완료
                                getData();
                                break;
                            case 3:
                                Intent i = new Intent(Test2Activity.this, ReportActivity.class);
                                i.putExtra("postId", postId);
                                i.putExtra("pc", pc);
                                startActivity(i);
                                break;
                        }
                    } else if (type == Popup2Activity.POPUP2_TYPE_1) {
                        switch (select) {
                            case 1:
                            case 2:
                                //TODO 수정완료
                                //TODO 삭제완료
                                getData();
                                break;
                            case 3:

                                break;
                        }
                    }

                }
                break;
            case FOLLOW_RETURN:
                getData();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_main_timeline_home:
                //TODO refresh ListView
//                list.getRefreshableView().smoothScrollToPosition(0);
                break;
            case R.id.imv_main_timeline_top_alarm:
                startActivity(new Intent(Test2Activity.this, NotificationActivity.class));
                break;
            case R.id.imv_main_timeline_top_chatting:
//                Toast.makeText(this, "채팅 준비중", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_timeline_intro:
                Intent i = new Intent(Test2Activity.this, FriendResultActivity.class);
                i.putExtra("navi", 3);
                startActivityForResult(i, FOLLOW_RETURN);
                break;
            case R.id.lay_timeline_timeline:
                break;
            case R.id.lay_timeline_interest:
//                i = new Intent(Test2Activity.this, InterestFeedActivity.class);
                i = new Intent(Test2Activity.this, InterestFeedV1Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_camera:
                Intent gifCamera = new Intent(Test2Activity.this, GIFCameraActivity.class);
//                gifCamera.putExtra("navi", 0);
                gifCamera.putExtra("navi", 2);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivityForResult(gifCamera, POST_WRITE_RETURN);
                break;
            case R.id.lay_timeline_noti:
                i = new Intent(Test2Activity.this, NotificationActivity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
            case R.id.lay_timeline_mypage:
                i = new Intent(Test2Activity.this, MyPageV2Activity.class);
                overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
                startActivity(i);
                finish();
                break;
        }
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    protected boolean updateResultList(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
                m_ListAdapter.setResultList(m_ResultList);
            }
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    m_ListAdapter.notifyDataSetChanged();
//                }
//            });
            return true;
        }
        m_ResultList = resultList;

//        m_ListAdapter = new ArrayAdapter<TimelineItem>(this, 0, m_ResultList) {

        m_ListAdapter = new NewTimeLineAdapter(this, 0, m_ResultList);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO 터치했을때 해동
                    }
                });

            }
        });

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                list.onRefreshComplete();
//            }
//        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = (count / 10) + 1;

                    if (lastListCount != count) {
                        Log.e("list", count + "/" + page);

                        if (count % 10 == 0) {
                            if (!isLoading) {
                                isLoading = true;
                                Log.e("list", "OK");
                                Map<String, String> params = new HashMap<>();

                                params.put("my_id", AppController.getSp().getString("email", ""));
                                params.put("page", page + "");

                                AppController.apiTaskNew = new APITaskNew(Test2Activity.this, params, 508, stm);
                                AppController.apiTaskNew.execute();

                                isLoading = false;
                                lastListCount = count;
                            }
                        }
                    }

                }

                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
//                        AppController.imageExe.

                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
//                        AppController.thumbCheckTask.cancel(false);
//                        AppController.gifCheckTask.cancel(false);
//                        if (!AppController.gifExe.isTerminated()) {
//                            AppController.gifExe.shutdownNow();
//                        }
//                        if (!AppController.thumbExe.isTerminated()) {
//                            AppController.thumbExe.shutdownNow();
//                        }
//                        AppController.thumbExe = Executors.newFixedThreadPool(5);
//                        AppController.gifExe = Executors.newFixedThreadPool(1);
//                        break;
//                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
//                        if (!AppController.gifExe.isTerminated()) {
//                            AppController.gifExe.shutdownNow();
//                        }
//                        if (!AppController.thumbExe.isTerminated()) {
//                            AppController.thumbExe.shutdownNow();
//                        }
//                        AppController.thumbExe = Executors.newFixedThreadPool(5);
//                        AppController.gifExe = Executors.newFixedThreadPool(1);
//                        AppController.thumbCheckTask.cancel(false);
//                        AppController.gifCheckTask.cancel(false);
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);


//                        && (firstVisibleItem + visibleItemCount == totalItemCount);

//                for (int i = 0; i < m_ResultList.size(); i++) {
//                    m_ResultList.get(i).getThread().interrupt();
//                }
//
//                for (int i = firstVisibleItem; i < firstVisibleItem + 1; i++) {
//
//                }
//                TimelineResult tempResult = m_ListAdapter.getResultList();
//                for(int i=0;i<tempResult.size(); i++){
//                    tempResult.get(i).setIsLoading(false);
//                }
//                tempResult.get(firstVisibleItem).setIsLoading(true);
//                m_ListAdapter.setResultList(tempResult);
//                m_ListAdapter.notifyDataSetChanged();
            }
        });

        return true;
    }

}
