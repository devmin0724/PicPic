package com.picpic.sikkle;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RemoteViews;

import com.picpic.sikkle.gifcamera.GIFCameraFilmActivity;
import com.picpic.sikkle.ui.InitActivity;

public class CameraWidget extends AppWidgetProvider {

    final static String ACTION_AD_UPDATE = "AdChange";
    final static String ACTION_LEFT = "adfactory.devmin.golden_time_widget.action.CLICK.left";
    final static String ACTION_RIGHT = "adfactory.devmin.golden_time_widget.action.CLICK.right";
    final static String ACTION_CLICK = "com.picpic.sikkle.action.CLICK";
    final static String TAG = "PicPic Widget";

    static Bitmap bit;
    int arrowNavi = 0;

    public static AppWidgetManager _appWidgetManager;
    public static int[] _appWidgetIds;

    static int bgNavi = 0;
    static boolean leftNavi = false;
    static boolean rightNavi = false;
    static LayoutInflater m_LayoutInflater = null;
    static View convertView;
    AppWidgetManager manager;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        _appWidgetManager = appWidgetManager;
        _appWidgetIds = appWidgetIds;

        Log.d(TAG, "onUpdate, length = " + appWidgetIds.length + ", id = "
                + appWidgetIds[0]);

        ConnectivityManager cManager;
        NetworkInfo mobile;
        NetworkInfo wifi;

        cManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        mobile = cManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        for (int i = 0; i < _appWidgetIds.length; i++) {
            inspectUpdateAd(context, _appWidgetManager, _appWidgetIds[i]);
        }

//		if (mobile.isConnected() || wifi.isConnected()) {
//			new Inspect(context).execute();
//		} else {
//			for (int i = 0; i < _appWidgetIds.length; i++) {
//				NonConnectUpdateAd(context, _appWidgetManager, _appWidgetIds[i]);
//			}
//		}

    }

    static void UpdateAd(Context context, AppWidgetManager appWidgetManager,
                         int widgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);

        // 금액 업데이트

//        views.setImageViewResource(R.id.widget_camera, R.mipmap.widget_camera);
        Intent i = new Intent(context, InitActivity.class);
//        i.setAction(Intent.ACTION_)
//        i.addCategory(Intent.CATEGORY_)
//        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra("navi", 1);
        PendingIntent pending = PendingIntent.getActivity(context, widgetId, i,
                PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.widget_camera, pending);

        appWidgetManager.updateAppWidget(widgetId, views);

    }

    static void inspectUpdateAd(Context context,
                                AppWidgetManager appWidgetManager, int widgetId) {
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);

        // 금액 업데이트

//        views.setImageViewResource(R.id.widget_camera, R.mipmap.widget_camera);
        Intent i = new Intent(context, InitActivity.class);
//        i.setAction(Intent.ACTION_)
//        i.addCategory(Intent.CATEGORY_)
//        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra("navi", 1);
        PendingIntent pending = PendingIntent.getActivity(context, widgetId, i,
                PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.widget_camera, pending);

        appWidgetManager.updateAppWidget(widgetId, views);

    }

    @Override
    public void onReceive(Context context, Intent intent) {

//		Log.d("widget pre", sp.getInt("pTime", 0) + "");

        String action = intent.getAction();
        if (action != null && action.equals(ACTION_AD_UPDATE)) {
            int id = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            UpdateAd(context, AppWidgetManager.getInstance(context), id);
            Log.d(TAG, "onReceive(CHANE), id = " + id);
            return;
        }
        manager = AppWidgetManager.getInstance(context);
//		if (action.equals(ACTION_LEFT)) {
//			// views.seti
//			Log.e(TAG, "left ok");
//			arrowNavi = 1;
//			new Inspect(context).execute();
//
//		} else if (action.equals(ACTION_RIGHT)) {
//			Log.e(TAG, "right ok");
//			arrowNavi = 2;
//			new Inspect(context).execute();
//		}

        super.onReceive(context, intent);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {
            Log.d(TAG, "onDelete, id = " + appWidgetIds[i]);
        }
    }

    @Override
    public void onEnabled(Context context) {
        Log.d(TAG, "onEnabled");
    }

    @Override
    public void onDisabled(Context context) {
        Log.d(TAG, "onDisabled");
    }

}
