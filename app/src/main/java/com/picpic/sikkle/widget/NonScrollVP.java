package com.picpic.sikkle.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class NonScrollVP extends ViewPager {

    private boolean mIsEnabled;

    public NonScrollVP(Context context) {
        super(context);
        mIsEnabled = true;
    }

    public NonScrollVP(Context context, AttributeSet attrs) {
        super(context, attrs);
        mIsEnabled = true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mIsEnabled) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mIsEnabled) {
            return super.onTouchEvent(ev);
        } else {
            return false;
        }
    }

    public void setPagingEnabled(boolean enabled) {
        this.mIsEnabled = enabled;
    }
}
