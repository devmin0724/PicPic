package com.picpic.sikkle.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Jong-min on 2015-08-27.
 */
public class ReviewImageView extends ImageView {
    private int maxWidth;
    private int maxHeight;

    public ReviewImageView(Context context) {
        super(context);
    }

    public ReviewImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.maxWidth, android.R.attr.maxHeight});
        maxWidth = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        maxHeight = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        ta.recycle();
    }

    public ReviewImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        if (maxWidth >= 0) width = Math.min(maxWidth, width);
        Drawable d = getDrawable();
        int height;
        if (d == null) {
            height = getMeasuredHeight();
        } else {
            int min = d.getMinimumWidth();
            height = min > 0 ? width * d.getMinimumHeight() / min : 0;
        }
        if (maxHeight >= 0) height = Math.min(maxHeight, height);
        setMeasuredDimension(width, height);
    }
}
