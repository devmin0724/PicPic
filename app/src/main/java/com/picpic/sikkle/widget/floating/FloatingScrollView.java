package com.picpic.sikkle.widget.floating;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

public class FloatingScrollView extends ScrollView {
    Handler m_hd = null;
    Rect m_rect;
    private OnScrollChangedListener mOnScrollChangedListener;

    public FloatingScrollView(Context mContext) {
        super(mContext);
    }

    public FloatingScrollView(Context mContext, AttributeSet attrs) {
        super(mContext, attrs);
    }

    public FloatingScrollView(Context mContext, AttributeSet attrs, int defStyle) {
        super(mContext, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);

        if (mOnScrollChangedListener != null)
            mOnScrollChangedListener.onScrollChanged(this, x, y, oldx, oldy);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener mListener) {
        mOnScrollChangedListener = mListener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
// TODO Auto-generated method stub
        super.onDraw(canvas);
        checkIsLocatedAtFooter(); /// 여기서 그리기 끝나면 함수 콜

    }

    private void checkIsLocatedAtFooter() {
        if (m_rect == null)     /// 처음에는 Rect가 없을테니....
        {
            m_rect = new Rect();    /// new합니다.
            getLocalVisibleRect(m_rect);  /// 스크롤 영역 구합니다.(저는 0,480,0,696 이던가 했네요)
            return;       /// 그리고 걍 리턴합니다.
        }
        int oldBottom = m_rect.bottom;   /// 이전 bottom저장 이유는 맨아래인 상태에서 아래로 스크롤 했을떄 쌩까려구요

        getLocalVisibleRect(m_rect);   /// 현재 스크롤뷰의 영역을 구합니다.

        int height = getMeasuredHeight();  /// 스크롤 뷰의 높이를 구합니다.

        View v = getChildAt(0);    /// 스크롤 뷰 안에 들어있는 내용물의 높이도 구합니다.

        if (oldBottom > 0 && height > 0)   /// 스크롤 뷰나 이전 bottom이 0 이상이어야만 처리
        {
            if (oldBottom != m_rect.bottom && m_rect.bottom == v.getMeasuredHeight()) {
                Log.d("ghlab", "끝에 왔을 때의 처리");

                if (m_hd != null) {
                    m_hd.sendEmptyMessage(1);
                }
            }
        } else {
            if (m_hd != null) {
                m_hd.sendEmptyMessage(0);
            }
        }
    }

    public void setHandler(Handler hd) {
        m_hd = hd;
    }

    public interface OnScrollChangedListener {
        void onScrollChanged(ScrollView mScrollView, int x, int y, int oldx,
                             int oldy);
    }

}