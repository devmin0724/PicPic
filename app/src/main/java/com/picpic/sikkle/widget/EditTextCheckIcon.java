package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.picpic.sikkle.R;

/**
 * Created by Jong-min on 2015-07-21.
 */
public class EditTextCheckIcon extends ImageView {

    public static final int CHOICE_NON = 0;
    public static final int CHOICE_X = 1;
    public static final int CHOICE_WRONG = 2;

    public static int stateInt = 0;

    public EditTextCheckIcon(Context context) {
        super(context);
        setState(CHOICE_NON);
    }

    public EditTextCheckIcon(Context context, AttributeSet attrs) {
        super(context, attrs);
        setState(CHOICE_NON);
    }

    public EditTextCheckIcon(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setState(CHOICE_NON);
    }

    public int getState() {
        return stateInt;
    }

    public void setState(int state) {
        stateInt = state;
        switch (state) {
            case CHOICE_NON:
                setImageResource(R.drawable.btn_login);
                break;
            case CHOICE_WRONG:
                setImageResource(R.drawable.icon_plus);
                break;
            case CHOICE_X:
                setImageResource(R.drawable.icon_plus);
                break;
        }
    }

}
