package com.picpic.sikkle.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;
import com.picpic.sikkle.utils.MinUtils;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-09.
 */
public class CameraProgressBar extends LinearLayout {

    public static ArrayList<ImageView> progresses, margins;
    public static ImageView padding;
    int screenWidth = MinUtils.screenWidth;
    int colorNow = 0xff6a79ba, colorBebore = 0xff13827b;

    LinearLayout lay;

    float nowProgress, paddingProgress;
    ImageView progress;

    public CameraProgressBar(Context context) {
        super(context);
        init();
    }

    public CameraProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CameraProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        progresses = new ArrayList<>();
        margins = new ArrayList<>();

        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.view_camera_progressbar, this, true);

        padding = new ImageView(getContext());

        lay = (LinearLayout) findViewById(R.id.lay_view_camera_progress);

        padding.setBackgroundColor(0x00000000);

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        padding.setLayoutParams(params);

        lay.addView(padding);


//        screenWidth


    }

    public void setProgressesBar(Activity ac, int navi, final int pro) {
        progress = new ImageView(getContext());
        switch (navi) {
            case 0:
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lay.removeAllViews();
                        progress.setBackgroundColor(colorNow);

                        nowProgress = pro / 8000;

                        paddingProgress = 1 - nowProgress;

                        LayoutParams params1 = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, nowProgress);
                        LayoutParams params2 = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, paddingProgress);

                        padding.setLayoutParams(params2);
                        progress.setLayoutParams(params1);

                        progresses.add(progress);

                        lay.addView(progress);
                        lay.addView(padding);
                    }
                });
                break;
            default:
                ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lay.removeAllViews();

                        progress.setBackgroundColor(colorNow);

                        nowProgress = pro / 8000;

                        paddingProgress = 1 - nowProgress;
                    }
                });

                break;
        }


    }
}
