package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-07-16.
 */
public class GifCameraProgressBar extends LinearLayout {

    public static final int MAX_VALUE = 11000;
    public static int nowNum = -1;
    public static int now = 0;

    DisplayMetrics metrics;

    LinearLayout tempLay;

    int screenWidth = 0;
    int screenHeight = 0;

    ArrayList<LinearLayout> lays;

    LinearLayout layProgress;

    public GifCameraProgressBar(Context context) {
        super(context);
        init();
    }

    public GifCameraProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public GifCameraProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.widget_gif_camera_progress_bar, this, true);

        metrics = getContext().getApplicationContext().getResources().getDisplayMetrics();

        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;

        lays = new ArrayList<>();

        layProgress = (LinearLayout) findViewById(R.id.lay_widget_gif_camera_progress_bar);
    }

    public void setProgress(int num, int progress, boolean isEnd) {
        if (nowNum < num) {
            if (progress > now) {
                addView();
            }
        } else if (num == nowNum) {
            if (isEnd) {
                endProgress();
            } else {
                addProgress(progress);

            }
        }
    }

    private void addView() {
        nowNum = nowNum + 1;

        tempLay = new LinearLayout(getContext());

        LayoutParams params = new LayoutParams(0, 18);

        tempLay.setLayoutParams(params);

        tempLay.setBackgroundColor(0xff697fff);

        lays.add(tempLay);

        Thread thread = new Thread() {
            @Override
            public void run() {
                addView(tempLay);
            }
        };

    }

    private void addProgress(int pro) {
        float nowPro = pro / MAX_VALUE;

        int nowProgress = (int) (screenWidth * nowPro);

        LayoutParams params = (LayoutParams) lays.get(nowNum).getLayoutParams();

        params.width = nowProgress;

        lays.get(nowNum).setLayoutParams(params);
    }

    private void endProgress() {
        LayoutParams par = (LayoutParams) lays.get(nowNum).getLayoutParams();
        par.setMargins(0, 0, 2, 0);
        lays.get(nowNum).setLayoutParams(par);
        lays.get(nowNum).setBackgroundColor(0xff127f78);
    }

}
