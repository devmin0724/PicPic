
package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class GifMovieView extends View {

    private static final String TAG = GifMovieView.class.getSimpleName();
    private Movie mMovie;
    private long mMoviestart;

    public GifMovieView(Context context, InputStream stream) {
        super(context);

        //mMovie = Movie.decodeStream(stream);
        int streamSize = 0;

        try {
            streamSize = stream.available();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        byte[] array = streamToBytes(stream, streamSize);

        int duration = 0;

        if (array != null) {
            Movie mMovieTemp;
            InputStream byteInputStream = new ByteArrayInputStream(array);
            byteInputStream.mark(0);
            mMovieTemp = Movie.decodeStream(byteInputStream);

            array = null;
            if (mMovieTemp != null && (mMovieTemp.duration() > 0)) {
                Log.d(TAG, "Decode Agif  Success");
                duration = mMovieTemp.duration();
            }

            mMovie = mMovieTemp;

        } else {
            Log.e(TAG, "AGIF stream is null");
        }
    }

    public static byte[] streamToBytes(InputStream is, int streamSize) {
        byte[] buffer = new byte[streamSize];
        int readsize = -1;
        try {
            readsize = is.read(buffer, 0, streamSize);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        if (readsize != streamSize)
            buffer = null;
        return buffer;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
        super.onDraw(canvas);
        final long now = SystemClock.uptimeMillis();

        if (mMoviestart == 0) {
            mMoviestart = now;
        }

        final int relTime = (int) ((now - mMoviestart) % mMovie.duration());
        mMovie.setTime(relTime);
        mMovie.draw(canvas, 10, 10);
        this.invalidate();
    }
}
