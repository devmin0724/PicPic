package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.picpic.sikkle.R;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-11-10.
 */
public class DrawView extends ImageView implements View.OnTouchListener {

    private Canvas mCanvas;
    private Path mPath;
    private Paint mPaint;
    public static ArrayList<Path> paths = new ArrayList<Path>();
    public static ArrayList<Path> undonePaths = new ArrayList<Path>();
    private Bitmap mBitmap;
    int width = 60;
//    int startX = 0;
//    int startY = 0;
//    int endX = 0;
//    int endY = 0;

    public DrawView(Context c) {
        super(c);
        init();
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Bitmap getmBitmap() {
        return mBitmap;
    }

//    public int getXX() {
//        return startX;
//    }
//
//    public int getYY() {
//        return startY;
//    }

//    public void setmBitmap(Bitmap b, int x, int y) {
    public void setmBitmap(Bitmap b) {
        mBitmap = b;
//        if (b.getWidth() % 2 == 1) {
//            startX = x - 1;
//        } else {
//            startX = x;
//        }
//
//        if (b.getHeight() % 2 == 1) {
//            startY = y - 1;
//        } else {
//            startY = y;
//        }
//        endX = mBitmap.getWidth() + startX;
//        endY = mBitmap.getHeight() + startY;
        setImageBitmap(mBitmap);
    }

    public void setWidth(int width) {
        this.width = width;
        mPaint.setStrokeWidth(width);
        invalidate();
    }

    public ArrayList<Path> getPaths() {
        return paths;
    }

    public void setPaths(ArrayList<Path> paths) {
        this.paths = paths;
        transparent();
    }

    public void reset(Bitmap b) {
//        this.mBitmap = b;
        setmBitmap(b);

        paths = new ArrayList<>();
        undonePaths = new ArrayList<>();

        mPaint = new Paint();
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        mPaint.setAlpha(0);
        mPaint.setStrokeWidth(width);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mCanvas = new Canvas();
        mPath = new Path();
        paths.add(mPath);

        setOnTouchListener(this);
        invalidate();

    }

    public void init() {
        setScaleType(ScaleType.CENTER_CROP);
//        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_filter);

        paths = new ArrayList<>();
        undonePaths = new ArrayList<>();

        mPaint = new Paint();
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        mPaint.setAlpha(0);
        mPaint.setStrokeWidth(width);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mCanvas = new Canvas();
        mPath = new Path();
        paths.add(mPath);

        setOnTouchListener(this);
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        canvas.clip
        canvas.drawBitmap(mBitmap, null, new Rect(0, 0, mBitmap.getWidth(), mBitmap.getHeight()), null);
//        canvas.drawBitmap(mBitmap, 0, 0, new Paint());
//        canvas.drawBitmap(filter, null, new Rect(0, 0, getWidth(), getHeight()), null);
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        transparent();
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {
//        mPath.
        mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        transparent();
        // kill this so we don't double draw
        mPath = new Path();
        paths.add(mPath);
    }

    public void onClickUndo(Bitmap b) {
        Log.e("paths size", paths.size() + "");
        if (paths.size() > 0) {

//            this.mBitmap = b;
            setmBitmap(b);

            mPaint = new Paint();
            mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            mPaint.setAlpha(0);
            mPaint.setStrokeWidth(width);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mCanvas = new Canvas();

            undonePaths.add(paths.remove(paths.size() - 1));
            transparent();
        } else {

        }
        //toast the user
    }

    public void onClickRedo() {
        if (undonePaths.size() > 0) {
            paths.add(undonePaths.remove(undonePaths.size() - 1));
            transparent();
        } else {

        }
        //toast the user
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }

    public void transparent() {
        Bitmap tmp = Bitmap.createBitmap(mBitmap.getWidth(), mBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(tmp);
//        Bitmap tmp2 = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas c2 = new Canvas(tmp2);
        c.drawBitmap(mBitmap, null, new RectF(0, 0, mBitmap.getWidth(), mBitmap.getHeight()), null);
//        c.drawBitmap(mBitmap, 0, 0, new Paint());
//        c2.drawBitmap(filter, null, new RectF(0, 0, getWidth(), getHeight()), null);
//        c.drawColor(0xbb000000);
//        path.setFillType(Path.FillType.WINDING);
//        path.addOval((new RectF(x1, y1, x2, y2)), Path.Direction.CW);
//        path.add
//        Paint paint = new Paint();
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//        paint.setAlpha(0);
//        paint.setStrokeWidth(width);
//        c.drawLine(x1, y1, x2, y2, paint);
        for (Path p : paths) {
            c.drawPath(p, mPaint);
//            c2.drawPath(p, mPaint);
        }
//        c.drawCircle(x1, y1, width,paint);
//        c2.drawCircle(x1, y1, width,paint);
//        c.drawArc(x1, y1, x2, y2, 0f, width, true, paint);
//        c.drawArc(x1, y1, x2, y2, paint);
//        c.drawPath(path, paint);
        mBitmap = tmp;
//        filter = tmp2;
        invalidate();
    }

}