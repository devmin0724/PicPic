package com.picpic.sikkle.widget.loadingbutton;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
