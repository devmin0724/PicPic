package com.picpic.sikkle.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class BoundableOfflineImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 250;
    public OnImageChangedListener listener;

    private int maxWidth;
    private int maxHeight;
    private int width = 0;
    private String urlString;

    public BoundableOfflineImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.maxWidth, android.R.attr.maxHeight});
        maxWidth = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        maxHeight = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        ta.recycle();

    }

    @Override
    public void setImageBitmap(Bitmap bm) {

        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getResources().getColor(android.R.color.transparent)),
                new BitmapDrawable(getContext().getResources(), bm)});

        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = width==0 ? getMeasuredWidth() : width;
        if (maxWidth >= 0) width = Math.min(maxWidth, width);
        Drawable d = getDrawable();
        int height;
        if (d == null) {
            height = urlString == null ? getMeasuredHeight() : maxHeight;
//            height = 100;
        } else {
            int min = d.getMinimumWidth();
            height = min > 0 ? width * d.getMinimumHeight() / min : 0;
        }
        if (maxHeight >= 0) height = Math.min(maxHeight, height);
        setMeasuredDimension(width, height);
    }

    @Override
    public void setImageDrawable(Drawable d) {
        super.setImageDrawable(d);

        if (listener != null) {
            listener.onImageChanged(d);
        }

    }

    public void setWidth(int width){
        this.width = width;
//        onMeasure();
    }

    @Override
    public void setMaxWidth(int width) {
        maxWidth = width;
        super.setMaxWidth(width);
    }

    @Override
    public void setMaxHeight(int height) {
        maxHeight = height;
        super.setMaxHeight(height);
    }

    public void setOnImageChangedListener(OnImageChangedListener listener) {
        this.listener = listener;
    }

    public interface OnImageChangedListener {
        void onImageChanged(Drawable d);
    }

}
