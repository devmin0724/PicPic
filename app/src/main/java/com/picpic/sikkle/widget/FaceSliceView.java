package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;

import com.picpic.sikkle.R;

/**
 * Created by devmin-sikkle on 2015-12-09.
 */
public class FaceSliceView extends View {

    private Path mPath;
    private Paint mPaint;
    private Bitmap mBitmap;

    public FaceSliceView(Context context) {
        super(context);
        init(context);
    }

    public FaceSliceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        Bitmap obmp = BitmapFactory.decodeResource(getResources(), R.drawable.ab_background);
        Bitmap resultImg = Bitmap.createBitmap(obmp.getWidth(), obmp.getHeight(), Bitmap.Config.ARGB_8888);
        Bitmap maskImg = Bitmap.createBitmap(obmp.getWidth(), obmp.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas mCanvas = new Canvas(resultImg);
        Canvas maskCanvas = new Canvas(maskImg);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);;
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        Path path = new Path();
        path.moveTo(0,0);
        path.lineTo(10,10);
        path.lineTo(10,20 );
        path.lineTo(20,20);
        path.lineTo(20,10);
        path.lineTo(0,0);
        path.close();

        maskCanvas.drawPath(path, paint);
        mCanvas.drawBitmap(obmp, 0, 0, null);
        mCanvas.drawBitmap(maskImg, 0, 0, paint);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        int width = mBitmap.getWidth();
//        int height = mBitmap.getHeight();
//
//        canvas.drawBitmap(mBitmap, 0,0, null);
//
//        canvas.save();
//
////        canvas.clipPath(mPath);
//        canvas.clipPath(mPath, Region.Op.DIFFERENCE);
//
////        canvas.clipRect(0,0,width, height);
////
////        canvas.drawColor(Color.BLACK);
//
//        canvas.restore();

    }
}
