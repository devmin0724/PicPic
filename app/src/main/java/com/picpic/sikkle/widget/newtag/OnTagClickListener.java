package com.picpic.sikkle.widget.newtag;

/**
 * listener for tag delete
 */
public interface OnTagClickListener {
    void onTagClick(Tag tag, int position);
}