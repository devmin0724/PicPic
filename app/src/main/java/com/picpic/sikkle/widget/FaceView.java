package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;

import com.picpic.sikkle.utils.AppController;

/**
 * Created by devmin-sikkle on 2015-12-14.
 */
public class FaceView extends View{
    public FaceView(Context context) {
        super(context);
    }

    public FaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Bitmap obmp = BitmapFactory.decodeFile(AppController.FACE_SLICE_SRC);
        Bitmap maskImg = Bitmap.createBitmap(obmp.getWidth(), obmp.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas maskCanvas = new Canvas(maskImg);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);;
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        Path path = new Path();
        path.moveTo(100,100);
        path.lineTo(200,100);
        path.lineTo(200,200);
        path.lineTo(150,150);
        path.lineTo(100,200);
        path.close();

        maskCanvas.drawPath(path, paint);
        canvas.drawBitmap(obmp, 0, 0, null);
        canvas.drawBitmap(maskImg, 0, 0, paint);


    }
}
