package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.picpic.sikkle.utils.MinUtils;

/**
 * Created by Jong-min on 2015-08-22.
 */
public class SquareLayout extends FrameLayout {
    public SquareLayout(Context context) {
        super(context);
    }

    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int a = MinUtils.screenWidth / 3;
        super.onMeasure(a, a);
    }
}
