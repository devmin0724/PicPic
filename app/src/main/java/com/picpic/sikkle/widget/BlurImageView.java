package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageLoader;

public class BlurImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 250;
    public OnImageChangedListener listener;
    private MinImageLoader minImageLoader = AppController.getmInstance().getMinImageLoader();

    public BlurImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(blur(getContext(), bm, 80));
    }

    public Bitmap blur(Context ct, Bitmap sentBitmap, int radius) {

        if (Build.VERSION.SDK_INT > 16) {
            Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

            final RenderScript rs = RenderScript.create(ct);
            final Allocation input = Allocation.createFromBitmap(rs, sentBitmap, Allocation.MipmapControl.MIPMAP_NONE,
                    Allocation.USAGE_SCRIPT);
            final Allocation output = Allocation.createTyped(rs, input.getType());
            final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(radius);
            script.setInput(input);
            script.forEach(output);
            output.copyTo(bitmap);
            return bitmap;
        }
        return sentBitmap;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setImageDrawable(Drawable d) {
        super.setImageDrawable(d);
        if (listener != null) {
            listener.onImageChanged(d);
        }
    }

    public void setImageURLString(String url) {
        setImageDrawable(null);
//        if (url != null) {
//            minImageLoader.DisplayImage(url, this);
//        }
    }

    public void setOnImageChangedListener(OnImageChangedListener listener) {
        this.listener = listener;
    }

    public interface OnImageChangedListener {
        void onImageChanged(Drawable d);
    }

}
