package com.picpic.sikkle.widget.newtag;

/**
 * listener for tag delete
 */
public interface OnTagDeleteListener {
    void onTagDeleted(Tag tag, int position);
}