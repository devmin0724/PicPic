package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinFixImageLoader;

public class FixedStopImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 250;
    public OnImageChangedListener listener;
    private MinFixImageLoader minImageLoader = AppController.getmInstance().getMinFixImageLoader();

    public FixedStopImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {

        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getResources().getColor(android.R.color.transparent)),
                new BitmapDrawable(getContext().getResources(), bm)});

        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setImageDrawable(Drawable d) {
        super.setImageDrawable(d);
        if (listener != null) {
            listener.onImageChanged(d);
        }
    }

    public void setImageURLString(String url) {
        setImageDrawable(null);
        if (url != null) {
            minImageLoader.DisplayImage(url, this);
        }
    }

    public void setOnImageChangedListener(OnImageChangedListener listener) {
        this.listener = listener;
    }

    public interface OnImageChangedListener {
        void onImageChanged(Drawable d);
    }

}
