/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.ui.popup.Popup2Activity;
import com.picpic.sikkle.ui.popup.SharePopUp;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.cache.ImageLoader;

/**
 * Created by devmin-sikkle on 2016-03-22.
 */
public class TimelineBottomComponent extends LinearLayout {

    CircleImageView cimv;
    TextView tvId;
    LinearLayout layMore;
    TextView tvTags;
    LinearLayout layLike;
    TextView tvLike;
    LinearLayout layComment;
    TextView tvComment;
    LinearLayout layPlayCount;
    TextView tvPlayCount;
    Button btnShare;
    ImageLoader imageLoader;

    public TimelineBottomComponent(Context context) {
        super(context);
        init(context);
    }

    public TimelineBottomComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TimelineBottomComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context con) {

        imageLoader = new ImageLoader(con);

        View v = inflate(con, R.layout.component_timeline_bottom, null);

        cimv = (CircleImageView) v.findViewById(R.id.cimv_recycler_row_timeline);

        tvId = (TextView) v.findViewById(R.id.tv_recycler_row_timeline_id);
        layMore = (LinearLayout) v.findViewById(R.id.imv_recycler_row_timeline_more);
        tvTags = (TextView) v.findViewById(R.id.tv_recycler_row_timeline_tags);

        layLike = (LinearLayout) v.findViewById(R.id.lay_recycler_row_timeline_like);
        tvLike = (TextView) v.findViewById(R.id.tv_recycler_row_timeline_like_count);

        layComment = (LinearLayout) v.findViewById(R.id.lay_recycler_row_timeline_comment);
        tvComment = (TextView) v.findViewById(R.id.tv_recycler_row_timeline_comment_count);

        layPlayCount = (LinearLayout) v.findViewById(R.id.lay_recycler_row_timeline_play_count);
        tvPlayCount = (TextView) v.findViewById(R.id.tv_recycler_row_timeline_play_count);

        btnShare = (Button) v.findViewById(R.id.btn_recycler_row_timeline_share);

    }

    public void setLikeCount(int a) {
        MinUtils.setCount(tvLike, a);
        invalidate();
    }

    public boolean isLike() {
        if (layLike.isSelected()) {
            return true;
        } else {
            return false;
        }
    }

    public void setLike(boolean is) {
        layLike.setSelected(is);
        invalidate();
    }

    public void setData(final TimelineItem model, final Activity ac, final LikeView lv) {
        cimv.setTag(model.getOwnerId());

        imageLoader.DisplayImage(AppController.URL + model.getPro_url(), cimv, false);

        tvId.setText(model.getId());

        MinUtils.setCount(tvLike, model.getLikeCount());
        MinUtils.setCount(tvComment, model.getComCount());
        MinUtils.setCount(tvPlayCount, model.getPlayCnt());

        MinUtils.setTags(ac, tvTags, model.getTags());

        if (model.isLike()) {
            layLike.setSelected(true);
        } else {
            layLike.setSelected(false);
        }

        layLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layLike.isSelected()) {
                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (model.isLike()) {
                                int lc = model.getLikeCount();
                                tvLike.setText(lc - 1 + "");
                            }
                        }
                    });
                    MinUtils.like(true, ac, model.getPsot_id());

                } else {

                    ac.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!model.isLike()) {
                                int lc = model.getLikeCount();
                                tvLike.setText(lc + 1 + "");
                            }
                        }
                    });
                    lv.startAni();
                    MinUtils.like(false, ac, model.getPsot_id());

                }
            }
        });

        layComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || layLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                ac.startActivity(i);
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SharePopUp.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("body", model.getBody());
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("w", 0);
                i.putExtra("h", 0);
                ac.startActivityForResult(i, TimeLineActivity.SHARE_RETURN);
            }
        });

        layMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, Popup2Activity.class);
                i.putExtra("email", model.getOwnerId());
                if (model.getOwnerId().equals(AppController.getSp().getString("email", ""))) {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_1);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                } else {
                    i.putExtra("pageNavi", Popup2Activity.POPUP2_TYPE_2);
                    i.putExtra("postId", model.getPsot_id());
                    i.putExtra("pc", "P");
                }
                i.putExtra("url", AppController.URL + model.getUrl());
                i.putExtra("title", model.getId());
                i.putExtra("body", model.getBody());
                ac.startActivityForResult(i, TimeLineActivity.MORE_RETURN);
            }
        });

        tvComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, SinglePostV1Activity.class);
                i.putExtra("postId", model.getPsot_id());
                i.putExtra("navi", 1);
                i.putExtra("likeCount", model.getLikeCount() + "");
                i.putExtra("comCount", model.getComCount() + "");
                if (model.isLike() || layLike.isSelected()) {
                    i.putExtra("isLike", true);
                } else {
                    i.putExtra("isLike", false);
                }
                ac.startActivity(i);
            }
        });
        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ac, UserListActivity.class);
                i.putExtra("pageNavi", UserListActivity.USER_LIST_LIKE);
                i.putExtra("postId", model.getPsot_id());
                ac.startActivity(i);
            }
        });

        cimv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });

        tvId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myId = AppController.getSp().getString("email", "");
                String tempEmail = cimv.getTag().toString();
                if (tempEmail.equals(myId)) {
                    ac.startActivity(new Intent(ac, MyPageV2Activity.class));
                    ac.finish();
                } else {
                    if (!tempEmail.equals("")) {
                        Intent i = new Intent(ac, UserFeedActivity.class);
                        i.putExtra("email", tempEmail);
                        ac.startActivity(i);
                    }
                }
            }
        });
    }
}
