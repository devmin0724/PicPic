package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-20.
 */
public class MinProgress extends View {

    public static ArrayList<LinearLayout> lays;
    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Canvas can;
    int process = 0;
    int colorBefore = getResources().getColor(R.color.min_progress_before);
    int colorNow = getResources().getColor(R.color.min_progress_now);

    public MinProgress(Context context) {
        super(context);
    }

    public MinProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MinProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void add(int progress) {
        this.process = progress;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(colorNow);
        paint.setAntiAlias(true);

        int[] loc = new int[2];

        getLocationInWindow(loc);

//        int realRight = loc[0] + getWidth();
//        int realBotoom = loc[1] + getHeight();

        Log.e("l / t / r / b", getLeft() + " / " + getTop() + " / " + getRight() + " / " + getBottom());

        Rect rect = new Rect(getLeft(), getTop(), process, getTop() + 10);
//        Rect rect = new Rect(100, 100, process, getBottom());

        canvas.drawRect(rect, paint);
    }
}
