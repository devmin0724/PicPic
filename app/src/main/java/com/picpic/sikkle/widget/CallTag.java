package com.picpic.sikkle.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV1Activity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.UserFeedForIdActivity;
import com.picpic.sikkle.utils.AppController;

/**
 * Created by Jong-min on 2015-08-12.
 */
public class CallTag extends ClickableSpan {
    Context context;
    TextPaint textPaint;

    public CallTag(Context ctx) {
        super();
        context = ctx;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
//        ds.setColor(ds.linkColor);
        ds.setColor(0xff484848);
        ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
//        ds.setARGB(255, 30, 144, 255);
    }

    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
        String theWord = s.subSequence(start + 1, end).toString();
        if (AppController.getSp().getString("id", "").equals(theWord)) {
            Intent i = new Intent(context, MyPageV2Activity.class);
            i.putExtra("id", theWord);
            context.startActivity(i);
        } else {
            Intent i = new Intent(context, UserFeedForIdActivity.class);
            i.putExtra("id", theWord);
            context.startActivity(i);
        }
//        Toast.makeText(context, String.format("Tags for tags : %s", theWord), Toast.LENGTH_SHORT).show();
    }
}
