package com.picpic.sikkle.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.picpic.sikkle.ui.setteings.TermActivity;

/**
 * Created by Jong-min on 2015-08-04.
 */
public class Term1Span extends ClickableSpan {
    Context context;
    TextPaint textPaint;

    public Term1Span(Context ctx) {
        super();
        context = ctx;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
        ds.setColor(ds.linkColor);
        ds.setColor(Color.BLACK);
        ds.setTextSize(40f);
        ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
//        ds.setARGB(255, 30, 144, 255);
    }

    @Override
    public void onClick(View widget) {
        Intent i = new Intent(context, TermActivity.class);
        i.putExtra("navi", 0);
        context.startActivity(i);
//        Toast.makeText(context, String.format("Tags for tags : %s", theWord), Toast.LENGTH_SHORT).show();
    }
}
