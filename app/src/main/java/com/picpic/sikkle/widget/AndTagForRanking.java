package com.picpic.sikkle.widget;

import android.content.Context;
import android.content.Intent;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.picpic.sikkle.ui.AndTagFeedActivity;

/**
 * Created by Jong-min on 2015-08-12.
 */
public class AndTagForRanking extends ClickableSpan {
    Context context;
    TextPaint textPaint;
    String tagId;

    public AndTagForRanking(Context ctx) {
        super();
        context = ctx;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
        ds.setColor(ds.linkColor);
        ds.setColor(0xffffffff);
//        ds.setARGB(255, 30, 144, 255);
    }

    public void setTagId(String id) {
        tagId = id;
    }

    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
        String theWord = s.subSequence(start + 1, end).toString();
        Intent i = new Intent(context, AndTagFeedActivity.class);
        i.putExtra("tag", tagId);
        context.startActivity(i);
//        Toast.makeText(context, String.format("Tags for tags : %s", theWord), Toast.LENGTH_SHORT).show();
    }
}
