/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;

public class HeaderLayout extends LinearLayout {
    private TextView mTextView;
//    private TabLayout mTabLayout;
    private LinearLayout layHeader;

    public HeaderLayout(Context context) {
        super(context);
    }

    public HeaderLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeaderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HeaderLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        layHeader = (LinearLayout)findViewById(R.id.lay_timeline_header_new);

//        mTextView = (TextView) findViewById(R.id.text_view);
//        mTabLayout = (TabLayout)findViewById(R.id.tab_layout);
    }

    public int getScrollRange() {
//        return getHeight() - mTextView.getHeight();
        return getHeight() - layHeader.getHeight();
    }
}
