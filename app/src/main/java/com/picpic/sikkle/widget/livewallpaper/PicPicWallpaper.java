package com.picpic.sikkle.widget.livewallpaper;

import android.graphics.Bitmap;
import android.service.wallpaper.WallpaperService;

import com.picpic.sikkle.utils.GIFAnimationThread;

import java.io.IOException;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by devmin on 2015-12-30.
 */
public class PicPicWallpaper extends WallpaperService{
    GIFAnimationThread thread;
    @Override
    public Engine onCreateEngine() {
        return new Wallpaper();
    }

    private class Wallpaper extends Engine{
        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            GifDrawable gd = null;
            ArrayList<Bitmap> arr = new ArrayList<>();
            try {
                gd = new GifDrawable(getResources().getAssets().open("loading_bg_4.gif"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            for(int i=0; i<gd.getNumberOfFrames(); i++){
                arr.add(gd.seekToFrameAndGet(i));
            }

            thread = new GIFAnimationThread(getSurfaceHolder());

            if(!thread.getRunning()){
                thread.interrupt();
                thread.setRunning(false);
                thread = new GIFAnimationThread(getSurfaceHolder());
            }
            thread.setAni(arr, gd.getFrameDuration(1));
            if(!thread.isAlive()){
                thread.start();
            }
        }
    }
}
