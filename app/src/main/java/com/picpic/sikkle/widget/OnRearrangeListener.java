package com.picpic.sikkle.widget;

public interface OnRearrangeListener {

    void onRearrange(int oldIndex, int newIndex);
}
