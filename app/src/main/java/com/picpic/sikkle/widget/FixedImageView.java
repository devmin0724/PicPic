package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinFixImageLoader;

import pl.droidsonroids.gif.GifDrawable;

public class FixedImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 250;
    public OnImageChangedListener listener;
    private MinFixImageLoader minImageLoader = AppController.getmInstance().getMinFixImageLoader();
    //    private MinImageLoader minImageLoader = AppController.getmInstance().getMinImageLoader();
    private String urlString;

    public FixedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {

        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getResources().getColor(android.R.color.transparent)),
                new BitmapDrawable(getContext().getResources(), bm)});

        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);
    }

    @Override
    public void setImageDrawable(Drawable d) {
        super.setImageDrawable(d);
        if (listener != null) {
            listener.onImageChanged(d);
        }
        if (d instanceof GifDrawable) {

        } else {
            if (urlString != null) {
                minImageLoader.DisplayImage(urlString, this);
            }
        }
    }

    public void setImageURLString(String url, String thumb) {
        urlString = url;

        if (thumb != null) {
            minImageLoader.DisplayImage(thumb, this);
        }
    }

    public void setOnImageChangedListener(OnImageChangedListener listener) {
        this.listener = listener;
    }

    public interface OnImageChangedListener {
        void onImageChanged(Drawable d);
    }

}
