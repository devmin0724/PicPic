package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.picpic.sikkle.beans.TagListItem;

/**
 * Created by Jong-min on 2015-08-17.
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView {
    public CustomAutoCompleteTextView(Context context) {
        super(context);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        TagListItem ti = (TagListItem) selectedItem;
        return ti.getTag_name() + " ";
//        return super.convertSelectionToString(selectedItem);
    }
}
