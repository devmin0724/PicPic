package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by Jong-min on 2015-11-06.
 */
public class NewTimelineScrollView extends ScrollView{
    protected OnEdgeTouchListener onEdgeTouchListener;
    public enum DIRECTION {
        TOP, BOTOM, NONE
    }

    public NewTimelineScrollView(Context context) {
        super(context);
    }

    public NewTimelineScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewTimelineScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void computeScroll() {

        super.computeScroll();

        if (onEdgeTouchListener != null) {

            if (getScrollY() == 0) {

                onEdgeTouchListener.onEdgeTouch(DIRECTION.TOP);

            } else if ((getScrollY() + getHeight()) == computeVerticalScrollRange()) {

                onEdgeTouchListener.onEdgeTouch(DIRECTION.BOTOM);

            } else {

                onEdgeTouchListener.onEdgeTouch(DIRECTION.NONE);

            }

        }

    }
    public void setOnEdgeTouchListener(OnEdgeTouchListener l) {

        this.onEdgeTouchListener = l;

    }
    public interface OnEdgeTouchListener {

        void onEdgeTouch(DIRECTION direction);

    }
}
