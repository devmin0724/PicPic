package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Jong-min on 2015-09-23.
 */
public class GifPartView extends View {

    private static final float MINP = 0.25f;
    private static final float MAXP = 0.75f;
    private static final float TOUCH_TOLERANCE = 4;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint;
    private Paint mPaint;
    private float mX, mY;

    public GifPartView(Context context) {
        super(context);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        mPaint.setXfermode(new PorterDuffXfermode(
                PorterDuff.Mode.CLEAR));
    }

    public GifPartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
        mPaint.setXfermode(new PorterDuffXfermode(
                PorterDuff.Mode.CLEAR));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
//        Bitmap.createBitmap
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(Color.TRANSPARENT);
    }

    public void setBitmap(Bitmap b) {
        mBitmap = b;

        mCanvas = new Canvas(mBitmap);
        mCanvas.drawColor(Color.TRANSPARENT);
        invalidate();
    }

    public Bitmap getCanvasToBitmap() {
        return mBitmap;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(0xFFAAAAAA);

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

        canvas.drawPath(mPath, mPaint);

//        canvas.drawBitmap(mBitmap, 0, 0, mPaint);
        /* mEraserMode 는 boolean 타입의 변수로서, 현재 지우개 사용모드인지를 판단.
         * 지우개 모드일때 화면에 패스를 그리지 않는 이유는 앞서 말했듯
         * PorterDuff.Mode.CLEAR 모드인 paint 는 검정색 색상의 속성을 지니기 때문에,
         * 이를 화면에 표시 하지 않기 위해서 이다.
         * */
    }

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        // commit the path to our offscreen
        mCanvas.drawPath(mPath, mPaint);
        // kill this so we don't double draw
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
//        return draw(mPath,event,0);

    }

//    private boolean draw(Path path, MotionEvent event, int type){
//        boolean ret = true;
//        float x = event.getX();
//        float y = event.getY();
//        switch(event.getAction()){
//            case MotionEvent.ACTION_DOWN:
//                path.reset();
//                path.moveTo(x, y);
//                mX = x;
//                mY = y;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                path.quadTo(mX, mY, (mX+x)/2, (mY+y)/2);
//                mX = x;
//                mY = y;
//                // 지우개를 실시간으로 반영하기 위해서는 캐시 캔버스인 mCanvas에 바로바로 그려줘야한다.
//                if(mEraserMode) mCanvas.drawPath(path, mPaint);
//                break;
//            case MotionEvent.ACTION_UP:
//                if(mPrevAction == MotionEvent.ACTION_DOWN){
//                    mCanvas.drawPoint(x, y, mPaint);
//                }else{
//                    path.quadTo(mX, mY, (mX+x)/2, (mY+y)/2);
//                    mCanvas.drawPath(path, mPaint);
//                }
//                path.reset();
//                break;
//            default:
//                ret = false;
//        }
//        invalidate();
//        mPrevAction = event.getAction();
//        return ret;
//    }
}
