/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.utils.cache.ImageFullLoader;
import com.picpic.sikkle.utils.cache.ImageLoader;

/**
 * Created by devmin-sikkle on 2016-03-22.
 */
public class TimelineAdComponent extends LinearLayout {
    private NativeAd nativeAd;
    private ImageLoader imageLoader;
    private ImageFullLoader imageFullLoader;

    LinearLayout layAd;
    TextView tvTitle;
    BoundableOfflineImageView bimv;
    ImageView icon;
    TextView tvSubTitle;
    Button btn;

    public TimelineAdComponent(Context context) {
        super(context);
        init(context);
    }

    public TimelineAdComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TimelineAdComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context con) {

        nativeAd = new NativeAd(con, con.getResources().getString(R.string.facenook_ad_id));

        imageLoader = new ImageLoader(con);
        imageFullLoader = new ImageFullLoader(con);

        View v = inflate(con, R.layout.view_timeline_ad, null);

        layAd = (LinearLayout) v.findViewById(R.id.lay_ad_row);
        tvTitle = (TextView) v.findViewById(R.id.tv_ad_row_title);
        bimv = (BoundableOfflineImageView) v.findViewById(R.id.bimv_ad_row);
        icon = (ImageView) v.findViewById(R.id.imv_ad_row);
        tvSubTitle = (TextView) v.findViewById(R.id.tv_ad_row_sub_title);
        btn = (Button) v.findViewById(R.id.btn_ad_row_do_it);

        nativeAd.setAdListener(new AbstractAdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Log.e("error", adError.getErrorMessage() + " / " + adError.getErrorCode());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (ad != nativeAd) {
//                    viewHolder.layAd.setVisibility(View.GONE);
                    return;
                }
//                        if (BuildConfig.DEBUG) {
                Log.e("face book ad", "fbad / " + nativeAd.getAdBody());
                Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesLinkUrl());
                Log.e("face book ad", "fbad / " + nativeAd.getAdCallToAction());
                Log.e("face book ad", "fbad / " + nativeAd.getAdSocialContext());
                Log.e("face book ad", "fbad / " + nativeAd.getAdSubtitle());
                Log.e("face book ad", "fbad / " + nativeAd.getAdTitle());
                Log.e("face book ad", "fbad / " + nativeAd.getId());
                Log.e("face book ad", "fbad / " + nativeAd.getAdChoicesIcon().getUrl());
                Log.e("face book ad", "fbad / " + nativeAd.getAdCoverImage().getUrl());
                Log.e("face book ad", "fbad / " + nativeAd.getAdIcon().getUrl());
//                        }

                tvTitle.setText(nativeAd.getAdTitle());

                imageFullLoader.DisplayImage(nativeAd.getAdCoverImage().getUrl(), bimv, false);

                imageLoader.DisplayImage(nativeAd.getAdChoicesIcon().getUrl(), icon, false);

                tvSubTitle.setText(nativeAd.getAdSubtitle());

                btn.setText(nativeAd.getAdCallToAction());

                nativeAd.registerViewForInteraction(layAd);
            }

            @Override
            public void onAdClicked(Ad ad) {
                super.onAdClicked(ad);
            }

        });

        nativeAd.loadAd();
    }

    public void setData(TimelineItem model) {
        tvTitle.setText(model.getAdTitle());

        imageFullLoader.DisplayImage(model.getAdCoverImg(), bimv, false);

        imageLoader.DisplayImage(model.getAdChoicesIcon(), icon, false);

        tvSubTitle.setText(model.getAdSubTitle());

        btn.setText(model.getAdCallToAction());

        nativeAd.registerViewForInteraction(layAd);

        invalidate();
    }
}
