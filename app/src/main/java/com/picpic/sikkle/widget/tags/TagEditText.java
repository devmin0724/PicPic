package com.picpic.sikkle.widget.tags;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.widget.MultiAutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jong-min on 2015-08-06.
 */
public class TagEditText extends MultiAutoCompleteTextView implements TextWatcher {

//    public static final String TAG_END_WITH_REGULAR_EXPRESSION = "#\\w+|@\\w+|&\\w+";
//    public static final String TAG_END_WITH_REGULAR_EXPRESSION = "#\\S*|@\\S*|&\\S*|\\S*#\\S*|\\S*@\\S*|\\S*&\\S*";
//    public static final String TAG_END_WITH_REGULAR_EXPRESSION = "@\\S*";
//    public static final String TAG_END_WITH_REGULAR_EXPRESSION = "(#\\w+)|(@\\w+)|(&\\w+)";

    private List<Link> mLinks = new ArrayList<>();

    private LinkModifier mLinkModifier;

//    private String[] tags, userTags, andTags;

    public TagEditText(Context context) {
        this(context, null);
    }

    public TagEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TagEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
//        adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
//        setAdapter(adapter);
//        setDropDownWidth(WindowManager.LayoutParams.WRAP_CONTENT);

        addTextChangedListener(this);
        setMovementMethod(LinkMovementMethod.getInstance());

        mLinkModifier = new LinkModifier(LinkModifier.ViewType.EDIT_TEXT);
    }

    public TagEditText addLink(Link link) {
        mLinks.add(link);

        mLinkModifier.setLinks(mLinks);

        return this;
    }

    public TagEditText addLinks(List<Link> links) {
        mLinks.addAll(links);

        mLinkModifier.setLinks(mLinks);

        return this;
    }

//    public void setTagList(String[] tags, String[] userTags, String[] andTags) {
//        this.tags = tags;
//        this.userTags = userTags;
//        this.andTags = andTags;
//
//        adapter = new ArrayAdapter<String>(getContext(),
//                android.R.layout.simple_dropdown_item_1line, userTags);
//        setAdapter(adapter);
//    }

    @Override
    public boolean enoughToFilter() {
        if (getText() != null) {
            return getText().length() != 0;
        }
        return true;
    }

//    @Override
//    protected void performFiltering(CharSequence text, int keyCode) {
//        String beforeCursor = getText().toString().substring(0, getSelectionStart());
//        Pattern pattern = Pattern.compile(TAG_END_WITH_REGULAR_EXPRESSION);
//        Matcher matcher = pattern.matcher(beforeCursor);
//        if (matcher.find()) {
//            text = matcher.group(0);
//        }
//        super.performFiltering(text, keyCode);
//    }
//
//    @Override
//    protected void replaceText(CharSequence text) {
//        String beforeCursor = getText().toString().substring(0, getSelectionStart());
//        String afterCursor = getText().toString().substring(getSelectionStart());
//
////        Pattern pattern = Pattern.compile("#\\w+|@\\w+|&\\w+");
////        Pattern pattern = Pattern.compile("#\\S*|@\\S*|&\\S*");
//        Pattern pattern = Pattern.compile(TAG_END_WITH_REGULAR_EXPRESSION);
//        Matcher matcher = pattern.matcher(beforeCursor);
//        StringBuffer sb = new StringBuffer();
//        int matcherStart = 0;
//        while (matcher.find()) {
//            int curPos = getSelectionStart();
//            if (curPos > matcher.start() &&
//                    curPos <= matcher.end()) {
//                matcherStart = matcher.start();
//                matcher.appendReplacement(sb, text.toString() + " ");
//            }
//        }
//        matcher.appendTail(sb);
//        setText(sb.toString() + afterCursor);
//        setSelection(matcherStart + text.length() + 1);
//    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mLinkModifier.setSpannable(s);

        mLinkModifier.build();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        if (s.length() != 0) {
//
//            String str = s.toString();
//            String lastStr = s.toString().substring(s.length() - 1, s.length());
//
//            CharSequence lastC = s.subSequence(s.length()-1, s.length());
////            lastC.
//            Log.e("str", str);
//            Log.e("last str", lastStr);
//            Log.e("last str length", lastStr.length() + "");
//
//            if (lastStr.equals(" ")) {
//                dismissDropDown();
//            }
//
//            if (s.subSequence(s.length()-1, s.length()).equals('#')) {
//                adapter = new ArrayAdapter<String>(getContext(),
//                        android.R.layout.simple_dropdown_item_1line, tags);
//                setAdapter(adapter);
//            }
//
//            if (s.subSequence(s.length()-1, s.length()).equals('@')) {
//                adapter = new ArrayAdapter<String>(getContext(),
//                        android.R.layout.simple_dropdown_item_1line, userTags);
//                setAdapter(adapter);
//            }
//
//            if (s.subSequence(s.length()-1, s.length()).equals('&')) {
//                adapter = new ArrayAdapter<String>(getContext(),
//                        android.R.layout.simple_dropdown_item_1line, andTags);
//                setAdapter(adapter);
//            }
//
//        }

        super.onTextChanged(s, start, before, count);
    }
}
