package com.picpic.sikkle.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;

/**
 * Created by Jong-min on 2015-08-05.
 */
public class TagSearchItemView extends LinearLayout {
    public TagSearchItemView(Context context) {
        super(context);
        invalidate();
    }

    public TagSearchItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        invalidate();
    }

    public TagSearchItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        invalidate();
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//
//        int width = (int) getDip(10);
//        int height = (int) getDip(10);
//
//        switch (widthMode) {
//            case MeasureSpec.UNSPECIFIED: // unspecified
//                width = widthMeasureSpec;
//                break;
//            case MeasureSpec.AT_MOST:  // wrap_content
//                break;
//            case MeasureSpec.EXACTLY:  // match_parent
//                width = MeasureSpec.getSize(widthMeasureSpec);
//                break;
//        }
//
//        switch (heightMode) {
//            case MeasureSpec.UNSPECIFIED: // unspecified
//                height = heightMeasureSpec;
//                break;
//            case MeasureSpec.AT_MOST:  // wrap_content
//                break;
//            case MeasureSpec.EXACTLY:  // match_parent
//                height = MeasureSpec.getSize(heightMeasureSpec);
//                break;
//        }
//
//        setMeasuredDimension(width, height);
//    }

    // 사용편리를 위해 작성한 함수 : dip 값을 pixel 값으로 변환하는 함수
    public float getDip(float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
//        paint.setAntiAlias(true);

//        RectF rf = new RectF(0, 0, getRight(), getBottom());
        int size = 0;
        if (getRight() - getLeft() < getBottom() - getTop()) {
            size = getBottom();
        } else {
            size = getRight();
        }
//
        RectF rf = new RectF(getLeft(), getTop(), getRight(), getBottom());
//
//        Rect r = new Rect(size, getTop(), getRight(), getBottom());
//        canvas.drawArc(rf, 90, 180, true, paint);
//        canvas.drawRect(r, paint);

        canvas.drawRoundRect(rf, getRight() / 4, getBottom(), paint);

    }
}