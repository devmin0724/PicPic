package com.picpic.sikkle.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-10-01.
 */
public class MultiProgressBar extends LinearLayout {

    ArrayList<ImageView> imvArr;

    public MultiProgressBar(Context context) {
        super(context);
        init();
    }

    public MultiProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

    }
}
