package com.picpic.sikkle.widget.swipemenuList;


/**
 * 
 * @author baoyz
 * @date 2014-8-24
 *
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
