package com.picpic.sikkle.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinImageLoader;

import pl.droidsonroids.gif.GifDrawable;

public class BoundableImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 250;
    public OnImageChangedListener listener;
    private MinImageLoader minImageLoader = AppController.getmInstance().getMinImageLoader();
//    private MinGIFImageLoader minImageLoader = AppController.getmInstance().getMinGIFImageLoader();

    private int maxWidth;
    private int maxHeight;
    private String urlString;
    Activity ac;
    private double mHeightRatio;

    public BoundableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.maxWidth, android.R.attr.maxHeight});
        maxWidth = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        maxHeight = ta.getDimensionPixelSize(0, Integer.MAX_VALUE);
        ta.recycle();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {

        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getResources().getColor(android.R.color.transparent)),
                new BitmapDrawable(getContext().getResources(), bm)});

        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);

//        minImageLoader.DisplayImage(urlString, this, this.ac, AppController.gifExe);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        if (maxWidth >= 0) width = Math.min(maxWidth, width);
        Drawable d = getDrawable();
        int height;
        if (d == null) {
            height = urlString == null ? getMeasuredHeight() : maxHeight;
//            height = 100;
        } else {
            int min = d.getMinimumWidth();
            height = min > 0 ? width * d.getMinimumHeight() / min : 0;
        }
        if (maxHeight >= 0) height = Math.min(maxHeight, height);
        setMeasuredDimension(width, height);
//        if (mHeightRatio > 0.0) {
//            // set the image views size
//            int width = MeasureSpec.getSize(widthMeasureSpec);
//            int height = (int) (width * mHeightRatio);
//            setMeasuredDimension(width, height);
//        } else {
//            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        }
    }

    @Override
    public void setImageDrawable(Drawable d) {
        super.setImageDrawable(d);

        if (listener != null) {
            listener.onImageChanged(d);
        }

        if (d instanceof GifDrawable) {

        } else {
//            AppController.gifExe.shutdownNow();
//            minImageLoader.DisplayImage(urlString, this, this.ac, AppController.gifExe);
        }

    }

    public void setImageURLString(String url, String thumb, Activity ac) {
        this.ac = ac;
        this.urlString = url;

        final Object image = AppController.memoryCache.get(url);

        if (image instanceof Drawable) {
//            AppController.gifExe.shutdownNow();
//            setImageDrawable((Drawable)image);
//            minImageLoader.DisplayImage(url, this, this.ac, AppController.gifExe);
        } else {
//            minImageLoader.DisplayImage(thumb, this, this.ac, AppController.thumbExe);
        }


    }

    @Override
    public void setMaxWidth(int width) {
        maxWidth = width;
        super.setMaxWidth(width);
    }

    @Override
    public void setMaxHeight(int height) {
        maxHeight = height;
        super.setMaxHeight(height);
    }

    public void setOnImageChangedListener(OnImageChangedListener listener) {
        this.listener = listener;
    }

    public interface OnImageChangedListener {
        void onImageChanged(Drawable d);
    }

}
