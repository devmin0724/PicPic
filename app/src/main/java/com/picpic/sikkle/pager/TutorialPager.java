package com.picpic.sikkle.pager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.login_join.LoginActivity;

import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class TutorialPager extends Fragment implements View.OnClickListener {

    ImageView gif;
    TextView tv;
    LinearLayout layout;
    Button next;

    int position = 0;

    public TutorialPager() {
    }

    @SuppressLint("ValidFragment")
    public TutorialPager(int aa) {
        position = aa;
    }

    public void TutorialPager() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = (LinearLayout) inflater.inflate(
                R.layout.pager_tutorial, container, false);

        tv = (TextView) layout.findViewById(R.id.tv_tutorial_pager);

        gif = (ImageView) layout.findViewById(R.id.imv_tutorial_pager_gif);
        GifDrawable gd = null;

        next = (Button) layout.findViewById(R.id.btn_tutorial_pager);
        next.setOnClickListener(this);

        switch (position) {
            case 0:
                layout.setBackgroundResource(R.drawable.bg_tutorial_1);
                try {
                    gd = new GifDrawable(getResources().getAssets().open("tuto_1.gif"));
                    gif.setImageDrawable(gd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                next.setVisibility(View.GONE);
                tv.setText(getResources().getString(R.string.tutorial_ment_1));
                break;
            case 1:

                layout.setBackgroundResource(R.drawable.bg_tutorial_2);
                try {
                    gd = new GifDrawable(getResources().getAssets().open("tuto_2.gif"));
                    gif.setImageDrawable(gd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                next.setVisibility(View.GONE);
                tv.setText(getResources().getString(R.string.tutorial_ment_2));
                break;
            case 2:

                layout.setBackgroundResource(R.drawable.bg_tutorial_3);
                try {
                    gd = new GifDrawable(getResources().getAssets().open("tuto_3.gif"));
                    gif.setImageDrawable(gd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                next.setVisibility(View.GONE);
                tv.setText(getResources().getString(R.string.tutorial_ment_3));
                break;

            case 3:

                layout.setBackgroundResource(R.drawable.bg_tutorial_4);
                try {
                    gd = new GifDrawable(getResources().getAssets().open("tuto_4_kr.gif"));
                    gif.setImageDrawable(gd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                next.setVisibility(View.GONE);
                tv.setText(getResources().getString(R.string.tutorial_ment_4));
                break;

            case 4:

                layout.setBackgroundResource(R.drawable.bg_tutorial_5);
                try {
                    gd = new GifDrawable(getResources().getAssets().open("tuto_5.gif"));
                    gif.setImageDrawable(gd);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                next.setVisibility(View.VISIBLE);
                tv.setText(getResources().getString(R.string.tutorial_ment_5));
                break;
        }

        return layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_tutorial_pager:
                getActivity().finish();
                getActivity().overridePendingTransition(0, 0);
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                break;
        }
    }
}
