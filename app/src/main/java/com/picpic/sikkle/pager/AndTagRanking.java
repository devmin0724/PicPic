package com.picpic.sikkle.pager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.AndTagRankItem;
import com.picpic.sikkle.beans.AndTagTaggerItem;
import com.picpic.sikkle.gifcamera.GIFCameraActivity;
import com.picpic.sikkle.ui.AndTagFeedActivity;
import com.picpic.sikkle.ui.UserFeedForIdActivity;
import com.picpic.sikkle.ui.UserListActivity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;

import java.util.ArrayList;

/**
 * Created by Jong-min on 2015-08-03.
 */
public class AndTagRanking extends Fragment implements View.OnClickListener {

    FrameLayout layout;
    CircleImageView cimv, cimv1, cimv2, cimv3;
    TextView tvId, tvCount, tvRank, tvTitle, tvBody;
    Button btnJoin;
    LinearLayout layNavi;
    //    BoundableImageView fimv;
    FixedImageView fimv;
    AndTagRankItem item = new AndTagRankItem();

    int countIndexes = 6;
    ImageView navis[];

    public AndTagRanking() {
    }

    @SuppressLint("ValidFragment")
    public AndTagRanking(AndTagRankItem aa, int length) {
        item = aa;
        countIndexes = length;
    }

    public void AndTagRanking() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = (FrameLayout) inflater.inflate(
                R.layout.pager_and_tag_rank, container, false);

        fimv = (FixedImageView) layout.findViewById(R.id.imv_and_tag_rank_bg);

        cimv = (CircleImageView) layout.findViewById(R.id.cimv_and_tag_rank_pager);
        cimv1 = (CircleImageView) layout.findViewById(R.id.cimv_and_tag_rank_1);
        cimv2 = (CircleImageView) layout.findViewById(R.id.cimv_and_tag_rank_2);
        cimv3 = (CircleImageView) layout.findViewById(R.id.cimv_and_tag_rank_3);

        tvId = (TextView) layout.findViewById(R.id.tv_and_tag_rank_id);
        tvCount = (TextView) layout.findViewById(R.id.tv_and_tag_rank_count);
        tvRank = (TextView) layout.findViewById(R.id.tv_and_tag_rank_rank);
        tvTitle = (TextView) layout.findViewById(R.id.tv_and_tag_rank_title);
        tvBody = (TextView) layout.findViewById(R.id.tv_and_tag_rank_body);

        tvId.setText(item.getId());
        tvRank.setText(item.getCount() + "");
        tvCount.setText("+" + item.getJoinCount());
        tvTitle.setText(item.getTitle());
        tvBody.setText(item.getBody());

        String tempUrl = item.getUrl();

        int lastIdx = tempUrl.lastIndexOf("_");

        final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

        fimv.setImageURLString(AppController.URL + item.getUrl(), AppController.URL + tempName);

        cimv.setImageURLString(AppController.URL + item.getPro());
        ArrayList<AndTagTaggerItem> arr = item.getTaggers();

        int size = 0;
        if (arr != null) {
            size = arr.size();
        }

        switch (size) {
            case 0:

                break;
            case 1:
                cimv1.setImageURLString(AppController.URL + arr.get(0).getUrl());
                break;
            case 2:
                cimv1.setImageURLString(AppController.URL + arr.get(0).getUrl());
                cimv2.setImageURLString(AppController.URL + arr.get(1).getUrl());
                break;
            case 3:
                cimv1.setImageURLString(AppController.URL + arr.get(0).getUrl());
                cimv2.setImageURLString(AppController.URL + arr.get(1).getUrl());
                cimv3.setImageURLString(AppController.URL + arr.get(2).getUrl());
                break;
        }

        cimv.setOnClickListener(this);
        cimv1.setOnClickListener(this);
        cimv2.setOnClickListener(this);
        cimv3.setOnClickListener(this);
        tvCount.setOnClickListener(this);

        btnJoin = (Button) layout.findViewById(R.id.btn_and_tag_join);

        btnJoin.setOnClickListener(this);

        layNavi = (LinearLayout) layout.findViewById(R.id.lay_and_tag_navi);

        navis = new ImageView[countIndexes];
        int tempSize = (int) (MinUtils.density * 7);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(tempSize, tempSize);
        lp.leftMargin = tempSize;

        for (int j = 0; j < countIndexes; j++) {
            navis[j] = new ImageView(getActivity());

            if (j == item.getCount() - 1) {
                navis[j].setBackgroundResource(R.drawable.imv_ranking_now);
            } else {
                navis[j].setBackgroundResource(R.drawable.imv_ranking_unnow);
            }

            navis[j].setLayoutParams(lp);
            layNavi.addView(navis[j], lp);

        }

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AndTagFeedActivity.class);
                i.putExtra("tag", item.getTag_id());
                startActivity(i);
            }
        });
        tvBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AndTagFeedActivity.class);
                i.putExtra("tag", item.getTag_id());
                startActivity(i);
            }
        });

        return layout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_and_tag_join:
//                Toast.makeText(getActivity(), "참여 ㄱㄱ", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(), GIFCameraActivity.class);
                i.putExtra("navi", 1);
                i.putExtra("tag_id", item.getTag_id());
                i.putExtra("tag_title", item.getTitle());
                i.putExtra("tag_body", item.getBody());
                startActivity(i);
                break;
            case R.id.cimv_and_tag_rank_pager:
                //TODO 사용자 피드로 이동
                Intent i2 = new Intent(getActivity(), UserFeedForIdActivity.class);
                i2.putExtra("id", item.getId());
                getActivity().startActivity(i2);
                break;
            case R.id.cimv_and_tag_rank_1:
            case R.id.cimv_and_tag_rank_2:
            case R.id.cimv_and_tag_rank_3:
            case R.id.tv_and_tag_rank_count:
                Intent i3 = new Intent(getActivity(), UserListActivity.class);
                i3.putExtra("tagId", item.getTag_id());
                i3.putExtra("pageNavi", UserListActivity.USER_LIST_ANDTAG);
                startActivity(i3);
                break;
        }
    }
}
