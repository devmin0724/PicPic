package com.picpic.sikkle.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.NewTimeLineAdapter;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.beans.TimelineResult;
import com.picpic.sikkle.ui.NewGridTimelineActivity;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class NewTimelineAllFragment extends android.support.v4.app.Fragment {

    public static TimelineResult m_ResultList = null;
    public static NewTimeLineAdapter m_ListAdapter = null;
    public static LinearLayout layProgress;
    public static boolean isEnd = false, lastItemVisibleFlag = false, isLoading = false;
    public static int gifWidth = 200, gifHeight = 200;
    public static int lastListCount = 0;
    public static StaggeredGridView list;

    public static String tempDate = "";
    public static int tempPostCount = 0;

    public static boolean mLockListView;

    private static final int INITIAL_DELAY_MILLIS = 300;
//    public static AnimationAdapter mAnimAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 전체피드 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        return inflater.inflate(R.layout.pager_new_timeline_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (StaggeredGridView) view.findViewById(R.id.gv_new_timelien_list);

        layProgress = (LinearLayout) view.findViewById(R.id.lay_new_timeline_progress);
        getData();
    }

    public static void initData() {
        m_ListAdapter = null;
        m_ResultList = null;

        TimelineResult tr = new TimelineResult();

        for (int i = 0; i < 8; i++) {
            TimelineItem ti = new TimelineItem();
            tr.add(ti);
        }

        updateResultList(tr);
    }

    public ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public static void getData() {

        initData();

        layProgress.setVisibility(View.VISIBLE);

        m_ListAdapter = null;
        m_ResultList = null;

        StringTransMethod stmDataResult = new StringTransMethod() {
            @Override
            public void endTrans(final String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    MinUtils.d("dataALl", result);
                    if (jd.getInt("result") == 0) {
//                        MinUtils.d("timelinedata", result);
                        if (jd.isNull("data")) {
                            NewGridTimelineActivity.ac.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    list.setVisibility(View.GONE);
//                                layIntro.setVisibility(View.VISIBLE);
                                }
                            });

                        } else {
                            NewGridTimelineActivity.ac.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    list.setVisibility(View.VISIBLE);
//                                layIntro.setVisibility(View.GONE);
                                }
                            });

                            JSONArray jarr = new JSONArray(jd.getString("data"));
                            TimelineResult tr = new TimelineResult();
                            TimelineItem ti;
                            JSONObject j;
                            TimelineLastCommentItem tlci;
                            JSONObject jj;


                            for (int i = 0; i < jarr.length(); i++) {
                                ti = new TimelineItem();

                                j = jarr.getJSONObject(i);

                                ti.setBody(j.getString("body"));
                                ti.setId(j.getString("id"));
                                ti.setComCount(j.getInt("com_cnt"));
                                if (j.getString("like_yn").equals("Y")) {
                                    ti.setIsLike(true);
                                } else if (j.getString("like_yn").equals("N")) {
                                    ti.setIsLike(false);
                                } else {
                                    ti.setIsLike(false);
                                }
                                ti.setLikeCount(j.getInt("like_cnt"));
                                if (!j.getString("and_tag_id").equals("")) {
                                    ti.setIsAndTag(true);
                                    ti.setAndTagId(j.getString("and_tag_id"));
                                    ti.setAndTagName(j.getString("and_tag"));
                                    ti.setAndTagBody(j.getString("and_tag_body"));
                                } else {
                                    ti.setIsAndTag(false);
                                    ti.setAndTagId("");
                                    ti.setAndTagName("");
                                    ti.setAndTagBody("");
                                }
                                if (j.getString("follow_yn").equals("Y")) {
                                    ti.setIsFollow(true);
                                } else {
                                    ti.setIsFollow(false);
                                }
                                ti.setOwnerId(j.getString("email"));
                                ti.setPlayCnt(j.getInt("play_cnt"));
                                ti.setPro_url(j.getString("profile_picture"));
                                ti.setPsot_id(j.getString("post_id"));
                                ti.setUrl(j.getString("url"));
                                ti.setTime(j.getString("date"));

                                ti.setWidth(j.getInt("width1"));
                                ti.setHeight(j.getInt("height1"));

                                tlci = new TimelineLastCommentItem();

                                jj = new JSONObject(j.getString("last_com"));

                                if (jj.getString("id").equals("null")) {
                                    tlci.setIsExist(false);
                                } else {
                                    tlci.setIsExist(true);
                                    tlci.setId(jj.getString("id"));
                                    tlci.setPro_url(jj.getString("profile_picture"));
                                    tlci.setBody(jj.getString("body"));
                                    tlci.setCom_id(jj.getString("com_id"));
                                    tlci.setEmail(jj.getString("email"));
                                }

                                ti.setTlci(tlci);

                                tr.add(ti);
                            }

                            isEnd = jarr.length() != 10;
                            tempPostCount = jd.getInt("posting_cnt");

//                            if (tempPostCount == 0) {
//                                //TODO 띄워라
//                                NewGridTimelineActivity.showToolTip();
//                            } else {
//                                if (!jd.isNull("last_post")) {
//                                    tempDate = jd.getString("last_post");
//                                }
//                                if (!tempDate.equals("")) {
//                                    // 현재날짜와 일수 비교
//
//                                    int year = Integer.parseInt(tempDate.substring(0, 3));
//                                    int month = Integer.parseInt(tempDate.substring(3, 5));
//                                    int day = Integer.parseInt(tempDate.substring(5, 7));
//
//                                    Calendar c = Calendar.getInstance();
//                                    int doy = c.get(Calendar.DAY_OF_YEAR);
//
//                                    Calendar c2 = Calendar.getInstance();
//                                    c2.set(year, month, day);
//
//                                    int doy2 = c2.get(Calendar.DAY_OF_YEAR);
//
//                                    if (doy2 > doy) {
//                                        if (doy2 - doy >= 10) {
//                                            //TODO 띄워라
//                                            NewGridTimelineActivity.showToolTip();
//                                        }
//                                    }
//                                }
//                            }
                            updateResultList(tr);
                        }
                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(NewGridTimelineActivity.ac.getApplicationContext(), NewGridTimelineActivity.ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("page", "1");

        AppController.apiTaskNew = new APITaskNew(NewGridTimelineActivity.ac, params, 521, stmDataResult);
        AppController.apiTaskNew.execute();
//        AppController.apiTestTaskNew = new APITestTaskNew(ac, params, 521, stmDataResult);
//        AppController.apiTestTaskNew.execute();

    }

    public static boolean updateResultList(TimelineResult resultList) {
        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
                m_ListAdapter.setResultList(m_ResultList);
            }
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new NewTimeLineAdapter(NewGridTimelineActivity.ac, 0, m_ResultList);

        NewGridTimelineActivity.ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                list.setAdapter(m_ListAdapter);

//                SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(m_ListAdapter);
//                swingBottomInAnimationAdapter.setAbsListView(list);
//
//                assert swingBottomInAnimationAdapter.getViewAnimator() != null;
//                swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(INITIAL_DELAY_MILLIS);
//
//                list.setAdapter(swingBottomInAnimationAdapter);
//
//                mAnimAdapter = new ScaleInAnimationAdapter(m_ListAdapter);
//                mAnimAdapter.setAbsListView(list);
//                list.setAdapter(mAnimAdapter);

//                mAnimAdapter = new ScaleInAnimationAdapter(m_ListAdapter);
//                mAnimAdapter.setAbsListView(list);
//                mAnimAdapter.notifyDataSetChanged();
//                list.setAdapter(mAnimAdapter);
//                list.requestLayout();

            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view,
                                    final int position, long id) {

                NewGridTimelineActivity.ac.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO 터치했을때 해동
                        try {
                            Intent i = new Intent(NewGridTimelineActivity.ac, SinglePostV1Activity.class);
                            i.putExtra("postId", m_ResultList.get(position).getPsot_id());
                            i.putExtra("navi", 0);
                            NewGridTimelineActivity.ac.startActivity(i);
                        } catch (NullPointerException e) {

                        }
                    }
                });

            }
        });


        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                if (lastItemVisibleFlag) {
//                    lastItemVisibleFlag = false;
//                    int count = view.getCount();
//                    int page = (count / 10) + 1;
//
////                    if (lastListCount != count) {
//                        Log.e("list", count + "/" + page);
//
//                        if (count % 10 == 0) {
//                            if (!isLoading) {
//                                isLoading = true;
//                                Log.e("list", "OK");
//
//                                Map<String, String> params = new HashMap<>();
//
//                                params.put("my_id", AppController.getSp().getString("email", ""));
//                                params.put("page", page + "");
//
//                                AppController.apiTaskNew = new APITaskNew(NewGridTimelineActivity.ac, params, 521, stm);
//                                AppController.apiTaskNew.execute();
////                                AppController.apiTestTaskNew = new APITestTaskNew(ac, params, 521, stm);
////                                AppController.apiTestTaskNew.execute();
//
//                                isLoading = false;
//                                lastListCount = count;
//                            }
//                        }
////                    }
//
//                }

//                switch (scrollState) {
//                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
//
//                        break;
//                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
//                        if (!AppController.gifExe.isTerminated()) {
//                            AppController.gifExe.shutdownNow();
//                        }
//                        if (!AppController.thumbExe.isTerminated()) {
//                            AppController.thumbExe.shutdownNow();
//                        }
//                        AppController.thumbExe = Executors.newFixedThreadPool(5);
//                        AppController.gifExe = Executors.newFixedThreadPool(1);
//                        break;
//                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
//                        if (!AppController.gifExe.isTerminated()) {
//                            AppController.gifExe.shutdownNow();
//                        }
//                        if (!AppController.thumbExe.isTerminated()) {
//                            AppController.thumbExe.shutdownNow();
//                        }
//                        AppController.thumbExe = Executors.newFixedThreadPool(5);
//                        AppController.gifExe = Executors.newFixedThreadPool(1);
//                        break;
//                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

//                lastItemVisibleFlag = (totalItemCount > 0)
//                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);

                int count = totalItemCount - visibleItemCount;

                if (firstVisibleItem >= count && totalItemCount != 0
                        && mLockListView == false) {
//                    Log.i(LOG, "Loading next items");
                    addItems();
                }

            }
        });

        layProgress.setVisibility(View.GONE);

        return true;
    }

    public static void addItems() {
        System.gc();
        mLockListView = true;

        Runnable run = new Runnable() {
            @Override
            public void run() {
                int count = list.getCount();
                int page = (count / 10) + 1;

//                    if (lastListCount != count) {
                Log.e("list", count + "/" + page);

                if (count % 10 == 0) {
                    if (!isLoading) {
                        isLoading = true;
                        Log.e("list", "OK");

                        Map<String, String> params = new HashMap<>();

                        params.put("my_id", AppController.getSp().getString("email", ""));
                        params.put("page", page + "");

                        AppController.apiTaskNew = new APITaskNew(NewGridTimelineActivity.ac, params, 521, stm);
                        AppController.apiTaskNew.execute();
//                                AppController.apiTestTaskNew = new APITestTaskNew(ac, params, 521, stm);
//                                AppController.apiTestTaskNew.execute();

                        isLoading = false;
                        lastListCount = count;
                    }
                }
//                    }
            }
        };

        // 속도의 딜레이를 구현하기 위한 꼼수
        Handler handler = new Handler();
        handler.postDelayed(run, 0);

    }

    public static StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {
            isEnd = false;
            String return_msg = result;
            Log.e("return_data2", return_msg);
            MinUtils.d("return_data2", return_msg);
            try {
                JSONObject jd = new JSONObject(return_msg);
                if (jd.getInt("result") == 0) {
                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineResult tr = new TimelineResult();
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;

                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));
                        ti.setTime(j.getString("date"));

                        ti.setWidth(j.getInt("width1"));
                        ti.setHeight(j.getInt("height1"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

                        tr.add(ti);
                    }

                    updateResultList(tr);

                    isEnd = true;
                    mLockListView = false;
                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(NewGridTimelineActivity.ac.getApplicationContext(), NewGridTimelineActivity.ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };
}