package com.picpic.sikkle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.InterestSearchUserItem;
import com.picpic.sikkle.beans.InterestSearchUserResult;
import com.picpic.sikkle.beans.SimplePostItem;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.beans.TagListResult;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedForIdActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;
import com.picpic.sikkle.widget.HashTag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class InterestSearchUserFragment extends android.support.v4.app.Fragment {

    public static ListView list;
    public static TextView hot;
    public static Activity ac;
    public static Context con;
    public static int pageNavi = 1;
    static InterestSearchUserResult m_ResultList2 = null;
    static ArrayAdapter<InterestSearchUserItem> m_ListAdapter2 = null;
    static TagListResult m_ResultList3 = null;
    static ArrayAdapter<TagListItem> m_ListAdapter3 = null;
    public static boolean lastItemVisibleFlag = false;
    public static String searchStr = "";

    public static void clearList() {
        pageNavi = 1;
        m_ResultList2 = null;
        m_ResultList3 = null;
        m_ListAdapter2 = null;
        m_ListAdapter3 = null;
        hot.setVisibility(View.GONE);
    }

    public static void getHotUserData(boolean init) {
        if (init) {
            clearList();
        }
        hot.setVisibility(View.VISIBLE);

        StringTransMethod stmHoutUser = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject j = new JSONObject(result);

                    if (j.getInt("result") == 0) {

                        JSONArray jHot = new JSONArray(j.getString("hot"));

                        InterestSearchUserResult isuResult = new InterestSearchUserResult();
                        InterestSearchUserItem isuItem;
                        JSONObject jd;
                        JSONArray best4;
                        ArrayList<SimplePostItem> arr;
                        JSONObject jj;
                        SimplePostItem spi;
                        for (int i1 = 0; i1 < jHot.length(); i1++) {
                            isuItem = new InterestSearchUserItem();

                            jd = jHot.getJSONObject(i1);

                            best4 = new JSONArray(jd.getString("best4"));

                            arr = new ArrayList<>();
                            for (int i2 = 0; i2 < best4.length(); i2++) {
                                jj = best4.getJSONObject(i2);
                                spi = new SimplePostItem();
                                spi.setUrl(jj.getString("url"));
                                spi.setPostId(jj.getString("post_id"));
                                arr.add(spi);
                            }

                            isuItem.setPostArr(arr);

                            isuItem.setEmail(jd.getString("email"));
                            if (jd.getString("follow_yn").equals("Y")) {
                                isuItem.setIsFollow(true);
                            } else {
                                isuItem.setIsFollow(false);
                            }
                            isuItem.setPro(jd.getString("profile_picture"));
                            isuItem.setId(jd.getString("id"));


                            if (jd.getString("tag1_id").equals("null")) {
                                isuItem.setTag1("");
                            } else {
                                isuItem.setTag1(jd.getString("tag1_name"));
                            }

                            if (jd.getString("tag2_id").equals("null")) {
                                isuItem.setTag2("");
                            } else {
                                isuItem.setTag2(jd.getString("tag2_name"));
                            }

                            if (jd.getString("tag3_id").equals("null")) {
                                isuItem.setTag3("");
                            } else {
                                isuItem.setTag3(jd.getString("tag3_name"));
                            }

                            isuResult.add(isuItem);
                        }

                        updateResultListUser1(isuResult);

                    } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));

        AppController.apiTaskNew = new APITaskNew(ac, params, 513, stmHoutUser);
        AppController.apiTaskNew.execute();

    }

    public static void getUserSearchData(final boolean init, String str, int page) {
        searchStr = str;
        if (init) {
            clearList();
        }

        StringTransMethod stmUserSearch = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                if (init) {
                    clearList();
                }
                try {
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {

                        if (j.isNull("user")) {
                            clearList();
                        } else {
                            JSONArray ja = new JSONArray(j.getString("user"));
                            TagListResult tlr = new TagListResult();
                            JSONObject jd;
                            TagListItem tli;
                            if (ja.getJSONObject(0).getString("id").equals("null")) {
                                clearList();
                            } else {
                                for (int i = 0; i < ja.length(); i++) {
                                    jd = ja.getJSONObject(i);

                                    tli = new TagListItem();
                                    tli.setTag_name(jd.getString("id"));
                                    tli.setTag_id(jd.getString("email"));
                                    tli.setTag_url(jd.getString("profile_picture"));
                                    if (jd.getString("follow_yn").equals("Y")) {
                                        tli.setIsFolow(true);
                                    } else {
                                        tli.setIsFolow(false);
                                    }

                                    tlr.add(tli);
                                }

                                updateResultListUser3(tlr);
                            }
                        }
                    } else if (j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("str", str);
        params.put("str_list", "");
        params.put("type", "U");
        params.put("page", "" + page);

        AppController.apiTaskNew = new APITaskNew(ac, params, 515, stmUserSearch);
        AppController.apiTaskNew.execute();

    }

    public static ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public static boolean updateResultListUser1(InterestSearchUserResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList2 != null && list != null && m_ListAdapter2 != null) {
            if (m_ResultList2 != resultList) {
                m_ResultList2.addAll(resultList);
            }
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter2.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList2 = resultList;

        m_ListAdapter2 = new SearchAdapter(ac, 0, m_ResultList2);

        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter2);
            }
        });

        return true;
    }

    public static boolean updateResultListUser3(TagListResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList3 != null && list != null && m_ListAdapter3 != null) {
            if (m_ResultList3 != resultList) {
                m_ResultList3.addAll(resultList);
            }
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter3.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList3 = resultList;

        m_ListAdapter3 = new SearchUserAdapter(ac, 0, m_ResultList3);

        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter3);
            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 6;
                    Log.e("page", page + "/" + count);
                    if (view.getCount() % 6 == 0) {
                        getUserSearchData(false, searchStr, page + 1);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
            }
        });

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ac = getActivity();
        con = getActivity().getApplicationContext();
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("유저 검색 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        return inflater.inflate(R.layout.pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_item);
        list.setEmptyView(view.findViewById(R.id.emptyContent));

        hot = (TextView) view.findViewById(R.id.tv_pager_hot);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static class SearchAdapter extends ArrayAdapter<InterestSearchUserItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public SearchAdapter(Context ctx, int txtViewId, List<InterestSearchUserItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                if (position % 2 == 1) {
                    convertView = m_LayoutInflater.inflate(R.layout.row_interest_search_user_2, null);
                } else {
                    convertView = m_LayoutInflater.inflate(R.layout.row_interest_search_user_1, null);
                }

                CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_interest_search_user_1);

                final ImageView imvFollow = (ImageView) convertView.findViewById(R.id.imv_interest_search_user_1_follow);
                final FixedImageView imv1 = (FixedImageView) convertView.findViewById(R.id.imv_interest_search_user_1_1);
                final FixedImageView imv2 = (FixedImageView) convertView.findViewById(R.id.imv_interest_search_user_1_2);
                final FixedImageView imv3 = (FixedImageView) convertView.findViewById(R.id.imv_interest_search_user_1_3);
                final FixedImageView imv4 = (FixedImageView) convertView.findViewById(R.id.imv_interest_search_user_1_4);

                final TextView tvName = (TextView) convertView.findViewById(R.id.tv_interest_search_row_user_name);
                final TextView tvTags = (TextView) convertView.findViewById(R.id.tv_interest_search_row_user_tags);

                final LinearLayout lay = (LinearLayout)convertView.findViewById(R.id.lay_interest_search_user_1);

                final InterestSearchUserItem model = getItem(pos);

                tvName.setText(model.getId());
                cimv.setImageURLString(AppController.URL + model.getPro());

                String tag1 = model.getTag1(), tag2 = model.getTag2(), tag3 = model.getTag3();

                if (!tag1.equals("")) {
                    tag1 = "#" + tag1;
                }
                if (!tag2.equals("")) {
                    tag2 = "#" + tag2;
                }
                if (!tag3.equals("")) {
                    tag3 = "#" + tag3;
                }

                if (model.getEmail().equals(AppController.getSp().getString("email", ""))) {
                    imvFollow.setVisibility(View.GONE);
                } else {
                    imvFollow.setVisibility(View.VISIBLE);
                }

                tvTags.setText(tag1 + " " + tag2 + " " + tag3);

                ArrayList<SimplePostItem> arr = model.getPostArr();

                String tempStr = "", tempUrl = "";
                int underbarCount = 0;

                if (!arr.get(0).getUrl().equals("") && !arr.get(0).getUrl().equals("null")) {
                    tempStr = arr.get(0).getUrl();
                    underbarCount = tempStr.lastIndexOf("_");
                    tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imv1.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                    imv1.setTag(arr.get(0).getPostId());
                } else {
                    imv1.setImageResource(R.drawable.non_interest);
                    imv1.setTag("");
                }
                if (!arr.get(1).getUrl().equals("") && !arr.get(1).getUrl().equals("null")) {
                    tempStr = arr.get(1).getUrl();
                    underbarCount = tempStr.lastIndexOf("_");
                    tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imv2.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                    imv2.setTag(arr.get(1).getPostId());
                } else {
                    imv2.setImageResource(R.drawable.non_interest);
                    imv2.setTag("");
                }
                if (!arr.get(2).getUrl().equals("") && !arr.get(2).getUrl().equals("null")) {
                    tempStr = arr.get(2).getUrl();
                    underbarCount = tempStr.lastIndexOf("_");
                    tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imv3.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                    imv3.setTag(arr.get(2).getPostId());
                } else {
                    imv3.setImageResource(R.drawable.non_interest);
                    imv3.setTag("");
                }
                if (!arr.get(3).getUrl().equals("") && !arr.get(3).getUrl().equals("null")) {
                    tempStr = arr.get(3).getUrl();
                    underbarCount = tempStr.lastIndexOf("_");
                    tempUrl = tempStr.substring(0, underbarCount + 1) + "1" + tempStr.substring(underbarCount + 2, tempStr.length());

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imv4.setImageURLString(AppController.URL + tempUrl, AppController.URL + tempName);
                    imv4.setTag(arr.get(3).getPostId());
                } else {
                    imv4.setImageResource(R.drawable.non_interest);
                    imv4.setTag("");
                }

                if (model.isFollow()) {
                    imvFollow.setSelected(true);
                } else {
                    imvFollow.setSelected(false);
                }

                String texts = tvTags.getText().toString();

                ArrayList<int[]> hashs = getSpans(texts, '#');

                SpannableString contents = new SpannableString(texts);

                for (int i = 0; i < hashs.size(); i++) {
                    int[] span = hashs.get(i);
                    int hashTagStart = span[0];
                    int hashTagEnd = span[1];

                    contents.setSpan(new HashTag(getContext()),
                            hashTagStart, hashTagEnd, 0);
                }

                tvTags.setMovementMethod(LinkMovementMethod.getInstance());
                tvTags.setText(contents);
                tvTags.setHighlightColor(0x00ffffff);

                cimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id = tvName.getText().toString();
                        if (!id.equals(AppController.getSp().getString("id", ""))) {
                            Intent i = new Intent(con, UserFeedForIdActivity.class);
                            i.putExtra("id", id);
                            ac.startActivity(i);
                        } else {
                            ac.startActivity(new Intent(con, MyPageV2Activity.class));
                            ac.finish();
                        }
                    }
                });

                tvName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id = tvName.getText().toString();
                        if (!id.equals(AppController.getSp().getString("id", ""))) {
                            Intent i = new Intent(con, UserFeedForIdActivity.class);
                            i.putExtra("id", id);
                            ac.startActivity(i);
                        } else {
                            ac.startActivity(new Intent(con, MyPageV2Activity.class));
                            ac.finish();
                        }
                    }
                });

                lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id = tvName.getText().toString();
                        if (!id.equals(AppController.getSp().getString("id", ""))) {
                            Intent i = new Intent(con, UserFeedForIdActivity.class);
                            i.putExtra("id", id);
                            ac.startActivity(i);
                        } else {
                            ac.startActivity(new Intent(con, MyPageV2Activity.class));
                            ac.finish();
                        }
                    }
                });

                imv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("id1", imv1.getTag().toString());
                        if (!imv1.getTag().toString().equals("")) {
                            String postId = imv1.getTag().toString();
                            Intent i = new Intent(con, SinglePostV1Activity.class);
                            i.putExtra("postId", postId);
                            i.putExtra("navi", 0);
                            ac.startActivity(i);
                        }
                    }
                });
                imv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("id2", imv1.getTag().toString());
                        if (!imv2.getTag().toString().equals("")) {
                            String postId = imv2.getTag().toString();
                            Intent i = new Intent(con, SinglePostV1Activity.class);
                            i.putExtra("postId", postId);
                            i.putExtra("navi", 0);
                            ac.startActivity(i);
                        }
                    }
                });
                imv3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("id3", imv1.getTag().toString());
                        if (!imv3.getTag().toString().equals("")) {
                            String postId = imv3.getTag().toString();
                            Intent i = new Intent(con, SinglePostV1Activity.class);
                            i.putExtra("postId", postId);
                            i.putExtra("navi", 0);
                            ac.startActivity(i);
                        }
                    }
                });
                imv4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("id4", imv1.getTag().toString());
                        if (!imv4.getTag().toString().equals("")) {
                            String postId = imv4.getTag().toString();
                            Intent i = new Intent(con, SinglePostV1Activity.class);
                            i.putExtra("postId", postId);
                            i.putExtra("navi", 0);
                            ac.startActivity(i);
                        }
                    }
                });

                imvFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            StringTransMethod stmFollow = new StringTransMethod() {
                                @Override
                                public void endTrans(String result) {
                                    try {
                                        JSONObject jd = new JSONObject(result);
                                        if (jd.getInt("result") == 0) {
                                            if (jd.getString("follow").equals("Y")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        imvFollow.setSelected(true);
                                                    }
                                                });
                                                m_ResultList2.get(pos).setIsFollow(true);
                                            } else if (jd.getString("follow").equals("N")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        imvFollow.setSelected(false);
                                                    }
                                                });
                                                m_ResultList2.get(pos).setIsFollow(false);
                                            } else {
                                                Toast.makeText(getContext(), getContext().getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                            Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };

                            JSONObject tempJ = new JSONObject();
                            tempJ.put("email", model.getEmail());

                            JSONArray jarr = new JSONArray();
                            jarr.put(tempJ);

                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
                            params.put("type", "N");

                            AppController.apiDataTaskNew = new APIDataTaskNew(ac, params, 402, stmFollow);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }

    public static class SearchUserAdapter extends ArrayAdapter<TagListItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public SearchUserAdapter(Context ctx, int txtViewId, List<TagListItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                if (convertView == null) {
                    convertView = m_LayoutInflater.inflate(R.layout.row_user_search, null);
                }

                CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_tag_search_row);

                TextView tv = (TextView) convertView.findViewById(R.id.tv_tag_search_row);

                final ImageView imv = (ImageView) convertView.findViewById(R.id.imv_tag_search_row_check);

                final TagListItem model = getItem(pos);

                cimv.setVisibility(View.VISIBLE);

                cimv.setImageURLString(AppController.URL + model.getTag_url());

                tv.setText(model.getTag_name());

                if (model.isFolow()) {
                    imv.setSelected(true);
                } else {
                    imv.setSelected(false);
                }

                cimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getContext(), UserFeedForIdActivity.class);
                        i.putExtra("id", model.getTag_name());
                        getContext().startActivity(i);
                    }
                });

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getContext(), UserFeedForIdActivity.class);
                        i.putExtra("id", model.getTag_name());
                        getContext().startActivity(i);
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getContext(), UserFeedForIdActivity.class);
                        i.putExtra("id", model.getTag_name());
                        getContext().startActivity(i);
                    }
                });

                imv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            StringTransMethod stmFollow = new StringTransMethod() {
                                @Override
                                public void endTrans(final String result) {
                                    try {
                                        JSONObject jd = new JSONObject(result);
                                        if (jd.getInt("result") == 0) {
                                            if (jd.getString("follow").equals("Y")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + model.getTag_id()).build());
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                                        imv.setSelected(true);
                                                    }
                                                });
//                                    isLikes.set(pos, true);
                                            } else if (jd.getString("follow").equals("N")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + model.getTag_id()).build());
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                                        imv.setSelected(false);
                                                    }
                                                });
//                                    isLikes.set(pos, false);
                                            } else {
                                                Toast.makeText(getContext().getApplicationContext(), getContext().getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                            }
                                        } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                            Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }

                                }
                            };
                            JSONObject tempJ = new JSONObject();
                            tempJ.put("email", model.getTag_id());

                            JSONArray jarr = new JSONArray();
                            jarr.put(tempJ);

                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
                            params.put("type", "N");

//                            AppController.apiTaskNew = new APITaskNew(params, 402, stmFollow);
//                            AppController.apiTaskNew.execute();

                            AppController.apiDataTaskNew = new APIDataTaskNew(ac, params, 402, stmFollow);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}