package com.picpic.sikkle.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.FriendResult;
import com.picpic.sikkle.beans.FriendResultItem;
import com.picpic.sikkle.ui.MyPageActivity;
import com.picpic.sikkle.ui.MyPageV2Activity;
import com.picpic.sikkle.ui.SinglePostContentActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class FindFriendRecommandFragment extends android.support.v4.app.Fragment {

    private static final int RECOMMAND_RETURN = 1111;
    public static FriendResult fr = null;
    private static String URL = "";
    ListView list;
    TextView tvTitle;
    Button btnAll;
    FriendResult m_ResultList3 = null;
    ArrayAdapter<FriendResultItem> m_ListAdapter3 = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_find_friends, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_find_friends_item);

        tvTitle = (TextView) view.findViewById(R.id.tv_pager_find_friends_title);

        btnAll = (Button) view.findViewById(R.id.btn_pager_find_friends_all_follow);

        URL = AppController.URL;

        getData();
    }

    private void getData() {

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("friends", "");
        params.put("register_form", "10009");

        AppController.apiTaskNew = new APITaskNew(getActivity(), params, 401, stmFriendRecommend);
        AppController.apiTaskNew.execute();


    }

    StringTransMethod stmFriendRecommend = new StringTransMethod() {
        @Override
        public void endTrans(String result) {

            final FriendResult fr = new FriendResult();
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    final JSONArray jarr = new JSONArray(jd.getString("data"));
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvTitle.setText(getResources().getString(R.string.friends_suggestion) + jarr.length());

                        }
                    });

                    for (int i = 0; i < jarr.length(); i++) {

                        JSONObject j = jarr.getJSONObject(i);

                        FriendResultItem ff = new FriendResultItem();
                        if (j.getString("follow_yn").equals("Y")) {
                            ff.setIsFollow(true);
                        } else {
                            ff.setIsFollow(false);
                        }
                        ff.setEmail(j.getString("email"));
                        ff.setName(j.getString("id"));
                        ff.setProUrl(j.getString("profile_picture"));

                        JSONArray jr = new JSONArray(j.getString("posts"));
                        ff.setUrl1(jr.getJSONObject(0).getString("url"));
                        ff.setPostId1(jr.getJSONObject(0).getString("post_id"));
                        ff.setUrl2(jr.getJSONObject(1).getString("url"));
                        ff.setPostId2(jr.getJSONObject(1).getString("post_id"));
                        ff.setUrl3(jr.getJSONObject(2).getString("url"));
                        ff.setPostId3(jr.getJSONObject(2).getString("post_id"));
                        ff.setUrl4(jr.getJSONObject(3).getString("url"));
                        ff.setPostId4(jr.getJSONObject(3).getString("post_id"));
                        fr.add(ff);

                    }
                    updateResultListUser3(fr);
                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {

            }
            btnAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringTransMethod stmFollowAll = new StringTransMethod() {
                        @Override
                        public void endTrans(String result) {
                            try {
                                JSONObject jd = new JSONObject(result);

                                if (jd.getInt("result") == 0) {
                                    for (int i = 0; i < m_ResultList3.size(); i++) {
                                        m_ResultList3.get(i).setIsFollow(true);
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            m_ListAdapter3.notifyDataSetChanged();
                                        }
                                    });

                                }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {

                            }
                        }
                    };
                    if (fr.size() > 0) {
                        try {
                            Map<String, String> params = new HashMap<>();
                            JSONObject tempJ;

                            JSONArray jarr = new JSONArray();
                            for (int i = 0; i < fr.size(); i++) {
                                tempJ = new JSONObject();
                                tempJ.put("email", fr.get(i).getEmail());
                                jarr.put(tempJ);
                            }

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
                            params.put("type", "R");

                            AppController.apiDataTaskNew = new APIDataTaskNew(getActivity(), params, 402, stmFollowAll);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    };

    private boolean updateResultListUser3(FriendResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList3 != null && list != null && m_ListAdapter3 != null) {
            if (m_ResultList3 != resultList) {
                m_ResultList3.addAll(resultList);
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter3.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList3 = resultList;

        m_ListAdapter3 = new FriendAdapter(getActivity(), 0, m_ResultList3);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter3);
            }
        });

        return true;
    }

    class FriendAdapter extends ArrayAdapter<FriendResultItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public FriendAdapter(Context ctx, int txtViewId, List<FriendResultItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_friend_result, null);

                CircleImageView imvPro = (CircleImageView) convertView.findViewById(R.id.cimv_friend_result_row_pro);
                final TextView tvName = (TextView) convertView.findViewById(R.id.tv_friend_result_row_name);
                final Button btnFollow = (Button) convertView.findViewById(R.id.btn_friend_result_row_follow);
                final FixedImageView imvPost1 = (FixedImageView) convertView.findViewById(R.id.bimv_friend_result_row_1);
                final FixedImageView imvPost2 = (FixedImageView) convertView.findViewById(R.id.bimv_friend_result_row_2);
                final FixedImageView imvPost3 = (FixedImageView) convertView.findViewById(R.id.bimv_friend_result_row_3);
                final FixedImageView imvPost4 = (FixedImageView) convertView.findViewById(R.id.bimv_friend_result_row_4);
                
                final FriendResultItem fri = getItem(pos);
                //TODO 타임라인 데이터 insert

                imvPro.setImageURLString(URL + fri.getProUrl());
                tvName.setText(fri.getName());
                tvName.setTag(fri.getEmail());

                Log.e("urls", fri.getUrl1() + "/" + fri.getUrl2() + "/" + fri.getUrl3() + "/" + fri.getUrl4());

                if (fri.isFollow()) {
                    btnFollow.setSelected(true);
                } else {
                    btnFollow.setSelected(false);
                }

                if (!fri.getUrl1().equals("null") && !fri.getUrl1().equals("")) {
                    String tempUrl = fri.getUrl1();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imvPost1.setImageURLString(URL + fri.getUrl1(), URL + tempName);
                    imvPost1.setTag(fri.getPostId1());
                } else {
                    imvPost1.setImageResource(R.drawable.non_p);
                    imvPost1.setTag("");
                }

                if (!fri.getUrl2().equals("null") && !fri.getUrl2().equals("")) {
                    String tempUrl = fri.getUrl2();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imvPost2.setImageURLString(URL + fri.getUrl2(), URL + tempName);
                    imvPost2.setTag(fri.getPostId2());
                } else {
                    imvPost2.setImageResource(R.drawable.non_p);
                    imvPost2.setTag("");
                }

                if (!fri.getUrl3().equals("null") && !fri.getUrl3().equals("")) {
                    String tempUrl = fri.getUrl3();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imvPost3.setImageURLString(URL + fri.getUrl3(), URL + tempName);
                    imvPost3.setTag(fri.getPostId3());
                } else {
                    imvPost3.setImageResource(R.drawable.non_p);
                    imvPost3.setTag("");
                }

                if (!fri.getUrl4().equals("null") && !fri.getUrl4().equals("")) {
                    String tempUrl = fri.getUrl4();

                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                    imvPost4.setImageURLString(URL + fri.getUrl4(), URL + tempName);
                    imvPost4.setTag(fri.getPostId4());
                } else {
                    imvPost4.setImageResource(R.drawable.non_p);
                    imvPost4.setTag("");
                }

                btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringTransMethod stmFollow = new StringTransMethod() {
                            @Override
                            public void endTrans(String result) {
                                try {
                                    JSONObject jd = new JSONObject(result);
                                    if (jd.getInt("result") == 0) {
                                        if (jd.getString("follow").equals("Y")) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnFollow.setSelected(true);

                                                }
                                            });
                                        } else if (jd.getString("follow").equals("N")) {
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    btnFollow.setSelected(false);

                                                }
                                            });
                                        } else {

                                            Toast.makeText(getContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                        }
                                    } else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                                        Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        };
                        try {

                            Map<String, String> params = new HashMap<>();
                            JSONObject tempJ = new JSONObject();
                            tempJ.put("email", fri.getEmail());

                            JSONArray jarr = new JSONArray();
                            jarr.put(tempJ);

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
//                            params.put("type", "N");
//                            params.put("type", "R");
                            params.put("type", "F");

//                            AppController.apiTaskNew = new APITaskNew(params, 402, stmFollow);
//                            AppController.apiTaskNew.execute();

                            AppController.apiDataTaskNew = new APIDataTaskNew(getActivity(), params, 402, stmFollow);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                tvName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = tvName.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            getActivity().startActivity(new Intent(getActivity(), MyPageV2Activity.class));
                            getActivity().finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(getActivity(), UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                getActivity().startActivity(i);
                            }
                        }
                    }
                });

                imvPro.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String myId = AppController.getSp().getString("email", "");
                        String tempEmail = tvName.getTag().toString();
                        if (tempEmail.equals(myId)) {
                            getActivity().startActivity(new Intent(getActivity(), MyPageV2Activity.class));
                            getActivity().finish();
                        } else {
                            if (!tempEmail.equals("")) {
                                Intent i = new Intent(getActivity(), UserFeedActivity.class);
                                i.putExtra("email", tempEmail);
                                getActivity().startActivity(i);
                            }
                        }
                    }
                });

                imvPost1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!imvPost1.getTag().toString().equals("")) {
                            Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                            i.putExtra("postId", imvPost1.getTag().toString());
                            i.putExtra("navi", 0);
                            getActivity().startActivity(i);
                        }
                    }
                });
                imvPost2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!imvPost2.getTag().toString().equals("")) {
                            Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                            i.putExtra("postId", imvPost2.getTag().toString());
                            i.putExtra("navi", 0);
                            getActivity().startActivity(i);
                        }
                    }
                });
                imvPost3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!imvPost3.getTag().toString().equals("")) {
                            Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                            i.putExtra("postId", imvPost3.getTag().toString());
                            i.putExtra("navi", 0);
                            getActivity().startActivity(i);
                        }
                    }
                });
                imvPost4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!imvPost4.getTag().toString().equals("")) {
                            Intent i = new Intent(getActivity(), SinglePostV1Activity.class);
                            i.putExtra("postId", imvPost4.getTag().toString());
                            i.putExtra("navi", 0);
                            getActivity().startActivity(i);
                        }
                    }
                });

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }

}