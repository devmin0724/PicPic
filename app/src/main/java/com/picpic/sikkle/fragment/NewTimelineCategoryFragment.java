package com.picpic.sikkle.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.ui.CategoryListActivity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.CircleImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class NewTimelineCategoryFragment extends android.support.v4.app.Fragment {

    GridView gv;
    ArrayList<CategoryItem> cateArr;

    int[] ress = {
            R.drawable.category_daylife, R.drawable.category_animal, R.drawable.category_celebrities, R.drawable.category_emotion, R.drawable.category_animation, R.drawable.category_food, R.drawable.category_fashion, R.drawable.category_beauty, R.drawable.category_artdesign, R.drawable.category_sports, R.drawable.category_movie, R.drawable.category_tv, R.drawable.category_game, R.drawable.category_cartoon, R.drawable.category_reaction, R.drawable.category_vehicle, R.drawable.category_music, R.drawable.category_expression, R.drawable.category_action, R.drawable.category_interest, R.drawable.category_decades, R.drawable.category_nature, R.drawable.category_sticker, R.drawable.category_science, R.drawable.category_holidays
    };

    int[] titles = {
            R.string.category_1, R.string.category_2, R.string.category_3, R.string.category_4, R.string.category_5, R.string.category_6, R.string.category_7, R.string.category_8, R.string.category_9, R.string.category_10, R.string.category_11, R.string.category_12, R.string.category_13, R.string.category_14, R.string.category_15, R.string.category_16, R.string.category_17, R.string.category_18, R.string.category_19, R.string.category_20, R.string.category_21, R.string.category_22, R.string.category_23, R.string.category_24, R.string.category_25
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 카테고리 리스트 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        return inflater.inflate(R.layout.pager_item_new_timeline_category, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        gv = (GridView) view.findViewById(R.id.gv_new_timeline_category);

        cateArr = new ArrayList<>();

        for (int i = 0; i < ress.length; i++) {
            CategoryItem ci = new CategoryItem();
            ci.setRes(ress[i]);
            ci.setTitleRes(titles[i]);
            cateArr.add(ci);
        }

        gv.setAdapter(new CategoryAdapter(getActivity(), 0, cateArr));

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), CategoryListActivity.class);
                intent.putExtra("pageNavi", i);
                startActivity(intent);
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder()
                        .setCategory(getClass()
                                .getName())
                        .setAction("Press Button")
                        .setLabel("category item Click" + "_" + getResources().getString(cateArr.get(i).getTitleRes())).build());
                AppController.t.send(new HitBuilders.EventBuilder()
                        .setCategory(getClass()
                                .getName())
                        .setAction("Press Button")
                        .setLabel("category item Click").build());
            }
        });

    }

    class CategoryAdapter extends ArrayAdapter<CategoryItem> {

        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public CategoryAdapter(Context context, int resource, List<CategoryItem> objects) {
            super(context, resource, objects);
            this.viewArray = new SparseArray<WeakReference<View>>(objects.size());
            this.m_LayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null) {
                    return convertView;
                }
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_new_timeline_category, null);

                LinearLayout lay = (LinearLayout)convertView.findViewById(R.id.lay_new_timeline_blank);
                CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_new_timeliine_category);
                TextView tv = (TextView) convertView.findViewById(R.id.tv_new_timeline_category);

                if(position == 0 || position == 1 || position == 2){
                    lay.setVisibility(View.VISIBLE);
                }else{
                    lay.setVisibility(View.GONE);
                }

                cimv.setImageResource(cateArr.get(position).getRes());
                tv.setText(getContext().getResources().getString(cateArr.get(position).getTitleRes()));

            } finally {

            }

            return convertView;
        }
    }

    class CategoryItem {
        private int res = 0;
        private int titleRes = 0;

        public int getTitleRes() {
            return titleRes;
        }

        public void setTitleRes(int titleRes) {
            this.titleRes = titleRes;
        }

        public int getRes() {
            return res;
        }

        public void setRes(int res) {
            this.res = res;
        }

    }

}