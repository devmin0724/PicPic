package com.picpic.sikkle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.FriendResultItem;
import com.picpic.sikkle.ui.DataLoadingActivity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class FindFriendGooglePlusFragment extends android.support.v4.app.Fragment {

    private static final int RECOMMAND_RETURN = 1111;
    ArrayList<FriendResultItem> arr;
    ListView list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_find_friend_non, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
//        list = (ListView) view.findViewById(R.id.list_pager_item);

//        arr = new ArrayList<>();
//
//        for (int i = 0; i < 6; i++) {
//            FriendResultItem ff = new FriendResultItem();
//            ff.setIsFollow(true);
//            ff.setName("aaaa");
//            ff.setProUrl("aaaa");
//            ff.setUrl1("aa");
//            ff.setUrl2("aa");
//            ff.setUrl3("aa");
//            ff.setUrl4("aa");
//            arr.add(ff);
//        }
//
//        list.setAdapter(new FriendResultAdapter(getActivity(), 0, arr));
//        getData();
    }

    private void getData() {

//        final ProgressDialog pd = new ProgressDialog(getActivity());
//        pd.setCanceledOnTouchOutside(false);
//        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        pd.show();

        Intent i = new Intent(getActivity(), DataLoadingActivity.class);

        JSONObject jo = new JSONObject();
        JSONArray ja = new JSONArray();

        String tempStr = "";
        try {
            jo.put("email", "w@w.w");
            jo.put("email", "p@p.p");
            ja.put(jo);
            tempStr = ja.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("str_list", tempStr);


        HashMap<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("friends", tempStr);
        params.put("register_form", "10003");

        i.putExtra("data", params);
        i.putExtra("code", 401);

        startActivityForResult(i, RECOMMAND_RETURN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RECOMMAND_RETURN:
                if (resultCode == Activity.RESULT_OK) {
                    String return_msg = data.getExtras().getString("return");
                    MinUtils.d("recommand friend return", return_msg);
                }
                break;
        }
    }

    class FriendResultAdapter extends ArrayAdapter<FriendResultItem> {

        String ROOT = "";
        int[] draws = {
//                R.drawable.cimv_1, R.drawable.cimv_2, R.drawable.cimv_1, R.drawable.cimv_2
        };
        String[] names = {
                "구글빡친조슘", "구글출근준비조슘", "구글볼잡힌또롱", "구글개발자"
        };
        private LayoutInflater m_LayoutInflater;
        private Context _con;

        public FriendResultAdapter(Context context, int resource, List<FriendResultItem> models) {
            super(context, resource, models);
            this.m_LayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            _con = context;
            ROOT = AppController.URL;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;

            View v = convertView;

            final Holder holder;

            if (v == null) {
                if (m_LayoutInflater == null)
                    m_LayoutInflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                v = m_LayoutInflater.inflate(R.layout.row_friend_result, null);
                holder = new Holder();

                holder.imvPro = (CircleImageView) v.findViewById(R.id.cimv_friend_result_row_pro);
                holder.tvName = (TextView) v.findViewById(R.id.tv_friend_result_row_name);
                holder.btnFollow = (Button) v.findViewById(R.id.btn_friend_result_row_follow);
                holder.imvPost1 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_1);
                holder.imvPost2 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_2);
                holder.imvPost3 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_3);
                holder.imvPost4 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_4);

                v.setTag(holder);

            } else {
                holder = (Holder) v.getTag();
            }

            final FriendResultItem fri = getItem(pos);

            holder.imvPro.setImageResource(draws[pos % 4]);
            holder.tvName.setText(names[pos % 4]);
            holder.btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.btnFollow.isSelected()) {
                        holder.btnFollow.setSelected(false);
                    } else {
                        holder.btnFollow.setSelected(true);
                    }
                }
            });

//            holder.imvPost1.setImageResource(R.drawable.cimv_1);
//            holder.imvPost2.setImageResource(R.drawable.cimv_2);
//            holder.imvPost3.setImageResource(R.drawable.cimv_1);
            holder.imvPost4.setImageResource(R.drawable.icon_find_recommend);

            return v;
        }

        class Holder {
            CircleImageView imvPro;
            TextView tvName;
            Button btnFollow;
            ImageView imvPost1, imvPost2, imvPost3, imvPost4;
        }
    }
}