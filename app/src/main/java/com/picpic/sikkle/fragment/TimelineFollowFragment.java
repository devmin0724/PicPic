/*
 * Copyright PicPic (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.picpic.sikkle.fragment;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.etiennelawlor.quickreturn.library.enums.QuickReturnViewType;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnListViewOnScrollListener;
import com.etiennelawlor.quickreturn.library.listeners.QuickReturnRecyclerViewOnScrollListener;
import com.etiennelawlor.quickreturn.library.utils.QuickReturnUtils;
import com.etiennelawlor.quickreturn.library.utils.ScrollAPI;
import com.etiennelawlor.quickreturn.library.utils.TransAPI;
import com.google.android.gms.analytics.HitBuilders;
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener;
import com.picpic.sikkle.R;
import com.picpic.sikkle.adapter.VideoListViewAdapter;
import com.picpic.sikkle.beans.TimelineItem;
import com.picpic.sikkle.beans.TimelineLastCommentItem;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.utils.video_list_demo.adapter.VideoRecyclerViewAdapter;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.BaseVideoItem;
import com.picpic.sikkle.utils.video_list_demo.adapter.items.ItemFactory;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.ListViewItemPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class TimelineFollowFragment extends android.support.v4.app.Fragment {

    public static LinearLayout layProgress, layNextLoading;

    private static ListView mRecyclerView;

    public boolean is = false;
    private static int nowPage = 1;

    List<String> videos = Arrays.asList(
            "http://gif.picpic.world/nealjr@naver.com1441936649703_2.mp4",
            "http://gif.picpic.world/test@account.com1444023453505_2.mp4",
            "http://gif.picpic.world/test@account.com1444024443164_2.mp4",
            "http://gif.picpic.world/test@account.com1444031141394_2.mp4",
            "http://gif.picpic.world/15182586117974851444198100442_2.mp4",
            "http://gif.picpic.world/15182586117974851444198145077_2.mp4",
            "http://gif.picpic.world/11134719986825491447213625828_2.mp4",
            "http://gif.picpic.world/11134719986825491447297312415_2.mp4",
            "http://gif.picpic.world/9011312466289061447730201165_2.mp4"
    );

    private static final ArrayList<BaseVideoItem> mList = new ArrayList<>();

    /**
     * Only the one (most visible) view should be active (and playing).
     * To calculate visibility of views we use {@link SingleListViewItemActiveCalculator}
     */
    private ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    private static final ListItemsVisibilityCalculator mListItemVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);
    //    private VideoRecyclerViewAdapter videoRecyclerViewAdapter;
    private static VideoListViewAdapter videoRecyclerViewAdapter;

    /**
     * ItemsPositionGetter is used by {@link ListItemsVisibilityCalculator} for getting information about
     * items position in the RecyclerView and LayoutManager
     */
    private static ItemsPositionGetter mItemsPositionGetter;

    /**
     * Here we use {@link SingleVideoPlayerManager}, which means that only one video playback is possible.
     */
    private static final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {
        }
    });

    private static int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

    public static TimelineFollowFragment newInstance(int page, String title) {
        TimelineFollowFragment hotFragment = new TimelineFollowFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        hotFragment.setArguments(args);

        return hotFragment;
    }

    public TimelineFollowFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("타임라인 인기피드 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        View rootView = inflater.inflate(R.layout.pager_timeline_list_2, container, false);

        mRecyclerView = (ListView) rootView.findViewById(R.id.list_timeline_2);

//        int headerHeight = TimelineV1NewActivity.header.getHeight();

//        LinearLayout blankHeader = new LinearLayout(getActivity());
//        blankHeader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, headerHeight));

        layProgress = (LinearLayout) rootView.findViewById(R.id.lay_timeline_progress_2);

        layNextLoading = (LinearLayout) rootView.findViewById(R.id.lay_timeline_next_loading);

        try {
            mList.add(ItemFactory.createItemFromAsset("blank.mp4", R.drawable.non_p, getActivity(), mVideoPlayerManager));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
//        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        mRecyclerView.setLayoutManager(mLayoutManager);

        videoRecyclerViewAdapter = new VideoListViewAdapter(mVideoPlayerManager, getActivity(), mList);

        mRecyclerView.setAdapter(videoRecyclerViewAdapter);
        mItemsPositionGetter = new ListViewItemPositionGetter(mRecyclerView);
//        mRecyclerView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                mScrollState = scrollState;
//                if (scrollState == SCROLL_STATE_IDLE && !mList.isEmpty()) {
//                    mListItemVisibilityCalculator.onScrollStateIdle(mItemsPositionGetter, view.getFirstVisiblePosition(), view.getLastVisiblePosition());
//                }
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (!mList.isEmpty()) {
//                    // on each scroll event we need to call onScroll for mListItemVisibilityCalculator
//                    // in order to recalculate the items visibility
//                    mListItemVisibilityCalculator.onScroll(mItemsPositionGetter, firstVisibleItem, visibleItemCount, mScrollState);
//                }
//            }
//        });
//        QuickReturnListViewOnScrollListener listener = new QuickReturnListViewOnScrollListener.Builder(QuickReturnViewType.HEADER)
//                .header(TimelineV1NewActivity.header)
//                .minHeaderTranslation(-headerHeight)
//                .isSnappable(true)
//                .build();
//
//        mRecyclerView.setOnScrollListener(listener);
//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
//                Log.e("scroll1", mLayoutManager.findFirstVisibleItemPosition() + " / " + mLayoutManager.findLastVisibleItemPosition());
//                mScrollState = scrollState;
//                if (scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {
//
//                    mVideoVisibilityCalculator.onScrollStateIdle(
//                            mItemsPositionGetter,
//                            mLayoutManager.findFirstVisibleItemPosition(),
//                            mLayoutManager.findLastVisibleItemPosition());
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                Log.e("scroll2", mLayoutManager.findFirstVisibleItemPosition() + " / " + mLayoutManager.findLastVisibleItemPosition());
//                if (!mList.isEmpty()) {
//                    mVideoVisibilityCalculator.onScroll(
//                            mItemsPositionGetter,
//                            mLayoutManager.findFirstVisibleItemPosition(),
//                            mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
//                            mScrollState);
//                }
//            }
//        });
//        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);
//
//        mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager, -1) {
//            @Override
//            public void onLoadMore(int currentPage) {
//                Log.e("currentPage", nowPage + " / " + currentPage);
//                if (nowPage < currentPage) {
//                    layNextLoading.setVisibility(View.VISIBLE);
//
//                    test.ok(currentPage);
//
//                    nowPage = currentPage;
//                }
//            }
//        });
//
//        QuickReturnRecyclerViewOnScrollListener scrollListener
// = new QuickReturnRecyclerViewOnScrollListener.Builder(QuickReturnViewType.HEADER)
//                .header(TimelineV1NewActivity.header)
//                .minHeaderTranslation(-headerHeight)
//                .isSnappable(true)
//                .build();
//
//        mRecyclerView.addOnScrollListener(scrollListener);

        getData();

        return rootView;
    }

    public void getData() {

//        layProgress.setVisibility(View.VISIBLE);

        StringTransMethod stmDataResult = new StringTransMethod() {
            @Override
            public void endTrans(final String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    MinUtils.d("dataFollow", result);
                    if (jd.getInt("result") == 0) {

                        JSONArray jarr = new JSONArray(jd.getString("data"));
                        TimelineItem ti;
                        JSONObject j;


                        for (int i = 0; i < jarr.length(); i++) {

                            ti = new TimelineItem();

                            j = jarr.getJSONObject(i);

                            ti.setBody(j.getString("body"));
                            ti.setId(j.getString("id"));
                            ti.setComCount(j.getInt("com_cnt"));
                            if (j.getString("like_yn").equals("Y")) {
                                ti.setIsLike(true);
                            } else if (j.getString("like_yn").equals("N")) {
                                ti.setIsLike(false);
                            } else {
                                ti.setIsLike(false);
                            }
                            ti.setLikeCount(j.getInt("like_cnt"));
                            if (!j.getString("and_tag_id").equals("")) {
                                ti.setIsAndTag(true);
                                ti.setAndTagId(j.getString("and_tag_id"));
                                ti.setAndTagName(j.getString("and_tag"));
                                ti.setAndTagBody(j.getString("and_tag_body"));
                            } else {
                                ti.setIsAndTag(false);
                                ti.setAndTagId("");
                                ti.setAndTagName("");
                                ti.setAndTagBody("");
                            }
                            if (j.getString("follow_yn").equals("Y")) {
                                ti.setIsFollow(true);
                            } else {
                                ti.setIsFollow(false);
                            }
                            ti.setOwnerId(j.getString("email"));
                            ti.setPlayCnt(j.getInt("play_cnt"));
                            ti.setPro_url(j.getString("profile_picture"));
                            ti.setPsot_id(j.getString("post_id"));
                            ti.setUrl(j.getString("url"));
                            ti.setTime(j.getString("date"));

                            ti.setWidth(j.getInt("width1"));
                            ti.setHeight(j.getInt("height1"));

                            ti.setWidth2(j.getInt("width2"));
                            ti.setHeight2(j.getInt("height2"));

                            ti.setTags(j.getString("tag_list"));

                            ti.setAd(false);

                            mList.add(ItemFactory.createItemFromModel(getActivity(), ti, mVideoPlayerManager, 0));

                        }
                        TimelineItem adItem = new TimelineItem();
                        adItem.setAd(true);
                        adItem.setAdVisible(true);
                        adItem.setUrl("blank.mp4");

                        mList.add(ItemFactory.createItemFromModel(getActivity(), adItem, mVideoPlayerManager, 0));

                        mList.remove(0);

                        videoRecyclerViewAdapter.notifyDataSetChanged();

//                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);
                        mItemsPositionGetter = new ListViewItemPositionGetter(mRecyclerView);
                        layProgress.setVisibility(View.GONE);

                        layNextLoading.setVisibility(View.GONE);

//                        mRecyclerView.scrollToPosition(1);
//                        mRecyclerView.smoothScrollToPosition(1);

//                        mRecyclerView.smoothScrollToPosition(0);

                    } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("page", "1");


        AppController.apiTaskNew3 = new APITaskNew(getActivity(), params, 508, stmDataResult);

        AppController.apiTaskNew3.execute();

    }

    TransAPI test = new TransAPI() {
        @Override
        public void ok(int page) {
            Log.e("pages", nowPage + "/" + page);
            if (nowPage != page) {
                final Map<String, String> params = new HashMap<>();

                params.put("my_id", AppController.getSp().getString("email", ""));
                params.put("page", page + "");

                AppController.apiTaskNew3 = new APITaskNew(getActivity(), params, 508, stm);

                AppController.apiTaskNew3.execute();
                nowPage = page;
            }
        }
    };

    static StringTransMethod stm = new StringTransMethod() {
        @Override
        public void endTrans(final String result) {

            MinUtils.d("timelineR", result);
            try {
                JSONObject jd = new JSONObject(result);
                if (jd.getInt("result") == 0) {
                    JSONArray jarr = new JSONArray(jd.getString("data"));
                    TimelineItem ti;
                    JSONObject j;
                    TimelineLastCommentItem tlci;
                    JSONObject jj;

                    for (int i = 0; i < jarr.length(); i++) {
                        ti = new TimelineItem();

                        j = jarr.getJSONObject(i);

                        ti.setBody(j.getString("body"));
                        ti.setId(j.getString("id"));
                        ti.setComCount(j.getInt("com_cnt"));
                        if (j.getString("like_yn").equals("Y")) {
                            ti.setIsLike(true);
                        } else if (j.getString("like_yn").equals("N")) {
                            ti.setIsLike(false);
                        } else {
                            ti.setIsLike(false);
                        }
                        ti.setLikeCount(j.getInt("like_cnt"));
                        if (!j.getString("and_tag_id").equals("")) {
                            ti.setIsAndTag(true);
                            ti.setAndTagId(j.getString("and_tag_id"));
                            ti.setAndTagName(j.getString("and_tag"));
                            ti.setAndTagBody(j.getString("and_tag_body"));
                        } else {
                            ti.setIsAndTag(false);
                            ti.setAndTagId("");
                            ti.setAndTagName("");
                            ti.setAndTagBody("");
                        }
                        if (j.getString("follow_yn").equals("Y")) {
                            ti.setIsFollow(true);
                        } else {
                            ti.setIsFollow(false);
                        }
                        ti.setOwnerId(j.getString("email"));
                        ti.setPlayCnt(j.getInt("play_cnt"));
                        ti.setPro_url(j.getString("profile_picture"));
                        ti.setPsot_id(j.getString("post_id"));
                        ti.setUrl(j.getString("url"));

                        ti.setTime(j.getString("date"));

                        ti.setWidth(j.getInt("width1"));
                        ti.setHeight(j.getInt("height1"));

                        tlci = new TimelineLastCommentItem();

                        jj = new JSONObject(j.getString("last_com"));

                        if (jj.getString("id").equals("null")) {
                            tlci.setIsExist(false);
                        } else {
                            tlci.setIsExist(true);
                            tlci.setId(jj.getString("id"));
                            tlci.setPro_url(jj.getString("profile_picture"));
                            tlci.setBody(jj.getString("body"));
                            tlci.setCom_id(jj.getString("com_id"));
                            tlci.setEmail(jj.getString("email"));
                        }

                        ti.setTlci(tlci);

//                        mList.add(ItemFactory.createItemFromModel(TimelineV1NewActivity.ac, ti, mVideoPlayerManager, 0));
                    }

                    TimelineItem adItem = new TimelineItem();
                    adItem.setAd(true);
                    if (nowPage % 2 == 0) {
                        adItem.setAdVisible(false);
                    } else {
                        adItem.setAdVisible(true);
                    }

//                    mList.add(ItemFactory.createItemFromModel(TimelineV1NewActivity.ac, adItem, mVideoPlayerManager, 0));


                    updateResultList();

                } else if (jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
//                    Toast.makeText(TimelineV1NewActivity.ac.getApplicationContext(), TimelineV1NewActivity.ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                layNextLoading.setVisibility(View.GONE);
            }

        }
    };

    public static boolean updateResultList() {

        videoRecyclerViewAdapter.notifyDataSetChanged();

//        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);
        mItemsPositionGetter = new ListViewItemPositionGetter(mRecyclerView);
        layProgress.setVisibility(View.GONE);

        layNextLoading.setVisibility(View.GONE);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mList.isEmpty()) {

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {

                    mListItemVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mRecyclerView.getFirstVisiblePosition(),
                            mRecyclerView.getLastVisiblePosition());

                }
            });

            videoRecyclerViewAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mVideoPlayerManager.resetMediaPlayer();
    }

    static class QuickReturnListViewOnScrollListener implements AbsListView.OnScrollListener {

        // region Member Variables
        private final QuickReturnViewType mQuickReturnViewType;
        private final View mHeader;
        private final int mMinHeaderTranslation;
        private final View mFooter;
        private final int mMinFooterTranslation;
        private final boolean mIsSnappable; // Can Quick Return view snap into place?
        private int serviceCode = 0;
        private int lastPage = 0;
        private int lastFirstVisibleIDX = -1;
        private String id;
        private TransAPI transAPI;
        private ScrollAPI scrollAPI;

        private int mPrevScrollY = 0;
        private int mHeaderDiffTotal = 0;
        private int mFooterDiffTotal = 0;
        private List<AbsListView.OnScrollListener> mExtraOnScrollListenerList = new ArrayList<AbsListView.OnScrollListener>();
        // endregion

        // region Constructors
        private QuickReturnListViewOnScrollListener(Builder builder) {
            mQuickReturnViewType = builder.mQuickReturnViewType;
            mHeader = builder.mHeader;
            mMinHeaderTranslation = builder.mMinHeaderTranslation;
            mFooter = builder.mFooter;
            mMinFooterTranslation = builder.mMinFooterTranslation;
            mIsSnappable = builder.mIsSnappable;
            transAPI = builder.transAPI;
            scrollAPI = builder.scrollAPI;
        }
        // endregion

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
//        Log.d(getClass().getSimpleName(), "onScrollStateChanged() : scrollState - "+scrollState);
            // apply another list' s on scroll listener
            mScrollState = scrollState;
            if (scrollState == SCROLL_STATE_IDLE && !mList.isEmpty()) {
                mListItemVisibilityCalculator.onScrollStateIdle(mItemsPositionGetter, view.getFirstVisiblePosition(), view.getLastVisiblePosition());
            }
//            if(scrollState == SCROLL_STATE_IDLE){
//                scrollAPI.ok(lastFirstVisibleIDX);
//            }else{
////            scrollAPI.ok(-1);
//            }
            for (AbsListView.OnScrollListener listener : mExtraOnScrollListenerList) {
                listener.onScrollStateChanged(view, scrollState);
            }
            if (scrollState == SCROLL_STATE_IDLE && mIsSnappable) {

                int midHeader = -mMinHeaderTranslation / 2;
                int midFooter = mMinFooterTranslation / 2;

                switch (mQuickReturnViewType) {
                    case HEADER:
                        if (-mHeaderDiffTotal > 0 && -mHeaderDiffTotal < midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = 0;
                        } else if (-mHeaderDiffTotal < -mMinHeaderTranslation && -mHeaderDiffTotal >= midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), mMinHeaderTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = mMinHeaderTranslation;
                        }
                        break;
                    case FOOTER:
                        if (-mFooterDiffTotal > 0 && -mFooterDiffTotal < midFooter) { // slide up
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = 0;
                        } else if (-mFooterDiffTotal < mMinFooterTranslation && -mFooterDiffTotal >= midFooter) { // slide down
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), mMinFooterTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = -mMinFooterTranslation;
                        }
                        break;
                    case BOTH:
                        if (-mHeaderDiffTotal > 0 && -mHeaderDiffTotal < midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = 0;
                        } else if (-mHeaderDiffTotal < -mMinHeaderTranslation && -mHeaderDiffTotal >= midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), mMinHeaderTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = mMinHeaderTranslation;
                        }

                        if (-mFooterDiffTotal > 0 && -mFooterDiffTotal < midFooter) { // slide up
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = 0;
                        } else if (-mFooterDiffTotal < mMinFooterTranslation && -mFooterDiffTotal >= midFooter) { // slide down
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), mMinFooterTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = -mMinFooterTranslation;
                        }
                        break;
                    case TWITTER:
                        if (-mHeaderDiffTotal > 0 && -mHeaderDiffTotal < midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = 0;
                        } else if (-mHeaderDiffTotal < -mMinHeaderTranslation && -mHeaderDiffTotal >= midHeader) {
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mHeader, "translationY", mHeader.getTranslationY(), mMinHeaderTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mHeaderDiffTotal = mMinHeaderTranslation;
                        }

                        if (-mFooterDiffTotal > 0 && -mFooterDiffTotal < midFooter) { // slide up
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), 0);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = 0;
                        } else if (-mFooterDiffTotal < mMinFooterTranslation && -mFooterDiffTotal >= midFooter) { // slide down
                            ObjectAnimator anim = ObjectAnimator.ofFloat(mFooter, "translationY", mFooter.getTranslationY(), mMinFooterTranslation);
                            anim.setDuration(100);
                            anim.start();
                            mFooterDiffTotal = -mMinFooterTranslation;
                        }
                        break;
                }

            }
        }

        @Override
        public void onScroll(AbsListView listview, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            if (!mList.isEmpty()) {
                // on each scroll event we need to call onScroll for mListItemVisibilityCalculator
                // in order to recalculate the items visibility
                mListItemVisibilityCalculator.onScroll(mItemsPositionGetter, firstVisibleItem, visibleItemCount, mScrollState);
            }

            int count = totalItemCount - visibleItemCount;

            lastFirstVisibleIDX = firstVisibleItem;

            if (firstVisibleItem >= count - 5 && totalItemCount != 0) {
                int listCount = listview.getCount();
                int page = (listCount / 10) + 1;
                if (firstVisibleItem >= count - 5 && totalItemCount != 0) {
                    if (nowPage < page) {
                        layNextLoading.setVisibility(View.VISIBLE);

                        Log.e("pages", nowPage + "/" + page);
                        if (nowPage != page) {
                            final Map<String, String> params = new HashMap<>();

                            params.put("my_id", AppController.getSp().getString("email", ""));
                            params.put("page", page + "");

//                            AppController.apiTaskNew3 = new APITaskNew(TimelineV1NewActivity.ac, params, 508, stm);

                            AppController.apiTaskNew3.execute();
                            nowPage = page;
                        } else {
                            layNextLoading.setVisibility(View.GONE);
                        }

                        nowPage = page;
                    }
                }
            }

            for (AbsListView.OnScrollListener listener : mExtraOnScrollListenerList) {
                listener.onScroll(listview, firstVisibleItem, visibleItemCount, totalItemCount);
            }
            int scrollY = QuickReturnUtils.getScrollY(listview);
            int diff = mPrevScrollY - scrollY;

            if (diff != 0) {
                switch (mQuickReturnViewType) {
                    case HEADER:
                        if (diff < 0) { // scrolling down
                            mHeaderDiffTotal = Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation);
                        } else { // scrolling up
                            mHeaderDiffTotal = Math.min(Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation), 0);
                        }

                        mHeader.setTranslationY(mHeaderDiffTotal);
                        break;
                    case FOOTER:
                        if (diff < 0) { // scrolling down
                            mFooterDiffTotal = Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation);
                        } else { // scrolling up
                            mFooterDiffTotal = Math.min(Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation), 0);
                        }

                        mFooter.setTranslationY(-mFooterDiffTotal);
                        break;
                    case BOTH:
                        if (diff < 0) { // scrolling down
                            mHeaderDiffTotal = Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation);
                            mFooterDiffTotal = Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation);
                        } else { // scrolling up
                            mHeaderDiffTotal = Math.min(Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation), 0);
                            mFooterDiffTotal = Math.min(Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation), 0);
                        }

                        mHeader.setTranslationY(mHeaderDiffTotal);
                        mFooter.setTranslationY(-mFooterDiffTotal);
                        break;
                    case TWITTER:
                        if (diff < 0) { // scrolling down
                            if (scrollY > -mMinHeaderTranslation)
                                mHeaderDiffTotal = Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation);

                            if (scrollY > mMinFooterTranslation)
                                mFooterDiffTotal = Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation);
                        } else { // scrolling up
                            mHeaderDiffTotal = Math.min(Math.max(mHeaderDiffTotal + diff, mMinHeaderTranslation), 0);
                            mFooterDiffTotal = Math.min(Math.max(mFooterDiffTotal + diff, -mMinFooterTranslation), 0);
                        }

                        mHeader.setTranslationY(mHeaderDiffTotal);
                        mFooter.setTranslationY(-mFooterDiffTotal);
                    default:
                        break;
                }
            }

            mPrevScrollY = scrollY;
        }

        // region Helper Methods
        public void registerExtraOnScrollListener(AbsListView.OnScrollListener listener) {
            mExtraOnScrollListenerList.add(listener);
        }
        // endregion

        // region Inner Classes

        public static class Builder {
            // Required parameters
            private final QuickReturnViewType mQuickReturnViewType;

            // Optional parameters - initialized to default values
            private View mHeader = null;
            private int mMinHeaderTranslation = 0;
            private View mFooter = null;
            private int mMinFooterTranslation = 0;
            private boolean mIsSnappable = false;
            private TransAPI transAPI;
            private ScrollAPI scrollAPI;

            public Builder(QuickReturnViewType quickReturnViewType) {
                mQuickReturnViewType = quickReturnViewType;
            }

            public Builder header(View header) {
                mHeader = header;
                return this;
            }

            public Builder minHeaderTranslation(int minHeaderTranslation) {
                mMinHeaderTranslation = minHeaderTranslation;
                return this;
            }

            public Builder footer(View footer) {
                mFooter = footer;
                return this;
            }

            public Builder api(TransAPI api) {
                this.transAPI = api;
                return this;
            }

            public Builder scrollAPI(ScrollAPI api) {
                this.scrollAPI = api;
                return this;
            }

            public Builder minFooterTranslation(int minFooterTranslation) {
                mMinFooterTranslation = minFooterTranslation;
                return this;
            }

            public Builder isSnappable(boolean isSnappable) {
                mIsSnappable = isSnappable;
                return this;
            }

            public QuickReturnListViewOnScrollListener build() {
                return new QuickReturnListViewOnScrollListener(this);
            }
        }

        // endregion
    }

}