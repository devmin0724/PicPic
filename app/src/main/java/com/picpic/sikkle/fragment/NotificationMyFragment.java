package com.picpic.sikkle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.NotifiactionResult;
import com.picpic.sikkle.beans.NotificationItem;
import com.picpic.sikkle.ui.AndTagFeedActivity;
import com.picpic.sikkle.ui.SinglePostV1Activity;
import com.picpic.sikkle.ui.UserFeedForIdActivity;
import com.picpic.sikkle.utils.APIDataTaskNew;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.CircleImageView;
import com.picpic.sikkle.widget.FixedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class NotificationMyFragment extends android.support.v4.app.Fragment {

    NotifiactionResult myInfoResult = new NotifiactionResult();
    public static ListView list;
    boolean lastItemVisibleFlag = false, isEnd = false;
    NotifiactionResult m_ResultList = null;
    ArrayAdapter<NotificationItem> m_ListAdapter = null;
    public static Activity ac;

//    public static AnimationAdapter mAnimAdapter;

    public static int checkType(String type) {

        if (type.equals("PL")) {
            return NotificationItem.NOTIFIACIONT_POST_LIKE;
        } else if (type.equals("CL")) {
            return NotificationItem.NOTIFIACIONT_COMMENT_LIKE;
        } else if (type.equals("PC")) {
            return NotificationItem.NOTIFIACIONT_POST_COMMENT;
        } else if (type.equals("FM")) {
            return NotificationItem.NOTIFIACIONT_FOLLOW_ME;
        } else if (type.equals("RM")) {
            return NotificationItem.NOTIFIACIONT_REPIC_ME;
        } else if (type.equals("CMP")) {
            return NotificationItem.NOTIFIACIONT_REPORT_POST;
        } else if (type.equals("CMC")) {
            return NotificationItem.NOTIFIACIONT_REPORT_COMMENT;
        } else if (type.equals("FP")) {
            return NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE;
        } else if (type.equals("FI")) {
            return NotificationItem.NOTIFIACIONT_FOLLOW_ID;
        } else if (type.equals("FJA")) {
            return NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN;
        } else if (type.equals("FCA")) {
            return NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE;
        } else if (type.equals("TMP")) {
            return NotificationItem.NOTIFIACIONT_TAG_ME_POST;
        } else if (type.equals("TMC")) {
            return NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT;
        } else if (type.equals("RP")) {
            return NotificationItem.NOTIFIACIONT_REPLY_POST;
        } else {
            return NotificationItem.NOTIFIACIONT_POST_LIKE;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("알림 my리스트 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        ac = getActivity();
        return inflater.inflate(R.layout.pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_item);

        getData(true);
    }

    public void getData(boolean is) {
        if (is) {
            m_ResultList = null;
            m_ListAdapter = null;
        }
        int page = 1;
        if (list.getCount() % 15 == 0) {
            page = (list.getCount() / 15) + 1;
        }

        StringTransMethod stmMy = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);

                    if (jd.getInt("result") == 0) {
                        JSONArray jarr = new JSONArray(jd.getString("data"));

                        myInfoResult = new NotifiactionResult();
                        JSONObject j;
                        NotificationItem ni;
                        for (int i = 0; i < jarr.length(); i++) {
                            j = jarr.getJSONObject(i);
                            ni = new NotificationItem();

                            ni.setType(checkType(j.getString("type")));
                            ni.setId(j.getString("who_id"));
                            ni.setEmail(j.getString("who_email"));
                            ni.setPro(j.getString("profile_picture"));
                            if (j.getString("confirm_yn").equals("Y")) {
                                ni.setIsConfirm(true);
                            } else {
                                ni.setIsConfirm(false);
                            }
                            if (j.getString("follow_yn").equals("Y")) {
                                ni.setIsFollow(true);
                            } else {
                                ni.setIsFollow(false);
                            }
                            ni.setTime(j.getString("date"));
                            ni.setTargetId1(j.getString("target_id_1"));
                            ni.setTargetId2(j.getString("target_id_2"));
                            ni.setTargetName(j.getString("target_name"));
                            ni.setUrl(j.getString("url"));

                            myInfoResult.add(ni);
                        }

                        updateResultList(myInfoResult);
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                        Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };
        Map<String, String> params = new HashMap<>();

        params.put("my_id", AppController.getSp().getString("email", ""));
        params.put("type", "M");
        params.put("page", page + "");

        AppController.apiTaskNew = new APITaskNew(ac, params, 602, stmMy);
        AppController.apiTaskNew.execute();

    }

    public boolean updateResultList(NotifiactionResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList) {
                m_ResultList.addAll(resultList);
            }
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new NotiAdapter(ac, 0, m_ResultList);

        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);

//                mAnimAdapter = new ScaleInAnimationAdapter(m_ListAdapter);
//                mAnimAdapter.setAbsListView(list);
//                list.setAdapter(mAnimAdapter);
            }
        });
        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && lastItemVisibleFlag) {
//                    Log.e("onScrollChanged", lastItemVisibleFlag + " / " + AbsListView.OnScrollListener.SCROLL_STATE_IDLE);
                    if (!isEnd) {
                        lastItemVisibleFlag = false;

                        getData(false);
                    }

//                    }

                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 5);

                if (totalItemCount % 15 != 0) {
                    lastItemVisibleFlag = false;
                }

            }
        });
        return true;
    }

    class NotiAdapter extends ArrayAdapter<NotificationItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public NotiAdapter(Context ctx, int txtViewId, List<NotificationItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_notification, null);

                TextView tvTime = (TextView) convertView.findViewById(R.id.tv_notification_row_time);
                TextView tvBody = (TextView) convertView.findViewById(R.id.tv_notification_row_body);

                CircleImageView cimv = (CircleImageView) convertView.findViewById(R.id.cimv_notification_row);

                FixedImageView imvGIF = (FixedImageView) convertView.findViewById(R.id.imv_notification_row_gif);
                final ImageView imvCheck = (ImageView) convertView.findViewById(R.id.imv_notification_row_check);

                FrameLayout layimg = (FrameLayout) convertView.findViewById(R.id.lay_notification_row_content);

                final NotificationItem model = getItem(pos);

                final int navi = model.getType();

                cimv.setImageURLString(AppController.URL + model.getPro());

                String ment = "";

                switch (navi) {
                    case NotificationItem.NOTIFIACIONT_POST_LIKE:
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_post_like_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_comment_like_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_POST_COMMENT:
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_post_comment_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.VISIBLE);
                        imvGIF.setVisibility(View.GONE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_follow_me_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_REPIC_ME:
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_post_repic_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_REPORT_POST:
                        cimv.setImageURLString(AppController.URL + "noprofile.png");
//                        cimv.
                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = getContext().getResources().getString(R.string.notification_report_post_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
                        cimv.setImageURLString(AppController.URL + "noprofile.png");

                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);

                        ment = getContext().getResources().getString(R.string.notification_report_comment_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
                        layimg.setVisibility(View.GONE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_profile_change_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
                        layimg.setVisibility(View.GONE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_id_change_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
                        layimg.setVisibility(View.GONE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_and_join_1_ment) + " &" + model.getTargetName() + getContext().getResources().getString(R.string.notification_and_join_2_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
                        layimg.setVisibility(View.GONE);

                        ment = model.getId() + getContext().getResources().getString(R.string.notification_and_create_1_ment) + " &" + model.getTargetName() + getContext().getResources().getString(R.string.notification_and_create_2_ment);
                        break;
                    case NotificationItem.NOTIFIACIONT_TAG_ME_POST:

                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);
                        ment = model.getId() + getResources().getString(R.string.notification_tag_me_post);
                        break;
                    case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:

                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);
                        ment = model.getId() + getResources().getString(R.string.notification_tag_me_comment);
                        break;
                    case NotificationItem.NOTIFIACIONT_REPLY_POST:

                        layimg.setVisibility(View.VISIBLE);

                        imvCheck.setVisibility(View.GONE);
                        imvGIF.setVisibility(View.VISIBLE);
                        ment = model.getId() + getResources().getString(R.string.notification_post_comment_post);
                        break;
                }


                ArrayList<int[]> andSpans = MinUtils.getSpans(ment, '&');

                SpannableString commentsContent = new SpannableString(ment);

                for (int i = 0; i < andSpans.size(); i++) {
                    int[] span = andSpans.get(i);
                    int calloutStart = span[0];
                    int calloutEnd = span[1];

                    AndTag at = new AndTag(getContext());
                    at.setTagId(model.getTargetId2());
                    commentsContent.setSpan(at,
                            calloutStart,
                            calloutEnd, 0);

                }

                tvBody.setMovementMethod(LinkMovementMethod.getInstance());
                tvBody.setText(commentsContent);
                tvBody.setHighlightColor(getContext().getResources().getColor(android.R.color.transparent));

                String tempUrl = model.getUrl();

                if (!tempUrl.equals("") && tempUrl != null) {
                    int lastIdx = tempUrl.lastIndexOf("_");

                    final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";

                    imvGIF.setImageURLString(AppController.URL + model.getUrl(), AppController.URL + tempName);
                }

                String date = model.getTime().substring(4, 6) + getContext().getResources().getString(R.string.month) + " " + model.getTime().substring(6, 8) + getContext().getResources().getString(R.string.day);

                if (date.substring(0, 1).equals("0")) {
                    date = date.substring(1, date.length());
                }

                tvTime.setText(date);

                if (model.isFollow()) {
                    imvCheck.setSelected(true);
                } else {
                    imvCheck.setSelected(false);
                }

                imvCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            StringTransMethod stmFollow = new StringTransMethod() {
                                @Override
                                public void endTrans(String result) {
                                    try {
                                        JSONObject jd = new JSONObject(result);
                                        if (jd.getInt("result") == 0) {
                                            if (jd.getString("follow").equals("Y")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click" + "_" + model.getEmail()).build());
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Follow Click").build());
                                                        imvCheck.setSelected(true);
                                                    }
                                                });

                                            } else if (jd.getString("follow").equals("N")) {
                                                ac.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click" + "_" + model.getEmail()).build());
                                                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                                                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Unfollow Click").build());
                                                        imvCheck.setSelected(false);
                                                    }
                                                });

                                            } else {

                                                Toast.makeText(getContext(), getResources().getString(R.string.follow_over_count_today), Toast.LENGTH_SHORT).show();
                                            }
                                        }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000){
                                            Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {

                                    }
                                }
                            };

                            JSONObject tempJ = new JSONObject();
                            tempJ.put("email", model.getEmail());

                            JSONArray jarr = new JSONArray();
                            jarr.put(tempJ);

                            Map<String, String> params = new HashMap<>();

                            params.put("myId", AppController.getSp().getString("email", ""));
                            params.put("email", jarr.toString());
                            params.put("type", "N");

                            AppController.apiDataTaskNew = new APIDataTaskNew(ac, params, 402, stmFollow);
                            AppController.apiDataTaskNew.execute();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                tvBody.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, SinglePostV1Activity.class);
                        Intent i2 = new Intent(ac, SinglePostV1Activity.class);
                        Intent i3 = new Intent(ac, UserFeedForIdActivity.class);
                        Intent i4 = new Intent(ac, AndTagFeedActivity.class);
                        switch (navi) {
                            case NotificationItem.NOTIFIACIONT_POST_LIKE:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_POST_COMMENT:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPIC_ME:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_POST:
//                                getToast("팝업창 뚜시");
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
//                                getToast("팝업창");
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_POST:
                                i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPLY_POST:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                        }
                    }
                });

                imvGIF.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, SinglePostV1Activity.class);
                        Intent i2 = new Intent(ac, SinglePostV1Activity.class);
                        Intent i3 = new Intent(ac, UserFeedForIdActivity.class);
                        Intent i4 = new Intent(ac, AndTagFeedActivity.class);
                        switch (navi) {
                            case NotificationItem.NOTIFIACIONT_POST_LIKE:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_POST_COMMENT:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPIC_ME:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_POST:
//                                getToast("팝업창 뚜시");
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
//                                getToast("팝업창");
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_POST:
                                i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPLY_POST:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                        }
                    }
                });

                tvTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, SinglePostV1Activity.class);
                        Intent i2 = new Intent(ac, SinglePostV1Activity.class);
                        Intent i3 = new Intent(ac, UserFeedForIdActivity.class);
                        Intent i4 = new Intent(ac, AndTagFeedActivity.class);
                        switch (navi) {
                            case NotificationItem.NOTIFIACIONT_POST_LIKE:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_POST_COMMENT:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPIC_ME:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_POST:
//                                getToast("팝업창 뚜시");
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
//                                getToast("팝업창");
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_POST:
                                i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPLY_POST:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                        }
                    }
                });

                cimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (navi != NotificationItem.NOTIFIACIONT_REPORT_POST
                                && navi != NotificationItem.NOTIFIACIONT_REPORT_COMMENT) {
                            Intent i3 = new Intent(ac, UserFeedForIdActivity.class);
                            i3.putExtra("id", model.getId());
                            ac.startActivity(i3);
                        }
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, SinglePostV1Activity.class);
                        Intent i2 = new Intent(ac, SinglePostV1Activity.class);
                        Intent i3 = new Intent(ac, UserFeedForIdActivity.class);
                        Intent i4 = new Intent(ac, AndTagFeedActivity.class);
                        switch (navi) {
                            case NotificationItem.NOTIFIACIONT_POST_LIKE:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_COMMENT_LIKE:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_POST_COMMENT:
//                                getToast("해당 댓글목록으로 이동");
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ME:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPIC_ME:
//                                getToast("해당 포스트로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_POST:
//                                getToast("팝업창 뚜시");
                                break;
                            case NotificationItem.NOTIFIACIONT_REPORT_COMMENT:
//                                getToast("팝업창");
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_PROFILE:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ID:
//                                getToast("해당 사용자 피드로 이동");
                                i3.putExtra("id", model.getId());
                                ac.startActivity(i3);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_JOIN:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_FOLLOW_ANDTAG_CREATE:
//                                getToast("해당 &태그 페이지로 이동");
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_POST:
                                i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i.putExtra("postId", model.getTargetId1());
                                i.putExtra("navi", 0);
                                ac.startActivity(i);
                                break;
                            case NotificationItem.NOTIFIACIONT_TAG_ME_COMMENT:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                            case NotificationItem.NOTIFIACIONT_REPLY_POST:
                                i2.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                i2.putExtra("postId", model.getTargetId1());
                                i2.putExtra("navi", 1);
                                ac.startActivity(i2);
                                break;
                        }
                    }
                });

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}