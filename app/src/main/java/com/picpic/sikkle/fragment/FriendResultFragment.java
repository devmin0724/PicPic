package com.picpic.sikkle.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.FriendResultItem;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.CircleImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jong-min on 2015-08-24.
 */
public class FriendResultFragment extends android.support.v4.app.Fragment {

    ArrayList<FriendResultItem> arr;

    int pageNavi = 0;
    ListView list;

    public FriendResultFragment() {

    }

    @SuppressLint("ValidFragment")
    public FriendResultFragment(int navi) {
        pageNavi = navi;
    }

    public void FriendResultFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_item);

        arr = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            FriendResultItem ff = new FriendResultItem();
            ff.setIsFollow(true);
            ff.setName("aaaa");
            ff.setProUrl("aaaa");
            ff.setUrl1("aa");
            ff.setUrl2("aa");
            ff.setUrl3("aa");
            ff.setUrl4("aa");
            arr.add(ff);
        }

        list.setAdapter(new FriendResultAdapter(getActivity(), 0, arr));
    }


    class FriendResultAdapter extends ArrayAdapter<FriendResultItem> {

        String ROOT = "";
        int[] draws = {
//                R.drawable.cimv_1, R.drawable.cimv_2, R.drawable.cimv_1, R.drawable.cimv_2
        };
        String[] names = {
                "빡친조슘", "출근준비조슘", "볼잡힌또롱", "개발자"
        };
        private LayoutInflater m_LayoutInflater;
        private Context _con;

        public FriendResultAdapter(Context context, int resource, List<FriendResultItem> models) {
            super(context, resource, models);
            this.m_LayoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            _con = context;
            ROOT = AppController.URL;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final int pos = position;

            View v = convertView;

            final Holder holder;

            if (v == null) {
                if (m_LayoutInflater == null)
                    m_LayoutInflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                v = m_LayoutInflater.inflate(R.layout.row_friend_result, null);
                holder = new Holder();

                holder.imvPro = (CircleImageView) v.findViewById(R.id.cimv_friend_result_row_pro);
                holder.tvName = (TextView) v.findViewById(R.id.tv_friend_result_row_name);
                holder.btnFollow = (Button) v.findViewById(R.id.btn_friend_result_row_follow);
                holder.imvPost1 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_1);
                holder.imvPost2 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_2);
                holder.imvPost3 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_3);
                holder.imvPost4 = (ImageView) v.findViewById(R.id.bimv_friend_result_row_4);

                v.setTag(holder);

            } else {
                holder = (Holder) v.getTag();
            }

            final FriendResultItem fri = getItem(pos);

            holder.imvPro.setImageResource(draws[pos % 4]);
            holder.tvName.setText(names[pos % 4]);
            holder.btnFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.btnFollow.isSelected()) {
                        holder.btnFollow.setSelected(false);
                    } else {
                        holder.btnFollow.setSelected(true);
                    }
                }
            });

//            holder.imvPost1.setImageResource(R.drawable.cimv_1);
//            holder.imvPost2.setImageResource(R.drawable.cimv_2);
//            holder.imvPost3.setImageResource(R.drawable.cimv_1);
            holder.imvPost4.setImageResource(R.drawable.icon_find_recommend);

            return v;
        }

        class Holder {
            CircleImageView imvPro;
            TextView tvName;
            Button btnFollow;
            ImageView imvPost1, imvPost2, imvPost3, imvPost4;
        }
    }
}
