package com.picpic.sikkle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.TagListItem;
import com.picpic.sikkle.beans.TagListResult;
import com.picpic.sikkle.ui.TagFeedActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.CircleImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class InterestSearchTagFragment extends android.support.v4.app.Fragment {

    public static ListView list;
    public static TagListResult tlr = new TagListResult();
    public static int pageNavi = 1;
    public static Context con;
    public static String searchStr = "";
    static TagListResult m_ResultList = null;
    static ArrayAdapter<TagListItem> m_ListAdapter = null;
    public static boolean lastItemVisibleFlag = false;
    public static Activity ac;

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    public static ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public static void clearList() {
        pageNavi = 1;
        m_ResultList = null;
        m_ListAdapter = null;
    }

    public static boolean updateResultListTag(TagListResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList != null && list != null && m_ListAdapter != null) {
            if (m_ResultList != resultList || m_ResultList.size() == 0) {
                m_ResultList.addAll(resultList);
            }
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList = resultList;

        m_ListAdapter = new SearchAdapter(ac, 0, m_ResultList);

        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list.setAdapter(m_ListAdapter);
            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 10;
//                    Log.e("page", page + "/" + count);
                    if (view.getCount() % 10 == 0) {
                        getTagSearchData(false, searchStr, page + 1);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
//                Log.e("last", lastItemVisibleFlag + "");
            }
        });

        pageNavi = pageNavi + 1;
        return true;
    }

    public static void getTagSearchData(final boolean init, String str, int a) {
        searchStr = str;
        if (init) {
            pageNavi = 1;
            if (m_ResultList != null) {
                m_ResultList.clear();
                m_ListAdapter = new SearchAdapter(ac, 0, m_ResultList);
//                ac.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        list.setAdapter(m_ListAdapter);
//
//                    }
//                });
                list.setAdapter(m_ListAdapter);
//                m_ListAdapter.notifyDataSetChanged();
            }
        } else {

        }

        StringTransMethod stmTagSearch = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                if (init) {
                    pageNavi = 1;
                    if (m_ResultList != null) {
                        m_ResultList.clear();
                        m_ListAdapter = new SearchAdapter(ac, 0, m_ResultList);
//                ac.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        list.setAdapter(m_ListAdapter);
//
//                    }
//                });
                        list.setAdapter(m_ListAdapter);
//                m_ListAdapter.notifyDataSetChanged();
                    }
                } else {

                }
                try {
                    Log.e("result", result);
                    JSONObject j = new JSONObject(result);
                    if (j.getInt("result") == 0) {

                        if (j.isNull("data")) {
                            if (m_ResultList != null) {
                                m_ResultList.clear();
                                m_ListAdapter = new SearchAdapter(ac, 0, m_ResultList);
                                ac.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        list.setAdapter(m_ListAdapter);

                                    }
                                });
                            }
                        } else {
                            JSONArray ja = new JSONArray(j.getString("data"));
                            ArrayList<TagListItem> arr = new ArrayList<>();
                            ArrayList<String> arr2 = new ArrayList<>();
                            JSONObject jd;
                            TagListItem tli;
                            for (int i = 0; i < ja.length(); i++) {
                                jd = ja.getJSONObject(i);

                                tli = new TagListItem();
                                tli.setTag_name(jd.getString("tag_str"));
                                tli.setTag_id(jd.getString("tag_id"));

                                tlr.add(tli);
                            }

                            updateResultListTag(tlr);
                        }

                    } else if(j.getInt("result") == 100 || j.getInt("result") == 1000) {
                        Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> parmas = new HashMap<>();
        parmas.put("tag_name", str);
        parmas.put("page", "" + a);

        AppController.apiTaskNew = new APITaskNew(ac, parmas, 501, stmTagSearch);
        AppController.apiTaskNew.execute();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        con = getActivity();
        ac = getActivity();
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("Tag검색 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        return inflater.inflate(R.layout.pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_item);
        list.setEmptyView(view.findViewById(R.id.emptyContent));

    }

    public static class SearchAdapter extends ArrayAdapter<TagListItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public SearchAdapter(Context ctx, int txtViewId, List<TagListItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                if(convertView == null){
                    convertView = m_LayoutInflater.inflate(R.layout.row_tag_search, null);
                }

                TextView tv = (TextView) convertView.findViewById(R.id.tv_tag_search_row);

                final TagListItem model = getItem(pos);

                tv.setText("#" + model.getTag_name());

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i2 = new Intent(getContext(), TagFeedActivity.class);
                        i2.putExtra("tag", model.getTag_name());
                        getContext().startActivity(i2);
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i2 = new Intent(getContext(), TagFeedActivity.class);
                        i2.putExtra("tag", model.getTag_name());
                        getContext().startActivity(i2);
                    }
                });

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}