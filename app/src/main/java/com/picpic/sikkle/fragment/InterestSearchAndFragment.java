package com.picpic.sikkle.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.InterestAndTagSearchItem;
import com.picpic.sikkle.beans.InterestAndTagSearchResult;
import com.picpic.sikkle.ui.AndTagFeedActivity;
import com.picpic.sikkle.utils.APITaskNew;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.StringTransMethod;
import com.picpic.sikkle.widget.AndTag;
import com.picpic.sikkle.widget.FixedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jong-min on 2015-08-25.
 */
public class InterestSearchAndFragment extends android.support.v4.app.Fragment {

    public static ListView list;
    public static int pageNavi = 1;
    public static Activity ac;
    static InterestAndTagSearchResult m_ResultList4 = null;
    static ArrayAdapter<InterestAndTagSearchItem> m_ListAdapter4 = null;
    public static boolean lastItemVisibleFlag = false;
    public static String searchStr = "";

    public static void clearList() {
        pageNavi = 1;
        m_ResultList4 = null;
        m_ListAdapter4 = null;
    }
    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }
    public static void getAndTagSearchData(boolean init, String str, int page) {
        if (init) {
            clearList();
        }

        StringTransMethod stmAndTagSearch = new StringTransMethod() {
            @Override
            public void endTrans(String result) {
                try {
                    JSONObject jd = new JSONObject(result);
                    if (jd.getInt("result") == 0) {
                        InterestAndTagSearchResult iatsResult = new InterestAndTagSearchResult();
                        if (!jd.isNull("data")) {
                            JSONArray jarr = new JSONArray(jd.getString("data"));
                            JSONObject j;
                            InterestAndTagSearchItem iatsItem;
                            for (int i = 0; i < jarr.length(); i++) {
                                j = jarr.getJSONObject(i);
                                iatsItem = new InterestAndTagSearchItem();
                                iatsItem.setUrl(j.getString("url"));
                                iatsItem.setBody(j.getString("explanation"));
                                iatsItem.setTag_id(j.getString("tag_id"));
                                iatsItem.setTitle(j.getString("tag_name"));

                                iatsResult.add(iatsItem);
                            }

                            updateResultListUser4(iatsResult);
                        }
                    }else if(jd.getInt("result") == 100 || jd.getInt("result") == 1000) {
                        Toast.makeText(ac.getApplicationContext(), ac.getResources().getString(R.string.warning_non_network), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map<String, String> params = new HashMap<>();

        params.put("str", str);
        params.put("page", "" + page);

        AppController.apiTaskNew = new APITaskNew(ac, params, 502, stmAndTagSearch);
        AppController.apiTaskNew.execute();

    }

    public static ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(prefix + "\\S+");
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }

    public static boolean updateResultListUser4(InterestAndTagSearchResult resultList) {

        if (resultList == null || resultList.size() == 0)
            return false;

        if (m_ResultList4 != null && list != null && m_ListAdapter4 != null) {
            if (m_ResultList4 != resultList) {
                m_ResultList4.addAll(resultList);
            }
            ac.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    m_ListAdapter4.notifyDataSetChanged();
                }
            });
            return true;
        }
        m_ResultList4 = resultList;

        m_ListAdapter4 = new SearchAdapter(ac, 0, m_ResultList4);
//        m_ListAdapter4 = new ArrayAdapter<InterestAndTagSearchItem>(ac, 0, m_ResultList4) {};
        ac.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                list.setAdapter(m_ListAdapter4);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ac, AndTagFeedActivity.class);
                i.putExtra("tag", m_ResultList4.get(position).getTag_id());
                ac.startActivity(i);
            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (lastItemVisibleFlag) {
                    lastItemVisibleFlag = false;
                    int count = view.getCount();
                    int page = view.getCount() / 10;
                    Log.e("page", page + "/" + count);
                    if (view.getCount() % 10 == 0) {
                        getAndTagSearchData(false, searchStr, page + 1);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItemVisibleFlag = (totalItemCount > 0)
                        && (firstVisibleItem + visibleItemCount >= totalItemCount - 3);
            }
        });

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ac = getActivity();
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("AndTag 검색 페이지");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        return inflater.inflate(R.layout.pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        list = (ListView) view.findViewById(R.id.list_pager_item);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    static class SearchAdapter extends ArrayAdapter<InterestAndTagSearchItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;

        public SearchAdapter(Context ctx, int txtViewId, List<InterestAndTagSearchItem> modles) {
            super(ctx, txtViewId, modles);
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_interest_and_tag_search, null);

                FixedImageView fimv = (FixedImageView) convertView.findViewById(R.id.fimv_and_tag_search_row);

                TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_and_tag_search_row_title);
                TextView tvBody = (TextView) convertView.findViewById(R.id.tv_and_tag_search_row_body);

                final InterestAndTagSearchItem model = getItem(pos);

                String tempUrl = model.getUrl();

                int lastIdx = tempUrl.lastIndexOf("_");

                final String tempName = tempUrl.substring(0, lastIdx) + ".jpg";
                fimv.setImageURLString(AppController.URL + model.getUrl(), AppController.URL + tempName);
                fimv.setTag(model.getTag_id());

                tvTitle.setText("&" + model.getTitle());
                tvBody.setText(model.getBody());

                String commentsText = tvTitle.getText().toString();

                ArrayList<int[]> andSpans = MinUtils.getSpans(commentsText, '&');

                SpannableString commentsContent = new SpannableString(commentsText);

                for (int i = 0; i < andSpans.size(); i++) {
                    int[] span = andSpans.get(i);
                    int calloutStart = span[0];
                    int calloutEnd = span[1];


                    AndTag at = new AndTag(getContext());
                    at.setTagId(model.getTag_id());
                    commentsContent.setSpan(at,
                            calloutStart,
                            calloutEnd, 0);

                }

                tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
                tvTitle.setText(commentsContent);
                tvTitle.setHighlightColor(0x00ffffff);

                tvBody.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, AndTagFeedActivity.class);
                        i.putExtra("tag", m_ResultList4.get(pos).getTag_id());
                        ac.startActivity(i);
                    }
                });

                tvTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, AndTagFeedActivity.class);
                        i.putExtra("tag", m_ResultList4.get(pos).getTag_id());
                        ac.startActivity(i);
                    }
                });

                fimv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ac, AndTagFeedActivity.class);
                        i.putExtra("tag", m_ResultList4.get(pos).getTag_id());
                        ac.startActivity(i);
                    }
                });
                

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }
    }
}