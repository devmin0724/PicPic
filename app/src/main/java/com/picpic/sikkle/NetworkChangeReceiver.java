package com.picpic.sikkle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.NetworkUtil;

/**
 * Created by devmin-sikkle on 2015-12-02.
 */
public class NetworkChangeReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        AppController.NETWORK_STATUS = NetworkUtil.getConnectivityStatus(context);
        switch (AppController.NETWORK_STATUS){
            case 0:
                //TODO 네크워크 끊김
//                Toast.makeText(context.getApplicationContext(), "disconnect", Toast.LENGTH_SHORT).show();
                break;
            case 1 :
                //TODO WIFI
//                Toast.makeText(context.getApplicationContext(), "WIFI", Toast.LENGTH_SHORT).show();
                break;
            case 2 :
                //TODO 4G
//                Toast.makeText(context.getApplicationContext(), "4G", Toast.LENGTH_SHORT).show();
                break;
            case 3 :
                //TODO 3G
//                Toast.makeText(context.getApplicationContext(), "3G", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
