package com.picpic.sikkle.gifcamera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fingram.agifEncoder.QAGIFEncoder;
import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.ui.WritePostActivityV1;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.filter.IFAmaroFilter;
import com.picpic.sikkle.utils.filter.IFBrannanFilter;
import com.picpic.sikkle.utils.filter.IFEarlybirdFilter;
import com.picpic.sikkle.utils.filter.IFHefeFilter;
import com.picpic.sikkle.utils.filter.IFHudsonFilter;
import com.picpic.sikkle.utils.filter.IFInkwellFilter;
import com.picpic.sikkle.utils.filter.IFLomoFilter;
import com.picpic.sikkle.utils.filter.IFNashvilleFilter;
import com.picpic.sikkle.utils.filter.IFSierraFilter;
import com.picpic.sikkle.utils.filter.IFToasterFilter;
import com.picpic.sikkle.utils.filter.IFValenciaFilter;
import com.picpic.sikkle.utils.filter.IFWaldenFilter;
import com.picpic.sikkle.utils.filter.IFXprollFilter;
import com.picpic.sikkle.widget.DrawView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageRenderer;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.PixelBuffer;
import jp.co.cyberagent.android.gpuimage.Rotation;
import pl.droidsonroids.gif.GifDrawable;

public class GIFCameraEdit1Activity extends Activity implements View.OnClickListener {

    private static final int INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED = 11112;
    private static final int INIT_VIEWS_EDITOR_1_FILLTER = 11114;
    GPUImageView imvPlayer;
    SeekBar seekSpeed, seekFrame;
    ImageView imvWater, imvPart, imvText;
    LinearLayout layWater, layFilter, layControll, layReserve, layPlus, layControllerWhole, layFilterWhole, layNext, layBack, layTop, layBottom, layText;
    LinearLayout layGuide, layGuide2, layGuide3, layGuide2_1, layGuide2_2, layGuide3_1, layGuide3_2;
    TextView tvGuide2, tvGuide3, tvBtn;
    ImageView imvGuide1, imvGuide3;

    int[] frameValuese = {
            6, 4, 3
    };
    int[] frameValueseGIF = {
            6, 4, 3, 1
    };
    int[] speedValuese = {
            5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
    };
    FilterType[] ff = {
            FilterType.I_AMARO, FilterType.I_AMARO, FilterType.I_HEFE, FilterType.I_NASHVILLE,
            FilterType.I_SIERRA, FilterType.I_INKWELL, FilterType.I_VALENCIA,
            FilterType.I_WALDEN, FilterType.I_XPROII, FilterType.BRIGHTNESS,
            FilterType.I_BRANNAN, FilterType.I_EARLYBIRD, FilterType.I_HUDSON,
            FilterType.I_LOMO, FilterType.I_TOASTER,
            FilterType.CONTRAST, FilterType.VIGNETTE, FilterType.TONE_CURVE,
            FilterType.LOOKUP_AMATORKA
    };
    ArrayList<LinearLayout> layFilters;
    ArrayList<ImageView> gpus;

    BackPressCloseHandler backPressCloseHandler;
    ExecutorService executor = Executors.newFixedThreadPool(1);

    int guideNavi = 0, viewNavi = 0;
    boolean isFilterSetting = false, isPlaying = false, first = false;
    ArrayList<Bitmap> playArray;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gifcamera_edit1);

        backPressCloseHandler = new BackPressCloseHandler(this);

        layWater = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_water_mark);
        layWater.setOnClickListener(this);

        layControll = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor2_control);
        layControll.setOnClickListener(this);
        layReserve = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor2_reverse);
        layReserve.setOnClickListener(this);
        layFilter = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor2_filter);
        layFilter.setOnClickListener(this);
        layPlus = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor2_plus);
        layPlus.setOnClickListener(this);
        layText = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor2_text);
        layText.setOnClickListener(this);
        layNext = (LinearLayout) findViewById(R.id.imv_gif_camera_edit1_film_next);
        layNext.setOnClickListener(this);
        layNext.setSelected(true);
        layBack = (LinearLayout) findViewById(R.id.imv_gif_camera_edit1_film_back);
        layBack.setOnClickListener(this);

        layControllerWhole = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_editor_btns_values);
        layFilterWhole = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter);

        imvGuide1 = (ImageView) findViewById(R.id.imv_gif_camera_edit1_guide);
        imvGuide3 = (ImageView) findViewById(R.id.imv_gif_camera_edit1_guide3);

        layGuide = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide);
        layGuide2 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_2);
        layGuide3 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_3);

        layGuide2_1 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_touch_2_1);
        layGuide2_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide2_2 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_touch_2_2);
        layGuide2_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide3_1 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_touch_3_1);
        layGuide3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide3_2 = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_guide_touch_3_2);
        layGuide3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvGuide2 = (TextView) findViewById(R.id.tv_gif_camera_edit1_tu_ment2);
        tvGuide3 = (TextView) findViewById(R.id.tv_gif_camera_edit1_tu_ment3);
        tvBtn = (TextView) findViewById(R.id.tv_gif_camera_edit1_guide_btn);

        if (AppController.tagNavi == 2) {
            initGuide(2);
        }

        try {
            GifDrawable gdGuide1 = new GifDrawable(getResources().getAssets().open("tu_touch_1.gif"));
            gdGuide1.setLoopCount(0);
            gdGuide1.setSpeed(2.0f);
            imvGuide1.setImageDrawable(gdGuide1);

            GifDrawable gdGuide3 = new GifDrawable(getResources().getAssets().open("tu_slide.gif"));
            gdGuide3.setLoopCount(0);
            gdGuide3.setSpeed(2.0f);
            imvGuide3.setImageDrawable(gdGuide3);

        } catch (IOException e) {
            e.printStackTrace();
        }

        imvWater = (ImageView) findViewById(R.id.imv_gif_camera_edit1_water_mark);

        imvPart = (ImageView) findViewById(R.id.imv_gif_camera_edit1_part);
        imvText = (ImageView) findViewById(R.id.imv_gif_camera_edit1_text);

        if (AppController.isText) {
            imvText.setVisibility(View.VISIBLE);
            imvText.setImageBitmap(AppController.stvBitmap);
        } else {
            imvText.setVisibility(View.GONE);
        }

        imvPlayer = (GPUImageView) findViewById(R.id.imv_gif_camera_edit1_player);

        if (AppController.isPart) {

            imvPart.setImageBitmap(AppController.dvBitmap);

            imvPart.setVisibility(View.VISIBLE);
        } else {
            imvPart.setVisibility(View.GONE);
        }

        seekSpeed = (SeekBar) findViewById(R.id.sb_gif_camera_edit1_editor_play_speed);
        seekFrame = (SeekBar) findViewById(R.id.sb_gif_camera_edit1_editor_frame_control);

        seekSpeed.setProgress(returnSpeed(AppController.speedNavi));
        seekFrame.setProgress(returnFrame(AppController.frameNavi));

        if (AppController.isLoadGIF) {
            seekFrame.setMax(3);
            seekFrame.setProgress(3);
            AppController.frameNavi = 1;
        }

        seekSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                AppController.speedNavi = speedValuese[seekBar.getProgress()];
                startPlay();
            }
        });

        seekFrame.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (AppController.tagNavi == 2) {
                    if (seekBar.getProgress() == 0) {
                        initGuide(3);
                    }
                }

                if (AppController.isLoadGIF) {
                    AppController.frameNavi = frameValueseGIF[seekBar.getProgress()];
                } else {
                    AppController.frameNavi = frameValuese[seekBar.getProgress()];
                }
                startPlay();
            }
        });

        layFilters = new ArrayList<>();

        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_1));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_2));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_3));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_4));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_5));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_6));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_7));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_8));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_9));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_10));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_11));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_12));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_13));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_14));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_15));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_16));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_17));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_18));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_edit1_filter_19));

        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setBackgroundColor(0x00000000);
        }

        layFilters.get(AppController.filterIndex).setBackgroundColor(0xff697fff);

        gpus = new ArrayList<>();

        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_1));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_2));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_3));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_4));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_5));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_6));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_7));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_8));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_9));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_10));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_11));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_12));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_13));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_14));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_15));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_16));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_17));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_18));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_edit1_19));

        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setOnClickListener(this);
        }

        layFilters.get(0).setSelected(true);

//        switchFilterTo(AppController.mFilter);

//        imvPlayer.setFilter(AppController.mFilter);
//        imvPlayer.requestRender();

        layTop = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_top);
        layBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_edit1_bottom);

        if (AppController.is11) {
            layTop.setVisibility(View.VISIBLE);
            layBottom.setVisibility(View.VISIBLE);
        } else {
            layTop.setVisibility(View.GONE);
            layBottom.setVisibility(View.GONE);
        }

        try {
            if (AppController.sectionArr.size() <= 0) {
                AppController.sectionArr = new ArrayList<>();
                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    int[] tempI = {0, AppController.editFrames.get(i).size() - 1};
                    AppController.sectionArr.add(tempI);
                }

                Log.e("sectionArr", AppController.sectionArr.size() + "");

                for (int i = 0; i < AppController.sectionArr.size(); i++) {
                    Log.e("sectionArr", AppController.sectionArr.get(i)[0] + "/" + AppController.sectionArr.get(i)[1]);
                }
            }
        } catch (NullPointerException e) {
            AppController.sectionArr = new ArrayList<>();
            for (int i = 0; i < AppController.editFrames.size(); i++) {
                int[] tempI = {0, AppController.editFrames.get(i).size() - 1};
                AppController.sectionArr.add(tempI);
            }
            Log.e("sectionArr", AppController.sectionArr.size() + "");
            for (int i = 0; i < AppController.sectionArr.size(); i++) {
                Log.e("sectionArr", AppController.sectionArr.get(i)[0] + "/" + AppController.sectionArr.get(i)[1]);
            }
        } finally {
            changeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        AppController.initFilter(GIFCameraEdit1Activity.this, imvPlayer);
        if (AppController.mFilter != null) {
            imvPlayer.setFilter(AppController.mFilter);
            imvPlayer.requestRender();
        }
    }

    private int returnSpeed(int a) {
        for (int i = 0; i < speedValuese.length; i++) {
            if (speedValuese[i] == a) {
                return i;
            }
        }
        return -1;
    }

    private int returnFrame(int a) {
        for (int i = 0; i < frameValuese.length; i++) {
            if (frameValuese[i] == a) {
                return i;
            }
        }
        return -1;
    }

    private void initGuide(int a) {
        guideNavi = a;
        switch (a) {
            case 2:
                imvGuide1.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.VISIBLE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.GONE);
                layGuide3.setVisibility(View.VISIBLE);
                tvBtn.setVisibility(View.GONE);
                tvGuide3.setText(getResources().getString(R.string.tu_camera_ment_3));
                break;
            case 3:
                imvGuide1.setVisibility(View.VISIBLE);
                imvGuide3.setVisibility(View.GONE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.VISIBLE);
                layGuide3.setVisibility(View.GONE);
                tvBtn.setVisibility(View.VISIBLE);
                tvBtn.setText(getResources().getString(R.string.camera_complete));
                tvGuide2.setText(getResources().getString(R.string.tu_camera_ment_4));
                break;
            default:
                imvGuide1.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.GONE);
                layGuide.setVisibility(View.GONE);
                layGuide2.setVisibility(View.GONE);
                layGuide3.setVisibility(View.GONE);
                tvGuide2.setVisibility(View.GONE);
                tvGuide3.setVisibility(View.GONE);
                tvBtn.setVisibility(View.GONE);
                break;
        }

    }

    public static Bitmap getBitmapWithFilterApplied(final Bitmap bitmap) {

        GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
        renderer.setRotation(Rotation.NORMAL,
                renderer.isFlippedHorizontally(), renderer.isFlippedVertically());
        renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
        PixelBuffer buffer = new PixelBuffer(bitmap.getWidth(), bitmap.getHeight());
        buffer.setRenderer(renderer);
        renderer.setImageBitmap(bitmap, false);
        Bitmap result = buffer.getBitmap();
        renderer.deleteImage();
        buffer.destroy();

        return result;
    }

    private void initFilterLays(int a) {
        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setSelected(false);
        }
        layFilters.get(a).setSelected(true);

        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setBackgroundColor(0x00000000);
        }
        AppController.filterIndex = a;

        layFilters.get(AppController.filterIndex).setBackgroundColor(0xff697fff);

        AppController.initFilter(GIFCameraEdit1Activity.this, imvPlayer);

        if (AppController.isPart) {

//            dv.setmBitmap(AppController.dvOriginBitmap);
//            dv.reset(AppController.dvOriginBitmap);
//            dv.setPaths(AppController.dvPaths);
//
//            dv.invalidate();

            imvPart.setImageBitmap(AppController.getBitmapWithFilterApplied(AppController.dvOriginBitmap));

            imvPart.setVisibility(View.VISIBLE);

        } else {
            imvPart.setVisibility(View.GONE);
        }

//        Bitmap tempB = AppController.dvBitmap;

//        if (AppController.isPart) {
//
////            int w = AppController.originFrames.get(0).get(0).getbitmap().getWidth();
////            int h = AppController.originFrames.get(0).get(0).getbitmap().getHeight();
//
//            GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
//            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
//            PixelBuffer buffer = new PixelBuffer(tempB.getWidth(), tempB.getHeight());
//            buffer.setRenderer(renderer);
//            renderer.setImageBitmap(tempB, false);
//            tempB = buffer.getBitmap();
//            imvPart.setImageBitmap(tempB);
//            imvPart.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            renderer.deleteImage();
//            buffer.destroy();
//
//        }

    }

    private void startPlay() {
        try {
            isPlaying = false;

            playArray = new ArrayList<>();

            int duration = 1000 / AppController.speedNavi;

            if (!AppController.isReverse) {
                Log.e("whSize1", AppController.editFrames.size() + "");
                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    Log.e("whSize2_" + i, AppController.editFrames.get(i).size() + "");
                }

                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    Log.e("i", i + "");
                    for (int j = AppController.sectionArr.get(i)[0]; j <= AppController.sectionArr.get(i)[1]; j = j + AppController.frameNavi) {
                        Log.e("sectionArrrrrrrr", AppController.sectionArr.get(i)[0] + " _ " + AppController.sectionArr.get(i)[1]);
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }

            } else {
                for (int i = AppController.editFrames.size() - 1; i >= 0; i--) {
                    for (int j = AppController.sectionArr.get(i)[1]; j >= AppController.sectionArr.get(i)[0]; j = j - AppController.frameNavi) {
                        Log.e("sectionArrrrrrrr", AppController.sectionArr.get(i)[0] + " _ " + AppController.sectionArr.get(i)[1]);
                        //아웃오브 인덱스처리
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }
            }

            imvPlayer.setImageArray(playArray, duration);

            isPlaying = true;
        } catch (Exception e) {

        }
    }


    private void filterSetting() {
        if (!isFilterSetting) {
            Bitmap b = AppController.editFrames.get(0).get(0).getbitmap();
            if (b.getWidth() > 480) {
                if (AppController.is11) {
                    b = Bitmap.createScaledBitmap(b, 480, 480, false);
                } else {
                    b = Bitmap.createScaledBitmap(b, 480, 640, false);
                }
            }

            final Bitmap tempB = b;
            for (int i = 0; i < ff.length; i++) {
                final int finalI = i;
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        if (finalI != 0) {
                            GPUImageRenderer renderer = new GPUImageRenderer(createFilterForType(GIFCameraEdit1Activity.this, ff[finalI]));
                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                            PixelBuffer buffer = new PixelBuffer(tempB.getWidth(), tempB.getHeight());
                            buffer.setRenderer(renderer);
                            renderer.setImageBitmap(tempB, false);
                            bitmap = buffer.getBitmap();
                            renderer.deleteImage();
                            buffer.destroy();
                        } else {
                            bitmap = tempB;
                        }
                        final Bitmap finalBitmap = bitmap;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gpus.get(finalI).setImageBitmap(finalBitmap);
                            }
                        });

                    }
                });


            }
            isFilterSetting = true;
        }


    }

    private GPUImageFilter createFilterForType(final Context context, final FilterType type) {
        switch (type) {
            case CONTRAST:
                return new GPUImageContrastFilter(2.0f);
            case BRIGHTNESS:
                return new GPUImageBrightnessFilter(0.1f);
            case VIGNETTE:
                PointF centerPoint = new PointF();
                centerPoint.x = 0.5f;
                centerPoint.y = 0.5f;
                return new GPUImageVignetteFilter(centerPoint, new float[]{0.0f, 0.0f, 0.0f}, 0.3f, 0.75f);
            case TONE_CURVE:
                GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
                toneCurveFilter.setFromCurveFileInputStream(
                        context.getResources().openRawResource(R.raw.tone_cuver_sample));
                return toneCurveFilter;
            case LOOKUP_AMATORKA:
                GPUImageLookupFilter amatorka = new GPUImageLookupFilter();
                amatorka.setBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.lookup_amatorka));
                return amatorka;
            case I_AMARO:
                return new IFAmaroFilter(context);
            case I_BRANNAN:
                return new IFBrannanFilter(context);
            case I_EARLYBIRD:
                return new IFEarlybirdFilter(context);
            case I_HEFE:
                return new IFHefeFilter(context);
            case I_HUDSON:
                return new IFHudsonFilter(context);
            case I_INKWELL:
                return new IFInkwellFilter(context);
            case I_LOMO:
                return new IFLomoFilter(context);
            case I_NASHVILLE:
                return new IFNashvilleFilter(context);
            case I_SIERRA:
                return new IFSierraFilter(context);
            case I_TOASTER:
                return new IFToasterFilter(context);
            case I_VALENCIA:
                return new IFValenciaFilter(context);
            case I_WALDEN:
                return new IFWaldenFilter(context);
            case I_XPROII:
                return new IFXprollFilter(context);

            default:
                throw new IllegalStateException("No filter of that type!");
        }

    }

    @Override
    public void onBackPressed() {
        if (viewNavi == INIT_VIEWS_EDITOR_1_FILLTER) {
            changeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
        } else {
            if (AppController.tagNavi == 2) {
                backPressCloseHandler.onBackPressed();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        try {
            super.onWindowFocusChanged(hasFocus);

            int width2 = AppController.editFrames.get(0).get(0).getbitmap().getWidth();
            int height2 = AppController.editFrames.get(0).get(0).getbitmap().getHeight();

            int w = 0, h = 0, margin = 0;
            int lw = (int) (MinUtils.density * 59);
            int lh = (int) (MinUtils.density * 23);
            FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) layWater.getLayoutParams();

            if (height2 >= width2) {
                w = width2 * imvPlayer.getHeight() / height2;
                margin = (imvPlayer.getWidth() - w) / 2;
                p.rightMargin = margin;
            } else {
                h = height2 * imvPlayer.getWidth() / width2;
                margin = (imvPlayer.getHeight() - h) / 2;
                p.bottomMargin = margin;
            }

            Log.e("!asd!", MinUtils.screenWidth + "/" + MinUtils.screenHeight + "/" + w + "/" + h + "/" + margin + "/" + imvPlayer.getWidth() + "/" + imvPlayer.getHeight());

            layWater.setLayoutParams(p);

        } catch (Exception e) {

        }
    }

    private void changeViews(int a) {

        viewNavi = a;
        switch (a) {
            case INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED:
                layControll.setSelected(true);
                layFilter.setSelected(false);
                layControllerWhole.setVisibility(View.VISIBLE);
                layFilterWhole.setVisibility(View.GONE);

                startPlay();

                break;
            case INIT_VIEWS_EDITOR_1_FILLTER:
                layControll.setSelected(false);
                layFilter.setSelected(true);
                layControllerWhole.setVisibility(View.GONE);
                layFilterWhole.setVisibility(View.VISIBLE);
                filterSetting();

                startPlay();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_gif_camera_edit1_editor2_control:
                if (!layControll.isSelected()) {
                    changeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
                }
                break;
            case R.id.lay_gif_camera_edit1_editor2_reverse:
                if (!layReserve.isSelected()) {
                    layReserve.setSelected(true);
                    AppController.isReverse = true;
                    startPlay();
                } else {
                    layReserve.setSelected(false);
                    AppController.isReverse = false;
                    startPlay();
                }
                break;
            case R.id.lay_gif_camera_edit1_editor2_text:
                startActivity(new Intent(GIFCameraEdit1Activity.this, GIFCameraWriteActivity.class));
                finish();
                overridePendingTransition(0, 0);
            case R.id.lay_gif_camera_edit1_water_mark:
                switch (AppController.waterNavi) {
                    case 0:
                        AppController.waterNavi = 1;
                        imvWater.setImageResource(R.drawable.new_water_mark);
                        break;
                    case 1:
                        AppController.waterNavi = 0;
                        imvWater.setImageResource(R.drawable.new_water_mark_c);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;
                    case 8:

                        break;
                    case 9:

                        break;
                }
                break;
            case R.id.lay_gif_camera_edit1_editor2_filter:
                if (!layFilter.isSelected()) {
                    changeViews(INIT_VIEWS_EDITOR_1_FILLTER);
                }
                break;
            case R.id.lay_gif_camera_edit1_editor2_plus:
                Intent editIntent = new Intent(GIFCameraEdit1Activity.this, GIFCameraEdit2Activity.class);
//                startActivity(editIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                startActivity(editIntent);
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_gif_camera_edit1_filter_1:
                initFilterLays(0);
                break;
            case R.id.lay_gif_camera_edit1_filter_2:
                initFilterLays(1);
                break;
            case R.id.lay_gif_camera_edit1_filter_3:
                initFilterLays(2);
                break;
            case R.id.lay_gif_camera_edit1_filter_4:
                initFilterLays(3);
                break;
            case R.id.lay_gif_camera_edit1_filter_5:
                initFilterLays(4);
                break;
            case R.id.lay_gif_camera_edit1_filter_6:
                initFilterLays(5);
                break;
            case R.id.lay_gif_camera_edit1_filter_7:
                initFilterLays(6);
                break;
            case R.id.lay_gif_camera_edit1_filter_8:
                initFilterLays(7);
                break;
            case R.id.lay_gif_camera_edit1_filter_9:
                initFilterLays(8);
                break;
            case R.id.lay_gif_camera_edit1_filter_10:
                initFilterLays(9);
                break;
            case R.id.lay_gif_camera_edit1_filter_11:
                initFilterLays(10);
                break;
            case R.id.lay_gif_camera_edit1_filter_12:
                initFilterLays(11);
                break;
            case R.id.lay_gif_camera_edit1_filter_13:
                initFilterLays(12);
                break;
            case R.id.lay_gif_camera_edit1_filter_14:
                initFilterLays(13);
                break;
            case R.id.lay_gif_camera_edit1_filter_15:
                initFilterLays(14);
                break;
            case R.id.lay_gif_camera_edit1_filter_16:
                initFilterLays(15);
                break;
            case R.id.lay_gif_camera_edit1_filter_17:
                initFilterLays(16);
                break;
            case R.id.lay_gif_camera_edit1_filter_18:
                initFilterLays(17);
                break;
            case R.id.lay_gif_camera_edit1_filter_19:
                initFilterLays(18);
                break;
            case R.id.imv_gif_camera_edit1_film_next:

                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Film Complete Click").build());
                if (AppController.tagNavi == 2) {
                    initGuide(3);
                }

                final ProgressDialog pd = new ProgressDialog(this);
                pd.setCanceledOnTouchOutside(false);
                pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pd.show();

                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Frame> tempArrr = new ArrayList<>();
                        int wholeSize = 0;
                        for (int q = 0; q < AppController.editFrames.size(); q++) {
                            for (int w = 0; w < AppController.editFrames.get(q).size(); w++) {
                                wholeSize += 1;
                                tempArrr.add(AppController.editFrames.get(q).get(w));
                            }
                        }
                        Log.e("size", wholeSize + "");

                        int pdRatio = 0;
                        try {
                            pdRatio = 100 / (wholeSize / AppController.frameNavi);
                        } catch (ArithmeticException e) {
                            return;
                        }
                        pd.setMax(100);

                        QAGIFEncoder encoder2 = new QAGIFEncoder();
                        encoder2.setRepeat(0);
                        encoder2.setDelay(1000 / AppController.speedNavi);

//                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
//
//                        MinAnimationGifEncoder encoder = new MinAnimationGifEncoder();
//                        encoder.start(bos);

                        try {
                            encoder2.start(AppController.cameraPath2);
//                            encoder2.start(AppController.BASE_SRC + "test.gif");
                            int i = 0;
                            if (!AppController.isReverse) {
                                Log.e("size1", AppController.editFrames.size() + "");
                                for (int q = 0; q < AppController.editFrames.size(); q++) {
                                    Log.e("section" + q, AppController.sectionArr.get(q)[0] + "/" + AppController.sectionArr.get(q)[1]);
                                    for (int w = AppController.sectionArr.get(q)[0]; w <= AppController.sectionArr.get(q)[1]; w += AppController.frameNavi) {
                                        Frame f = AppController.editFrames.get(q).get(w);
                                        Timer t = new Timer();

                                        QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                        opt2.inSampleSize = 1;

                                        if (q == 0) {
                                            if (w == 0) {
                                                AppController.cameraThumbFile = f.getFile();
                                            }
                                        }

                                        Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

                                        int b2w = b2.getWidth();

                                        if (AppController.is11) {
                                            b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
                                        }

                                        if (AppController.mFilter != new GPUImageFilter()) {
                                            GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
                                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                            PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                            buffer.setRenderer(renderer);
                                            renderer.setImageBitmap(b2, false);
                                            b2 = buffer.getBitmap();
                                            renderer.deleteImage();
                                            buffer.destroy();
                                        }

                                        if (AppController.isPart) {

                                            Bitmap firstB = AppController.dvBitmap;

                                            firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                            b2 = AppController.overlayPart(b2, firstB);

                                        }

                                        if (AppController.waterNavi != 0) {
                                            Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                            bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                            b2 = AppController.overlayMark(b2, bb);
                                        }


                                        if (AppController.isText) {
                                            b2 = AppController.overlayMark(b2, AppController.stvBitmap);
                                        }

                                        Log.e("size2", b2.getWidth() + " / " + b2.getHeight());

                                        t.logDelay("createBitmap");
                                        encoder2.addFrame(b2);
                                        t.logDelay("addFrame");
                                        if (!pd.isShowing()) {
                                            break;
                                        }
                                        i = i + pdRatio;
                                        pd.setProgress(i);
                                    }
                                }
                            } else {
                                for (int q = AppController.editFrames.size() - 1; q >= 0; q--) {
                                    for (int w = AppController.sectionArr.get(q)[1]; w >= AppController.sectionArr.get(q)[0]; w = w - AppController.frameNavi) {
                                        Frame f = AppController.editFrames.get(q).get(w);
                                        Timer t = new Timer();

                                        QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                        opt2.inSampleSize = 1;

                                        if (q == AppController.editFrames.size() - 1) {
                                            if (w == AppController.sectionArr.get(q)[1]) {
                                                AppController.cameraThumbFile = f.getFile();
                                            }
                                        }

                                        Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

                                        int b2w = b2.getWidth();

                                        if (AppController.is11) {
                                            b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
                                        }


                                        if (AppController.mFilter != new GPUImageFilter()) {
                                            GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
                                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                            PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                            buffer.setRenderer(renderer);
                                            renderer.setImageBitmap(b2, false);
                                            b2 = buffer.getBitmap();
                                            renderer.deleteImage();
                                            buffer.destroy();
                                        }

                                        if (AppController.isPart) {
                                            Bitmap firstB = AppController.dvBitmap;
                                            firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                            b2 = AppController.overlayPart(b2, firstB);

                                        }

                                        if (AppController.waterNavi != 0) {
                                            Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                            bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                            b2 = AppController.overlayMark(b2, bb);
                                        }

                                        if (AppController.isText) {
                                            b2 = AppController.overlayMark(b2, AppController.stvBitmap);
                                        }

                                        Log.e("size2", b2.getWidth() + " / " + b2.getHeight());

                                        t.logDelay("createBitmap");
                                        encoder2.addFrame(b2);
                                        t.logDelay("addFrame");
                                        if (!pd.isShowing()) {
                                            break;
                                        }
                                        i = i + pdRatio;
                                        pd.setProgress(i);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (pd.isShowing()) {
                            encoder2.finish();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (pd.isShowing()) {
                                    pd.dismiss();
                                    Intent data = new Intent(GIFCameraEdit1Activity.this, WritePostActivityV1.class);
                                    data.putExtra("url2", AppController.cameraPath2);
                                    try {
                                        data.putExtra("thumb", AppController.cameraThumbFile.getAbsolutePath());
                                    } catch (NullPointerException e) {
                                    }
                                    data.putExtra("navi", AppController.tagNavi);
                                    if (AppController.tagNavi == 1) {
                                        data.putExtra("tag_id", AppController.tag_id);
                                        data.putExtra("tag_title", AppController.tag_title);
                                        data.putExtra("tag_body", AppController.tag_body);
                                    }
                                    startActivity(data);
                                    finish();
                                    try {
                                        GIFCameraFilmActivity.ac.finish();
                                    } catch (NullPointerException e) {

                                    }
                                }
                            }
                        });
                    }
                });
                break;
            case R.id.imv_gif_camera_edit1_film_back:
                if (AppController.tagNavi == 2) {
                    backPressCloseHandler.onBackPressed();
                } else {
                    finish();
                }
                break;
        }
    }

    public class BackPressCloseHandler {

        private long backKeyPressedTime = 0;
        private Toast toast;

        private Activity activity;

        public BackPressCloseHandler(Activity context) {
            this.activity = context;
        }

        public void onBackPressed() {
            if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                backKeyPressedTime = System.currentTimeMillis();
                showGuide();
                return;
            }
            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                GIFCameraFilmActivity.ac.finish();
                activity.finish();
//                startActivity(new Intent(GIFCameraActivity.this, NewGridTimelineActivity.class));
                Intent i = new Intent(GIFCameraEdit1Activity.this, TimelineV1NewActivity.class);
                i.putExtra("navi", 0);
                startActivity(i);
                toast.cancel();
            }
        }


        private void showGuide() {
            toast = Toast.makeText(activity, "튜토리얼을 끝내야 PicPic을 시작할수 있어요. \'뒤로\'버튼을 한번 더 누르시면 튜토리얼이 종료됩니다.",
                    Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    enum FilterType {
        I_AMARO, I_HEFE, I_NASHVILLE,
        I_SIERRA, I_INKWELL, I_VALENCIA,
        I_WALDEN, I_XPROII, BRIGHTNESS,
        I_BRANNAN, I_EARLYBIRD, I_HUDSON,
        I_LOMO, I_TOASTER,
        CONTRAST, VIGNETTE, TONE_CURVE,
        LOOKUP_AMATORKA
    }


    static class Timer {
        long lastTime = System.currentTimeMillis();

        void logDelay(String tag) {
            long current = System.currentTimeMillis();
            Log.w("Timer", String.format("delay(%s): %d", tag, (current - lastTime)));
            lastTime = current;
        }
    }

}
