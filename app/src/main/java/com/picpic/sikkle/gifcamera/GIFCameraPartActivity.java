package com.picpic.sikkle.gifcamera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.widget.DrawView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class GIFCameraPartActivity extends Activity implements View.OnClickListener {

    GPUImageView imvPlayer;
    LinearLayout layWater, layBack, layUndo, layInit, layO, layX;
    DrawView dv;

    boolean isPlaying = false, first = false;
    ArrayList<Bitmap> playArray;
    Bitmap tempBitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gifcamera_part);

        imvPlayer = (GPUImageView) findViewById(R.id.imv_gif_camera_part_player);

        layWater = (LinearLayout) findViewById(R.id.lay_gif_camera_part_water_mark);
        layBack = (LinearLayout) findViewById(R.id.imv_gif_camera_part_film_back);
        layUndo = (LinearLayout) findViewById(R.id.lay_gif_camera_part_part_undo);
        layInit = (LinearLayout) findViewById(R.id.lay_gif_camera_part_part_init);
        layO = (LinearLayout) findViewById(R.id.lay_gif_camera_part_part_bottom_o);
        layX = (LinearLayout) findViewById(R.id.lay_gif_camera_part_part_bottom_x);

        layWater.setOnClickListener(this);
        layBack.setOnClickListener(this);
        layUndo.setOnClickListener(this);
        layInit.setOnClickListener(this);
        layO.setOnClickListener(this);
        layX.setOnClickListener(this);

        dv = (DrawView) findViewById(R.id.dv_gif_camera_part_part);

        imvPlayer.setFilter(AppController.mFilter);
        imvPlayer.requestRender();

        AppController.initFilter(GIFCameraPartActivity.this, imvPlayer);

        startPlay();

    }

    @Override
    protected void onDestroy() {
//        dv.recycle();
        tempBitmap.recycle();
        tempBitmap = null;
        super.onDestroy();
    }

    private void startPlay() {
        try{
            isPlaying = false;

            playArray = new ArrayList<>();

            int duration = 1000 / AppController.speedNavi;

            if (!AppController.isReverse) {

                Log.e("whSize1", AppController.editFrames.size() + "");

                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    for (int j = AppController.sectionArr.get(i)[0]; j <= AppController.sectionArr.get(i)[1]; j = j + AppController.frameNavi) {
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }

            } else {
                for (int i = AppController.editFrames.size() - 1; i >= 0; i--) {
                    for (int j = AppController.sectionArr.get(i)[1]; j >= AppController.sectionArr.get(i)[0]; j = j - AppController.frameNavi) {
                        //아웃오브 인덱스처리
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }
            }

            imvPlayer.setImageArray(playArray, duration);

            isPlaying = true;
        }catch (Exception e){

        }
    }

    private boolean isWidth(Bitmap b) {
        int width = b.getWidth();
        int height = b.getHeight();
        if (width > height) {
            return true;
        } else {
            return false;
        }
    }

    private boolean copyFile(File file, String save_file) {
        boolean result;
        if (file != null && file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                FileOutputStream newfos = new FileOutputStream(save_file);
                int readcount = 0;
                byte[] buffer = new byte[1024];
                while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
                    newfos.write(buffer, 0, readcount);
                }
                newfos.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    @Override
    public void onClick(View v) {
        File f;
        Bitmap b;
        switch (v.getId()) {
            case R.id.lay_gif_camera_part_water_mark:

                break;
            case R.id.imv_gif_camera_part_film_back:
                onBackPressed();
                break;
            case R.id.lay_gif_camera_part_part_undo:
                if (AppController.isReverse) {
                    Frame frame = AppController.editFrames.get(AppController.editFrames.size() - 1).get(AppController.editFrames.get(AppController.editFrames.size() - 1).size() - 1);
                    b = frame.getbitmap();
                } else {
                    Frame frame = AppController.editFrames.get(0).get(0);
                    b = frame.getbitmap();
                }
                Bitmap scaleB = null;

                if (isWidth(b)) {
                    int width = imvPlayer.getWidth();
                    int height = b.getHeight() * imvPlayer.getWidth() / b.getWidth();
                    scaleB = Bitmap.createScaledBitmap(b, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = b.getWidth() * imvPlayer.getHeight() / b.getHeight();
                    scaleB = Bitmap.createScaledBitmap(b, width, height, false);
                }

                scaleB = AppController.getBitmapWithFilterApplied(scaleB);
                dv.onClickUndo(scaleB);
                break;
            case R.id.lay_gif_camera_part_part_init:
                if (AppController.isReverse) {
                    Frame frame = AppController.editFrames.get(AppController.editFrames.size() - 1).get(AppController.editFrames.get(AppController.editFrames.size() - 1).size() - 1);
                    AppController.dvBitmap = frame.getbitmap();
                } else {
                    Frame frame = AppController.editFrames.get(0).get(0);
                    AppController.dvBitmap = frame.getbitmap();
                }
                Bitmap scaleB2 = null;

                if (isWidth(AppController.dvBitmap)) {
                    int width = imvPlayer.getWidth();
                    int height = AppController.dvBitmap.getHeight() * imvPlayer.getWidth() / AppController.dvBitmap.getWidth();
                    scaleB2 = Bitmap.createScaledBitmap(AppController.dvBitmap, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = AppController.dvBitmap.getWidth() * imvPlayer.getHeight() / AppController.dvBitmap.getHeight();
                    scaleB2 = Bitmap.createScaledBitmap(AppController.dvBitmap, width, height, false);
                }

                scaleB2 = AppController.getBitmapWithFilterApplied(scaleB2);
                dv.reset(scaleB2);
                dv.invalidate();
                break;
            case R.id.lay_gif_camera_part_part_bottom_o:
                Log.e("dvSize", dv.getPaths().size() + "");
                Log.e("dvWH", dv.getmBitmap().getWidth() + "/" + dv.getmBitmap().getHeight());
                if (dv.getPaths().size() != 0) {
                    try{
                        dv.setmBitmap(tempBitmap);

                        AppController.isPart = true;
                        AppController.dvPaths = dv.getPaths();
//                    dv.setPaths(AppController.dvPaths);

                        int width = 0, height = 0;

                        if (isWidth(dv.getmBitmap())) {
                            width = imvPlayer.getWidth();
                            height = dv.getmBitmap().getHeight() * imvPlayer.getWidth() / dv.getmBitmap().getWidth();
                            scaleB2 = Bitmap.createScaledBitmap(dv.getmBitmap(), width, height, false);
                        } else {
                            height = imvPlayer.getHeight();
                            width = dv.getmBitmap().getWidth() * imvPlayer.getHeight() / dv.getmBitmap().getHeight();
                            scaleB2 = Bitmap.createScaledBitmap(dv.getmBitmap(), width, height, false);
                        }

                        AppController.playerWidth = width;
                        AppController.playerHeight = height;

                        AppController.dvOriginBitmap = scaleB2;

                        AppController.dvBitmap = AppController.getBitmapWithFilterApplied(scaleB2);
                    }catch (RuntimeException e){

                    }
                } else {
                    AppController.isPart = false;
                }
                onBackPressed();
                break;
            case R.id.lay_gif_camera_part_part_bottom_x:
                AppController.isPart = false;
                onBackPressed();
                break;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!first) {
            Bitmap scaleB2 = null;
            try {
                super.onWindowFocusChanged(hasFocus);

                int width1 = AppController.editFrames.get(0).get(0).getbitmap().getWidth();
                int height1 = AppController.editFrames.get(0).get(0).getbitmap().getHeight();

                int w = 0, h = 0, margin = 0;
                int lw = (int) (MinUtils.density * 59);
                int lh = (int) (MinUtils.density * 23);
                FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) layWater.getLayoutParams();

                if (height1 >= width1) {
                    w = width1 * imvPlayer.getHeight() / height1;
                    margin = (imvPlayer.getWidth() - w) / 2;
                    p.rightMargin = margin;
                } else {
                    h = height1 * imvPlayer.getWidth() / width1;
                    margin = (imvPlayer.getHeight() - h) / 2;
                    p.bottomMargin = margin;
                }

//            Log.e("!asd!", MinUtils.screenWidth + "/" + MinUtils.screenHeight + "/" + w + "/" + h + "/" + margin + "/" + imvPlayer.getWidth() + "/" + imvPlayer.getHeight());

                layWater.setLayoutParams(p);

                if (AppController.isReverse) {
                    Frame frame = AppController.editFrames.get(AppController.editFrames.size() - 1).get(AppController.editFrames.get(AppController.editFrames.size() - 1).size() - 1);
                    scaleB2 = frame.getbitmap();

                } else {
                    Frame frame = AppController.editFrames.get(0).get(0);
                    scaleB2 = frame.getbitmap();
                }

                int width = 0, height = 0;

                if (isWidth(scaleB2)) {
                    width = imvPlayer.getWidth();
                    height = scaleB2.getHeight() * imvPlayer.getWidth() / scaleB2.getWidth();
                    scaleB2 = Bitmap.createScaledBitmap(scaleB2, width, height, false);
                } else {
                    height = imvPlayer.getHeight();
                    width = scaleB2.getWidth() * imvPlayer.getHeight() / scaleB2.getHeight();
                    scaleB2 = Bitmap.createScaledBitmap(scaleB2, width, height, false);
                }

                tempBitmap = scaleB2;

                scaleB2 = AppController.getBitmapWithFilterApplied(scaleB2);

                dv.reset(scaleB2);

                dv.setPaths(AppController.dvPaths);

                dv.invalidate();

            } catch (Exception e) {
                Log.e("e", e.toString());
            }finally {
                scaleB2.recycle();
                scaleB2 = null;
            }

            first = true;
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(GIFCameraPartActivity.this, GIFCameraEdit2Activity.class));
        finish();
    }
}
