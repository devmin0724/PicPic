package com.picpic.sikkle.gifcamera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.YuvImage;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.atom.utils.AtomUtils;
import com.facebook.messenger.MessengerThreadParams;
import com.facebook.messenger.MessengerUtils;
import com.fingram.agifEncoder.QAGIFEncoder;
import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.LoadToTempItem;
import com.picpic.sikkle.beans.LoadToTempResult;
import com.picpic.sikkle.beans.StickerData;
import com.picpic.sikkle.ui.NewGridTimelineActivity;
import com.picpic.sikkle.ui.WritePostActivity;
import com.picpic.sikkle.ui.WritePostActivityV1;
import com.picpic.sikkle.ui.tutorial.TutorialCamera6;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.GPUImageFilterTools;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.MyFaceDetectionListener;
import com.picpic.sikkle.utils.PreviewUtils;
import com.picpic.sikkle.utils.filter.IFAmaroFilter;
import com.picpic.sikkle.utils.filter.IFBrannanFilter;
import com.picpic.sikkle.utils.filter.IFEarlybirdFilter;
import com.picpic.sikkle.utils.filter.IFHefeFilter;
import com.picpic.sikkle.utils.filter.IFHudsonFilter;
import com.picpic.sikkle.utils.filter.IFInkwellFilter;
import com.picpic.sikkle.utils.filter.IFLomoFilter;
import com.picpic.sikkle.utils.filter.IFNashvilleFilter;
import com.picpic.sikkle.utils.filter.IFSierraFilter;
import com.picpic.sikkle.utils.filter.IFToasterFilter;
import com.picpic.sikkle.utils.filter.IFValenciaFilter;
import com.picpic.sikkle.utils.filter.IFWaldenFilter;
import com.picpic.sikkle.utils.filter.IFXprollFilter;
import com.picpic.sikkle.widget.DrawView;
import com.picpic.sikkle.widget.MultiSlider;
import com.picpic.sikkle.widget.sticker.StickerView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageRenderer;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.PixelBuffer;
import pl.droidsonroids.gif.GifDrawable;

public class GIFCameraActivity extends Activity implements View.OnClickListener, View.OnTouchListener,
        Camera.PreviewCallback, SurfaceHolder.Callback, GPUImage.OnPictureSavedListener {

    private static final int INIT_VIEWS_SHOOTING = 11111;
    private static final int INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED = 11112;
    private static final int INIT_VIEWS_EDITOR_1_REVERSE = 11113;
    private static final int INIT_VIEWS_EDITOR_1_FILLTER = 11114;
    private static final int INIT_VIEWS_EDITOR_2_DELETE_AND_COPY = 11115;
    private static final int INIT_VIEWS_EDITOR_2_TEXT = 11116;
    private static final int INIT_VIEWS_EDITOR_2_PART_GIF = 11117;
    private static final int PICK_FROM_ALBUM = 1111;
    private static final int CROP_FROM_ALBUM = 1112;
    //    private static final float delay = 37f;
    private static final float delay = 28f;
    private static final float TOTAL = 5000f;
    private static final int REQUEST_PICK_IMAGE = 1;
    //    private static final float TOTAL = 15000f;
    public static Activity ac;
    public static ArrayList<ArrayList<Frame>> wholeFrames;
    //    public static ArrayList<ArrayList<Frame>> tempOriginFrames;
    final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
    protected Camera mCamera;
    protected Camera.Parameters mParameters = null;
    protected Camera.Size mPreviewSize;
    protected int mPreviewFormat;
    ImageView imvBackground, imvReturn, imvNextIcon, imvCompleteIcon;
    //    LinearLayout layGrid, layShot, layTop, imvBack, imvNext, imvGrid, imvFlash,
    //    imvGhost, imvChange, imvCamera, imvPlus, imvRatio, imvSave, imvAlbum,
    //    imvLoad, layReturn, layProgress;
    LinearLayout layGrid, layTop, imvBack, imvNext, imvGrid, imvFlash,
            imvGhost, imvChange, imvCamera, imvPlus, imvRatio, layProgress, imvAlbum;
    FrameLayout layReturn, layShot;
    SurfaceView sfv;
    LinearLayout layBottom, layBtn1, layBtn2;
    LinearLayout layBottom2, layTop2;
    LinearLayout layMiddleBtns, layMiddleSelect, layMiddleValues,
            layBottomBtnsShooting, layBottomEdit1, layBottomEdit2;
    float total = 0.0f;
    ProgressDialog pd2;
    ProgressDialog dialog;
    ArrayList<int[]> sectionArr;
    //    GridView gv;
    //    ImageView imvHandlerArrow, imvHandlerX;
    //    MultiDirectionSlidingDrawer msd;
    //    Uri output1, output2, output3;
    Uri output1, output2;
    boolean isFlashOn = false;
    boolean isChangeingCamera = false;
    java.util.Timer mTimer;
    TimerTask mTaks;
    File f, atomFF;
    int tempViewWidth = 1;
    int colorBefore, colorNow, tagNavi;
    String tag_id, tag_title, tag_body;
    ArrayList<String> tempFolders;
    //    BoundableOfflineImageView imvPlayer;
    GPUImageView imvPlayer;
    SeekBar seekSpeed, seekFrame;
    TextView tvAll;
    boolean isShootingMode = false, isPlaying = false;
    int[] frameValuese = {
            6, 4, 3
    };
    int[] frameValueseGIF = {
            6, 4, 3, 1
    };
    int[] speedValuese = {
            5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
    };
    int frameNavi = 4, speedNavi = 13;
    ArrayList<Frame> tempArrFrames;
    int playFrameNavi = 0;
    AnimationDrawable previewAnimation;
    LinearLayout layEditor2Contorller, layEditor2Reverse, layEditor2Fillter, layEditor2Plus;
    ImageView imvEditor2Contorller, imvEditor2Reverse, imvEditor2Fillter;
    boolean isReverse = false;
    int selectedNavi = 0;
    boolean isSelectAll = false;
    int tempSelectIndex = 0;
    LinearLayout layEditor3X, layEditor3Delete, layEditor3Copy, layEditor3Save, layEditor3Load, layEditor3Text, layEditor3Part;
    ImageView imvEditor3X, imvEditor3Delete, imvEditor3Copy, imvEditor3Save, imvEditor3Load, imvEditor3Text, imvEditor3Part;
    MultiSlider ms;
    //
    Toast toast = null;
    float ratio = delay / TOTAL;
    int w;
    LinearLayout layGuide, layGuideAlpha, layGuide2, layGuide3, layGuide2_1, layGuide2_2, layGuide3_1, layGuide3_2, layFirst;
    ImageView imvGuide1, imvGuide2, imvGuide3;
    TextView tvGuide, tvGuide2, tvGuide3, tvBtn;
    int guideNavi = 0;
    DrawView dv;
    LinearLayout layWater;
    ImageView imvWater;
    int waterNavi = 1;
    ImageView imvPart;
    boolean isPart = false;
    LinearLayout layPart, layPartBottom, layPoint1, layPoint2, layPoint3, layPartUndo, layPartInit, layPartX, layPartO;
    LinearLayout layFilter;
    //    GPUImageView mGPUImageView;
    //    GPUImageView mGPUImageViewTemp;
    LinearLayout layTextBottom, layTextBottomO, layTextBottomX, layTextMiddle, layTextMiddleEdit, layTextMiddleFont;
    List<ImageView> pickColors = new ArrayList<>(20);
    //    RelativeLayout layText;
//    LinearLayout layText;
    StickerView stv;
    String stickerStr = "PicPic";
    int stickerColor = Color.WHITE;
    Typeface stickerTypeface = null;
    LinearLayout layEdit;
    //    float w, m;
    EditText edtEdit;
    ImageView imvEditOK;
    TextView tempTv;
    boolean isText = false;
    Bitmap textB;
    ArrayList<LinearLayout> layFilters;
    ArrayList<ImageView> gpus;
    boolean isFilterSetting = false;
    boolean isLoadToGif = false;
    ArrayList<Bitmap> playArray;
    FilterType[] ff = {
            FilterType.I_AMARO, FilterType.I_AMARO, FilterType.I_HEFE, FilterType.I_NASHVILLE,
            FilterType.I_SIERRA, FilterType.I_INKWELL, FilterType.I_VALENCIA,
            FilterType.I_WALDEN, FilterType.I_XPROII, FilterType.BRIGHTNESS,
            FilterType.I_BRANNAN, FilterType.I_EARLYBIRD, FilterType.I_HUDSON,
            FilterType.I_LOMO, FilterType.I_TOASTER,
            FilterType.CONTRAST, FilterType.VIGNETTE, FilterType.TONE_CURVE,
            FilterType.LOOKUP_AMATORKA
    };
    int a = 1;
    BackPressCloseHandler backPressCloseHandler;
    //    int progressNavi = 0;
    Uri mImageCaptureUri;
    int nowCameraNum = -1;
    Camera.CameraInfo mCameraInfo;
    SurfaceHolder mHolder;
    boolean isPreviewRunning;
    boolean is11 = false, isShot = false, isOK = false;

    //    FrameLayout laySticker;
    ExecutorService executor = Executors.newFixedThreadPool(1);
    ArrayList<Frame> frames = new ArrayList<>();
    boolean recording;
    long lastTime;
    long nextTime;
    LinearLayout selectHListLayout;
    GestureDetector mGestureDetector;
    ArrayList<ImageView> imvArr;
    View mDrapView;
    File thumbFile;
    int nowProgressNavi = 0;
    boolean isPhoto = false;
    TextView tvTutorial, tvNextIcon, tvCompleteIcon;
    ArrayList<Boolean> cameraNums = new ArrayList<>();
    GPUImageFilter mFilter = null;
    //    GPUImageFilter mFilterTemp = null;
//    GPUImageFilterTools.FilterAdjuster mFilterAdjusterTemp;
    GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    Matrix mMatrix;
    Paint mPaint;
    Bitmap partImage = null;
    //    StickerTextView stickerTextView;
    int playNavi = 0;
    //    private ArrayList<Boolean> booleanArr = new ArrayList<>();
//    private java.util.Timer playTimer;
//    private TimerTask playTimerTask;
//    LinearLayout layPartBlack;
    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mDrapView = v;

            if (mGestureDetector.onTouchEvent(event))
                return true;

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:

                    break;
                case MotionEvent.ACTION_UP:

                    break;
            }

            return false;
        }
    };
    private View.OnDragListener mOnDragListener = new View.OnDragListener() {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setAlpha(0.5F);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setAlpha(1F);
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    for (int i = 0, j = selectHListLayout.getChildCount(); i < j; i++) {
                        if (selectHListLayout.getChildAt(i) == v) {
                            selectHListLayout.removeView(view);
                            selectHListLayout.addView(view, i);

                            // i : 내려놓은 index  // view : 이동된뷰

                            int a = (int) view.getTag();

                            Log.e("i", i + "/" + a);

                            ArrayList<Frame> tempArr = wholeFrames.get(a);
                            if (a < i) {
                                wholeFrames.add(i + 1, wholeFrames.get(a));
                            } else {
                                wholeFrames.add(i, wholeFrames.get(a));
                            }

                            Log.e("size1", wholeFrames.size() + "");
                            for (int q = 0; q < wholeFrames.size(); q++) {
                                Log.e("arrSize" + q, wholeFrames.get(q).size() + "");
                            }
                            if (a < i) {
                                wholeFrames.remove(a);
                            } else {
                                wholeFrames.remove(a + 1);
                            }

                            Log.e("size2", wholeFrames.size() + "");
                            for (int q = 0; q < wholeFrames.size(); q++) {
                                Log.e("arrSize" + q, wholeFrames.get(q).size() + "");
                            }
                            Log.e("i2", i + "/" + a);

                            if (a < i) {
                                sectionArr.add(i + 1, sectionArr.get(a));
                                sectionArr.remove(a);
                            } else {
                                sectionArr.add(i, sectionArr.get(a));
                                sectionArr.remove(a + 1);
                            }

                            if (a < i) {
                            } else {
                            }

                            selectListSetting();
                            break;
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setAlpha(1F);
                default:
                    break;
            }
            return true;
        }
    };
    private int previewNavi = -1;

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHolder = sfv.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mHolder.addCallback(this);
//        if(mCamera != null){
//            mCamera.setPreviewCallback(this);
//        }
//        mCamera.setPreviewCallback(this);
//        mHolder.get
//        mHolder.is

        Handler hh = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                startPreview();
            }
        };
        hh.sendEmptyMessageDelayed(0, 500);
//        mHolder = sfv.getHolder();
//        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//        mHolder.addCallback(this);
////        startPreview();
////
////        111
//
////        if (mParameters != null)
////            initCamera();
//
//        Handler hh = new Handler() {
//            @Override
//            public void handleMessage(Message msg) {
//                startPreview();
//            }
//        };
//        hh.sendEmptyMessageDelayed(0, 500);
//        int numberOfCameras = Camera.getNumberOfCameras();
//        Log.e("cameraNum", numberOfCameras + "");
//        mCameraInfo = new Camera.CameraInfo();
//
//        for (int i = 0; i < numberOfCameras; i++) {
//            Camera.getCameraInfo(i, mCameraInfo);
//            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
//                mCamera = Camera.open(i);
//                nowCameraNum = i;
//            }
//        }
//
//        mParameters = mCamera.getParameters();
//
//        List<Integer> formats = mParameters.getSupportedPreviewFormats();
//        for (int format : formats) {
//            if (format == ImageFormat.RGB_565) {
//                mParameters.setPreviewFormat(format);
//                break;
//            }
//        }
//        mPreviewFormat = mParameters.getPreviewFormat();
//
//        List<Camera.Size> sizes = mParameters.getSupportedPreviewSizes();
//
//        for (int i = 0; i < sizes.size(); i++) {
//            Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
//        }
//
//        for (int i = sizes.size() - 1; i >= 0; i--) {
//            boolean isSizeOK = false;
//            Camera.Size size = sizes.get(i);
//            if (size.width == 640 && size.height == 480) {
//                mParameters.setPreviewSize(size.width, size.height);
//                isSizeOK = true;
//                Log.e("size", size.width + "/" + size.height);
//                break;
//            }
//
//            if (!isSizeOK) {
//                Camera.Size size2 = sizes.get(0);
//                mParameters.setPreviewSize(size2.width, size2.height);
//                isSizeOK = true;
//                Log.e("size", size.width + "/" + size.height);
//            }
//
//        }
//
//
//        mPreviewSize = mParameters.getPreviewSize();
//
//        mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//
//        mCamera.setParameters(mParameters);
//        mCamera.setPreviewCallback(this);
//
//
//        try {
//            mCamera.setPreviewDisplay(mHolder);
//        } catch (IOException e) {
//            mCamera.release();
//            mCamera = null;
//        }
    }

    private void initGuide(int a) {
        guideNavi = a;
        switch (a) {
            case 0:
                layFirst.setVisibility(View.VISIBLE);
                imvGuide1.setVisibility(View.GONE);
                imvGuide2.setVisibility(View.VISIBLE);
                imvGuide3.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.VISIBLE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.GONE);
                layGuide3.setVisibility(View.GONE);
                tvGuide.setVisibility(View.VISIBLE);
                tvBtn.setVisibility(View.GONE);
                tvGuide.setText(getResources().getString(R.string.tu_camera_ment_1));
                break;
            case 1:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.VISIBLE);
                imvGuide2.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.VISIBLE);
                layGuide3.setVisibility(View.GONE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.VISIBLE);
                tvBtn.setText(getResources().getString(R.string.camera_next));
                tvGuide2.setText(getResources().getString(R.string.tu_camera_ment_2));
                break;
            case 2:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.GONE);
                imvGuide2.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.VISIBLE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.GONE);
                layGuide3.setVisibility(View.VISIBLE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.GONE);
                tvGuide3.setText(getResources().getString(R.string.tu_camera_ment_3));
                break;
            case 3:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.VISIBLE);
                imvGuide2.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.VISIBLE);
                layGuide3.setVisibility(View.GONE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.VISIBLE);
                tvBtn.setText(getResources().getString(R.string.camera_complete));
                tvGuide2.setText(getResources().getString(R.string.tu_camera_ment_4));
                break;
            default:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.GONE);
                imvGuide2.setVisibility(View.GONE);
                imvGuide3.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.GONE);
                layGuide2.setVisibility(View.GONE);
                layGuide3.setVisibility(View.GONE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_gifcamera);

//        tagNavi = getIntent().getExtras().getInt("navi");
//        Intent intentt = new Intent(GIFCameraActivity.this, GIFCameraFilmActivity.class);
//        intentt.putExtra("navi", tagNavi);
//        startActivity(intentt);
//
//        finish();

        backPressCloseHandler = new BackPressCloseHandler(this);

//        mRunOnDraw = new LinkedList<Runnable>();

        ac = this;

        pd2 = new ProgressDialog(this);
        dialog = new ProgressDialog(this);

        tempViewWidth = MinUtils.screenWidth;

//        laySticker = (FrameLayout) findViewById(R.id.lay_sticker_gif_camera);
//        stickerTextView = new StickerTextView(this);
//
//        stickerTextView.setText("PicPic");
//        laySticker.addView(stickerTextView);

        layWater = (LinearLayout) findViewById(R.id.lay_gif_camera_water_mark);
        layWater.setOnClickListener(this);

        imvWater = (ImageView) findViewById(R.id.imv_gif_camera_water_mark);

        layFilter = (LinearLayout) findViewById(R.id.lay_gif_camera_filter);
//        mGPUImageView.setEGLConfigChooser(8,8,8,8,16,0);
//        mGPUImageView.setRenderer(mGPUImageView.getRenderer());
//        mGPUImageView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
//        mGPUImageView.getRenderer().changeRenderer();
//        mGPUImageView.setRenderer(mre);
//        mGPUImageViewTemp = (GPUImageView) findViewById(R.id.gpuimage_gif_camera_temp);
//        111111

        layTextBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_text_bottom);
        layTextBottomO = (LinearLayout) findViewById(R.id.lay_gif_camera_text_bottom_o);
        layTextBottomO.setOnClickListener(this);
        layTextBottomX = (LinearLayout) findViewById(R.id.lay_gif_camera_text_bottom_x);
        layTextBottomX.setOnClickListener(this);

        layTextMiddle = (LinearLayout) findViewById(R.id.lay_gif_camera_text_middle);
        layTextMiddleEdit = (LinearLayout) findViewById(R.id.lay_gif_camera_text_edit);
        layTextMiddleEdit.setOnClickListener(this);
        layTextMiddleFont = (LinearLayout) findViewById(R.id.lay_gif_camera_text_font);
        layTextMiddleFont.setOnClickListener(this);

//        layText = (LinearLayout) findViewById(R.id.lay_gif_camera_sticker);

        stv = (StickerView) findViewById(R.id.stv_gif_camera);

//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.MATCH_PARENT);
//        params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.image);
//        params.addRule(RelativeLayout.ALIGN_TOP, R.id.image);
//        ((ViewGroup)imgView.getParent()).addView(stickerView, params);

        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_1));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_2));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_3));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_4));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_5));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_6));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_7));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_8));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_9));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_10));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_11));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_12));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_13));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_14));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_15));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_16));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_17));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_18));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_19));
        pickColors.add((ImageView) findViewById(R.id.imv_gif_camera_text_color_20));

        for (int i = 0; i < pickColors.size(); i++) {
            pickColors.get(i).setOnClickListener(this);
        }

        imvPart = (ImageView) findViewById(R.id.imv_gif_camera_part_preview);

        layEdit = (LinearLayout) findViewById(R.id.lay_gif_camera_text_input);
        edtEdit = (EditText) findViewById(R.id.edt_gif_camera_text_input);
        imvEditOK = (ImageView) findViewById(R.id.imv_gif_camera_text_input_ok);
        imvEditOK.setOnClickListener(this);

        layPart = (LinearLayout) findViewById(R.id.lay_gif_camera_part);
        layPartBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_part_bottom);
        layPoint1 = (LinearLayout) findViewById(R.id.lay_gif_camera_part_point_1);
        layPoint2 = (LinearLayout) findViewById(R.id.lay_gif_camera_part_point_2);
        layPoint3 = (LinearLayout) findViewById(R.id.lay_gif_camera_part_point_3);
        layPartUndo = (LinearLayout) findViewById(R.id.lay_gif_camera_part_undo);
        layPartInit = (LinearLayout) findViewById(R.id.lay_gif_camera_part_init);
        layPartX = (LinearLayout) findViewById(R.id.lay_gif_camera_part_bottom_x);
        layPartO = (LinearLayout) findViewById(R.id.lay_gif_camera_part_bottom_o);
        layPoint1.setOnClickListener(this);
        layPoint2.setOnClickListener(this);
        layPoint3.setOnClickListener(this);
        layPartUndo.setOnClickListener(this);
        layPartInit.setOnClickListener(this);
        layPartX.setOnClickListener(this);
        layPartO.setOnClickListener(this);

//        layPartBlack = (LinearLayout)findViewById(R.id.lay_gif_camera_part_black);

        layFirst = (LinearLayout) findViewById(R.id.lay_gif_camera_first);
        layFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layFirst.setVisibility(View.GONE);
            }
        });
        layGuide = (LinearLayout) findViewById(R.id.lay_gif_camera_guide);
        layGuideAlpha = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_alpha);

        tvGuide = (TextView) findViewById(R.id.tv_gif_camera_tu_ment);
        tvGuide2 = (TextView) findViewById(R.id.tv_gif_camera_tu_ment2);
        tvGuide3 = (TextView) findViewById(R.id.tv_gif_camera_tu_ment3);
        tvBtn = (TextView) findViewById(R.id.tv_gif_camera_guide_btn);


        imvGuide1 = (ImageView) findViewById(R.id.imv_gif_camera_guide);
        imvGuide2 = (ImageView) findViewById(R.id.imv_gif_camera_guide2);
        imvGuide3 = (ImageView) findViewById(R.id.imv_gif_camera_guide3);

        layPoint2.setSelected(true);

        dv = (DrawView) findViewById(R.id.dv_gif_camera_part);

        layGuide2 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_2);
        layGuide3 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_3);
        layGuide2_1 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_touch_2_1);
        layGuide2_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide2_2 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_touch_2_2);
        layGuide2_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide3_1 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_touch_3_1);
        layGuide3_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        layGuide3_2 = (LinearLayout) findViewById(R.id.lay_gif_camera_guide_touch_3_2);
        layGuide3_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        try {
            GifDrawable gdGuide1 = new GifDrawable(getResources().getAssets().open("tu_touch_1.gif"));
            gdGuide1.setLoopCount(0);
            gdGuide1.setSpeed(2.0f);
            imvGuide1.setImageDrawable(gdGuide1);

            GifDrawable gdGuide2 = new GifDrawable(getResources().getAssets().open("tu_touch_2.gif"));
            gdGuide2.setLoopCount(0);
            gdGuide2.setSpeed(2.0f);
            imvGuide2.setImageDrawable(gdGuide2);

            GifDrawable gdGuide3 = new GifDrawable(getResources().getAssets().open("tu_slide.gif"));
            gdGuide3.setLoopCount(0);
            gdGuide3.setSpeed(2.0f);
            imvGuide3.setImageDrawable(gdGuide3);

        } catch (IOException e) {
            e.printStackTrace();
        }

        tagNavi = getIntent().getExtras().getInt("navi");

        if (tagNavi == 1) {
            tag_id = getIntent().getExtras().getString("tag_id");
            tag_title = getIntent().getExtras().getString("tag_title");
            tag_body = getIntent().getExtras().getString("tag_body");
            initGuide(-1);
        } else if (tagNavi == 2) {
            initGuide(0);
        } else {
            initGuide(-1);
        }

        if (tagNavi == 2) {
            initGuide(0);
        }

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("카메라_" + tagNavi);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("카메라");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());

        imvBack = (LinearLayout) findViewById(R.id.imv_gif_camera_back);
        imvNext = (LinearLayout) findViewById(R.id.imv_gif_camera_next);
        imvGrid = (LinearLayout) findViewById(R.id.imv_gif_camera_gird);
        imvFlash = (LinearLayout) findViewById(R.id.imv_gif_camera_flash);
        imvGhost = (LinearLayout) findViewById(R.id.imv_gif_camera_ghost);
        imvChange = (LinearLayout) findViewById(R.id.imv_gif_camera_change);
        imvReturn = (ImageView) findViewById(R.id.imv_gif_camera_return);
        layReturn = (FrameLayout) findViewById(R.id.lay_gif_camera_return);
        imvCamera = (LinearLayout) findViewById(R.id.imv_gif_camera_camera);
        imvPlus = (LinearLayout) findViewById(R.id.imv_gif_camera_plus);
        imvRatio = (LinearLayout) findViewById(R.id.imv_gif_camera_ratio);
        imvBackground = (ImageView) findViewById(R.id.imv_gif_camera_background);
        imvAlbum = (LinearLayout) findViewById(R.id.imv_gif_camera_album_pic);

        imvBack.setOnClickListener(this);
        imvNext.setOnClickListener(this);
        imvGrid.setOnClickListener(this);
        imvFlash.setOnClickListener(this);
        imvGhost.setOnClickListener(this);
        imvChange.setOnClickListener(this);
        imvReturn.setOnClickListener(this);
        layReturn.setOnClickListener(this);
        imvCamera.setOnClickListener(this);
        imvPlus.setOnClickListener(this);
        imvRatio.setOnClickListener(this);
        imvAlbum.setOnClickListener(this);

        layGrid = (LinearLayout) findViewById(R.id.lay_gif_camera_grid);
        layShot = (FrameLayout) findViewById(R.id.lay_gif_camera_shot);
        layTop = (LinearLayout) findViewById(R.id.lay_gif_camera_top);
        layBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_bottom);
        layTop2 = (LinearLayout) findViewById(R.id.lay_gif_camera_top2);
        layBottom2 = (LinearLayout) findViewById(R.id.lay_gif_camera_bottom2);
        layBtn1 = (LinearLayout) findViewById(R.id.lay_gif_camera_btns1);
//        layBtn2 = (LinearLayout) findViewById(R.id.lay_gif_camera_btns2);

        layMiddleBtns = (LinearLayout) findViewById(R.id.lay_gif_camera_btns1);
        layMiddleValues = (LinearLayout) findViewById(R.id.lay_gif_camera_editor_btns_values);
        layMiddleSelect = (LinearLayout) findViewById(R.id.lay_gif_camera_select_whole_lay);

        layBottomBtnsShooting = (LinearLayout) findViewById(R.id.lay_gif_camera_editor_bottom_btns1);
        layBottomEdit1 = (LinearLayout) findViewById(R.id.lay_gif_camera_editor_bottom_btns2);
        layBottomEdit2 = (LinearLayout) findViewById(R.id.lay_gif_camera_editor_bottom_btns3);

        layProgress = (LinearLayout) findViewById(R.id.lay_gif_camera_progress);

        layEditor2Contorller = (LinearLayout) findViewById(R.id.lay_gif_camera_editor2_control);
        layEditor2Reverse = (LinearLayout) findViewById(R.id.lay_gif_camera_editor2_reverse);
        layEditor2Fillter = (LinearLayout) findViewById(R.id.lay_gif_camera_editor2_filter);
        layEditor2Plus = (LinearLayout) findViewById(R.id.lay_gif_camera_editor2_plus);

        layEditor2Contorller.setOnClickListener(this);
        layEditor2Reverse.setOnClickListener(this);
        layEditor2Fillter.setOnClickListener(this);
        layEditor2Plus.setOnClickListener(this);

        imvEditor2Contorller = (ImageView) findViewById(R.id.imv_gif_camera_editor2_control);
        imvEditor2Reverse = (ImageView) findViewById(R.id.imv_gif_camera_editor2_reverse);
        imvEditor2Fillter = (ImageView) findViewById(R.id.imv_gif_camera_editor2_filter);


        selectHListLayout = (LinearLayout) findViewById(R.id.lay_gif_camera_select_lay);

        tvAll = (TextView) findViewById(R.id.tv_gif_camera_editor_select_all);
        tvAll.setOnClickListener(this);

        mGestureDetector = new GestureDetector(this, new DrapGestureListener());

//        imvArr = new ArrayList<ImageView>();
//
////        imvArr.add(R.id.imv_gif_camera_select_item1);
//
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item1));
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item2));
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item3));
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item4));
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item5));
//        imvArr.add((ImageView) findViewById(R.id.imv_gif_camera_select_item6));

        layEditor3X = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_x);
        layEditor3Delete = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_trash);
        layEditor3Copy = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_layer_plus);
        layEditor3Save = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_save);
        layEditor3Load = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_album);
        layEditor3Text = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_text);
        layEditor3Part = (LinearLayout) findViewById(R.id.lay_gif_camera_editor3_part);
        imvEditor3X = (ImageView) findViewById(R.id.imv_gif_camera_editor3_x);
        imvEditor3Delete = (ImageView) findViewById(R.id.imv_gif_camera_editor3_trash);
        imvEditor3Copy = (ImageView) findViewById(R.id.imv_gif_camera_editor3_layer_plus);
        imvEditor3Save = (ImageView) findViewById(R.id.imv_gif_camera_editor3_save);
        imvEditor3Load = (ImageView) findViewById(R.id.imv_gif_camera_editor3_album);
        imvEditor3Text = (ImageView) findViewById(R.id.imv_gif_camera_editor3_text);
        imvEditor3Part = (ImageView) findViewById(R.id.imv_gif_camera_editor3_part);

        layEditor3X.setOnClickListener(this);
        layEditor3Delete.setOnClickListener(this);
        layEditor3Copy.setOnClickListener(this);
        layEditor3Save.setOnClickListener(this);
        layEditor3Load.setOnClickListener(this);
        layEditor3Text.setOnClickListener(this);
        layEditor3Part.setOnClickListener(this);
        imvEditor3X.setOnClickListener(this);
        imvEditor3Delete.setOnClickListener(this);
        imvEditor3Copy.setOnClickListener(this);
        imvEditor3Save.setOnClickListener(this);
        imvEditor3Load.setOnClickListener(this);
        imvEditor3Text.setOnClickListener(this);
        imvEditor3Part.setOnClickListener(this);

//        w = layProgress.getWidth() * ratio;
        layProgress.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int tempWidth = layProgress.getWidth();
//                int ra = (int) (TOTAL / total);
//                w = tempWidth / ra;
                w = (int) (tempWidth * ratio);
//                Log.e("tempWidth", tempWidth + "");
//                Log.e("ra", ratio + "");
//                Log.e("wwww", w + "");
            }
        });
//        m = layProgress.getWidth() * 0.005f;

        sfv = (SurfaceView) findViewById(R.id.sfv_gif_camera);

//        sfv.setZOrderOnTop(true);

//        msd = (MultiDirectionSlidingDrawer) findViewById(R.id.drawer_load_to_temp);

//        gv = (GridView) findViewById(R.id.gv_content_load_to_temp);
//
//        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                for (int i = 0; i < parent.getCount(); i++) {
//                    ImageView bg = (ImageView) parent.getChildAt(i).findViewById(R.id.imv_load_to_temp_row_bg);
//                    LinearLayout lay = (LinearLayout) parent.getChildAt(i).findViewById(R.id.lay_load_to_temp_row_btns);
//                    if (i == position) {
//                        bg.setVisibility(View.VISIBLE);
//                        lay.setVisibility(View.VISIBLE);
//                    } else {
//                        bg.setVisibility(View.GONE);
//                        lay.setVisibility(View.GONE);
//                    }
//                }
//            }
//        });

//        imvHandlerArrow = (ImageView) findViewById(R.id.imv_handle_load_to_temp_arrow);
//        imvHandlerArrow.setOnClickListener(this);
//        imvHandlerX = (ImageView) findViewById(R.id.imv_handle_load_to_temp_x);
//        imvHandlerX.setOnClickListener(this);

        layShot.setOnTouchListener(this);

//        msd.setVisibility(View.GONE);
//
//        msd.setOnDrawerOpenListener(new MultiDirectionSlidingDrawer.OnDrawerOpenListener() {
//            @Override
//            public void onDrawerOpened() {
//                msd.setVisibility(View.VISIBLE);
//                imvHandlerArrow.setImageResource(R.drawable.icon_cameraroll_down);
//            }
//        });
//
//        msd.setOnDrawerCloseListener(new MultiDirectionSlidingDrawer.OnDrawerCloseListener() {
//            @Override
//            public void onDrawerClosed() {
//                msd.setVisibility(View.GONE);
//                imvHandlerArrow.setImageResource(R.drawable.icon_cameraroll_up);
//            }
//        });

        imvPlayer = (GPUImageView) findViewById(R.id.imv_gif_camera_player);

        seekSpeed = (SeekBar) findViewById(R.id.sb_gif_camera_editor_play_speed);
        seekFrame = (SeekBar) findViewById(R.id.sb_gif_camera_editor_frame_control);

//        seekSpeed.setThumb(drawable);
//        seekFrame.setThumb(drawable);

        seekSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                speedNavi = speedValuese[seekBar.getProgress()];
                startPlay();
            }
        });

        seekFrame.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (guideNavi == 2) {
                    if (frameValuese[seekBar.getProgress()] == 6) {
                        if (tagNavi == 2) {
                            initGuide(3);
                        }
                    }
                }
                if (isLoadToGif) {
                    frameNavi = frameValueseGIF[seekBar.getProgress()];
                } else {
                    frameNavi = frameValuese[seekBar.getProgress()];
                }
                startPlay();
            }
        });

        imvNextIcon = (ImageView) findViewById(R.id.imv_gif_camera_next_icon);
        imvCompleteIcon = (ImageView) findViewById(R.id.imv_gif_camera_complete_icon);
        tvNextIcon = (TextView) findViewById(R.id.tv_gif_camera_next_icon);
        tvCompleteIcon = (TextView) findViewById(R.id.tv_gif_camera_complete_icon);

        wholeFrames = new ArrayList<ArrayList<Frame>>();
//        tempOriginFrames = new ArrayList<ArrayList<Frame>>();

//        progressBar.setMax(TOTAL);
//        csb.setMax(TOTAL);
//        progressBar.setProgress(0);

        imvPlus.setSelected(false);
        changeMenuVisivle(false);

//        QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//
//        Bitmap bm = QrBitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/PicPic/_1438582052894.jpg", opt);
//
//        imvBackground.setImageBitmap(makeTransparent(bm, 80));

//        progresses = new ArrayList<>();

        mHolder = sfv.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        Handler hh = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                startPreview();
            }
        };

        hh.sendEmptyMessageDelayed(0, 500);
        colorBefore = getResources().getColor(R.color.min_progress_before);
        colorNow = getResources().getColor(R.color.min_progress_now);

        String tempFileName = String.valueOf(System.currentTimeMillis());
        File f1 = new File(Environment.getExternalStorageDirectory() + "/picpic/"
                + AppController.getSp().getString("email", "") + "_" + tempFileName + "_1.gif");
//        File f2 = new File(Environment.getExternalStorageDirectory() + "/picpic/"
//                + AppController.getSp().getString("email", "") + "_" + tempFileName + "_2.gif");
        File f2 = new File(AppController.BASE_SRC
                + MinUtils.fromMId(AppController.getSp().getString("m_id", "")) + "_"
                + MinUtils.fromCurrnet(tempFileName) + "_2.gif");
//        File f3 = new File(Environment.getExternalStorageDirectory() + "/picpic/"
//                + AppController.getSp().getString("email", "") + "_" + tempFileName + "_3.gif");

        output1 = Uri.fromFile(f1);
        output2 = Uri.fromFile(f2);
//        output3 = Uri.fromFile(f3);
        f = new File(Environment.getExternalStorageDirectory() + "/PicPic/" + tempFileName + ".jpg");
        atomFF = new File(Environment.getExternalStorageDirectory() + "/PicPic/atom_" + tempFileName + ".jpg");
//        changeMenuVisivle(false);
//        getTempDataForGridView();

        ChangeViews(INIT_VIEWS_SHOOTING);

        ms = (MultiSlider) findViewById(R.id.ms_gif_camera);
//        ms.getThumb(0).setm
        ms.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
//                multiSlider.getThumb(thumbIndex).getValue();
                int[] tempI;
                if (thumbIndex == 0) {
                    tempI = new int[]{thumb.getValue(), sectionArr.get(tempSelectIndex)[1]};
                    Log.e("msg", "point1 : " + tempI);
                    Log.e("v1", value + "");
                } else {
//                    multiSlider.getThumb()
                    tempI = new int[]{sectionArr.get(tempSelectIndex)[0], thumb.getValue()};
                    Log.e("msg", "point2 : " + tempI);
                    Log.e("v2", value + "");
                }

                sectionArr.set(tempSelectIndex, tempI);
                startPartPlay(tempSelectIndex);
            }
        });

//        stv.setDraw(true);
        layEdit.setVisibility(View.GONE);

        tempTv = new TextView(GIFCameraActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tempTv.setLayoutParams(layoutParams);
        tempTv.setTextSize(100);
        tempTv.setText(stickerStr);
        tempTv.setTextColor(stickerColor);
        tempTv.setBackgroundColor(Color.TRANSPARENT);
        tempTv.measure(0, 0);

        int width = tempTv.getMeasuredWidth();
        int height = tempTv.getMeasuredHeight();

        Log.e("onCreate w/h", width + "/" + height);

        Bitmap testB;

        testB = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(testB);
        tempTv.layout(0, 0, width, height);
        tempTv.draw(c);

        stv.setWaterMark(testB);

        tvTutorial = (TextView) findViewById(R.id.tv_gif_camera_tuto);

        int tuNavi = AppController.getSp().getInt("tucame", -1);
        int tuNavi2 = AppController.getSp().getInt("tucame2", -1);

        if (tuNavi > 0) {
            tuNavi = tuNavi - 1;
            AppController.getSe().putInt("tucame", tuNavi);
            AppController.getSe().commit();

            startActivity(new Intent(GIFCameraActivity.this, TutorialCamera6.class));
        } else {

        }

        if (tuNavi2 > 0) {
            tuNavi2 = tuNavi2 - 1;
            AppController.getSe().putInt("tucame2", tuNavi2);
            AppController.getSe().commit();

            tvTutorial.setVisibility(View.VISIBLE);
        } else {
            tvTutorial.setVisibility(View.GONE);
        }


        layFilters = new ArrayList<>();

        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_1));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_2));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_3));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_4));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_5));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_6));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_7));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_8));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_9));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_10));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_11));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_12));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_13));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_14));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_15));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_16));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_17));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_18));
        layFilters.add((LinearLayout) findViewById(R.id.lay_gif_camera_filter_19));

        gpus = new ArrayList<>();

        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_1));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_2));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_3));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_4));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_5));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_6));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_7));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_8));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_9));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_10));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_11));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_12));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_13));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_14));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_15));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_16));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_17));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_18));
        gpus.add((ImageView) findViewById(R.id.gpuimage_gif_camera_19));

        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setOnClickListener(this);
        }

        layFilters.get(0).setSelected(true);

        Intent intent = getIntent();

        if (Intent.ACTION_PICK.equals(intent.getAction())) {
//            mPicking = true;
            MessengerThreadParams mThreadParams = MessengerUtils.getMessengerThreadParamsForIntent(intent);

            String metadata = mThreadParams.metadata;
            List<String> participantIds = mThreadParams.participants;
        }
    }

    private void initLayEditor3() {
        imvEditor3X.setSelected(false);
        imvEditor3Delete.setSelected(false);
        imvEditor3Copy.setSelected(false);
        imvEditor3Save.setSelected(false);
        imvEditor3Load.setSelected(false);
        imvEditor3Text.setSelected(false);
        imvEditor3Part.setSelected(false);
    }

    private void lastTranImageToggle(boolean is) {
//        QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//
//        Bitmap bm = QrBitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/PicPic/_1438582052894.jpg", opt);

        if (wholeFrames.size() > 0) {
            Bitmap bm = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();

            imvBackground.setImageBitmap(makeTransparent(bm, 80));

            if (wholeFrames.size() != 0) {

                if (is) {
//                File tempFile = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getFile();
//                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                Bitmap bm = QrBitmapFactory.decodeFile(tempFile.getAbsolutePath(), opt);

                    imvBackground.setVisibility(View.VISIBLE);
                } else {
                    imvBackground.setVisibility(View.GONE);
                }
            }
        }
    }

    private void ChangeViews(final int navi) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (navi) {
                            case INIT_VIEWS_SHOOTING:
//                                layPartBlack.setVisibility(View.GONE);
                                layTextBottom.setVisibility(View.GONE);
//                                layText.setVisibility(View.GONE);
                                stv.setVisibility(View.GONE);
//                                laySticker.setVisibility(View.GONE);
                                layTextMiddle.setVisibility(View.GONE);

                                layEdit.setVisibility(View.GONE);
//                                mGPUImageViewTemp.setVisibility(View.GONE);
                                layFilter.setVisibility(View.GONE);

                                layPart.setVisibility(View.GONE);
                                layPartBottom.setVisibility(View.GONE);

                                layShot.setVisibility(View.VISIBLE);
                                dv.setVisibility(View.GONE);
                                imvPart.setVisibility(View.GONE);
                                layWater.setVisibility(View.GONE);

                                isShootingMode = true;

                                sfv.setVisibility(View.VISIBLE);
                                imvPlayer.setVisibility(View.GONE);

                                imvGrid.setVisibility(View.VISIBLE);
                                imvFlash.setVisibility(View.VISIBLE);
                                imvGhost.setVisibility(View.VISIBLE);
                                imvChange.setVisibility(View.VISIBLE);

                                layMiddleBtns.setVisibility(View.VISIBLE);
                                layBottomBtnsShooting.setVisibility(View.VISIBLE);

                                layBottomEdit1.setVisibility(View.GONE);
                                layMiddleValues.setVisibility(View.GONE);


                                layBottomEdit2.setVisibility(View.GONE);
                                layMiddleSelect.setVisibility(View.GONE);

//                                imvCompleteIcon.setVisibility(View.GONE);
//                                imvNextIcon.setVisibility(View.VISIBLE);
                                tvCompleteIcon.setVisibility(View.GONE);
                                tvNextIcon.setVisibility(View.VISIBLE);
                                break;
                            case INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED:
                                tvTutorial.setVisibility(View.GONE);
//                                layPartBlack.setVisibility(View.GONE);

                                layTextBottom.setVisibility(View.GONE);
//                                layText.setVisibility(View.GONE);
                                if (isText) {
                                    stv.setVisibility(View.VISIBLE);
                                    stv.setDraw(false);
//                                    laySticker.setVisibility(View.VISIBLE);
                                } else {
                                    stv.setVisibility(View.GONE);
//                                    laySticker.setVisibility(View.GONE);
                                }
                                layTextMiddle.setVisibility(View.GONE);

                                sfv.setVisibility(View.GONE);

                                layEdit.setVisibility(View.GONE);
//                                mGPUImageViewTemp.setVisibility(View.GONE);
                                layFilter.setVisibility(View.GONE);
                                layPart.setVisibility(View.GONE);
                                layPartBottom.setVisibility(View.GONE);
                                layShot.setVisibility(View.GONE);
                                dv.setVisibility(View.GONE);
                                if (isPart) {
                                    imvPart.setImageBitmap(dv.getmBitmap());
                                    imvPart.setScaleType(ImageView.ScaleType.FIT_XY);
//                                    imvPart.setbi
                                    imvPart.setVisibility(View.VISIBLE);
                                } else {
                                    imvPart.setVisibility(View.GONE);
                                }
                                layWater.setVisibility(View.VISIBLE);
//                                Bitmap tempBit = wholeFrames.get(0).get(0).getbitmap();
//                                tempBit.

                                if (tagNavi == 2) {
                                    initGuide(2);
                                }
                                if (frameNavi == 0) {
                                    seekFrame.setProgress(1);
                                    frameNavi = frameValuese[1];
                                }

                                if (speedNavi == 0) {
                                    seekSpeed.setProgress(8);
                                    speedNavi = speedValuese[8];
                                }

                                try {
                                    if (sectionArr.size() <= 0) {
                                        sectionArr = new ArrayList<>();
                                        for (int i = 0; i < wholeFrames.size(); i++) {
                                            int[] tempI = {0, wholeFrames.get(i).size() - 1};
                                            sectionArr.add(tempI);
                                        }

                                        Log.e("sectionArr", sectionArr.size() + "");
                                        for (int i = 0; i < sectionArr.size(); i++) {
                                            Log.e("sectionArr", sectionArr.get(i)[0] + "/" + sectionArr.get(i)[1]);
                                        }
                                    }
                                } catch (NullPointerException e) {
                                    sectionArr = new ArrayList<>();
                                    for (int i = 0; i < wholeFrames.size(); i++) {
                                        int[] tempI = {0, wholeFrames.get(i).size() - 1};
                                        sectionArr.add(tempI);
                                    }
                                    Log.e("sectionArr", sectionArr.size() + "");
                                    for (int i = 0; i < sectionArr.size(); i++) {
                                        Log.e("sectionArr", sectionArr.get(i)[0] + "/" + sectionArr.get(i)[1]);
                                    }
                                }

//                                stopPartPlay();
//                                stopPlay();

                                imvGrid.setVisibility(View.GONE);
                                imvFlash.setVisibility(View.GONE);
                                imvGhost.setVisibility(View.GONE);
                                imvChange.setVisibility(View.GONE);

                                isShootingMode = false;

//                                mCamera.release();
//                                sfv.setVisibility(View.GONE);
                                imvPlayer.setVisibility(View.VISIBLE);

//                                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//
//                                Bitmap b = QrBitmapFactory.decodeFile(wholeFrames.get(0).get(0).getFile().getAbsolutePath(), opt);
//
//                                imvPlayer.setImageBitmap(b);

                                layMiddleBtns.setVisibility(View.GONE);
                                layBottomBtnsShooting.setVisibility(View.GONE);

                                layBottomEdit1.setVisibility(View.VISIBLE);
                                layMiddleValues.setVisibility(View.VISIBLE);


                                layBottomEdit2.setVisibility(View.GONE);
                                layMiddleSelect.setVisibility(View.GONE);

//                                imvCompleteIcon.setVisibility(View.VISIBLE);
//                                imvNextIcon.setVisibility(View.GONE);
                                tvCompleteIcon.setVisibility(View.VISIBLE);
                                tvNextIcon.setVisibility(View.GONE);

                                imvEditor2Contorller.setSelected(true);
                                imvEditor2Fillter.setSelected(false);

                                stopPlay();
                                stopPartPlay();

                                startPlay();

                                break;
                            case INIT_VIEWS_EDITOR_1_FILLTER:
                                tvTutorial.setVisibility(View.GONE);
//                                layPartBlack.setVisibility(View.GONE);
                                layEdit.setVisibility(View.GONE);
                                layTextBottom.setVisibility(View.GONE);
//                                layText.setVisibility(View.GONE);
                                if (isText) {
                                    stv.setVisibility(View.VISIBLE);
                                    stv.setDraw(false);
//                                    laySticker.setVisibility(View.VISIBLE);
                                } else {
                                    stv.setVisibility(View.GONE);
//                                    laySticker.setVisibility(View.GONE);
                                }
                                layTextMiddle.setVisibility(View.GONE);


//                                mGPUImageViewTemp.setVisibility(View.VISIBLE);
                                layFilter.setVisibility(View.VISIBLE);

                                sfv.setVisibility(View.GONE);

                                filterSetting();

                                if (isPart) {
                                    imvPart.setImageBitmap(dv.getmBitmap());
                                    imvPart.setScaleType(ImageView.ScaleType.FIT_XY);
//                                    imvPart.setbi
                                    imvPart.setVisibility(View.VISIBLE);
                                    dv.setVisibility(View.VISIBLE);
                                } else {
                                    imvPart.setVisibility(View.GONE);
                                    dv.setVisibility(View.GONE);
                                }

                                layPart.setVisibility(View.GONE);
                                layPartBottom.setVisibility(View.GONE);
                                layShot.setVisibility(View.GONE);
                                dv.setVisibility(View.GONE);
                                layWater.setVisibility(View.GONE);
                                imvPart.setVisibility(View.GONE);

                                imvGrid.setVisibility(View.GONE);
                                imvFlash.setVisibility(View.GONE);
                                imvGhost.setVisibility(View.GONE);
                                imvChange.setVisibility(View.GONE);

                                isShootingMode = false;

//                                sfv.setVisibility(View.GONE);
//                                imvPlayer.setVisibility(View.GONE);
                                imvPlayer.setVisibility(View.VISIBLE);

                                layMiddleBtns.setVisibility(View.GONE);
                                layBottomBtnsShooting.setVisibility(View.GONE);

                                layBottomEdit1.setVisibility(View.VISIBLE);
                                layMiddleValues.setVisibility(View.GONE);


                                layBottomEdit2.setVisibility(View.GONE);
                                layMiddleSelect.setVisibility(View.GONE);

//                                imvCompleteIcon.setVisibility(View.VISIBLE);
//                                imvNextIcon.setVisibility(View.GONE);
                                tvCompleteIcon.setVisibility(View.VISIBLE);
                                tvNextIcon.setVisibility(View.GONE);

                                imvEditor2Contorller.setSelected(false);
                                imvEditor2Fillter.setSelected(true);

                                break;
                            case INIT_VIEWS_EDITOR_2_DELETE_AND_COPY:
                                tvTutorial.setVisibility(View.GONE);
//                                layPartBlack.setVisibility(View.GONE);
                                layEdit.setVisibility(View.GONE);
                                layTextBottom.setVisibility(View.GONE);
//                                layText.setVisibility(View.GONE);
                                if (isText) {
                                    stv.setDraw(false);
                                    stv.setVisibility(View.VISIBLE);
//                                    laySticker.setVisibility(View.VISIBLE);
                                } else {
                                    stv.setVisibility(View.GONE);
//                                    laySticker.setVisibility(View.GONE);
                                }
                                layTextMiddle.setVisibility(View.GONE);

//                                mGPUImageViewTemp.setVisibility(View.GONE);
                                layFilter.setVisibility(View.GONE);
                                layPart.setVisibility(View.GONE);
                                layPartBottom.setVisibility(View.GONE);
                                layShot.setVisibility(View.GONE);
                                dv.setVisibility(View.GONE);

                                layWater.setVisibility(View.VISIBLE);
                                if (isPart) {
                                    imvPart.setImageBitmap(dv.getmBitmap());
                                    imvPart.setScaleType(ImageView.ScaleType.FIT_XY);
                                    imvPart.setVisibility(View.VISIBLE);
                                } else {
                                    imvPart.setVisibility(View.GONE);
                                }

                                ms.setMin(0);
                                ms.setMax(wholeFrames.get(0).size() - 1);
                                ms.getThumb(0).setValue(sectionArr.get(0)[0]);
                                ms.getThumb(1).setValue(sectionArr.get(0)[1]);

                                if (!isPlaying) {
                                    startPlay();
                                }

                                imvGrid.setVisibility(View.GONE);
                                imvFlash.setVisibility(View.GONE);
                                imvGhost.setVisibility(View.GONE);
                                imvChange.setVisibility(View.GONE);

                                isShootingMode = false;

//                                sfv.setVisibility(View.GONE);
                                imvPlayer.setVisibility(View.VISIBLE);

                                layMiddleBtns.setVisibility(View.GONE);
                                layBottomBtnsShooting.setVisibility(View.GONE);

                                layBottomEdit1.setVisibility(View.GONE);
                                layMiddleValues.setVisibility(View.GONE);


                                layBottomEdit2.setVisibility(View.VISIBLE);
                                layMiddleSelect.setVisibility(View.VISIBLE);

//                                imvCompleteIcon.setVisibility(View.VISIBLE);
//                                imvNextIcon.setVisibility(View.GONE);
                                tvCompleteIcon.setVisibility(View.VISIBLE);
                                tvNextIcon.setVisibility(View.GONE);

//                                TODO

                                selectListSetting();

                                ms.setVisibility(View.INVISIBLE);
                                tempSelectIndex = 0;
                                isSelectAll = true;
                                tvAll.setSelected(true);
                                for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
                                    selectHListLayout.getChildAt(i).setSelected(true);
                                }
                                stopPartPlay();
                                startPlay();
                                break;
                            case INIT_VIEWS_EDITOR_2_TEXT:
                                tvTutorial.setVisibility(View.GONE);
//                                layPartBlack.setVisibility(View.GONE);
//                                laySticker.setVisibility(View.VISIBLE);

                                layTextBottom.setVisibility(View.VISIBLE);
//                                layText.setVisibility(View.VISIBLE);
                                stv.setVisibility(View.VISIBLE);
                                stv.setDraw(true);
//                                laySticker.setVisibility(View.VISIBLE);
                                layTextMiddle.setVisibility(View.VISIBLE);

//                                mGPUImageViewTemp.setVisibility(View.GONE);
                                layFilter.setVisibility(View.GONE);
//                                isPart = true;
                                layPart.setVisibility(View.GONE);
                                layPartBottom.setVisibility(View.GONE);
                                layShot.setVisibility(View.GONE);
                                imvPart.setVisibility(View.GONE);
                                layWater.setVisibility(View.GONE);

                                dv.setVisibility(View.GONE);

                                if (isPart) {
                                    imvPart.setImageBitmap(dv.getmBitmap());
                                    imvPart.setScaleType(ImageView.ScaleType.FIT_XY);
                                    imvPart.setVisibility(View.VISIBLE);
                                } else {
                                    imvPart.setVisibility(View.GONE);
                                }

                                if (!isPlaying) {
                                    startPlay();
                                }

                                imvGrid.setVisibility(View.GONE);
                                imvFlash.setVisibility(View.GONE);
                                imvGhost.setVisibility(View.GONE);
                                imvChange.setVisibility(View.GONE);

                                isShootingMode = false;

//                                sfv.setVisibility(View.GONE);
                                imvPlayer.setVisibility(View.VISIBLE);

                                layMiddleBtns.setVisibility(View.GONE);
                                layBottomBtnsShooting.setVisibility(View.GONE);

                                layBottomEdit1.setVisibility(View.GONE);
                                layMiddleValues.setVisibility(View.GONE);


                                layBottomEdit2.setVisibility(View.GONE);
                                layMiddleSelect.setVisibility(View.GONE);

//                                imvCompleteIcon.setVisibility(View.VISIBLE);
//                                imvNextIcon.setVisibility(View.GONE);
                                tvCompleteIcon.setVisibility(View.GONE);
                                tvNextIcon.setVisibility(View.GONE);

                                selectListSetting();
                                break;
                            case INIT_VIEWS_EDITOR_2_PART_GIF:
                                tvTutorial.setVisibility(View.GONE);
//                                layPartBlack.setVisibility(View.VISIBLE);
                                layEdit.setVisibility(View.GONE);
                                layTextBottom.setVisibility(View.GONE);
//                                layText.setVisibility(View.GONE);

//                                if (isText) {
//                                    stv.setVisibility(View.VISIBLE);
//                                    stv.setDraw(false);
////                                    laySticker.setVisibility(View.VISIBLE);
//                                } else {
                                stv.setVisibility(View.GONE);
//                                    laySticker.setVisibility(View.GONE);
//                                }
                                layTextMiddle.setVisibility(View.GONE);

//                                mGPUImageViewTemp.setVisibility(View.GONE);
                                layFilter.setVisibility(View.GONE);
//                                isPart = true;
                                imvPart.setVisibility(View.GONE);
                                layPart.setVisibility(View.VISIBLE);
                                layPartBottom.setVisibility(View.VISIBLE);
                                layShot.setVisibility(View.GONE);
                                imvPart.setVisibility(View.GONE);
                                layWater.setVisibility(View.GONE);

                                dv.setVisibility(View.VISIBLE);
//                                1111
                                if (partImage == null) {
                                    if (isReverse) {
                                        partImage = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();
                                    } else {
                                        partImage = wholeFrames.get(0).get(0).getbitmap();
                                    }
                                } else {
                                    partImage = dv.getmBitmap();
                                }
                                Bitmap tempB = partImage;
                                Bitmap scaleB = null;

                                if (isWidth(tempB)) {
                                    int width = imvPlayer.getWidth();
                                    int height = tempB.getHeight() * imvPlayer.getWidth() / tempB.getWidth();
//                                    int oldHeight = imvPlayer.getHeight();
//                                    Log.e("ori/scale1", tempB.getWidth() + "," + tempB.getHeight() + "/" + imvPlayer.getWidth() + "," + imvPlayer.getHeight() + "---" + layProgress.getWidth());
//                                    int height = (width / tempB.getWidth()) * dv.getHeight();
//                                    Log.e("ori/scale", tempB.getWidth() + "," + tempB.getHeight() + "/" + imvPlayer.getWidth() + "," + imvPlayer.getHeight());
//                                    FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(width, height);
//                                    dv.setLayoutParams(p);
                                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);

//                                    int y = (imvPlayer.getHeight() - height) / 2;

//                                    dv.setmBitmap(scaleB, 0, y);
//                                    dv.setmBitmap(scaleB, 0, 0);
//                                    FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                                    dv.setLayoutParams(p);
                                } else {
                                    int height = imvPlayer.getHeight();
                                    int width = tempB.getWidth() * imvPlayer.getHeight() / tempB.getHeight();
                                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);

//                                    int x = (imvPlayer.getWidth() - width) / 2;

//                                    dv.setmBitmap(scaleB, x, 0);
//                                    dv.setmBitmap(scaleB, 0, 0);
//                                    FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
//                                    dv.setLayoutParams(p);
                                }
//                                11
                                dv.setmBitmap(scaleB);
                                dv.invalidate();

                                Bitmap b2 = dv.getmBitmap();

                                if (mFilter != null) {
                                    GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
//                                                renderer.setRotation(Rotation.NORMAL,
//                                                        mRenderer.isFlippedHorizontally(), mRenderer.isFlippedVertically());
                                    renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                    PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                    buffer.setRenderer(renderer);
                                    renderer.setImageBitmap(b2, false);
                                    b2 = buffer.getBitmap();
                                    dv.setmBitmap(b2);
//                                                mFilter.destroy();
                                    renderer.deleteImage();
                                    buffer.destroy();
                                }

                                if (!isPlaying) {
                                    startPlay();
                                }

                                imvGrid.setVisibility(View.GONE);
                                imvFlash.setVisibility(View.GONE);
                                imvGhost.setVisibility(View.GONE);
                                imvChange.setVisibility(View.GONE);

                                isShootingMode = false;

//                                sfv.setVisibility(View.GONE);
                                imvPlayer.setVisibility(View.VISIBLE);

                                layMiddleBtns.setVisibility(View.GONE);
                                layBottomBtnsShooting.setVisibility(View.GONE);

                                layBottomEdit1.setVisibility(View.GONE);
                                layMiddleValues.setVisibility(View.GONE);


                                layBottomEdit2.setVisibility(View.GONE);
                                layMiddleSelect.setVisibility(View.GONE);

//                                imvCompleteIcon.setVisibility(View.VISIBLE);
//                                imvNextIcon.setVisibility(View.GONE);
                                tvCompleteIcon.setVisibility(View.GONE);
                                tvNextIcon.setVisibility(View.GONE);

                                selectListSetting();
                                break;
                            default:

                                break;
                        }
                    }
                });
            }
        });
    }

    private boolean isWidth(Bitmap b) {
        int width = b.getWidth();
        int height = b.getHeight();
        if (width > height) {
            return true;
        } else {
            return false;
        }
    }

    private void selectListSetting() {

        ms.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float dp = 50f;
        float fpixels = metrics.density * dp;
        int pixels = (int) (fpixels + 0.5f);

        float dp2 = 2f;
        float fpixels2 = metrics.density * dp2;
        int pixels2 = (int) (fpixels2 + 0.5f);

        selectHListLayout.removeAllViews();

        imvArr = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            imvArr.add(new ImageView(this));
            LinearLayout.LayoutParams parmas = new LinearLayout.LayoutParams(pixels, pixels);
            parmas.setMargins(pixels2, 0, 0, 0);

            imvArr.get(i).setLayoutParams(parmas);

            imvArr.get(i).setPadding(pixels2, pixels2, pixels2, pixels2);

            imvArr.get(i).setBackgroundResource(R.drawable.gif_camera_select);
            imvArr.get(i).setImageResource(R.drawable.icon_editplus_nogif);
            imvArr.get(i).setScaleType(ImageView.ScaleType.CENTER_CROP);
            selectHListLayout.addView(imvArr.get(i));
        }


        if (wholeFrames.size() > 6) {
            int aa = wholeFrames.size() - 6;
            int size = imvArr.size();

            for (int i = size; i < size + aa; i++) {
                imvArr.add(new ImageView(this));
                LinearLayout.LayoutParams parmas = new LinearLayout.LayoutParams(pixels, pixels);
                parmas.setMargins(pixels2, 0, 0, 0);

                imvArr.get(i).setLayoutParams(parmas);

                imvArr.get(i).setPadding(pixels2, pixels2, pixels2, pixels2);

                imvArr.get(i).setBackgroundResource(R.drawable.gif_camera_select);
                imvArr.get(i).setImageResource(R.drawable.icon_editplus_nogif);
                imvArr.get(i).setScaleType(ImageView.ScaleType.CENTER_CROP);
                selectHListLayout.addView(imvArr.get(i));
            }

        }

        for (int i = 0; i < wholeFrames.size(); i++) {
            Log.e("select size" + i, wholeFrames.get(i).size() + "");
        }

        for (int i = 0; i < wholeFrames.size(); i++) {

            selectHListLayout.getChildAt(i).setTag(i);
            bindDrapListener(selectHListLayout.getChildAt(i));

            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
            opt.inSampleSize = 8;
//            Log.e("path" + i, wholeFrames.get(i).get(0).getFile().getAbsolutePath());
//            Log.e("id" + i, imvArr.get(i).toString());
            Bitmap thumb = null;
            if (wholeFrames.get(i).size() > 0) {
                thumb = QrBitmapFactory.decodeFile(wholeFrames.get(i).get(0).getFile().getAbsolutePath(), opt);
                Log.e("path" + i, wholeFrames.get(i).get(0).getFile().getAbsolutePath());
            }

            selectHListLayout.getChildAt(i).setBackgroundResource(R.drawable.btn_onclick_camera_select);
            ((ImageView) selectHListLayout.getChildAt(i)).setImageBitmap(thumb);

//            imvArr.get(i).setBackgroundResource(R.drawable.btn_onclick_camera_select);
//            imvArr.get(i).setImageBitmap(thumb);
//            bindDrapListener(imvArr.get(i));
//            imvArr.get(i).setTag(i);
//            final int finalI = i;
//            imvArr.get(i).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    for (int j = 0; j < imvArr.size(); j++) {
//                        imvArr.get(j).setSelected(false);
//                    }
////                    imvArr.get(finalI).setSelected(true);
//                    v.setSelected(true);
//                    selectedNavi = finalI;
//                }
//            });
        }
        tempSelectIndex = 0;
        selectHListLayout.getChildAt(0).setSelected(true);

        startPartPlay(0);

    }

//    private void switchFilterTo2(final GPUImageFilter filter) {
//        if (mFilterTemp == null
//                || (filter != null && !mFilterTemp.getClass().equals(filter.getClass()))) {
//            mFilterTemp = filter;
////            mGPUImageViewTemp.setFilter(mFilterTemp);
//            mFilterAdjusterTemp = new GPUImageFilterTools.FilterAdjuster(mFilterTemp);
//            mFilterAdjusterTemp.adjust(50);
//        }
//    }

    private void getTempDataForGridView() {
        int tempCount = AppController.getSp().getInt("tempCount", 0);
        Log.e("nowTempCount", tempCount + "");
        if (tempCount != 0) {
            tempFolders = new ArrayList<>();
            LoadToTempResult ltr = new LoadToTempResult();
            File file = new File(Environment.getExternalStorageDirectory() + "/PicPic/temp/");
            if (file.exists()) {

            } else {
                file.mkdirs();
            }
            File[] files = file.listFiles();
            for (File inFile : files) {
                if (inFile.isDirectory()) {
                    LoadToTempItem li = new LoadToTempItem();
                    li.setFolderName(inFile.getName());
                    Log.e("folderName", inFile.getName());
                    li.setFolderUrl(inFile.getAbsolutePath());
                    Log.e("folderNPath", inFile.getAbsolutePath());
//                    try{
//
//                    }catch ()
                    File[] ff = inFile.listFiles();
//                    File[] fff = ff[0].listFiles();
                    QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                    Log.e("filePath", ff[0].getAbsolutePath());
                    li.setThumb(QrBitmapFactory.decodeFile(ff[0].getAbsolutePath(), opt));
                    li.setSize(ff.length);
                    tempFolders.add(inFile.getName());

                    ltr.add(li);
                }
            }
//            gv.setAdapter(new LoadToTempListAdapter(ltr));
        }
    }

    private void getFonts() {
        File f = new File("/system/fonts/");
        File[] fs = f.listFiles();
        for (int i = 0; i < fs.length; i++) {
            Log.e("fonts" + i, fs[i].getName());
        }
    }

    @Override
    public void onPictureSaved(Uri uri) {

    }

    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            imvPlayer.setFilter(mFilter);
//            mGPUImageViewTemp.setFilter(mFilter);
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);
            mFilterAdjuster.adjust(50);
        }
    }

    private void handleImage() {

        playArray = new ArrayList<>();

        int duration = 1000 / speedNavi;

//        mGPUImageView.setImage(selectedImage);
//        mGPUImageView.setImage(wholeFrames.get(0).get(0).getbitmap());
//        mGPUImageView.setImage(wholeFrames.get(0).get(0).getFile());
//        imvPlayer.setImageBitmap(wholeFrames.get(0).get(0).getbitmap());
//        if (tempOriginFrames.size() == 0) {
//            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.non);
//            mGPUImageView.setImage(b);
//            mGPUImageView.setImage(wholeFrames.get(0).get(0).getbitmap());
//            mGPUImageViewTemp.setImage(wholeFrames.get(0).get(0).getbitmap());
        if (!isReverse) {

            for (int i = 0; i < wholeFrames.size(); i++) {
                for (int j = sectionArr.get(i)[0]; j <= sectionArr.get(i)[1]; j = j + frameNavi) {
                    playArray.add(wholeFrames.get(i).get(j).getbitmap());
//                    Drawable d = new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap());
//                    previewAnimation.addFrame(d, duration);
                }
            }

        } else {
            for (int i = wholeFrames.size() - 1; i >= 0; i--) {
                for (int j = sectionArr.get(i)[1]; j >= sectionArr.get(i)[0]; j = j - frameNavi) {
                    playArray.add(wholeFrames.get(i).get(j).getbitmap());
//                    Drawable d = new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap());
//                    previewAnimation.addFrame(d, duration);
                }
            }
        }
//        } else {
////            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.non);
////            mGPUImageView.setImage(b);
////            mGPUImageView.setImage(tempOriginFrames.get(0).get(0).getbitmap());
////            mGPUImageViewTemp.setImage(tempOriginFrames.get(0).get(0).getbitmap());
//            if (!isReverse) {
//
//                for (int i = 0; i < tempOriginFrames.size(); i++) {
//                    for (int j = sectionArr.get(i)[0]; j <= sectionArr.get(i)[1]; j = j + frameNavi) {
//                        playArray.add(tempOriginFrames.get(i).get(j).getbitmap());
////                    Drawable d = new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap());
////                    previewAnimation.addFrame(d, duration);
//                    }
//                }
//
//            } else {
//                for (int i = tempOriginFrames.size() - 1; i >= 0; i--) {
//                    for (int j = sectionArr.get(i)[1]; j >= sectionArr.get(i)[0]; j = j - frameNavi) {
//                        playArray.add(tempOriginFrames.get(i).get(j).getbitmap());
////                    Drawable d = new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap());
////                    previewAnimation.addFrame(d, duration);
//                    }
//                }
//            }
//        }


//        private int playNavi = 0;
//        playTimer = new java.util.Timer();
//        playTimerTask = new TimerTask() {
//            @Override
//            public void run() {
//                imvPlayer.setImage(playArray.get(playNavi));
//
//                if (playNavi == playArray.size() - 1) {
//                    playNavi = 0;
//                } else {
//                    playNavi = playNavi + 1;
//                }
//            }
//        };
//
//        playTimer.schedule(playTimerTask, 0, duration);
//        mGPUImageView.setImageArray(arr, duration);
//        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.non);
//        mGPUImageView.setImage(b);
    }

    private void filterSetting() {
//        handleImage();

//        hlist.setAdapter(new FilterAdapter(this, 0));
//
//        hlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i != 0) {
//                    switchFilterTo(createFilterForType(GIFCameraActivity.this, ff[i]));
//                    imvPlayer.requestRender();
//                    mFilter = createFilterForType(GIFCameraActivity.this, ff[i]);
//                } else {
//                    switchFilterTo(new GPUImageFilter());
//                    mFilter = null;
//                }
//            }
//        });
        if (!isFilterSetting) {
            Bitmap b = wholeFrames.get(0).get(0).getbitmap();
            if (b.getWidth() > 480) {
                if (is11) {
                    b = Bitmap.createScaledBitmap(b, 480, 480, false);
                } else {
                    b = Bitmap.createScaledBitmap(b, 480, 640, false);
                }
            }

//        Log.e("length", ff.length + "");
            final Bitmap tempB = b;
            for (int i = 0; i < ff.length; i++) {
                final int finalI = i;
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap;
                        if (finalI != 0) {
                            GPUImageRenderer renderer = new GPUImageRenderer(createFilterForType(GIFCameraActivity.this, ff[finalI]));
                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                            PixelBuffer buffer = new PixelBuffer(tempB.getWidth(), tempB.getHeight());
                            buffer.setRenderer(renderer);
                            renderer.setImageBitmap(tempB, false);
                            bitmap = buffer.getBitmap();
                            renderer.deleteImage();
                            buffer.destroy();
                        } else {
                            bitmap = tempB;
                        }
                        final Bitmap finalBitmap = bitmap;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gpus.get(finalI).setImageBitmap(finalBitmap);
                            }
                        });

                    }
                });


//            gpus.get(i).setImage(wholeFrames.get(0).get(0).getbitmap());
//            if(i!=0){
//                gpus.get(i).setFilter(createFilterForType(GIFCameraActivity.this, ff[i]));
//            }
            }
            isFilterSetting = true;
        }


    }

    private GPUImageFilter createFilterForType(final Context context, final FilterType type) {
        switch (type) {
            case CONTRAST:
                return new GPUImageContrastFilter(2.0f);
            case BRIGHTNESS:
                return new GPUImageBrightnessFilter(0.1f);
            case VIGNETTE:
                PointF centerPoint = new PointF();
                centerPoint.x = 0.5f;
                centerPoint.y = 0.5f;
                return new GPUImageVignetteFilter(centerPoint, new float[]{0.0f, 0.0f, 0.0f}, 0.3f, 0.75f);
            case TONE_CURVE:
                GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
                toneCurveFilter.setFromCurveFileInputStream(
                        context.getResources().openRawResource(R.raw.tone_cuver_sample));
                return toneCurveFilter;
            case LOOKUP_AMATORKA:
                GPUImageLookupFilter amatorka = new GPUImageLookupFilter();
                amatorka.setBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.lookup_amatorka));
                return amatorka;
            case I_AMARO:
                return new IFAmaroFilter(context);
            case I_BRANNAN:
                return new IFBrannanFilter(context);
            case I_EARLYBIRD:
                return new IFEarlybirdFilter(context);
            case I_HEFE:
                return new IFHefeFilter(context);
            case I_HUDSON:
                return new IFHudsonFilter(context);
            case I_INKWELL:
                return new IFInkwellFilter(context);
            case I_LOMO:
                return new IFLomoFilter(context);
            case I_NASHVILLE:
                return new IFNashvilleFilter(context);
            case I_SIERRA:
                return new IFSierraFilter(context);
            case I_TOASTER:
                return new IFToasterFilter(context);
            case I_VALENCIA:
                return new IFValenciaFilter(context);
            case I_WALDEN:
                return new IFWaldenFilter(context);
            case I_XPROII:
                return new IFXprollFilter(context);

            default:
                throw new IllegalStateException("No filter of that type!");
        }

    }

    @Override
    public void onBackPressed() {
//        if (msd.isOpened()) {
//            msd.animateClose();
//        } else {
        if (tagNavi == 2) {
            backPressCloseHandler.onBackPressed();
        } else {
            finish();
        }
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executor.shutdownNow();
//        if (playTimer != null) {
//            playTimer.cancel();
//            playTimerTask.cancel();
//        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void changeMenuVisivle(boolean isVisible) {
        int y = 0;
        if (isVisible) {
            y = 0;
        } else {
            y = 300;
        }

        imvRatio.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        imvGrid.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        imvFlash.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        imvGhost.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        imvAlbum.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
//        imvSave.animate().setInterpolator(mInterpolator)
//                .setDuration(200)
//                .translationY(y);
//
//        imvAlbum.animate().setInterpolator(mInterpolator)
//                .setDuration(200)
//                .translationY(y);
//
//        imvLoad.animate().setInterpolator(mInterpolator)
//                .setDuration(200)
//                .translationY(y);
    }

    public Bitmap makeTransparent(Bitmap src, int value) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap transBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(transBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        // config paint
        final Paint paint = new Paint();
        paint.setAlpha(value);
        canvas.drawBitmap(src, 0, 0, paint);
        return transBitmap;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public void onClick(View v) {
        Bitmap b = null;
        switch (v.getId()) {
            case R.id.imv_gif_camera_back:
                finish();
                break;
            case R.id.imv_gif_camera_gird:
                if (isShootingMode) {
                    if (imvGrid.isSelected()) {
                        imvGrid.setSelected(false);
                        layGrid.setVisibility(View.INVISIBLE);
                    } else {
                        imvGrid.setSelected(true);
                        layGrid.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.imv_gif_camera_flash:
                try {
                    if (isShootingMode) {
                        if (isFlashOn) {
                            mParameters.setFlashMode("off");
                            isFlashOn = false;
                            imvFlash.setSelected(false);
                        } else {
                            mParameters.setFlashMode("torch");
                            isFlashOn = true;
                            imvFlash.setSelected(true);
                        }

                        mCamera.setParameters(mParameters);
                    }

                } catch (RuntimeException e) {

                }
                break;
            case R.id.imv_gif_camera_ghost:
                if (isShootingMode) {
                    if (imvGhost.isSelected()) {
                        imvGhost.setSelected(false);
                        lastTranImageToggle(false);
                    } else {
                        lastTranImageToggle(true);
                        imvGhost.setSelected(true);
                    }
                }
                break;
            case R.id.imv_gif_camera_change:
                if (isShootingMode) {
                    switchCamera();
//                    if (imvChange.isSelected()) {
//                        imvChange.setSelected(false);
//                    } else {
//                        imvChange.setSelected(true);
//                    }
                }
                break;
            case R.id.imv_gif_camera_next:

//                int wholeSize = 0;
//                for (int i = 0; i < wholeFrames.size(); i++) {
//                    for (int j = 0; j < wholeFrames.get(i).size(); j++) {
//                        wholeSize = wholeSize + 1;
//                    }
//                }
//
//                Log.e("out", "out");
//                if (wholeSize > 4) {
                if (imvNext.isSelected()) {
                    if (tagNavi == 2) {
                        initGuide(-1);
                    }
                    if (isShootingMode) {

//                        tempOriginFrames = wholeFrames;

                        flashToggle(false);

                        imvGhost.setSelected(false);
                        lastTranImageToggle(false);

                        imvGrid.setSelected(false);
                        layGrid.setVisibility(View.INVISIBLE);
                        Log.e("in", "in");
                        //TODO 에디터 ㄱㄱㄱ
//                    layBtn1.setVisibility(View.GONE);
//                    layBtn2.setVisibility(View.VISIBLE);
                        final ProgressDialog pd2 = new ProgressDialog(this);
                        pd2.setCanceledOnTouchOutside(false);
                        pd2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        pd2.setMessage(getResources().getString(R.string.please_wait));
                        pd2.show();

//                        Handler handler = new Handler() {
//                            @Override
//                            public void handleMessage(Message msg) {
//
//                            }
//                        };
//
//                        handler.sendEmptyMessageDelayed(0, 12000);

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
////                            dialog = ProgressDialog.show(getApplicationContext(), "", getResources().getString(R.string.please_wait), true);
////                                startActivity(new Intent(GIFCameraActivity.this, ProgressActivity.class));
//                            }
//                        });

                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (pd2.isShowing()) {
                                            pd2.dismiss();
                                        }
//                                        if (dialog.isShowing()) {
//                                            dialog.dismiss();
//                                        }
                                        ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
                                    }
                                });
                            }
                        });

                    } else {


                        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                        AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Film Complete Click").build());
                        if (tagNavi == 2) {
                            initGuide(3);
                        }

                        nextTime = Long.MAX_VALUE;

                        final ProgressDialog pd = new ProgressDialog(this);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        pd.show();

                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<Frame> tempArrr = new ArrayList<>();
                                int wholeSize = 0;
                                for (int q = 0; q < wholeFrames.size(); q++) {
//                                    for (int q = 0; q < wholeFrames.size(); q = q + frameNavi) {
//                                for (int q = 0; q < wholeFrames.size(); q++) {
                                    for (int w = 0; w < wholeFrames.get(q).size(); w++) {
                                        wholeSize += 1;
                                        tempArrr.add(wholeFrames.get(q).get(w));
                                    }
                                }
                                Log.e("size", wholeSize + "");

//                                pd.setMax(wholeSize / frameNavi);\
                                int pdRatio = 0;
                                try {
                                    pdRatio = 100 / (wholeSize / frameNavi);
                                } catch (ArithmeticException e) {
                                    return;
                                }
                                // java.lang.ArithmeticException: divide by zero
                                // FATAL EXCEPTION: pool-16-thread-1
                                pd.setMax(100);

//                                QAGIFEncoder encoder1 = new QAGIFEncoder();
//                                encoder1.setRepeat(0);
//                                encoder1.setDelay(1000 / speedNavi);
                                QAGIFEncoder encoder2 = new QAGIFEncoder();
                                encoder2.setRepeat(0);
                                encoder2.setDelay(1000 / speedNavi);
//                                QAGIFEncoder encoder3 = new QAGIFEncoder();
//                                encoder3.setRepeat(0);
//                                encoder3.setDelay(1000 / speedNavi);
                                try {
//                                    encoder1.start(getPath1());
                                    encoder2.start(getPath2());
//                                    encoder3.start(getPath3());
                                    int i = 0;
                                    if (!isReverse) {
                                        Log.e("size1", wholeFrames.size() + "");
//                                    for (int q = 0; q < wholeFrames.size(); q = q + frameNavi) {
//                                        for (int w = 0; w < wholeFrames.get(q).size(); w++) {
//                                        for (int q = 0; q < tempArrr.size(); q = q + frameNavi) {
                                        for (int q = 0; q < wholeFrames.size(); q++) {
                                            Log.e("section" + q, sectionArr.get(q)[0] + "/" + sectionArr.get(q)[1]);
                                            for (int w = sectionArr.get(q)[0]; w <= sectionArr.get(q)[1]; w += frameNavi) {
//                                            Log.e("size2", wholeFrames.get(q).size() + "");
//                                            Frame f = wholeFrames.get(q).get(w);
//                                                Frame f = tempArrr.get(q);
                                                Frame f = wholeFrames.get(q).get(w);
                                                Timer t = new Timer();

                                                QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                                opt2.inSampleSize = 1;

                                                if (q == 0) {
                                                    if (w == 0) {
                                                        thumbFile = f.getFile();
                                                    }
                                                }

                                                Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

//                                                int b1w = b1.getWidth();
                                                int b2w = b2.getWidth();
//                                                int b3w = b3.getWidth();

                                                if (is11) {
//                                                    b1 = Bitmap.createBitmap(b1, 0, (b1.getHeight() - b1w) / 2, b1w, b1w);
                                                    b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
//                                                    b3 = Bitmap.createBitmap(b3, 0, (b3.getHeight() - b3w) / 2, b3w, b3w);
                                                }

                                                if (isPart) {

                                                    Bitmap firstB = dv.getmBitmap();
                                                    firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                                    b2 = overlayPart(b2, firstB);

                                                }

                                                if (mFilter != null) {
                                                    GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
//                                                renderer.setRotation(Rotation.NORMAL,
//                                                        mRenderer.isFlippedHorizontally(), mRenderer.isFlippedVertically());
                                                    renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                                    PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                                    buffer.setRenderer(renderer);
                                                    renderer.setImageBitmap(b2, false);
                                                    b2 = buffer.getBitmap();
//                                                mFilter.destroy();
                                                    renderer.deleteImage();
                                                    buffer.destroy();
                                                }

                                                if (waterNavi != 0) {
                                                    Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                                    bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                                    b2 = overlayMark(b2, bb);
                                                }


                                                if (isText) {
//                                                    textB = Bitmap.createScaledBitmap(textB, b2.getWidth(), b2.getHeight(), false);
//                                                    b2 = textMark(b2, textB);
                                                    b2 = overlayMark(b2, textB);
                                                }

//                                                Log.e("size1", b1.getWidth() + " / " + b1.getHeight());
                                                Log.e("size2", b2.getWidth() + " / " + b2.getHeight());
//                                                Log.e("size2", b3.getWidth() + " / " + b3.getHeight());

                                                t.logDelay("createBitmap");
//                                                encoder1.addFrame(b1);
                                                encoder2.addFrame(b2);
//                                                encoder2.set
//                                                encoder3.addFrame(b3);
                                                t.logDelay("addFrame");
                                                if (!pd.isShowing()) {
                                                    break;
                                                }
                                                i = i + pdRatio;
                                                pd.setProgress(i);
                                            }
                                        }
                                    } else {
//                                        for (int q = tempArrr.size() - 1; q >= 0; q = q - frameNavi) {
                                        for (int q = wholeFrames.size() - 1; q >= 0; q--) {
                                            for (int w = sectionArr.get(q)[1]; w >= sectionArr.get(q)[0]; w = w - frameNavi) {
//                                            Log.e("size2", wholeFrames.get(q).size() + "");
                                                Frame f = wholeFrames.get(q).get(w);
//                                                Frame f = tempArrr.get(q);
                                                Timer t = new Timer();

                                                QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                                opt2.inSampleSize = 1;

                                                if (q == wholeFrames.size() - 1) {
                                                    if (w == sectionArr.get(q)[1]) {
                                                        thumbFile = f.getFile();
                                                    }
                                                }

                                                Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

//                                                int b1w = b1.getWidth();
                                                int b2w = b2.getWidth();
//                                                int b3w = b3.getWidth();

                                                if (is11) {
//                                                    b1 = Bitmap.createBitmap(b1, 0, (b1.getHeight() - b1w) / 2, b1w, b1w);
                                                    b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
//                                                    b3 = Bitmap.createBitmap(b3, 0, (b3.getHeight() - b3w) / 2, b3w, b3w);
                                                }

                                                if (isPart) {
                                                    Bitmap firstB = dv.getmBitmap();
                                                    firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                                    b2 = overlayPart(b2, firstB);

                                                }

                                                if (mFilter != null) {
                                                    GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
//                                                renderer.setRotation(Rotation.NORMAL,
//                                                        mRenderer.isFlippedHorizontally(), mRenderer.isFlippedVertically());
                                                    renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                                    PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                                    buffer.setRenderer(renderer);
                                                    renderer.setImageBitmap(b2, false);
                                                    b2 = buffer.getBitmap();
//                                                mFilter.destroy();
                                                    renderer.deleteImage();
                                                    buffer.destroy();
                                                }

                                                if (waterNavi != 0) {
                                                    Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                                    bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                                    b2 = overlayMark(b2, bb);
                                                }

                                                if (isText) {
//                                                    textB = Bitmap.createScaledBitmap(textB, b2.getWidth(), b2.getHeight(), false);
//                                                    b2 = textMark(b2, textB);
                                                    b2 = overlayMark(b2, textB);
                                                }

//                                                Log.e("size1", b1.getWidth() + " / " + b1.getHeight());
                                                Log.e("size2", b2.getWidth() + " / " + b2.getHeight());
//                                                Log.e("size3", b3.getWidth() + " / " + b3.getHeight());

                                                t.logDelay("createBitmap");
//                                                encoder1.addFrame(b1);
                                                encoder2.addFrame(b2);
//                                                encoder3.addFrame(b3);
                                                t.logDelay("addFrame");
                                                if (!pd.isShowing()) {
                                                    break;
                                                }
                                                i = i + pdRatio;
                                                pd.setProgress(i);
//                                                pd.setProgress(i++);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (pd.isShowing()) {
//                                    encoder1.finish();
                                    encoder2.finish();
//                                    encoder3.finish();
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (pd.isShowing()) {
                                            pd.dismiss();
//                                            Intent data = new Intent(GIFCameraActivity.this, WritePostActivity.class);
                                            Intent data = new Intent(GIFCameraActivity.this, WritePostActivityV1.class);
//                                            data.putExtra("url1", getPath1());
                                            data.putExtra("url2", getPath2());
//                                            data.putExtra("url3", getPath3());
                                            try {
                                                data.putExtra("thumb", thumbFile.getAbsolutePath());
                                            } catch (NullPointerException e) {
//                                                data.putExtra("thumb", thumbFile.getAbsolutePath());
                                            }
                                            //????앱솔루트가 안된데
//                                            copyFile(thumbFile, Environment.getExternalStorageDirectory() + "/picpic/thumb.jpg");
                                            data.putExtra("navi", tagNavi);
                                            if (tagNavi == 1) {
                                                data.putExtra("tag_id", tag_id);
                                                data.putExtra("tag_title", tag_title);
                                                data.putExtra("tag_body", tag_body);
                                            }
//                                data.setData(output1);
//                                data.setData(output2);
//                                data.setData(output3);
                                            startActivity(data);
                                            finish();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
                break;
            case R.id.lay_gif_camera_return:
            case R.id.imv_gif_camera_return:

                if (layProgress.getChildCount() != 0) {
                    if (!imvReturn.isSelected()) {
                        imvReturn.setSelected(true);
                        //TODO 삭제 프로세스 색상 변경

                        layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_now));
                        layProgress.invalidate();
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_now));
//                                layProgress.invalidate();
//                            }
//                        });
                    } else {
                        final int tempI = layProgress.getChildCount() - 1;
//                        if (wholeFrames.get(tempI) != null) {
                        layProgress.removeViewAt(tempI);
                        layProgress.invalidate();
                        imvReturn.setSelected(false);

                        try {
                            List<Runnable> list = executor.shutdownNow();

                            if (list.size() == 0) {
                                if (wholeFrames.size() > 0) {
                                    cameraNums.remove(cameraNums.size() - 1);
                                    wholeFrames.remove(wholeFrames.size() - 1);
                                }
                            } else {
                                if (!executor.isTerminated() && !executor.isShutdown()) {
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i != list.size() - 1) {
                                            executor.submit(list.get(i));
                                        }
                                    }

                                    cameraNums.remove(tempI);
                                    nowProgressNavi = nowProgressNavi - 1;
                                }

                            }
                        } catch (NullPointerException e) {
//                            executor.execute(new Runnable() {
//                                @Override
//                                public void run() {
//                                    wholeFrames.remove(tempI);
//                                    cameraNums.remove(tempI);
//                                    nowProgressNavi = nowProgressNavi - 1;
//                                }
//                            });
                            Log.e("remove null", e.toString());
                        } catch (Exception e) {
                            Log.e("remove ex", e.toString());
                        }


//                        }
                    }
                }
                int tempSize = 0;

                if (layProgress.getChildCount() > 1) {
                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                        tempSize += layProgress.getChildAt(i).getWidth();
                    }
                } else if (layProgress.getChildCount() == 1) {
                    tempSize = layProgress.getChildAt(0).getWidth();
                }

                if (tempSize > 50) {
                    imvNext.setSelected(true);
                } else {
                    imvNext.setSelected(false);
                }
                break;
            case R.id.imv_gif_camera_camera:
                if (imvCamera.isSelected()) {
                    imvCamera.setSelected(false);
                    isShot = false;
                } else {
                    isShot = true;
                    imvCamera.setSelected(true);
                }
                break;
            case R.id.imv_gif_camera_plus:
                if (imvPlus.isSelected()) {
                    imvPlus.setSelected(false);
                    changeMenuVisivle(false);
                } else {
                    imvPlus.setSelected(true);
                    changeMenuVisivle(true);
                }
                Log.e("isSelected", imvPlus.isSelected() + "");
                break;
            case R.id.imv_gif_camera_ratio:
                if (wholeFrames.size() < 1) {
                    if (imvRatio.isSelected()) {
                        is11 = false;
                        int layTopHeight = layTop.getHeight();
                        imvRatio.setSelected(false);
                        layTop.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0 - layTopHeight);
                        layBottom.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(layBottom.getHeight());
                        layTop.setVisibility(View.GONE);
                        layBottom.setVisibility(View.GONE);
                        layTop2.setVisibility(View.GONE);
                        layBottom2.setVisibility(View.GONE);
                    } else {
                        is11 = true;
                        imvRatio.setSelected(true);
                        layTop.setVisibility(View.VISIBLE);
                        layBottom.setVisibility(View.VISIBLE);
                        layTop2.setVisibility(View.VISIBLE);
                        layBottom2.setVisibility(View.VISIBLE);

                        layTop.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0);
                        layBottom.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0);
                    }
                }
                break;
//            case R.id.imv_gif_camera_save:
//                //TODO 임시저장
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_wait_next_version), Toast.LENGTH_SHORT).show();
////                if (wholeFrames.size() != 0) {
////                    doSaveToTemp();
////                }
//                break;
            case R.id.imv_gif_camera_album_pic:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Camera Load To Album").build());
//                if (total == 0) {
//                    doTakeAlbumAction();
                doTakePictureAlbumAction();
//                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.cant_shoot2), Toast.LENGTH_SHORT).show();
//                }
                break;
//            case R.id.imv_gif_camera_load:
//                //TODO 임시저장 불러오기
////                doLoadToTemp();
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_wait_next_version), Toast.LENGTH_SHORT).show();
//                break;
            case R.id.imv_handle_load_to_temp_x:
//                if (msd.isOpened()) {
//                    msd.animateClose();
//                }
                break;
            case R.id.imv_handle_load_to_temp_arrow:
//                if (msd.isOpened()) {
//                    msd.animateClose();
//                } else {
//                    msd.animateOpen();
//                }
                break;
            case R.id.tv_gif_camera_editor_select_all:
                ms.setVisibility(View.INVISIBLE);
                tempSelectIndex = 0;
                isSelectAll = true;
                tvAll.setSelected(true);
                for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
                    selectHListLayout.getChildAt(i).setSelected(true);
                }
                stopPartPlay();
                startPlay();
                break;
//            case R.id.imv_gif_camera_select_item1:
//
//                if (imvArr.get(0).isSelected()) {
//                    imvArr.get(0).setSelected(false);
//                } else {
//                    imvArr.get(0).setSelected(true);
//                }
//                break;
//            case R.id.imv_gif_camera_select_item2:
//                if (imvArr.get(1).isSelected()) {
//                    imvArr.get(1).setSelected(false);
//                } else {
//                    imvArr.get(1).setSelected(true);
//                }
//                break;
//            case R.id.imv_gif_camera_select_item3:
//                if (imvArr.get(2).isSelected()) {
//                    imvArr.get(2).setSelected(false);
//                } else {
//                    imvArr.get(2).setSelected(true);
//                }
//                break;
//            case R.id.imv_gif_camera_select_item4:
//                if (imvArr.get(3).isSelected()) {
//                    imvArr.get(3).setSelected(false);
//                } else {
//                    imvArr.get(3).setSelected(true);
//                }
//                break;
//            case R.id.imv_gif_camera_select_item5:
//                if (imvArr.get(4).isSelected()) {
//                    imvArr.get(4).setSelected(false);
//                } else {
//                    imvArr.get(4).setSelected(true);
//                }
//                break;
//            case R.id.imv_gif_camera_select_item6:
//                if (imvArr.get(5).isSelected()) {
//                    imvArr.get(5).setSelected(false);
//                } else {
//                    imvArr.get(5).setSelected(true);
//                }
//                break;
            case R.id.lay_gif_camera_editor2_control:
                if (!imvEditor2Contorller.isSelected()) {
                    ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
                }
                break;
            case R.id.lay_gif_camera_editor2_reverse:
                if (!imvEditor2Reverse.isSelected()) {
                    imvEditor2Reverse.setSelected(true);
                    isReverse = true;
                    startPlay();
                } else {
                    imvEditor2Reverse.setSelected(false);
                    isReverse = false;
                    startPlay();
                }
                break;
            case R.id.lay_gif_camera_editor2_filter:
            case R.id.imv_gif_camera_editor2_filter:
                ChangeViews(INIT_VIEWS_EDITOR_1_FILLTER);
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.unservice_wait), Toast.LENGTH_SHORT).show();
                break;
            case R.id.lay_gif_camera_editor2_plus:
                ChangeViews(INIT_VIEWS_EDITOR_2_DELETE_AND_COPY);
                break;
            case R.id.lay_gif_camera_editor3_x:
            case R.id.imv_gif_camera_editor3_x:
                ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);
                break;
            case R.id.lay_gif_camera_editor3_trash:
            case R.id.imv_gif_camera_editor3_trash:
                if (!isSelectAll) {
                    sectionArr.remove(tempSelectIndex);
                    wholeFrames.remove(tempSelectIndex);
                    if (sectionArr.size() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.edit_list_size_0), Toast.LENGTH_SHORT).show();
                        finish();
                        overridePendingTransition(0, 0);
                    } else {
                        selectListSetting();
                    }
                }

                break;
            case R.id.lay_gif_camera_editor3_layer_plus:
            case R.id.imv_gif_camera_editor3_layer_plus:
                if (!isSelectAll) {
                    int nowSize = 0;
                    for (int i = 0; i < wholeFrames.size(); i++) {
                        nowSize += wholeFrames.get(i).size();
                    }
                    int addSize = wholeFrames.get(tempSelectIndex).size();
                    if (nowSize + addSize > 80) {
                        Toast.makeText(getApplicationContext(), "추가가능한 시간이 초과되었습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        sectionArr.add(new int[]{0, wholeFrames.get(tempSelectIndex).size() - 1});
                        wholeFrames.add(wholeFrames.get(tempSelectIndex));
                        selectListSetting();
                    }
//1111
                }
                break;
            case R.id.lay_gif_camera_editor3_save:
            case R.id.imv_gif_camera_editor3_save:
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.unservice_wait), Toast.LENGTH_SHORT).show();
                break;
            case R.id.lay_gif_camera_editor3_album:
            case R.id.imv_gif_camera_editor3_album:
//                doTakeAlbumAction();
                doTakePictureAlbumAction();
                break;
            case R.id.lay_gif_camera_editor3_text:
            case R.id.imv_gif_camera_editor3_text:
//                stv.setDraw(false);
                ChangeViews(INIT_VIEWS_EDITOR_2_TEXT);
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.unservice_wait), Toast.LENGTH_SHORT).show();
                break;
            case R.id.lay_gif_camera_editor3_part:
            case R.id.imv_gif_camera_editor3_part:
                ChangeViews(INIT_VIEWS_EDITOR_2_PART_GIF);
                break;
            case R.id.lay_gif_camera_water_mark:
                switch (waterNavi) {
                    case 0:
                        waterNavi = 1;
                        imvWater.setImageResource(R.drawable.water_mark);
                        break;
                    case 1:
                        waterNavi = 0;
                        imvWater.setImageResource(R.drawable.non_water_mark);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;
                    case 8:

                        break;
                    case 9:

                        break;
                }
                break;
            case R.id.lay_gif_camera_part_point_1:
                initPen(0);
                dv.setWidth(30);
                dv.invalidate();
                break;
            case R.id.lay_gif_camera_part_point_2:
                initPen(1);
                dv.setWidth(60);
                dv.invalidate();
                break;
            case R.id.lay_gif_camera_part_point_3:
                initPen(2);
                dv.setWidth(100);
                dv.invalidate();
                break;
            case R.id.lay_gif_camera_part_undo:
                if (isReverse) {
                    Frame frame = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1);
                    b = frame.getbitmap();
                } else {
                    Frame frame = wholeFrames.get(0).get(0);
                    b = frame.getbitmap();
                }
                Bitmap scaleB = null;

                if (isWidth(b)) {
                    int width = imvPlayer.getWidth();
                    int height = b.getHeight() * imvPlayer.getWidth() / b.getWidth();
                    scaleB = Bitmap.createScaledBitmap(b, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = b.getWidth() * imvPlayer.getHeight() / b.getHeight();
                    scaleB = Bitmap.createScaledBitmap(b, width, height, false);
                }

                dv.onClickUndo(scaleB);
                break;
            case R.id.lay_gif_camera_part_init:
                if (isReverse) {
                    Frame frame = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1);
                    b = frame.getbitmap();
                } else {
                    Frame frame = wholeFrames.get(0).get(0);
                    b = frame.getbitmap();
                }
                Bitmap scaleB2 = null;

                if (isWidth(b)) {
                    int width = imvPlayer.getWidth();
                    int height = b.getHeight() * imvPlayer.getWidth() / b.getWidth();
                    scaleB2 = Bitmap.createScaledBitmap(b, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = b.getWidth() * imvPlayer.getHeight() / b.getHeight();
                    scaleB2 = Bitmap.createScaledBitmap(b, width, height, false);
                }
                dv.reset(scaleB2);
                dv.invalidate();
                break;
            case R.id.lay_gif_camera_part_bottom_x:
                isPart = false;
                ChangeViews(INIT_VIEWS_EDITOR_2_DELETE_AND_COPY);
                break;
            case R.id.lay_gif_camera_part_bottom_o:
                if (dv.getPaths().size() > 1) {
                    isPart = true;
                } else {
                    isPart = false;
                }
                ChangeViews(INIT_VIEWS_EDITOR_2_DELETE_AND_COPY);
                break;
            case R.id.lay_gif_camera_text_bottom_o:
                //TODO ok
                isText = true;
                textB = stv.getBitmap();
//                textB = stv.getTextBitmap();

//                try {
//                    textB.compress(Bitmap.CompressFormat.PNG,100,new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/picpic/_1.png")));
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }

//                Log.e("test0", imvPlayer.getWidth() + "/" + imvPlayer.getHeight());
//                float ratioWidth = 0;
//                float ratioHeight = 0;
//
//                ratioWidth = 480f / imvPlayer.getWidth();
//                if(is11){
//                    ratioHeight = 480f / imvPlayer.getHeight();
//                }else{
//                    ratioHeight = 640f / imvPlayer.getHeight();
//                }
//
//                Log.e("test", ratioWidth + "/" + ratioHeight);
//                Log.e("test2", textB.getWidth() + "/" + textB.getHeight());


                if (isLoadToGif) {
                    Bitmap bitmap = null;
                    if (isReverse) {
                        bitmap = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();
                    } else {
                        bitmap = wholeFrames.get(0).get(0).getbitmap();
                    }
//                    textB = Bitmap.createScaledBitmap(textB, bitmap.getWidth(), bitmap.getHeight(), false);

                    Bitmap scaleB1 = null;

                    int widthb = 0;
                    int heightb = 0;
//
                    if (isWidth(bitmap)) {
                        widthb = imvPlayer.getWidth();
                        heightb = bitmap.getHeight() * imvPlayer.getWidth() / bitmap.getWidth();
                        Log.e("sizessss0", widthb + "," + heightb);
//                        scaleB1 = Bitmap.createScaledBitmap(textB, widthb, heightb, false);
                    } else {
                        heightb = imvPlayer.getHeight();
                        widthb = bitmap.getWidth() * imvPlayer.getHeight() / bitmap.getHeight();
                        Log.e("sizessss0", widthb + "," + heightb);
//                        scaleB1 = Bitmap.createScaledBitmap(textB, widthb, heightb, false);
                    }
//
//                    Log.e("sizessss1", textB.getWidth() + "," + textB.getHeight());
//                    Log.e("sizessss2", bitmap.getWidth() + "," + bitmap.getHeight());
//                    Log.e("sizessss3", imvPlayer.getWidth() + "," + imvPlayer.getHeight());
////                    Log.e("sizessss4", scaleB1.getWidth() + "," + scaleB1.getHeight());
//
////                    textB = scaleB1;
//
                    int x = (imvPlayer.getWidth() - widthb) / 2;
                    int y = (imvPlayer.getHeight() - heightb) / 2;
                    try {
                        textB = Bitmap.createBitmap(textB, x, y, widthb, heightb);
                    } catch (IllegalArgumentException e) {
                        textB = Bitmap.createBitmap(textB, 100, 100, widthb, heightb);
                    }
                    textB = Bitmap.createScaledBitmap(textB, bitmap.getWidth(), bitmap.getHeight(), false);
//                    Log.e("sizessss5", x + "," + y);
//                    Log.e("sizessss6", textB.getWidth() + "," + textB.getHeight());


//                    textB = Bitmap.createScaledBitmap(textB, bitmap.getWidth(), bitmap.getHeight(), false);
                } else {
                    if (is11) {
                        textB = Bitmap.createScaledBitmap(textB, 480, 480, false);
                    } else {
                        textB = Bitmap.createScaledBitmap(textB, 480, 640, false);
                    }
                }
                StickerData sd = stv.getData();
//                StickerData sd = stv.getData();
//
//                mMatrix = sd.getmMatrix();
//
//                float[] tempF = new float[10];
//                sd.getmMatrix().getValues(tempF);
//                for(int i=0; i<tempF.length; i++){
//                    Log.e("original"+i, tempF[i] + "");
//                    if(i%2 == 0){
//                        tempF[i] = tempF[i]*ratioWidth;
//                    }else{
//                        tempF[i] = tempF[i]*ratioHeight;
//                    }
//                    Log.e("add"+i, tempF[i] + "");
//                }
//                mMatrix.setValues(tempF);
                mMatrix = sd.getmMatrix();
                mPaint = sd.getmPaint();

                ChangeViews(INIT_VIEWS_EDITOR_2_DELETE_AND_COPY);
                break;
            case R.id.lay_gif_camera_text_bottom_x:
                isText = false;
                ChangeViews(INIT_VIEWS_EDITOR_2_DELETE_AND_COPY);
                break;
            case R.id.lay_gif_camera_text_edit:
                layEdit.setVisibility(View.VISIBLE);
//                edtEdit.setText(stickerTextView.getText().toString());
                edtEdit.setText(tempTv.getText().toString());
                edtEdit.requestFocus();
                edtEdit.setSelection(edtEdit.getText().toString().length());
                break;
            case R.id.imv_gif_camera_text_input_ok:

                stickerStr = edtEdit.getText().toString();
                commitSticker();
//                stickerTextView.setText(edtEdit.getText().toString());
                layEdit.setVisibility(View.GONE);
                break;
            case R.id.lay_gif_camera_text_font:

                break;
            case R.id.imv_gif_camera_text_color_1:
                stickerColorSetting(0);
                break;
            case R.id.imv_gif_camera_text_color_2:
                stickerColorSetting(1);
                break;
            case R.id.imv_gif_camera_text_color_3:
                stickerColorSetting(2);
                break;
            case R.id.imv_gif_camera_text_color_4:
                stickerColorSetting(3);
                break;
            case R.id.imv_gif_camera_text_color_5:
                stickerColorSetting(4);
                break;
            case R.id.imv_gif_camera_text_color_6:
                stickerColorSetting(5);
                break;
            case R.id.imv_gif_camera_text_color_7:
                stickerColorSetting(6);
                break;
            case R.id.imv_gif_camera_text_color_8:
                stickerColorSetting(7);
                break;
            case R.id.imv_gif_camera_text_color_9:
                stickerColorSetting(8);
                break;
            case R.id.imv_gif_camera_text_color_10:
                stickerColorSetting(9);
                break;
            case R.id.imv_gif_camera_text_color_11:
                stickerColorSetting(10);
                break;
            case R.id.imv_gif_camera_text_color_12:
                stickerColorSetting(11);
                break;
            case R.id.imv_gif_camera_text_color_13:
                stickerColorSetting(12);
                break;
            case R.id.imv_gif_camera_text_color_14:
                stickerColorSetting(13);
                break;
            case R.id.imv_gif_camera_text_color_15:
                stickerColorSetting(14);
                break;
            case R.id.imv_gif_camera_text_color_16:
                stickerColorSetting(15);
                break;
            case R.id.imv_gif_camera_text_color_17:
                stickerColorSetting(16);
                break;
            case R.id.imv_gif_camera_text_color_18:
                stickerColorSetting(17);
                break;
            case R.id.imv_gif_camera_text_color_19:
                stickerColorSetting(18);
                break;
            case R.id.imv_gif_camera_text_color_20:
                stickerColorSetting(19);
                break;
            case R.id.lay_gif_camera_filter_1:
                initFilterLays(0);
                break;
            case R.id.lay_gif_camera_filter_2:
                initFilterLays(1);
                break;
            case R.id.lay_gif_camera_filter_3:
                initFilterLays(2);
                break;
            case R.id.lay_gif_camera_filter_4:
                initFilterLays(3);
                break;
            case R.id.lay_gif_camera_filter_5:
                initFilterLays(4);
                break;
            case R.id.lay_gif_camera_filter_6:
                initFilterLays(5);
                break;
            case R.id.lay_gif_camera_filter_7:
                initFilterLays(6);
                break;
            case R.id.lay_gif_camera_filter_8:
                initFilterLays(7);
                break;
            case R.id.lay_gif_camera_filter_9:
                initFilterLays(8);
                break;
            case R.id.lay_gif_camera_filter_10:
                initFilterLays(9);
                break;
            case R.id.lay_gif_camera_filter_11:
                initFilterLays(10);
                break;
            case R.id.lay_gif_camera_filter_12:
                initFilterLays(11);
                break;
            case R.id.lay_gif_camera_filter_13:
                initFilterLays(12);
                break;
            case R.id.lay_gif_camera_filter_14:
                initFilterLays(13);
                break;
            case R.id.lay_gif_camera_filter_15:
                initFilterLays(14);
                break;
            case R.id.lay_gif_camera_filter_16:
                initFilterLays(15);
                break;
            case R.id.lay_gif_camera_filter_17:
                initFilterLays(16);
                break;
            case R.id.lay_gif_camera_filter_18:
                initFilterLays(17);
                break;
            case R.id.lay_gif_camera_filter_19:
                initFilterLays(18);
                break;
        }
    }

    private void initFilterLays(int a) {
        for (int i = 0; i < layFilters.size(); i++) {
            layFilters.get(i).setSelected(false);
        }
        layFilters.get(a).setSelected(true);

        if (a != 0) {
            switchFilterTo(createFilterForType(GIFCameraActivity.this, ff[a]));
            imvPlayer.requestRender();
            mFilter = createFilterForType(GIFCameraActivity.this, ff[a]);

            if (isPart) {
//                Bitmap b2 = dv.getmBitmap();
                Bitmap b2 = null;
                if (isReverse) {
                    b2 = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();
                } else {
                    b2 = wholeFrames.get(0).get(0).getbitmap();
                }
                Bitmap tempB = b2;
                Bitmap scaleB = null;

                if (isWidth(tempB)) {
                    int width = imvPlayer.getWidth();
                    int height = tempB.getHeight() * imvPlayer.getWidth() / tempB.getWidth();
                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = tempB.getWidth() * imvPlayer.getHeight() / tempB.getHeight();
                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);
                }
                ArrayList<Path> paths = dv.getPaths();
                GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
                renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                PixelBuffer buffer = new PixelBuffer(scaleB.getWidth(), scaleB.getHeight());
                buffer.setRenderer(renderer);
                renderer.setImageBitmap(scaleB, false);
                scaleB = buffer.getBitmap();
//                dv.setmBitmap(b2, dv.getXX(), dv.getYY());
                dv.setmBitmap(scaleB);
                dv.setPaths(paths);
//                                                mFilter.destroy();
                renderer.deleteImage();
                buffer.destroy();
            }

        } else {
            switchFilterTo(new GPUImageFilter());
            mFilter = null;

            if (isPart) {
//                Bitmap b2 = dv.getmBitmap();
                Bitmap b2 = null;
                if (isReverse) {
                    b2 = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();
                } else {
                    b2 = wholeFrames.get(0).get(0).getbitmap();
                }
                Bitmap tempB = b2;
                Bitmap scaleB = null;

                if (isWidth(tempB)) {
                    int width = imvPlayer.getWidth();
                    int height = tempB.getHeight() * imvPlayer.getWidth() / tempB.getWidth();
                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);
                } else {
                    int height = imvPlayer.getHeight();
                    int width = tempB.getWidth() * imvPlayer.getHeight() / tempB.getHeight();
                    scaleB = Bitmap.createScaledBitmap(tempB, width, height, false);
                }
                ArrayList<Path> paths = dv.getPaths();
                GPUImageRenderer renderer = new GPUImageRenderer(mFilter);
                renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                PixelBuffer buffer = new PixelBuffer(scaleB.getWidth(), scaleB.getHeight());
                buffer.setRenderer(renderer);
                renderer.setImageBitmap(scaleB, false);
                scaleB = buffer.getBitmap();
//                dv.setmBitmap(b2, dv.getXX(), dv.getYY());
                dv.setmBitmap(scaleB);
                dv.setPaths(paths);
//                                                mFilter.destroy();
                renderer.deleteImage();
                buffer.destroy();
            }
        }
    }

    private void stickerColorSetting(int selectColor) {
        int[] colors = getResources().getIntArray(R.array.gif_camera_text_colors);
        stickerColor = colors[selectColor];

//        stickerTextView.setColor(stickerColor);

//        LinearLayout.LayoutParams parmas = pickColors.get(0).getLayoutParams();
        LinearLayout.LayoutParams marginParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        marginParams.topMargin = 20;
        LinearLayout.LayoutParams nonMarginParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        nonMarginParams.topMargin = 0;

        for (int i = 0; i < pickColors.size(); i++) {
//            pickColors.get(i).setLayoutParams();
            if (i == selectColor) {
                pickColors.get(i).setLayoutParams(nonMarginParams);
            } else {
                pickColors.get(i).setLayoutParams(marginParams);
            }
        }

        commitSticker();
    }

    private void stickerFontSetting() {
//        stickerTypeface = Typeface.createFromFile();
    }

    private void commitSticker() {

        StickerData sd = stv.getData();

        Log.e("scale", sd.getmStickerScaleSize() + "");

        tempTv = new TextView(GIFCameraActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tempTv.setLayoutParams(layoutParams);
        tempTv.setTextSize(100);
//        tempTv.setTextSize(tempTv.getTextSize()*sd.getmStickerScaleSize());
        tempTv.setText(stickerStr);
        if (stickerTypeface != null) {
            tempTv.setTypeface(stickerTypeface);
        }
        tempTv.setTextColor(stickerColor);
        tempTv.setBackgroundColor(Color.TRANSPARENT);
        tempTv.measure(0, 0);

        int width = tempTv.getMeasuredWidth();
        int height = tempTv.getMeasuredHeight();
        Bitmap testB;
//tempTv.getMeasuredWidth()
        testB = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(testB);
        tempTv.layout(0, 0, width, height);
        tempTv.draw(c);

        Bitmap tempB = null;
        if (isReverse) {
            tempB = wholeFrames.get(wholeFrames.size() - 1).get(wholeFrames.get(wholeFrames.size() - 1).size() - 1).getbitmap();
        } else {
            tempB = wholeFrames.get(0).get(0).getbitmap();
        }
//        dv.get

        Bitmap scaleB = null;

        if (isWidth(tempB)) {
            int widthb = imvPlayer.getWidth();
            int heightb = tempB.getHeight() * imvPlayer.getWidth() / tempB.getWidth();
            scaleB = Bitmap.createScaledBitmap(tempB, widthb, heightb, false);
        } else {
            int heightb = imvPlayer.getHeight();
            int widthb = tempB.getWidth() * imvPlayer.getHeight() / tempB.getHeight();
            scaleB = Bitmap.createScaledBitmap(tempB, widthb, heightb, false);
        }

        stv.setWaterMark(testB);
        stv.setData(sd);
    }

    private void initPen(int a) {
        layPoint1.setSelected(false);
        layPoint2.setSelected(false);
        layPoint3.setSelected(false);
        switch (a) {
            case 0:
                layPoint1.setSelected(true);
                break;
            case 1:
                layPoint2.setSelected(true);
                break;
            case 2:
                layPoint3.setSelected(true);
                break;
        }
    }

    private Bitmap overlayMark(Bitmap baseBmp, Bitmap overlayBmp) {
//        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth() +
//                        distanceLeft, baseBmp.getHeight() + distanceTop,
//                baseBmp.getConfig());
        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth(), baseBmp.getHeight(),
                baseBmp.getConfig());
        Canvas canvas = new Canvas(resultBmp);
        canvas.drawBitmap(baseBmp, 0, 0, null);
        canvas.drawBitmap(overlayBmp, baseBmp.getWidth() - overlayBmp.getWidth() - 14, baseBmp.getHeight() - overlayBmp.getHeight() - 14, null);
        return resultBmp;
    }

    private Bitmap textMark(Bitmap baseBmp, Bitmap textBmp) {
        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth(), baseBmp.getHeight(),
                baseBmp.getConfig());
        Canvas canvas = new Canvas(resultBmp);
        canvas.drawBitmap(baseBmp, 0, 0, null);
//        if (is11) {
//            canvas.drawBitmap(baseBmp, 480, 480, null);
//        } else {
//            canvas.drawBitmap(baseBmp, 480, 640, null);
//        }

        int nowWidth = 480;
        int nowHeight = 0;
        if (is11) {
            nowHeight = 480;
        } else {
            nowHeight = 640;
        }
        int beforeWidth = stv.getWidth();
        int beforeHeight = stv.getHeight();

        float ratioWidth = (float) nowWidth / (float) beforeWidth;
        float ratioHeight = (float) nowHeight / (float) beforeHeight;

        float[] tempmPoints = stv.getData().getmPoints();
        for (int i = 0; i < tempmPoints.length; i++) {
            if (i % 3 == 0 || i % 3 == 1) {
                tempmPoints[i] = tempmPoints[i] * ratioWidth;
            } else {
                tempmPoints[i] = tempmPoints[i] * ratioHeight;
            }
        }

        float[] tempOriginPoints = stv.getData().getmOriginPoints();
        for (int i = 0; i < tempOriginPoints.length; i++) {
            if (i % 3 == 0 || i % 3 == 1) {
                tempOriginPoints[i] = tempOriginPoints[i] * ratioWidth;
            } else {
                tempOriginPoints[i] = tempOriginPoints[i] * ratioHeight;
            }
        }

        RectF tempRectF = stv.getData().getmOriginContentRect();
        tempRectF.right = tempRectF.right * ratioWidth;
        tempRectF.bottom = tempRectF.bottom * ratioHeight;

        mMatrix = stv.getData().getmMatrix();
        mPaint = stv.getData().getmPaint();

        mMatrix.mapPoints(tempmPoints, tempOriginPoints);
        mMatrix.mapRect(stv.getData().getmContentRect(), tempRectF);
        canvas.drawBitmap(textBmp, mMatrix, mPaint);

        return resultBmp;
    }

    private Bitmap overlayPart(Bitmap baseBmp, Bitmap overlayBmp) {
//        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth() +
//                        distanceLeft, baseBmp.getHeight() + distanceTop,
//                baseBmp.getConfig());
        Bitmap resultBmp = Bitmap.createBitmap(baseBmp.getWidth(), baseBmp.getHeight(),
                baseBmp.getConfig());
        Canvas canvas = new Canvas(resultBmp);
        canvas.drawBitmap(baseBmp, 0, 0, null);
        canvas.drawBitmap(overlayBmp, 0, 0, null);
        return resultBmp;
    }

    private void doSaveToTemp() {
        int tempCount = AppController.getSp().getInt("tempCount", 0);
        tempCount = tempCount + 1;
        File folder1 = new File(Environment.getExternalStorageDirectory(), "PicPic/temp/" + tempCount + "/");
        if (folder1.exists()) {

        } else {
            folder1.mkdirs();
        }

        for (int j = 0; j < wholeFrames.size(); j++) {
            for (int i = 0; i < wholeFrames.get(j).size(); i++) {

                File tempFile = wholeFrames.get(j).get(i).getFile();

                copyFile(tempFile, folder1.getAbsolutePath() + "/temp_" + j + "_" + i);

            }
        }
        AppController.getSe().putInt("tempCount", tempCount);
        AppController.getSe().commit();
        Toast.makeText(getApplicationContext(), "저장완료", Toast.LENGTH_SHORT).show();

        Handler handle = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                super.handleMessage(msg);
                try {
                    getTempDataForGridView();
                } catch (ArrayIndexOutOfBoundsException e) {

                }
            }
        };

        handle.sendEmptyMessageDelayed(0, 1500);
    }

    private boolean copyFile(File file, String save_file) {
        boolean result;
        if (file != null && file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                FileOutputStream newfos = new FileOutputStream(save_file);
                int readcount = 0;
                byte[] buffer = new byte[1024];
                while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
                    newfos.write(buffer, 0, readcount);
                }
                newfos.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    private void doTakeAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private void doTakePictureAlbumAction() {
//        Intent intent = new Intent(Intent.ACTION_PICK);
//        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
//        intent.setType("image/gif");
//        intent.setAction(Intent.ACTION_GET_CONTENT);

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        String type = "image/gif";
        intent.setDataAndType(data, type);

        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

//    private void doLoadToTemp() {
//        msd.animateOpen();
//    }

    private void doTakeGIFAlbumAction() {
        Intent intent = new Intent(Intent.ACTION_PICK);
//        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        intent.setType("image/gif");
        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private boolean isGIF(Uri uri) {

        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
//        String type = mime.getExtensionFromMimeType(cR.getType(uri));
        String type = cR.getType(uri);

//        String type = "";
//        String extention = MimeTypeMap.getFileExtensionFromUrl(uri.getPath());
////        MimeTypeMap.getSingleton().
//
//        if(extention != null){
//            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extention);
//        }
//
//        if(extention == null){
//            extention = "ㅠㅠ";
//        }
//
//        Log.e("mime_extention1", extention);
        Log.e("mime_extention2", type);
        return type.equals("image/gif");
    }

    public String getPathFromUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToNext();
        String path = cursor.getString(cursor.getColumnIndex("_data"));
        cursor.close();

        return path;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_FROM_ALBUM:
                    try {
                        mImageCaptureUri = data.getData();

                        Log.e("uri", mImageCaptureUri.toString());

                        if (isGIF(mImageCaptureUri)) {
                            if (wholeFrames.size() < 1) {
                                long nowCurrent = System.currentTimeMillis();

                                final File f2 = new File(AppController.cacheDir + "/" + nowCurrent + "_2.gif");

                                if (f2.length() <= 5 * 1024 * 1024) {
                                    GifDrawable gd = new GifDrawable(getContentResolver(), mImageCaptureUri);

                                    QAGIFEncoder encoder2 = new QAGIFEncoder();
                                    encoder2.setRepeat(0);
                                    encoder2.setDelay(1000 / speedNavi);
//
                                    encoder2.start(f2.getAbsolutePath());

                                    ArrayList<Frame> tempFrames = new ArrayList<>();

                                    for (int i = 0; i < gd.getNumberOfFrames(); i++) {
                                        final int finalI = i;
//                                    Bitmap b = gd.seekToPositionAndGet(i);
                                        Bitmap b = gd.seekToFrameAndGet(finalI);
//                                    Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
//                                    bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

//                                    b = overlayMark(b, bb);
                                        final File tempFile = new File(AppController.cacheDir + "/" + nowCurrent + "_" + i);
//                                        final File tempFile = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_" + i + ".jpg");
//                                        final File tempFile = new File(getCacheDir().getAbsolutePath() + "/picpic/" + nowCurrent + "_" + i + ".jpg");
                                        b.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(tempFile));
                                        final Bitmap finalB = b;
                                        tempFrames.add(new Frame() {
                                            @Override
                                            public File getFile() {
                                                return tempFile;
                                            }

                                            @Override
                                            public Bitmap getbitmap() {
                                                return finalB;
                                            }
                                        });
                                        encoder2.addFrame(b);
//                                        b.recycle();
//                                        b = null;
                                    }

                                    encoder2.finish();

                                    isLoadToGif = true;
                                    frameNavi = 1;
                                    seekFrame.setMax(3);

                                    imvNext.setSelected(true);
                                    wholeFrames.add(tempFrames);
                                    cameraNums.add(false);
                                    ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);

                                /*
//                                File tempF = new File(getPathFromUri(mImageCaptureUri));
                                File tempF = new File(String.valueOf(mImageCaptureUri));

                                long tempSize = 5 * 1024 * 1024;

                                if (tempF.length() < tempSize) {
                                    long nowCurrent = System.currentTimeMillis();
                                    File f = new File(getPathFromUri(mImageCaptureUri));
//                                    File f1 = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_1.gif");
                                    final File f2 = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_2.gif");
//                                    File f3 = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_3.gif");


//                                    copyFile(f, f1.getAbsolutePath());
//                                    copyFile(f, f2.getAbsolutePath());
//                                    copyFile(f, f3.getAbsolutePath());

//                                    final GifDrawable gd = new GifDrawable(f.getAbsolutePath());
////                                    gd.
////
                                    GifDrawable gg = new GifDrawable(getContentResolver(), mImageCaptureUri);

                                    QAGIFEncoder encoder2 = new QAGIFEncoder();
                                    encoder2.setRepeat(0);
                                    encoder2.setDelay(1000 / speedNavi);
//
                                    encoder2.start(f2.getAbsolutePath());

                                    ArrayList<Frame> tempFrames = new ArrayList<>();

                                    for (int i = 0; i < gd.getNumberOfFrames(); i++) {
                                        final int finalI = i;
                                        Bitmap b = gd.seekToPositionAndGet(finalI);
//                                        Bitmap b = gd.seekToFrameAndGet(finalI);
                                        Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                        bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                        b = overlayMark(b, bb);
                                        final File tempFile = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_" + i + ".jpg");
                                        b.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(tempFile));
                                        final Bitmap finalB = b;
                                        tempFrames.add(new Frame() {
                                            @Override
                                            public File getFile() {
                                                return tempFile;
                                            }

                                            @Override
                                            public Bitmap getbitmap() {
                                                return finalB;
                                            }
                                        });
                                        encoder2.addFrame(b);
                                    }

                                    encoder2.finish();

                                    isLoadToGif = true;
                                    frameNavi = 1;
                                    seekFrame.setMax(3);

                                    imvNext.setSelected(true);
                                    wholeFrames.add(tempFrames);
                                    cameraNums.add(false);
                                    ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);

//                                    Intent i = new Intent(GIFCameraActivity.this, WritePostActivity.class);
//                                    i.putExtra("url1", f1.getAbsolutePath());
//                                    i.putExtra("url2", f2.getAbsolutePath());
//                                    i.putExtra("url3", f3.getAbsolutePath());
//                                    i.putExtra("thumb", "");
//                                    i.putExtra("navi", tagNavi);
//                                    if (tagNavi == 1) {
//                                        i.putExtra("tag_id", tag_id);
//                                        i.putExtra("tag_title", tag_title);
//                                        i.putExtra("tag_body", tag_body);
//                                    }
//                                    startActivity(i);
//                                    finish();
//                                    overridePendingTransition(0, 0);
                                    */

                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif_size), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif), Toast.LENGTH_SHORT).show();
                            }
                        } else {
//                            mImageCaptureUri = data.getData();
////                    data.getData().gett
//                            Log.e("filepath", mImageCaptureUri.getPath());
//
//                            Intent intent = new Intent("com.android.camera.action.CROP");
//                            intent.setDataAndType(mImageCaptureUri, "image/*");
//
//                            if (is11) {
//                                intent.putExtra("aspectX", 1);
//                                intent.putExtra("aspectY", 1);
//                            } else {
//                                intent.putExtra("aspectX", 3);
//                                intent.putExtra("aspectY", 4);
//                            }
//                            intent.putExtra("output", Uri.fromFile(f));
//
//                            startActivityForResult(intent, CROP_FROM_ALBUM);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.cant_load_image), Toast.LENGTH_LONG).show();
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
//                    }else {
//                            mImageCaptureUri = data.getData();
////                    data.getData().gett
//                            Log.e("filepath", mImageCaptureUri.getPath());
//
//                            Intent intent = new Intent("com.android.camera.action.CROP");
//                            intent.setDataAndType(mImageCaptureUri, "image/*");
//
//                            if (is11) {
//                                intent.putExtra("aspectX", 1);
//                                intent.putExtra("aspectY", 1);
//                            } else {
//                                intent.putExtra("aspectX", 3);
//                                intent.putExtra("aspectY", 4);
//                            }
//                            intent.putExtra("output", Uri.fromFile(f));
//
//                            startActivityForResult(intent, CROP_FROM_ALBUM);
//                        }
                    } catch (IllegalStateException e) {
                        File tempF = new File(String.valueOf(mImageCaptureUri));

                        long tempSize = 5 * 1024 * 1024;

                        if (tempF.length() < tempSize) {
                            long nowCurrent = System.currentTimeMillis();
                            File f = new File(getPathFromUri(mImageCaptureUri));

                            final File f2 = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_2.gif");

                            copyFile(f, f2.getAbsolutePath());

//                            Intent i = new Intent(GIFCameraActivity.this, WritePostActivity.class);
                            Intent i = new Intent(GIFCameraActivity.this, WritePostActivityV1.class);
                            i.putExtra("url2", f2.getAbsolutePath());
                            i.putExtra("thumb", "");
                            i.putExtra("navi", tagNavi);
                            if (tagNavi == 1) {
                                i.putExtra("tag_id", tag_id);
                                i.putExtra("tag_title", tag_title);
                                i.putExtra("tag_body", tag_body);
                            }
                            startActivity(i);
                            finish();
                            overridePendingTransition(0, 0);
                        }
                    } catch (Exception e) {
                        Log.e("e", e.toString());
                    }
//                    File tempFile = new File(mImageCaptureUri.getPath());
//
//                    if(tempFile instanceof Gif)
                    /*
                    Intent data = new Intent(GIFCameraActivity.this, WritePostActivity.class);
                    data.putExtra("url1", getPath1());
                    data.putExtra("url2", getPath2());
                    data.putExtra("navi", tagNavi);
                    if (tagNavi == 1) {
                        data.putExtra("tag_id", tag_id);
                        data.putExtra("tag_title", tag_title);
                        data.putExtra("tag_body", tag_body);
                    }

                    instanceof
                    */
                    break;
                case CROP_FROM_ALBUM:
                    long atomSize1 = AtomUtils.encodeAtomJPEG(f.getAbsolutePath(), atomFF.getAbsolutePath(), 480, 640);

                    QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                    opt.inSampleSize = 1;
                    opt.inPreferredConfig = QrBitmapFactory.Options.Config.ARGB_8888;
//                BitmapFactory.Options opt = new BitmapFactory.Options();
                    opt.inSampleSize = 1;
//                opt.inPreferredConfig = BitmapFactory.Options.

                    Bitmap photo = QrBitmapFactory.decodeFile(atomFF.getAbsolutePath(), opt);
                    Bitmap resize = null;
                    if (is11) {
                        resize = Bitmap.createScaledBitmap(photo, 480, 480, false);
                    } else {
                        resize = Bitmap.createScaledBitmap(photo, 480, 640, false);
                    }

//                    OutputStream out = null;
//
//                    String tempFileName = String.valueOf(System.currentTimeMillis());
//                    final File tempFile = new File(getCacheDir().getAbsolutePath() + "/PicPic/" + tempFileName + ".jpg");
//
//                    try {
//                        tempFile.createNewFile();
//                        out = new FileOutputStream(tempFile);
//
//                        resize.compress(Bitmap.CompressFormat.JPEG, 100, out);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    } finally {
//                        try {
//                            out.close();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }

                    frames = new ArrayList<>();

                    final Bitmap tempBitmap = resize;

                    frames.add(new Frame() {

                        @Override
                        public File getFile() {
                            return atomFF;
                        }

                        @Override
                        public Bitmap getbitmap() {
//                            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                            Bitmap b = QrBitmapFactory.decodeFile(tempFile.getAbsolutePath(), opt);
//                            return b;
                            return tempBitmap;
                        }
                    });

                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            wholeFrames.add(frames);
                            total = total + delay;
                        }
                    });

                    cameraNums.add(false);

                    nowProgressNavi = nowProgressNavi + 1;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layProgress.addView(new ImageView(GIFCameraActivity.this));

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, ViewGroup.LayoutParams.MATCH_PARENT);

                            if (layProgress.getChildCount() > 1) {
                                params.setMargins(3, 0, 0, 0);
                            }

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(1);

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_before));

                            layProgress.invalidate();

                        }
                    });
//                    progressNavi = progressNavi + 1;
                    if (!isShootingMode) {
                        sectionArr.add(new int[]{0, 0});
                        selectListSetting();
                    } else {
                        isPhoto = true;
                    }

//                //TODO jpg 파일 저장후 경로 저장
//                f.getAbsolutePath();
                    Log.e("FrameSize", wholeFrames.get(wholeFrames.size() - 1).size() + "");
                    Log.e("wFrameSize", wholeFrames.size() + "");

                    isShootingMode = true;
                    recording = false;

                    break;
            }
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            tvTutorial.setVisibility(View.GONE);

            if (mCamera != null) {
                mCamera.cancelAutoFocus();
            }

            if (isShootingMode == true) {
                try {
                    mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
                    mCamera.setParameters(mParameters);
                } catch (RuntimeException e) {

                }
                for (int i = 0; i < layProgress.getChildCount(); i++) {
                    layProgress.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.min_progress_before));
                }

                imvReturn.setSelected(false);

                if (isShootingMode) {
                    Log.e("whole Size", wholeFrames.size() + "");
                    Log.e("w", w + "");
                    Log.e("width", layProgress.getWidth() + "");

                    for (int i = 0; i < wholeFrames.size(); i++) {
                        Log.e("index" + i + " size", wholeFrames.get(i).size() + "");
                    }

                    cameraNums.add(!(nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK));

                    Log.e("cameraaaaaa", !(nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) + "");

                    isOK = false;
                    if (executor.isShutdown()) {
                        executor = Executors.newFixedThreadPool(1);
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("ex", "ggggg");
                                frames = new ArrayList<>();

                            }
                        });
                    } else {
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("ex", "ggggg");
                                frames = new ArrayList<>();

                            }
                        });
                    }
                    imvRatio.setFocusable(false);
                    imvRatio.setClickable(false);
                    int width = layProgress.getWidth();
                    int totalWidth = 0;
                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                        totalWidth += layProgress.getChildAt(i).getWidth();
                    }
                    if (width > totalWidth) {
                        if (!recording) {
                            lastTime = 0;
                            recording = true;
                            total = total + delay;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    layProgress.addView(new ImageView(GIFCameraActivity.this));

                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, ViewGroup.LayoutParams.MATCH_PARENT);

                                    if (layProgress.getChildCount() > 1) {
                                        params.setMargins(3, 0, 0, 0);
                                    }

                                    layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                                    layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(1);

                                    layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_before));

                                    layProgress.invalidate();
                                }
                            });


                            mTaks = new TimerTask() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            recording = true;
                                            total = total + delay;
                                            int proNavi = (int) (layProgress.getChildAt(layProgress.getChildCount() - 1).getTag());
                                            proNavi = proNavi + 1;
                                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w * proNavi, ViewGroup.LayoutParams.MATCH_PARENT);
                                            if (layProgress.getChildCount() > 1) {
                                                params.setMargins(3, 0, 0, 0);
                                            }
                                            layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(proNavi);
                                            layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                                            layProgress.invalidate();

                                            int tempSize = 0;

                                            if (layProgress.getChildCount() != 0) {
                                                for (int i = 0; i < layProgress.getChildCount(); i++) {
                                                    tempSize += layProgress.getChildAt(i).getWidth();
                                                }
                                            } else {
                                                tempSize = layProgress.getChildAt(0).getWidth();
                                            }

                                            if (tempSize > 50) {
                                                imvNext.setSelected(true);
                                            } else {
                                                imvNext.setSelected(false);
                                            }

                                        }
                                    });

                                }
                            };

                            if (!isShot) {
                                mTimer = new java.util.Timer();

                                mTimer.schedule(mTaks, 0, (int) delay);
                            }
                        }

                    } else {
                        recording = false;
                    }
                } else {
                    if (isPlaying) {
                        stopPlay();
                    } else {
                        startPlay();
                    }
                }
            }
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (tagNavi == 2) {
                int tempSize = 0;

                if (layProgress.getChildCount() != 0) {
                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                        tempSize += layProgress.getChildAt(i).getWidth();
                    }
                } else {
                    tempSize = layProgress.getChildAt(0).getWidth();
                }

                if (tempSize > 50) {
                    initGuide(1);
                }
            }

            if (isShootingMode == true) {
                recording = false;
                if (!isShot) {
                    if (mTimer != null) {
                        mTimer.cancel();
                    }
                }
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (frames.size() > 0) {
                            wholeFrames.add(frames);
                            nowProgressNavi = nowProgressNavi + 1;
                        }

                    }
                });
                try {
                    mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                    mCamera.setParameters(mParameters);
                } catch (RuntimeException e) {

                }

            }

        }
        return true;
    }

    public void startPreview() {
        try {
            mCamera.startPreview();
            isPreviewRunning = true;
//            mCamera.startFaceDetection();
//            startFaceDetection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            int numberOfCameras = Camera.getNumberOfCameras();
            Log.e("cameraNum", numberOfCameras + "");
            mCameraInfo = new Camera.CameraInfo();

            for (int i = 0; i < numberOfCameras; i++) {
                Camera.getCameraInfo(i, mCameraInfo);
                if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    mCamera = Camera.open(i);
                    nowCameraNum = i;
                }
            }

            mParameters = mCamera.getParameters();

            if (mParameters.getMaxNumDetectedFaces() > 0) {
                mCamera.setFaceDetectionListener(new MyFaceDetectionListener());
            }

            List<Integer> formats = mParameters.getSupportedPreviewFormats();
            for (int format : formats) {
                if (format == ImageFormat.RGB_565) {
                    mParameters.setPreviewFormat(format);
                    break;
                }
            }
            mPreviewFormat = mParameters.getPreviewFormat();

            List<Camera.Size> sizes = mParameters.getSupportedPreviewSizes();

            for (int i = 0; i < sizes.size(); i++) {
                Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
            }

            for (int i = sizes.size() - 1; i >= 0; i--) {
                boolean isSizeOK = false;
                Camera.Size size = sizes.get(i);
                if (size.width == 640 && size.height == 480) {
                    mParameters.setPreviewSize(size.width, size.height);
                    isSizeOK = true;
                    Log.e("size", size.width + "/" + size.height);
                    break;
                }

                if (!isSizeOK) {
                    Camera.Size size2 = sizes.get(0);
                    mParameters.setPreviewSize(size2.width, size2.height);
                    isSizeOK = true;
                    Log.e("size", size.width + "/" + size.height);
                }

            }


            mPreviewSize = mParameters.getPreviewSize();

            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);

//            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

            mCamera.setParameters(mParameters);
            mCamera.setPreviewCallback(this);


            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
        } catch (Exception e) {

        }
    }

    public void flashToggle(boolean is) {
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            nowCameraNum = (nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                    Camera.CameraInfo.CAMERA_FACING_BACK : Camera.CameraInfo.CAMERA_FACING_FRONT;

            mCamera = Camera.open(nowCameraNum);

            if (nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == 640 && size.height == 480) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }


                if (!is) {
                    mParameters.setFlashMode("off");
                    isFlashOn = false;
                    imvFlash.setSelected(false);
                } else {
                    mParameters.setFlashMode("torch");
                    isFlashOn = true;
                    imvFlash.setSelected(true);
                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
        } catch (Exception e) {

        }
    }

    public void initCamera() {
        isChangeingCamera = true;
        try {

            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            nowCameraNum = (nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                    Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;


            mCamera = Camera.open(nowCameraNum);

            if (nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == 640 && size.height == 480) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
            isChangeingCamera = false;
        } catch (Exception e) {

        }
    }

    public void switchCamera() {
        isChangeingCamera = true;
        try {

            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            nowCameraNum = (nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                    Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;


            mCamera = Camera.open(nowCameraNum);

            if (nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == 640 && size.height == 480) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
            isChangeingCamera = false;
        } catch (Exception e) {

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            if (isPreviewRunning) {
                mCamera.stopPreview();
            }

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);
        } catch (Exception e) {

        }

//        int numberOfCameras = Camera.getNumberOfCameras();
//        Log.e("cameraNum", numberOfCameras + "");
//        mCameraInfo = new Camera.CameraInfo();
//
//        for (int i = 0; i < numberOfCameras; i++) {
//            Camera.getCameraInfo(i, mCameraInfo);
//            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
//                mCamera = Camera.open(i);
//                nowCameraNum = i;
//            }
//        }
//
//        int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
//        int result;
//        if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//            result = (mCameraInfo.orientation + degrees) % 360;
//            result = (360 - result) % 360; // compensate the mirror
//        } else { // back-facing
//            result = (mCameraInfo.orientation - degrees + 360) % 360;
//        }
//        mCamera.setDisplayOrientation(result);
//
//        mParameters = mCamera.getParameters();
//
//        List<Integer> formats = mParameters.getSupportedPreviewFormats();
//        for (int format1 : formats) {
//            if (format1 == ImageFormat.RGB_565) {
//                mParameters.setPreviewFormat(format1);
//                break;
//            }
//        }
//        mPreviewFormat = mParameters.getPreviewFormat();
//
//        List<Camera.Size> sizes = mParameters.getSupportedPreviewSizes();
//
//        for (int i = 0; i < sizes.size(); i++) {
//            Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
//        }
//
//        for (int i = sizes.size() - 1; i >= 0; i--) {
//            boolean isSizeOK = false;
//            Camera.Size size = sizes.get(i);
//            if (size.width == 640 && size.height == 480) {
//                mParameters.setPreviewSize(size.width, size.height);
//                isSizeOK = true;
//                Log.e("size", size.width + "/" + size.height);
//                break;
//            }
//
//            if (!isSizeOK) {
//                Camera.Size size2 = sizes.get(0);
//                mParameters.setPreviewSize(size2.width, size2.height);
//                isSizeOK = true;
//                Log.e("size", size.width + "/" + size.height);
//            }
//
//        }
//
//        mPreviewSize = mParameters.getPreviewSize();
//
//        mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//
//        mCamera.setParameters(mParameters);
//        mCamera.setPreviewCallback(this);
//
//
//        try {
//            mCamera.setPreviewDisplay(holder);
//        } catch (IOException e) {
//            mCamera.release();
//            mCamera = null;
//        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPreviewFrame(final byte[] data, final Camera camera) {
        int width = layProgress.getWidth();
        int totalWidth = 0;
        for (int i = 0; i < layProgress.getChildCount(); i++) {
            totalWidth += layProgress.getChildAt(i).getWidth();
        }
        if (data == null || !recording) return;
        if (width <= totalWidth) return;
        long current = System.currentTimeMillis();
        if (lastTime == 0) {
            lastTime = current;
        } else if (current < nextTime) {
            return;
        }
        nextTime = lastTime + (int) delay;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (isShot) {
                    if (frames.size() != 0) {
                        return;
                    }
                }
                final String fileName1 = getCacheDir() + "/" + Long.toString(System.currentTimeMillis());
                final String atomName1 = getCacheDir() + "/atom_" + Long.toString(System.currentTimeMillis());
                Log.e("cachedir", getCacheDir().getAbsolutePath());
                final File f1 = new File(fileName1);
                final File atomF1 = new File(atomName1);
                try {
                    FileOutputStream fout1 = new FileOutputStream(f1);
                    switch (mPreviewFormat) {
                        case ImageFormat.RGB_565:
                            fout1.write(data);
                            fout1.close();
                            break;
                        default: // ImageFormat.YUV_420_888
                            byte[] rotated = PreviewUtils.rotateYUV420Degree90(data, mPreviewSize.width, mPreviewSize.height);

                            YuvImage image = new YuvImage(rotated, mPreviewFormat, mPreviewSize.height, mPreviewSize.width, null);
                            image.compressToJpeg(new Rect(0, 0, mPreviewSize.height, mPreviewSize.width), 100, fout1);

                            if (frames.size() == 0) {
                                previewNavi = previewNavi + 1;
                            }
                            try {
                                if (cameraNums.get(previewNavi)) {
                                    QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                                    Bitmap b = QrBitmapFactory.decodeFile(fileName1, opt);
                                    int w = b.getWidth();
                                    int h = b.getHeight();

                                    Matrix mtx = new Matrix();
                                    mtx.postRotate(180);

                                    Bitmap rotateB = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);

                                    Matrix sideInversion = new Matrix();
                                    sideInversion.setScale(-1, 1);
                                    Bitmap sideInversionImg = Bitmap.createBitmap(rotateB, 0, 0,
                                            rotateB.getWidth(), rotateB.getHeight(), sideInversion, false);

                                    QrBitmapFactory.compressToFile(sideInversionImg, QrBitmapFactory.QrJPEG, fileName1, 100, opt);
                                }
                            } catch (IndexOutOfBoundsException e) {

                            }
                            long atomSize1 = AtomUtils.encodeAtomJPEG(fileName1, atomF1.getAbsolutePath(), 0, 0);
//
                            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                            final Bitmap b = QrBitmapFactory.decodeFile(atomF1.getAbsolutePath(), opt);

                            Log.e("frames size", frames.size() + "");

                            frames.add(new Frame() {

                                @Override
                                public File getFile() {
                                    return atomF1;
                                }

                                @Override
                                public Bitmap getbitmap() {

                                    return b;
                                }

                            });

//                            if(wholeFrames.size() == 0){
//                                if(frames.size() > 4){
//                                    imvNext.setVisibility(View.VISIBLE);
//                                }else{
//                                    imvNext.setVisibility(View.GONE);
//                                }
//                            }else if(wholeFrames.size() == 1){
//                                if(wholeFrames.get(0).size() + frames.size() > 4){
//                                    imvNext.setVisibility(View.VISIBLE);
//                                }else{
//                                    imvNext.setVisibility(View.GONE);
//                                }
//                            }else{
//
//                            if(wholeFrames.size() != 0 && frames.size() > 4){
//
//                            }else{
//
//                            }

                    }
                    Log.i("Camera", String.format("frame added %dx%d", mPreviewSize.width, mPreviewSize.height));
                } catch (IOException e) {
                    f1.delete();
                    atomF1.delete();
                    e.printStackTrace();
                }
                recording = false;
            }
        });
        lastTime = current;

//        recording = false;

        if (!isOK) {
            isOK = true;
        }
    }

    private String getPath2() {

//        return output.getPath();
        if ("file".equals(output2.getScheme())) {
            return output2.getPath();
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(output2, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(idx);
        } finally {
            cursor.close();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void bindDrapListener(View v) {
        v.setOnTouchListener(mOnTouchListener);
        v.setOnDragListener(mOnDragListener);
    }

    private void startPlay() {
        isPlaying = false;

//        if (playTimer != null) {
//            Log.e("msg1", "playTimer1 Not null");
//            playTimer.cancel();
////            playTimerTask = null;
//            playTimer = new java.util.Timer();
//        } else {
//            Log.e("msg1", "playTimer1 Null");
//            playTimer = new java.util.Timer();
//        }
//
//        if (playTimerTask != null) {
//            playTimerTask.cancel();
//            playTimerTask = null;
//        }

        previewAnimation = new AnimationDrawable();

        playArray = new ArrayList<>();

        int duration = 1000 / speedNavi;

        if (!isReverse) {

            Log.e("whSize1", wholeFrames.size() + "");

            for (int i = 0; i < wholeFrames.size(); i++) {
                for (int j = sectionArr.get(i)[0]; j <= sectionArr.get(i)[1]; j = j + frameNavi) {
                    playArray.add(wholeFrames.get(i).get(j).getbitmap());
//                    previewAnimation.addFrame(new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap()),duration);
                    Log.e("msg1", "i:" + i);
                }
            }

        } else {
            for (int i = wholeFrames.size() - 1; i >= 0; i--) {
                for (int j = sectionArr.get(i)[1]; j >= sectionArr.get(i)[0]; j = j - frameNavi) {
                    //아웃오브 인덱스처리
                    playArray.add(wholeFrames.get(i).get(j).getbitmap());
//                    previewAnimation.addFrame(new BitmapDrawable(wholeFrames.get(i).get(j).getbitmap()),duration);
                    Log.e("msg1", "i:" + i);
                }
            }
        }

        imvPlayer.setImageArray(playArray, duration);

//        imvPlayer.set

//        previewAnimation.setb
//            previewAnimation.draw(new Canvas());
//        playTimerTask = new TimerTask() {
//            @Override
//            public void run() {
//                if (isPlaying) {
//                    Log.e("msg1", "playing1 true");
//                    try {
//                        imvPlayer.setImage(playArray.get(playNavi));
//                        Log.e("msg1", "pagenavi/" + playNavi);
//                    } catch (IndexOutOfBoundsException e) {
//                        Log.e("msg1", e.toString());
//                        cancel();
//                    }
//
//                    if (playNavi >= playArray.size() - 1) {
//                        playNavi = 0;
//                    } else {
//                        playNavi = playNavi + 1;
//                    }
//                }
//
//            }
//        };
//
//        playTimer.schedule(playTimerTask, 0, duration);

        isPlaying = true;
    }

    private void startPartPlay(final int index) {
        try {
//            if (playTimer != null) {
//                Log.e("msg2", "playTImer null");
//                playTimer.cancel();
////                    playTimerTask = null;
//                playTimer = new java.util.Timer();
//            } else {
//                Log.e("msg2", "playTImer Not Null");
//                playTimer = new java.util.Timer();
//            }
//
//            if (playTimerTask != null) {
//                playTimerTask.cancel();
//                playTimerTask = null;
//            }

            if (wholeFrames.size() > 0) {
                isPlaying = false;

                playArray = new ArrayList<>();

                int duration = 1000 / speedNavi;

                tempArrFrames = wholeFrames.get(index);

                if (!isReverse) {
                    if (sectionArr.get(index)[1] - sectionArr.get(index)[0] <= 0) {
                        for (int i = sectionArr.get(index)[0]; i <= sectionArr.get(index)[1]; i = i + frameNavi) {
                            playArray.add(tempArrFrames.get(i).getbitmap());
                            Log.e("msg2", "i:" + i);
                        }
                    } else {
                        for (int i = sectionArr.get(index)[0]; i < sectionArr.get(index)[1]; i = i + frameNavi) {
                            playArray.add(tempArrFrames.get(i).getbitmap());
                            Log.e("msg2", "i:" + i);
                        }
                    }

                } else {
                    for (int i = sectionArr.get(index)[1]; i >= sectionArr.get(index)[0]; i = i - frameNavi) {
                        playArray.add(tempArrFrames.get(i).getbitmap());
                        Log.e("msg2", "i:" + i);
                    }
                }

                imvPlayer.setImageArray(playArray, duration);

                isPlaying = true;

            }
        } catch (IndexOutOfBoundsException e) {
            for (int i = 0; i < sectionArr.size(); i++) {
                Log.e("iobe2" + i, sectionArr.get(i)[0] + "/" + sectionArr.get(i)[1]);
            }

            for (int i = 0; i < wholeFrames.size(); i++) {
                Log.e("whframe2" + i, wholeFrames.get(i).size() + "");
            }
        }
    }

    private void stopPlay() {
//        isPlaying = false;
//
//        if (playTimer != null) {
//            playTimer.cancel();
//            playTimerTask.cancel();
//        }
    }

    private void stopPartPlay() {
//        isPlaying = false;
//
//        if (playTimer != null) {
//            playTimer.cancel();
//            playTimerTask.cancel();
//        }
    }

    enum FilterType {
        I_AMARO, I_HEFE, I_NASHVILLE,
        I_SIERRA, I_INKWELL, I_VALENCIA,
        I_WALDEN, I_XPROII, BRIGHTNESS,
        I_BRANNAN, I_EARLYBIRD, I_HUDSON,
        I_LOMO, I_TOASTER,
        CONTRAST, VIGNETTE, TONE_CURVE,
        LOOKUP_AMATORKA
    }

    interface Frame {
        File getFile();

        Bitmap getbitmap();
    }

    static class Timer {
        long lastTime = System.currentTimeMillis();

        void logDelay(String tag) {
            long current = System.currentTimeMillis();
            Log.w("Timer", String.format("delay(%s): %d", tag, (current - lastTime)));
            lastTime = current;
        }
    }

    class FilterAdapter extends ArrayAdapter<Integer> {


        SparseArray<WeakReference<View>> viewArray;

        String[] filterName = {
                "Original", "Amaro", "Hefe", "Rise", "Sierra", "Inkwell", "Valencia", "Walden", "Xproll",
                "Brightness",
                "Brannan", "Earlybird", "Hudson", "Lomo", "Toaster", "Contrast", "Vignette", "ToneCurve",
                "Lookup (Amatorka)"
        };

        LayoutInflater m_LayoutInflater = null;

        public FilterAdapter(Context context, int resource) {
            super(context, resource);
            m_LayoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.viewArray = new SparseArray<WeakReference<View>>(ff.length);
        }

        @Override
        public int getCount() {
            return ff.length;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_gif_camera_filter, parent, false);

                GPUImageView gpuView = (GPUImageView) convertView.findViewById(R.id.gpuimage_gif_camera_row);
                TextView tv = (TextView) convertView.findViewById(R.id.tv_gif_camera_filter);

                gpuView.setImage(wholeFrames.get(0).get(0).getbitmap());

                if (pos == 0) {

                } else {
                    gpuView.setFilter(createFilterForType(GIFCameraActivity.this, ff[pos]));
                }

                tv.setText(filterName[pos]);

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }

//            holder.gpuView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (pos != 0) {
//                        switchFilterTo(createFilterForType(GIFCameraActivity.this, ff[pos]));
//                        imvPlayer.requestRender();
//                        mFilter = createFilterForType(GIFCameraActivity.this, ff[pos]);
//                    } else {
//                        switchFilterTo(new GPUImageFilter());
//                    }
//                }
//            });


            return convertView;
        }

    }

    public class BackPressCloseHandler {

        private long backKeyPressedTime = 0;
        private Toast toast;

        private Activity activity;

        public BackPressCloseHandler(Activity context) {
            this.activity = context;
        }

        public void onBackPressed() {
            if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                backKeyPressedTime = System.currentTimeMillis();
                showGuide();
                return;
            }
            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                activity.finish();
//                startActivity(new Intent(GIFCameraActivity.this, NewGridTimelineActivity.class));
                Intent i = new Intent(GIFCameraActivity.this, NewGridTimelineActivity.class);
                i.putExtra("navi", 0);
                startActivity(i);
                toast.cancel();
            }
        }


        private void showGuide() {
            toast = Toast.makeText(activity, "튜토리얼을 끝내야 PicPic을 시작할수 있어요. \'뒤로\'버튼을 한번 더 누르시면 튜토리얼이 종료됩니다.",
                    Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private class DrapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
            ClipData data = ClipData.newPlainText("", "");
            MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
                    mDrapView);
            mDrapView.startDrag(data, shadowBuilder, mDrapView, 0);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
                selectHListLayout.getChildAt(i).setSelected(false);
            }
            mDrapView.setSelected(true);
            isSelectAll = false;
            tempSelectIndex = (int) mDrapView.getTag();
            ms.setMin(0);
            ms.setMax(wholeFrames.get(tempSelectIndex).size() - 1);
            ms.getThumb(0).setValue(sectionArr.get(tempSelectIndex)[0]);
            ms.getThumb(1).setValue(sectionArr.get(tempSelectIndex)[1]);
            startPartPlay(tempSelectIndex);
            tvAll.setSelected(false);
            ms.setVisibility(View.VISIBLE);
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private class MyDragShadowBuilder extends View.DragShadowBuilder {

        private final WeakReference<View> mView;

        public MyDragShadowBuilder(View view) {
            super(view);
            mView = new WeakReference<View>(view);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            canvas.scale(1.5F, 1.5F);
            super.onDrawShadow(canvas);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {
            // super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);

            final View view = mView.get();
            if (view != null) {
                shadowSize.set((int) (view.getWidth() * 1.5F),
                        (int) (view.getHeight() * 1.5F));
                shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
            } else {
                // Log.e(View.VIEW_LOG_TAG,
                // "Asked for drag thumb metrics but no view");
            }
        }
    }


}