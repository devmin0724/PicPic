package com.picpic.sikkle.gifcamera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atom.utils.AtomUtils;
import com.fingram.agifEncoder.QAGIFEncoder;
import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.ui.TimeLineActivity;
import com.picpic.sikkle.ui.TimelineV1NewActivity;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.utils.MyFaceDetectionListener;
import com.picpic.sikkle.utils.PreviewUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifDrawable;

public class GIFCameraFilmActivity extends Activity implements View.OnClickListener, View.OnTouchListener,
        Camera.PreviewCallback, SurfaceHolder.Callback {

    private static final float delay = 28f;
    private static final float TOTAL = 5000f;
    private static final int PICK_FROM_ALBUM = 1111;
    private static final int CROP_FROM_ALBUM = 1112;
    private final int CAMERA_WIDTH = 640;
    private final int CAMERA_HEIGHT = 480;

    final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();

    protected Camera mCamera;
    protected Camera.Parameters mParameters = null;
    protected Camera.Size mPreviewSize;
    Camera.CameraInfo mCameraInfo;
    SurfaceHolder mHolder;
    public static Activity ac;

    boolean isFlashOn = false, isPreviewRunning, isOK = false, isChangeingCamera = false,
            isPlaying = false, recording = false, isShoting = false,
            is11 = false, isPart = false, isShot = false, isFirst = false;
    //isShootingMode = true,
    private int mPreviewFormat, previewNavi = 0, tempViewWidth = 1, colorBefore, colorNow, w, guideNavi = 0,
            nowProgressNavi = 0, speedNavi = 13;
    float total = 0.0f, ratio = delay / TOTAL;
    long lastTime, nextTime;
    Uri mImageCaptureUri;

    LinearLayout layProgress, layFlash, layRatio, layNext, layFirst, layGuide, layGuideAlpha, layGuide2, layGrid, layGhost, layAlbum,
            imvNext, imvCamera, imvPlus, layTop, layBottom, layTop2, layBottom2, layGrids, laySwitch, layBack;
    FrameLayout layShot, layReturn;
    TextView tvTutorial, tvGuide, tvGuide2, tvBtn;
    ProgressDialog pd2, dialog;
    SurfaceView sfv;
    ImageView imvBackground, imvReturn, imvNextIcon, imvCompleteIcon, imvGuide1, imvGuide2;

    java.util.Timer mTimer;
    TimerTask mTaks;

    BackPressCloseHandler backPressCloseHandler;
    ExecutorService executor = Executors.newFixedThreadPool(1);

    ArrayList<Frame> frames = new ArrayList<>();
    ArrayList<Boolean> isFront = new ArrayList<>();
//    ArrayList<Integer> front = new ArrayList<>();
//    ArrayList<ArrayList<Frame>> AppController.originFrames = new ArrayList<ArrayList<Frame>>();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//        getWindow().setExitTransition(new Explode());
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.activity_gifcamera_film);

        AppController.initCamera(true);

        ac = this;

        backPressCloseHandler = new BackPressCloseHandler(this);

        pd2 = new ProgressDialog(this);
        dialog = new ProgressDialog(this);

        tempViewWidth = MinUtils.screenWidth;

        sfv = (SurfaceView) findViewById(R.id.sfv_gif_camera_film);

        int w = MinUtils.screenWidth;
        int h = 0;

//            h = CAMERA_HEIGHT * w / CAMERA_WIDTH;
        h = CAMERA_WIDTH * w / CAMERA_HEIGHT;

        FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) sfv.getLayoutParams();

        p.width = w;
        p.height = h;

        sfv.setLayoutParams(p);

        mHolder = sfv.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        Handler hh = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                startPreview();
            }
        };

        hh.sendEmptyMessageDelayed(0, 500);

        colorBefore = getResources().getColor(R.color.min_progress_before);
        colorNow = getResources().getColor(R.color.min_progress_now);

        String tempFileName = String.valueOf(System.currentTimeMillis());
        AppController.cameraOutput1 = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/picpic/"
                + AppController.getSp().getString("email", "") + "_" + tempFileName + "_1.gif"));
        File ff = new File(AppController.BASE_SRC
                + MinUtils.fromMId(AppController.getSp().getString("m_id", "")) + "_"
                + MinUtils.fromCurrnet(tempFileName) + "_2.gif");
        try {
            ff.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AppController.cameraOutput2 = Uri.fromFile(ff);
        Log.e("output", AppController.cameraOutput2.getPath() + "");
        AppController.cameraF = new File(Environment.getExternalStorageDirectory() + "/PicPic/" + tempFileName + ".jpg");
        AppController.cameraAtomFF = new File(Environment.getExternalStorageDirectory() + "/PicPic/atom_" + tempFileName + ".jpg");

        File f = new File(AppController.BASE_SRC
                + MinUtils.fromMId(AppController.getSp().getString("m_id", "")) + "_"
                + MinUtils.fromCurrnet(tempFileName) + "_2.gif");
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AppController.cameraPath2 = getPath2();
//        AppController.cameraPath2 = f.getAbsolutePath();
//        Log.e("path", AppController.cameraPath2);

        initViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHolder = sfv.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mHolder.addCallback(this);

        Handler hh = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                startPreview();


                switchCamera(AppController.nowCameraNum);
            }
        };
        hh.sendEmptyMessageDelayed(0, 500);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            int numberOfCameras = Camera.getNumberOfCameras();
            Log.e("cameraNum", numberOfCameras + "");
            mCameraInfo = new Camera.CameraInfo();

            for (int i = 0; i < numberOfCameras; i++) {
                Camera.getCameraInfo(i, mCameraInfo);
                if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    mCamera = Camera.open(i);
                    AppController.nowCameraNum = i;
                }
            }

            mParameters = mCamera.getParameters();

            if (mParameters.getMaxNumDetectedFaces() > 0) {
                mCamera.setFaceDetectionListener(new MyFaceDetectionListener());
            }

            List<Integer> formats = mParameters.getSupportedPreviewFormats();
            for (int format : formats) {
                if (format == ImageFormat.RGB_565) {
                    mParameters.setPreviewFormat(format);
                    break;
                }
            }
            mPreviewFormat = mParameters.getPreviewFormat();

            List<Camera.Size> sizes = mParameters.getSupportedPreviewSizes();

            for (int i = 0; i < sizes.size(); i++) {
                Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
            }

            for (int i = sizes.size() - 1; i >= 0; i--) {
                boolean isSizeOK = false;
                Camera.Size size = sizes.get(i);
                if (size.width == CAMERA_WIDTH && size.height == CAMERA_HEIGHT) {
                    mParameters.setPreviewSize(size.width, size.height);
                    isSizeOK = true;
                    Log.e("size", size.width + "/" + size.height);
                    break;
                }

                if (!isSizeOK) {
                    Camera.Size size2 = sizes.get(0);
                    mParameters.setPreviewSize(size2.width, size2.height);
                    isSizeOK = true;
                    Log.e("size", size.width + "/" + size.height);
                }

            }


            mPreviewSize = mParameters.getPreviewSize();

            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);

            mCamera.setParameters(mParameters);
            mCamera.setPreviewCallback(this);


            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            if (isPreviewRunning) {
                mCamera.stopPreview();
            }

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);
        } catch (Exception e) {

        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);
            mCamera.release();
            mCamera = null;
        }
    }

    public void startPreview() {
        try {
            mCamera.startPreview();
            isPreviewRunning = true;
//            mCamera.startFaceDetection();
//            startFaceDetection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void flashToggle(boolean is) {
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            AppController.nowCameraNum = (AppController.nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                    Camera.CameraInfo.CAMERA_FACING_BACK : Camera.CameraInfo.CAMERA_FACING_FRONT;

            mCamera = Camera.open(AppController.nowCameraNum);

            if (AppController.nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == CAMERA_WIDTH && size.height == CAMERA_HEIGHT) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }


                if (!is) {
                    mParameters.setFlashMode("off");
                    isFlashOn = false;
                    layFlash.setSelected(false);
                } else {
                    mParameters.setFlashMode("torch");
                    isFlashOn = true;
                    layFlash.setSelected(true);
                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
        } catch (Exception e) {

        }
    }

    public void switchCamera() {
        isChangeingCamera = true;
        try {

            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            AppController.nowCameraNum = (AppController.nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) ?
                    Camera.CameraInfo.CAMERA_FACING_FRONT : Camera.CameraInfo.CAMERA_FACING_BACK;


            mCamera = Camera.open(AppController.nowCameraNum);

            if (AppController.nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == CAMERA_WIDTH && size.height == CAMERA_HEIGHT) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
            isChangeingCamera = false;
        } catch (Exception e) {

        }
    }
    public void switchCamera(int a) {
        isChangeingCamera = true;
        try {

            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mHolder.removeCallback(this);
                mCamera.release();
                mCamera = null;
            }

            mCamera = Camera.open(a);

            if (AppController.nowCameraNum == 0) {
                mCamera.setParameters(mParameters);
            } else {
                Camera.Parameters pp = mCamera.getParameters();

                List<Integer> formats = pp.getSupportedPreviewFormats();
                for (int format : formats) {
                    if (format == ImageFormat.RGB_565) {
                        pp.setPreviewFormat(format);
                        break;
                    }
                }

                List<Camera.Size> sizes = pp.getSupportedPreviewSizes();

                for (int i = 0; i < sizes.size(); i++) {
                    Log.e("pre size " + i, sizes.get(i).width + "/" + sizes.get(i).height);
                }

                for (int i = sizes.size() - 1; i >= 0; i--) {
                    boolean isSizeOK = false;
                    Camera.Size size = sizes.get(i);
                    if (size.width == CAMERA_WIDTH && size.height == CAMERA_HEIGHT) {
                        pp.setPreviewSize(size.width, size.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                        break;
                    }

                    if (!isSizeOK) {
                        Camera.Size size2 = sizes.get(0);
                        pp.setPreviewSize(size2.width, size2.height);
                        isSizeOK = true;
                        Log.e("size", size.width + "/" + size.height);
                    }

                }

                mCamera.setParameters(pp);
            }

            mCamera.setPreviewCallback(this);

            int degrees = getWindowManager().getDefaultDisplay().getRotation() * 3;
            int result;
            if (mCameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (mCameraInfo.orientation + degrees) % 360;
                result = (360 - result) % 360; // compensate the mirror
            } else { // back-facing
                result = (mCameraInfo.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            mCamera.startPreview();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                mCamera.release();
                mCamera = null;
            }
            isChangeingCamera = false;
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_gif_camera_film_back:
                finish();
                break;
            case R.id.imv_gif_camera_film_gird:
                if (layGrid.isSelected()) {
                    layGrid.setSelected(false);
                    layGrids.setVisibility(View.INVISIBLE);
                } else {
                    layGrid.setSelected(true);
                    layGrids.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.imv_gif_camera_film_flash:
                try {
                    if (isFlashOn) {
                        mParameters.setFlashMode("off");
                        isFlashOn = false;
                        layFlash.setSelected(false);
                    } else {
                        mParameters.setFlashMode("torch");
                        isFlashOn = true;
                        layFlash.setSelected(true);
                    }

                    mCamera.setParameters(mParameters);

                } catch (RuntimeException e) {

                }
                break;
            case R.id.imv_gif_camera_film_ghost:
                if (layGhost.isSelected()) {
                    layGhost.setSelected(false);
                    lastTranImageToggle(false);
                } else {
                    lastTranImageToggle(true);
                    layGhost.setSelected(true);
                }
                break;
            case R.id.imv_gif_camera_film_change:
                switchCamera();
                break;
            case R.id.lay_gif_camera_film_return:
            case R.id.imv_gif_camera_film_return:

                if (layProgress.getChildCount() != 0) {
                    if (!imvReturn.isSelected()) {
                        imvReturn.setSelected(true);
                        //TODO 삭제 프로세스 색상 변경

                        layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_now));
                        layProgress.invalidate();
                    } else {
                        isFront.remove(isFront.size() - 1);


                        nowProgressNavi = nowProgressNavi - 1;

                        final int tempI = layProgress.getChildCount() - 1;
                        layProgress.removeViewAt(tempI);
                        layProgress.invalidate();
                        imvReturn.setSelected(false);

                        Log.e("sizesizeFront", isFront.size() + "");

                        try {
                            List<Runnable> list = executor.shutdownNow();

//                            executor.execute(new Runnable() {
//                                @Override
//                                public void run() {
//                                    AppController.originFrames.remove(tempI);
//                                }
//                            });


                            if (list.size() == 0) {
                                if (AppController.originFrames.size() > 0) {
                                    AppController.originFrames.remove(AppController.originFrames.size() - 1);
                                }
                            } else {
                                if (!executor.isTerminated() && !executor.isShutdown()) {
                                    for (int i = 0; i < list.size(); i++) {
                                        if (i != list.size() - 1) {
                                            executor.submit(list.get(i));
                                        }
                                    }
                                }

                            }
                        } catch (NullPointerException e) {
                            Log.e("remove null", e.toString());
                        } catch (Exception e) {
                            Log.e("remove ex", e.toString());
                        }

                    }
                }
                int tempSize = 0;

                if (layProgress.getChildCount() > 1) {
                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                        tempSize += layProgress.getChildAt(i).getWidth();
                    }
                } else if (layProgress.getChildCount() == 1) {
                    tempSize = layProgress.getChildAt(0).getWidth();
                }

                if (tempSize > 50) {
                    imvNext.setSelected(true);
                } else {
                    imvNext.setSelected(false);
                }
                if (AppController.originFrames.size() == 0) {
                    isShoting = false;
                }

                if (nowProgressNavi == 0) {
                    laySwitch.setVisibility(View.VISIBLE);
                } else {
                    laySwitch.setVisibility(View.GONE);
                }

                break;
            case R.id.imv_gif_camera_film_camera:
                if (imvCamera.isSelected()) {
                    imvCamera.setSelected(false);
                    isShot = false;
                } else {
                    isShot = true;
                    imvCamera.setSelected(true);
                }
                break;
            case R.id.imv_gif_camera_film_plus:
                if (imvPlus.isSelected()) {
                    imvPlus.setSelected(false);
                    changeMenuVisivle(false);
                } else {
                    imvPlus.setSelected(true);
                    changeMenuVisivle(true);
                }
                Log.e("isSelected", imvPlus.isSelected() + "");
                break;
            case R.id.imv_gif_camera_film_ratio:
                Log.e("origin size", AppController.originFrames.size() + "");
                if (AppController.originFrames.size() < 1) {
                    if (layRatio.isSelected()) {
                        is11 = false;
                        int layTopHeight = layTop.getHeight();
                        layRatio.setSelected(false);
                        layTop.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0 - layTopHeight);
                        layBottom.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(layBottom.getHeight());
                        layTop.setVisibility(View.GONE);
                        layBottom.setVisibility(View.GONE);
                        layTop2.setVisibility(View.GONE);
                        layBottom2.setVisibility(View.GONE);
                    } else {
                        is11 = true;
                        layRatio.setSelected(true);
                        layTop.setVisibility(View.VISIBLE);
                        layBottom.setVisibility(View.VISIBLE);
                        layTop2.setVisibility(View.VISIBLE);
                        layBottom2.setVisibility(View.VISIBLE);

                        layTop.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0);
                        layBottom.animate().setInterpolator(mInterpolator)
                                .setDuration(200)
                                .translationY(0);
                    }
                }
                break;
            case R.id.imv_gif_camera_film_album_pic:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Camera Load To Album").build());
                doTakePictureAlbumAction();
                break;
            case R.id.imv_gif_camera_film_next:

                if (imvNext.isSelected()) {
                    if (AppController.tagNavi == 2) {
                        initGuide(-1);
                    }

                    flashToggle(false);

                    layGhost.setSelected(false);
                    lastTranImageToggle(false);

                    layGrid.setSelected(false);
                    layGrids.setVisibility(View.INVISIBLE);
                    final ProgressDialog pd2 = new ProgressDialog(this);
                    pd2.setCanceledOnTouchOutside(false);
                    pd2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pd2.setMessage(getResources().getString(R.string.please_wait));
                    pd2.show();

                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (pd2.isShowing()) {
                                        pd2.dismiss();
                                    }
                                    //TODO 편집 ㄱㄱㄱ

                                    AppController.initCamera(false);

                                    AppController.sectionArr = new ArrayList<int[]>();

                                    AppController.editFrames = AppController.originFrames;

                                    Intent i = new Intent(GIFCameraFilmActivity.this, GIFCameraEdit1Activity.class);
                                    AppController.is11 = is11;
                                    AppController.isPart = isPart;
                                    AppController.isLoadGIF = false;
                                    startActivity(i);
                                    finish();
                                }
                            });
                        }
                    });

                }
                break;
        }
    }

    private String getPath2() {

//        return output.getPath();
        if ("file".equals(AppController.cameraOutput2.getScheme())) {
            return AppController.cameraOutput2.getPath();
        }

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(AppController.cameraOutput2, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(idx);
        } finally {
            cursor.close();
        }
    }

    private void doTakePictureAlbumAction() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        String type = "image/gif";
        intent.setDataAndType(data, type);

        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private void lastTranImageToggle(boolean is) {

        if (AppController.originFrames.size() > 0) {
            Bitmap bm = AppController.originFrames.get(AppController.originFrames.size() - 1).get(AppController.originFrames.get(AppController.originFrames.size() - 1).size() - 1).getbitmap();

            imvBackground.setImageBitmap(makeTransparent(bm, 80));

            if (AppController.originFrames.size() != 0) {

                if (is) {
                    imvBackground.setVisibility(View.VISIBLE);
                } else {
                    imvBackground.setVisibility(View.GONE);
                }
            }
        }
    }

    public Bitmap makeTransparent(Bitmap src, int value) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap transBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(transBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        // config paint
        final Paint paint = new Paint();
        paint.setAlpha(value);
        canvas.drawBitmap(src, 0, 0, paint);
        return transBitmap;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            laySwitch.setVisibility(View.GONE);

            tvTutorial.setVisibility(View.GONE);

            if (mCamera != null) {
                mCamera.cancelAutoFocus();
            }

            try {
                mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
                mCamera.setParameters(mParameters);
            } catch (RuntimeException e) {

            }
            for (int i = 0; i < layProgress.getChildCount(); i++) {
                layProgress.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.min_progress_before));
            }

            imvReturn.setSelected(false);

            isShoting = true;
            Log.e("whole Size", AppController.originFrames.size() + "");
            Log.e("w", w + "");
            Log.e("width", layProgress.getWidth() + "");

            for (int i = 0; i < AppController.originFrames.size(); i++) {
                Log.e("index" + i + " size", AppController.originFrames.get(i).size() + "");
            }

            isOK = false;
            if (executor.isShutdown()) {
                executor = Executors.newFixedThreadPool(1);
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("ex", "ggggg");
                        frames = new ArrayList<>();

                    }
                });
            } else {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("ex", "ggggg");
                        frames = new ArrayList<>();

                    }
                });
            }
            int width = layProgress.getWidth();
            int totalWidth = 0;
            for (int i = 0; i < layProgress.getChildCount(); i++) {
                totalWidth += layProgress.getChildAt(i).getWidth();
            }
            if (width > totalWidth) {

                nowProgressNavi = nowProgressNavi + 1;

                Log.e("sizesizeFront", isFront.size() + "");

//                if (AppController.nowCameraNum == 1) {
//                    isFront.add(true);
//                } else {
//                    isFront.add(false);
//                }

                if (AppController.nowCameraNum == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    isFront.add(true);
                    Log.e("front", "true");
                } else {
                    isFront.add(false);
                    Log.e("front", "false");
                }

                lastTime = 0;
                recording = true;
                total = total + delay;

                Log.e("cameraaaaaa", !(AppController.nowCameraNum == Camera.CameraInfo.CAMERA_FACING_BACK) + "");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        layProgress.addView(new ImageView(GIFCameraFilmActivity.this));

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, ViewGroup.LayoutParams.MATCH_PARENT);

                        if (layProgress.getChildCount() > 1) {
                            params.setMargins(3, 0, 0, 0);
                        }

                        layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                        layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(1);

                        layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_before));

                        layProgress.invalidate();

//                        if (AppController.nowCameraNum == Camera.CameraInfo.CAMERA_FACING_FRONT) {
////                            layProgress.getChildAt(layProgress.getChildCount() - 1).setTag("F");
////                            layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(1, "F");
//                            isFront.add(true);
//                        } else {
//                            isFront.add(false);
//                        }

                        Log.e("sizesizeFront", isFront.size() + "");
                    }
                });


                mTaks = new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                recording = true;
                                total = total + delay;
                                int proNavi = (int) (layProgress.getChildAt(layProgress.getChildCount() - 1).getTag());
                                proNavi = proNavi + 1;
                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w * proNavi, ViewGroup.LayoutParams.MATCH_PARENT);
                                if (layProgress.getChildCount() > 1) {
                                    params.setMargins(3, 0, 0, 0);
                                }
                                layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(proNavi);
                                layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                                layProgress.invalidate();

                                int tempSize = 0;

                                if (layProgress.getChildCount() != 0) {
                                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                                        tempSize += layProgress.getChildAt(i).getWidth();
                                    }
                                } else {
                                    tempSize = layProgress.getChildAt(0).getWidth();
                                }

                                if (tempSize > 50) {
                                    layNext.setSelected(true);
                                } else {
                                    layNext.setSelected(false);
                                }

                            }
                        });

                    }
                };

                if (!isShot) {
                    mTimer = new java.util.Timer();

                    mTimer.schedule(mTaks, 0, (int) delay);
                }

            } else {
                recording = false;
            }
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (AppController.tagNavi == 2) {
                int tempSize = 0;

                if (layProgress.getChildCount() != 0) {
                    for (int i = 0; i < layProgress.getChildCount(); i++) {
                        tempSize += layProgress.getChildAt(i).getWidth();
                    }
                } else {
                    tempSize = layProgress.getChildAt(0).getWidth();
                }

                if (tempSize > 50) {
                    initGuide(1);
                }
            }

            recording = false;
            if (!isShot) {
                if (mTimer != null) {
                    mTimer.cancel();
                }
            }
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    if (frames.size() > 0) {
                        AppController.originFrames.add(frames);
                    }

                }
            });
            try {
                mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                mCamera.setParameters(mParameters);
            } catch (RuntimeException e) {

            }


        }
        return true;
    }

    private int getSpecialFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    @Override
    public void onPreviewFrame(final byte[] data, final Camera camera1) {
        try {
            int width = layProgress.getWidth();
            int totalWidth = 0;
            for (int i = 0; i < layProgress.getChildCount(); i++) {
                totalWidth += layProgress.getChildAt(i).getWidth();
            }
            if (data == null || !recording) return;
            if (width <= totalWidth) return;
            long current = System.currentTimeMillis();
            if (lastTime == 0) {
                lastTime = current;
            } else if (current < nextTime) {
                return;
            }
            nextTime = lastTime + (int) delay;
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    if (isShot) {
                        if (frames.size() != 0) {
                            return;
                        }
                    }
                    final String fileName1 = getCacheDir() + "/" + Long.toString(System.currentTimeMillis());
                    final String atomName1 = getCacheDir() + "/atom_" + Long.toString(System.currentTimeMillis());
                    Log.e("cachedir", getCacheDir().getAbsolutePath());
                    final File f1 = new File(fileName1);
                    final File atomF1 = new File(atomName1);
                    try {
                        FileOutputStream fout1 = new FileOutputStream(f1);
                        switch (mPreviewFormat) {
                            case ImageFormat.RGB_565:
                                fout1.write(data);
                                fout1.close();
                                break;
                            default: // ImageFormat.YUV_420_888
                                byte[] rotated = PreviewUtils.rotateYUV420Degree90(data, mPreviewSize.width, mPreviewSize.height);

                                YuvImage image = new YuvImage(rotated, mPreviewFormat, mPreviewSize.height, mPreviewSize.width, null);
                                image.compressToJpeg(new Rect(0, 0, mPreviewSize.height, mPreviewSize.width), 100, fout1);


//                                if (frames.size() == 0) {
//                                if(AppController.originFrames.size() == 0){
//
//                                }else{
//                                    previewNavi = AppController.originFrames.size();
//                                }
//                                }
//                                if (frames.size() == 0) {
                                previewNavi = AppController.originFrames.size();
//                                } else {
//                                    previewNavi = AppController.originFrames.size() - 1;
//                                }

                                try {
//                                    PreviewUtils.
//                                    ExifInterface exif = new ExifInterface(fileName1);
//                                    Log.e("oror", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
//                                    Log.e("or", exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL) + "");
//                                    Log.e("or2", ExifInterface.ORIENTATION_ROTATE_180 + "");
//                                    if(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL) == ExifInterface.ORIENTATION_ROTATE_180){
//                                        QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                                        Bitmap b = QrBitmapFactory.decodeFile(fileName1, opt);
//                                        int w = b.getWidth();
//                                        int h = b.getHeight();
//
//                                        Matrix mtx = new Matrix();
//                                        mtx.postRotate(180);
//
//                                        Bitmap rotateB = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);
//
//                                        Matrix sideInversion = new Matrix();
//                                        sideInversion.setScale(-1, 1);
//                                        Bitmap sideInversionImg = Bitmap.createBitmap(rotateB, 0, 0,
//                                                rotateB.getWidth(), rotateB.getHeight(), sideInversion, false);
//
//                                        QrBitmapFactory.compressToFile(sideInversionImg, QrBitmapFactory.QrJPEG, fileName1, 100, opt);
//                                    }

                                    if(AppController.nowCameraNum == 1){
                                        QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                                        Bitmap b = QrBitmapFactory.decodeFile(fileName1, opt);
                                        int w = b.getWidth();
                                        int h = b.getHeight();

                                        Matrix mtx = new Matrix();
                                        mtx.postRotate(180);

                                        Bitmap rotateB = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);

                                        Matrix sideInversion = new Matrix();
                                        sideInversion.setScale(-1, 1);
                                        Bitmap sideInversionImg = Bitmap.createBitmap(rotateB, 0, 0,
                                                rotateB.getWidth(), rotateB.getHeight(), sideInversion, false);

                                        QrBitmapFactory.compressToFile(sideInversionImg, QrBitmapFactory.QrJPEG, fileName1, 100, opt);
                                    }

//                                    if (isFront.get(previewNavi)) {
//                                        QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                                        Bitmap b = QrBitmapFactory.decodeFile(fileName1, opt);
//                                        int w = b.getWidth();
//                                        int h = b.getHeight();
//
//                                        Matrix mtx = new Matrix();
//                                        mtx.postRotate(180);
//
//                                        Bitmap rotateB = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);
//
//                                        Matrix sideInversion = new Matrix();
//                                        sideInversion.setScale(-1, 1);
//                                        Bitmap sideInversionImg = Bitmap.createBitmap(rotateB, 0, 0,
//                                                rotateB.getWidth(), rotateB.getHeight(), sideInversion, false);
//
//                                        QrBitmapFactory.compressToFile(sideInversionImg, QrBitmapFactory.QrJPEG, fileName1, 100, opt);
//                                    }
//                                    Camera.CameraInfo.CAMERA_FACING_BACK
//Log.e("ff", camera1.getParameters().get("facing"));

//                                    if(Camera.CameraInfo. camera1.getParameters().get("camera-id")

//                                        Camera.CameraInfo cc = new Camera.CameraInfo();
//
                                    //                                        Camera.getCameraInfo(Integer.parseInt(camera1.getParameters().get("camera-id")), cc);
//                                        if (cc.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                                            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
//                                            Bitmap b = QrBitmapFactory.decodeFile(fileName1, opt);
//                                            int w = b.getWidth();
//                                            int h = b.getHeight();
//
//                                            Matrix mtx = new Matrix();
//                                            mtx.postRotate(180);
//
//                                            Bitmap rotateB = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);
//
//                                            Matrix sideInversion = new Matrix();
//                                            sideInversion.setScale(-1, 1);
//                                            Bitmap sideInversionImg = Bitmap.createBitmap(rotateB, 0, 0,
//                                                    rotateB.getWidth(), rotateB.getHeight(), sideInversion, false);
//
//                                            QrBitmapFactory.compressToFile(sideInversionImg, QrBitmapFactory.QrJPEG, fileName1, 100, opt);
//                                        }

                                    try {

//                                        if (layProgress.getChildAt(AppController.originFrames.size() - 1).getTag(1).equals("F")) {
                                        Log.e("isFornt", previewNavi + "/" + isFront.get(previewNavi));


                                    } catch (NullPointerException e) {

                                    }

                                } catch (IndexOutOfBoundsException e) {

                                }
                                long atomSize1 = AtomUtils.encodeAtomJPEG(fileName1, atomF1.getAbsolutePath(), 0, 0);
//
                                QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                                final Bitmap b = QrBitmapFactory.decodeFile(atomF1.getAbsolutePath(), opt);

                                Log.e("frames size", frames.size() + "");

                                frames.add(new Frame() {

                                    @Override
                                    public File getFile() {
                                        return atomF1;
                                    }

                                    @Override
                                    public Bitmap getbitmap() {

                                        return b;
                                    }

                                });

                        }
                        Log.i("Camera", String.format("frame added %dx%d", mPreviewSize.width, mPreviewSize.height));
                    } catch (IOException e) {
                        f1.delete();
                        atomF1.delete();
                        e.printStackTrace();
                    }
                    recording = false;
                }
            });
            lastTime = current;

//        recording = false;

            if (!isOK) {
                isOK = true;
            }
        } catch (Exception e) {

        }
    }

    private void initViews() {
        layProgress = (LinearLayout) findViewById(R.id.lay_gif_camera_film_progress);
        layFlash = (LinearLayout) findViewById(R.id.imv_gif_camera_film_flash);
        layRatio = (LinearLayout) findViewById(R.id.imv_gif_camera_film_ratio);
        layNext = (LinearLayout) findViewById(R.id.imv_gif_camera_film_next);
        layGuide = (LinearLayout) findViewById(R.id.lay_gif_camera_film_guide);
        layGuide2 = (LinearLayout) findViewById(R.id.lay_gif_camera_film_guide_2);
        layGuideAlpha = (LinearLayout) findViewById(R.id.lay_gif_camera_film_guide_alpha);
        layShot = (FrameLayout) findViewById(R.id.lay_gif_camera_film_shot);
        layGrid = (LinearLayout) findViewById(R.id.imv_gif_camera_film_gird);
        layGhost = (LinearLayout) findViewById(R.id.imv_gif_camera_film_ghost);
        layAlbum = (LinearLayout) findViewById(R.id.imv_gif_camera_film_album_pic);
        layFirst = (LinearLayout) findViewById(R.id.lay_gif_camera_film_first);
        imvNext = (LinearLayout) findViewById(R.id.imv_gif_camera_film_next);
        imvCamera = (LinearLayout) findViewById(R.id.imv_gif_camera_film_camera);
        imvPlus = (LinearLayout) findViewById(R.id.imv_gif_camera_film_plus);
        layBack = (LinearLayout) findViewById(R.id.imv_gif_camera_film_back);

        layTop = (LinearLayout) findViewById(R.id.lay_gif_camera_film_top);
        layBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_film_bottom);
        layTop2 = (LinearLayout) findViewById(R.id.lay_gif_camera_film_top2);
        layBottom2 = (LinearLayout) findViewById(R.id.lay_gif_camera_film_bottom2);

        layGrids = (LinearLayout) findViewById(R.id.lay_gif_camera_film_grid);
        laySwitch = (LinearLayout) findViewById(R.id.imv_gif_camera_film_change);

        layFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layFirst.setVisibility(View.GONE);
            }
        });

        tvTutorial = (TextView) findViewById(R.id.tv_gif_camera_film_tuto);
        tvBtn = (TextView) findViewById(R.id.tv_gif_camera_film_guide_btn);
        tvGuide = (TextView) findViewById(R.id.tv_gif_camera_film_tu_ment);
        tvGuide2 = (TextView) findViewById(R.id.tv_gif_camera_film_tu_ment2);

        imvReturn = (ImageView) findViewById(R.id.imv_gif_camera_film_return);
        imvGuide1 = (ImageView) findViewById(R.id.imv_gif_camera_film_guide);
        imvGuide2 = (ImageView) findViewById(R.id.imv_gif_camera_film_guide2);
        imvBackground = (ImageView) findViewById(R.id.imv_gif_camera_film_background);

        layReturn = (FrameLayout) findViewById(R.id.lay_gif_camera_film_return);
        layReturn.setOnClickListener(this);

        try{
            AppController.tagNavi = getIntent().getExtras().getInt("navi");
        }catch (NullPointerException e){
            AppController.tagNavi = 0;
        }

        if (AppController.tagNavi == 1) {
            AppController.tag_id = getIntent().getExtras().getString("tag_id");
            AppController.tag_title = getIntent().getExtras().getString("tag_title");
            AppController.tag_body = getIntent().getExtras().getString("tag_body");
            initGuide(-1);
        } else if (AppController.tagNavi == 2) {
            initGuide(0);
        } else {
            initGuide(-1);
        }

        if (AppController.tagNavi == 2) {
            initGuide(0);
        }

        layFlash.setOnClickListener(this);
        layBack.setOnClickListener(this);
        layRatio.setOnClickListener(this);
        layNext.setOnClickListener(this);
//        layGuide.setOnClickListener(this);
//        layGuide2.setOnClickListener(this);
//        layGuideAlpha.setOnClickListener(this);
        layGrid.setOnClickListener(this);
        layGhost.setOnClickListener(this);
        layAlbum.setOnClickListener(this);
        laySwitch.setOnClickListener(this);

        imvNext.setOnClickListener(this);
        imvCamera.setOnClickListener(this);
        imvPlus.setOnClickListener(this);

//        imvReturn.setOnClickListener(this);

        layShot.setOnTouchListener(this);

        try {
            GifDrawable gdGuide1 = new GifDrawable(getResources().getAssets().open("tu_touch_1.gif"));
            gdGuide1.setLoopCount(0);
            gdGuide1.setSpeed(2.0f);
            imvGuide1.setImageDrawable(gdGuide1);

            GifDrawable gdGuide2 = new GifDrawable(getResources().getAssets().open("tu_touch_2.gif"));
            gdGuide2.setLoopCount(0);
            gdGuide2.setSpeed(2.0f);
            imvGuide2.setImageDrawable(gdGuide2);

        } catch (IOException e) {
            e.printStackTrace();
        }

        layProgress.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int tempWidth = layProgress.getWidth();
                w = (int) (tempWidth * ratio);
            }
        });

        changeMenuVisivle(false);

//        layReturn.setOnTouchListener(new OnSwipeTouchListener(GIFCameraFilmActivity.this) {
//            @Override
//            public void onClick() {
//                super.onClick();
//                if (layProgress.getChildCount() != 0) {
//                    if (!imvReturn.isSelected()) {
//                        imvReturn.setSelected(true);
//                        //TODO 삭제 프로세스 색상 변경
//
//                        layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_now));
//                        layProgress.invalidate();
//                    } else {
//                        final int tempI = layProgress.getChildCount() - 1;
//                        layProgress.removeViewAt(tempI);
//                        layProgress.invalidate();
//                        imvReturn.setSelected(false);
//
//                        try {
//                            List<Runnable> list = executor.shutdownNow();
//
//                            if (list.size() == 0) {
//                                if (AppController.originFrames.size() > 0) {
//                                    cameraNums.remove(cameraNums.size() - 1);
//                                    AppController.originFrames.remove(AppController.originFrames.size() - 1);
//                                }
//                            } else {
//                                if (!executor.isTerminated() && !executor.isShutdown()) {
//                                    for (int i = 0; i < list.size(); i++) {
//                                        if (i != list.size() - 1) {
//                                            executor.submit(list.get(i));
//                                        }
//                                    }
//
//                                    cameraNums.remove(tempI);
//                                    nowProgressNavi = nowProgressNavi - 1;
//                                }
//
//                            }
//                        } catch (NullPointerException e) {
//                            Log.e("remove null", e.toString());
//                        } catch (Exception e) {
//                            Log.e("remove ex", e.toString());
//                        }
//
//                    }
//                }
//                int tempSize = 0;
//
//                if (layProgress.getChildCount() > 1) {
//                    for (int i = 0; i < layProgress.getChildCount(); i++) {
//                        tempSize += layProgress.getChildAt(i).getWidth();
//                    }
//                } else if (layProgress.getChildCount() == 1) {
//                    tempSize = layProgress.getChildAt(0).getWidth();
//                }
//
//                if (tempSize > 50) {
//                    imvNext.setSelected(true);
//                } else {
//                    imvNext.setSelected(false);
//                }
//                if (AppController.originFrames.size() == 0) {
//                    isShoting = false;
//                }
//            }
//
//            @Override
//            public void onLongClick() {
//                super.onLongClick();
//                layProgress.removeAllViews();
//                layProgress.invalidate();
//                cameraNums = new ArrayList<Boolean>();
//                nowProgressNavi = 0;
//                imvReturn.setSelected(false);
//                executor.shutdownNow();
//                isShoting = false;
////                executor =
//                AppController.originFrames = new ArrayList<ArrayList<Frame>>();
//            }
//        });

        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("카메라촬영_" + AppController.tagNavi);
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
        AppController.t.set("&uid", AppController.getSp().getString("email", ""));
        AppController.t.setScreenName("카메라촬영");
        AppController.t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void initGuide(int a) {
        guideNavi = a;
        switch (a) {
            case 0:
                layFirst.setVisibility(View.VISIBLE);
                imvGuide1.setVisibility(View.GONE);
                imvGuide2.setVisibility(View.VISIBLE);
                layGuideAlpha.setVisibility(View.VISIBLE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.GONE);
                tvGuide.setVisibility(View.VISIBLE);
                tvBtn.setVisibility(View.GONE);
                tvGuide.setText(getResources().getString(R.string.tu_camera_ment_1));
                break;
            case 1:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.VISIBLE);
                imvGuide2.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.VISIBLE);
                layGuide2.setVisibility(View.VISIBLE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.VISIBLE);
                tvBtn.setText(getResources().getString(R.string.camera_next));
                tvGuide2.setText(getResources().getString(R.string.tu_camera_ment_2));
                break;
            default:
                layFirst.setVisibility(View.GONE);
                imvGuide1.setVisibility(View.GONE);
                imvGuide2.setVisibility(View.GONE);
                layGuideAlpha.setVisibility(View.GONE);
                layGuide.setVisibility(View.GONE);
                layGuide2.setVisibility(View.GONE);
                tvGuide.setVisibility(View.GONE);
                tvBtn.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (AppController.tagNavi == 2) {
            backPressCloseHandler.onBackPressed();
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executor.shutdownNow();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void changeMenuVisivle(boolean isVisible) {
        int y = 0;
        if (isVisible) {
            y = 0;
        } else {
            y = 300;
        }

        layRatio.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        layGrid.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        layFlash.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        layGhost.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
        layAlbum.animate().setInterpolator(mInterpolator)
                .setDuration(200)
                .translationY(y);
    }

    private boolean isGIF(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = cR.getType(uri);
        Log.e("mime_extention2", type);
        return type.equals("image/gif");
    }

    public String getPathFromUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToNext();
        String path = cursor.getString(cursor.getColumnIndex("_data"));
        cursor.close();

        return path;
    }

    private boolean copyFile(File file, String save_file) {
        boolean result;
        if (file != null && file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                FileOutputStream newfos = new FileOutputStream(save_file);
                int readcount = 0;
                byte[] buffer = new byte[1024];
                while ((readcount = fis.read(buffer, 0, 1024)) != -1) {
                    newfos.write(buffer, 0, readcount);
                }
                newfos.close();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_FROM_ALBUM:
                    try {
                        mImageCaptureUri = data.getData();

                        Log.e("uri", mImageCaptureUri.toString());

                        if (isGIF(mImageCaptureUri)) {
                            if (AppController.originFrames.size() < 1) {
                                long nowCurrent = System.currentTimeMillis();

                                final File f2 = new File(AppController.cacheDir + "/" + nowCurrent + "_2.gif");

                                if (f2.length() <= 5 * 1024 * 1024) {
                                    GifDrawable gd = new GifDrawable(getContentResolver(), mImageCaptureUri);

                                    QAGIFEncoder encoder2 = new QAGIFEncoder();
                                    encoder2.setRepeat(0);
                                    encoder2.setDelay(1000 / speedNavi);
                                    encoder2.start(f2.getAbsolutePath());

                                    ArrayList<Frame> tempFrames = new ArrayList<>();

                                    for (int i = 0; i < gd.getNumberOfFrames(); i++) {
                                        final int finalI = i;
                                        Bitmap b = gd.seekToFrameAndGet(finalI);
                                        final File tempFile = new File(AppController.cacheDir + "/" + nowCurrent + "_" + i);
                                        b.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(tempFile));
                                        final Bitmap finalB = b;
                                        tempFrames.add(new Frame() {
                                            @Override
                                            public File getFile() {
                                                return tempFile;
                                            }

                                            @Override
                                            public Bitmap getbitmap() {
                                                return finalB;
                                            }
                                        });
                                        encoder2.addFrame(b);
                                    }

                                    encoder2.finish();

//                                    isLoadToGif = true;
//                                    frameNavi = 1;
//                                    seekFrame.setMax(3);

                                    imvNext.setSelected(true);
                                    AppController.originFrames.add(tempFrames);

//                                    ChangeViews(INIT_VIEWS_EDITOR_1_FRAME_AND_SPEED);

                                    //TODO 편집 ㄱㄱ

                                    AppController.initCamera(false);

                                    AppController.sectionArr = new ArrayList<int[]>();

                                    AppController.editFrames = AppController.originFrames;

                                    Intent i = new Intent(GIFCameraFilmActivity.this, GIFCameraEdit1Activity.class);
                                    AppController.is11 = is11;
                                    AppController.isPart = isPart;
                                    AppController.isLoadGIF = true;
                                    startActivity(i);
                                    finish();

                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif_size), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.add_not_gif), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.cant_load_image), Toast.LENGTH_LONG).show();
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
//                        File tempF = new File(String.valueOf(mImageCaptureUri));
//
//                        long tempSize = 5 * 1024 * 1024;
//
//                        if (tempF.length() < tempSize) {
//                            long nowCurrent = System.currentTimeMillis();
//                            File f = new File(getPathFromUri(mImageCaptureUri));
//
//                            final File f2 = new File(Environment.getExternalStorageDirectory() + "/picpic/" + nowCurrent + "_2.gif");
//
//                            copyFile(f, f2.getAbsolutePath());
//
//                            Intent i = new Intent(GIFCameraFilmActivity.this, WritePostActivity.class);
//                            i.putExtra("url2", f2.getAbsolutePath());
//                            i.putExtra("thumb", "");
//                            i.putExtra("navi", AppController.tagNavi);
//                            if (AppController.tagNavi == 1) {
//                                i.putExtra("tag_id", AppController.tag_id);
//                                i.putExtra("tag_title", AppController.tag_title);
//                                i.putExtra("tag_body", AppController.tag_body);
//                            }
//                            startActivity(i);
//                            finish();
//                            overridePendingTransition(0, 0);
//                        }
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e("e", e.toString());
                    }
                    break;
                case CROP_FROM_ALBUM:
                    long atomSize1 = AtomUtils.encodeAtomJPEG(AppController.cameraF.getAbsolutePath(), AppController.cameraAtomFF.getAbsolutePath(), CAMERA_HEIGHT, CAMERA_WIDTH);

                    QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
                    opt.inSampleSize = 1;
                    opt.inPreferredConfig = QrBitmapFactory.Options.Config.ARGB_8888;
                    opt.inSampleSize = 1;

                    Bitmap photo = QrBitmapFactory.decodeFile(AppController.cameraAtomFF.getAbsolutePath(), opt);
                    Bitmap resize = null;
                    if (is11) {
                        resize = Bitmap.createScaledBitmap(photo, CAMERA_HEIGHT, CAMERA_HEIGHT, false);
                    } else {
                        resize = Bitmap.createScaledBitmap(photo, CAMERA_HEIGHT, CAMERA_WIDTH, false);
                    }

                    frames = new ArrayList<>();

                    final Bitmap tempBitmap = resize;

                    frames.add(new Frame() {

                        @Override
                        public File getFile() {
                            return AppController.cameraAtomFF;
                        }

                        @Override
                        public Bitmap getbitmap() {
                            return tempBitmap;
                        }
                    });

                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            AppController.originFrames.add(frames);
                            total = total + delay;
                        }
                    });

                    nowProgressNavi = nowProgressNavi + 1;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layProgress.addView(new ImageView(GIFCameraFilmActivity.this));

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(w, ViewGroup.LayoutParams.MATCH_PARENT);

                            if (layProgress.getChildCount() > 1) {
                                params.setMargins(3, 0, 0, 0);
                            }

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setLayoutParams(params);

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setTag(1);

                            layProgress.getChildAt(layProgress.getChildCount() - 1).setBackgroundColor(getResources().getColor(R.color.min_progress_before));

                            layProgress.invalidate();

                        }
                    });

                    Log.e("FrameSize", AppController.originFrames.get(AppController.originFrames.size() - 1).size() + "");
                    Log.e("wFrameSize", AppController.originFrames.size() + "");

                    break;
            }
        }

    }

    public class BackPressCloseHandler {

        private long backKeyPressedTime = 0;
        private Toast toast;

        private Activity activity;

        public BackPressCloseHandler(Activity context) {
            this.activity = context;
        }

        public void onBackPressed() {
            if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                backKeyPressedTime = System.currentTimeMillis();
                showGuide();
                return;
            }
            if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                activity.finish();
//                startActivity(new Intent(GIFCameraActivity.this, NewGridTimelineV1NewActivity.class));
                Intent i = new Intent(GIFCameraFilmActivity.this, TimelineV1NewActivity.class);
                i.putExtra("navi", 0);
                startActivity(i);
                toast.cancel();
            }
        }


        private void showGuide() {
            toast = Toast.makeText(activity, "튜토리얼을 끝내야 PicPic을 시작할수 있어요. \'뒤로\'버튼을 한번 더 누르시면 튜토리얼이 종료됩니다.",
                    Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    static class Timer {
        long lastTime = System.currentTimeMillis();

        void logDelay(String tag) {
            long current = System.currentTimeMillis();
            Log.w("Timer", String.format("delay(%s): %d", tag, (current - lastTime)));
            lastTime = current;
        }
    }

}
