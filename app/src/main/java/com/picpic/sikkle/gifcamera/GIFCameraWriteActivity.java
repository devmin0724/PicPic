package com.picpic.sikkle.gifcamera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.StickerData;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.widget.sticker.StickerView;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class GIFCameraWriteActivity extends Activity implements View.OnClickListener {

    GPUImageView imvPlayer;
    StickerView stv;
    LinearLayout layInputText, layWater, layText, layO, layX, layBack;
    EditText edtInputText;
    ImageView imvInputText, imvWater;
    List<ImageView> imvColorArr = new ArrayList<>(20);
    TextView tempTv;

    int stickerColor = Color.WHITE;
    String stickerStr = "PicPic";
    Typeface stickerTypeface = null;
    boolean isPlaying = false;
    ArrayList<Bitmap> playArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gifcamera_write);

        imvPlayer = (GPUImageView) findViewById(R.id.imv_gif_camera_text_player);

        stv = (StickerView) findViewById(R.id.stv_gif_camera_text);

        layInputText = (LinearLayout) findViewById(R.id.lay_gif_camera_text_text_input);
        layWater = (LinearLayout) findViewById(R.id.lay_gif_camera_text_water_mark);
        layText = (LinearLayout) findViewById(R.id.lay_gif_camera_text_text_edit);
        layO = (LinearLayout) findViewById(R.id.lay_gif_camera_text_text_bottom_o);
        layX = (LinearLayout) findViewById(R.id.lay_gif_camera_text_text_bottom_x);
        layBack = (LinearLayout) findViewById(R.id.imv_gif_camera_text_film_back);

        layInputText.setOnClickListener(this);
        layWater.setOnClickListener(this);
        layText.setOnClickListener(this);
        layO.setOnClickListener(this);
        layX.setOnClickListener(this);
        layBack.setOnClickListener(this);

        edtInputText = (EditText) findViewById(R.id.edt_gif_camera_text_text_input);

        imvInputText = (ImageView) findViewById(R.id.imv_gif_camera_text_text_input_ok);
        imvInputText.setOnClickListener(this);
        imvWater = (ImageView) findViewById(R.id.imv_gif_camera_text_water_mark);

        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_1));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_2));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_3));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_4));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_5));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_6));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_7));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_8));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_9));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_10));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_11));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_12));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_13));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_14));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_15));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_16));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_17));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_18));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_19));
        imvColorArr.add((ImageView) findViewById(R.id.imv_gif_camera_text_text_color_20));

        for (int i = 0; i < imvColorArr.size(); i++) {
            imvColorArr.get(i).setOnClickListener(this);
            imvColorArr.get(i).setTag(i);
        }

        tempTv = new TextView(GIFCameraWriteActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tempTv.setLayoutParams(layoutParams);
        tempTv.setTextSize(100);
        tempTv.setText(AppController.stvString);
        tempTv.setTextColor(stickerColor);
        tempTv.setBackgroundColor(Color.TRANSPARENT);
        tempTv.measure(0, 0);

        int width = tempTv.getMeasuredWidth();
        int height = tempTv.getMeasuredHeight();

        Log.e("onCreate w/h", width + "/" + height);

        Bitmap testB;

        testB = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(testB);
        tempTv.layout(0, 0, width, height);
        tempTv.draw(c);

        stv.setWaterMark(testB);

        if (AppController.isText) {
            StickerData sd = stv.getData();

            sd.setmMatrix(AppController.mMatrix);
            sd.setmPaint(AppController.mPaint);
            sd.setmPoints(AppController.mPoint);

            stv.setData(sd);
        }

        stickerColorSetting(AppController.stickerColorNavi);

        imvPlayer.setFilter(AppController.mFilter);
        imvPlayer.requestRender();

        startPlay();

    }

    private void startPlay() {
        try{
            isPlaying = false;

            playArray = new ArrayList<>();

            int duration = 1000 / AppController.speedNavi;

            if (!AppController.isReverse) {

                Log.e("whSize1", AppController.editFrames.size() + "");

                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    for (int j = AppController.sectionArr.get(i)[0]; j <= AppController.sectionArr.get(i)[1]; j = j + AppController.frameNavi) {
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }

            } else {
                for (int i = AppController.editFrames.size() - 1; i >= 0; i--) {
                    for (int j = AppController.sectionArr.get(i)[1]; j >= AppController.sectionArr.get(i)[0]; j = j - AppController.frameNavi) {
                        //아웃오브 인덱스처리
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }
            }

            imvPlayer.setImageArray(playArray, duration);

            isPlaying = true;
        }catch (Exception e){

        }
    }

    private void stickerColorSetting(int selectColor) {
        int[] colors = getResources().getIntArray(R.array.gif_camera_text_colors);
        stickerColor = colors[selectColor];

        AppController.stickerColorNavi = selectColor;

        LinearLayout.LayoutParams marginParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        marginParams.topMargin = 20;
        LinearLayout.LayoutParams nonMarginParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        nonMarginParams.topMargin = 0;

        for (int i = 0; i < imvColorArr.size(); i++) {
            if (i == selectColor) {
                imvColorArr.get(i).setLayoutParams(nonMarginParams);
            } else {
                imvColorArr.get(i).setLayoutParams(marginParams);
            }
        }

        commitSticker();
    }

    private void commitSticker() {

        StickerData sd = stv.getData();

        Log.e("scale", sd.getmStickerScaleSize() + "");

        tempTv = new TextView(GIFCameraWriteActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tempTv.setLayoutParams(layoutParams);
        tempTv.setTextSize(100);
        tempTv.setText(AppController.stvString);
        if (stickerTypeface != null) {
            tempTv.setTypeface(stickerTypeface);
        }
        tempTv.setTextColor(stickerColor);
        tempTv.setBackgroundColor(Color.TRANSPARENT);
        tempTv.measure(0, 0);

        int width = tempTv.getMeasuredWidth();
        int height = tempTv.getMeasuredHeight();
        Bitmap testB;
        testB = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(testB);
        tempTv.layout(0, 0, width, height);
        tempTv.draw(c);

        stv.setWaterMark(testB);
        stv.setData(sd);
    }

    private boolean isWidth(Bitmap b) {
        int width = b.getWidth();
        int height = b.getHeight();
        if (width > height) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_gif_camera_text_text_color_1:
            case R.id.imv_gif_camera_text_text_color_2:
            case R.id.imv_gif_camera_text_text_color_3:
            case R.id.imv_gif_camera_text_text_color_4:
            case R.id.imv_gif_camera_text_text_color_5:
            case R.id.imv_gif_camera_text_text_color_6:
            case R.id.imv_gif_camera_text_text_color_7:
            case R.id.imv_gif_camera_text_text_color_8:
            case R.id.imv_gif_camera_text_text_color_9:
            case R.id.imv_gif_camera_text_text_color_11:
            case R.id.imv_gif_camera_text_text_color_12:
            case R.id.imv_gif_camera_text_text_color_13:
            case R.id.imv_gif_camera_text_text_color_14:
            case R.id.imv_gif_camera_text_text_color_15:
            case R.id.imv_gif_camera_text_text_color_16:
            case R.id.imv_gif_camera_text_text_color_17:
            case R.id.imv_gif_camera_text_text_color_18:
            case R.id.imv_gif_camera_text_text_color_19:
            case R.id.imv_gif_camera_text_text_color_20:
                stickerColorSetting((int) v.getTag());
                break;
            case R.id.imv_gif_camera_text_text_input_ok:
                AppController.stvString = edtInputText.getText().toString();
                commitSticker();
//                stickerTextView.setText(edtEdit.getText().toString());
                layInputText.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtInputText.getWindowToken(), 0);
                break;
            case R.id.lay_gif_camera_text_text_edit:

                layInputText.setVisibility(View.VISIBLE);
//                edtEdit.setText(stickerTextView.getText().toString());
                edtInputText.setText(tempTv.getText().toString());
                edtInputText.requestFocus();
                edtInputText.setSelection(edtInputText.getText().toString().length());

                InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm2.showSoftInput(edtInputText, InputMethodManager.SHOW_FORCED);
                imm2.showSoftInputFromInputMethod (edtInputText .getApplicationWindowToken(),InputMethodManager.SHOW_FORCED);
                break;
            case R.id.lay_gif_camera_text_text_bottom_o:
                AppController.isText = true;
                AppController.stvBitmap = stv.getBitmap();

                if (AppController.isLoadGIF) {
                    Bitmap bitmap = null;
                    if (AppController.isReverse) {
                        bitmap = AppController.editFrames.get(AppController.editFrames.size() - 1).get(AppController.editFrames.get(AppController.editFrames.size() - 1).size() - 1).getbitmap();
                    } else {
                        bitmap = AppController.editFrames.get(0).get(0).getbitmap();
                    }

                    Bitmap scaleB1 = null;

                    int widthb = 0;
                    int heightb = 0;

                    if (isWidth(bitmap)) {
                        widthb = imvPlayer.getWidth();
                        heightb = bitmap.getHeight() * imvPlayer.getWidth() / bitmap.getWidth();
                        Log.e("sizessss0", widthb + "," + heightb);
                    } else {
                        heightb = imvPlayer.getHeight();
                        widthb = bitmap.getWidth() * imvPlayer.getHeight() / bitmap.getHeight();
                        Log.e("sizessss0", widthb + "," + heightb);
                    }
                    int x = (imvPlayer.getWidth() - widthb) / 2;
                    int y = (imvPlayer.getHeight() - heightb) / 2;
                    try {
                        AppController.stvBitmap = Bitmap.createBitmap(AppController.stvBitmap, x, y, widthb, heightb);
                    } catch (IllegalArgumentException e) {
                        AppController.stvBitmap = Bitmap.createBitmap(AppController.stvBitmap, 100, 100, widthb, heightb);
                    }
                    AppController.stvBitmap = Bitmap.createScaledBitmap(AppController.stvBitmap, bitmap.getWidth(), bitmap.getHeight(), false);
                } else {
                    if (AppController.is11) {
                        AppController.stvBitmap = Bitmap.createScaledBitmap(AppController.stvBitmap, 480, 480, false);
                    } else {
                        AppController.stvBitmap = Bitmap.createScaledBitmap(AppController.stvBitmap, 480, 640, false);
                    }
                }
                StickerData sd = stv.getData();

                AppController.mMatrix = sd.getmMatrix();
                AppController.mPaint = sd.getmPaint();
                AppController.mPoint = sd.getmPoints();

                onBackPressed();
                break;
            case R.id.lay_gif_camera_text_text_bottom_x:
                AppController.isText = false;

                onBackPressed();
                break;
            case R.id.lay_gif_camera_text_water_mark:
                switch (AppController.waterNavi) {
                    case 0:
                        AppController.waterNavi = 1;
                        imvWater.setImageResource(R.drawable.new_water_mark);
                        break;
                    case 1:
                        AppController.waterNavi = 0;
                        imvWater.setImageResource(R.drawable.new_water_mark_c);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;
                    case 8:

                        break;
                    case 9:

                        break;
                }
                break;
            case R.id.imv_gif_camera_text_film_back:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(GIFCameraWriteActivity.this, GIFCameraEdit2Activity.class));
        startActivity(new Intent(GIFCameraWriteActivity.this, GIFCameraEdit1Activity.class));
        finish();
    }
}
