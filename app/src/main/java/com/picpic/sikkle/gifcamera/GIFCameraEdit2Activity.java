package com.picpic.sikkle.gifcamera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fingram.agifEncoder.QAGIFEncoder;
import com.fingram.qrb.QrBitmapFactory;
import com.google.android.gms.analytics.HitBuilders;
import com.picpic.sikkle.R;
import com.picpic.sikkle.beans.Frame;
import com.picpic.sikkle.ui.WritePostActivityV1;
import com.picpic.sikkle.utils.AppController;
import com.picpic.sikkle.utils.MinUtils;
import com.picpic.sikkle.widget.DrawView;
import com.picpic.sikkle.widget.MultiSlider;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageRenderer;
import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.PixelBuffer;

public class GIFCameraEdit2Activity extends Activity implements View.OnClickListener {

    private static final int PICK_FROM_ALBUM = 1111;
    private static final int CROP_FROM_ALBUM = 1112;

    LinearLayout layBack, layNext, selectHListLayout, layEditor3X, layEditor3Delete, layEditor3Save,
            layEditor3Copy, layEditor3Load, layEditor3Text, layEditor3Part, layWater, layTop, layBottom;
    TextView tvAll;
    ImageView imvPart, imvText, imvWater;

    GPUImageView imvPlayer;
    MultiSlider ms;
    View mDrapView;


    GestureDetector mGestureDetector;
    ExecutorService executor = Executors.newFixedThreadPool(1);

    private int previewNavi = -1, tempSelectIndex = 0;
    boolean isSelectAll = true, isPlaying = false;
    ArrayList<ImageView> imvArr;
    ArrayList<Bitmap> playArray;
    ArrayList<Frame> tempArrFrames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gifcamera_edit2);

        imvPlayer = (GPUImageView) findViewById(R.id.imv_gif_camera_edit2_player);

        ms = (MultiSlider) findViewById(R.id.ms_gif_camera_edit2);

        mGestureDetector = new GestureDetector(this, new DrapGestureListener());

        layBack = (LinearLayout) findViewById(R.id.imv_gif_camera_edit2_film_back);
        layNext = (LinearLayout) findViewById(R.id.imv_gif_camera_edit2_film_next);
        selectHListLayout = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_select_lay);

        layWater = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_water_mark);
        layWater.setOnClickListener(this);
        imvWater = (ImageView) findViewById(R.id.imv_gif_camera_edit2_water_mark);

        imvPart = (ImageView) findViewById(R.id.imv_gif_camera_edit2_part);
        imvText = (ImageView) findViewById(R.id.imv_gif_camera_edit2_text);

        if (AppController.isPart) {
            imvPart.setImageBitmap(AppController.dvBitmap);
            imvPart.setVisibility(View.VISIBLE);
//            imvPart.setImageBitmap(AppController.getBitmapWithFilterApplied(AppController.dvOriginBitmap));

//            Bitmap tempB = BitmapFactory.decodeFile(AppController.PART_OUTPUT_SRC);
//
//            tempB = AppController.getBitmapWithFilterApplied(tempB);

//            imvPart.setImageBitmap(AppController.dvBitmap);
        } else {
            imvPart.setVisibility(View.GONE);
        }

        if (AppController.isText) {
            imvText.setVisibility(View.VISIBLE);
            imvText.setImageBitmap(AppController.stvBitmap);
        } else {
            imvText.setVisibility(View.GONE);
        }

        imvPlayer.setFilter(AppController.mFilter);
        imvPlayer.requestRender();

        layEditor3X = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_x);
        layEditor3Delete = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_trash);
        layEditor3Copy = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_layer_plus);
        layEditor3Save = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_save);
        layEditor3Load = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_album);
        layEditor3Text = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_text);
        layEditor3Part = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_editor3_part);

        tvAll = (TextView) findViewById(R.id.tv_gif_camera_edit2_editor_select_all);

        layEditor3X.setOnClickListener(this);
        layEditor3Delete.setOnClickListener(this);
        layEditor3Copy.setOnClickListener(this);
        layEditor3Save.setOnClickListener(this);
        layEditor3Load.setOnClickListener(this);
        layEditor3Text.setOnClickListener(this);
        layEditor3Part.setOnClickListener(this);
        layNext.setOnClickListener(this);
        layBack.setOnClickListener(this);
        layWater.setOnClickListener(this);

        tvAll.setOnClickListener(this);
        layNext.setSelected(true);

//        switchFilterTo(AppController.mFilter);
//        imvPlayer.setFilter(AppController.mFilter);
//        imvPlayer.requestRender();
        AppController.initFilter(GIFCameraEdit2Activity.this, imvPlayer);
//        initFilterLays();

        layTop = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_top);
        layBottom = (LinearLayout) findViewById(R.id.lay_gif_camera_edit2_bottom);

        if (AppController.is11) {
            layTop.setVisibility(View.VISIBLE);
            layBottom.setVisibility(View.VISIBLE);
        } else {
            layTop.setVisibility(View.GONE);
            layBottom.setVisibility(View.GONE);
        }

        ms.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                int[] tempI;
                if (thumbIndex == 0) {
                    tempI = new int[]{thumb.getValue(), AppController.sectionArr.get(tempSelectIndex)[1]};
                    Log.e("msg", "point1 : " + tempI);
                    Log.e("v1", value + "");
                } else {
                    tempI = new int[]{AppController.sectionArr.get(tempSelectIndex)[0], thumb.getValue()};
                    Log.e("msg", "point2 : " + tempI);
                    Log.e("v2", value + "");
                }

                AppController.sectionArr.set(tempSelectIndex, tempI);
                startPartPlay(tempSelectIndex);
            }
        });

        selectListSetting();
        tvAll.setSelected(true);
        for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
            selectHListLayout.getChildAt(i).setSelected(true);
        }

        startPlay();

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        try {
            super.onWindowFocusChanged(hasFocus);

            int width2 = AppController.editFrames.get(0).get(0).getbitmap().getWidth();
            int height2 = AppController.editFrames.get(0).get(0).getbitmap().getHeight();

            int w = 0, h = 0, margin = 0;
            int lw = (int) (MinUtils.density * 59);
            int lh = (int) (MinUtils.density * 23);
            FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) layWater.getLayoutParams();

            if (height2 >= width2) {
                w = width2 * imvPlayer.getHeight() / height2;
                margin = (imvPlayer.getWidth() - w) / 2;
                p.rightMargin = margin;
            } else {
                h = height2 * imvPlayer.getWidth() / width2;
                margin = (imvPlayer.getHeight() - h) / 2;
                p.bottomMargin = margin;
            }

            Log.e("!asd!", MinUtils.screenWidth + "/" + MinUtils.screenHeight + "/" + w + "/" + h + "/" + margin + "/" + imvPlayer.getWidth() + "/" + imvPlayer.getHeight());

            layWater.setLayoutParams(p);

        } catch (Exception e) {

        }
    }

    private boolean isWidth(Bitmap b) {
        int width = b.getWidth();
        int height = b.getHeight();
        if (width > height) {
            return true;
        } else {
            return false;
        }
    }

    private void selectListSetting() {

        //imgArr 부터 할 차례

        ms.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float dp = 50f;
        float fpixels = metrics.density * dp;
        int pixels = (int) (fpixels + 0.5f);

        float dp2 = 2f;
        float fpixels2 = metrics.density * dp2;
        int pixels2 = (int) (fpixels2 + 0.5f);

        selectHListLayout.removeAllViews();

        imvArr = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            imvArr.add(new ImageView(this));
            LinearLayout.LayoutParams parmas = new LinearLayout.LayoutParams(pixels, pixels);
            parmas.setMargins(pixels2, 0, 0, 0);

            imvArr.get(i).setLayoutParams(parmas);

            imvArr.get(i).setPadding(pixels2, pixels2, pixels2, pixels2);

            imvArr.get(i).setBackgroundResource(R.drawable.gif_camera_select);
            imvArr.get(i).setImageResource(R.drawable.icon_editplus_nogif);
            imvArr.get(i).setScaleType(ImageView.ScaleType.CENTER_CROP);
            selectHListLayout.addView(imvArr.get(i));
        }


        if (AppController.editFrames.size() > 6) {
            int aa = AppController.editFrames.size() - 6;
            int size = imvArr.size();

            for (int i = size; i < size + aa; i++) {
                imvArr.add(new ImageView(this));
                LinearLayout.LayoutParams parmas = new LinearLayout.LayoutParams(pixels, pixels);
                parmas.setMargins(pixels2, 0, 0, 0);

                imvArr.get(i).setLayoutParams(parmas);

                imvArr.get(i).setPadding(pixels2, pixels2, pixels2, pixels2);

                imvArr.get(i).setBackgroundResource(R.drawable.gif_camera_select);
                imvArr.get(i).setImageResource(R.drawable.icon_editplus_nogif);
                imvArr.get(i).setScaleType(ImageView.ScaleType.CENTER_CROP);
                selectHListLayout.addView(imvArr.get(i));
            }

        }

        for (int i = 0; i < AppController.editFrames.size(); i++) {
            Log.e("select size" + i, AppController.editFrames.get(i).size() + "");
        }

        for (int i = 0; i < AppController.editFrames.size(); i++) {

            selectHListLayout.getChildAt(i).setTag(i);
            bindDrapListener(selectHListLayout.getChildAt(i));

            QrBitmapFactory.Options opt = new QrBitmapFactory.Options();
            opt.inSampleSize = 8;
            Bitmap thumb = null;
            if (AppController.editFrames.get(i).size() > 0) {
                thumb = QrBitmapFactory.decodeFile(AppController.editFrames.get(i).get(0).getFile().getAbsolutePath(), opt);
                Log.e("path" + i, AppController.editFrames.get(i).get(0).getFile().getAbsolutePath());
            }

            selectHListLayout.getChildAt(i).setBackgroundResource(R.drawable.btn_onclick_camera_select);
            ((ImageView) selectHListLayout.getChildAt(i)).setImageBitmap(thumb);

        }
        tempSelectIndex = 0;
        selectHListLayout.getChildAt(0).setSelected(true);

        startPartPlay(0);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void bindDrapListener(View v) {
        v.setOnTouchListener(mOnTouchListener);
        v.setOnDragListener(mOnDragListener);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(GIFCameraEdit2Activity.this, GIFCameraEdit1Activity.class));
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_gif_camera_edit2_film_back:
                onBackPressed();
                break;
            case R.id.imv_gif_camera_edit2_film_next:
                AppController.t.set("&uid", AppController.getSp().getString("email", ""));
                AppController.t.send(new HitBuilders.EventBuilder().setCategory(getClass().getName()).setAction("Press Button").setLabel("Film Complete Click").build());

                final ProgressDialog pd = new ProgressDialog(this);
                pd.setCanceledOnTouchOutside(false);
                pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pd.show();

                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Frame> tempArrr = new ArrayList<>();
                        int wholeSize = 0;
                        for (int q = 0; q < AppController.editFrames.size(); q++) {
                            for (int w = 0; w < AppController.editFrames.get(q).size(); w++) {
                                wholeSize += 1;
                                tempArrr.add(AppController.editFrames.get(q).get(w));
                            }
                        }
                        Log.e("size", wholeSize + "");

                        int pdRatio = 0;
                        try {
                            pdRatio = 100 / (wholeSize / AppController.frameNavi);
                        } catch (ArithmeticException e) {
                            return;
                        }
                        pd.setMax(100);

                        QAGIFEncoder encoder2 = new QAGIFEncoder();
                        encoder2.setRepeat(0);
                        encoder2.setDelay(1000 / AppController.speedNavi);
                        try {
                            encoder2.start(AppController.cameraPath2);
                            int i = 0;
                            if (!AppController.isReverse) {
                                Log.e("size1", AppController.editFrames.size() + "");
                                for (int q = 0; q < AppController.editFrames.size(); q++) {
                                    Log.e("section" + q, AppController.sectionArr.get(q)[0] + "/" + AppController.sectionArr.get(q)[1]);
                                    for (int w = AppController.sectionArr.get(q)[0]; w <= AppController.sectionArr.get(q)[1]; w += AppController.frameNavi) {
                                        Frame f = AppController.editFrames.get(q).get(w);
                                        Timer t = new Timer();

                                        QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                        opt2.inSampleSize = 1;

                                        if (q == 0) {
                                            if (w == 0) {
                                                AppController.cameraThumbFile = f.getFile();
                                            }
                                        }

                                        Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

                                        int b2w = b2.getWidth();

                                        if (AppController.is11) {
                                            b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
                                        }

                                        if (AppController.mFilter != new GPUImageFilter()) {
                                            GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
                                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                            PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                            buffer.setRenderer(renderer);
                                            renderer.setImageBitmap(b2, false);
                                            b2 = buffer.getBitmap();
                                            renderer.deleteImage();
                                            buffer.destroy();
                                        }

                                        if (AppController.isPart) {

                                            Bitmap firstB = AppController.dvBitmap;
                                            firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                            b2 = AppController.overlayPart(b2, firstB);

                                        }

                                        if (AppController.waterNavi != 0) {
                                            Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                            bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                            b2 = AppController.overlayMark(b2, bb);
                                        }


                                        if (AppController.isText) {
                                            b2 = AppController.overlayMark(b2, AppController.stvBitmap);
                                        }

                                        Log.e("size2", b2.getWidth() + " / " + b2.getHeight());

                                        t.logDelay("createBitmap");
                                        encoder2.addFrame(b2);
                                        t.logDelay("addFrame");
                                        if (!pd.isShowing()) {
                                            break;
                                        }
                                        i = i + pdRatio;
                                        pd.setProgress(i);
                                    }
                                }
                            } else {
                                for (int q = AppController.editFrames.size() - 1; q >= 0; q--) {
                                    for (int w = AppController.sectionArr.get(q)[1]; w >= AppController.sectionArr.get(q)[0]; w = w - AppController.frameNavi) {
                                        Frame f = AppController.editFrames.get(q).get(w);
                                        Timer t = new Timer();

                                        QrBitmapFactory.Options opt2 = new QrBitmapFactory.Options();
                                        opt2.inSampleSize = 1;

                                        if (q == AppController.editFrames.size() - 1) {
                                            if (w == AppController.sectionArr.get(q)[1]) {
                                                AppController.cameraThumbFile = f.getFile();
                                            }
                                        }

                                        Bitmap b2 = QrBitmapFactory.decodeFile(f.getFile().getAbsolutePath(), opt2);

                                        int b2w = b2.getWidth();

                                        if (AppController.is11) {
                                            b2 = Bitmap.createBitmap(b2, 0, (b2.getHeight() - b2w) / 2, b2w, b2w);
                                        }

                                        if (AppController.mFilter != new GPUImageFilter()) {
                                            GPUImageRenderer renderer = new GPUImageRenderer(AppController.mFilter);
                                            renderer.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                                            PixelBuffer buffer = new PixelBuffer(b2.getWidth(), b2.getHeight());
                                            buffer.setRenderer(renderer);
                                            renderer.setImageBitmap(b2, false);
                                            b2 = buffer.getBitmap();
                                            renderer.deleteImage();
                                            buffer.destroy();
                                        }

                                        if (AppController.isPart) {
                                            Bitmap firstB = AppController.dvBitmap;
                                            firstB = Bitmap.createScaledBitmap(firstB, b2.getWidth(), b2.getHeight(), false);

                                            b2 = AppController.overlayPart(b2, firstB);

                                        }

                                        if (AppController.waterNavi != 0) {
                                            Bitmap bb = BitmapFactory.decodeResource(getResources(), R.drawable.water_mark);
                                            bb = Bitmap.createScaledBitmap(bb, 86, 25, false);

                                            b2 = AppController.overlayMark(b2, bb);
                                        }

                                        if (AppController.isText) {
                                            b2 = AppController.overlayMark(b2, AppController.stvBitmap);
                                        }

                                        Log.e("size2", b2.getWidth() + " / " + b2.getHeight());

                                        t.logDelay("createBitmap");
                                        encoder2.addFrame(b2);
                                        t.logDelay("addFrame");
                                        if (!pd.isShowing()) {
                                            break;
                                        }
                                        i = i + pdRatio;
                                        pd.setProgress(i);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (pd.isShowing()) {
                            encoder2.finish();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (pd.isShowing()) {
                                    pd.dismiss();
                                    Intent data = new Intent(GIFCameraEdit2Activity.this, WritePostActivityV1.class);
                                    data.putExtra("url2", AppController.cameraPath2);
                                    try {
                                        data.putExtra("thumb", AppController.cameraThumbFile.getAbsolutePath());
                                    } catch (NullPointerException e) {
                                    }
                                    data.putExtra("navi", AppController.tagNavi);
                                    if (AppController.tagNavi == 1) {
                                        data.putExtra("tag_id", AppController.tag_id);
                                        data.putExtra("tag_title", AppController.tag_title);
                                        data.putExtra("tag_body", AppController.tag_body);
                                    }
                                    startActivity(data);
                                    finish();
                                    GIFCameraFilmActivity.ac.finish();
                                }
                            }
                        });
                    }
                });
                break;
            case R.id.lay_gif_camera_edit2_editor3_x:
                onBackPressed();
                break;
            case R.id.lay_gif_camera_edit2_editor3_trash:
                if (!isSelectAll) {
                    AppController.sectionArr.remove(tempSelectIndex);
                    AppController.editFrames.remove(tempSelectIndex);
                    if (AppController.sectionArr.size() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.edit_list_size_0), Toast.LENGTH_SHORT).show();
                        finish();
                        overridePendingTransition(0, 0);
                    } else {
                        selectListSetting();
                    }
                }
                break;
            case R.id.lay_gif_camera_edit2_editor3_layer_plus:
                if (!isSelectAll) {
                    int nowSize = 0;
                    for (int i = 0; i < AppController.editFrames.size(); i++) {
                        nowSize += AppController.editFrames.get(i).size();
                    }
                    Log.e("nowSize", nowSize + "");
                    int addSize = AppController.editFrames.get(tempSelectIndex).size();
                    Log.e("addSize", addSize + "");
                    if (nowSize + addSize > 144) {
                        Toast.makeText(getApplicationContext(), "추가가능한 시간이 초과되었습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        AppController.sectionArr.add(new int[]{0, AppController.editFrames.get(tempSelectIndex).size() - 1});
                        AppController.editFrames.add(AppController.editFrames.get(tempSelectIndex));
                        selectListSetting();
                    }
//1111
                }
                break;
            case R.id.lay_gif_camera_edit2_editor3_save:
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.unservice_wait), Toast.LENGTH_SHORT).show();
                break;
            case R.id.lay_gif_camera_edit2_editor3_album:
//                doTakePictureAlbumAction();
                break;
            case R.id.lay_gif_camera_edit2_editor3_text:
                startActivity(new Intent(GIFCameraEdit2Activity.this, GIFCameraWriteActivity.class));
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.lay_gif_camera_edit2_editor3_part:
                startActivity(new Intent(GIFCameraEdit2Activity.this, GIFCameraPartActivity.class));
                finish();
                overridePendingTransition(0, 0);
                break;
            case R.id.tv_gif_camera_edit2_editor_select_all:
                ms.setVisibility(View.INVISIBLE);
                tempSelectIndex = 0;
                isSelectAll = true;
                tvAll.setSelected(true);
                for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
                    selectHListLayout.getChildAt(i).setSelected(true);
                }
                startPlay();
                break;
            case R.id.lay_gif_camera_edit2_water_mark:
                switch (AppController.waterNavi) {
                    case 0:
                        AppController.waterNavi = 1;
                        imvWater.setImageResource(R.drawable.new_water_mark);
                        break;
                    case 1:
                        AppController.waterNavi = 0;
                        imvWater.setImageResource(R.drawable.new_water_mark_c);
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    case 5:

                        break;
                    case 6:

                        break;
                    case 7:

                        break;
                    case 8:

                        break;
                    case 9:

                        break;
                }
                break;
        }
    }

    private void doTakePictureAlbumAction() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        String type = "image/*";
        intent.setDataAndType(data, type);

        startActivityForResult(intent, PICK_FROM_ALBUM);
    }

    private void startPartPlay(final int index) {
        try {

            if (AppController.editFrames.size() > 0) {
                isPlaying = false;

                playArray = new ArrayList<>();

                int duration = 1000 / AppController.speedNavi;

                tempArrFrames = AppController.editFrames.get(index);

                if (!AppController.isReverse) {
                    if (AppController.sectionArr.get(index)[1] - AppController.sectionArr.get(index)[0] <= 0) {
                        for (int i = AppController.sectionArr.get(index)[0]; i <= AppController.sectionArr.get(index)[1]; i = i + AppController.frameNavi) {
                            playArray.add(tempArrFrames.get(i).getbitmap());
                            Log.e("msg2", "i:" + i);
                        }
                    } else {
                        for (int i = AppController.sectionArr.get(index)[0]; i < AppController.sectionArr.get(index)[1]; i = i + AppController.frameNavi) {
                            playArray.add(tempArrFrames.get(i).getbitmap());
                            Log.e("msg2", "i:" + i);
                        }
                    }

                } else {
                    for (int i = AppController.sectionArr.get(index)[1]; i >= AppController.sectionArr.get(index)[0]; i = i - AppController.frameNavi) {
                        playArray.add(tempArrFrames.get(i).getbitmap());
                        Log.e("msg2", "i:" + i);
                    }
                }

                imvPlayer.setImageArray(playArray, duration);

                isPlaying = true;

            }
        } catch (IndexOutOfBoundsException e) {
            for (int i = 0; i < AppController.sectionArr.size(); i++) {
                Log.e("iobe2" + i, AppController.sectionArr.get(i)[0] + "/" + AppController.sectionArr.get(i)[1]);
            }

            for (int i = 0; i < AppController.editFrames.size(); i++) {
                Log.e("whframe2" + i, AppController.editFrames.get(i).size() + "");
            }
        }
    }

    private void startPlay() {
        try {
            isPlaying = false;

            playArray = new ArrayList<>();

            int duration = 1000 / AppController.speedNavi;

            if (!AppController.isReverse) {

                Log.e("whSize1", AppController.editFrames.size() + "");

                for (int i = 0; i < AppController.editFrames.size(); i++) {
                    for (int j = AppController.sectionArr.get(i)[0]; j <= AppController.sectionArr.get(i)[1]; j = j + AppController.frameNavi) {
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }

            } else {
                for (int i = AppController.editFrames.size() - 1; i >= 0; i--) {
                    for (int j = AppController.sectionArr.get(i)[1]; j >= AppController.sectionArr.get(i)[0]; j = j - AppController.frameNavi) {
                        //아웃오브 인덱스처리
                        playArray.add(AppController.editFrames.get(i).get(j).getbitmap());
                        Log.e("msg1", "i:" + i);
                    }
                }
            }

            imvPlayer.setImageArray(playArray, duration);

            isPlaying = true;
        } catch (Exception e) {

        }
    }

    private class DrapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
            ClipData data = ClipData.newPlainText("", "");
            MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
                    mDrapView);
            mDrapView.startDrag(data, shadowBuilder, mDrapView, 0);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            for (int i = 0; i < selectHListLayout.getChildCount(); i++) {
                selectHListLayout.getChildAt(i).setSelected(false);
            }
            mDrapView.setSelected(true);
            isSelectAll = false;
            tempSelectIndex = (int) mDrapView.getTag();
            ms.setMin(0);
            ms.setMax(AppController.editFrames.get(tempSelectIndex).size() - 1);
            ms.getThumb(0).setValue(AppController.sectionArr.get(tempSelectIndex)[0]);
            ms.getThumb(1).setValue(AppController.sectionArr.get(tempSelectIndex)[1]);
            startPartPlay(tempSelectIndex);
            tvAll.setSelected(false);
            ms.setVisibility(View.VISIBLE);
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private class MyDragShadowBuilder extends View.DragShadowBuilder {

        private final WeakReference<View> mView;

        public MyDragShadowBuilder(View view) {
            super(view);
            mView = new WeakReference<View>(view);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            canvas.scale(1.5F, 1.5F);
            super.onDrawShadow(canvas);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {

            final View view = mView.get();
            if (view != null) {
                shadowSize.set((int) (view.getWidth() * 1.5F),
                        (int) (view.getHeight() * 1.5F));
                shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
            } else {
            }
        }
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mDrapView = v;

            if (mGestureDetector.onTouchEvent(event))
                return true;

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:

                    break;
                case MotionEvent.ACTION_UP:

                    break;
            }

            return false;
        }
    };
    private View.OnDragListener mOnDragListener = new View.OnDragListener() {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setAlpha(0.5F);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setAlpha(1F);
                    break;
                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();
                    for (int i = 0, j = selectHListLayout.getChildCount(); i < j; i++) {
                        if (selectHListLayout.getChildAt(i) == v) {
                            selectHListLayout.removeView(view);
                            selectHListLayout.addView(view, i);

                            // i : 내려놓은 index  // view : 이동된뷰

                            int a = (int) view.getTag();

                            Log.e("i", i + "/" + a);

                            ArrayList<Frame> tempArr = AppController.editFrames.get(a);
                            if (a < i) {
                                AppController.editFrames.add(i + 1, AppController.editFrames.get(a));
                            } else {
                                AppController.editFrames.add(i, AppController.editFrames.get(a));
                            }

                            Log.e("size1", AppController.editFrames.size() + "");
                            for (int q = 0; q < AppController.editFrames.size(); q++) {
                                Log.e("arrSize" + q, AppController.editFrames.get(q).size() + "");
                            }
                            if (a < i) {
                                AppController.editFrames.remove(a);
                            } else {
                                AppController.editFrames.remove(a + 1);
                            }

                            Log.e("size2", AppController.editFrames.size() + "");
                            for (int q = 0; q < AppController.editFrames.size(); q++) {
                                Log.e("arrSize" + q, AppController.editFrames.get(q).size() + "");
                            }
                            Log.e("i2", i + "/" + a);

                            if (a < i) {
                                AppController.sectionArr.add(i + 1, AppController.sectionArr.get(a));
                                AppController.sectionArr.remove(a);
                            } else {
                                AppController.sectionArr.add(i, AppController.sectionArr.get(a));
                                AppController.sectionArr.remove(a + 1);
                            }

                            if (a < i) {
                            } else {
                            }

                            selectListSetting();
                            break;
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setAlpha(1F);
                default:
                    break;
            }
            return true;
        }
    };

    static class Timer {
        long lastTime = System.currentTimeMillis();

        void logDelay(String tag) {
            long current = System.currentTimeMillis();
            Log.w("Timer", String.format("delay(%s): %d", tag, (current - lastTime)));
            lastTime = current;
        }
    }
}
